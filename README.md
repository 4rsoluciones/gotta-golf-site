Andres Gotta Golf
========================

##Instalación

A continuación se listan una serie de comandos necesarios para la instalación del sitio.

* *Requisitos:*
    * Composer
    * MySQL
    * PHP >= 5.3.3 & <= 5.6.3

##1 - Composer
Instalar las dependencias y configurar los parámetros mediante el comando:

    composer install
    
##2 - Permisos
Ejecutar el siguiente comando para evitar conflitctos en los permisos de escritura de la caché:

    bin/acl.sh
    
##3 - Base de datos
Generar las tablas necesarias y cargar los fixtures mediante el comando:

    bin/reset-db.sh
    
##4 - Assets
Instalar los assets mediante el siguiente comando:

    bin/assets.sh
    
##5 - Crontab
Añadir el siguiente listado de crons.

* **Vencimiento de servicios**
    

    * * * * * php /path/to/app/console gotta:service:check

* **Cuotas débito**


    11 11 14 * * php /path/to/app/console gotta:dues:check

* **Días de prueba**


    0 0 * * * php /path/to/app/console gotta:trials:check

* **Códigos promocinoales**


    0 0 * * * php /path/to/app/console check:promotional-codes

