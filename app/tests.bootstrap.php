<?php

$database = __DIR__ . '/cache/test/database.sqlite';
$backup = __DIR__ . '/cache/test/database.sqlite.bak';

if (file_exists($backup)) {
    copy($backup, $database) or die('No pude restaurar base de datos');
} else {
    if (isset($_ENV['BOOTSTRAP_CLEAR_CACHE_ENV'])) {
        passthru(sprintf('php "%s/console" cache:clear --env=%s --no-warmup', __DIR__, $_ENV['BOOTSTRAP_CLEAR_CACHE_ENV']));
        passthru(sprintf('php "%s/console" doctrine:database:drop --env=%s --force', __DIR__, $_ENV['BOOTSTRAP_CLEAR_CACHE_ENV']));
        passthru(sprintf('php "%s/console" doctrine:database:create --env=%s', __DIR__, $_ENV['BOOTSTRAP_CLEAR_CACHE_ENV']));
        passthru(sprintf('php "%s/console" doctrine:schema:create --env=%s', __DIR__, $_ENV['BOOTSTRAP_CLEAR_CACHE_ENV']));
        passthru(sprintf('php "%s/console" doctrine:fixtures:load --env=%s --append', __DIR__, $_ENV['BOOTSTRAP_CLEAR_CACHE_ENV']));
    }

    copy($database, $backup) or die('No pude hacer backup base de datos');
}

require __DIR__ . '/bootstrap.php.cache';