<?php

use Symfony\Component\HttpKernel\Kernel;
use Symfony\Component\Config\Loader\LoaderInterface;

class AppKernel extends Kernel
{

    const VERSION = '0.2.0.1';

    public function registerBundles()
    {
        $bundles = array(
            new FOS\RestBundle\FOSRestBundle(),
            new Symfony\Bundle\FrameworkBundle\FrameworkBundle(),
            new Symfony\Bundle\SecurityBundle\SecurityBundle(),
            new Symfony\Bundle\TwigBundle\TwigBundle(),
            new Symfony\Bundle\MonologBundle\MonologBundle(),
            new Symfony\Bundle\SwiftmailerBundle\SwiftmailerBundle(),
            new Symfony\Bundle\AsseticBundle\AsseticBundle(),
            new Doctrine\Bundle\DoctrineBundle\DoctrineBundle(),
            new FOS\UserBundle\FOSUserBundle(),
            new Mopa\Bundle\BootstrapBundle\MopaBootstrapBundle(),
            new Knp\Bundle\MenuBundle\KnpMenuBundle(),
            new Knp\Bundle\PaginatorBundle\KnpPaginatorBundle(),
            new Craue\FormFlowBundle\CraueFormFlowBundle(),
            new APY\DataGridBundle\APYDataGridBundle(),
            new DavidBadura\FakerBundle\DavidBaduraFakerBundle(),
            new Genemu\Bundle\FormBundle\GenemuFormBundle(),
            new Gregwar\FormBundle\GregwarFormBundle(),
            new Finite\Bundle\FiniteBundle\FiniteFiniteBundle(),
            new Stof\DoctrineExtensionsBundle\StofDoctrineExtensionsBundle(),
            new Liip\ImagineBundle\LiipImagineBundle(),
            new WebFactory\Bundle\ContactBundle\WebFactoryContactBundle(),
            new Ivory\CKEditorBundle\IvoryCKEditorBundle(),
            new FM\ElfinderBundle\FMElfinderBundle(),
            new JMS\SerializerBundle\JMSSerializerBundle(),
            new Lexik\Bundle\FormFilterBundle\LexikFormFilterBundle(),
            new Sensio\Bundle\FrameworkExtraBundle\SensioFrameworkExtraBundle(),
            new Sensio\Bundle\DistributionBundle\SensioDistributionBundle(),
            new Sensio\Bundle\GeneratorBundle\SensioGeneratorBundle(),

            new Nelmio\ApiDocBundle\NelmioApiDocBundle(),
            new League\Tactician\Bundle\TacticianBundle(),
            new EasyCorp\Bundle\EasyAdminBundle\EasyAdminBundle(),
            new Nelmio\CorsBundle\NelmioCorsBundle(),

            //---------------------------------------------------------------
            new WebFactory\Bundle\UserBundle\WebFactoryUserBundle(),
            new WebFactory\Bundle\LocationBundle\WebFactoryLocationBundle(),
            new WebFactory\Bundle\ConfigurationBundle\WebFactoryConfigurationBundle(),
            new WebFactory\Bundle\BackendThemeBundle\WebFactoryBackendThemeBundle(),
            new WebFactory\Bundle\NewsBundle\WebFactoryNewsBundle(),

//            new AndresGotta\Bundle\ServiceBundle\AndresGottaServiceBundle(),

            new WebFactory\Bundle\CommonBundle\WebFactoryCommonBundle(),

            //todo: Este bloque debe ser removido
//            new WebFactory\Bundle\ProductBundle\WebFactoryProductBundle(),
//            new WebFactory\Bundle\OrderBundle\WebFactoryOrderBundle(),
//            new WebFactory\Bundle\CartBundle\WebFactoryCartBundle(),
//            new WebFactory\Bundle\PaymentBundle\WebFactoryPaymentBundle(),
//            new AndresGotta\Bundle\SaleBundle\AndresGottaSaleBundle(),

            new WebFactory\Bundle\NewsLetterBundle\WebFactoryNewsLetterBundle(),
            new WebFactory\Bundle\MessagingBundle\WebFactoryMessagingBundle(),
            new WebFactory\Bundle\FaqBundle\WebFactoryFaqBundle(),
            new WebFactory\Bundle\FormBundle\WebFactoryFormBundle(),
            new WebFactory\Bundle\RestBundle\WebFactoryRestBundle(),
            new WebFactory\Bundle\PushNotificationBundle\WebFactoryPushNotificationBundle(),

            //---------------------------------------------------------------
            new AndresGotta\Bundle\FrontendThemeBundle\AndresGottaFrontendThemeBundle(),
            new AndresGotta\Bundle\HomepageBundle\AndresGottaHomepageBundle(),
            new AndresGotta\Bundle\VideoBundle\AndresGottaVideoBundle(),
            new AndresGotta\Bundle\GroupBundle\AndresGottaGroupBundle(),
            new AndresGotta\Bundle\GolfBundle\AndresGottaGolfBundle(),
            new AndresGotta\Bundle\MailBundle\AndresGottaMailBundle(),
            new AndresGotta\Bundle\AcademyBundle\AndresGottaAcademyBundle(),
        );

        if (in_array($this->getEnvironment(), array('dev', 'test'))) {
            $bundles[] = new Symfony\Bundle\WebProfilerBundle\WebProfilerBundle();
            $bundles[] = new Elao\WebProfilerExtraBundle\WebProfilerExtraBundle();
            $bundles[] = new RaulFraile\Bundle\LadybugBundle\RaulFraileLadybugBundle();
            $bundles[] = new DavidBadura\FixturesBundle\DavidBaduraFixturesBundle();
            $bundles[] = new Doctrine\Bundle\FixturesBundle\DoctrineFixturesBundle();
            $bundles[] = new CoreSphere\ConsoleBundle\CoreSphereConsoleBundle();
        }

        return $bundles;
    }

    public function registerContainerConfiguration(LoaderInterface $loader)
    {
        $loader->load(__DIR__.'/config/config_'.$this->getEnvironment().'.yml');
    }
}
