<?php

namespace WebFactory\Bundle\CommonBundle\Model;

use Doctrine\ORM\EntityManager;

class TagManager implements TagManagerInterface
{

    /**
     * @var EntityManager
     */
    protected $em;

    /**
     * @var string
     */
    protected $repositoryId;

    /**
     * @var string
     */
    protected $tagClass;

    /**
     * Ctor.
     *
     * @param EntityManager $em
     */
    public function __construct(EntityManager $em, $repositoryId, $tagClass)
    {
        $this->em = $em;
        $this->repositoryId = $repositoryId;
        $this->tagClass = $tagClass;
    }

    /**
     * 
     * @param string $name
     * @return 
     */
    public function findOneByNameOrCreate($name)
    {
        $repository = $this->em->getRepository($this->repositoryId);
        $tag = $repository->findOneByName($name);

        if (null === $tag) {
            $tag = $this->create();
            $tag->setName($name);

            $this->em->persist($tag);
        }

        return $tag;
    }

    /**
     * 
     * @return 
     */
    protected function create()
    {
        $tagClass = $this->tagClass;

        return new $tagClass();
    }

}