<?php

namespace WebFactory\Bundle\CommonBundle\Model;

use APY\DataGridBundle\Grid\Mapping as Grid;
use DateTime;
use Doctrine\ORM\Mapping as ORM;
use JMS\Serializer\Annotation\Type;
use Symfony\Component\HttpFoundation\File\UploadedFile;
use Symfony\Component\Validator\Constraints as Assert;
use Gedmo\Mapping\Annotation as Gedmo;

/**
 * Image Model
 *
 * @ORM\MappedSuperclass
 * @ORM\HasLifecycleCallbacks
 */
abstract class Image
{
 
    /**
     * Directorio relativo a guardar las imágenes
     */
    const DIRECTORY = 'uploads/DIRECTORY/images';

    /**
     * @var UploadedFile
     * @Assert\File(
     *     maxSize = "40M"
     * )
     * @Assert\Image(
     *     mimeTypes={"image/jpeg"},
     *     maxSize="40M",
     *     minWidth=220,
     *     minHeight=140,
     *     maxWidth=1920,
     *     maxHeight=1280
     * )
     */
    protected $file;

    /**
     * Representa el path del archivo
     *
     * @var string
     * @ORM\Column(name="file_path",type="string", length=255, nullable=true)
     * @Grid\Column(title="File Path")
     * @Type("string")
     */
    protected $filePath;

    /**
     * Determina si debe eliminar la imágen actual
     *
     * @var boolean
     */
    protected $removeFile;

    /**
     * @var DateTime
     *
     * @Gedmo\Timestampable(on="create")
     * @ORM\Column(name="created_at", type="datetime", nullable=true)
     * @Type("DateTime")
     */
    protected $createdAt;

    /**
     * @var DateTime
     *
     * @Gedmo\Timestampable(on="update")
     * @ORM\Column(name="updated_at", type="datetime", nullable=true)
     * @Type("DateTime")
     */
    protected $updatedAt;

    /**
     * Constructor
     */
    public function __construct()
    {
        $this->createdAt = new DateTime;
        $this->updatedAt = new DateTime;
    }

    /**
     * Get id
     *
     * @return integer
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Get FilePath
     *
     * @return string
     */
    public function getFilePath()
    {
        return $this->filePath;
    }

    /**
     * Set FilePath
     *
     * @param string $filePath
     * @return Image
     */
    public function setFilePath($filePath)
    {
        $this->filePath = $filePath;
        return $this;
    }

    /**
     *
     * @return DateTime
     */
    public function getCreatedAt()
    {
        return $this->createdAt;
    }

    /**
     *
     * @param DateTime $createdAt
     * @return Image
     */
    public function setCreatedAt(DateTime $createdAt)
    {
        $this->createdAt = $createdAt;
        return $this;
    }

    /**
     *
     * @return DateTime
     */
    public function getUpdatedAt()
    {
        return $this->updatedAt;
    }

    /**
     *
     * @param DateTime $updatedAt
     * @return Image
     */
    public function setUpdatedAt(DateTime $updatedAt)
    {
        $this->updatedAt = $updatedAt;

        return $this;
    }

    //--------------------------------------------------------------------------
    /**
     *
     * @return UploadedFile
     */
    public function getFile()
    {
        return $this->file;
    }

    /**
     *
     * @param UploadedFile $file
     * @return Image
     */
    public function setFile($file)
    {
        $this->file = $file;
        $this->updatedAt = new DateTime(); // Hack!
        return $this;
    }

    /**
     *
     * @return boolean
     */
    public function getRemoveFile()
    {
        return $this->removeFile;
    }

    /**
     *
     * @param boolean $removeFile
     * @return Image
     */
    public function setRemoveFile($removeFile)
    {
        $this->removeFile = $removeFile;
        if ($this->removeFile) {
            $this->updatedAt = new DateTime(); // Hack!
        }
        return $this;
    }

    /**
     * Virtual getter that returns logo web path
     * @return string
     */
    public function getCover()
    {
        return $this->getWebPath();
    }

    public function getAbsolutePath()
    {
        return null === $this->filePath ? null : $this->getUploadRootDir() . '/' . $this->filePath;
    }

    public function getWebPath()
    {
        return null === $this->filePath ? null : $this->getUploadDir() . '/' . $this->filePath;
    }

    protected function getUploadRootDir()
    {
        return __DIR__ . '/../../../../../web/' . $this->getUploadDir();
    }

    protected function getUploadDir()
    {
        return static::DIRECTORY;
    }

    protected function removeOldUpload()
    {
        if (!empty($this->filePathOld) && is_file($this->filePathOld)) {
            @unlink($this->filePathOld);
            unset($this->filePathOld);
        }
    }

    public function storeFilenameForRemove()
    {
        if ($this->getAbsolutePath()) {
            $this->filePathOld = $this->getAbsolutePath();
        }
    }

    /**
     * @ORM\PrePersist
     * @ORM\PreUpdate
     */
    public function preUpload()
    {
        if (null !== $this->file) {
            // do whatever you want to generate a unique name
            $filename = sha1(uniqid(mt_rand(), true)) . $this->file->getClientOriginalName();
            $this->storeFilenameForRemove();
            $this->filePath = $filename;
        } elseif ($this->removeFile) {
            $this->storeFilenameForRemove();
            $this->filePath = '';
        }
    }

    /**
     * @ORM\PostPersist
     * @ORM\PostUpdate
     */
    public function upload()
    {
        if (null === $this->file && !$this->removeFile) {
            return;
        }

        if ($this->file) {
            $this->file->move($this->getUploadRootDir(), $this->filePath);
            $this->file = null;
        }

        $this->removeOldUpload();
    }

    /**
     * @ORM\PostRemove
     */
    public function removeUpload()
    {
        if (($file = $this->getAbsolutePath())) {
            @unlink($file);
        }
    }

}