<?php

namespace WebFactory\Bundle\CommonBundle\Model;

interface TagManagerInterface
{
    public function findOneByNameOrCreate($name);
}