Bundle con utilidades comunes


# Doctrine Configuration
doctrine:
    orm:
        entity_managers:
            default:
                auto_mapping: true
                mappings:
                    # por bug en symfony o doctrine
                    common_image:
                        type: annotation
                        prefix: WebFactory\Bundle\CommonBundle\Model
                        dir: "%kernel.root_dir%/../src/WebFactory/Bundle/CommonBundle/Model"
                        alias: CommonImage
                        is_bundle: false