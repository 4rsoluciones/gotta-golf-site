<?php

namespace WebFactory\Bundle\CommonBundle\Twig;

use Twig_Extension;
use Twig_SimpleFilter;
use Twig_SimpleFunction;

/**
* String Extension
*/
class StringExtension extends Twig_Extension
{
	
	public function getFilters()
    {
        return array(
            new Twig_SimpleFilter('toStr', array($this, 'convert')),
        );
    }

    public function getName()
    {
        return 'webfactory_common_string_extension';
    }

    public function convert($from)
    {
    	$to = $from;

    	if(is_bool($from)) {
    		$to = $from ? 'yes' : 'no';
    	}

    	return $to;
    }
}