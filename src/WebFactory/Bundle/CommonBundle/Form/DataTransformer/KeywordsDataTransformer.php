<?php

namespace WebFactory\Bundle\CommonBundle\Form\DataTransformer;

use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Symfony\Component\Form\DataTransformerInterface;
use Symfony\Component\Form\Exception\UnexpectedTypeException;
use WebFactory\Bundle\CommonBundle\Model\TagManagerInterface;

/**
 * Keywords DataTransformer.
 */
class KeywordsDataTransformer implements DataTransformerInterface
{
    /**
     * @var TagManagerInterface
     */
    private $tagManager;

    /**
     * Ctor.
     *
     * @param TagManagerInterface $tagManager
     */
    public function __construct(TagManagerInterface $tagManager)
    {
        $this->tagManager = $tagManager;
    }

    /**
     * Convert string of tags to array.
     *
     * @param string $string
     *
     * @return array
     */
    private function stringToArray($string)
    {
        $keywords = explode(',', $string);

        // strip whitespaces from beginning and end of a keyword text
        foreach ($keywords as &$keyword) {
            $keyword = trim($keyword);
        }

        // removes duplicates
        return array_unique($keywords);
    }

    /**
     * Transforms tags entities into string (separated by comma).
     *
     * @param Collection | null $tagCollection A collection of entities or NULL
     *
     * @return string | null An string of tags or NULL
     * @throws UnexpectedTypeException
     */
    public function transform($keywordCollection)
    {
        if (null === $keywordCollection) {
            return null;
        }

        if (!($keywordCollection instanceof Collection)) {
            throw new UnexpectedTypeException($keywordCollection, 'Doctrine\Common\Collections\Collection');
        }

        $keywords = array();

        foreach ($keywordCollection as $keyword) {
            array_push($keywords, $keyword->getName());//Necesitamos una interface
        }

        return implode(', ', $keywords);
    }

    /**
     * Transforms string into keyword entities.
     *
     * @param string | null $data Input string data
     *
     * @return Collection
     * @throws UnexpectedTypeException
     */
    public function reverseTransform($data)
    {
        $keywordCollection = new ArrayCollection();

        if ('' === $data || null === $data) {
            return $keywordCollection;
        }

        if (!is_string($data)) {
            throw new UnexpectedTypeException($data, 'string');
        }

        foreach ($this->stringToArray($data) as $name) {
            $keyword = $this->tagManager->findOneByNameOrCreate($name);
            $keywordCollection->add($keyword);
        }

        return $keywordCollection;
    }
}