<?php

namespace WebFactory\Bundle\CommonBundle\Form\Type;

use InvalidArgumentException;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormInterface;
use Symfony\Component\Form\FormView;
use WebFactory\Bundle\CommonBundle\Model\Image;

class FilePreviewType extends AbstractType
{

    public function buildView(FormView $view, FormInterface $form, array $options)
    {
        $data = $form->getParent()->getNormData();

        if($data) {
            if(!$data instanceof Image) {
                throw new InvalidArgumentException('\WebFactory\Bundle\CommonBundle\Model\Image object expected');
            }
            $view->vars['cover'] = $data->getCover();
            $view->vars['filePath'] = $data->getFilePath();
            $view->vars['isImg'] = is_array(@getimagesize($data->getAbsolutePath()));
        }
    }

    public function getParent()
    {
        return 'file';
    }

    public function getName()
    {
        return 'file_preview';
    }

}
