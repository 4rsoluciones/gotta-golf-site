<?php

namespace WebFactory\Bundle\CommonBundle\Form\Type;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use WebFactory\Bundle\CommonBundle\Form\DataTransformer\KeywordsDataTransformer;
use WebFactory\Bundle\CommonBundle\Model\TagManagerInterface;

class KeywordType extends AbstractType
{

    /**
     * @var TagManagerInterface
     */
    private $tagManager;

    /**
     * Ctor.
     *
     * @param TagManagerInterface $tagManager
     */
    public function __construct(TagManagerInterface $tagManager)
    {
        $this->tagManager = $tagManager;
    }

    /**
     * {@inheritdoc}
     */
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder->addViewTransformer(
                new KeywordsDataTransformer(
                $this->tagManager
                ), true
        );
    }

    /**
     * {@inheritdoc}
     */
    public function getParent()
    {
        return 'text';
    }

    /**
     * {@inheritdoc}
     */
    public function getName()
    {
        return 'keyword';
    }

}