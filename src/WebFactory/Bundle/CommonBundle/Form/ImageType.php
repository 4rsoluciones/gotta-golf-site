<?php

namespace WebFactory\Bundle\CommonBundle\Form;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolverInterface;
use Symfony\Component\Form\FormEvent;
use Symfony\Component\Form\FormEvents;

class ImageType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('file', 'file_preview', array(
                'required' => false,
                'label' => false,
            ))
        ;
        
        $builder->addEventListener(
                FormEvents::PRE_SET_DATA, function(FormEvent $event) {
                    $data = $event->getData();
                    $form = $event->getForm();

                    if ($data === NULL || !$data->getFilePath()) {
                        return NULL;
                    }

                    $form->add('removeFile', 'checkbox', array('required' => false));
                }
        );
    }

    public function setDefaultOptions(OptionsResolverInterface $resolver)
    {
        $resolver->setDefaults(array(
            'data_class' => 'WebFactory\Bundle\NewsBundle\Entity\Image'
        ));
    }

    public function getName()
    {
        return 'image';
    }
}
