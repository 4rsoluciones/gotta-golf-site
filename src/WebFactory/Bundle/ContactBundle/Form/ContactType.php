<?php

namespace WebFactory\Bundle\ContactBundle\Form;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolverInterface;
use Symfony\Component\Validator\Constraints\Collection;
use Symfony\Component\Validator\Constraints\Email;
use Symfony\Component\Validator\Constraints\Length;
use Symfony\Component\Validator\Constraints\NotBlank;

class ContactType extends AbstractType
{

    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
                ->add('name', 'text', array(
                    'attr' => array(
                        'pattern' => '.{2,}'
                    )
                ))
                ->add('email', 'email', array(
                    'attr' => array(
                    )
                ))
//                //TODO: Acoplamiento con AndresGottaServiceBundle
//                ->add('category', 'entity', array(
//                    'class' => 'AndresGottaServiceBundle:Area',
//                    'query_builder' => function ($r) {
//                        return $r->createQueryBuilder('Area')->orderBy('Area.name', 'asc');
//                    },
//                ))
                ->add('subject', 'text', array(
                    'attr' => array(
                        'pattern' => '.{3,}'
                    )
                ))
                ->add('message', 'textarea', array(
                    'attr' => array(
                        'cols' => 90,
                        'rows' => 10,
                    )
                ))
                ->add('subscribeToNewsLetter', 'checkbox', array(
                    'widget_checkbox_label' => 'label',
                    'help_block' => 'Check to subscribe to our newsletter',
                    'widget_controls_attr' => array('class' => 'helper'),
                ));
    }

    public function setDefaultOptions(OptionsResolverInterface $resolver)
    {
        $collectionConstraint = new Collection(array(
            'name' => array(
                new NotBlank,
                new Length(array('min' => 2))
            ),
//            'category' => array(
//                new NotBlank,
//            ),
            'email' => array(
                new NotBlank,
                new Email
            ),
            'subject' => array(
                new NotBlank,
                new Length(array('min' => 3))
            ),
            'message' => array(
                new NotBlank,
                new Length(array('min' => 5))
            ),
            'subscribeToNewsLetter' => array(

            )
        ));

        $resolver->setDefaults(array(
            'constraints' => $collectionConstraint,
            'data_class' => null
        ));
    }

    public function getName()
    {
        return 'contact';
    }

}
