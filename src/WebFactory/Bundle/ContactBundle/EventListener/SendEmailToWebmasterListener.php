<?php

namespace WebFactory\Bundle\ContactBundle\EventListener;

use Symfony\Component\HttpFoundation\Request;
use Symfony\Bundle\TwigBundle\TwigEngine;
use WebFactory\Bundle\ContactBundle\Event\ContactEvent;
use WebFactory\Bundle\ConfigurationBundle\Manager\ConfigurationManager;
use Swift_Mailer;
use WebFactory\Bundle\ContactBundle\Form\ContactType;

class SendEmailToWebmasterListener
{
    /**
     * @var Request
     */
    protected $request;

    /**
     * @var TwigEngine
     */
    private $templating;

    /**
     * @var string
     */
    private $contactEmail;

    /**
     * @var ConfigurationManager
     */
    private $configurationManager;

    /**
     * @var Swift_Mailer
     */
    private $mailer;

    /**
     * [__construct description]
     * @param TwigEngine           $templating           [description]
     * @param [type]               $contactEmail         [description]
     * @param ConfigurationManager $configurationManager [description]
     * @param Swift_Mailer         $mailer               [description]
     */
    public function __construct(TwigEngine $templating, $contactEmail, ConfigurationManager $configurationManager, Swift_Mailer $mailer)
    {
        $this->templating = $templating;
        $this->contactEmail = $contactEmail;
        $this->configurationManager = $configurationManager;
        $this->mailer = $mailer;
    }

    public function setRequest(Request $request = null)
    {
        $this->request = $request;
    }

    /**
     * Envia email al administrador
     *
     * @param  ContactEvent $event [description]
     * @return [type]              [description]
     */
    public function onContactSubmitted(ContactEvent $event)
    {
        $contact = $event->getContact();

        $message = \Swift_Message::newInstance()
            ->setSubject($contact['subject'])
            ->setFrom($contact['email'])
            ->setTo($this->_getEmailTo($contact))
            ->setBody(
                $this->templating->render(
                    'WebFactoryContactBundle:Frontend/Contact:email.html.twig',
                    array(
                        'ip' => $this->request->getClientIp(),
                        'name' => $contact['name'],
                        'email' => $contact['email'],
//                        'category' => $contact['category'],
                        'subject' => $contact['subject'],
                        'message' => $contact['message'],
                    )
                ), 'text/html'
            );

        $this->mailer->send($message);
    }

    /**
     * Resuelve y retorna el email de contacto
     *
     * @param array $contact Datos del contacto
     *
     * @return string El email de contacto configurado
     */
    private function _getEmailTo($contact)
    {
        $emailTo = $this->configurationManager->get('contact_email');
        if (!$emailTo) {
            $emailTo = $this->contactEmail;
        }

        return $emailTo;
    }
}