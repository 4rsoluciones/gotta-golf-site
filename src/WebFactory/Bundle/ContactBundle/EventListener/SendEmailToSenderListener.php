<?php

namespace WebFactory\Bundle\ContactBundle\EventListener;

use Symfony\Component\HttpFoundation\Request;
use Symfony\Bundle\TwigBundle\TwigEngine;
use WebFactory\Bundle\ContactBundle\Event\ContactEvent;
use WebFactory\Bundle\ConfigurationBundle\Manager\ConfigurationManager;
use Swift_Mailer;

class SendEmailToSenderListener
{
    /**
     * @var TwigEngine
     */
    private $templating;

    /**
     * @var Swift_Mailer
     */
    private $mailer;


    /**
     * Constructor
     *
     * @param TwigEngine   $templating
     * @param Swift_Mailer $mailer
     */
    public function __construct(TwigEngine $templating, Swift_Mailer $mailer)
    {
        $this->templating = $templating;
        $this->mailer = $mailer;
    }

    /**
     * Envia email al sender
     *
     * @param  ContactEvent $event [description]
     * @return [type]              [description]
     */
    public function onContactSubmitted(ContactEvent $event)
    {
        $contact = $event->getContact();

        $message = \Swift_Message::newInstance()
            ->setSubject('Mensaje Recibido')
            ->setFrom('noreply@localhost')// Posible bug?
            ->setTo($contact['email'])
            ->setBody(
                $this->templating->render(
                    'WebFactoryContactBundle:Frontend/Contact:email_recepted.html.twig',
                    array()
                ), 'text/html'
            );

        $this->mailer->send($message);
    }

}