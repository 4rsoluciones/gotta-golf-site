<?php

namespace WebFactory\Bundle\ContactBundle\Tests\Controller\Frontend;

use Symfony\Bundle\FrameworkBundle\Test\WebTestCase;

class ContactControllerTest extends WebTestCase
{
    public function testSendContactForm()
    {
        $client = static::createClient();

        $crawler = $client->request('GET', '/contact/');

        $this->assertTrue(200 === $client->getResponse()->getStatusCode());

        $form = $crawler->filter('form')->form(
            array(
                'contact[name]'  => 'Gastón Leandro',
                'contact[email]'  => 'listeroabsoluto@gmail.com',
//                'contact[category]'  => '1', // id
                'contact[subject]'  => 'Mi motivo es el de ...',
                'contact[message]'  => 'Lorem ipsum dolor...',
                'contact[subscribeToNewsLetter]'  => '1',
            )
        );

        $form->get('contact[subscribeToNewsLetter]')->tick();
        $crawler = $client->submit($form);

        $this->assertEquals(302, $client->getResponse()->getStatusCode());

        $profile = $client->getProfile();
        $client->enableProfiler();

        $crawler = $client->request('GET', '/profiler');

        if ($profile) {
            $collector = $profile->getCollector('swiftmailer');

            $this->assertEquals(2, $collector->getMessageCount());
        }

    }
}
