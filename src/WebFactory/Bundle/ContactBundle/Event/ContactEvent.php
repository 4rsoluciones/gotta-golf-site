<?php

namespace WebFactory\Bundle\ContactBundle\Event;

use Symfony\Component\EventDispatcher\Event;

class ContactEvent extends Event
{
    private $contact;

    public function __construct(array $contact)
    {
        $this->contact = $contact;
    }

    public function getContact()
    {
        return $this->contact;
    }
}