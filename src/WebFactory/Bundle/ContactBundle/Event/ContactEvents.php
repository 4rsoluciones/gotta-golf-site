<?php

namespace WebFactory\Bundle\ContactBundle\Event;

final class ContactEvents
{
    /**
     * This event occurs when a contact is submitted
     *
     *
     * @var string
     */
    const SUBMITTED = 'contact.submitted';
}