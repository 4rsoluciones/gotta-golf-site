<?php

namespace WebFactory\Bundle\ContactBundle\Controller\Frontend;

use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Template;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use WebFactory\Bundle\ContactBundle\Form\ContactType;
use Symfony\Component\HttpFoundation\Request;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;
use WebFactory\Bundle\ContactBundle\Event\ContactEvents;
use WebFactory\Bundle\ContactBundle\Event\ContactEvent;

/**
 * @Route("/contact")
 */
class ContactController extends Controller
{
    /**
     * @Route("/", name="frontend_contact")
     * @Method({"GET"})
     * @Template()
     */
    public function indexAction()
    {
        $form = $this->createForm(new ContactType());

        return array(
            'form' => $form->createView(),
        );
    }

    /**
     * @Route("/", name="frontend_contact_send")
     * @Method({"POST"})
     * @Template("WebFactoryContactBundle:Frontend/Contact:index.html.twig")
     */
    public function sendAction(Request $request)
    {
        $form = $this->createForm(new ContactType());

        $form->bind($request);

        if ($form->isValid()) {

            $data = $form->getData();
            $contactEvent = new ContactEvent($data);
            $contactEvent = $this->get('event_dispatcher')->dispatch(ContactEvents::SUBMITTED, $contactEvent);

            if (!$contactEvent->isPropagationStopped()) {
                //Creo que esto deberia ir en los listeners
                $request->getSession()->getFlashBag()->add('success', 'Your email has been sent! Thanks!');
                return $this->redirect($this->generateUrl('frontend_contact'));
            }
        }

        return array(
            'form' => $form->createView(),
        );
    }

}
