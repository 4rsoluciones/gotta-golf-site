<?php

namespace WebFactory\Bundle\OrderBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use Gedmo\Mapping\Annotation as Gedmo;
use Symfony\Component\Validator\Constraints as Assert;
use WebFactory\Bundle\OrderBundle\Model\AbstractOrder;
use WebFactory\Bundle\OrderBundle\Model\OrderStatuses;
use APY\DataGridBundle\Grid\Mapping as Grid;
use Symfony\Component\Validator\ExecutionContextInterface;

/**
 * Order
 *
 * @ORM\Table(name="orders")
 * @ORM\Entity(repositoryClass="WebFactory\Bundle\OrderBundle\ORM\Repository\OrderRepository")
 */
class Order extends AbstractOrder
{
    /**
     * @var integer
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     * @Grid\Column(filterable=false)
     */
    protected $id;

    /**
     * @var Item
     *
     * @ORM\OneToMany(targetEntity="Item", mappedBy="order", cascade={"persist"})
     * @Assert\NotBlank
     */
    protected $items;

    /**
     * @var Payment
     *
     * @ORM\OneToOne(targetEntity="WebFactory\Bundle\PaymentBundle\Entity\Payment", mappedBy="order", cascade={"persist"})
     */
    protected $payment;
    
    /**
     * @var \WebFactory\Bundle\UserBundle\Entity\User
     *
     * @ORM\ManyToOne(targetEntity="\WebFactory\Bundle\UserBundle\Entity\User")
     * @ORM\JoinColumn(name="user_id", referencedColumnName="id")
     * @Assert\NotBlank
     * @Grid\Column(title="User", field="user.email")
     */
    protected $user;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="created_at", type="datetime")
     * @Gedmo\Timestampable(on="create")
     * @Grid\Column(title="Created At")
     */
    protected $createdAt;
    
    /**
     * @var \DateTime
     *
     * @ORM\Column(name="updated_at", type="datetime", nullable=true)
     * @Gedmo\Timestampable(on="update")
     */
    protected $updatedAt;

    /**
     * @var string
     * 
     * @ORM\Column(name="status", type="string", length=255)
     * @Assert\NotBlank
     * @Grid\Column(title="Status", filter="select", selectFrom="values", values={
     *      "order.status.pending"="order.status.pending",
     *      "order.status.paid"="order.status.paid",
     *      "order.status.cancelled"="order.status.cancelled"
     * })
     */
    protected $status = OrderStatuses::PENDING;
    
    /**
     * @var integer 
     * 
     *@ORM\Column(name="expire_days_for_payment", type="integer")
     */
//    protected $expireDaysForPayment;
    
    /**
     * @var boolean 
     *
     *@ORM\Column(name="pay_cash", type="boolean", nullable=true)
     * @Grid\Column(title="Pay Cash")
     */
    protected $payCash;

    /**
     * @var OrderDebitCard
     * 
     * @ORM\OneToOne(targetEntity="OrderDebitCard", mappedBy="order", cascade={"all"})
     */
    private $debitCard;

    /**
     * @var string
     *
     * @ORM\Column(name="promotional_code", type="string", nullable=true)
     */
    private $promotionalCode;

    /**
     * @var integer
     *
     * @ORM\Column(name="discount", type="integer", nullable=true)
     */
    private $discount;



    /**
     * Get id
     *
     * @return integer 
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set createdAt
     *
     * @param \DateTime $createdAt
     * @return Order
     */
    public function setCreatedAt($createdAt)
    {
        $this->createdAt = $createdAt;

        return $this;
    }

    /**
     * Get createdAt
     *
     * @return \DateTime 
     */
    public function getCreatedAt()
    {
        return $this->createdAt;
    }
    
    /**
     * Get updatedAt
     * 
     * @return \DateTime
     */
    public function getUpdatedAt()
    {
        return $this->updatedAt;
    }

    /**
     * Set updatedAt
     * 
     * @param \DateTime $updatedAt
     * @return Order
     */
    public function setUpdatedAt(\DateTime $updatedAt)
    {
        $this->updatedAt = $updatedAt;

        return $this;
    }

    /**
     * @return boolean
     */
    public function getPayCash()
    {
        return $this->payCash;
    }

    /**
     * @param boolean $payCash
     */
    public function setPayCash($payCash)
    {
        $this->payCash = $payCash;
        
        return $this;
    }

    /**
     * @return OrderDebitCard
     */
    public function getDebitCard()
    {
        return $this->debitCard;
    }

    /**
     * @param OrderDebitCard
     * @return Order
     */
    public function setDebitCard(\WebFactory\Bundle\OrderBundle\Entity\OrderDebitCard $debitCard)
    {
        $this->debitCard = $debitCard;
        
        return $this;
    }
    
    /**
     * @Assert\Callback
     */
    public function validate(ExecutionContextInterface $context)
    {
        if (!in_array($this->getStatus(), OrderStatuses::getChoices())) {
            $context->addViolationAt(
                'status',
                'Invalid order status',
                array(),
                null
            );
        }
    }

    /**
     * @return string
     */
    public function getPromotionalCode()
    {
        return $this->promotionalCode;
    }

    /**
     * @param string $promotionalCode
     */
    public function setPromotionalCode($promotionalCode)
    {
        $this->promotionalCode = $promotionalCode;
    }

    /**
     * @return int
     */
    public function getDiscount()
    {
        return $this->discount;
    }

    /**
     * @param int $discount
     */
    public function setDiscount($discount)
    {
        $this->discount = $discount;
    }
}
