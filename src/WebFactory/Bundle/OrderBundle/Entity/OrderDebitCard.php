<?php

namespace WebFactory\Bundle\OrderBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * OrderDebitCard
 *
 * @ORM\Table(name="order_debit_cards")
 * @ORM\Entity
 */
class OrderDebitCard
{
    /**
     * @var integer
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @var Order
     *
     * @ORM\OneToOne(targetEntity="\WebFactory\Bundle\OrderBundle\Entity\Order", inversedBy="debitCard")
     * @ORM\JoinColumn(name="order_id", referencedColumnName="id")
     */
    private $order;

    /**
     * @var string 
     * 
     *@ORM\Column(name="card_type", type="string")
     */
    private $provider;

    /**
     * @var bigint 
     * 
     *@ORM\Column(name="card_number", type="bigint")
     */
    private $cardNumber;
    

    /**
     * Get id
     *
     * @return integer
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set provider
     *
     * @param string $provider
     *
     * @return OrderDebitCard
     */
    public function setProvider($provider)
    {
        $this->provider = $provider;

        return $this;
    }

    /**
     * Get provider
     *
     * @return string
     */
    public function getProvider()
    {
        return $this->provider;
    }

    /**
     * Set cardNumber
     *
     * @param integer $cardNumber
     *
     * @return OrderDebitCard
     */
    public function setCardNumber($cardNumber)
    {
        $this->cardNumber = $cardNumber;

        return $this;
    }

    /**
     * Get cardNumber
     *
     * @return integer
     */
    public function getCardNumber()
    {
        return $this->cardNumber;
    }

    /**
     * Set order
     *
     * @param \WebFactory\Bundle\OrderBundle\Entity\Order $order
     *
     * @return OrderDebitCard
     */
    public function setOrder(\WebFactory\Bundle\OrderBundle\Entity\Order $order = null)
    {
        $this->order = $order;

        return $this;
    }

    /**
     * Get order
     *
     * @return \WebFactory\Bundle\OrderBundle\Entity\Order
     */
    public function getOrder()
    {
        return $this->order;
    }
}
