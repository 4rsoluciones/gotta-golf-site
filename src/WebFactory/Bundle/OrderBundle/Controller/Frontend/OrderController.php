<?php

namespace WebFactory\Bundle\OrderBundle\Controller\Frontend;

use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Template;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;
use WebFactory\Bundle\OrderBundle\Entity\Order;
use WebFactory\Bundle\OrderBundle\Form\OrderFilterType;
use WebFactory\Bundle\OrderBundle\Model\OrderStatuses;
use WebFactory\Bundle\PaymentBundle\Factory\PaymentFactory;
use WebFactory\Bundle\PaymentBundle\Model\PaymentStatuses;
use AndresGotta\Bundle\SaleBundle\Form\AddAmexCardType;
use AndresGotta\Bundle\SaleBundle\Form\AddMastercardCardType;
use AndresGotta\Bundle\SaleBundle\Entity\AmexCard;
use AndresGotta\Bundle\SaleBundle\Entity\MastercardCard;

/**
 * @Route("/order")
 */
class OrderController extends Controller
{

    /**
     * @Route("/")
     * @Method({"GET"})
     * @Template()
     */
    public function indexAction(Request $request)
    {
        $filterBuilder = $this->get('web_factory_order.order.manager')->findOrdersByUser($this->getUser(), true);
        $filterForm = $this->createForm(new OrderFilterType);

        if ($request->query->has('submit-filter')) {
            $filterForm->submit($request);
            $this->get('lexik_form_filter.query_builder_updater')->addFilterConditions($filterForm, $filterBuilder);
        }

        $pagination = $this->get('knp_paginator')->paginate(
                $filterBuilder, $request->query->get('page', 1), 10
        );

        return array(
            'filter_form' => $filterForm->createView(),
            'pagination' => $pagination,
        );
    }

    /**
     * @Route("/{id}/show")
     * @Method({"GET"})
     * @Template()
     */
    public function showAction(Request $request, Order $order)
    {
        if ($request->get('selected_card')) {
            $this->get('web_factory_order.order.manager')->payOrderByDebit($order, $request->get('selected_card'));
            $url = $this->generateUrl('webfactory_order_frontend_order_index');

            return $this->redirect($url);
        }

        $amexCard = new AmexCard;
        $addAmexCardForm = $this->createForm(new AddAmexCardType, $amexCard);
        if ($request->query->has('add_amex_card_type')) {
            $addAmexCardForm->submit($request);
            $amexCardManager = $this->get('andres_gotta_sale.amex_card_manager');
            $card = $amexCardManager->addCard($this->getUser(), $amexCard->getCardNumber());
            
            $url = $this->generateUrl('webfactory_order_frontend_order_show', array(
                'id' => $order->getId(),
                'amexNewCard' => $card->getId(),
            ));

            return $this->redirect($url);
        }

        $mastercardCard = new MastercardCard;
        $addMastercardCardForm = $this->createForm(new AddMastercardCardType, $mastercardCard);
        if ($request->query->has('add_mastercard_card_type')) {
            $addMastercardCardForm->submit($request);
            $mastercardCardManager = $this->get('andres_gotta_sale.mastercard_card_manager');
            $card = $mastercardCardManager->addCard($this->getUser(), $mastercardCard->getCardNumber());
            
            $url = $this->generateUrl('webfactory_order_frontend_order_show', array(
                'id' => $order->getId(),
                'mastercardNewCard' => $card->getId(),
            ));

            return $this->redirect($url);
        }

        foreach ($order->getItems() as $item) {
            $firstItem = $item;
            break;
        }
        $serviceName = $firstItem->getProduct()->getName();
        $orderService = $this->getDoctrine()->getRepository('AndresGottaServiceBundle:Service')->findByName($serviceName);

        return array(
            'order' => $order,
            'add_amex_card_form' => $addAmexCardForm->createView(),
            'add_mastercard_card_form' => $addMastercardCardForm->createView(),
            'amexNewCard' => $request->get('amexNewCard'),
            'mastercardNewCard' => $request->get('mastercardNewCard'),
            'orderService' => $orderService[0],
        );
    }

    /**
     * @Route("/{order}/cancel")
     * @Method({"GET"})
     */
    public function cancelAction(Order $order)
    {
        $flashBag = $this->get('session')->getFlashBag();

        if ($order->getUser() != $this->getUser()) {
            $flashBag->add('error', 'Invalid order');
            $url = $this->generateUrl('webfactory_order_frontend_order_index');

            return $this->redirect($url);
        }

        try {
            $this->get('web_factory_order.order.manager')->cancelOrder($order);
            $flashBag->add('success', 'Order status has been changed');
        } catch (\DomainException $e) {
            $flashBag->add('error', 'Invalid order status');
        }
        
        $url = $this->generateUrl('webfactory_order_frontend_order_show', array('id' => $order->getId()));

        return $this->redirect($url);
    }
    
    /**
     * @Route("/{order}/update/{status}")
     * @Method({"GET"})
     */
    public function updateAction(Order $order, $status)
    {
        $session = $this->get('session');

        if (!OrderStatuses::canChange($order, $status)) {
            $session->getFlashBag()->add('error', 'Invalid order status');
            $url = $this->generateUrl('webfactory_order_frontend_order_show', array('id' => $order->getId()));

            return $this->redirect($url);
        }

        if ($status === OrderStatuses::PAID && !$order->getPayment() && $order->getTotal() == 0) {
            $order->setStatus($status);
            $log = "Pagado por usuario: " . $this->getUser();
            
            $payment = PaymentFactory::createPayment($order, PaymentStatuses::PAID, $log);
            $order->setPayment($payment);
            $this->getDoctrine()->getManager()->persist($payment);
        }

        $this->getDoctrine()->getManager()->flush();
        $session->getFlashBag()->add('success', 'Order status has been changed');

        $url = $this->generateUrl('webfactory_order_frontend_order_show', array('id' => $order->getId()));

        return $this->redirect($url);
    }
    
    /**
     * @Route("/{order}/pay-cash")
     * @param Order $order
     * @return type
     */
    public function setPayCashAction(Order $order)
    {
        if ($order->getStatus() == OrderStatuses::PENDING) {
            $order->setPayCash(true);
            $this->getDoctrine()->getManager()->flush();
        }
        
        $url = $this->generateUrl('webfactory_order_frontend_order_index');
        
        return $this->redirect($url);
    }
}
