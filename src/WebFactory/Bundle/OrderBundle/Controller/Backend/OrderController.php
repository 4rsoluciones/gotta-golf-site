<?php

namespace WebFactory\Bundle\OrderBundle\Controller\Backend;

use APY\DataGridBundle\Grid\Action\RowAction;
use APY\DataGridBundle\Grid\Column\BlankColumn;
use APY\DataGridBundle\Grid\Source\Entity;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Template;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use WebFactory\Bundle\OrderBundle\Entity\Order;
use WebFactory\Bundle\OrderBundle\Model\OrderStatuses;
use WebFactory\Bundle\PaymentBundle\Factory\PaymentFactory;
use WebFactory\Bundle\PaymentBundle\Model\PaymentStatuses;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpFoundation\ResponseHeaderBag;
use Symfony\Component\HttpFoundation\File\UploadedFile;
use WebFactory\Bundle\OrderBundle\Form\UploadAmexReturnType;
use WebFactory\Bundle\OrderBundle\Form\UploadMastercardReturnType;
use Symfony\Component\HttpFoundation\Session\Session;

/**
 * @Route("/order")
 */
class OrderController extends Controller
{

    /**
     * @Route("/", name="webfactory_order_backend_order_index")
     * @Method({"GET","POST"})
     * @Template()
     */
    public function indexAction(Request $request)
    {
        $orderManager = $this->get('web_factory_order.order.manager');
        $mailManager = $this->get('andres_gotta.mail_manager');
        $orderRepository = $this->getDoctrine()->getRepository('WebFactoryOrderBundle:Order');
        $amexRegistryManager = $this->get('andres_gotta_sale.amex_registry_manager');
        $mastercardRegistryManager = $this->get('andres_gotta_sale.mastercard_registry_manager');
        $session = $this->get('session');
        $registries = null;

        if ($registries = $session->get('amex_registries')) $session->clear('amex_registries');
        elseif ($registries = $session->get('mastercard_registries')) $session->clear('mastercard_registries');

        $uploadAmexReturnForm = $this->createForm(new UploadAmexReturnType);
        $uploadAmexReturnForm->handleRequest($request);
        if ($uploadAmexReturnForm->isSubmitted()) {
            $file = $uploadAmexReturnForm->get('amex_return')->getData();
            $fileContent = file_get_contents($file);
            $registries = $amexRegistryManager->processReturn($fileContent);
            if ($registries[0]['registryType'] != 3) {
                $session->getFlashBag()->add('error', 'Imposible procesar el archivo. Verifique que el mismo no se encuentre dañado y que no haya sido modificado.');
                return $this->redirectToRoute('webfactory_order_backend_order_index');
            }
            foreach ($registries as $registry) {
                if ($registry['registryType'] == 2) {
                    if ($registry['rejectMark'] == '00') {
                        $mailManager->notifyPaymentSuccess($registry['order']);
                        if ($registry['order']->getStatus() !== OrderStatuses::PAID) {
                            $orderManager->updateOrder($registry['order'], OrderStatuses::PAID, 'AMEX');
                            $mailManager->notifyServiceActivation($registry['order']);
                        }
                    } else {
                        $orderManager->updateOrder($registry['order'], OrderStatuses::CANCELLED, 'AMEX');
                        $mailManager->notifyPaymentRejected($registry['order']);
                    }
                }
            }
            $session->set('amex_registries', $registries);

            return $this->redirectToRoute('webfactory_order_backend_order_index');
        }

        $uploadMastercardReturnForm = $this->createForm(new UploadMastercardReturnType);
        $uploadMastercardReturnForm->handleRequest($request);
        if ($uploadMastercardReturnForm->isSubmitted()) {
            $file = $uploadMastercardReturnForm->get('mastercard_return')->getData();
            $fileContent = file_get_contents($file);
            $registries = $mastercardRegistryManager->processReturn($fileContent);
            if ($registries[0]['registryType'] != 1) {
                $session->getFlashBag()->add('error', 'Imposible procesar el archivo. Verifique que el mismo no se encuentre dañado y que no haya sido modificado.');
                return $this->redirectToRoute('webfactory_order_backend_order_index');
            }
            foreach ($registries as $registry) {
                if ($registry['registryType'] == 2) {
                    if ($registry['rejectMark'] == '00') {
                        $mailManager->notifyPaymentSuccess($registry['order']);
                        if ($registry['order']->getStatus() !== OrderStatuses::PAID) {
                            $orderManager->updateOrder($registry['order'], OrderStatuses::PAID, 'MASTERCARD');
                            $mailManager->notifyServiceActivation($registry['order']);
                        }
                    } else {
                        $orderManager->updateOrder($registry['order'], OrderStatuses::CANCELLED, 'MASTERCARD');
                        $mailManager->notifyPaymentRejected($registry['order']);
                    }
                }
            }
            $session->set('mastercard_registries', $registries);

            return $this->redirectToRoute('webfactory_order_backend_order_index');
        }

        /* @var $source \APY\DataGridBundle\Grid\Source\Entity */
        $source = new Entity('WebFactoryOrderBundle:Order');
        /* @var $grid \APY\DataGridBundle\Grid\Grid */
        $grid = $this->get('grid');
        $grid->setSource($source);
        $grid->setId('main');
        $grid->setHiddenColumns(array('id'));
        $grid->setDefaultOrder('createdAt', 'desc');

        $showRowAction = new RowAction('Show', 'webfactory_order_backend_order_show', false, '_self', array('class' => 'btn-action glyphicons eye_open btn-info'));
        $grid->addRowAction($showRowAction);
        
        $totalColumn = new BlankColumn(array('id' => 'total', 'title' => 'Total'));
        $grid->addColumn($totalColumn, 4);

        $grid->getColumn('promotionalCode')->manipulateRenderCell(
            function($value, $row, $router) {
                if ($value) {
                    return $value;
                } else {
                    return 'N/A';
                }
            }
        );

        $grid->getColumn('discount')->manipulateRenderCell(
            function($value, $row, $router) {
                if ($value) {
                    return $value.'%';
                } else {
                    return 'N/A';
                }
            }
        );

        return $grid->getGridResponse('WebFactoryOrderBundle:Backend/Order:index.html.twig', array(
            'amexRegistryUrl' => $this->generateUrl('webfactory_order_backend_order_amex_registry'),
            'mastercardRegistryUrl' => $this->generateUrl('webfactory_order_backend_order_mastercard_registry'),
            'uploadAmexReturnForm' => $uploadAmexReturnForm->createView(),
            'uploadMastercardReturnForm' => $uploadMastercardReturnForm->createView(),
            'registries' => $registries,
        ));
    }

    /**
     * @Route("/{id}/show")
     * @Method({"GET"})
     * @Template()
     */
    public function showAction(Order $order)
    {
        return array('order' => $order);
    }

    /**
     * @Route("/{order}/update/{status}")
     * @Method({"GET"})
     */
    public function updateAction(Order $order, $status)
    {
        $session = $this->get('session');

        if (!OrderStatuses::canChange($order, $status)) {
            $session->getFlashBag()->add('error', 'Invalid order status');
            $url = $this->generateUrl('webfactory_order_backend_order_show', array('id' => $order->getId()));

            return $this->redirect($url);
        }

        $this->get('web_factory_order.order.manager')->updateOrder($order, $status, $this->getUser());

        $session->getFlashBag()->add('success', 'Order status has been changed');

        $url = $this->generateUrl('webfactory_order_backend_order_show', array('id' => $order->getId()));

        return $this->redirect($url);
    }

    /**
     * @Route("/amex", name="webfactory_order_backend_order_amex_registry")
     * @Method({"GET"})
     */
    public function amexRegistryAction()
    {
        $amexRegistryManager = $this->get('andres_gotta_sale.amex_registry_manager');
        $fileContent = $amexRegistryManager->dumpRegistries();
        $response = new Response($fileContent);

        $disposition = $response->headers->makeDisposition(
            ResponseHeaderBag::DISPOSITION_ATTACHMENT,
            'DAS_GOTTAG00PRD_SUB.TXT'
        );

        $response->headers->set('Content-Disposition', $disposition);

        return $response;
    }

    /**
     * @Route("/mastercard", name="webfactory_order_backend_order_mastercard_registry")
     * @Method({"GET"})
     */
    public function mastercardRegistryAction()
    {
        $mastercardRegistryManager = $this->get('andres_gotta_sale.mastercard_registry_manager');
        $fileContent = $mastercardRegistryManager->dumpRegistries();
        $response = new Response($fileContent);

        $disposition = $response->headers->makeDisposition(
            ResponseHeaderBag::DISPOSITION_ATTACHMENT,
            'DA168D.txt'
        );

        $response->headers->set('Content-Disposition', $disposition);

        return $response;
    }
}
