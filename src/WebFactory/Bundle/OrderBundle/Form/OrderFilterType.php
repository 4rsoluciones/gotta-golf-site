<?php

namespace WebFactory\Bundle\OrderBundle\Form;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolverInterface;

class OrderFilterType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder->add('status', 'filter_choice', array(
            'required' => false,
            'choices' => \WebFactory\Bundle\OrderBundle\Model\OrderStatuses::getChoices()
        ));
        $builder->add('createdAt', 'filter_date_range', array(
            'left_date_options' => array(
                'required' => false,
                'widget' => 'single_text',
                'format' => 'd/M/y',
                'attr' => array(
                    'data-date-format' => 'dd/mm/yyyy',
                )
            ),
            'right_date_options' => array(
                'required' => false,
                'widget' => 'single_text',
                'format' => 'd/M/y',
                'attr' => array(
                    'data-date-format' => 'dd/mm/yyyy',
                )
            ),
            
        ));
    }

    public function getName()
    {
        return 'order_filter';
    }

    public function setDefaultOptions(OptionsResolverInterface $resolver)
    {
        $resolver->setDefaults(array(
            'csrf_protection'   => false,
            'validation_groups' => array('filtering')
        ));
    }
}