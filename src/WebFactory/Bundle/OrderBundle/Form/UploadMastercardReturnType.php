<?php

namespace WebFactory\Bundle\OrderBundle\Form;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolverInterface;

class UploadMastercardReturnType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('mastercard_return', 'file', array(
                'mapped' => false,
                'attr' => array(
                    'class' => 'hidden',
                    'onchange' => '$("#'.$this->getName().'").submit()',
                ),
            ));
    }

    public function getName()
    {
        return 'upload_mastercard_return';
    }

    public function setDefaultOptions(OptionsResolverInterface $resolver)
    {
        $resolver->setDefaults(array(
            'attr' => array(
                'id' => $this->getName(),
            ),
        ));
    }
}
