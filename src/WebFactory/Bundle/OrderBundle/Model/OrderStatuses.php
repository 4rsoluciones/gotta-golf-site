<?php

namespace WebFactory\Bundle\OrderBundle\Model;

use WebFactory\Bundle\OrderBundle\Entity\Order;

abstract class OrderStatuses
{

    /**
     * Indica que el pedido está listo y cerrado
     * pero que no se ha intentado siquiera abonar
     */
    const PENDING = 'order.status.pending';

    /**
     * Indica que el pedido está pago y listo para ser entregado
     */
    const PAID = 'order.status.paid';

    /**
     * Indica que el pedido está cancelado y debe ser ignorado
     */
    const CANCELLED = 'order.status.cancelled';

    /**
     * Indica que el pedido ha sido pagado pero que se espera su confirmación
     * por parte de la pasarela de pagos
     */
    const WAITING = 'order.status.waiting';

    /**
     * Indica que el pedido ha sido suspendido por base de datos, sin importar otra cuestión
     */
    const SUSPENDED = 'order.status.suspended';

    /**
     * Constructor privado
     */
    private function __construct()
    {
        
    }

    /**
     * 
     * @return array
     */
    public static function getChoices()
    {
        return array(
            static::PENDING => static::PENDING,
            static::PAID => static::PAID,
            static::CANCELLED => static::CANCELLED,
            static::WAITING => static::WAITING,
        );
    }

    /**
     * Determina si la orden puede pasar al nuevo estado
     *
     * @param Order $order
     * @param string $newStatus
     * @return boolean
     */
    public static function canChange(Order $order, $newStatus)
    {
        $oldStatus = $order->getStatus();

        $transitions = array(
            static::PENDING => array(static::WAITING, static::PAID, static::CANCELLED),
            static::WAITING => array(static::PENDING, static::PAID, static::CANCELLED),
            static::PAID => array(),
            static::CANCELLED => array(),
            static::SUSPENDED => array(),
        );
        
        return in_array($newStatus, $transitions[$oldStatus]);
    }

}
