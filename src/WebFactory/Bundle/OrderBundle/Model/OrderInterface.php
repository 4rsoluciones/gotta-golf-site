<?php

namespace WebFactory\Bundle\OrderBundle\Model;

use Symfony\Component\Security\Core\User\UserInterface;
use WebFactory\Bundle\OrderBundle\Entity\Item;

interface OrderInterface
{
    /**
     * Add item
     * 
     * @param Item $item
     * @return Order
     */
    public function addItem(Item $item);
    
    /**
     * Remove item
     * 
     * @param Item $item
     * @return Order
     */
    public function removeItem(Item $item);
    
    /**
     * Get items
     * 
     * @return ArrayCollection
     */
    public function getItems();
    
    /**
     * Has item
     * 
     * @return boolean
     */
    public function hasItem(Item $item);
    
    /**
     * Set payment
     * 
     * @param Payment $payment 
     * @return Order 
     */
    public function setPayment($payment);
    
    /**
     * Get payment
     * 
     * @return Payment 
     */
    public function getPayment();
    
    /**
     * Set status
     * 
     * @param string $status 
     * @return OrderInterface 
     */
    public function setStatus($status);
    
    /**
     * Get status
     * 
     * @return string 
     */
    public function getStatus();
    
    /**
     * @return float
     */
    public function getTotal();
    
    /**
     * @param UserInterface $user
     * @return Order
     */
    public function setUser(UserInterface $user);

    /**
     * Get user
     *
     * @return UserInterface
     */
    public function getUser();
            
}
