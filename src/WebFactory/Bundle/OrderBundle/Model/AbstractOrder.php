<?php

namespace WebFactory\Bundle\OrderBundle\Model;

use Doctrine\Common\Collections\ArrayCollection;
use Symfony\Component\Security\Core\User\UserInterface;
use WebFactory\Bundle\OrderBundle\Entity\Item;
use WebFactory\Bundle\OrderBundle\Entity\Order;
use WebFactory\Bundle\UserBundle\Entity\User;

class AbstractOrder implements OrderInterface
{

    protected $items;
    protected $payment;
    protected $status;
    protected $user;

   
    /**
     * Constructor
     */
    public function __construct()
    {
        $this->items = new ArrayCollection();
    }

    /**
     * Add item
     * 
     * @param \WebFactory\Bundle\OrderBundle\Entity\Item $item
     * @return \WebFactory\Bundle\OrderBundle\Model\AbstractOrder
     */
    public function addItem(Item $item)
    {
        $this->items[] = $item;
        $item->setOrder($this);

        return $this;
    }

    /**
     * Get items
     * 
     * @return ArrayCollection
     */
    public function getItems()
    {
        return $this->items;
    }

    /**
     * Remove item
     * 
     * @param \WebFactory\Bundle\OrderBundle\Entity\Item $item
     * @return \WebFactory\Bundle\OrderBundle\Model\AbstractOrder
     */
    public function removeItem(Item $item)
    {
        $this->items->removeElement($item);

        return $this;
    }

    /**
     * Has item
     * 
     * @param \WebFactory\Bundle\OrderBundle\Entity\Item $item
     * @return type
     */
    public function hasItem(Item $item)
    {
        return $this->items->contains($item);
    }

    /**
     * Set payment
     * 
     * @param Payment $payment
     * @return \WebFactory\Bundle\OrderBundle\Model\AbstractOrder
     */
    public function setPayment($payment = null)
    {
        $this->payment = $payment;

        return $this;
    }

    /**
     * Get payment
     * 
     * @return Payment
     */
    public function getPayment()
    {
        return $this->payment;
    }

    /**
     * Get status
     * 
     * @return string
     */
    public function getStatus()
    {
        return $this->status;
    }

    /**
     * Set status
     * 
     * @param string $status
     * @return \WebFactory\Bundle\OrderBundle\Model\AbstractOrder
     */
    public function setStatus($status)
    {
        $this->status = $status;

        return $this;
    }

    /**
     * Get total
     * 
     * @return float
     */
    public function getTotal()
    {
        $total = 0;
        foreach ($this->items as $item) {
            $total += $item->getTotal();
        }

        return $total;
    }

    /**
     * Set user
     *
     * @param User $user
     * @return Order
     */
    public function setUser(UserInterface $user)
    {
        $this->user = $user;

        return $this;
    }

    /**
     * Get user
     *
     * @return User 
     */
    public function getUser()
    {
        return $this->user;
    }

}
