<?php

namespace WebFactory\Bundle\OrderBundle\Twig\Extension;

use Twig_Extension;
use Twig_Filter_Method;
use WebFactory\Bundle\OrderBundle\Entity\Order;
use WebFactory\Bundle\OrderBundle\Model\OrderStatuses;

/**
 *
 */
class OrderExtension extends Twig_Extension
{

    public function getFilters()
    {
        return array(
            'can_change_order' => new Twig_Filter_Method($this, 'canChangeOrder'),
        );
    }

    public function canChangeOrder(Order $order, $status)
    {
        return OrderStatuses::canChange($order, $status);
    }

    /**
     *
     * @return string
     */
    public function getName()
    {
        return 'order.twig.extension';
    }

}
