<?php

namespace WebFactory\Bundle\OrderBundle\Manager;

use Doctrine\ORM\EntityManagerInterface;
use Doctrine\ORM\EntityRepository;
use DomainException;
use Symfony\Component\Security\Core\User\UserInterface;
use Symfony\Component\Validator\ValidatorInterface;
use WebFactory\Bundle\OrderBundle\Entity\Order;
use WebFactory\Bundle\OrderBundle\Model\OrderStatuses;
use WebFactory\Bundle\OrderBundle\Entity\OrderDebitCard;
use WebFactory\Bundle\PaymentBundle\Factory\PaymentFactory;
use WebFactory\Bundle\PaymentBundle\Model\PaymentStatuses;
use AndresGotta\Bundle\SaleBundle\Manager\AmexRegistryManager;
use AndresGotta\Bundle\SaleBundle\Manager\MastercardRegistryManager;
use Symfony\Component\Filesystem\Filesystem;

class OrderManager
{

    /**
     *
     * @var EntityRepository
     */
    protected $orderRepository;

    /**
     *
     * @var EntityManagerInterface 
     */
    protected $entityManager;

    /**
     *
     * @var ValidatorInterface 
     */
    protected $validator;

    /**
     *
     * @var AmexRegistryManager 
     */
    protected $amexRegistryManager;

    /**
     *
     * @var MastercardRegistryManager 
     */
    protected $mastercardRegistryManager;

    /**
     * 
     * @param EntityRepository $orderRepository
     */
    public function __construct(
        EntityRepository $orderRepository,
        EntityManagerInterface $entityManager,
        ValidatorInterface $validator,
        AmexRegistryManager $amexRegistryManager,
        MastercardRegistryManager $mastercardRegistryManager
    ) {
        $this->orderRepository = $orderRepository;
        $this->entityManager = $entityManager;
        $this->validator = $validator;
        $this->amexRegistryManager = $amexRegistryManager;
        $this->mastercardRegistryManager = $mastercardRegistryManager;
    }

    /**
     * 
     * @param UserInterface $user
     * @param boolean $returnQueryBuilder
     * @return mixed
     */
    public function findOrdersByUser(UserInterface $user, $returnQueryBuilder = false)
    {
        $queryBuilder = $this->orderRepository->createQueryBuilder('o')
                ->where('o.user = :user')
                ->setParameter('user', $user)
        ;

        if ($returnQueryBuilder) {
            return $queryBuilder;
        }

        return $queryBuilder->getQuery()->getResult();
    }

    /**
     * 
     * @param Order $order
     * @throws DomainException
     */
    public function cancelOrder(Order $order)
    {
        $order->setStatus(OrderStatuses::CANCELLED);
        if ($order->getDebitCard()) {
            if ($order->getDebitCard()->getProvider() == 1) {
                $this->amexRegistryManager->dropRegistry($order->getId());
            }
            elseif ($order->getDebitCard()->getProvider() == 2) {
                $this->mastercardRegistryManager->dropRegistry($order->getId());
            }
        }

        if ($this->validator->validate($order)->count() > 0) {
            throw new DomainException('Invalid order status');
        }

        $this->entityManager->persist($order);
        $this->entityManager->flush();
    }
    
    /**
     * 
     * @return ArrayCollection
     */
    public function findPendingOrders()
    {
        $queryBuilder = $this->orderRepository->createQueryBuilder('o')
                ->where('o.status in (:statuses)')
                ->setParameter('statuses', array(OrderStatuses::PENDING, OrderStatuses::WAITING))
        ;

        return $queryBuilder->getQuery()->getResult();
    }

    public function payOrderByDebit(Order $order, $selected_card)
    {
        $services = $this->entityManager->getRepository('AndresGottaServiceBundle:Service')->getAllServices();

        foreach ($services as $service) {
            if ($service->getName() == $order->getItems()[0]->getProduct()) {
                if ($debitDues = $service->getDebitDues()) {
                    $orderTotal = number_format($order->getTotal()/$debitDues, 2);
                }
                else $orderTotal = number_format($order->getTotal(), 2);
                $user = $order->getUser();
                if (!$user->getTrialledServices()->contains($service)) {
                    $trialDays = $service->getTrialDays();
                    $user->addTrialledService($service);
                } else {
                    $trialDays = 0;
                }
            }
        }

        if ($selected_card[0] == 'a') {
            $id = explode('a', $selected_card)[1];
            $card = $this->entityManager->getRepository('AndresGottaSaleBundle:AmexCard')->getCardById($id);
            $card = $this->createCard($order, 1, $card->getCardNumber());
            $this->amexRegistryManager->newRegistry($card->getCardNumber(), $orderTotal, $order->getId(), $debitDues, $trialDays);
        } elseif ($selected_card[0] == 'm') {
            $id = explode('m', $selected_card)[1];
            $card = $this->entityManager->getRepository('AndresGottaSaleBundle:MastercardCard')->getCardById($id);
            $card = $this->createCard($order, 2, $card->getCardNumber());
            $this->mastercardRegistryManager->newRegistry($card->getCardNumber(), $orderTotal, $order->getId(), $debitDues, $trialDays);
        }
        $order->setStatus(OrderStatuses::WAITING);
        $order->setDebitCard($card);
        $this->entityManager->persist($order);
        $this->entityManager->flush();
    }

    private function createCard($order, $provider, $cardNumber)
    {
        $card = new OrderDebitCard();
        $card->setOrder($order);
        $card->setProvider($provider);
        $card->setCardNumber($cardNumber);

        return $card;
    }

    public function updateOrder($order, $status, $user)
    {
        $order->setStatus($status);
        
        if ($status === OrderStatuses::PAID && !$order->getPayment()) {
            
            $log = "Pagado por usuario: " . $user;
            
            $payment = PaymentFactory::createPayment($order, PaymentStatuses::PAID, $log);
            $order->setPayment($payment);
        }

        $this->entityManager->persist($order);
        $this->entityManager->flush();
    }
}
