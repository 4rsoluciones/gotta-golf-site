<?php

namespace WebFactory\Bundle\OrderBundle\ORM\Repository;

use Doctrine\ORM\EntityRepository;
use WebFactory\Bundle\OrderBundle\Model\OrderStatuses;

/**
 * Class OrderRepository
 * @package WebFactory\Bundle\OrderBundle\ORM\Repository
 */
class OrderRepository extends EntityRepository
{
    /**
     * @param $id
     * @return order
     */
    public function getOrderById($id)
    {
        $queryBuilder = $this->createQueryBuilder('o')
            ->where('o.id = :id')->setParameter('id', $id)
        ;

        return $queryBuilder->getQuery()->getOneOrNullResult();
    }
}
