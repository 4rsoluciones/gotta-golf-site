<?php

namespace WebFactory\Bundle\OrderBundle\ORM\Repository;

use Doctrine\ORM\EntityRepository;
use Symfony\Component\Security\Core\User\UserInterface;
use WebFactory\Bundle\OrderBundle\Model\OrderStatuses;

/**
 * Class ItemRepository
 * @package WebFactory\Bundle\OrderBundle\ORM\Repository
 */
class ItemRepository extends EntityRepository
{
    /**
     * @param UserInterface $player
     * @return array
     */
    public function findPlayerActiveServices(UserInterface $player)
    {
        $queryBuilder = $this->createQueryBuilder('i')
            ->join('i.order', 'o')
            ->join('i.product', 's')
            ->where('o.user = :user')->setParameter('user', $player->getId())
            ->andWhere('o.status = :status')->setParameter('status', OrderStatuses::PAID)
        ;

        $queryBuilder->andWhere(
            "DATE_DIFF(CURRENT_DATE(), o.updatedAt) <= s.days"
        );

        return $queryBuilder->getQuery()->getResult();
    }
}