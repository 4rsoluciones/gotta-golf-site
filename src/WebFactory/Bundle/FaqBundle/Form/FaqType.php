<?php

namespace WebFactory\Bundle\FaqBundle\Form;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolverInterface;

class FaqType extends AbstractType
{

    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
                ->add('enabled', NULL, array(
                    'required' => false,
                ))
                ->add('question', NULL, array(
                    'attr' => array(
                        'class' => 'input-xxlarge',
                    ),
                ))
                ->add('answer', 'textarea', array(
                    'required' => false,
                    'attr' => array(
                        'class' => 'input-xxlarge tinymce',
                        'rows' => 10,
                        'data-theme' => 'medium' // simple, advanced, bbcode
                    )
                ))
                ->add('answer', 'ckeditor', array(
                    'config' => array(
                        'toolbar' => array(
                            array('name' => 'document', 'items' => array('Source')),
                            array('name' => 'clipboard', 'items' => array('Cut', 'Copy', 'Paste', 'PasteText', 'PasteFromWord', '-', 'Undo', 'Redo')),
                            array('name' => 'editing', 'items' => array('Find', 'Replace', '-', 'SelectAll', '-', 'Scayt')),
                            array('name' => 'insert', 'items' => array('Image', 'Flash', 'Table', 'HorizontalRule', 'Smiley', 'SpecialChar', 'PageBreak', 'Iframe')),
                            '/',
                            array('name' => 'styles', 'items' => array('Styles', 'Format')),
                            array('name' => 'basicstyles', 'items' => array('Bold', 'Italic', 'Strike', '-', 'RemoveFormat')),
                            array('name' => 'paragraph', 'items' => array('NumberedList', 'BulletedList', '-', 'Outdent', 'Indent', '-', 'Blockquote')),
                            array('name' => 'links', 'items' => array('Link', 'Unlink', 'Anchor')),
                            array('name' => 'tools', 'items' => array('Maximize', '-', 'About'))
                        ),
                        'filebrowserBrowseRoute' => 'elfinder',
                        'height' => '450px',
                    ),
                    'render_optional_text' => false,
                ))
        ;
    }

    public function setDefaultOptions(OptionsResolverInterface $resolver)
    {
        $resolver->setDefaults(array(
            'data_class' => 'WebFactory\Bundle\FaqBundle\Entity\Faq'
        ));
    }

    public function getName()
    {
        return 'webfactory_faqbundle_faqtype';
    }

}
