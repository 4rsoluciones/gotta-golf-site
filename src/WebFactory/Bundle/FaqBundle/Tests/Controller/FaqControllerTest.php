<?php

namespace WebFactory\FaqBundle\Tests\Controller;

use Symfony\Bundle\FrameworkBundle\Test\WebTestCase;

class FaqControllerTest extends WebTestCase
{

    public function testCompleteScenario()
    {
        $client = static::createClient();

        $crawler = $client->request('GET', '/faq');

        $this->assertTrue(200 === $client->getResponse()->getStatusCode());
        $this->assertTrue($crawler->filter('h3.titleFaq')->count() > 0);
    }

}