<?php

namespace WebFactory\Bundle\FaqBundle\Controller\Frontend;

use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Template;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;

/**
 * Faq Frontend controller.
 *
 * @Route("/")
 */
class FaqController extends Controller
{

    /**
     * Lists all Faq entities.
     *
     * @Route("/faq")
     * @Template()
     */
    public function indexAction()
    {
        $em = $this->getDoctrine()->getManager();

        // Buscamos Faqs habilitados
        $criteria = array('enabled' => true);
        $sort = array('sort' => 'ASC');
        $results = $em->getRepository('WebFactoryFaqBundle:Faq')->findBy($criteria, $sort);

        return array(
            'results' => $results,
        );
    }

}
