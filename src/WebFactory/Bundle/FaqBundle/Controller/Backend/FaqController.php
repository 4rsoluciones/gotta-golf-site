<?php

namespace WebFactory\Bundle\FaqBundle\Controller\Backend;

use Symfony\Component\HttpFoundation\Request;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Template;
use WebFactory\Bundle\FaqBundle\Entity\Faq;
use WebFactory\Bundle\FaqBundle\Form\FaqType;
use APY\DataGridBundle\Grid\Source\Entity;
use APY\DataGridBundle\Grid\Action\RowAction;
use APY\DataGridBundle\Grid\Action\MassAction;

/**
 * Faq controller.
 *
 * @Route("/faq")
 */
class FaqController extends Controller
{
    /**
     * Lists all Faq entities.
     *
     * @Route("/")
     * @Template()
     */
    public function indexAction()
    {
        $source = new Entity('WebFactoryFaqBundle:Faq');

        /* @var $grid \APY\DataGridBundle\Grid\Grid */
        $grid = $this->get('grid');
        $grid->setHiddenColumns(array('id'));
        $grid->setSource($source);
        $grid->setDefaultOrder('sort', 'asc');

        $grid->addRowAction(new RowAction('Show', 'webfactory_faq_backend_faq_show', false, '_self', array('class' => 'btn-action glyphicons eye_open btn-info')));
        $grid->addRowAction(new RowAction('Edit', 'webfactory_faq_backend_faq_edit', false, '_self', array('class' => 'btn-action glyphicons pencil btn-success')));
        $grid->addRowAction(new RowAction('Delete', 'webfactory_faq_backend_faq_delete', true, '_self', array('class' => 'btn-action glyphicons remove_2 btn-danger')));

        $em = $this->getDoctrine()->getManager();
        $translator = $this->get('translator');

        $updateEnabled = function($ids, $selectAll, $session, $parameters) use ($em, $translator) {
            if ($selectAll) {
                $items = $em->getRepository('WebFactoryFaqBundle:Faq')->findAll();
                foreach ($items as $item) {
                    $item->setEnabled($parameters['enabled']);
                }
                $session->getFlashBag()->add('success', $translator->trans('Updated!'));
            } else {
                if ($ids) {
                    foreach ($ids as $id) {
                        $item = $em->getRepository('WebFactoryFaqBundle:Faq')->find($id);
                        $item->setEnabled($parameters['enabled']);
                    }
                    $session->getFlashBag()->add('success', $translator->trans('Updated!'));
                } else {
                    $session->getFlashBag()->add('error', $translator->trans('No items selecteds'));
                }
            }
            $em->flush();
        };

        $grid->addMassAction(new MassAction('Enable', $updateEnabled, false, array('enabled' => true)));
        $grid->addMassAction(new MassAction('Disable', $updateEnabled, false, array('enabled' => false)));

        return $grid->getGridResponse('WebFactoryFaqBundle:Backend/Faq:index.html.twig');
    }

    /**
     * Finds and displays a Faq entity.
     *
     * @Route("/{id}/show")
     * @Template()
     */
    public function showAction($id)
    {
        $em = $this->getDoctrine()->getManager();

        $entity = $em->getRepository('WebFactoryFaqBundle:Faq')->find($id);

        if (!$entity) {
            throw $this->createNotFoundException('Unable to find Faq entity.');
        }

        $deleteForm = $this->createDeleteForm($id);

        return array(
            'entity' => $entity,
            'delete_form' => $deleteForm->createView(),
        );
    }

    /**
     * Displays a form to create a new Faq entity.
     *
     * @Route("/new")
     * @Template()
     */
    public function newAction()
    {
        $entity = new Faq();
        $form = $this->createForm(new FaqType(), $entity);

        return array(
            'entity' => $entity,
            'form' => $form->createView(),
        );
    }

    /**
     * Creates a new Faq entity.
     *
     * @Route("/create")
     * @Method("POST")
     * @Template("WebFactoryFaqBundle:Backend/Faq:new.html.twig")
     */
    public function createAction(Request $request)
    {
        $entity = new Faq();
        $form = $this->createForm(new FaqType(), $entity);
        $form->bind($request);

        if ($form->isValid()) {
            $em = $this->getDoctrine()->getManager();
            $em->persist($entity);
            $em->flush();

            $this->get('session')->getFlashBag()->add('success', $this->get('translator')->trans('Faq created!'));

            return $this->redirect($this->generateUrl('webfactory_faq_backend_faq_show', array('id' => $entity->getId())));
        }

        return array(
            'entity' => $entity,
            'form' => $form->createView(),
        );
    }

    /**
     * Displays a form to edit an existing Faq entity.
     *
     * @Route("/{id}/edit")
     * @Template()
     */
    public function editAction($id)
    {
        $em = $this->getDoctrine()->getManager();

        $entity = $em->getRepository('WebFactoryFaqBundle:Faq')->find($id);

        if (!$entity) {
            throw $this->createNotFoundException('Unable to find Faq entity.');
        }

        $editForm = $this->createForm(new FaqType(), $entity);
        $deleteForm = $this->createDeleteForm($id);

        return array(
            'entity' => $entity,
            'edit_form' => $editForm->createView(),
            'delete_form' => $deleteForm->createView(),
        );
    }

    /**
     * Edits an existing Faq entity.
     *
     * @Route("/{id}/update")
     * @Method("POST")
     * @Template("WebFactoryFaqBundle:Backend/Faq:edit.html.twig")
     */
    public function updateAction(Request $request, $id)
    {
        $em = $this->getDoctrine()->getManager();

        $entity = $em->getRepository('WebFactoryFaqBundle:Faq')->find($id);

        if (!$entity) {
            throw $this->createNotFoundException('Unable to find Faq entity.');
        }

        $deleteForm = $this->createDeleteForm($id);
        $editForm = $this->createForm(new FaqType(), $entity);
        $editForm->bind($request);

        if ($editForm->isValid()) {
            $em->persist($entity);
            $em->flush();

            $this->get('session')->getFlashBag()->add('success', $this->get('translator')->trans('Faq updated!'));

            return $this->redirect($this->generateUrl('webfactory_faq_backend_faq_edit', array('id' => $id)));
        }

        return array(
            'entity' => $entity,
            'edit_form' => $editForm->createView(),
            'delete_form' => $deleteForm->createView(),
        );
    }

    /**
     * Deletes a Faq entity.
     *
     * @Route("/{id}/delete")
     * @Method("POST|GET")
     */
    public function deleteAction(Request $request, $id)
    {
        $form = $this->createDeleteForm($id);
        $form->bind($request);

        if ($form->isValid()) {
            $em = $this->getDoctrine()->getManager();
            $entity = $em->getRepository('WebFactoryFaqBundle:Faq')->find($id);

            if (!$entity) {
                throw $this->createNotFoundException('Unable to find Faq entity.');
            }

            $em->remove($entity);
            $em->flush();

            $this->get('session')->getFlashBag()->add('success', $this->get('translator')->trans('Faq deleted!'));
        }

        return $this->redirect($this->generateUrl('webfactory_faq_backend_faq_index'));
    }

    /**
     * Ordena las Preguntas Frecuentes habilitadas
     *
     * @Route("/arrange")
     * @Template()
     */
    public function arrangeAction()
    {
        $em = $this->getDoctrine()->getManager();

        $results = $em->getRepository('WebFactoryFaqBundle:Faq')
                ->findBy(array('enabled' => 1), array('sort' => 'ASC'));

        // Intenta guardar el formulario
        if ($this->getRequest()->isMethod('POST')) {
            try {

                // Instancia del Validador
                $validator = $this->get('validator');

                // Validamos que envíe elementos a ordenar
                $results = $this->getRequest()->get('items');
                if (!is_array($results)) {
                    throw new \Exception('You must send items to order.');
                }

                // Recorremos y ordenamos
                $position = 1;
                foreach ($results as $k => $item_id) {

                    /* @var $item Faq */
                    $item = $em->getRepository('WebFactoryFaqBundle:Faq')
                            ->findOneBy(array(
                        'id' => $item_id,
                    ));

                    if (!$item) {
                        throw new \Exception('Unable to find the Item specified.');
                    }

                    $item->setSort($position);

                    $errors = $validator->validate($item);

                    if (count($errors) > 0) {
                        throw new \Exception($errors[0]->getMessage());
                    }

                    $em->persist($item);

                    $position++;
                }

                $em->flush();

                $this->get('session')->getFlashBag()->add('success', 'faq.messages.arranged');
            } catch (\Exception $exc) {
                $this->get('session')->getFlashBag()->add('error', $exc->getMessage());
            }

            return $this->redirect($this->generateUrl('webfactory_faq_backend_faq_arrange'));
        }

        return array(
            'results' => $results,
        );
    }

    /**
     * 
     * @param type $id
     * @return type
     */
    private function createDeleteForm($id)
    {
        return $this->createFormBuilder(array('id' => $id), array('csrf_protection' => false))
                        ->add('id', 'hidden')
                        ->getForm()
        ;
    }

}
