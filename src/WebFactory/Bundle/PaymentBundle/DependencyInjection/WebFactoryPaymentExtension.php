<?php

namespace WebFactory\Bundle\PaymentBundle\DependencyInjection;

use Symfony\Component\Config\FileLocator;
use Symfony\Component\DependencyInjection\ContainerBuilder;
use Symfony\Component\DependencyInjection\Loader;
use Symfony\Component\HttpKernel\DependencyInjection\Extension;

/**
 * This is the class that loads and manages your bundle configuration
 *
 * To learn more see {@link http://symfony.com/doc/current/cookbook/bundles/extension.html}
 */
class WebFactoryPaymentExtension extends Extension
{

    /**
     * {@inheritDoc}
     */
    public function load(array $configs, ContainerBuilder $container)
    {
        $configuration = new Configuration();
        $config = $this->processConfiguration($configuration, $configs);

        $loader = new Loader\XmlFileLoader($container, new FileLocator(__DIR__ . '/../Resources/config'));
        $loader->load('services.xml');

        $selectedGateway = $config['gateway'];
        $container->getDefinition('web_factory_payment.abstract_gateway')->replaceArgument(0, $config['config'][$selectedGateway]);

        if ($config['use_default_listener']) {
            $loader->load('listener.xml');
        }

        $serviceGateway = $config['gateway_service'];

        if (empty($serviceGateway)) {
            if ($selectedGateway === 'RccPagos') {
                $serviceGateway = $this->loadRccPagos($loader, $container, $config['config']['RccPagos']);
            }
            if ($selectedGateway === 'MercadoPago') {
                $serviceGateway = $this->loadMercadoPago($loader, $container, $config['config']['MercadoPago']);
            }
        }

        $container->setAlias('web_factory_payment.gateway', $serviceGateway);
    }

    private function loadMercadoPago(Loader\XmlFileLoader $loader, ContainerBuilder $container, array $config)
    {
        $container->setParameter('web_factory_payment.config.id', $config['id']);
        $container->setParameter('web_factory_payment.config.secret', $config['secret']);

        $loader->load('mercadopago.xml');
        $serviceGateway = 'web_factory_payment.mercadopago.gateway';

        return $serviceGateway;
    }

    private function loadRccPagos(Loader\XmlFileLoader $loader, ContainerBuilder $container, array $config)
    {
        $loader->load('rccpagos.xml');
        $serviceGateway = 'web_factory_payment.rccpagos.gateway';

        return $serviceGateway;
    }

}
