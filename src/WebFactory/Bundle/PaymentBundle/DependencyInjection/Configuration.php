<?php

namespace WebFactory\Bundle\PaymentBundle\DependencyInjection;

use Symfony\Component\Config\Definition\Builder\TreeBuilder;
use Symfony\Component\Config\Definition\ConfigurationInterface;

/**
 * This is the class that validates and merges configuration from your app/config files
 *
 * To learn more see {@link http://symfony.com/doc/current/cookbook/bundles/extension.html#cookbook-bundles-extension-config-class}
 */
class Configuration implements ConfigurationInterface
{

    /**
     * {@inheritDoc}
     */
    public function getConfigTreeBuilder()
    {
        $treeBuilder = new TreeBuilder();
        $rootNode = $treeBuilder->root('web_factory_payment');
        
        $implementdGateways = array('MercadoPago', 'RccPagos');

        $rootNode
                ->children()
                    ->scalarNode('gateway')
                        ->isRequired()
                        ->cannotBeEmpty()
                        ->validate()
                            ->ifNotInArray($implementdGateways)
                            ->thenInvalid('Invalid gateway "%s"')
                        ->end()
                    ->end()
                    ->scalarNode('gateway_service')
                        ->defaultNull()
                    ->end()
                    ->booleanNode('use_default_listener')
                        ->defaultTrue()
                    ->end()
                    ->arrayNode('config')
                        ->isRequired()
                        ->requiresAtLeastOneElement()
                        ->useAttributeAsKey('name')
                        ->prototype('array')
                            ->children()
                                ->enumNode('name')->values($implementdGateways)->end()
                                ->scalarNode('id')->end()
                                ->scalarNode('secret')->end()
                                ->booleanNode('sandbox_mode')->defaultTrue()->end()
                                ->arrayNode('extra')
                                    ->useAttributeAsKey('name')
                                    ->prototype('scalar')->end()
                                ->end()
                            ->end()
                        ->end()
                    ->end()
                ->end();

        return $treeBuilder;
    }

}
