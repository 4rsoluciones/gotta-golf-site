<?php

namespace WebFactory\Bundle\PaymentBundle\Form;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolverInterface;

class DuesType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('quantity')
            ->add('coefficient')
            ->add('creditCard', 'entity', array(
                'class' => 'WebFactoryPaymentBundle:CreditCard',
                'property' => 'name'
            ))
        ;
    }

    public function setDefaultOptions(OptionsResolverInterface $resolver)
    {
        $resolver->setDefaults(array(
            'data_class' => 'WebFactory\Bundle\PaymentBundle\Entity\Dues',
        ));
    }

    public function getName()
    {
        return 'dues';
    }
}