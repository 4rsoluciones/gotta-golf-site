<?php

namespace WebFactory\Bundle\PaymentBundle\DataFixtures\ORM;

use AndresGotta\Bundle\SaleBundle\Entity\CustomItem;
use Doctrine\Common\DataFixtures\AbstractFixture;
use Doctrine\Common\DataFixtures\OrderedFixtureInterface;
use Doctrine\Common\Persistence\ObjectManager;
use Symfony\Component\DependencyInjection\ContainerAwareInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;
use WebFactory\Bundle\OrderBundle\Entity\Item;
use WebFactory\Bundle\OrderBundle\Entity\Order;
use WebFactory\Bundle\OrderBundle\Model\OrderStatuses;
use WebFactory\Bundle\PaymentBundle\Entity\CreditCard;
use WebFactory\Bundle\PaymentBundle\Entity\Dues;
use WebFactory\Bundle\PaymentBundle\Entity\Payment;
use WebFactory\Bundle\PaymentBundle\Model\PaymentStatuses;
use WebFactory\Bundle\ProductBundle\Model\ProductInterface;
use WebFactory\Bundle\UserBundle\Entity\User;

class LoadPayments extends AbstractFixture implements OrderedFixtureInterface, ContainerAwareInterface
{
    
    /**
     * @var ContainerInterface
     */
    private $container;
    
    /**
     * {@inheritDoc}
     */
    public function setContainer(ContainerInterface $container = null)
    {
        $this->container = $container;
    }

    /**
     * {@inheritDoc}
     */
    public function getOrder()
    {
        return 6;
    }

    /**
     * {@inheritDoc}
     */
    public function load(ObjectManager $manager)
    {
        $dues = [3, 6, 12];
        $creditCards = ['Visa' => 2, 'Mastercard' => 3, 'Cabal' => 1, 'NATIVA' => 5, 'Argencard' => 8];

        foreach ($creditCards as $creditCard => $id) {
            $card = new CreditCard();
            $card->setName($creditCard);
            $card->setIdentificationCode($id);

            foreach ($dues as $quantity) {
                $entity = new Dues();
                $entity->setQuantity($quantity);
                $entity->setCoefficient(mt_rand(0, 10));
                $entity->setCreditCard($card);

                $card->addDue($entity);
            }
            $manager->persist($card);
        }

        $manager->flush();
    }

}