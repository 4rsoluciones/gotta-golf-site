<?php

namespace WebFactory\Bundle\PaymentBundle\Model;

use WebFactory\Bundle\OrderBundle\Entity\Order;

interface GatewayFormGeneratorInterface
{

    /**
     * Construye el formulario/enlace a mostrar al usuario
     */
    public function buildForm($extra = array());

    /**
     * Procesa la solicitud del usuario basado en la orden
     */
    public function processForm(Order $order);
}
