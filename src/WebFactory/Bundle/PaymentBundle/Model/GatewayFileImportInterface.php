<?php

namespace WebFactory\Bundle\PaymentBundle\Model;

interface GatewayFileImportInterface
{
    /**
     * Realiza la importación
     */
    public function import($file);

    /**
     * Revisa que el archivo sea válido
     */
    public function validateFile($file);
}
