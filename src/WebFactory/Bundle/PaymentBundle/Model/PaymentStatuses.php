<?php

namespace WebFactory\Bundle\PaymentBundle\Model;

abstract class PaymentStatuses
{

    const PAID = 'payment.status.paid';
    const CANCELLED = 'payment.status.cancelled';
    const PENDING = 'payment.status.pending';
    

    public static function getChoices()
    {
        return array(
            static::PAID => static::PAID,
            static::CANCELLED => static::CANCELLED,
            static::PENDING => static::PENDING,
        );
    }

}
