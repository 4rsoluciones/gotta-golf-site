<?php

namespace WebFactory\Bundle\PaymentBundle\Model;

use Doctrine\Common\Collections\ArrayCollection;

interface GatewayApiInterface
{

    public function search($criteria);

    public function updateStatuses(ArrayCollection $orders);
}
