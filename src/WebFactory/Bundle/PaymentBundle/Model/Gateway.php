<?php

namespace WebFactory\Bundle\PaymentBundle\Model;

use Symfony\Bundle\TwigBundle\TwigEngine;
use Symfony\Component\EventDispatcher\EventDispatcherInterface;
use WebFactory\Bundle\OrderBundle\Manager\OrderManager;

abstract class Gateway
{

    /**
     *
     * @var array
     */
    protected $config;

    /**
     *
     * @var OrderManager
     */
    protected $orderManager;

    /**
     *
     * @var EventDispatcherInterface
     */
    protected $dispatcher;

    /**
     *
     * @var TwigEngine
     */
    protected $templating;

    /**
     * 
     * @param array $config
     * @param OrderManager $orderManager
     * @param TwigEngine $templating
     */
    public function __construct(array $config, OrderManager $orderManager, TwigEngine $templating)
    {
        $this->config = $config;
        $this->orderManager = $orderManager;
        $this->templating = $templating;
    }
    
    /**
     * 
     * @param EventDispatcherInterface $dispatcher
     * @return Gateway
     */
    public function setDispatcher(EventDispatcherInterface $dispatcher)
    {
        $this->dispatcher = $dispatcher;
        return $this;
    }

    /**
     * 
     * @return array
     */
    public function getConfig()
    {
        return $this->config;
    }

    /**
     * 
     * @param string $name
     * @return string
     */
    public function getExtra($name)
    {
        return isset($this->config['extra'][$name]) ? $this->config['extra'][$name] : null;
    }

    /**
     * 
     * @return boolean
     */
    public function isSandboxMode()
    {
        return (boolean) $this->config['sandbox_mode'];
    }

    /**
     * 
     * @return boolean
     */
    public function hasImportFeatures()
    {
        return $this instanceof GatewayFileImportInterface;
    }

    /**
     * @return string
     */
    public abstract function getName();
}
