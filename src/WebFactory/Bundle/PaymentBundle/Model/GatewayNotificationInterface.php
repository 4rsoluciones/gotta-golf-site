<?php

namespace WebFactory\Bundle\PaymentBundle\Model;

use Symfony\Component\HttpFoundation\Request;

interface GatewayNotificationInterface
{

    const SUCCESS_NOTIFICATION = 'success';
    const FAILURE_NOTIFICATION = 'failure';
    const PENDING_NOTIFICATION = 'pending';
    const UNDEFINED_NOTIFICATION = 'undefined';
    const INFORMATION_NOTIFICATION = 'information';

    /**
     * Procesa la notificación de la pasarela
     */
    public function processNotification($type, Request $notification);
}
