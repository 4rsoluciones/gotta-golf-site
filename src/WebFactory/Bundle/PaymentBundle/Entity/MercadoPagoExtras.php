<?php

namespace WebFactory\Bundle\PaymentBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use Gedmo\Mapping\Annotation as Gedmo;
use Symfony\Component\Validator\Constraints as Assert;
use WebFactory\Bundle\OrderBundle\Entity\Order;
use WebFactory\Bundle\OrderBundle\Model\OrderStatuses;

/**
 * RccPagosExtras
 *
 * @ORM\Table(name="mercado_pago_extras")
 * @ORM\Entity
 */
class MercadoPagoExtras
{

    const STATUS_PENDING = 'pending';
    const STATUS_SUCCESS = 'success';
    const STATUS_FAILURE = 'failure';

    /**
     * @var integer
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    protected $id;

    /**
     * @var string 
     * 
     * @ORM\Column(name="code", type="string", length=255)
     * @Assert\NotBlank
     */
    protected $code;

    /**
     * @var string
     *
     * @ORM\Column(name="status", type="string", length=255, nullable=true)
     */
    protected $status;

    /**
     * @var string
     *
     * @ORM\Column(name="error", type="string", length=255, nullable=true)
     */
    protected $error;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="created_at", type="datetime")
     * @Gedmo\Timestampable(on="create")
     */
    protected $createdAt;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="updated_at", type="datetime", nullable=true)
     * @Gedmo\Timestampable(on="update")
     */
    protected $updatedAt;

    /**
     * @var Order
     *  
     * @ORM\OneToOne(targetEntity="WebFactory\Bundle\OrderBundle\Entity\Order", cascade={"persist"})
     * @ORM\JoinColumn(name="order_id", referencedColumnName="id")
     * @Assert\NotBlank
     */
    protected $order;

    /**
     * Constructor
     */
    public function __construct($code = null, Order $order = null)
    {
        $this->code = $code;
        $this->order = $order;
        $this->status = static::STATUS_PENDING;
    }

    /**
     * @return integer
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * 
     * @return string
     */
    public function getCode()
    {
        return $this->code;
    }

    /**
     * 
     * @param string $code
     * @return \WebFactory\Bundle\PaymentBundle\Entity\MercadoPagoExtras
     */
    public function setCode($code)
    {
        $this->code = $code;
        return $this;
    }

    /**
     * @return string
     */
    public function getStatus()
    {
        return $this->status;
    }

    /**
     * @return \DateTime
     */
    public function getCreatedAt()
    {
        return $this->createdAt;
    }

    /**
     * @return \DateTime
     */
    public function getUpdatedAt()
    {
        return $this->updatedAt;
    }

    /**
     * @return Order
     */
    public function getOrder()
    {
        return $this->order;
    }

    /**
     * @param string $status
     */
    public function setStatus($status)
    {
        $this->status = $status;
    }

    /**
     * @param \DateTime $createdAt
     */
    public function setCreatedAt(\DateTime $createdAt)
    {
        $this->createdAt = $createdAt;
    }

    /**
     * @param \DateTime $updatedAt
     */
    public function setUpdatedAt(\DateTime $updatedAt)
    {
        $this->updatedAt = $updatedAt;
    }

    /**
     * @param \WebFactory\Bundle\OrderBundle\Entity\Order $order
     */
    public function setOrder(Order $order)
    {
        $this->order = $order;
    }

    /**
     * @return string
     */
    public function getError()
    {
        return $this->error;
    }

    /**
     * @param string $error
     */
    public function setError($error)
    {
        $this->error = $error;
    }

    /**
     * Registra el exito, aplica el estado SUCCESS
     */
    public function registerSuccess()
    {
        $this->setStatus(static::STATUS_SUCCESS);
        $this->order->setStatus(OrderStatuses::WAITING);
    }

    /**
     * Registra el fallo, solicita el mensaje de error y aplica el estado FAILURE
     *
     * @param string $failure
     */
    public function registerFailure($failure)
    {
        $this->setStatus(static::STATUS_FAILURE);
        $this->setError($failure);
    }

}
