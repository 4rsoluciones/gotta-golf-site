<?php

namespace WebFactory\Bundle\PaymentBundle\Entity\Repository;

use Doctrine\ORM\EntityRepository;

class DuesRepository extends EntityRepository
{
    /**
     * Busca las cuotas de una tarjeta de crédito dada
     *
     * @param $identificationCode
     * @return array
     */
    public function findDuesByCreditCard($identificationCode)
    {
        $qb = $this->createQueryBuilder('d')
            ->select('d.quantity, d.coefficient')
            ->innerJoin('d.creditCard', 'Card')
            ->where('Card.identificationCode = :code')->setParameter('code', $identificationCode)
            ->orderBy('d.quantity', 'ASC')
        ;

        return $qb->getQuery()->getResult();
    }

    /**
     * Busca las tarjetas de crédito que tengan cuotas asignadas
     * 
     * @return array
     */
    public function findCreditCardsWithDues()
    {
        $qb = $this->createQueryBuilder('Dues')
            ->select('Card.name, Card.identificationCode')
            ->innerJoin('Dues.creditCard', 'Card')
            ->groupBy('Card.name')
            ->orderBy('Card.name', 'ASC')
        ;

        return $qb->getQuery()->getResult();
    }
}