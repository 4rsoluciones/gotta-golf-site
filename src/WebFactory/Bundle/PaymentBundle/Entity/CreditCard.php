<?php

namespace WebFactory\Bundle\PaymentBundle\Entity;

use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\ORM\Mapping as ORM;
use APY\DataGridBundle\Grid\Mapping as Grid;

/**
 * Class CreditCard
 * @package WebFactory\Bundle\PaymentBundle
 * @ORM\Table(name="credit_cards")
 * @ORM\Entity
 *
 */
class CreditCard
{
    /**
     * @var integer
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     * @Grid\Column(filterable=false)
     */
    protected $id;

    /**
     * @var string
     * @ORM\Column(type="string", length=255)
     * @Grid\Column(field="name", title="Credit Card", type="text")
     */
    protected $name;

    /**
     * @var integer
     * @ORM\Column(name="identification_code", type="integer", nullable=true)
     */
    protected $identificationCode;

    /**
     * @var ArrayCollection
     * @ORM\OneToMany(targetEntity="Dues", mappedBy="creditCard", cascade={"all"})
     * @Grid\Column(field="dues.quantity", title="Dues")
     * @Grid\Column(field="dues.coefficient", title="Coefficient")
     */
    protected $dues;

    public function __construct()
    {
        $this->dues = new ArrayCollection();
    }

    /**
     * Get id
     *
     * @return integer 
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set name
     *
     * @param string $name
     * @return CreditCard
     */
    public function setName($name)
    {
        $this->name = $name;

        return $this;
    }

    /**
     * Get name
     *
     * @return string 
     */
    public function getName()
    {
        return $this->name;
    }

    /**
     * Add dues
     *
     * @param \WebFactory\Bundle\PaymentBundle\Entity\Dues $dues
     * @return CreditCard
     */
    public function addDue(\WebFactory\Bundle\PaymentBundle\Entity\Dues $dues)
    {
        $this->dues[] = $dues;

        return $this;
    }

    /**
     * Remove dues
     *
     * @param \WebFactory\Bundle\PaymentBundle\Entity\Dues $dues
     */
    public function removeDue(\WebFactory\Bundle\PaymentBundle\Entity\Dues $dues)
    {
        $this->dues->removeElement($dues);
    }

    /**
     * Get dues
     *
     * @return \Doctrine\Common\Collections\Collection 
     */
    public function getDues()
    {
        return $this->dues;
    }

    /**
     * @return int
     */
    public function getIdentificationCode()
    {
        return $this->identificationCode;
    }

    /**
     * @param int $identificationCode
     */
    public function setIdentificationCode($identificationCode)
    {
        $this->identificationCode = $identificationCode;
    }


}
