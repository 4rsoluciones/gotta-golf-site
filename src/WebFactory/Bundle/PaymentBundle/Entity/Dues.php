<?php

namespace WebFactory\Bundle\PaymentBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use APY\DataGridBundle\Grid\Mapping as Grid;

/**
 * Class Dues
 * @package WebFactory\Bundle\PaymentBundle
 * @ORM\Table(name="dues")
 * @ORM\Entity(repositoryClass="WebFactory\Bundle\PaymentBundle\Entity\Repository\DuesRepository")
 * @GRID\Source(columns="id, creditCard.name, quantity, coefficient")
 */
class Dues
{
    /**
     * @var integer
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    protected $id;

    /**
     * @var integer
     * @ORM\Column(name="quantity", type="integer")
     * @Grid\Column(field="quantity", title="Quantity")
     */
    protected $quantity;

    /**
     * @var float
     *
     * @ORM\Column(name="coefficient", type="float")
     *
     * @Grid\Column(field="coefficient", title="Coefficient")
     */
    protected $coefficient;

    /**
     * @var CreditCard
     * @ORM\ManyToOne(targetEntity="CreditCard", inversedBy="dues")
     * @ORM\JoinColumn(name="credit_card_id", referencedColumnName="id")
     * @Grid\Column(field="creditCard.name", title="Credit Card")
     */
    protected $creditCard;

    /**
     * Get id
     *
     * @return integer 
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set quantity
     *
     * @param integer $quantity
     * @return Dues
     */
    public function setQuantity($quantity)
    {
        $this->quantity = $quantity;

        return $this;
    }

    /**
     * Get quantity
     *
     * @return integer 
     */
    public function getQuantity()
    {
        return $this->quantity;
    }

    /**
     * Set coefficient
     *
     * @param float $coefficient
     * @return Dues
     */
    public function setCoefficient($coefficient)
    {
        $this->coefficient = $coefficient;

        return $this;
    }

    /**
     * Get coefficient
     *
     * @return float 
     */
    public function getCoefficient()
    {
        return $this->coefficient;
    }

    /**
     * Set creditCard
     *
     * @param \WebFactory\Bundle\PaymentBundle\Entity\CreditCard $creditCard
     * @return Dues
     */
    public function setCreditCard(\WebFactory\Bundle\PaymentBundle\Entity\CreditCard $creditCard = null)
    {
        $this->creditCard = $creditCard;

        return $this;
    }

    /**
     * Get creditCard
     *
     * @return \WebFactory\Bundle\PaymentBundle\Entity\CreditCard 
     */
    public function getCreditCard()
    {
        return $this->creditCard;
    }
}
