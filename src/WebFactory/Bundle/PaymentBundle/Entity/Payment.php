<?php

namespace WebFactory\Bundle\PaymentBundle\Entity;

use APY\DataGridBundle\Grid\Mapping as Grid;
use Doctrine\ORM\Mapping as ORM;
use Gedmo\Mapping\Annotation as Gedmo;
use Symfony\Bridge\Doctrine\Validator\Constraints\UniqueEntity;
use Symfony\Component\Validator\Constraints as Assert;
use Symfony\Component\Validator\ExecutionContextInterface;
use WebFactory\Bundle\OrderBundle\Entity\Order;
use WebFactory\Bundle\OrderBundle\Model\OrderInterface;
use WebFactory\Bundle\PaymentBundle\Model\PaymentStatuses;

/**
 * Payment
 *
 * @ORM\Table(name="sale_payments")
 * @ORM\Entity
 * @UniqueEntity("order")
 * @GRID\Source(columns="id, order.id, status, createdAt, updatedAt, order.user.email")
 */
class Payment
{

    /**
     * @var integer
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     * @Grid\Column(filterable=false)
     */
    protected $id;

    /**
     * @var string
     *
     * @ORM\Column(name="status", type="string", length=255)
     * @Assert\NotBlank
     * @Grid\Column(title="Status", filter="select", selectFrom="values", values={
     *      "payment.status.paid"="payment.status.paid",
     *      "payment.status.cancelled"="payment.status.cancelled"
     * })
     */
    protected $status;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="created_at", type="datetime")
     * @Gedmo\Timestampable(on="create")
     * @Grid\Column(title="Created At")
     */
    protected $createdAt;
    
    
    /**
     * @var \DateTime
     *
     * @ORM\Column(name="updated_at", type="datetime", nullable=true)
     * @Gedmo\Timestampable(on="update")
     */
    protected $updatedAt;
    
    /**
     * @var Order
     *  
     * @ORM\OneToOne(targetEntity="WebFactory\Bundle\OrderBundle\Entity\Order", inversedBy="payment")
     * @ORM\JoinColumn(name="order_id", referencedColumnName="id")
     * @Assert\NotBlank
     * @GRID\Column(title="Order", field="order.id", filterable=false)
     * @GRID\Column(title="User", field="order.user.email")
     */
    protected $order;
    
    /**
     * @var string
     *
     * @ORM\Column(name="log", type="text", nullable=true)
     */
    protected $log;

    
    /**
     * Get id
     *
     * @return integer
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set status
     *
     * @param string $status
     * @return Payment
     */
    public function setStatus($status)
    {
        $this->status = $status;

        return $this;
    }

    /**
     * Get status
     *
     * @return string
     */
    public function getStatus()
    {
        return $this->status;
    }

    /**
     * Set createdAt
     *
     * @param \DateTime $createdAt
     * @return Payment
     */
    public function setCreatedAt($createdAt)
    {
        $this->createdAt = $createdAt;

        return $this;
    }

    /**
     * Get createdAt
     *
     * @return \DateTime
     */
    public function getCreatedAt()
    {
        return $this->createdAt;
    }
    
    /**
     * Set updatedAt
     *
     * @param \DateTime $updatedAt
     * @return Payment
     */
    public function setUpdatedAt($updatedAt)
    {
        $this->updatedAt = $updatedAt;

        return $this;
    }

    /**
     * Get updatedAt
     *
     * @return \DateTime
     */
    public function getUpdatedAt()
    {
        return $this->updatedAt;
    }

    /**
     * Set order
     *
     * @param Order $order
     * @return Payment
     */
    public function setOrder(OrderInterface $order)
    {
        $this->order = $order;

        return $this;
    }

    /**
     * Get order
     *
     * @return Order
     */
    public function getOrder()
    {
        return $this->order;
    }
    
    /**
     * Get log
     * 
     * @return string
     */
    public function getLog()
    {
        return $this->log;
    }

    /**
     * Set log
     * 
     * @param string $log
     */
    public function setLog($log = null)
    {
        $this->log = $log;
    }
        
    /**
     * @Assert\Callback
     */
    public function validateStatus(ExecutionContextInterface $context)
    {
        if (!in_array($this->getStatus(), PaymentStatuses::getChoices())) {
            $context->addViolationAt(
                'status',
                'Invalid payment status',
                array(),
                null
            );
        }
    }

}
