<?php

namespace WebFactory\Bundle\PaymentBundle\Event;

class PaymentEvents
{
    const PRE_BUILD_FORM = 'payment.pre_build_form';
    const POST_BUILD_FORM = 'payment.post_build_form';
    const PRE_PROCESS_FORM = 'payment.pre_process_form';
    const POST_PROCESS_FORM = 'payment.post_process_form';
    const SUCCESS_NOTIFICATION = 'payment.success_notification';
    const FAILURE_NOTIFICATION = 'payment.failure_notification';
}