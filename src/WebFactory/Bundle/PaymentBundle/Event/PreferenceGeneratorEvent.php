<?php

namespace WebFactory\Bundle\PaymentBundle\Event;

use Symfony\Component\EventDispatcher\Event;
use WebFactory\Bundle\OrderBundle\Entity\Order;
use WebFactory\Bundle\PaymentBundle\Model\Gateway;

class PreferenceGeneratorEvent extends Event
{

    /**
     *
     * @var array
     */
    private $preferences;

    /**
     *
     * @var Gateway
     */
    private $gateway;

    /**
     *
     * @var Order
     */
    private $order;

    /**
     * 
     * @param Gateway $gateway
     * @param mixed $data
     */
    public function __construct(Gateway $gateway, Order $order)
    {
        $this->gateway = $gateway;
        $this->order = $order;
        $this->preferences = array();
    }

    /**
     * 
     * @return Order
     */
    public function getOrder()
    {
        return $this->order;
    }

    /**
     * 
     * @return Gateway
     */
    public function getGateway()
    {
        return $this->gateway;
    }

    /**
     * 
     * @return array
     */
    public function getPreferences()
    {
        return $this->preferences;
    }

    /**
     * 
     * @param array $preferences
     */
    public function setPreferences($preferences)
    {
        $this->preferences = $preferences;
    }

}
