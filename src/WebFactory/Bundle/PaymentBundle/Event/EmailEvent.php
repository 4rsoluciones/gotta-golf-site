<?php

namespace WebFactory\Bundle\PaymentBundle\Event;

class EmailEvent
{
    private $data;
    private $template;
    private $subject;
    private $from;
    private $replyTo;
    private $to;

    
    function __construct($data, $template, $subject, $from, $replyTo, $to)
    {
        $this->data = $data;
        $this->template = $template;
        $this->subject = $subject;
        $this->from = $from;
        $this->replyTo = $replyTo;
        $this->to = $to;
    }

    
    public function getData()
    {
        return $this->data;
    }

    public function getTemplate()
    {
        return $this->template;
    }

    public function getSubject()
    {
        return $this->subject;
    }

    public function getFrom()
    {
        return $this->from;
    }

    public function getReplyTo()
    {
        return $this->replyTo;
    }

}
