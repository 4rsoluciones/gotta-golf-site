<?php

namespace WebFactory\Bundle\PaymentBundle\Event\Listener;

use Swift_Mailer;
use Swift_Message;
use Symfony\Bundle\TwigBundle\TwigEngine;
use Symfony\Component\Translation\TranslatorInterface;
use WebFactory\Bundle\PaymentBundle\Event\NotificationSuccessEvent;

class SendNotificationSuccessListener
{

    private $mailer;
    private $templating;
    private $template;
    private $subject;
    private $from;
    private $replyTo;
    private $translator;

    public function __construct(Swift_Mailer $mailer, TwigEngine $templating, TranslatorInterface $translator, array $mailingData)
    {
        $this->mailer = $mailer;
        $this->templating = $templating;
        $this->translator = $translator;
        $this->template = $mailingData['template'];
        $this->subject = $translator->trans($mailingData['subject']);
        $this->from = $mailingData['from'];
        $this->replyTo = $mailingData['replyTo'];
    }

    public function sendEmail(NotificationSuccessEvent $event)
    {
        $order = $event->getOrder();

        $body = $this->templating->render($this->template, array('data' => $order));
        $message = Swift_Message::newInstance()
                ->setSubject($this->subject)
                ->setFrom($this->from)
                ->setReplyTo($this->replyTo)
                ->setTo($order->getUser()->getEmail())
                ->setBody($body, 'text/html');

        return $this->mailer->send($message);
    }

}
