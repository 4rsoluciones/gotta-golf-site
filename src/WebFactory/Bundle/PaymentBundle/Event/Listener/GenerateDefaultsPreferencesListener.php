<?php

namespace WebFactory\Bundle\PaymentBundle\Event\Listener;

use Symfony\Component\Routing\Router;
use WebFactory\Bundle\PaymentBundle\Event\PreferenceGeneratorEvent;
use WebFactory\Bundle\PaymentBundle\Gateway\RccPagosGateway;

class GenerateDefaultsPreferencesListener
{

    private $router;

    public function __construct(Router $router)
    {
        $this->router = $router;
    }

    /**
     * 
     * @param string $route
     * @return string
     */
    protected function generateAbsoluteUrl($route)
    {
        return $this->router->generate($route, array(), Router::ABSOLUTE_URL);
    }

    /**
     * 
     * @param PreferenceGeneratorEvent $event
     */
    public function generateMercadoPagoPreferences(PreferenceGeneratorEvent $event)
    {
        $order = $event->getOrder();
        $gateway = $event->getGateway();

        $items = array();
        foreach ($order->getItems() as $item) {
            $a = array(
                "title" => $item->getDescription(),
                "quantity" => $item->getQuantity(),
                "currency_id" => "ARS",
                "unit_price" => (float) $item->getPrice(),
                "id" => $item->getId(),
            );
            $items[] = $a;
        }

        $preferences = array(
            "external_reference" => $order->getId(),
            "items" => $items,
            "payer" => array(
                "email" => $order->getUser()->getEmail(),
                "name" => $order->getUser()->getProfile()->getFirstName(),
                "surname" => $order->getUser()->getProfile()->getLastName(),
            ),
            "back_urls" => array(
                "success" => $this->generateAbsoluteUrl($gateway->getExtra('back_url_success')),
                "failure" => $this->generateAbsoluteUrl($gateway->getExtra('back_url_failure')),
            //"pending" => $this->getExtra('back_url_pending')
            )
        );

        $event->setPreferences($preferences);
    }
    /**
     * 
     * @param PreferenceGeneratorEvent $event
     */
    public function generateRccPagosPreferences(PreferenceGeneratorEvent $event)
    {
        $order = $event->getOrder();
        $gateway = $event->getGateway();

        $preferences = array();
        
        $config = $gateway->getConfig();

        $preferences['rccpagosEmpresa'] = $config['id'];
        $preferences['rccpagosImporte'] = $order->getTotal();
        $preferences['rccpagosOrden'] = $order->getId();
        $errorUrl = $this->generateAbsoluteUrl($gateway->getExtra('back_url_failure'));
        $preferences['rccpagosURLError'] = urlencode($errorUrl);
        $okUrl = $this->generateAbsoluteUrl($gateway->getExtra('back_url_success'));
        $preferences['rccpagosURLOk'] = urlencode($okUrl);
        $preferences['rccpagosUsuario'] = $order->getUser()->getUsername();
        $errorUrl = $this->generateAbsoluteUrl($gateway->getExtra('back_url_failure')) . '?v=###VERIF###&e=###ERROR###';
        $preferences['rccpagosURLError'] = rawurlencode($errorUrl);
        $okUrl = $this->generateAbsoluteUrl($gateway->getExtra('back_url_success')) . '?v=###VERIF###';
        $preferences['rccpagosURLOk'] = rawurlencode($okUrl);
        
        // genero los codigos de verificacion
        // para imprimoYpago y
        // por cada tarjeta de credito 
        $date = date('Y-m-d', strtotime('+5 day'));
        $preferences['rccpagosFechaVencimiento'] = $date;
        $rccpagosVerificacion = array();
        $rccpagosVerificacion[0] = RccPagosGateway::generateVerificationCode($preferences, $config['secret']);
        unset($preferences['rccpagosFechaVencimiento']);
        $preferences['rccpagosCuotas'] = 1;
        $preferences['rccpagosCorreoElectronico'] = $order->getUser()->getEmail();
        for ($i = 1; $i <= 9; $i++) {
            $preferences['rccpagosMedioDePago'] = $i;
            $rccpagosVerificacion[] = RccPagosGateway::generateVerificationCode($preferences, $config['secret']);
        }
        $preferences['rccpagosFechaVencimiento'] = $date;
        for ($i = 0; $i <= 9; $i++) {
            $preferences['rccpagosVerificacion'][$i] = $rccpagosVerificacion[$i];        
        }

        $event->setPreferences($preferences);
    }


}
