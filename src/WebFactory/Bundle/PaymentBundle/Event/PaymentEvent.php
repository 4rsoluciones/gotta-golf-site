<?php

namespace WebFactory\Bundle\PaymentBundle\Event;

use Symfony\Component\EventDispatcher\Event;
use WebFactory\Bundle\PaymentBundle\Model\Gateway;

class PaymentEvent extends Event
{

    /**
     *
     * @var array
     */
    private $data;

    /**
     *
     * @var Gateway
     */
    private $gateway;

    /**
     * 
     * @param Gateway $gateway
     * @param mixed $data
     */
    public function __construct(Gateway $gateway, $data = array())
    {
        $this->gateway = $gateway;
        $this->data = $data;
    }

    /**
     * 
     * @return mixed
     */
    public function getData()
    {
        return $this->data;
    }

    /**
     * 
     * @return Gateway
     */
    public function getGateway()
    {
        return $this->gateway;
    }

}
