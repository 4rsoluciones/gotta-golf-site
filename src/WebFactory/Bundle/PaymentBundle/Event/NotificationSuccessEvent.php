<?php

namespace WebFactory\Bundle\PaymentBundle\Event;

use Symfony\Component\EventDispatcher\Event;
use WebFactory\Bundle\OrderBundle\Entity\Order;

class NotificationSuccessEvent extends Event
{

    /**
     *
     * @var Order
     */
    private $order;

    /**
     * 
     * @param Order $order
     */
    public function __construct(Order $order)
    {
        $this->order = $order;
    }

    /**
     * 
     * @return Order
     */
    public function getOrder()
    {
        return $this->order;
    }

}
