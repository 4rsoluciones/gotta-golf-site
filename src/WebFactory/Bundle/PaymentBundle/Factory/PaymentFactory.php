<?php

namespace WebFactory\Bundle\PaymentBundle\Factory;

use WebFactory\Bundle\OrderBundle\Entity\Order;
use WebFactory\Bundle\PaymentBundle\Entity\Payment;

class PaymentFactory
{
    /**
     * 
     * @param Order $order
     * @param string $status
     * @param string $log
     * @return Payment
     */
    public static function createPayment(Order $order, $status, $log = null)
    {
        $payment = new Payment();
        $payment->setOrder($order);
        $payment->setStatus($status);
        $payment->setLog($log);

        return $payment;
    }
}