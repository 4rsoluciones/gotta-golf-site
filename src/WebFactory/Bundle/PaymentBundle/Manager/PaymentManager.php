<?php

namespace WebFactory\Bundle\PaymentBundle\Manager;

use Doctrine\Common\Collections\ArrayCollection;
use InvalidArgumentException;
use Symfony\Component\HttpFoundation\Response;
use WebFactory\Bundle\OrderBundle\Entity\Order;
use WebFactory\Bundle\PaymentBundle\Model\Gateway;
use WebFactory\Bundle\PaymentBundle\Model\GatewayApiInterface;
use WebFactory\Bundle\PaymentBundle\Model\GatewayFileImportInterface;
use WebFactory\Bundle\PaymentBundle\Model\GatewayFormGeneratorInterface;
use WebFactory\Bundle\PaymentBundle\Model\GatewayNotificationInterface;

class PaymentManager
{

    /**
     *
     * @var Gateway
     */
    private $gateway;

    /**
     * 
     * @param Gateway $gateway
     */
    public function __construct(Gateway $gateway)
    {
        $this->gateway = $gateway;
    }

    /**
     * 
     * @return string
     * @throws InvalidArgumentException
     */
    public function generateForm($extra = array())
    {
        if (!$this->gateway instanceof GatewayFormGeneratorInterface) {
            throw new InvalidArgumentException;
        }

        return $this->gateway->buildForm($extra);
    }

    /**
     * 
     * @return string|Response
     * @throws InvalidArgumentException
     */
    public function processForm(Order $order)
    {
        if (!$this->gateway instanceof GatewayFormGeneratorInterface) {
            throw new InvalidArgumentException;
        }

        return $this->gateway->processForm($order);
    }

    /**
     * 
     * @param array $notification
     */
    public function processNotification($notification, $request)
    {
        if (!$this->gateway instanceof GatewayNotificationInterface) {
            throw new InvalidArgumentException;
        }

        return $this->gateway->processNotification($notification, $request);
    }

    /**
     * 
     * @param mixed $criteria
     * @return mixed
     * @throws InvalidArgumentException
     */
    public function queryStatus($criteria)
    {
        if (!$this->gateway instanceof GatewayApiInterface) {
            throw new InvalidArgumentException;
        }

        return $this->gateway->search($criteria);
    }

    /**
     * 
     * @param \Doctrine\Common\Collections\ArrayCollection $orders
     * @throws InvalidArgumentException
     */
    public function updateStatuses(ArrayCollection $orders)
    {
        if (!$this->gateway instanceof GatewayApiInterface) {
            throw new InvalidArgumentException;
        }

        $this->gateway->updateOrders($orders);
    }

    /**
     * 
     * @param string $file
     * @return boolean
     * @throws InvalidArgumentException
     */
    public function import($file)
    {
        if (!$this->gateway instanceof GatewayFileImportInterface) {
            throw new InvalidArgumentException;
        }

        return $this->gateway->import($file);
    }

}
