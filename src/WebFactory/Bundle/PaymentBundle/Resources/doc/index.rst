Eventos:

- MercadoPago

    En processForm dispara:
        PRE_PROCESS_FORM (para obtener preferencias mediante PreferenceGeneratorEvent)
        POST_PROCESS_FORM (informa la preferencia modificada por mp api mediante PaymentEvent)

- RccPagos

    En buildForm dispara:
        PRE_BUILD_FORM (para obtener preferencias mediante PreferenceGeneratorEvent)

Estos listener pueden deshabilitarse mediante configuración, pero los eventos se disparan bajo estas circunstancias.