<?php

namespace WebFactory\Bundle\PaymentBundle\Gateway;

use DomainException;
use Exception;
use MP;
use Doctrine\ORM\EntityManager;
use Symfony\Bundle\TwigBundle\TwigEngine;
use Symfony\Component\HttpFoundation\RedirectResponse;
use Symfony\Component\HttpFoundation\Request;
use WebFactory\Bundle\OrderBundle\Entity\Order;
use WebFactory\Bundle\OrderBundle\Manager\OrderManager;
use WebFactory\Bundle\PaymentBundle\Event\PaymentEvent;
use WebFactory\Bundle\PaymentBundle\Event\PaymentEvents;
use WebFactory\Bundle\PaymentBundle\Event\PreferenceGeneratorEvent;
use WebFactory\Bundle\PaymentBundle\Model\Gateway;
use WebFactory\Bundle\PaymentBundle\Model\GatewayApiInterface;
use WebFactory\Bundle\PaymentBundle\Model\GatewayFormGeneratorInterface;
use WebFactory\Bundle\PaymentBundle\Model\GatewayNotificationInterface;
use WebFactory\Bundle\PaymentBundle\Entity\MercadoPagoExtras;
use WebFactory\Bundle\PaymentBundle\Factory\PaymentFactory;
use WebFactory\Bundle\OrderBundle\Model\OrderStatuses;
use WebFactory\Bundle\PaymentBundle\Event\NotificationFailureEvent;
use WebFactory\Bundle\PaymentBundle\Event\NotificationSuccessEvent;
use Doctrine\Common\Collections\ArrayCollection;

/**
 * Implementa la pasarela de MercadoPago
 */
class MercadoPagoGateway extends Gateway implements GatewayFormGeneratorInterface, GatewayApiInterface, GatewayNotificationInterface
{

    /**
     *
     * @var MP
     */
    private $mp;

    /**
     *
     * @var EntityManager
     */
    protected $entityManager;

    /**
     * 
     * @param array $config
     * @param OrderManager $orderManager
     * @param TwigEngine $templating
     * @param MP $mp
     */
    public function __construct(array $config, OrderManager $orderManager, TwigEngine $templating, MP $mp)
    {
        parent::__construct($config, $orderManager, $templating);
        $this->mp = $mp;
        $this->mp->sandbox_mode($this->isSandboxMode());
    }

    /**
     * 
     * @param EntityManager $entityManager
     */
    public function setEntityManager(EntityManager $entityManager)
    {
        $this->entityManager = $entityManager;
    }

    /**
     * 
     * @return string
     */
    public function getName()
    {
        return 'MercadoPago';
    }

    /**
     * 
     * {@inheritdoc}
     */
    public function buildForm($extra = array())
    {
        $data = array_merge($this->config, $extra);

        return $this->templating->render('WebFactoryPaymentBundle:MercadoPago:paymentForm.html.twig', $data);
    }

    /**
     * 
     * {@inheritdoc}
     */
    public function processForm(Order $order)
    {
        $mercadoPagoExtraRepository = $this->entityManager->getRepository('WebFactoryPaymentBundle:MercadoPagoExtras');
        $mercadoPagoExtra = $mercadoPagoExtraRepository->findOneByOrder($order->getId());

        if (!$mercadoPagoExtra) {
            $event = new PreferenceGeneratorEvent($this, $order);
            $this->dispatcher->dispatch(PaymentEvents::PRE_PROCESS_FORM, $event);
            $preference_data = $event->getPreferences();

            if (empty($preference_data)) {
                throw new DomainException('Gateway preference needed');
            }

            $preference = $this->mp->create_preference($preference_data);

            $mercadoPagoExtra = new MercadoPagoExtras($preference['response']['id'], $order);
            $this->entityManager->persist($mercadoPagoExtra);
            $this->entityManager->flush($mercadoPagoExtra);
        } else {
            $preference = $this->mp->get_preference($mercadoPagoExtra->getCode());
        }

        $event = new PaymentEvent($this, $preference);
        $this->dispatcher->dispatch(PaymentEvents::POST_PROCESS_FORM, $event);

        return new RedirectResponse($this->getInitPoint($preference));
    }

    /**
     * 
     * @param array $preference
     * @return string
     */
    private function getInitPoint($preference)
    {
        $initPointKey = $this->isSandboxMode() ? 'sandbox_init_point' : 'init_point';

        return $preference['response'][$initPointKey];
    }

    /**
     * 
     * {@inheritdoc}
     */
    public function processNotification($type, Request $notification)
    {
        if ($type === GatewayNotificationInterface::INFORMATION_NOTIFICATION) {
            // Es posible hacer que mercado pago nos informe aca sobre el estado de un pago cuando se produce (INFORMATION_NOTIFICATION)
            // debemos grabar el id de notificacion que nos envia y retornarle codigo 200/201
            // posteriormente debemos realizar la consulta a mp con el id de notificacion
            // Actualmente no está implementado, y estas son solo notificaciones web
        }

        $mercadoPagoExtraRepository = $this->entityManager->getRepository('WebFactoryPaymentBundle:MercadoPagoExtras');
        $mercadoPagoExtra = $mercadoPagoExtraRepository->findOneByCode($notification->get('preference_id'));

        if ($mercadoPagoExtra) {
            switch ($type) {
                case GatewayNotificationInterface::FAILURE_NOTIFICATION:
                    $mercadoPagoExtra->registerFailure(GatewayNotificationInterface::FAILURE_NOTIFICATION); //No se puede obtener el estado de error de la preferencia de mp
                    $this->entityManager->flush($mercadoPagoExtra);
                    $this->dispatcher->dispatch(PaymentEvents::FAILURE_NOTIFICATION, new NotificationFailureEvent($mercadoPagoExtra->getOrder()));
                    $message = 'payment.transaction.cancelled';
                    break;
                case GatewayNotificationInterface::SUCCESS_NOTIFICATION:
                    $mercadoPagoExtra->registerSuccess();
                    $this->entityManager->flush($mercadoPagoExtra);
                    $this->dispatcher->dispatch(PaymentEvents::SUCCESS_NOTIFICATION, new NotificationSuccessEvent($mercadoPagoExtra->getOrder()));
                    $message = 'payment.transaction.accepted';
                    break;
                default:
                    throw new Exception('Not implemented');
            }
        } else {
            $message = 'payment.transaction.invalid';
        }

        return $this->templating->render('WebFactoryPaymentBundle:MercadoPago:message.html.twig', array('message' => $message));
    }

    /**
     * 
     * @param type $criteria
     * @return type
     */
    public function search($criteria)
    {
        return $this->mp->search_payment($criteria);
    }

    /**
     * 
     * @param \Doctrine\Common\Collections\ArrayCollection $orders
     */
    public function updateStatuses(ArrayCollection $orders)
    {
        $mercadoPagoExtraRepository = $this->entityManager->getRepository('WebFactoryPaymentBundle:MercadoPagoExtras');

        foreach ($orders as $order) {
            $filters = array('external_reference' => $order->getId());
            $result = $this->mp->search_payment($filters);

            foreach ($result['results'] as $result) {
                $status = $this->determinePaymentStatus($result['collection']['status']);
                $log = json_encode($result['collection']);
                $payment = PaymentFactory::createPayment($order, $status, $log);
                $order->setStatus($this->determineOrderStatus($result['collection']['status']));
                $mercadoPagoExtra = $mercadoPagoExtraRepository->findOneByOrder($order->getId());
                $mercadoPagoExtra->setStatus($this->determinePreferenceStatus($result['collection']['status']));
                
                $this->entityManager->persist($payment);
                $this->entityManager->persist($order);
                $this->entityManager->persist($mercadoPagoExtra);
                $this->entityManager->flush();
            }
        }
    }
    
    /**
     * 
     * @param string $status
     * @return string
     */
    private function determinePaymentStatus($status)
    {
        switch ($status) {
            case 'approved':
                return \WebFactory\Bundle\PaymentBundle\Model\PaymentStatuses::PAID;
            case 'cancelled':
            case 'refunded':
                return \WebFactory\Bundle\PaymentBundle\Model\PaymentStatuses::CANCELLED;
            default:
                return \WebFactory\Bundle\PaymentBundle\Model\PaymentStatuses::PENDING;
        }
    }

    /**
     * 
     * @param string $status
     * @return string
     */
    private function determineOrderStatus($status)
    {
        switch ($status) {
            case 'approved':
                return OrderStatuses::PAID;
            case 'cancelled':
            case 'refunded':
                return OrderStatuses::CANCELLED;
            default:
                return OrderStatuses::PENDING;
        }
    }
    
    /**
     * 
     * @param string $status
     * @return string
     */
    private function determinePreferenceStatus($status)
    {
        switch ($status) {
            case 'approved':
                return \WebFactory\Bundle\PaymentBundle\Entity\RccPagosExtras::STATUS_SUCCESS;
            case 'cancelled':
            case 'refunded':
                return \WebFactory\Bundle\PaymentBundle\Entity\RccPagosExtras::STATUS_FAILURE;
            default:
                return \WebFactory\Bundle\PaymentBundle\Entity\RccPagosExtras::STATUS_PENDING;
        }
    }

}
