<?php

namespace WebFactory\Bundle\PaymentBundle\Gateway;

use Doctrine\ORM\EntityManager;
use Symfony\Component\HttpFoundation\File\UploadedFile;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Serializer\Exception\Exception;
use WebFactory\Bundle\OrderBundle\Entity\Order;
use WebFactory\Bundle\OrderBundle\Model\OrderStatuses;
use WebFactory\Bundle\PaymentBundle\Entity\RccPagosExtras;
use WebFactory\Bundle\PaymentBundle\Event\NotificationFailureEvent;
use WebFactory\Bundle\PaymentBundle\Event\NotificationSuccessEvent;
use WebFactory\Bundle\PaymentBundle\Event\PaymentEvents;
use WebFactory\Bundle\PaymentBundle\Event\PreferenceGeneratorEvent;
use WebFactory\Bundle\PaymentBundle\Factory\PaymentFactory;
use WebFactory\Bundle\PaymentBundle\Model\Gateway;
use WebFactory\Bundle\PaymentBundle\Model\GatewayFileImportInterface;
use WebFactory\Bundle\PaymentBundle\Model\GatewayFormGeneratorInterface;
use WebFactory\Bundle\PaymentBundle\Model\GatewayNotificationInterface;
use WebFactory\Bundle\PaymentBundle\Model\PaymentStatuses;

class RccPagosGateway extends Gateway implements GatewayFormGeneratorInterface, GatewayNotificationInterface, GatewayFileImportInterface
{
    
    const RCC_PAGOS_STATUS_PENDING = 'Pendiente';
    const RCC_PAGOS_STATUS_CANCELLED = 'Sin Respuesta, Anulada o Vencida';

    /**
     *
     * @var EntityManager
     */
    protected $entityManager;

    /**
     * 
     * @param array $preferences
     * @param string $salt
     * @return string
     */
    public static function generateVerificationCode($preferences, $salt)
    {
        @array_walk($preferences, 'rawurlencode'); //TODO: Es necesario el supresor?
        ksort($preferences);
        $concat = implode('', $preferences);
        $code = md5($concat . $salt);

        return $code;
    }

    /**
     * 
     * @param string $code
     * @param string $salt
     * @return string
     */
    public static function generateInverseVerificationCode($code, $salt)
    {
        $inverse = md5($code . $salt);

        return $inverse;
    }

    /**
     * 
     * @param EntityManager $entityManager
     */
    public function setEntityManager(EntityManager $entityManager)
    {
        $this->entityManager = $entityManager;
    }

    /**
     * {@inheritdoc}
     */
    public function getName()
    {
        return 'RccPagos';
    }

    /**
     * {@inheritdoc}
     */
    public function buildForm($extra = array())
    {
        $order = $extra['order'];

        $event = new PreferenceGeneratorEvent($this, $order);
        $this->dispatcher->dispatch(PaymentEvents::PRE_BUILD_FORM, $event);
        $preferences = $event->getPreferences();
        
        if (empty($preferences)) {
            throw new \DomainException('Gateway preference needed');
        }

        $this->createRccPagosExtras($order, $preferences);

        return $this->templating->render('WebFactoryPaymentBundle:RccPagos:paymentForm.html.twig', array('rccpForm' => $preferences));
    }

    //RccPagosExtras debería estar en modelo y ser implementado en la aplicacion para no grabar en la db si no se quiere
    private function createRccPagosExtras($order, $preferences)
    {
        $rccPagosExtraRepository = $this->entityManager->getRepository('WebFactoryPaymentBundle:RccPagosExtras');
        $rccPagosExtra = $rccPagosExtraRepository->findOneByOrder($order->getId());

        if (!$rccPagosExtra) {
            $rccPagosExtra = new RccPagosExtras();
            $rccPagosExtra->setOrder($order);
            // @todo
            // se asigna el primer codigo de verificación
            // pero vienen 4 por cada medio de pago
            $rccPagosExtra->setVerificationCode($preferences['rccpagosVerificacion'][0]);
            $this->entityManager->persist($rccPagosExtra);
            $this->entityManager->flush($rccPagosExtra);
        }
    }

    /**
     * {@inheritdoc}
     */
    public function processForm(Order $order)
    {
        throw new \InvalidArgumentException('Invalid request');
    }

    /**
     * 
     * {@inheritdoc}
     * 
     * @return string
     * @throws Exception
     */
    public function processNotification($type, Request $notification)
    {
        $rccPagosExtraRepository = $this->entityManager->getRepository('WebFactoryPaymentBundle:RccPagosExtras');
        $rccPagosExtra = $rccPagosExtraRepository->findOneByVerificationCode($notification->get('v'));

        if ($rccPagosExtra) {
            switch ($type) {
                case GatewayNotificationInterface::FAILURE_NOTIFICATION:
                    $rccPagosExtra->registerFailure($notification->get('e'));
                    $this->entityManager->flush($rccPagosExtra);
                    $this->dispatcher->dispatch(PaymentEvents::FAILURE_NOTIFICATION, new NotificationFailureEvent($rccPagosExtra->getOrder()));
                    $message = 'payment.transaction.cancelled';//Ojo que cancelled no es lo mismo que decir error
                    break;
                case GatewayNotificationInterface::SUCCESS_NOTIFICATION:
                    $rccPagosExtra->registerSuccess();
                    $this->entityManager->flush($rccPagosExtra);
                    $this->dispatcher->dispatch(PaymentEvents::SUCCESS_NOTIFICATION, new NotificationSuccessEvent($rccPagosExtra->getOrder()));
                    $message = 'payment.transaction.accepted';
                    break;
                default:
                    throw new \Exception('Not implemented');
            }
        } else {
            $message = 'payment.transaction.invalid';
        }

        return $this->templating->render('WebFactoryPaymentBundle:RccPagos:message.html.twig', array('message' => $message, 'type' => $type));
    }

    public function import($file)
    {
        if (!$this->validateFile($file)) {
            throw new \Exception('File error');
        }
        
        $payments = null;
        $extension = $file->guessExtension();
        switch ($extension) {
            case 'txt':
            case 'csv':
                $payments = $this->loadCSVPayments($file);
                break;
            default:
                break;
        }
        
        foreach ($payments as $payment) {
            $this->entityManager->persist($payment);
        }
        $this->entityManager->flush();
    }

    /**
     * Valida el $file, en este caso la extension
     * 
     * @param UploadedFile $file
     * @return boolean
     */
    public function validateFile($file)
    {
        if (!is_null($file)) {
            $extension = $file->guessExtension();
            switch ($extension) {
//                case 'doc':
//                case 'xls':
                case 'txt':
                case 'csv':
                    $value = true;
                    break;
                default:
                    $value = false;
                    break;
            }
        }
        
        return $value;
    }
    
    /**
     * @param UploadedFile $file
     * @return array<Payment>
     */
    public function loadCSVPayments($file) 
    {
        $payments = array();
        if (($gestor = fopen($file, "r")) !== FALSE) {
            $data = fgetcsv($gestor, 1000, ","); // leo el primer renglon que tiene los nombres de los campos
            while (($data = fgetcsv($gestor, 1000, ",")) !== FALSE) {
                $payment = $this->createPayment($data[3], $data[2], $data[5], $data[6], implode(',', $data));
                if ($payment) {
                    $payments[] = $payment;
                } else {
                    continue;
                }
            }
            fclose($gestor);
        }
        
        return $payments;
    }
    
    public function createPayment($orderId, $price, $username, $rccPagosStatus, $log)
    {
        $payment = null;
        $pendingOrders = $this->orderManager->findPendingOrders();
        $order = null;
        foreach ($pendingOrders as $pendingOrder) {
            // se verifica que haya una order pending y que el precio sea el total de la order
            // tb se verifica el user
            if ($pendingOrder->getId() == $orderId && $pendingOrder->getTotal() == $price && $pendingOrder->getUser()->getUsername() == $username) { 
                $order = $pendingOrder;
            }
        }
        
        if ($order) {
            switch ($rccPagosStatus) {
                case RccPagosGateway::RCC_PAGOS_STATUS_PENDING:
                    $paymentStatus = PaymentStatuses::PENDING;
                    $orderStatus = OrderStatuses::PENDING;
                    break;
                case RccPagosGateway::RCC_PAGOS_STATUS_CANCELLED:
                    $paymentStatus = PaymentStatuses::CANCELLED;
                    $orderStatus = OrderStatuses::CANCELLED;
                    break;
                default:
                    $paymentStatus = PaymentStatuses::PAID;
                    $orderStatus = OrderStatuses::PAID;
                    break;
            }
            
            $payment = $order->getPayment();
            if ($payment) {
                $payment->setStatus($paymentStatus);
            } else {
                $payment = PaymentFactory::createPayment($order, $paymentStatus, $log);
                $order->setPayment($payment);
            }
            
            if (OrderStatuses::canChange($order, $orderStatus)) {
                $order->setStatus($orderStatus);
            }
        }
        
        return $payment;
    }

}
