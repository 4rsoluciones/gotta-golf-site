<?php

namespace WebFactory\Bundle\PaymentBundle\Controller\Frontend;

use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Template;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use WebFactory\Bundle\OrderBundle\Entity\Order;
use WebFactory\Bundle\PaymentBundle\Entity\CreditCard;
use WebFactory\Bundle\PaymentBundle\Manager\PaymentManager;
use WebFactory\Bundle\PaymentBundle\Model\GatewayNotificationInterface;

/**
 * @Route("/credit-card")
 */
class CreditCardController extends Controller
{
    /**
     * @Route("/", name="web_factory_creditcard")
     * @Method("GET")
     * @Template()
     */
    public function listAction()
    {
        $em = $this->getDoctrine()->getManager();
        $repo = $em->getRepository('WebFactoryPaymentBundle:Dues');

        return array('entities' => $repo->findCreditCardsWithDues(), 'type' => 'creditCard');
    }

    /**
     * @param CreditCard $creditCard
     * @Route("/dues", name="web_factory_dues")
     * @Method("POST")
     * @Template("WebFactoryPaymentBundle:Frontend/CreditCard:list.html.twig")
     */
    public function duesListAction(Request $request)
    {
        $em = $this->getDoctrine()->getManager();
        $repo = $em->getRepository('WebFactoryPaymentBundle:Dues');
        $identificationCode = $request->request->get('card');

        return array('entities' => $repo->findDuesByCreditCard($identificationCode), 'type' => 'dues');
    }

}
