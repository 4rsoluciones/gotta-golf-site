<?php

namespace WebFactory\Bundle\PaymentBundle\Controller\Frontend;

use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Template;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use WebFactory\Bundle\OrderBundle\Entity\Order;
use WebFactory\Bundle\PaymentBundle\Entity\CreditCard;
use WebFactory\Bundle\PaymentBundle\Manager\PaymentManager;
use WebFactory\Bundle\PaymentBundle\Model\GatewayNotificationInterface;

/**
 * @Route("/debit-card")
 */
class DebitCardController extends Controller
{
    /**
     * @Route("/", name="web_factory_debitcard")
     * @Method("GET")
     * @Template()
     */
    public function listAction($amexNewCard = null, $mastercardNewCard = null)
    {
        $debitCardProfile = $this->getUser()->getDebitCardProfile();

        return array(
            'amexCards' => $debitCardProfile->getAmexCards(),
            'mastercardCards' => $debitCardProfile->getMastercardCards(),
            'amexNewCard' => $amexNewCard,
            'mastercardNewCard' => $mastercardNewCard,
        );
    }
}
