<?php

namespace WebFactory\Bundle\PaymentBundle\Controller\Frontend;

use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Template;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use WebFactory\Bundle\OrderBundle\Entity\Order;
use WebFactory\Bundle\PaymentBundle\Manager\PaymentManager;
use WebFactory\Bundle\PaymentBundle\Model\GatewayNotificationInterface;
use AndresGotta\Bundle\SaleBundle\Form\AddAmexCardType;
use AndresGotta\Bundle\SaleBundle\Entity\AmexCard;

/**
 * @Route("/payment")
 */
class PaymentController extends Controller
{
    /**
     * 
     * @param Order $order
     * @return Response
     */
    public function payAction(Order $order)
    {
        /* @var $paymentManager PaymentManager */
        $paymentManager = $this->get('web_factory_payment.manager');
        $addAmexCardForm = $this->createForm(new AddAmexCardType, new AmexCard);
        $extra = array(
            'order' => $order,
            'processFormRoute' => 'web_factory_payment_pay_process'
        );
        
        return new Response($paymentManager->generateForm($extra)) ;
    }
    
    /**
     * @Route("/{order}/pay", name="web_factory_payment_pay_process")
     * @Method({"GET", "POST"})
     * @Template()
     */
    public function payProcessAction(Order $order)
    {
        /* @var $paymentManager PaymentManager */
        $paymentManager = $this->get('web_factory_payment.manager');
        
        $response = $paymentManager->processForm($order);
        if ($response instanceof Response) {
            return $response;
        }

        return $this->redirect($this->generateUrl('webfactory_order_frontend_order_show', array('id' => $order->getId())));
    }
    
    /**
     * @Route("/notification/success", name="web_factory_payment_success_notification")
     * @param Request $request
     */
    public function successNotificationAction(Request $request)
    {
        $paymentManager = $this->get('web_factory_payment.manager');
        
        $response = $paymentManager->processNotification(GatewayNotificationInterface::SUCCESS_NOTIFICATION, $request);
        if ($response instanceof Response) {
            return $response;
        }
        
        return new Response($response);
       
    }

    /**
     * @Route("/notification/failure", name="web_factory_payment_failure_notification")
     * @param Request $request
     */
    public function failureNotificationAction(Request $request)
    {
        $paymentManager = $this->get('web_factory_payment.manager');
     
        $response = $paymentManager->processNotification(GatewayNotificationInterface::FAILURE_NOTIFICATION, $request);
        if ($response instanceof Response) {
            return $response;
        }
        
        return new Response($response);
    }
    
    /**
     * @Route("/super-secret-url/for/update-order-statuses", name="web_factory_payment_update_orders")
     */
    public function updateOrdersAction()
    {
        $orders = $this->get('web_factory_order.order.manager')->findPendingOrders();
        $this->get('web_factory_payment.manager')->updateStatuses($orders);
        
        //Muted response...
        return new Response();
    }

}
