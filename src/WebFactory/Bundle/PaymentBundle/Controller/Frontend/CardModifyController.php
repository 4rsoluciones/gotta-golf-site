<?php

namespace WebFactory\Bundle\PaymentBundle\Controller\Frontend;

use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;

/**
 * @Route("/card-modify")
 */
class CardModifyController extends Controller
{
    /**
     * @Route("/", name="web_factory_cardmodify")
     * @Method("GET")
     */
    public function modifyAction(Request $request)
    {
        if ($request->query->has('amex_card_number')) {
            $amexCardManager = $this->get('andres_gotta_sale.amex_card_manager');
            $card = $amexCardManager->modifyCardById($request->get('amex_selected_card'), $request->get('amex_card_number'));
        }
        elseif ($request->query->has('mastercard_card_number')) {
            $mastercardCardManager = $this->get('andres_gotta_sale.mastercard_card_manager');
            $card = $mastercardCardManager->modifyCardById($request->get('mastercard_selected_card'), $request->get('mastercard_card_number'));
        }

        return $this->redirectToRoute('frontend_fos_user_profile_show');
    }
}
