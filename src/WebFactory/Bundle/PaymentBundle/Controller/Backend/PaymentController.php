<?php

namespace WebFactory\Bundle\PaymentBundle\Controller\Backend;

use APY\DataGridBundle\Grid\Action\RowAction;
use APY\DataGridBundle\Grid\Column\BlankColumn;
use APY\DataGridBundle\Grid\Source\Entity;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Template;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\Config\Definition\Exception\Exception;
use Symfony\Component\HttpFoundation\Request;
use WebFactory\Bundle\PaymentBundle\Entity\Payment;
use WebFactory\Bundle\PaymentBundle\Model\Gateway;

/**
 * @Route("/payment")
 */
class PaymentController extends Controller
{
    /**
     * @Route("/")
     * @Method({"GET","POST"})
     * @Template()
     */
    public function indexAction()
    {
        /* @var $source \APY\DataGridBundle\Grid\Source\Entity */
        $source = new Entity('WebFactoryPaymentBundle:Payment');
        /* @var $grid \APY\DataGridBundle\Grid\Grid */
        $grid = $this->get('grid');
        $grid->setSource($source);
        $grid->setId('main');
        $grid->setHiddenColumns(array('id'));
        $grid->setDefaultOrder('createdAt', 'desc');

        $showRowAction = new RowAction('Show', 'webfactory_payment_backend_payment_show', false, '_self', array('class' => 'btn-action glyphicons eye_open btn-info'));
        $grid->addRowAction($showRowAction);
        
        $totalColumn = new BlankColumn(array('id' => 'amount', 'title' => 'Amount'));
        $grid->addColumn($totalColumn, 3);
        
        /* @var $gateway Gateway */
        $gateway = $this->get('web_factory_payment.gateway');
        $hasImport = $gateway->hasImportFeatures();

        return $grid->getGridResponse('WebFactoryPaymentBundle:Backend/Payment:index.html.twig', array('showImportButton' => $hasImport));
    }

    /**
     * @Route("/{id}/show")
     * @Method({"GET"})
     * @Template()
     */
    public function showAction(Payment $payment)
    {
        return array('payment' => $payment);
    }

    /**
     * @Route("/import")
     * @Method({"GET","POST"})
     * @Template
     */
    public function importAction(Request $request)
    {
        $gateway = $this->get('web_factory_payment.gateway');
        if (!$gateway->hasImportFeatures()) {
            throw Exception('import.error');
        }
        
        $file = $request->files->get('file');
        if ($file) {
            try {
                $gateway->import($file);
                $this->get('session')->getFlashBag()->add('success', 'Payments updated');
            } catch (\Exception $ex) {
                $this->get('session')->getFlashBag()->add('error', $ex->getMessage());
            }
            $url = $this->generateUrl('webfactory_payment_backend_payment_index');

            return $this->redirect($url);
        }
        
        return array();
    }
}
