<?php

namespace WebFactory\Bundle\PaymentBundle\Controller\Backend;

use APY\DataGridBundle\Grid\Action\RowAction;
use APY\DataGridBundle\Grid\Source\Entity;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Template;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;
use WebFactory\Bundle\PaymentBundle\Entity\Dues;
use WebFactory\Bundle\PaymentBundle\Form\DuesType;

/**
 * Class CreditCardController
 * @package WebFactory\Bundle\PaymentBundle\Controller\Backend
 *
 * @Route("/credit-card", name="webfactory_payment_creditcard")
 */
class CreditCardController extends Controller
{
    /**
     * @Route("/", name="webfactory_payment_backend_creditcard_index")
     */
    public function indexAction()
    {
        $source = new Entity('WebFactoryPaymentBundle:Dues');
        $grid = $this->get('grid');
        $grid->setSource($source);
        $grid->setDefaultOrder('creditCard.name', 'ASC');

        $grid->setHiddenColumns(array('id'));
        $editRowAction = new RowAction('Edit', 'webfactory_payment_backend_creditcard_edit', false, '_self', array('class' => 'btn-action glyphicons pencil btn-success'));
        $grid->addRowAction($editRowAction);
        $deleteRowAction = new RowAction('Delete', 'webfactory_payment_backend_creditcard_delete', true, '_self', array('class' => 'btn-action glyphicons remove_2 btn-danger'));
        $grid->addRowAction($deleteRowAction);

        return $grid->getGridResponse('WebFactoryPaymentBundle:Backend/CreditCard:index.html.twig');
    }

    /**
     * @Route("/{id}/edit", name="webfactory_payment_backend_creditcard_edit")
     * @Method("GET")
     * @Template()
     */
    public function editAction(Dues $entity)
    {
        $editForm = $this->createForm(new DuesType(), $entity);
        $deleteForm = $this->createDeleteForm($entity->getId());

        return array(
            'entity'      => $entity,
            'edit_form'   => $editForm->createView(),
            'delete_form' => $deleteForm->createView(),
        );
    }

    /**
     * @Route("/{id}/update", name="webfactory_payment_backend_creditcard_update")
     * @Method("PUT")
     * @Template("WebFactoryPaymentBundle:Backend/CreditCard:edit.html.twig")
     */
    public function updateAction(Request $request, Dues $entity)
    {
        $deleteForm = $this->createDeleteForm($entity->getId());
        $editForm = $this->createForm(new DuesType(), $entity);
        $editForm->bind($request);

        if ($editForm->isValid()) {
            $em = $this->getDoctrine()->getManager();
            $em->persist($entity);
            $em->flush();

            $request->getSession()->getFlashBag()->add('success', 'flash.message.generic.updated');

            return $this->redirect($this->generateUrl('webfactory_payment_backend_creditcard_index'));
        }

        return array(
            'entity'      => $entity,
            'edit_form'   => $editForm->createView(),
            'delete_form' => $deleteForm->createView(),
        );
    }

    /**
     * @Route("/new", name="webfactory_payment_backend_creditcard_new")
     * @Method("GET")
     * @Template()
     */
    public function newAction()
    {
        $entity = new Dues();
        $form   = $this->createForm(new DuesType(), $entity);

        return array(
            'entity' => $entity,
            'form'   => $form->createView(),
        );
    }

    /**
     * @Route("/new", name="webfactory_payment_backend_creditcard_create")
     * @Method("POST")
     * @Template("AndresGottaServiceBundle:Backend/Service:new.html.twig")
     */
    public function createAction(Request $request)
    {
        $entity  = new Dues();
        $form = $this->createForm(new DuesType(), $entity);
        $form->bind($request);

        if ($form->isValid()) {
            $em = $this->getDoctrine()->getManager();
            $em->persist($entity);
            $em->flush();

            $request->getSession()->getFlashBag()->add('success', 'flash.message.generic.created');

            return $this->redirect($this->generateUrl('webfactory_payment_backend_creditcard_index'));
        }

        return array(
            'entity' => $entity,
            'form'   => $form->createView(),
        );
    }

    /**
     * @Route("/{id}/delete", name="webfactory_payment_backend_creditcard_delete")
     * @Method("DELETE|GET")
     * @Template()
     */
    public function deleteAction(Request $request, Dues $entity)
    {
        $form = $this->createDeleteForm($entity->getId());
        $form->bind($request);

        if ($form->isValid() || $request->getMethod() == 'GET') {
            $em = $this->getDoctrine()->getManager();
            $em->remove($entity);
            $em->flush();

            $request->getSession()->getFlashBag()->add('success', 'flash.message.generic.deleted');
        }

        return $this->redirect($this->generateUrl('webfactory_payment_backend_creditcard_index'));
    }

    /**
     * Creates a form to delete a Service entity by id.
     *
     * @param mixed $id The entity id
     *
     * @return \Symfony\Component\Form\Form The form
     */
    private function createDeleteForm($id)
    {
        return $this->createFormBuilder(array('id' => $id), array())
            ->add('id', 'hidden')
            ->getForm()
        ;
    }
}