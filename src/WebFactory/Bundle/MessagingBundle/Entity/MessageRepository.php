<?php

namespace WebFactory\Bundle\MessagingBundle\Entity;

use Doctrine\ORM\EntityRepository;
use Symfony\Component\Security\Core\User\UserInterface;

/**
 * MessageRepository
 *
 */
class MessageRepository extends EntityRepository
{

    /**
     *
     * @param \Symfony\Component\Security\Core\User\UserInterface $user
     * @param boolean $returnResults
     * @return \Doctrine\ORM\QueryBuilder|\Doctrine\Common\Collections\ArrayCollection
     */
    public function findAllVisibleByUser(UserInterface $user, $returnResults = true)
    {
        $qb = $this->createQueryBuilder('Message')
                ->addSelect('Reply')
                ->leftJoin('Message.replies', 'Reply', 'with', 'Reply.visible = true')
                ->where('Message.from = :user OR Message.to = :user')
                ->setParameter('user', $user)
                ->andWhere('Message.initialMessage IS NULL')
                ->andWhere('Message.visible = true')
                ->orderBy('Message.createdAt', 'DESC')
        ;

        if($returnResults) {
            return $qb->getQuery()->getResult();
        }

        return $qb;
    }

    /**
     * Solo se usa en frontend para mostrar el detalle
     *
     * @param  integer  $id
     * @param  boolean $returnResults
     * @return
     */
    public function findOneWithReplies($id, $returnResults = true)
    {
        $qb = $this->createQueryBuilder('Message')
                ->addSelect('Reply')
                ->leftJoin('Message.replies', 'Reply', 'with', 'Reply.visible = true')
                ->where('Message.id = :id')
                ->setParameter('id', $id)
                ->andWhere('Message.initialMessage IS NULL')
                ->andWhere('Message.visible = true')
                ->orderBy('Message.createdAt', 'DESC')
        ;

        if($returnResults) {
            return $qb->getQuery()->getSingleResult();
        }

        return $qb;
    }

}
