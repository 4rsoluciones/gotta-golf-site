<?php

namespace WebFactory\Bundle\MessagingBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use Gedmo\Mapping\Annotation as Gedmo;
use Symfony\Component\Validator\Constraints as Assert;
use WebFactory\Bundle\UserBundle\Entity\User;
use Doctrine\Common\Collections\ArrayCollection;
use APY\DataGridBundle\Grid\Mapping as Grid;

/**
 * Message
 *
 * @ORM\Table(name="messages")
 * @ORM\Entity(repositoryClass="WebFactory\Bundle\MessagingBundle\Entity\MessageRepository")
 * @Gedmo\SoftDeleteable(fieldName="deletedAt")
 * @Grid\Source(columns="id, visible, status, createdAt, replies.id:count, from.profile.firstName, from.profile.lastName, to.profile.firstName, to.profile.lastName", groupBy={"id"})
 */
class Message
{

    const UNREADED = 'unreaded';
    const READED = 'readed';

    const VISIBLE = true;
    const HIDDEN = false;

    /**
     * @var integer
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     * @Grid\column(filterable=false)
     */
    protected $id;

    /**
     * @var string
     *
     * @ORM\Column(name="visible", type="boolean", options={"default" = true})
     * @Assert\NotNull
     * @Grid\Column(title="Visible")
     */
    protected $visible;

    /**
     * @var string
     *
     * @ORM\Column(name="status", type="string", length=16)
     * @Assert\NotBlank
     * @Grid\Column(title="Status")
     */
    protected $status;

    /**
     * @var string
     *
     * @ORM\Column(name="content", type="text")
     * @Assert\NotBlank
     * @Assert\Length(
     *      min = "16",
     *      max = "512"
     * )
     */
    protected $content;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="created_at", type="datetime")
     * @Gedmo\Timestampable(on="create")
     * @Grid\Column(title="Created At")
     */
    protected $createdAt;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="updated_at", type="datetime")
     * @Gedmo\Timestampable(on="update")
     */
    protected $updatedAt;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="deleted_at", type="datetime", nullable=true)
     */
    protected $deletedAt;

    /**
     *
     * @var Message
     *
     * @ORM\ManyToOne(targetEntity="Message", inversedBy="replies", cascade={"persist"})
     * @ORM\JoinColumn(name="initial_message_id", referencedColumnName="id", onDelete="CASCADE")
     */
    protected $initialMessage;

    /**
     *
     * @var \Doctrine\Common\Collections\ArrayCollection
     *
     * @ORM\OneToMany(targetEntity="Message", mappedBy="initialMessage", cascade={"persist", "remove"})
     * @Grid\Column(title="Replies", field="replies.id:count")
     */
    protected $replies;

    /**
     *
     * @var \Symfony\Component\Security\Core\User\UserInterface
     * @ORM\ManyToOne(targetEntity="WebFactory\Bundle\UserBundle\Entity\User", inversedBy="sendedMessages", cascade={"persist"}, fetch="LAZY")
     * @Assert\NotBlank
     * @Grid\Column(title="From", field="from.profile.firstName", filterable=false)
     * @Grid\Column(title="From", field="from.profile.lastName", visible=false, filterable=false)
     */
    protected $from;

    /**
     *
     * @var \Symfony\Component\Security\Core\User\UserInterface
     * @ORM\ManyToOne(targetEntity="WebFactory\Bundle\UserBundle\Entity\User", inversedBy="receivedMessages", cascade={"persist"}, fetch="LAZY")
     * @Grid\Column(title="To", field="to.profile.firstName", filterable=false)
     * @Grid\Column(title="To", field="to.profile.lastName", visible=false, filterable=false)
     */
    protected $to;

    /**
     *
     * @param \WebFactory\Bundle\MessagingBundle\Entity\Message $replies
     */
    public function __construct(User $from = null, $content = null, User $to = null, Message $initialMessage = null)
    {
        $this->replies = new ArrayCollection();
        $this->setFrom($from);
        $this->content = $content;
        $this->setTo($to);
        $this->initialMessage = $initialMessage;
        $this->markAsUnread();
        $this->show();
    }

    /**
     * Compone un nuevo mensaje
     * @param  User   $from           El usuario que lo compone
     * @param  string $content        El contenido del mensaje
     * @param  User $to              El usuario que lo recibirá
     * @return Message
     */
    public static function compose(User $from, $content, User $to = null)
    {
        return new Message($from, $content, $to);
    }

    /**
     * Compone un nuevo mensaje de respuesta a otro mensaje
     *
     * @param  User    $from          El usuario que lo compone
     * @param  string  $content       El contenido del mensaje
     * @param  Message $replyTo       El mensaje que responde
     * @return Message
     */
    public static function reply(User $from, $content, Message $replyTo)
    {
        $initialMessage = $replyTo->isInitialMessage() ? $replyTo : $replyTo->getInitialMessage();

        $to = $initialMessage->getTo() ? $initialMessage->getTo() : $initialMessage->getFrom();

        return new Message($from, $content, $to, $initialMessage);
    }

    /**
     * Determina si es un mensaje inicial
     * @return boolean
     */
    public function isInitialMessage()
    {
        return null === $this->initialMessage;
    }

    /**
     * Oculta el mensaje
     */
    public function hide()
    {
        $this->visible = Message::HIDDEN;
    }

    /**
     * Muestra el mensaje
     */
    public function show()
    {
        $this->visible = Message::VISIBLE;
    }

    /**
     * Marca el mensaje como leido
     */
    public function markAsRead()
    {
        $this->status = Message::READED;
    }

    /**
     * Marca el mensaje como no leido
     */
    public function markAsUnread()
    {
        $this->status = Message::UNREADED;
    }

    /**
     * Get id
     *
     * @return integer
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set status
     *
     * @param string $status
     * @return Message
     */
    public function setStatus($status)
    {
        $this->status = $status;

        return $this;
    }

    /**
     * Get status
     *
     * @return string
     */
    public function getStatus()
    {
        return $this->status;
    }

    /**
     * Set content
     *
     * @param string $content
     * @return Message
     */
    public function setContent($content)
    {
        $this->content = $content;

        return $this;
    }

    /**
     * Get content
     *
     * @return string
     */
    public function getContent()
    {
        return $this->content;
    }

    /**
     * Set createdAt
     *
     * @param \DateTime $createdAt
     * @return Message
     */
    public function setCreatedAt($createdAt)
    {
        $this->createdAt = $createdAt;

        return $this;
    }

    /**
     * Get createdAt
     *
     * @return \DateTime
     */
    public function getCreatedAt()
    {
        return $this->createdAt;
    }

    /**
     * Set updatedAt
     *
     * @param \DateTime $updatedAt
     * @return Message
     */
    public function setUpdatedAt($updatedAt)
    {
        $this->updatedAt = $updatedAt;

        return $this;
    }

    /**
     * Get updatedAt
     *
     * @return \DateTime
     */
    public function getUpdatedAt()
    {
        return $this->updatedAt;
    }

    /**
     * Set deletedAt
     *
     * @param \DateTime $deletedAt
     * @return Message
     */
    public function setDeletedAt($deletedAt)
    {
        $this->deletedAt = $deletedAt;

        return $this;
    }

    /**
     * Get deletedAt
     *
     * @return \DateTime
     */
    public function getDeletedAt()
    {
        return $this->deletedAt;
    }

    /**
     * Set initialMessage
     *
     * @param \WebFactory\Bundle\MessagingBundle\Entity\Message $initialMessage
     * @return Message
     */
    public function setInitialMessage(Message $initialMessage = null)
    {
        $this->initialMessage = $initialMessage;

        return $this;
    }

    /**
     * Get initialMessage
     *
     * @return \WebFactory\Bundle\MessagingBundle\Entity\Message
     */
    public function getInitialMessage()
    {
        return $this->initialMessage;
    }

    /**
     * Set from
     *
     * @param \WebFactory\Bundle\UserBundle\Entity\User $from
     * @return Message
     */
    public function setFrom(User $from = null)
    {
        $this->from = $from;

        if ($from) {
            $from->addSendedMessage($this);
        }

        return $this;
    }

    /**
     * Get from
     *
     * @return \WebFactory\Bundle\UserBundle\Entity\User
     */
    public function getFrom()
    {
        return $this->from;
    }

    /**
     * Set to
     *
     * @param \WebFactory\Bundle\UserBundle\Entity\User $to
     * @return Message
     */
    public function setTo(User $to = null)
    {
        $this->to = $to;

        if ($to) {
            $to->addReceivedMessage($this);
        }

        return $this;
    }

    /**
     * Get to
     *
     * @return \WebFactory\Bundle\UserBundle\Entity\User
     */
    public function getTo()
    {
        return $this->to;
    }

    /**
     *
     * @return type
     */
    public function getReplies()
    {
        return $this->replies;
    }

    /**
     *
     * @param \WebFactory\Bundle\MessagingBundle\Entity\Message $replies
     * @return \WebFactory\Bundle\MessagingBundle\Entity\Message
     */
    public function setReplies(Message $replies)
    {
        $this->replies = $replies;
        return $this;
    }

    /**
     *
     * @param \WebFactory\Bundle\MessagingBundle\Entity\Message $reply
     */
    public function addReply(Message $reply)
    {
        $this->replies->add($reply);
    }

    /**
     *
     * @param \WebFactory\Bundle\MessagingBundle\Entity\Message $reply
     */
    public function removeReply(Message $reply)
    {
        $this->replies->removeElement($reply);
    }

    /**
     *
     * @return boolean
     */
    public function hasUnreads(User $user)
    {
        if ($this->getTo() === $user && $this->getStatus() === Message::UNREADED) {
            return true;
        }

        foreach ($this->replies as $reply) {
            if ($reply->getTo() === $user && $reply->getStatus() === Message::UNREADED) {
                return true;
            }

        }

        return false;
    }

    /**
     *
     * @param  User   $user [description]
     * @return [type]       [description]
     */
    public function markAsReadForUser(User $user = null)
    {
        if ($this->getTo() === $user) {
            $this->markAsRead();
        }

        foreach ($this->getReplies() as $reply) {
            if ($reply->getTo() === $user) {
                $reply->markAsRead();
            }
        }
    }

    /**
     * Gets the value of visible.
     *
     * @return string
     */
    public function getVisible()
    {
        return $this->visible;
    }

    /**
     * Sets the value of visible.
     *
     * @param string $visible the visible
     *
     * @return self
     */
    public function setVisible($visible)
    {
        $this->visible = $visible;

        return $this;
    }
}