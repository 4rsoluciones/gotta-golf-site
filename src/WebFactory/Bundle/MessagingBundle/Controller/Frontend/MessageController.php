<?php

namespace WebFactory\Bundle\MessagingBundle\Controller\Frontend;

use Symfony\Component\HttpFoundation\Request;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Template;
use WebFactory\Bundle\MessagingBundle\Entity\Message;
use WebFactory\Bundle\MessagingBundle\Form\MessageType;
use Symfony\Component\HttpFoundation\JsonResponse;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\ParamConverter;

/**
 * Message controller.
 *
 * @Route("/message")
 */
class MessageController extends Controller
{

    /**
     * Finds and displays a Message entity.
     *
     * @Route("/mark-as-read", name="frontend_message_mark_as_read")
     * @Method("GET")
     */
    public function markAsReadAction()
    {
        $id = $this->getRequest()->get('id');
        $em = $this->getDoctrine()->getManager();
        $message = $em->getRepository('WebFactoryMessagingBundle:Message')->find($id);

        if (!$message) {
            throw $this->createNotFoundException('Unable to find Message entity.');
        }

        $message->markAsReadForUser($this->getUser());

        $em->persist($message);
        $em->flush();

        return new JsonResponse(array('ok' => true));
    }

    /**
     * Deletes a Message entity.
     *
     * @Route("/delete", name="frontend_message_delete")
     * @Method("GET")
     */
    public function deleteAction()
    {
        $id = $this->getRequest()->get('id');
        $em = $this->getDoctrine()->getManager();
        $entity = $em->getRepository('WebFactoryMessagingBundle:Message')->find($id);

        if (!$entity) {
            throw $this->createNotFoundException('Unable to find Message entity.');
        }

        $initialMessage = $entity->getInitialMessage();

        $entity->hide();
        $em->flush();

        if ($initialMessage) {
            return $this->redirect($this->generateUrl('frontend_message_show', array('id' => $initialMessage->getId())));
        }

        return new \Symfony\Component\HttpFoundation\Response('');
    }

    /**
     * Lists all Message entities.
     *
     * @Route("/", name="frontend_message")
     * @Method("GET")
     * @Template()
     */
    public function indexAction(Request $request)
    {
        $em = $this->getDoctrine()->getManager();
        $qb = $em->getRepository('WebFactoryMessagingBundle:Message')->findAllVisibleByUser($this->getUser(), false);

        $entities = $this->get('knp_paginator')->paginate($qb, $request->query->get('page', 1), 5);

        return array(
            'entities' => $entities,
        );
    }

    /**
     * Creates a new Message entity.
     *
     * @Route("/", name="frontend_message_create")
     * @Method("POST")
     * @Template("WebFactoryMessagingBundle:Frontend/Message:new.html.twig")
     */
    public function createAction(Request $request)
    {
        $entity = new Message();
        $entity->setFrom($this->getUser());
        $form = $this->createCreateForm($entity);
        $form->handleRequest($request);

        if ($form->isValid()) {

            $em = $this->getDoctrine()->getManager();
            $em->persist($entity);
            $em->flush();

            if($entity->isInitialMessage())
            {
                return new JsonResponse(array('ok' => true, 'url' => $this->generateUrl('frontend_message')));
            }

            $initialMessage = $entity->getInitialMessage();

            return $this->redirect($this->generateUrl('frontend_message_show', array('id' => $initialMessage->getId())));
        }

        $initialMessage = $entity->getInitialMessage();

        if(!$initialMessage) {
            return array('form' => $form->createView());
        }

        return $this->render("WebFactoryMessagingBundle:Frontend/Message:show.html.twig", array(
                    'initialMessage' => $initialMessage,
                    'entity' => $entity,
                    'form' => $form->createView(),
        ));
    }

    /**
     * Creates a form to create a Message entity.
     *
     * @param Message $entity The entity
     *
     * @return \Symfony\Component\Form\Form The form
     */
    private function createCreateForm(Message $entity)
    {
        $form = $this->createForm(new MessageType(), $entity, array(
            'action' => $this->generateUrl('frontend_message_create'),
            'method' => 'POST',
            'attr' => array('novalidate' => 'novalidate',)
        ));

        return $form;
    }

    /**
     * Displays a form to create a new Message entity.
     *
     * @Route("/new", name="frontend_message_new")
     * @Method("GET")
     * @Template()
     */
    public function newAction()
    {
        $id = $this->getRequest()->get('initialMessage');
        $em = $this->getDoctrine()->getManager();

        $initialMessage = null;
        if($id) {
            $initialMessage = $em->getRepository('WebFactoryMessagingBundle:Message')->find($id);
            if (!$initialMessage) {
                throw $this->createNotFoundException('Unable to find Message entity.');
            }
        }

        $entity = new Message();
        $entity->setInitialMessage($initialMessage);
        $formView = $this->createCreateForm($entity)->createView();


        return array(
            'entity' => $entity,
            'form' => $formView,
        );
    }

    /**
     * Finds and displays a Message entity.
     *
     * @Route("/{id}", name="frontend_message_show")
     * @Method("GET")
     * @Template()
     * @ParamConverter("message", class="WebFactoryMessagingBundle:Message", options={"repository_method" = "findOneWithReplies"})
     */
    public function showAction(Message $message)
    {
        return array(
            'initialMessage' => $message,
        );
    }

}
