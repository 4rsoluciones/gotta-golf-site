<?php

namespace WebFactory\Bundle\MessagingBundle\Controller\Backend;

use Symfony\Component\HttpFoundation\Request;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Template;
use WebFactory\Bundle\MessagingBundle\Entity\Message;
use WebFactory\Bundle\MessagingBundle\Form\BackendMessageType as MessageType;
use WebFactory\Bundle\MessagingBundle\Form\ReplyMessageType;
use APY\DataGridBundle\Grid\Source\Entity;
use APY\DataGridBundle\Grid\Action\RowAction;
use APY\DataGridBundle\Grid\Export\CSVExport;
use APY\DataGridBundle\Grid\Column\BlankColumn;
use APY\DataGridBundle\Grid\Action\MassAction;

/**
 * Message controller.
 *
 * @Route("/message")
 */
class MessageController extends Controller
{

    /**
     * Lists all Initial Message entities.
     *
     * @Route("/", name="backend_message")
     * @Method("GET|POST")
     * @Template()
     */
    public function indexAction()
    {
        /* @var $source \APY\DataGridBundle\Grid\Source\Entity */
        $source = new Entity('WebFactoryMessagingBundle:Message');
        /* @var $grid \APY\DataGridBundle\Grid\Grid */
        $grid = $this->get('grid');
        $grid->setSource($source);
        $grid->setId('main');
        $grid->setHiddenColumns(array('id'));
        $grid->setDefaultOrder('id', 'asc');

        $showRowAction = new RowAction('Show', 'backend_message_show', false, '_self', array('class' => 'btn-action glyphicons eye_open btn-info'));
        $grid->addRowAction($showRowAction);
        $editRowAction = new RowAction('Edit', 'backend_message_edit', false, '_self', array('class' => 'btn-action glyphicons pencil btn-success'));
        $grid->addRowAction($editRowAction);
        $deleteRowAction = new RowAction('Delete', 'backend_message_delete', true, '_self', array('class' => 'btn-action glyphicons remove_2 btn-danger'));
        $grid->addRowAction($deleteRowAction);

        $source->manipulateRow(function (\APY\DataGridBundle\Grid\Row $row) {
            $fullname = $row->getField('from.profile.firstName') . ' ' . $row->getField('from.profile.lastName');
            $row->setField('from.profile.firstName', $fullname);

            $fullname = $row->getField('to.profile.firstName') . ' ' . $row->getField('to.profile.lastName');
            $row->setField('to.profile.firstName', $fullname);

            return $row;
        });

        $grid->setDefaultOrder('createdAt', 'desc');
        //$grid->setPermanentFilters(array('initialMessage.id' => array('operator' => 'isNull')));

        $tableAlias = $source->getTableAlias();

        $source->manipulateQuery(
            function ($query) use ($tableAlias) {
                $query->addSelect("
                    (
                        SELECT COUNT(M.id) AS cm
                        FROM WebFactoryMessagingBundle:Message M
                        WHERE
                        M.initialMessage = {$tableAlias} AND
                        (
                            (M.to IS NULL AND M.status = 'unreaded') OR
                            (SELECT COUNT(U.id)
                                FROM WebFactoryMessagingBundle:Message U
                                WHERE U.to IS NULL AND U.status = 'unreaded' AND U.initialMessage = M
                            ) > 0
                        )

                    ) AS unreads");
                $query->andWhere($tableAlias . '.initialMessage IS NULL');
            }
        );

        $em = $this->getDoctrine()->getManager();
        $translator = $this->get('translator');
        $applyPercent = function ($ids, $selectAll, $session, $parameters) use ($em, $translator) {
            if ($selectAll) {
                //No way
            } else {
                if ($ids) {
                    foreach ($ids as $id) {
                        $message = $em->getRepository('WebFactoryMessagingBundle:Message')->find($id);
                        if ($parameters['visible'] === Message::HIDDEN) {
                            $message->hide();
                        } else {
                            $message->show();
                        }
                    }
                    $session->getFlashBag()->add('success', $translator->trans('Updated!'));
                } else {
                    $session->getFlashBag()->add('error', $translator->trans('No items selecteds'));
                }
            }
            $em->flush();
        };

        $grid->addMassAction(new MassAction('Hide', $applyPercent, false, array('visible' => Message::HIDDEN)));
        $grid->addMassAction(new MassAction('Show', $applyPercent, false, array('visible' => Message::VISIBLE)));

        return $grid->getGridResponse('WebFactoryMessagingBundle:Backend/Message:index.html.twig');
    }

    /**
     * Creates a new Message entity.
     *
     * @Route("/new", name="backend_message_create")
     * @Method("POST")
     * @Template("WebFactoryMessagingBundle:Backend/Message:new.html.twig")
     */
    public function createAction(Request $request)
    {
        $entity = new Message();
        $entity->setFrom($this->getUser());
        $form = $this->createCreateForm($entity);
        $form->handleRequest($request);

        if ($form->isValid()) {
            $em = $this->getDoctrine()->getManager();
            $em->persist($entity);
            $em->flush();

            return $this->redirect($this->generateUrl('backend_message_show', array('id' => $entity->getId())));
        }

        return array(
            'entity' => $entity,
            'form'   => $form->createView(),
        );
    }

    /**
    * Creates a form to create a Message entity.
    *
    * @param Message $entity The entity
    *
    * @return \Symfony\Component\Form\Form The form
    */
    private function createCreateForm(Message $entity)
    {
        $form = $this->createForm(new MessageType(), $entity, array(
            'action' => $this->generateUrl('backend_message_create'),
            'method' => 'POST',
        ));

        return $form;
    }

    /**
     * Displays a form to create a new Message entity.
     *
     * @Route("/new", name="backend_message_new")
     * @Method("GET")
     * @Template()
     */
    public function newAction()
    {
        $entity = new Message();
        $form   = $this->createCreateForm($entity);

        return array(
            'entity' => $entity,
            'form'   => $form->createView(),
        );
    }

    /**
     * Finds and displays a Message entity.
     *
     * @Route("/{id}", name="backend_message_show", requirements={"id" = "\d+"})
     * @Method("GET")
     * @Template()
     */
    public function showAction($id)
    {
        $em = $this->getDoctrine()->getManager();

        $entity = $em->getRepository('WebFactoryMessagingBundle:Message')->find($id);

        if (!$entity) {
            throw $this->createNotFoundException('Unable to find Message entity.');
        }

        $entity->markAsReadForUser(null);

        $em->persist($entity);
        $em->flush();

        $deleteForm = $this->createDeleteForm($id);
        $replyForm = $this->createReplyForm(new Message, $id);

        return array(
            'entity'      => $entity,
            'reply_form'  => $replyForm->createView(),
            'delete_form' => $deleteForm->createView(),
        );
    }

    /**
     * Finds and displays a Message entity.
     *
     * @Route("/{id}/reply", name="backend_message_reply")
     * @Method("POST")
     * @Template("WebFactoryMessagingBundle:Backend/Message:show.html.twig")
     */
    public function replyAction(Message $entity)
    {
        $id = $entity->getId();

        $deleteForm = $this->createDeleteForm($id);

        $reply = Message::reply($this->getUser(), '', $entity);

        $form = $this->createReplyForm($reply, $id);
        $form->handleRequest($this->getRequest());

        if ($form->isValid()) {
            $em = $this->getDoctrine()->getManager();
            $em->persist($reply);
            $em->flush();

            $this->get('session')->getFlashBag()->add('notice', $this->get('translator')->trans('Message delivered'));

            return $this->redirect($this->generateUrl('backend_message_show', array('id' => $entity->getId())));
        }

        return array(
            'entity'      => $entity,
            'reply_form'  => $form->createView(),
            'delete_form' => $deleteForm->createView(),
        );
    }

    /**
     *
     * @param  [type] $entity [description]
     * @param  [type] $id     [description]
     * @return [type]         [description]
     */
    private function createReplyForm($entity, $id)
    {
        $form = $this->createForm(new ReplyMessageType(), $entity, array(
            'action' => $this->generateUrl('backend_message_reply', array('id' => $id)),
            'method' => 'POST',
        ));

        return $form;
    }

    /**
     * Displays a form to edit an existing Message entity.
     *
     * @Route("/{id}/edit", name="backend_message_edit")
     * @Method("GET")
     * @Template()
     */
    public function editAction($id)
    {
        $em = $this->getDoctrine()->getManager();

        $entity = $em->getRepository('WebFactoryMessagingBundle:Message')->find($id);

        if (!$entity) {
            throw $this->createNotFoundException('Unable to find Message entity.');
        }

        $editForm = $this->createEditForm($entity);
        $deleteForm = $this->createDeleteForm($id);

        return array(
            'entity'      => $entity,
            'edit_form'   => $editForm->createView(),
            'delete_form' => $deleteForm->createView(),
        );
    }

    /**
    * Creates a form to edit a Message entity.
    *
    * @param Message $entity The entity
    *
    * @return \Symfony\Component\Form\Form The form
    */
    private function createEditForm(Message $entity)
    {
        $form = $this->createForm(new MessageType(), $entity, array(
            'action' => $this->generateUrl('backend_message_update', array('id' => $entity->getId())),
            'method' => 'PUT',
        ));

        return $form;
    }
    /**
     * Edits an existing Message entity.
     *
     * @Route("/{id}", name="backend_message_update")
     * @Method("PUT")
     * @Template("WebFactoryMessagingBundle:Message:edit.html.twig")
     */
    public function updateAction(Request $request, $id)
    {
        $em = $this->getDoctrine()->getManager();

        $entity = $em->getRepository('WebFactoryMessagingBundle:Message')->find($id);

        if (!$entity) {
            throw $this->createNotFoundException('Unable to find Message entity.');
        }

        $deleteForm = $this->createDeleteForm($id);
        $editForm = $this->createEditForm($entity);
        $editForm->handleRequest($request);

        if ($editForm->isValid()) {
            $em->flush();

            return $this->redirect($this->generateUrl('backend_message_edit', array('id' => $id)));
        }

        return array(
            'entity'      => $entity,
            'edit_form'   => $editForm->createView(),
            'delete_form' => $deleteForm->createView(),
        );
    }
    /**
     * Deletes a Message entity.
     *
     * @Route("/{id}/delete", name="backend_message_delete")
     * @Method({"GET","DELETE"})
     */
    public function deleteAction(Request $request, $id)
    {
        if ($request->getMethod() === 'GET') {
            $valid = true;
        } else {
            $form = $this->createDeleteForm($id);
            $form->handleRequest($request);
            $valid = $form->isValid();
        }

        $url = $this->generateUrl('backend_message');

        if ($valid) {
            $em = $this->getDoctrine()->getManager();
            $entity = $em->getRepository('WebFactoryMessagingBundle:Message')->find($id);

            if (!$entity) {
                throw $this->createNotFoundException('Unable to find Message entity.');
            }

            if (!$entity->isInitialMessage()) {
                $url = $this->generateUrl('backend_message_show', array('id' => $entity->getInitialMessage()->getId()));
            }

            $em->remove($entity);
            $em->flush();

            $this->get('session')->getFlashBag()->add('notice', $this->get('translator')->trans('Message deleted'));
        }

        return $this->redirect($url);
    }

    /**
     * Hides a Message entity.
     *
     * @Route("/{id}/hide", name="backend_message_hide")
     * @Method("GET")
     */
    public function hideAction(Request $request, Message $message)
    {
        $message->hide();
        $em = $this->getDoctrine()->getManager();
        $em->flush();

        $this->get('session')->getFlashBag()->add('notice', $this->get('translator')->trans('Message hidden'));

        $id = $message->isInitialMessage() ? $message->getId() : $message->getInitialMessage()->getId();

        return $this->redirect($this->generateUrl('backend_message_show', array('id' => $id)));
    }

    /**
     * Hides a Message entity.
     *
     * @Route("/{id}/visible", name="backend_message_visible")
     * @Method("GET")
     */
    public function visibleAction(Request $request, Message $message)
    {
        $message->show();
        $em = $this->getDoctrine()->getManager();
        $em->flush();

        $this->get('session')->getFlashBag()->add('notice', $this->get('translator')->trans('Message showed'));

        $id = $message->isInitialMessage() ? $message->getId() : $message->getInitialMessage()->getId();

        return $this->redirect($this->generateUrl('backend_message_show', array('id' => $id)));
    }

    /**
     * Creates a form to delete a Message entity by id.
     *
     * @param mixed $id The entity id
     *
     * @return \Symfony\Component\Form\Form The form
     */
    private function createDeleteForm($id)
    {
        return $this->createFormBuilder()
            ->setAction($this->generateUrl('backend_message_delete', array('id' => $id), array('csrf_protection' => false)))
            ->setMethod('DELETE')
            ->add('id', 'hidden')
            ->getForm()
        ;
    }
}
