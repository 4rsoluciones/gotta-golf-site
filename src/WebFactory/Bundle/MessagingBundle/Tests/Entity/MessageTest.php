<?php

namespace WebFactory\Bundle\MessagingBundle\Tests\Entity;

use WebFactory\Bundle\MessagingBundle\Entity\Message;

class MessageTest extends \PHPUnit_Framework_TestCase
{
    public function testCompose()
    {
        $user = $this->getMock('\WebFactory\Bundle\UserBundle\Entity\User');
        $content = "This is a test message";
        $message = Message::compose($user, $content);

        $this->assertNotNull($message);
        $this->assertEquals($user, $message->getFrom());
        $this->assertNull($message->getTo());
        $this->assertNull($message->getInitialMessage());
        $this->assertEquals($content, $message->getContent());
        $this->assertEquals(Message::UNREADED, $message->getStatus());
        $this->assertTrue($message->getVisible());
    }
}