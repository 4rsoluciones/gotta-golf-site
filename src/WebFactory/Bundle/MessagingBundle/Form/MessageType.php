<?php

namespace WebFactory\Bundle\MessagingBundle\Form;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolverInterface;

class MessageType extends AbstractType
{

    /**
     * @param FormBuilderInterface $builder
     * @param array $options
     */
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
                ->add('initialMessage', 'entity_id', array(
                    'class' => 'WebFactoryMessagingBundle:Message',
                    'label' => false,
                ))
                ->add('content', 'textarea', array(
                    'label' => false,
                ))
                ->add('submit', 'submit', array(
                    'label' => 'Send',
                    'attr' => array('class' => 'btn btn-primary'),
                ))
                ->add('reset', 'reset', array(
                    'label' => 'Cancel',
                    'attr' => array('class' => 'btn btn-reset'),
                ))
        ;
    }

    /**
     * @param OptionsResolverInterface $resolver
     */
    public function setDefaultOptions(OptionsResolverInterface $resolver)
    {
        $resolver->setDefaults(array(
            'data_class' => 'WebFactory\Bundle\MessagingBundle\Entity\Message'
        ));
    }

    /**
     * @return string
     */
    public function getName()
    {
        return 'message';
    }

}
