<?php

namespace WebFactory\Bundle\MessagingBundle\Form;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolverInterface;
use WebFactory\Bundle\MessagingBundle\Entity\Message;
use Symfony\Component\Form\FormEvents;
use Symfony\Component\Form\FormEvent;

class BackendMessageType extends AbstractType
{

    /**
     * Construye el formulario
     *
     * @param FormBuilderInterface $builder constructor
     * @param array $options
     */
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
                ->add('to', 'entity', array(
                    'class' => 'WebFactory\Bundle\UserBundle\Entity\User',
                    'property' => 'profileWithEmailAsString',
                    'query_builder' => function ($repo) {
                        $array = array(
                            serialize(array('ROLE_PLAYER')),
                            serialize(array('ROLE_STUDENT')),
                            serialize(array('ROLE_CUSTOMER')),
                        );
                        return $repo->createQueryBuilder('u')
                            ->innerJoin('u.profile', 'p')
                            ->where('u.roles IN (:roles)')
                            ->setParameter('roles', $array)
                            ->orderBy('p.lastName', 'asc');
                    },
                    'attr' => array(
                        'class' => 'span4'
                    )
                ))
                ->add('initialMessage', 'entity_id', array(
                    'class' => 'WebFactoryMessagingBundle:Message',
                    'label' => false,
                ))
                ->add('content', 'textarea', array(
                    'attr' => array(
                        'class' => 'span12',
                        'rows' => 5,
                    )
                ))
        ;

        $builder->addEventListener(FormEvents::PRE_SET_DATA, function(FormEvent $event){
            $form = $event->getForm();
            $data = $event->getData();

            if(!$data) return;

            if($data->getId()) {
                $form
                    ->add('visible', 'checkbox', array(
                        'required' => false,
                    ))
                    ->add('status', 'choice', array(
                        'choices' => array(
                            Message::UNREADED => 'unreaded',
                            Message::READED => 'readed',
                        )
                    ))
                ;
            }
        });
    }

    /**
     * @param OptionsResolverInterface $resolver
     */
    public function setDefaultOptions(OptionsResolverInterface $resolver)
    {
        $resolver->setDefaults(array(
            'data_class' => 'WebFactory\Bundle\MessagingBundle\Entity\Message'
        ));
    }

    /**
     * @return string
     */
    public function getName()
    {
        return 'message';
    }

}
