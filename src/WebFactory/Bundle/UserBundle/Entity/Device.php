<?php

namespace WebFactory\Bundle\UserBundle\Entity;

use WebFactory\Bundle\PushNotificationBundle\Model\Device as BaseDevice;
use Doctrine\ORM\Mapping as ORM;

/**
 * Class Device
 *
 * @ORM\Entity
 * @ORM\Table(name="devices")
 */
class Device extends BaseDevice
{
    /**
     * @var User
     *
     * @ORM\ManyToOne(targetEntity="WebFactory\Bundle\UserBundle\Entity\User", inversedBy="devices")
     */
    protected $user;

    /**
     * @param string $code
     * @param string $type
     * @param User $user
     * @return Device
     */
    public static function create($code, $type, User $user)
    {
        $device = new self();
        $device->setUser($user);
        $device->setRegisteredCode($code);
        $device->setType($type);

        return $device;
    }

    /**
     * @return User
     */
    public function getUser()
    {
        return $this->user;
    }

    /**
     * @param User $user
     */
    public function setUser($user)
    {
        $this->user = $user;
    }
}