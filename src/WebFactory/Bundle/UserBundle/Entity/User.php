<?php

namespace WebFactory\Bundle\UserBundle\Entity;

use AndresGotta\Academy\Domain\Entity\AcademyInterface;
use AndresGotta\Academy\Domain\Entity\AcademyPlace;
use AndresGotta\Academy\Domain\Entity\StudentInterface;
use AndresGotta\Academy\Domain\Entity\TeacherInterface;
use AndresGotta\Academy\Domain\Entity\UserSport;
use AndresGotta\Academy\Domain\ValueObject\Sport;
use AndresGotta\Bundle\GolfBundle\Entity\Round;
use AndresGotta\Bundle\GroupBundle\Entity\Group;
use AndresGotta\Bundle\GroupBundle\Entity\GroupPlayer;
//use AndresGotta\Bundle\ServiceBundle\Entity\Service;
use APY\DataGridBundle\Grid\Mapping as GRID;
use DateTime;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;
use FOS\UserBundle\Entity\User as BaseUser;
use Gedmo\Mapping\Annotation as Gedmo;
use League\OAuth2\Client\Token\AccessToken;
use Symfony\Component\Validator\Constraints as Assert;
use Symfony\Component\Validator\Context\ExecutionContextInterface;
use WebFactory\Bundle\PushNotificationBundle\Model\HasDevicesInterface;
use WebFactory\Bundle\UserBundle\Entity\Profile;
//use AndresGotta\Bundle\SaleBundle\Entity\DebitCardProfile;
//use AndresGotta\Bundle\SaleBundle\Entity\AmexCard;
//use AndresGotta\Bundle\SaleBundle\Entity\MastercardCard;
use WebFactory\PushNotification\DeviceInterface;

/**
 * AppUser
 *
 * @ORM\Table(name="users")
 * @ORM\Entity(repositoryClass="WebFactory\Bundle\UserBundle\Entity\Repository\UserRepository")
 * @Grid\Source(columns="id, profile.firstName, profile.lastName, email, enabled, roles, lastLogin, createdAt", groups={"academy", "default"})
 * @ORM\HasLifecycleCallbacks()
 */
class User extends BaseUser implements TeacherInterface, StudentInterface, AcademyInterface, HasDevicesInterface
{

    const ROLE_PLAYER = 'ROLE_PLAYER';

    const ROLE_GROUP_OWNER   = 'ROLE_GROUP_OWNER';

    /**
     * @var integer
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     * @GRID\Column(filterable=false)
     */
    protected $id;

    /**
     *
     * @var Profile
     * @ORM\OneToOne(targetEntity="Profile", mappedBy="user", cascade={"all"}, orphanRemoval=true)
     * @GRID\Column(title="First Name", field="profile.firstName")
     * @GRID\Column(title="Last Name", field="profile.lastName")
     * @Assert\Valid
     */
    protected $profile;

    //--------------------------------------------------------------------------
    // Atributos de BaseUser con customización de atributos para GRID
    //--------------------------------------------------------------------------

    /**
     * @var string
     *
     * @GRID\Column(title="Email", type="text")
     */
    protected $email;

    /**
     * @var array
     *
     * @GRID\Column(title="Roles", filter="select", selectFrom="values", values={
     *      "ROLE_CUSTOMER"="role.ROLE_CUSTOMER",
     *      "ROLE_ACADEMY"="role.ROLE_ACADEMY",
     *      "ROLE_PLAYER"="role.ROLE_PLAYER",
     *      "ROLE_STUDENT"="role.ROLE_STUDENT",
     *      "ROLE_TEACHER"="role.ROLE_TEACHER",
     *      "ROLE_SECRETARY"="role.ROLE_SECRETARY",
     *      "ROLE_GROUP_OWNER"="role.ROLE_GROUP_OWNER",
     *      "ROLE_SUPER_ADMIN"="role.ROLE_SUPER_ADMIN"
     * })
     * @GRID\Column(title="Roles", filter="select", selectFrom="values", values={
     *      "ROLE_STUDENT"="role.ROLE_STUDENT",
     *      "ROLE_TEACHER"="role.ROLE_TEACHER",
     * }, groups={"academy"})
     */
    protected $roles;

    /**
     * @var boolean
     *
     * @GRID\Column(title="Enabled", type="boolean")
     */
    protected $enabled;

    /**
     * @var Datetime
     *
     * @GRID\Column(title="Last Login", type="datetime")
     */
    protected $lastLogin;

    /**
     * @var ArrayCollection
     *
     * @ORM\OneToMany(targetEntity="WebFactory\Bundle\NewsBundle\Entity\Comment", mappedBy="user", cascade={"persist"})
     */
    protected $newsComments;

    /**
     * @var ArrayCollection
     *
     * @ORM\OneToMany(targetEntity="WebFactory\Bundle\MessagingBundle\Entity\Message", mappedBy="from", cascade={"persist"})
     */
    protected $sendedMessages;

    /**
     * @var ArrayCollection
     *
     * @ORM\OneToMany(targetEntity="WebFactory\Bundle\MessagingBundle\Entity\Message", mappedBy="to", cascade={"persist"})
     */
    protected $receivedMessages;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="created_at", type="datetime")
     * @Gedmo\Timestampable(on="create")
     */
    protected $createdAt;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="updated_at", type="datetime")
     * @Gedmo\Timestampable(on="update")
     */
    protected $updatedAt;

    /**
     * @var ArrayCollection
     *
     * @ORM\OneToMany(targetEntity="AndresGotta\Bundle\GolfBundle\Entity\Round", mappedBy="player", cascade={"persist"})
     */
    protected $rounds;

//    /**
//     * @var ArrayCollection
//     *
//     * @ORM\OneToMany(targetEntity="\AndresGotta\Bundle\SaleBundle\Entity\CustomPrice", mappedBy="user", cascade={"all"}, orphanRemoval=true)
//     * @Assert\Valid
//     */
//    protected $customPrices;

    /**
     * @var ArrayCollection
     *
     * @ORM\OneToMany(targetEntity="AndresGotta\Bundle\GroupBundle\Entity\GroupPlayer", mappedBy="player")
     */
    protected $playerGroups;

//    /**
//     * @var ArrayCollection
//     *
//     * @ORM\OneToOne(targetEntity="AndresGotta\Bundle\SaleBundle\Entity\DebitCardProfile", mappedBy="user", cascade={"all"})
//     */
//    protected $debitCardProfile;

//    /**
//     * @var Service
//     *
//     * @ORM\ManyToMany(targetEntity="AndresGotta\Bundle\ServiceBundle\Entity\Service")
//     */
//    protected $trialledServices;

    /**
     * @var Sport
     * @ORM\Embedded(class="AndresGotta\Academy\Domain\ValueObject\Sport")
     */
    private $sport;

    //- Academy --------------------------------------------------------------------------------------------------------
    /**
     * @var string
     *
     * @ORM\Column(type="string", nullable=true)
     */
    private $academyName;

    /**
     * @var AcademyLogotype
     * @ORM\OneToOne(targetEntity="WebFactory\Bundle\UserBundle\Entity\AcademyLogotype", mappedBy="academy")
     */
    private $academyLogotype;

    /**
     * @var User[]
     * @ORM\ManyToMany(targetEntity="WebFactory\Bundle\UserBundle\Entity\User", inversedBy="teachesInAcademies")
     * @ORM\JoinTable(name="academy_teachers",
     *      joinColumns={@ORM\JoinColumn(name="academy_id", referencedColumnName="id")},
     *      inverseJoinColumns={@ORM\JoinColumn(name="teacher_id", referencedColumnName="id")}
     * )
     */
    private $academyTeachers;

    /**
     * @var User[]
     * @ORM\ManyToMany(targetEntity="WebFactory\Bundle\UserBundle\Entity\User", inversedBy="enrolledInAcademies")
     * @ORM\JoinTable(name="academy_students",
     *      joinColumns={@ORM\JoinColumn(name="academy_id", referencedColumnName="id")},
     *      inverseJoinColumns={@ORM\JoinColumn(name="student_id", referencedColumnName="id")}
     * )
     */
    private $academyStudents;

    /**
     * @var integer
     * @ORM\Column(type="integer", nullable=true)
     * @Assert\Range(min="0")
     */
    private $academyMaxTeachers;

    /**
     * @var integer
     * @ORM\Column(type="integer", nullable=true)
     * @Assert\Range(min="0")
     */
    private $academyMaxStudents;

    /**
     * @var AcademyPlace[]
     * @ORM\OneToMany(targetEntity="AndresGotta\Academy\Domain\Entity\AcademyPlace", mappedBy="academy", cascade={"persist"})
     */
    private $academyPlaces;

    //- Student --------------------------------------------------------------------------------------------------------
    /**
     * @var User[]
     * @ORM\ManyToMany(targetEntity="WebFactory\Bundle\UserBundle\Entity\User", mappedBy="teachesToStudents")
     */
    private $trainedByTeachers;

    /**
     * @var User[]
     * @ORM\ManyToMany(targetEntity="WebFactory\Bundle\UserBundle\Entity\User", mappedBy="academyStudents")
     */
    private $enrolledInAcademies;

    //- Teacher --------------------------------------------------------------------------------------------------------
    /**
     * @var User[]
     * @ORM\ManyToMany(targetEntity="WebFactory\Bundle\UserBundle\Entity\User", inversedBy="trainedByTeachers")
     * @ORM\JoinTable(name="teacher_students",
     *      joinColumns={@ORM\JoinColumn(name="teacher_id", referencedColumnName="id")},
     *      inverseJoinColumns={@ORM\JoinColumn(name="student_id", referencedColumnName="id")}
     * )
     */
    private $teachesToStudents;

    /**
     * @var User[]
     * @ORM\ManyToMany(targetEntity="WebFactory\Bundle\UserBundle\Entity\User", mappedBy="academyTeachers")
     */
    private $teachesInAcademies;

    //------------------------------------------------------------------------------------------------------------------
    /**
     * @var DeviceInterface[]|Collection
     * @ORM\OneToMany(targetEntity="WebFactory\Bundle\UserBundle\Entity\Device", mappedBy="user", cascade={"all"})
     */
    private $devices;
    //------------------------------------------------------------------------------------------------------------------

    /**
     * @var AccessToken
     * @ORM\Embedded(class="League\OAuth2\Client\Token\AccessToken", columnPrefix="gotta_center_")
     */
    private $accessToken;

    /**
     * Constructor
     */
    public function __construct()
    {
        parent::__construct();
        $this->newsComments = new ArrayCollection();
        $this->sendedMessages = new ArrayCollection();
        $this->receivedMessages = new ArrayCollection();
        $this->rounds = new ArrayCollection();
//        $this->customPrices = new ArrayCollection();
        $this->groups = new ArrayCollection();
//        $this->trialledServices = new ArrayCollection();
        $this->academyPlaces = new ArrayCollection();
        $this->academyStudents = new ArrayCollection();
        $this->enrolledInAcademies = new ArrayCollection();
        $this->academyTeachers = new ArrayCollection();
        $this->teachesInAcademies = new ArrayCollection();

        $this->academyMaxStudents = 0;
        $this->academyMaxTeachers = 0;

        $this->devices = new ArrayCollection();
    }

    /**
     * @return string
     */
    public static function generatePassword()
    {
        return substr(md5(uniqid()), 0, 8);
    }

    /**
     *
     * @param string $password
     * @return User
     */
    public function setPlainPassword($password)
    {
        parent::setPlainPassword($password);

        if ($password) {
            $this->setUpdatedAt(new \DateTime);
        }


        return $this;
    }

    /**
     * Get id
     *
     * @return integer
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set profile
     *
     * @param Profile $profile
     * @return User
     */
    public function setProfile(Profile $profile = null)
    {
        $this->profile = $profile;
        $profile->setUser($this);

        return $this;
    }

    /**
     * Get profile
     *
     * @return Profile
     */
    public function getProfile()
    {
        return $this->profile;
    }

    /**
     * Get Roles
     *
     * @param string $userRole
     * @return array
     */
    public static function getRolesOptions($userRole = null)
    {
        if (null === $userRole) {
            return [];
        }

        if ('ROLE_ADMIN' === $userRole) {
            return [
                'ROLE_PLAYER' => 'role.ROLE_PLAYER',
                self::ROLE_GROUP_OWNER => 'role.' . self::ROLE_GROUP_OWNER,
                'ROLE_STUDENT' => 'role.ROLE_STUDENT',
                'ROLE_TEACHER' => 'role.ROLE_TEACHER',
                'ROLE_ACADEMY' => 'role.ROLE_ACADEMY',
                'ROLE_SECRETARY' => 'role.ROLE_SECRETARY',
                'ROLE_SUPER_ADMIN' => 'role.ROLE_SUPER_ADMIN',
            ];
        }

        if ('ROLE_ACADEMY' === $userRole) {
            return  [
                'ROLE_STUDENT' => 'role.ROLE_STUDENT',
                'ROLE_TEACHER' => 'role.ROLE_TEACHER',
            ];
        }

        return [];
    }

    /**
     * Set createdAt
     *
     * @param \DateTime $createdAt
     * @return News
     */
    public function setCreatedAt($createdAt)
    {
        $this->createdAt = $createdAt;

        return $this;
    }

    /**
     * Get createdAt
     *
     * @return \DateTime
     */
    public function getCreatedAt()
    {
        return $this->createdAt;
    }

    /**
     * Set updatedAt
     *
     * @param \DateTime $updatedAt
     * @return News
     */
    public function setUpdatedAt($updatedAt)
    {
        $this->updatedAt = $updatedAt;

        return $this;
    }

    /**
     * Get updatedAt
     *
     * @return \DateTime
     */
    public function getUpdatedAt()
    {
        return $this->updatedAt;
    }

    /**
     * Get profile with email
     * 
     * @return string
     */
    public function getProfileWithEmailAsString()
    {
        return (string) $this->profile . " - {$this->email}";
    }

    /**
     * @ORM\PreRemove
     */
    public function dontRemoveIfHasNewsComments()
    {
        if (!$this->getNewsComments()->isEmpty()) {
            throw new \LogicException("The user has news comments and cannot be removed");
        }
    }

    /**
     * @ORM\PreRemove
     */
    public function dontRemoveIfHasMessages()
    {
        if (!$this->getReceivedMessages()->isEmpty() || !$this->getSendedMessages()->isEmpty()) {
            throw new \LogicException("The user has messages and cannot be removed");
        }
    }

    /**
     * Gets the value of newsComments.
     *
     * @return ArrayCollection
     */
    public function getNewsComments()
    {
        return $this->newsComments;
    }

    /**
     * Sets the value of newsComments.
     *
     * @param ArrayCollection $newsComments the news comments
     *
     * @return self
     */
    public function setNewsComments(ArrayCollection $newsComments)
    {
        $this->newsComments = $newsComments;

        return $this;
    }

    /**
     * Gets the value of sendedMessages.
     *
     * @return ArrayCollection
     */
    public function getSendedMessages()
    {
        return $this->sendedMessages;
    }

    /**
     * Sets the value of sendedMessages.
     *
     * @param ArrayCollection $sendedMessages the sended messages
     *
     * @return self
     */
    public function setSendedMessages(ArrayCollection $sendedMessages)
    {
        $this->sendedMessages = $sendedMessages;

        return $this;
    }

    /**
     * Gets the value of receivedMessages.
     *
     * @return ArrayCollection
     */
    public function getReceivedMessages()
    {
        return $this->receivedMessages;
    }

    /**
     * Sets the value of receivedMessages.
     *
     * @param ArrayCollection $receivedMessages the received messages
     *
     * @return self
     */
    public function setReceivedMessages(ArrayCollection $receivedMessages)
    {
        $this->receivedMessages = $receivedMessages;

        return $this;
    }

    /**
     * Add sended message
     * 
     * @param \WebFactory\Bundle\MessagingBundle\Entity\Message $message
     */
    public function addSendedMessage(\WebFactory\Bundle\MessagingBundle\Entity\Message $message)
    {
        $this->sendedMessages->add($message);
    }

    /**
     * Add received message
     * 
     * @param \WebFactory\Bundle\MessagingBundle\Entity\Message $message
     */
    public function addReceivedMessage(\WebFactory\Bundle\MessagingBundle\Entity\Message $message)
    {
        $this->receivedMessages->add($message);
    }

    /**
     * Add news comment
     * 
     * @param \WebFactory\Bundle\NewsBundle\Entity\Comment $comment
     */
    public function addNewsComment(\WebFactory\Bundle\NewsBundle\Entity\Comment $comment)
    {
        $this->newsComments[] = $comment;
    }

    /**
     * Remove newsComments
     *
     * @param \WebFactory\Bundle\NewsBundle\Entity\Comment $newsComments
     */
    public function removeNewsComment(\WebFactory\Bundle\NewsBundle\Entity\Comment $newsComments)
    {
        $this->newsComments->removeElement($newsComments);
    }

    /**
     * Remove sendedMessages
     *
     * @param \WebFactory\Bundle\MessagingBundle\Entity\Message $sendedMessages
     */
    public function removeSendedMessage(\WebFactory\Bundle\MessagingBundle\Entity\Message $sendedMessages)
    {
        $this->sendedMessages->removeElement($sendedMessages);
    }

    /**
     * Remove receivedMessages
     *
     * @param \WebFactory\Bundle\MessagingBundle\Entity\Message $receivedMessages
     */
    public function removeReceivedMessage(\WebFactory\Bundle\MessagingBundle\Entity\Message $receivedMessages)
    {
        $this->receivedMessages->removeElement($receivedMessages);
    }

    /**
     * Add rounds
     *
     * @param \AndresGotta\Bundle\GolfBundle\Entity\Round $round
     * @return User
     */
    public function addRound(\AndresGotta\Bundle\GolfBundle\Entity\Round $round)
    {
        $this->rounds[] = $round;
        $round->setPlayer($this);

        return $this;
    }

    /**
     * Remove rounds
     *
     * @param \AndresGotta\Bundle\GolfBundle\Entity\Round $rounds
     */
    public function removeRound(\AndresGotta\Bundle\GolfBundle\Entity\Round $rounds)
    {
        $this->rounds->removeElement($rounds);
    }

    /**
     * Get rounds
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getRounds()
    {
        return $this->rounds;
    }
    
//    /**
//     * Add customPrice
//     *
//     * @param \AndresGotta\Bundle\SaleBundle\Entity\CustomPrice $customPrice
//     * @return User
//     */
//    public function addCustomPrice(\AndresGotta\Bundle\SaleBundle\Entity\CustomPrice $customPrice)
//    {
//        $customPrice->setUser($this);
//        $this->customPrices[] = $customPrice;
//
//        return $this;
//    }
//
//    /**
//     * Remove customPrice
//     *
//     * @param \AndresGotta\Bundle\SaleBundle\Entity\CustomPrice $customPrice
//     * @return User
//     */
//    public function removeCustomPrice(\AndresGotta\Bundle\SaleBundle\Entity\CustomPrice $customPrice)
//    {
//        $customPrice->setUser(null);
//        $this->customPrices->removeElement($customPrice);
//
//        return $this;
//    }
//
//    /**
//     * Get customPrices
//     *
//     * @return \AndresGotta\Bundle\SaleBundle\Entity\CustomPrice
//     */
//    public function getCustomPrices()
//    {
//        return $this->customPrices;
//    }
//
//    /**
//     * Get customPrice
//     *
//     * @return \AndresGotta\Bundle\ServiceBundle\Entity\Service
//     */
//    public function getCustomPrice(Service $service)
//    {
//        $customPrice = 1;
//
//        foreach ($this->customPrices as $custom) {
//            if ($custom->getService() == $service) {
//                $customPrice = $custom->getPercentage();
//            }
//        }
//
//        return $customPrice;
//    }
//
//    /**
//     * Set customPrices
//     *
//     * @param array $customPrices
//     * @return User
//     */
//    public function setCustomPrices($customPrices)
//    {
//        $this->customPrices = $customPrices;
//
//        return $this;
//    }


    /**
     * Add playerGroup
     *
     * @param \AndresGotta\Bundle\GroupBundle\Entity\GroupPlayer $playerGroup
     *
     * @return User
     */
    public function addPlayerGroup(\AndresGotta\Bundle\GroupBundle\Entity\GroupPlayer $playerGroup)
    {
        $this->playerGroups[] = $playerGroup;

        return $this;
    }

    /**
     * Remove playerGroup
     *
     * @param \AndresGotta\Bundle\GroupBundle\Entity\GroupPlayer $playerGroup
     */
    public function removePlayerGroup(\AndresGotta\Bundle\GroupBundle\Entity\GroupPlayer $playerGroup)
    {
        $this->playerGroups->removeElement($playerGroup);
    }

    /**
     * Get playerGroups
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getPlayerGroups()
    {
        return $this->playerGroups;
    }

    /**
     * @return int
     */
    public function getTotalHoles()
    {
        $holes = 0;
        /** @var Round $round */
        foreach ($this->getRounds() as $round) {
            $holes += $round->getHoles()->count();
        }

        return $holes;
    }

    /**
     * @param Group $group
     * @return GroupPlayer|bool
     */
    public function getPlayerByGroup(Group $group)
    {
        /** @var GroupPlayer $player */
        foreach ($group->getPlayers() as $player) {
            if ($player->getPlayer() === $this) {
                return $player;
            }
        }

        return false;
    }

//    /**
//     * Set debitCardProfile
//     *
//     * @param DebitCardProfile $debitCardProfile
//     * @return User
//     */
//    public function setDebitCardProfile(DebitCardProfile $debitCardProfile = null)
//    {
//        $this->debitCardProfile = $debitCardProfile;
//        $debitCardProfile->setUser($this);
//
//        return $this;
//    }
//
//    /**
//     * Get debitCardProfile
//     *
//     * @return DebitCardProfile
//     */
//    public function getDebitCardProfile()
//    {
//        if (!$this->debitCardProfile) {
//            $this->setDebitCardProfile(new DebitCardProfile);
//        }
//        return $this->debitCardProfile;
//    }
//
//    public function addAmexCard(AmexCard $amexCard)
//    {
//        if (!$this->debitCardProfile) {
//            $this->setDebitCardProfile(new DebitCardProfile);
//        }
//        $this->debitCardProfile->addAmexCard($amexCard);
//    }
//
//
//    public function addMastercardCard(MastercardCard $mastercardCard)
//    {
//        if (!$this->debitCardProfile) {
//            $this->setDebitCardProfile(new DebitCardProfile);
//        }
//        $this->debitCardProfile->addMastercardCard($mastercardCard);
//    }

//    /**
//     * @return Service|ArrayCollection
//     */
//    public function getTrialledServices()
//    {
//        return $this->trialledServices;
//    }
//
//    /**
//     * @param Service $service
//     */
//    public function addTrialledService(Service $service)
//    {
//        $this->trialledServices[] = $service;
//    }

    /**
     * @return string
     */
    public function getAcademyName()
    {
        return $this->academyName;
    }

    /**
     * @param string $academyName
     */
    public function setAcademyName($academyName)
    {
        $this->academyName = $academyName;
    }

    /**
     * @return User[]|Collection
     */
    public function getAcademyTeachers()
    {
        return $this->academyTeachers;
    }

    /**
     * @param User[] $academyTeachers
     */
    public function setAcademyTeachers($academyTeachers)
    {
        $this->academyTeachers = $academyTeachers;
    }

    /**
     * @return User[]|Collection
     */
    public function getAcademyStudents()
    {
        return $this->academyStudents;
    }

    /**
     * @param User[] $academyStudents
     */
    public function setAcademyStudents($academyStudents)
    {
        $this->academyStudents = $academyStudents;
    }

    /**
     * @return int
     */
    public function getAcademyMaxTeachers()
    {
        return $this->academyMaxTeachers;
    }

    /**
     * @param int $academyMaxTeachers
     */
    public function setAcademyMaxTeachers($academyMaxTeachers)
    {
        $this->academyMaxTeachers = $academyMaxTeachers;
    }

    /**
     * @return int
     */
    public function getAcademyMaxStudents()
    {
        return $this->academyMaxStudents;
    }

    /**
     * @param int $academyMaxStudents
     */
    public function setAcademyMaxStudents($academyMaxStudents)
    {
        $this->academyMaxStudents = $academyMaxStudents;
    }

    /**
     * @return AcademyPlace[]|Collection
     */
    public function getAcademyPlaces()
    {
        return $this->academyPlaces;
    }

    /**
     * @param AcademyPlace[] $academyPlaces
     */
    public function setAcademyPlaces($academyPlaces)
    {
        $this->academyPlaces = $academyPlaces;
    }

    /**
     * @param string $name
     * @param string $address
     */
    public function addAcademyPlace($name, $address = null)
    {
        $this->academyPlaces->add(new AcademyPlace($this, $name, $address));
    }

    /**
     * @return User[]|Collection
     */
    public function getTeachers()
    {
        return $this->academyTeachers;
    }

    /**
     * @return User[]|Collection
     */
    public function getStudents()
    {
        return $this->academyStudents;
    }

    /**
     * @return string
     */
    public function getName()
    {
        return $this->academyName;
    }

    /**
     * @return AcademyLogotype
     */
    public function getLogotype()
    {
        return $this->academyLogotype;
    }

    /**
     * @return int
     */
    public function getMaxTeachers()
    {
        return $this->academyMaxTeachers;
    }

    /**
     * @return int
     */
    public function getMaxStudents()
    {
        return $this->academyMaxStudents;
    }

    /**
     * @return AcademyPlace[]|Collection
     */
    public function getPlaces()
    {
        return $this->academyPlaces;
    }

    /**
     * @return User[]|Collection
     */
    public function getTrainedByTeachers()
    {
        return $this->trainedByTeachers;
    }

    /**
     * @return User[]|Collection
     */
    public function getEnrolledInAcademies()
    {
        return $this->enrolledInAcademies;
    }

    /**
     * @return User[]|Collection
     */
    public function getTeachesToStudents()
    {
        return $this->teachesToStudents;
    }

    /**
     * @return User[]|Collection
     */
    public function getTeachesInAcademies()
    {
        return $this->teachesInAcademies;
    }

    /**
     * Add academyStudent
     *
     * @param User $academyStudent
     *
     * @return User
     */
    public function addAcademyStudent(User $academyStudent)
    {
        $this->academyStudents[] = $academyStudent;

        return $this;
    }

    /**
     * Remove academyStudent
     *
     * @param User $academyStudent
     */
    public function removeAcademyStudent(User $academyStudent)
    {
        $this->academyStudents->removeElement($academyStudent);
    }


    /**
     * @inheritdoc
     */
    public function addEnrolledInAcademy(AcademyInterface $academy)
    {
        if (!$this->hasRole('ROLE_STUDENT')) {
            $this->addRole('ROLE_STUDENT');
        }

        if ($academy->getAcademyStudents()->contains($this)) {
            return $this;
        }
        $academy->addAcademyStudent($this);
        $this->enrolledInAcademies[] = $academy;

        return $this;
    }

    /**
     * Remove enrolledInAcademy
     *
     * @param User $enrolledInAcademy
     */
    public function removeEnrolledInAcademy(User $enrolledInAcademy)
    {
        $enrolledInAcademy->removeAcademyStudent($this);
        $this->enrolledInAcademies->removeElement($enrolledInAcademy);
    }

    /**
     * Add academyTeacher
     *
     * @param User $academyTeacher
     *
     * @return User
     */
    public function addAcademyTeacher(User $academyTeacher)
    {
        $this->academyTeachers[] = $academyTeacher;

        return $this;
    }

    /**
     * Remove academyTeacher
     *
     * @param User $academyTeacher
     */
    public function removeAcademyTeacher(User $academyTeacher)
    {
        $this->academyTeachers->removeElement($academyTeacher);
    }

    /**
     * Add teachesInAcademy
     *
     * @param User $teachesInAcademy
     *
     * @return User
     */
    public function addTeachesInAcademy(User $teachesInAcademy)
    {
        $teachesInAcademy->addAcademyTeacher($this);
        $this->teachesInAcademies[] = $teachesInAcademy;

        return $this;
    }

    /**
     * Remove teachesInAcademy
     *
     * @param User $teachesInAcademy
     */
    public function removeTeachesInAcademy(User $teachesInAcademy)
    {
        $teachesInAcademy->removeAcademyTeacher($this);
        $this->teachesInAcademies->removeElement($teachesInAcademy);
    }

    /**
     * @inheritdoc
     */
    public function addTrainedByTeacher(TeacherInterface $teacher)
    {
        if (!$this->hasRole('ROLE_STUDENT')) {
            $this->addRole('ROLE_STUDENT');
        }

        if ($teacher->getTeachesToStudents()->contains($this)) {
            return $this;
        }
        $teacher->addTeachesToStudent($this);
        $this->trainedByTeachers[] = $teacher;

        return $this;
    }

    /**
     * Remove trainedByTeacher
     *
     * @param User $trainedByTeacher
     */
    public function removeTrainedByTeacher(User $trainedByTeacher)
    {

        $trainedByTeacher->removeTeachesToStudent($this);
        $this->trainedByTeachers->removeElement($trainedByTeacher);
    }

    /**
     * Add teachesToStudent
     *
     * @param User $teachesToStudent
     *
     * @return User
     */
    public function addTeachesToStudent(User $teachesToStudent)
    {
        $this->teachesToStudents[] = $teachesToStudent;

        return $this;
    }

    /**
     * Remove teachesToStudent
     *
     * @param User $teachesToStudent
     */
    public function removeTeachesToStudent(User $teachesToStudent)
    {
        $this->teachesToStudents->removeElement($teachesToStudent);
    }

    /**
     * @return Sport
     */
    public function getSport()
    {
        return $this->sport;
    }

    /**
     * @param $sport
     */
    public function setSport(Sport $sport)
    {
        $this->sport = $sport;
    }

    public function getDevices()
    {
        return $this->devices;
    }

    public function setDevices(Collection $devices)
    {
        $this->devices = $devices;
    }

    public function addDevice(DeviceInterface $device)
    {
        $device->setUser($this);
        $this->devices->add($device);
    }

    public function removeDevice(DeviceInterface $device)
    {
        $this->devices->removeElement($device);
    }

    public function hasDevice(DeviceInterface $device)
    {
        return $this->devices->contains($device);
    }

    /**
     * @param User $student
     * @return bool
     */
    public function hasAcademyStudent(User $student)
    {
        foreach ($this->academyStudents as $academyStudent) {
            if ($academyStudent === $student) {
                return true;
            }
        }

        return false;
    }

    /**
     * @param User $teacher
     * @return bool
     */
    public function hasAcademyTeacher(User $teacher)
    {
        foreach ($this->academyTeachers as $academyTeacher) {
            if ($academyTeacher === $teacher) {
                return true;
            }
        }

        return false;
    }

    /**
     * @param string $salt
     */
    public function setSalt($salt)
    {
        $this->salt = $salt;
    }

    /**
     * @return AccessToken
     */
    public function getAccessToken()
    {
        return $this->accessToken;
    }

    /**
     * @param AccessToken $accessToken
     */
    public function setAccessToken(AccessToken $accessToken)
    {
        $this->accessToken = $accessToken;
    }

    /**
     * @Assert\Callback(groups={"Academy"})
     */
    public function validateAcademyName(ExecutionContextInterface $context)
    {
        if ($this->hasRole('ROLE_ACADEMY') && !$this->academyName) {
            $context
                ->buildViolation('El nombre de la academia es requerido')
                ->atPath('academyName')
                ->addViolation();
        }
    }
}
