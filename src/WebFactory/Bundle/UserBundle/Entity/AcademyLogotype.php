<?php

namespace WebFactory\Bundle\UserBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity
 * @ORM\Table(name="academy_logotypes")
 */
class AcademyLogotype
{
    /**
     * @var integer
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @var User
     * @ORM\OneToOne(targetEntity="WebFactory\Bundle\UserBundle\Entity\User", inversedBy="academyLogotype")
     */
    private $academy;
}