<?php

namespace WebFactory\Bundle\UserBundle\Entity;

use AndresGotta\Bundle\GolfBundle\Entity\Tour;
use AndresGotta\Bundle\GolfBundle\ValueObject\PlayerCondition;
use Doctrine\ORM\Mapping as ORM;
use APY\DataGridBundle\Grid\Mapping as Grid;
use Symfony\Component\Validator\Constraints as Assert;
use Symfony\Component\Validator\ExecutionContextInterface;
use WebFactory\Bundle\LocationBundle\Entity\State;
use WebFactory\Bundle\LocationBundle\Model\LocalizableInterface;

/**
 * Profile
 *
 * @ORM\Table(name="profiles")
 * @ORM\Entity
 * @Assert\Callback(methods={"isBirthDateValid"})
 */
class Profile implements LocalizableInterface
{

    /**
     * @var integer
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    protected $id;

    /**
     *
     * @var User
     * @ORM\OneToOne(targetEntity="User", inversedBy="profile", cascade={"persist"}, fetch="LAZY")
     * @ORM\JoinColumn(name="user_id", referencedColumnName="id", onDelete="CASCADE")
     * @Assert\NotNull()
     */
    protected $user;

    /**
     * Ciudad del usuario
     *
     * @var State
     * @ORM\ManyToOne(targetEntity="WebFactory\Bundle\LocationBundle\Entity\State", cascade={"persist"}, fetch="LAZY")
     * @ORM\JoinColumn(name="state_id", referencedColumnName="id", onDelete="SET NULL")
     */
    protected $state;

    /**
     * Ciudad del usuario
     *
     * @var string
     * @ORM\Column(name="city", type="string", length=255, nullable=true)
     */
    protected $city;

    /**
     * @var string
     *
     * @ORM\Column(name="first_name", type="string", length=255, nullable=true)
     * @GRID\Column(title="First Name", type="text")
     */
    protected $firstName;

    /**
     * @var string
     *
     * @ORM\Column(name="last_name", type="string", length=255, nullable=true)
     * @GRID\Column(title="Last Name", type="text")
     */
    protected $lastName;

    /**
     * @var string
     *
     * @ORM\Column(name="company_name", type="string", length=255, nullable=true)
     * @GRID\Column(title="Company Name", type="text")
     */
    protected $companyName;

    /**
     * Fecha de nacimiento del usuario
     *
     * @var \DateTime
     *
     * @ORM\Column(name="birth_date", type="date", nullable=true)
     */
    protected $birthDate;

    /**
     * Género del usuario
     * Valores posibles: 'male', 'female'
     *
     * @var string
     * @ORM\Column(name="gender", type="string", length=6, nullable=true)
     */
    protected $gender;

    /**
     * Nick del usuario
     *
     * @var string
     * @ORM\Column(name="mobile", type="string", length=255, nullable=true)
     */
    protected $mobile;

    /**
     * @var string
     *
     * @ORM\Column(name="handicap", type="integer", nullable=true)
     * @Grid\Column(title="Handicap")
     * @Assert\NotNull()
     */
    protected $handicap = 0;

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     */
    protected $country;
    /**
     * @var Tour
     * @ORM\ManyToOne(targetEntity="AndresGotta\Bundle\GolfBundle\Entity\Tour")
     */
    protected $tour;

    /**
     * @ORM\Column(name="`condition`", type="string", length=255)
     * @Assert\NotNull()
     */
    protected $condition = PlayerCondition::AMATEUR;

    /**
     * @var float
     *
     * @ORM\Column(name="score", type="float", scale=10, precision=2)
     */
    protected $score = 0;

    /**
     * Get id
     *
     * @return integer
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     *
     * @return State
     */
    public function getState()
    {
        return $this->state;
    }

    /**
     *
     * @param State $state
     * @return \WebFactory\Bundle\UserBundle\Entity\Profile
     */
    public function setState($state)
    {
        $this->state = $state;
        return $this;
    }

    /**
     * Set city
     *
     * @param string $city
     * @return Profile
     */
    public function setCity($city)
    {
        $this->city = $city;

        return $this;
    }

    /**
     * Get city
     *
     * @return string
     */
    public function getCity()
    {
        return $this->city;
    }

    /**
     * Set firstName
     *
     * @param string $firstName
     * @return Profile
     */
    public function setFirstName($firstName)
    {
        $this->firstName = $firstName;

        return $this;
    }

    /**
     * Get firstName
     *
     * @return string
     */
    public function getFirstName()
    {
        return $this->firstName;
    }

    /**
     * Set lastName
     *
     * @param string $lastName
     * @return Profile
     */
    public function setLastName($lastName)
    {
        $this->lastName = $lastName;

        return $this;
    }

    /**
     * Get lastName
     *
     * @return string
     */
    public function getLastName()
    {
        return $this->lastName;
    }

    /**
     * Set birthDate
     *
     * @param \DateTime $birthDate
     * @return Profile
     */
    public function setBirthDate($birthDate)
    {
        $this->birthDate = $birthDate;

        return $this;
    }

    /**
     * Get birthDate
     *
     * @return \DateTime
     */
    public function getBirthDate()
    {
        return $this->birthDate;
    }

    /**
     * Set gender
     *
     * @param string $gender
     * @return Profile
     */
    public function setGender($gender)
    {
        $this->gender = $gender;

        return $this;
    }

    /**
     * Get gender
     *
     * @return string
     */
    public function getGender()
    {
        return $this->gender;
    }

    /**
     * Set mobile
     *
     * @param string $mobile
     * @return Profile
     */
    public function setMobile($mobile)
    {
        $this->mobile = $mobile;

        return $this;
    }

    /**
     * Get mobile
     *
     * @return string
     */
    public function getMobile()
    {
        return $this->mobile;
    }

    /**
     * Set user
     *
     * @param \WebFactory\Bundle\UserBundle\Entity\User $user
     * @return Profile
     */
    public function setUser(\WebFactory\Bundle\UserBundle\Entity\User $user = null)
    {
        $this->user = $user;

        return $this;
    }

    /**
     * Get user
     *
     * @return \WebFactory\Bundle\UserBundle\Entity\User
     */
    public function getUser()
    {
        return $this->user;
    }

    /**
     *
     * @return string
     */
    public function getCompanyName()
    {
        return $this->companyName;
    }

    /**
     *
     * @param string $companyName
     * @return \WebFactory\Bundle\UserBundle\Entity\Profile
     */
    public function setCompanyName($companyName)
    {
        $this->companyName = $companyName;
        return $this;
    }

    /**
     *
     * @return string
     */
    public function __toString()
    {
        if ($this->companyName) {
            return sprintf('%s, %s (%s)', $this->lastName, $this->firstName, $this->companyName);
        }

        return sprintf('%s, %s', $this->lastName, $this->firstName);
    }

    /**
     * Retorna el valor de handicap
     * @return integer
     */
    public function getHandicap()
    {
        return $this->handicap;
    }

    /**
     * Aplica el handicap
     * @param integer $handicap
     */
    public function setHandicap($handicap)
    {
        $this->handicap = $handicap;
    }

    /**
     * Retorna opciones de condición
     * @return array
     */
    public static function getConditionChoices()
    {
        return \AndresGotta\Bundle\GolfBundle\ValueObject\PlayerCondition::getChoices();
    }

    /**
     * Retorna la condición
     * @return string
     */
    public function getCondition()
    {
        return $this->condition;
    }

    /**
     * Aplica la condición
     * @param string $condition
     */
    public function setCondition($condition)
    {
        $this->condition = $condition;
    }

    /**
     * @return float
     */
    public function getScore()
    {
        return $this->score;
    }

    /**
     * @param float $score
     */
    public function setScore($score)
    {
        $this->score = $score;
    }

    /**
     *
     * @param \Symfony\Component\Validator\ExecutionContextInterface $context
     */
    public function isBirthDateValid(ExecutionContextInterface $context)
    {
        $birthDate = $this->getBirthDate();
        $now = new \DateTime;
        if($birthDate >= $now) {
            $context->addViolationAt('birthDate', 'Invalid', array(), null);
        }
    }

    /**
     * @return mixed
     */
    public function getCountry()
    {
        return $this->country;
    }

    /**
     * @param mixed $country
     */
    public function setCountry($country)
    {
        $this->country = $country;
    }

    /**
     * @return Tour
     */
    public function getTour()
    {
        return $this->tour;
    }

    /**
     * @param Tour $tour
     */
    public function setTour($tour)
    {
        $this->tour = $tour;
    }

}