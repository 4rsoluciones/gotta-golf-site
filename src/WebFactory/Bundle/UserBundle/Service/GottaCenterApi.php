<?php

namespace WebFactory\Bundle\UserBundle\Service;

use GuzzleHttp\Client;
use League\OAuth2\Client\Provider\GenericProvider;
use League\OAuth2\Client\Token\AccessToken;
use Psr\Log\LoggerInterface;
use Symfony\Component\HttpFoundation\Request;
use WebFactory\Bundle\UserBundle\Entity\User;
use GuzzleHttp\Exception\GuzzleException;

class GottaCenterApi implements GottaCenterApiInterface
{
    /**
     * @var GenericProvider
     */
    private $provider;
    /**
     * @var string
     */
    private $server;

    /**
     * @var LoggerInterface
     */
    private $logger;

    /**
     * GottaCenterApi constructor.
     * @param GenericProvider $provider
     * @param $server
     */
    public function __construct(GenericProvider $provider, $server)
    {
        $this->provider = $provider;
        $this->server = $server;
    }

    /**
     * @param LoggerInterface $logger
     */
    public function setLogger($logger)
    {
        $this->logger = $logger;
    }

    /**
     * @param $service
     * @return string
     */
    private function getUrl($service)
    {
        return $this->server . $service;
    }

    /**
     * @param AccessToken $accessToken
     * @param $method
     * @param $path
     * @param array $body
     * @return mixed
     * @throws GuzzleException
     */
    private function doRequest(AccessToken $accessToken, $method, $path, array $body = [])
    {
        $options = ['body' => \GuzzleHttp\json_encode($body)];
        if ($method !== Request::METHOD_GET) {
            $options['headers'] = [
                'Content-type' => 'application/json'
            ];
        }
        $request = $this->provider->getAuthenticatedRequest(
            $method,
            $this->getUrl($path),
            $accessToken,
            $options
        );

        //NOTE: 3 días para encontrar este bug!
        //      Al parecer necesita ser reinicializado en server
        $this->provider->setHttpClient(new Client());
        try {
            $json = $this->provider->getHttpClient()->send($request)->getBody()->getContents();
            $r = \GuzzleHttp\json_decode($json, true);

            return $r;
        } catch (GuzzleException $e) {
            if ($this->logger) {
                $this->logger->critical($e->getMessage());
            }

            throw $e;
        }

    }

    /**
     * @param $username
     * @param $password
     * @return AccessToken
     */
    public function getNewAccessToken($username, $password)
    {
        return $this->provider->getAccessToken('password', [
            'username' => $username,
            'password' => $password,
        ]);
    }

    /**
     * @param AccessToken $accessToken
     * @return array
     * @throws GuzzleException
     */
    public function getMe(AccessToken $accessToken)
    {
        return $this->doRequest($accessToken, 'GET', '/api/user');
    }

    /**
     * @param $email
     * @param AccessToken $accessToken
     * @return array
     * @throws GuzzleException
     */
    public function getUser($email, AccessToken $accessToken)
    {
        return $this->doRequest($accessToken, 'GET', '/api/user/' . $email);
    }

    /**
     * @param User $user
     * @param AccessToken $accessToken
     * @return bool
     * @throws GuzzleException
     */
    public function createUser(User $user, AccessToken $accessToken)
    {
        $userArray = [
            'first_name' => $user->getProfile()->getFirstName(),
            'last_name' => $user->getProfile()->getLastName(),
            'username' => $user->getUsername(),
            'email' => $user->getEmail(),
            'password' => $user->getPlainPassword(),
            'locked' => $user->isLocked(),
            'expired' => $user->isExpired(),
            'roles' => $user->getRoles(),
            'credentials_expired' => $user->isCredentialsExpired(),
            'enabled' => $user->isEnabled(),
            'salt' => $user->getSalt(),
        ];

        return $this->doRequest($accessToken, 'POST', '/api/user', $userArray);
    }

    /**
     * @param User $user
     * @param AccessToken $accessToken
     * @return bool
     * @throws GuzzleException
     */
    public function updateUser(User $user, AccessToken $accessToken)
    {
        $userArray = [
            'first_name' => $user->getProfile()->getFirstName(),
            'last_name' => $user->getProfile()->getLastName(),
            'username' => $user->getUsername(),
            'email' => $user->getEmail(),
            'password' => $user->getPlainPassword(),
            'locked' => $user->isLocked(),
            'expired' => $user->isExpired(),
            'roles' => $user->getRoles(),
            'credentials_expired' => $user->isCredentialsExpired(),
            'enabled' => $user->isEnabled(),
            'salt' => $user->getSalt(),
        ];

        return $this->doRequest($accessToken, 'PUT', '/api/user/' . $user->getEmail(), $userArray);
    }

//    /**
//     * @param $email
//     * @param AccessToken $accessToken
//     * @return array
//     * @throws GuzzleException
//     */
//    public function deleteUser($email, AccessToken $accessToken)
//    {
//        return $this->doRequest($accessToken, 'DELETE', '/api/user/' . $email);
//    }

    /**
     * @param $email
     * @param AccessToken $accessToken
     * @return array
     * @throws GuzzleException
     */
    public function getServicesStatus($email, AccessToken $accessToken)
    {
        return $this->doRequest($accessToken, 'GET', '/api/services/user/' . $email);
    }

    /**
     * @param $name
     * @param $email
     * @param AccessToken $accessToken
     * @return array
     * @throws GuzzleException
     */
    public function getServiceStatusByName($name, $email, AccessToken $accessToken)
    {
        return $this->doRequest($accessToken, 'GET', '/api/services/' . $name .  '/'. $email);
    }

    /**
     * @inheritdoc
     * @throws GuzzleException
     */
    public function getServiceStatusByPlatform($platform, $email, AccessToken $accessToken)
    {
        $endpoint = sprintf('/api/services/%s/platform/%s', $email, $platform);

        return $this->doRequest($accessToken, 'GET', $endpoint);
    }

    /**
     * @param $email
     * @param $encodedPassword
     * @return mixed
     * @throws GuzzleException
     */
    public function changePassword($email, $encodedPassword)
    {
        $body = [
            'encoded_password' => $encodedPassword,
        ];

        return $this->doAnonymousRequest('POST', '/api/user/change-password/' . $email, $body);

    }

    /**
     * @param $method
     * @param $path
     * @param array $body
     * @return mixed
     * @throws GuzzleException
     */
    private function doAnonymousRequest($method, $path, array $body = [])
    {
        $options = ['body' => \GuzzleHttp\json_encode($body)];
        if ($method === 'POST') {
            $options['headers'] = [
                'Content-type' => 'application/json'
            ];
        }
        $request = $this->provider->getRequest(
            $method,
            $this->getUrl($path),
            $options
        );

        try {
            $json = $this->provider->getHttpClient()->send($request)->getBody()->getContents();
            $r = \GuzzleHttp\json_decode($json, true);

            return $r;
        } catch (GuzzleException $e) {
            if ($this->logger) {
                $this->logger->critical($e->getMessage());
            }

            throw $e;
        }
    }
}
