<?php

namespace WebFactory\Bundle\UserBundle\Service;

use GuzzleHttp\Exception\GuzzleException;
use League\OAuth2\Client\Token\AccessToken;
use WebFactory\Bundle\UserBundle\Entity\User;

interface GottaCenterApiInterface
{
    /**
     * @param $username
     * @param $password
     * @return AccessToken
     */
    public function getNewAccessToken($username, $password);

    /**
     * @param AccessToken $accessToken
     * @return array
     * @throws GuzzleException
     */
    public function getMe(AccessToken $accessToken);

    /**
     * @param $email
     * @param AccessToken $accessToken
     * @return array
     * @throws GuzzleException
     */
    public function getUser($email, AccessToken $accessToken);

    /**
     * @param User $user
     * @param AccessToken $accessToken
     * @return bool
     * @throws GuzzleException
     */
    public function createUser(User $user, AccessToken $accessToken);

    /**
     * @param User $user
     * @param AccessToken $accessToken
     * @return bool
     */
    public function updateUser(User $user, AccessToken $accessToken);

    /**
     * @param $email
     * @param AccessToken $accessToken
     * @return array
     * @throws GuzzleException
     */
    public function getServicesStatus($email, AccessToken $accessToken);

    /**
     * @param $name
     * @param $email
     * @param AccessToken $accessToken
     * @return array
     * @throws GuzzleException
     */
    public function getServiceStatusByName($name, $email, AccessToken $accessToken);

    /**
     * @param $platform
     * @param $email
     * @param $accessToken
     * @return array
     * @throws GuzzleException
     */
    public function getServiceStatusByPlatform($platform, $email, AccessToken $accessToken);

    /**
     * @param $email
     * @param $encodedPassword
     * @return mixed
     */
    public function changePassword($email, $encodedPassword);
}
