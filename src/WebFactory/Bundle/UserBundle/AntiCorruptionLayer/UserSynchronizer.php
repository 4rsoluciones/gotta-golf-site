<?php

namespace WebFactory\Bundle\UserBundle\AntiCorruptionLayer;

use Doctrine\ORM\EntityManager;
use FOS\UserBundle\Model\UserManagerInterface;
use GuzzleHttp\Exception\GuzzleException;
use League\OAuth2\Client\Token\AccessToken;
use WebFactory\Bundle\UserBundle\Entity\Profile;
use WebFactory\Bundle\UserBundle\Entity\User;
use WebFactory\Bundle\UserBundle\Service\GottaCenterApi;

/**
 * Class UserSynchronizer
 * @package WebFactory\Bundle\UserBundle\AntiCorruptionLayer
 *
 * @note NO TIENE USO AHORA
 * Es la capa intermedia entre dominio de Center y Site
 */
class UserSynchronizer
{
    /**
     * @var GottaCenterApi
     */
    private $api;

    /**
     * @var UserManagerInterface
     */
    private $userManager;

    /**
     * @param $email
     * @param AccessToken $accessToken
     * @return User|null
     */
    public function getUserByEmailFromCenter($email, AccessToken $accessToken)
    {
        $userArray = $this->getCenterUserArrayByEmail($email, $accessToken);
        $translatedUser = $userArray && $userArray['success'] ? self::centerUserArrayToResourceUserArray($userArray) : null;
        $user = $translatedUser ? $this->getUserEntityFromArray($translatedUser) : null;

        return $user;
    }

    /**
     * @param $data
     * @return User
     */
    private function getUserEntityFromArray($data)
    {
        /** @var User $user */
        $user = $this->userManager->findUserByEmail($data['email']);
        if (!$user) {
            $user = $this->userManager->findUserByUsername($data['email']);
        }
        if (!$user) {
            $user = $this->userManager->createUser();
        }

        if (!$user->getProfile()) {
            $profile = new Profile();
            $user->setProfile($profile);
        }

        $user->setPassword($data['password']);
        $user->setUsername($data['username']);
        $user->setEmail($data['email']);

        $user->setRoles($data['roles']);
        $user->setSalt($data['salt']);
        $user->setEnabled($data['enabled']);
        $user->setLocked(false);
        $user->setExpired(false);
        $user->setCredentialsExpired(false);
        if (!empty($data['accessToken'])) {
            $user->setAccessToken($data['accessToken']);
            $this->checkRoles($data['roles']);//todo: esto es un problema
        }
        $user->setCreatedAt($data['createdAt']);
        $user->setUpdatedAt($data['updatedAt']);

        $profile = $user->getProfile();
        $profile->setBirthDate($data['profile']['birth_date']);
        $profile->setGender($data['profile']['gender']);
        $profile->setMobile($data['profile']['mobile']);
        $profile->setFirstName($data['profile']['first_name']);
        $profile->setLastName($data['profile']['last_name']);
        /** @var EntityManager $em */
        $em = $this->userManager->getEntityManager();
        $profile->setState($em->getReference('WebFactoryLocationBundle:State', $data['profile']['state']));
        $profile->setCity($data['profile']['city']);
        $profile->setCompanyName($data['profile']['company_name']);
        $this->userManager->updateUser($user);

        return $user;
    }

    /**
     * @param $data
     * @param AccessToken|null $accessToken
     * @return array
     *
     * @note Este paso es realmente necesario?
     */
    public static function centerUserArrayToResourceUserArray($data, AccessToken $accessToken = null)
    {
        $user = $data['user'];

        return [
            'username' => $user['username'],
            'email' => $user['email'],

            'password' => $user['password'],
            'salt' => $user['salt'],

            'enabled' => $user['profile']['enabled'],
            'roles' => unserialize($user['profile']['roles']),
            'accessToken' => $accessToken,


            'createdAt' => new \DateTime($user['created_at']),
            'updatedAt' => new \DateTime($user['updated_at']),

            'profile' => [
                'first_name' => $user['profile']['first_name'],
                'last_name' => $user['profile']['last_name'],
                'state' => $user['profile']['state_id'],
                'city' => $user['profile']['city'],
                'company_name' => $user['profile']['company_name'],
                'gender' => $user['profile']['gender'],
                'mobile' => $user['profile']['mobile_phonenumber'],
                'birth_date' => $user['profile']['birth_date'] ? new \DateTime($user['profile']['birth_date']) : null,
            ],
        ];
    }

    /**
     * @param $email
     * @param AccessToken $accessToken
     * @return array|null
     */
    private function getCenterUserArrayByEmail($email, AccessToken $accessToken)
    {
        try {
            $userArray = $this->api->getUser($email, $accessToken);
        } catch (GuzzleException $e) {
            $userArray = null;
        }

        return $userArray;
    }
}