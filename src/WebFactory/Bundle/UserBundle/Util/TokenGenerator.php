<?php

namespace WebFactory\Bundle\UserBundle\Util;


use FOS\UserBundle\Util\TokenGenerator as BaseTokenGenerator;

class TokenGenerator extends BaseTokenGenerator
{
    public function generateToken()
    {
        $token = parent::generateToken();

        return substr($token, 0, 6);
    }
}
