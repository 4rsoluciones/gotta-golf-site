<?php

namespace WebFactory\Bundle\UserBundle\Listener;

use WebFactory\Bundle\UserBundle\Entity as Entity;
use Doctrine\ORM\Event\LifecycleEventArgs;

/**
 * Role Setter Listener
 */
class RoleSetterListener
{

    /**
     * Asigna roles si corresponde
     *
     * @param LifecycleEventArgs $args evento
     * @return null
     */
    public function prePersist(LifecycleEventArgs $args)
    {
        $entity = $args->getEntity();

        if (!$entity instanceof Entity\User) {
            return;
        }

        $roles = $entity->getRoles();

        if (empty($roles) || (count($roles) === 1 && $roles[0] === 'ROLE_USER')) {
            // Por defecto es un jugador
            $entity->setRoles(array('ROLE_PLAYER'));
        }
    }

}