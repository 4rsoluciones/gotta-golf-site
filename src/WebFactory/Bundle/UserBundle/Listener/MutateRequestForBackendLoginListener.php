<?php

namespace WebFactory\Bundle\UserBundle\Listener;

use Symfony\Component\HttpKernel\Event\GetResponseEvent;

class MutateRequestForBackendLoginListener
{

    public function mutate(GetResponseEvent $event)
    {
        $request = $event->getRequest();
        if (!$request->query->has('username') || !$request->query->has('password')) {
            return;
        }

        $request->setMethod('POST');
        $request->request->set('_username', $_POST['_username'] = $request->query->get('username'));
        $request->request->set('_password', $_POST['_password'] = $request->query->get('password'));
    }

}