<?php

namespace WebFactory\Bundle\UserBundle\Form;

use FOS\UserBundle\Form\Type\ProfileFormType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolverInterface;

/**
*
*/
class PublicUserType extends ProfileFormType
{

	public function buildForm(FormBuilderInterface $builder, array $options)
    {
//    	parent::buildForm($builder, $options);
        $builder
                ->add('profile', new ProfileType(), array(
                    'show_legend' => false,
                ))
        ;
    }

    public function setDefaultOptions(OptionsResolverInterface $resolver)
    {
        $resolver->setDefaults(array(
            'data_class' => 'WebFactory\Bundle\UserBundle\Entity\User',
            'validation_groups' => array('Profile', 'Default'),
        ));
    }

    public function getName()
    {
        return 'webfactory_user_profile_form';
    }
}