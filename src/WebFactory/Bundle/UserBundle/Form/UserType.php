<?php

namespace WebFactory\Bundle\UserBundle\Form;

use AndresGotta\Academy\Domain\ValueObject\Sport;
use Doctrine\ORM\EntityRepository;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\CallbackTransformer;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\Form\FormEvent;
use Symfony\Component\Form\FormEvents;
use Symfony\Component\Form\FormInterface;
use Symfony\Component\OptionsResolver\OptionsResolverInterface;
use WebFactory\Bundle\UserBundle\Entity\User;

class UserType extends AbstractType
{

    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $sports = $options['sports'];
        $this->addSportField($builder, $sports);
        $builder->addEventListener(FormEvents::PRE_SET_DATA, function(FormEvent $event) {
            $user = $event->getData();
            $form = $event->getForm();
            if ($user === null) {
                return null;
            }

            if (!$user->getId()) {
                return;
            }


            if ($user->hasRole('ROLE_ACADEMY')) {
                $form
                    ->add('academyName')
                    ->add('academyMaxStudents')
                    ->add('academyMaxTeachers')
                ;

                $this->addAcademyTeachersField($form);
                $this->addAcademyStudentsField($form);
                $this->addAcademyPlacesField($form);
            } else {
                $form->remove('sport');
            }

            if ($user->hasRole('ROLE_PLAYER')) {
                $form
                    ->add('profile', new OtherUserProfileType(), [
                        'label' => false,
                    ])
                ;
            }
        });
    }

    public function setDefaultOptions(OptionsResolverInterface $resolver)
    {
        $resolver->setDefaults(array(
            'data_class' => 'WebFactory\Bundle\UserBundle\Entity\User',
            'cascade_validation' => true,
            'validation_groups' => function(FormInterface $form) {
                $data = $form->getData();

                $groups = [
                    'Default',
                ];

                if ($data->hasRole('ROLE_ACADEMY')) {
                    $groups[] = 'Academy';
                }

                if ($data->getId()) {
                    $groups[] = 'Profile';

                    return $groups;
                }

                $groups[] = 'Registration';

                return $groups;
            },
            'attr' => ['novalidate' => true, 'class' => 'form-horizontal'],
            'userRole' => null,
            'sports' => array(),
        ));
    }

    /**
     * @return string
     */
    public function getName()
    {
        return 'user';
    }



    /**
     * @param FormBuilderInterface $builder
     */
    protected function addAcademyTeachersField(FormInterface $builder)
    {
        $builder
            ->add('academyTeachers', 'entity', [
                'class' => User::class,
                'query_builder' => function (EntityRepository $er) {
                    return $er->createQueryBuilder('Teacher')
                        ->where('Teacher.roles LIKE :role')->setParameter('role', '%ROLE_TEACHER%');
                },
                'required' => false,
                'multiple' => true,
            ]);
    }

    /**
     * @param FormInterface $builder
     */
    protected function addAcademyStudentsField(FormInterface $builder)
    {
        $builder
            ->add('academyStudents', 'entity', [
                'class' => User::class,
                'query_builder' => function (EntityRepository $er) {
                    return $er->createQueryBuilder('Student')
                        ->where('Student.roles LIKE :role')->setParameter('role', '%ROLE_STUDENT%');
                },
                'required' => false,
                'multiple' => true,
            ]);
    }

    /**
     * @param FormInterface $builder
     */
    protected function addAcademyPlacesField(FormInterface $builder)
    {
        $builder->add('academyPlaces', 'collection', [
            'type' => new AcademyPlaceType(),
            'allow_add' => true,
            'allow_delete' => true,
            'error_bubbling' => false,
            'prototype' => true,
            'by_reference' => false,
            'widget_add_btn' => array(
                'label' => 'Add',
                'icon' => 'plus-sign icon-white',
                'attr' => array(
                    'class' => 'btn btn-primary'
                )
            ),
            'options' => array(
                'widget_remove_btn' => array(
                    'label' => '',
                    'attr' => array('class' => 'btn btn-danger'),
                    'icon' => 'remove icon-white',
                ),

                'attr' => array('class' => 'span3'),
                'widget_addon' => array(
                    'type' => 'prepend',
                    'text' => '@',
                ),
                'widget_control_group' => false,
                'label' => false,
                'error_bubbling' => false,
            ),
            'label_attr' => array('class' => 'visibilityHidden')
        ]);
    }

    /**
     * @param FormBuilderInterface $builder
     * @param $sports
     */
    protected function addSportField(FormBuilderInterface $builder, $sports)
    {
        $builder->add('sport', 'choice', [
            'choices' => array_combine($sports, $sports),
        ]);
        $builder->get('sport')
            ->addModelTransformer(new CallbackTransformer(function ($vo) {
                if (!$vo) {
                    return null;
                }

                return $vo->getName();
            }, function ($str) {
                if (!$str) {
                    return null;
                }

                return new Sport($str);
            }));
    }

}
