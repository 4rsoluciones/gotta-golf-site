<?php

namespace WebFactory\Bundle\UserBundle\Form;

use AndresGotta\Academy\Domain\Entity\AcademyPlace;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\Form\FormInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;

class AcademyPlaceType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('name')
            ->add('address');
    }

    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults(array(
            'data_class' => AcademyPlace::class,
            'empty_data' => function (FormInterface $form) {
                $academy = $form->getParent()->getParent()->getData();
                $name = $form->get('name')->getData();
                $address = $form->get('address')->getData();

                return new AcademyPlace($academy, $name, $address);
            },
        ));
    }


    public function getName()
    {
        return $this->getBlockPrefix();
    }


    public function getBlockPrefix()
    {
        return 'academy_place';
    }
}
