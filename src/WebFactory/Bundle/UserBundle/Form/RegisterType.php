<?php

namespace WebFactory\Bundle\UserBundle\Form;

use FOS\UserBundle\Form\Type\RegistrationFormType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolverInterface;
use Symfony\Component\Validator\Constraints\NotBlank;

/**
 * Class RegisterType
 * @package WebFactory\Bundle\UserBundle\Form
 */
class RegisterType extends RegistrationFormType
{
    /**
     * @inheritdoc
     */
	public function buildForm(FormBuilderInterface $builder, array $options)
    {
    	parent::buildForm($builder, $options);
        $builder
            ->add('profile', new ProfileType(), array(
                'show_legend' => false
            ))
            ->add('tos', 'checkbox', array(
                'required' => true,
                'mapped' => false,
                'widget_checkbox_label' => 'label',
                'constraints' => array(
                    new NotBlank(),
                )
            ))
            ->add('newsletter_subscription', 'checkbox', array(
                'required' => false,
                'mapped' => false,
                'widget_checkbox_label' => 'label',
            ))
        ;
    }

    /**
     * @param OptionsResolverInterface $resolver
     */
    public function setDefaultOptions(OptionsResolverInterface $resolver)
    {
        $resolver->setDefaults(array(
            'data_class' => 'WebFactory\Bundle\UserBundle\Entity\User',
            'attr' => ['novalidate' => true, 'class' => 'fos_user_registration_register']
        ));
    }

    /**
     * @return string
     */
    public function getName()
    {
        return 'webfactory_user_registration_form';
    }
}