<?php

namespace WebFactory\Bundle\UserBundle\Form;

use AndresGotta\Bundle\GolfBundle\ValueObject\Handicap;
use AndresGotta\Bundle\GolfBundle\ValueObject\PlayerCondition;
use FOS\UserBundle\Model\UserInterface;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\Form\FormEvent;
use Symfony\Component\Form\FormEvents;
use Symfony\Component\OptionsResolver\OptionsResolverInterface;
use WebFactory\Bundle\LocationBundle\Form\SimpleLocationType;
use WebFactory\Bundle\UserBundle\Entity\Profile;
use WebFactory\Bundle\UserBundle\ValueObject\Gender;

class ProfileType extends AbstractType
{

    public function buildForm(FormBuilderInterface $builder, array $options)
    {
//        parent::buildForm($builder, $options);
        $builder
//                ->add('city')
//                ->add('firstName')
//                ->add('lastName')
//                ->add('companyName')
//                ->add('birthDate', 'genemu_jquerydate', array(
//                    'widget' => 'single_text',
//                    'format' => 'd/M/y',
//                    'attr' => array(
//                        'data-date-format' => 'dd/mm/yyyy',
//                    )
//                ))
//                ->add('gender', 'choice', array(
//                    'choices' => Gender::getOptions(),
//                ))
//                ->add('mobile')
                ->add('condition', 'choice', array(
                    'choices' => Profile::getConditionChoices(),
                ))
                
                ->add('handicap', 'choice', array(
                    'choices' => array_reverse(Handicap::getChoices(), true)
                ))
//            ->add('plainPassword', 'repeated', array(
//                'required' => false,
//                'type' => 'password',
//                'invalid_message' => 'The password fields must match.',
//                'first_options' => array(
//                    'label' => 'New password',
//                    'attr' => array(
//                        'autocomplete' => 'off',
//                    )
//                ),
//                'second_options' => array(
//                    'label' => 'Repeat password',
//                    'attr' => array(
//                        'autocomplete' => 'off',
//                    )
//                ),
//                'property_path' => 'user.plainPassword'
//            ));
        ;
        $builder->addEventListener(
            FormEvents::PRE_SET_DATA, function(FormEvent $event) {
                $data = $event->getData();
                $form = $event->getForm();
                if ($data === NULL) {
                    return NULL;
                }
                
                if ($data->getId()) {
                    $form->add('club', 'text', array(
                    'required' => false,
                    ));
                }

                /** @var UserInterface $user */
                $user = $data->getUser();
                if (in_array('ROLE_ACADEMY', $user->getRoles())) {
                    $form
                        ->add('academyName', null, [
                            'property_path' => 'user.academyName'
                        ])
                        ->add('academyMaxStudents', null, [
                            'property_path' => 'user.academyMaxStudents'
                        ])
                        ->add('academyMaxTeachers', null, [
                            'property_path' => 'user.academyMaxTeachers'
                        ])
                    ;
                }
        });
    }

    public function setDefaultOptions(OptionsResolverInterface $resolver)
    {
        $resolver->setDefaults(array(
            'data_class' => 'WebFactory\Bundle\UserBundle\Entity\Profile',
        ));
    }

    public function getName()
    {
        return 'profile';
    }

}
