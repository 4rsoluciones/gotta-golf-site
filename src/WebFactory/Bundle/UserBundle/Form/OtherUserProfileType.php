<?php

namespace WebFactory\Bundle\UserBundle\Form;

use AndresGotta\Bundle\GolfBundle\ValueObject\Handicap;
use AndresGotta\Bundle\GolfBundle\ValueObject\PlayerCondition;
use FOS\UserBundle\Model\UserInterface;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\Form\FormEvent;
use Symfony\Component\Form\FormEvents;
use Symfony\Component\OptionsResolver\OptionsResolverInterface;
use WebFactory\Bundle\UserBundle\Entity\Profile;

class OtherUserProfileType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder->addEventListener(
            FormEvents::PRE_SET_DATA, function (FormEvent $event) {
            $data = $event->getData();
            $form = $event->getForm();
            if ($data === null) {
                return null;
            }

            /** @var UserInterface $user */
            $user = $data->getUser();
            if ($user->hasRole('ROLE_PLAYER')) {
                $form
                    ->add('condition', 'choice', array(
                        'choices' => Profile::getConditionChoices(),
                    ))
                    ->add('handicap', 'choice', array(
                        'choices' => array_reverse(Handicap::getChoices(), true)
                    ))

                ;
                if ($data->getId()) {
                    $form->add('club', 'text', array(
                        'required' => false,
                    ));
                }
            }
        });


    }

    public function setDefaultOptions(OptionsResolverInterface $resolver)
    {
        $resolver->setDefaults(array(
            'data_class' => 'WebFactory\Bundle\UserBundle\Entity\Profile',
            'myProfile' => false,
        ));
    }

    public function getName()
    {
        return 'profile';
    }
}
