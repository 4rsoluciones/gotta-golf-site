<?php

namespace WebFactory\Bundle\UserBundle\Model;

use FOS\UserBundle\Entity\UserManager as BaseUserManager;
use FOS\UserBundle\Model\UserInterface;
use WebFactory\Bundle\UserBundle\Entity\User;

class UserManager extends BaseUserManager
{
    public function findUserByCriteria($criteria)
    {
        $collection = $this->repository->matching($criteria);

        return $collection->first();
    }
    
    public function getCanonicalizedEmail($email)
    {
        return parent::canonicalizeEmail($email);
    }

    public function getCanonicalizedUsername($username)
    {
        return parent::canonicalizeUsername($username);
    }

    public function updateUser(UserInterface $user, $andFlush = true)
    {
        if (in_array('ROLE_ACADEMY', $user->getRoles()) && $user->getAcademyPlaces()->isEmpty()) {
            $user->addAcademyPlace($user->getAcademyName());
        }

        parent::updateUser($user, $andFlush);
    }

    /**
     * @param User $academy
     * @param User $user
     */
    public function addToAcademy(User $academy, User $user)
    {
        if (!in_array('ROLE_ACADEMY', $academy->getRoles())) {
            return;
        }

        if (in_array('ROLE_STUDENT', $user->getRoles()) && !$academy->hasAcademyStudent($user)) {
            $academy->addAcademyStudent($user);
        }

        if (in_array('ROLE_TEACHER', $user->getRoles()) && !$academy->hasAcademyStudent($user)) {
            $academy->addAcademyTeacher($user);
        }
    }

    public function getEntityManager()
    {
        return $this->em;
    }
}