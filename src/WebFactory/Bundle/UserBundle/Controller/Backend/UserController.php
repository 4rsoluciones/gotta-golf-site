<?php

namespace WebFactory\Bundle\UserBundle\Controller\Backend;

use Doctrine\ORM\QueryBuilder;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Template;
use Symfony\Component\HttpFoundation\Response;
use WebFactory\Bundle\UserBundle\Entity\User;
use WebFactory\Bundle\UserBundle\Form\UserType;
use APY\DataGridBundle\Grid\Source\Entity;
use APY\DataGridBundle\Grid\Action\RowAction;

/**
 * User controller.
 *
 * @Route("/user")
 */
class UserController extends Controller
{

    /**
     * Lists all User entities.
     *
     * @Route("/", name="backend_user")
     * @Method("GET|POST")
     * @Template()
     */
    public function indexAction()
    {
        $grid = $this->getGrid();
        $grid->setId('main');
        $grid->setHiddenColumns(array('id'));
        $grid->setDefaultOrder('id', 'asc');

//        $editRowAction = new RowAction('Edit', 'backend_user_edit', false, '_self', array('class' => 'btn-action glyphicons pencil btn-success'));
//        $grid->addRowAction($editRowAction);
        $deleteRowAction = new RowAction('Delete', 'backend_user_delete', true, '_self', array('class' => 'btn-action glyphicons remove_2 btn-danger'));
        $grid->addRowAction($deleteRowAction);

        $rowAction = new RowAction('CustomPrice', 'backend_custom_price_edit', false, '_self', array('class' => 'btn-action glyphicons calculator btn-success'));
        $rowAction->manipulateRender(function ($action, $row) {
                $roles = $row->getField('roles');

                if (in_array('ROLE_CUSTOMER', $roles) || in_array('ROLE_PLAYER', $roles) || in_array('ROLE_STUDENT', $roles)) {
                    return $action;
                }

                return null;
            });

        $rowAction = new RowAction('Academy\'s Places', 'andres_gotta_academy_backend_academy_places', true, '_self', array('class' => 'btn-action glyphicons home btn-info'));
        $rowAction->manipulateRender(function ($action, $row) {
                $roles = $row->getField('roles');

                if (in_array('ROLE_ACADEMY', $roles)) {
                    return $action;
                }

                return null;
            });
        $grid->addRowAction($rowAction);

        $rowAction = new RowAction('Academy\'s teachers and students', 'andres_gotta_academy_backend_academy_teachers_and_students', true, '_self', array('class' => 'btn-action glyphicons group btn-info'));
        $rowAction->manipulateRender(function ($action, $row) {
                $roles = $row->getField('roles');

                if (in_array('ROLE_ACADEMY', $roles)) {
                    return $action;
                }

                return null;
            });
        $grid->addRowAction($rowAction);

        $rowAction = new RowAction('Student\'s academies', 'andres_gotta_academy_backend_students_academies', true, '_self', array('class' => 'btn-action glyphicons home btn-info'));
        $rowAction->manipulateRender(function ($action, $row) {
                $roles = $row->getField('roles');

                if (in_array('ROLE_STUDENT', $roles)) {
                    return $action;
                }

                return null;
            });
        $grid->addRowAction($rowAction);

        $rowAction = new RowAction('Teacher\'s academies', 'andres_gotta_academy_backend_teachers_academies', true, '_self', array('class' => 'btn-action glyphicons home btn-info'));
        $rowAction->manipulateRender(function ($action, $row) {
                $roles = $row->getField('roles');

                if (in_array('ROLE_TEACHER', $roles)) {
                    return $action;
                }

                return null;
            });
        $grid->addRowAction($rowAction);
        
        return $grid->getGridResponse('WebFactoryUserBundle:Backend/User:index.html.twig');
    }

    /**
     * Creates a new User entity.
     *
     * @Route("/new", name="backend_user_create")
     * @Method("POST")
     * @Template("WebFactoryUserBundle:Backend/User:new.html.twig")
     */
    public function createAction(Request $request)
    {
        $entity = new User();
        $form = $this->createUserForm($entity);
        $form->bind($request);

        if ($form->isValid()) {
            $manager = $this->get('fos_user.user_manager');

            $manager->addToAcademy($this->getUser(), $entity);
            $manager->updateUser($entity);
            $this->get('fos_user.mailer')->sendWelcomeUser($entity);
            
            $request->getSession()->getFlashBag()->add('success', 'flash.message.generic.created');

            return $this->redirect($this->generateUrl('backend_user_edit', array('id' => $entity->getId())));
        }

        return array(
            'entity' => $entity,
            'form' => $form->createView(),
        );
    }

    /**
     * Displays a form to create a new User entity.
     *
     * @Route("/new", name="backend_user_new")
     * @Method("GET")
     * @Template()
     */
    public function newAction()
    {
        $entity = new User();
        $form = $this->createUserForm($entity);

        return array(
            'entity' => $entity,
            'form' => $form->createView(),
        );
    }

    /**
     * Displays a form to edit an existing User entity.
     *
     * @Route("/{email}/edit", name="backend_user_edit")
     * @Method("GET")
     * @Template()
     */
    public function editAction(Request $request, $email)
    {
        $jsonUser = $this->get('web_factory_user.gotta_center_api')->getUser($email, $this->getUser()->getAccessToken());
        if (!$jsonUser['success']) {
            throw $this->createNotFoundException();
        }

        $translatedUser = $this->get('webfactory.user_provider.backend')->translateLaravelUserJson($jsonUser);
        $entity = $this->get('webfactory.user_provider.backend')->getUserEntityFromArray($translatedUser);

        /** @var User $user */
        $user = $this->getUser();

        if ($user === $entity) {
            return $this->createFormCompletedResponse();
//            throw $this->createAccessDeniedException();
        }
        if (in_array('ROLE_ACADEMY', $user->getRoles()) && !($user->hasAcademyStudent($entity) || ($user->hasAcademyTeacher($entity)))) {
            return $this->createFormCompletedResponse();
//            throw $this->createAccessDeniedException();
        };

        $editForm = $this->createUserForm($entity);

        return array(
            'entity' => $entity,
            'edit_form' => $editForm->createView(),
        );
    }

    /**
     * Edits an existing User entity.
     *
     * @Route("/{id}", name="backend_user_update")
     * @Method("PUT")
     * @Template("WebFactoryUserBundle:Backend/User:edit.html.twig")
     */
    public function updateAction(Request $request, User $entity)
    {
        $deleteForm = $this->createDeleteForm($entity->getId());
        $editForm = $this->createUserForm($entity);
        $editForm->bind($request);

        if ($editForm->isValid()) {
            $this->get('fos_user.user_manager')->updateUser($entity);

//            $request->getSession()->getFlashBag()->add('success', 'flash.message.generic.updated');

//            return $this->redirect($this->generateUrl('backend_user_edit', array('email' => $entity->getEmail(), 'ok' => 'yes')));

            return $this->createFormCompletedResponse();
        }

        return array(
            'entity' => $entity,
            'edit_form' => $editForm->createView(),
            'delete_form' => $deleteForm->createView(),
        );
    }

    /**
     * Deletes a User entity.
     *
     * @Route("/{id}/delete", name="backend_user_delete")
     * @Method("DELETE|GET")
     */
    public function deleteAction(Request $request, User $entity)
    {
        $form = $this->createDeleteForm($entity->getId());

        $form->bind($request);

        if ($form->isValid()) {
            $em = $this->getDoctrine()->getManager();
            $em->remove($entity);
            $em->flush();
            
            $request->getSession()->getFlashBag()->add('success', 'flash.message.generic.deleted');
        }

        return $this->redirect($this->generateUrl('backend_user'));
    }

    /**
     * Creates a form to delete a User entity by id.
     *
     * @param mixed $id The entity id
     *
     * @return \Symfony\Component\Form\Form The form
     */
    private function createDeleteForm($id)
    {
        return $this->createFormBuilder(array('id' => $id), array('csrf_protection' => false))
                        ->add('id', 'hidden')
                        ->getForm()
        ;
    }

    /**
     * @param User $entity
     * @return \Symfony\Component\Form\Form
     */
    private function createUserForm(User $entity)
    {
        $role = null;
        if ($this->isGranted('ROLE_ADMIN')) {
            $role = 'ROLE_ADMIN';
        } elseif ($this->isGranted('ROLE_ACADEMY')) {
            $role = 'ROLE_ACADEMY';
        }

        $sports = $this->getParameter('sports');

        $form = $this->createForm(new UserType(), $entity, ['userRole' => $role, 'sports' => $sports]);

        return $form;
    }

    /**
     * @return \APY\DataGridBundle\Grid\Grid
     */
    private function getGrid()
    {
        $group = 'academy';
        if ($this->isGranted('ROLE_ADMIN')) {
            $group = 'default';
        }

        $source = new Entity('WebFactoryUserBundle:User', $group);

        $alias = $source->getTableAlias();
        $user = $this->getUser();

        $source->manipulateQuery(function (QueryBuilder $queryBuilder) use ($user, $alias, $group) {
            if ('default' === $group) {
                return;
            }

            $queryBuilder
                ->leftJoin("{$alias}.academyStudents", 'AcademyStudent')
                ->leftJoin("{$alias}.academyTeachers", 'AcademyTeacher')
                ->andWhere(
                    "({$alias}.roles LIKE :role_student AND :user MEMBER OF {$alias}.enrolledInAcademies) OR
                    ({$alias}.roles LIKE :role_teacher AND :user MEMBER OF {$alias}.teachesInAcademies)"
                )
                ->setParameter('role_student', '%ROLE_STUDENT%')
                ->setParameter('role_teacher', '%ROLE_TEACHER%')
                ->setParameter('user', $user)
            ;
        });

        /* @var $grid \APY\DataGridBundle\Grid\Grid */
        $grid = $this->get('grid');
        $grid->setSource($source);

        return $grid;
    }

    /**
     *
     */
    private function createFormCompletedResponse()
    {
        return Response::create('<script>parent.postMessage("formCompleted","*");</script>');
    }
}
