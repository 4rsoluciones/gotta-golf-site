<?php

namespace WebFactory\Bundle\UserBundle\Controller\Backend;

use FOS\UserBundle\Controller\SecurityController as BaseController;
use Symfony\Component\HttpFoundation\RedirectResponse;

/**
 * {@inheritDoc}
 */
class SecurityController extends BaseController
{
    /**
     * {@inheritDoc}
     */
    public function renderLogin(array $data)
    {
        if ($token = $this->container->get('security.token_storage')->getToken()) {
            if (is_object($token->getUser())) {
                return RedirectResponse::create($this->container->get('router')->generate('backend_dashboard'));
            }
        }

        $template = sprintf('WebFactoryUserBundle:Backend/Security:login.html.twig');

        return $this->container->get('templating')->renderResponse($template, $data);
    }
}