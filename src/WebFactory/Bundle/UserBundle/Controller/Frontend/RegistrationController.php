<?php

namespace WebFactory\Bundle\UserBundle\Controller\Frontend;

use FOS\UserBundle\Controller\RegistrationController as BaseController;
use Symfony\Component\HttpFoundation\RedirectResponse;
use Symfony\Component\HttpKernel\Exception\NotFoundHttpException;
use FOS\UserBundle\Model\UserInterface;
use WebFactory\Bundle\UserBundle\Event\FormEvent;

/**
 * {@inheritDoc}
 */
class RegistrationController extends BaseController
{

    /**
     * {@inheritDoc}
     */
    public function registerAction()
    {
        $form = $this->container->get('fos_user.registration.form');
        $formHandler = $this->container->get('fos_user.registration.form.handler');
        $confirmationEnabled = $this->container->getParameter('fos_user.registration.confirmation.enabled');

        $process = $formHandler->process($confirmationEnabled);
        if ($process) {
            $user = $form->getData();

            $authUser = false;
            if ($confirmationEnabled) {
                $this->container->get('session')->set('fos_user_send_confirmation_email/email', $user->getEmail());
                $route = 'frontend_fos_user_registration_check_email';
            } else {
                $authUser = true;
                $route = 'frontend_fos_user_registration_confirmed';
            }

            $this->setFlash('fos_user_success', 'registration.flash.user_created');
            $url = $this->container->get('router')->generate($route);
            $response = new RedirectResponse($url);

            if ($authUser) {
                $this->authenticateUser($user, $response);
            }

            /** @var $dispatcher \Symfony\Component\EventDispatcher\EventDispatcherInterface */
            $dispatcher = $this->container->get('event_dispatcher');
            $dispatcher->dispatch('fos_user.registration.success', new FormEvent($form));

            return $response;
        }

        return $this->container->get('templating')->renderResponse(
            'WebFactoryUserBundle:Frontend/Registration:register.html.' . $this->getEngine(), array(
            'form' => $form->createView(),
        ));
    }

    /**
     * {@inheritDoc}
     */
    public function checkEmailAction()
    {
        $email = $this->container->get('session')->get('fos_user_send_confirmation_email/email');
        $this->container->get('session')->remove('fos_user_send_confirmation_email/email');
        $user = $this->container->get('fos_user.user_manager')->findUserByEmail($email);

        if (null === $user) {
            throw new NotFoundHttpException(sprintf('The user with email "%s" does not exist', $email));
        }

        return $this->container->get('templating')->renderResponse('WebFactoryUserBundle:Frontend/Registration:checkEmail.html.' . $this->getEngine(), array(
                    'user' => $user,
        ));
    }

    /**
     * {@inheritDoc}
     */
    public function confirmAction($token)
    {
        //afuera
        return new RedirectResponse($this->container->getParameter('gotta_center_api.server') . '/register/confirm/' . $token);

//        $user = $this->container->get('fos_user.user_manager')->findUserByConfirmationToken($token);
//
//        if (null === $user) {
//            throw new NotFoundHttpException(sprintf('The user with confirmation token "%s" does not exist', $token));
//        }
//
//        $user->setConfirmationToken(null);
//        $user->setEnabled(true);
//        $user->setLastLogin(new \DateTime());
//
//        $this->container->get('fos_user.user_manager')->updateUser($user);
//        $response = new RedirectResponse($this->container->get('router')->generate('frontend_fos_user_registration_confirmed'));
//        $this->authenticateUser($user, $response);
//
//        return $response;
    }

    /**
     * {@inheritDoc}
     */
    public function confirmedAction()
    {
        $user = $this->container->get('security.context')->getToken()->getUser();
        if (!is_object($user) || !$user instanceof UserInterface) {
            throw new AccessDeniedException('This user does not have access to this section.');
        }

        return $this->container->get('templating')->renderResponse('WebFactoryUserBundle:Frontend/Registration:confirmed.html.' . $this->getEngine(), array(
                    'user' => $user,
        ));
    }

}
