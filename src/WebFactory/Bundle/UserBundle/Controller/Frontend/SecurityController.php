<?php

namespace WebFactory\Bundle\UserBundle\Controller\Frontend;

use FOS\UserBundle\Controller\SecurityController as BaseController;

/**
 * {@inheritDoc}
 */
class SecurityController extends BaseController
{
    /**
     * {@inheritDoc}
     */
    public function renderLogin(array $data)
    {
        $template = sprintf('WebFactoryUserBundle:Frontend/Security:login.html.twig');

        return $this->container->get('templating')->renderResponse($template, $data);
    }
}