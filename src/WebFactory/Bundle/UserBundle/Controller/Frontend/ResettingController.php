<?php

namespace WebFactory\Bundle\UserBundle\Controller\Frontend;

use FOS\UserBundle\Controller\ResettingController as BaseController;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpFoundation\RedirectResponse;
use Symfony\Component\HttpKernel\Exception\NotFoundHttpException;
use Symfony\Component\Security\Core\Exception\AccountStatusException;
use FOS\UserBundle\Model\UserInterface;

/**
 * {@inheritDoc}
 */
class ResettingController extends BaseController
{

    const SESSION_EMAIL = 'fos_user_send_resetting_email/email';

    /**
     * {@inheritDoc}
     */
    public function requestAction()
    {
        return $this->container->get('templating')->renderResponse('WebFactoryUserBundle:Frontend/Resetting:request.html.' . $this->getEngine());
    }

    /**
     * {@inheritDoc}
     */
    public function sendEmailAction()
    {
        $username = $this->container->get('request')->request->get('username');

        /** @var $user UserInterface */
        $user = $this->container->get('fos_user.user_manager')->findUserByUsernameOrEmail($username);

        if (null === $user) {
            return $this->container->get('templating')->renderResponse('WebFactoryUserBundle:Frontend/Resetting:request.html.' . $this->getEngine(), array('invalid_username' => $username));
        }

        if ($user->isPasswordRequestNonExpired($this->container->getParameter('fos_user.resetting.token_ttl'))) {
            return $this->container->get('templating')->renderResponse('WebFactoryUserBundle:Frontend/Resetting:passwordAlreadyRequested.html.' . $this->getEngine());
        }

        if (null === $user->getConfirmationToken()) {
            /** @var $tokenGenerator \FOS\UserBundle\Util\TokenGeneratorInterface */
            $tokenGenerator = $this->container->get('fos_user.util.token_generator');
            $user->setConfirmationToken($tokenGenerator->generateToken());
        }

        $this->container->get('session')->set(static::SESSION_EMAIL, $this->getObfuscatedEmail($user));
        $this->container->get('fos_user.mailer')->sendResettingEmailMessage($user);
        $user->setPasswordRequestedAt(new \DateTime());
        $this->container->get('fos_user.user_manager')->updateUser($user);

        return new RedirectResponse($this->container->get('router')->generate('frontend_fos_user_resetting_check_email'));
    }

    /**
     * {@inheritDoc}
     */
    public function checkEmailAction()
    {
        $session = $this->container->get('session');
        $email = $session->get(static::SESSION_EMAIL);
        $session->remove(static::SESSION_EMAIL);

        if (empty($email)) {
            // the user does not come from the sendEmail action
            return new RedirectResponse($this->container->get('router')->generate('frontend_fos_user_resetting_request'));
        }

        return $this->container->get('templating')->renderResponse('WebFactoryUserBundle:Frontend/Resetting:checkEmail.html.' . $this->getEngine(), array(
                    'email' => $email,
        ));
    }

    /**
     * {@inheritDoc}
     */
    public function resetAction($token)
    {
        return new RedirectResponse($this->container->getParameter('gotta_center_api.server') . '/resetting/reset/' . $token);
//        $user = $this->container->get('fos_user.user_manager')->findUserByConfirmationToken($token);
//
//        if (null === $user) {
//            throw new NotFoundHttpException(sprintf('The user with "confirmation token" does not exist for value "%s"', $token));
//        }
//
//        if (!$user->isPasswordRequestNonExpired($this->container->getParameter('fos_user.resetting.token_ttl'))) {
//            return new RedirectResponse($this->container->get('router')->generate('frontend_fos_user_resetting_request'));
//        }
//
//        $form = $this->container->get('fos_user.resetting.form');
//        $formHandler = $this->container->get('fos_user.resetting.form.handler');
//        $process = $formHandler->process($user);
//
//        if ($process) {
//            $this->setFlash('fos_user_success', 'resetting.flash.success');
//            $response = new RedirectResponse($this->getRedirectionUrl($user));
//            $this->authenticateUser($user, $response);
//
//            return $response;
//        }
//
//        return $this->container->get('templating')->renderResponse('WebFactoryUserBundle:Frontend/Resetting:reset.html.' . $this->getEngine(), array(
//                    'token' => $token,
//                    'form' => $form->createView(),
//        ));
    }

    /**
     * {@inheritDoc}
     */
    protected function authenticateUser(UserInterface $user, Response $response)
    {
        try {
            $this->container->get('fos_user.security.login_manager')->loginUser(
                    $this->container->getParameter('fos_user.firewall_name'), $user, $response);
        } catch (AccountStatusException $ex) {
            // We simply do not authenticate users which do not pass the user
            // checker (not enabled, expired, etc.).
        }
    }

    /**
     * {@inheritDoc}
     */
    protected function getRedirectionUrl(UserInterface $user)
    {
        return $this->container->get('router')->generate('frontend_fos_user_profile_show');
    }

}