<?php

namespace WebFactory\Bundle\UserBundle\DataFixtures\ORM;

use Closure;
use DateTime;
use Doctrine\Common\DataFixtures\AbstractFixture;
use Doctrine\Common\DataFixtures\OrderedFixtureInterface;
use Doctrine\Common\Persistence\ObjectManager;
use WebFactory\Bundle\UserBundle\Entity\Customer;
use WebFactory\Bundle\UserBundle\Entity\GroupAdmin;
use WebFactory\Bundle\UserBundle\Entity\Player;
use WebFactory\Bundle\UserBundle\Entity\PlayerProfile;
use WebFactory\Bundle\UserBundle\Entity\Profile;
use WebFactory\Bundle\UserBundle\Entity\Student;
use WebFactory\Bundle\UserBundle\Entity\StudentProfile;
use WebFactory\Bundle\UserBundle\Entity\User;

class LoadUsers extends AbstractFixture implements OrderedFixtureInterface
{

    private $manager;
    
    /**
     * {@inheritDoc}
     */
    public function getOrder()
    {
        return 1;
    }

    /**
     * @return mixed
     */
    public function getManager()
    {
        return $this->manager;
    }

    /**
     * @param mixed $manager
     */
    public function setManager($manager)
    {
        $this->manager = $manager;
    }

    /**
     * {@inheritDoc}
     */
    public function load(ObjectManager $manager)
    {
        $academy = $this->getReference('academy-academy');

        $this->setManager($manager);
        $teacher = $this->createTeacher();
        $teacher->addTeachesInAcademy($academy);
        $manager->persist($teacher);
        $this->addReference('user-teacher', $teacher);

        $customer = $this->createCustomer();
        $manager->persist($customer);
        $this->addReference('user-customer', $customer);

        $player = $this->createPlayer();
        $manager->persist($player);
        $this->addReference('user-player', $player);

        $student = $this->createStudent();
        $student->addEnrolledInAcademy($academy);
        $manager->persist($student);
        $this->addReference('user-student', $student);

        $secretary = $this->createSecretary();
        $manager->persist($secretary);

        $groupAdmin = $this->createGroupAdmin();
        $manager->persist($groupAdmin);


        $superAdmin = $this->createSuperAdmin();
        $manager->persist($superAdmin);

        $manager->flush();
    }

    protected function createUser(User $user, $username, $email, $password, $roles)
    {
        $user->setUsername($username);
        $user->setEmail($email);
        $user->setPlainPassword($password);

        foreach ($roles as $role) {
            $user->addRole($role);
        }

        $user->setEnabled(true);

        $profile = new Profile();

        $profile->setFirstName($username);
        $profile->setLastName($username);
        $profile->setGender('male');
        $profile->setMobile(null);
        $profile->setBirthDate(new DateTime('-18 year'));
        $profile->setState($this->getManager()->getRepository('WebFactoryLocationBundle:State')->findOneBy([]));
        $profile->setCity('santa fe');
        $profile->setHandicap(rand(1, 20));
        $profile->setCondition('player.professional');

        $user->setProfile($profile);

        return $user;
    }

    protected function createTeacher()
    {
        $teacher = $this->createUser(new User(), 'teacher', 'teacher@example.com', 'teacher', array(User::ROLE_GROUP_OWNER, 'ROLE_TEACHER'));

        return $teacher;
    }

    protected function createCustomer()
    {
        $customer = $this->createUser(new User(), 'customer', 'customer@example.com', 'customer', array('ROLE_CUSTOMER'));

        return $customer;
    }

    protected function createPlayer()
    {
        $player = $this->createUser(new User(), 'player', 'player@example.com', 'player', array('ROLE_PLAYER'));

        return $player;
    }

    protected function createStudent()
    {
        $student = $this->createUser(new User(), 'student', 'student@example.com', 'student', array('ROLE_STUDENT'));

        return $student;
    }

    protected function createSecretary()
    {
        $secretary = $this->createUser(new User(), 'secretary', 'secretary@example.com', 'secretary', array('ROLE_SECRETARY'));

        return $secretary;
    }

    protected function createGroupAdmin()
    {
        $student = $this->createUser(new User(), 'group_admin', 'group_admin@example.com', 'group_admin', array('ROLE_GROUP_ADMIN'));

        return $student;
    }

    protected function createSuperAdmin()
    {
        $student = $this->createUser(new User(), 'admin', 'super_admin@example.com', 'master', array('ROLE_SUPER_ADMIN'));

        return $student;
    }

}