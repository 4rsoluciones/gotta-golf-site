<?php

namespace WebFactory\Bundle\UserBundle\Mailer;

use FOS\UserBundle\Model\UserInterface;
use FOS\UserBundle\Mailer\TwigSwiftMailer as BaseMailer;

/**
 * Class Mailer
 * @package WebFactory\Bundle\UserBundle\Mailer
 */
class Mailer extends BaseMailer
{
    /**
     * @param UserInterface $user
     */
    public function sendConfirmationEmailMessage(UserInterface $user)
    {
        $template = $this->parameters['template']['confirmation'];
        $url = $this->router->generate('frontend_fos_user_registration_confirm', array('token' => $user->getConfirmationToken()), true);
        $context = array(
            'user' => $user,
            'confirmationUrl' => $url
        );

        $this->sendMessage($template, $context, $this->parameters['from_email']['confirmation'], $user->getEmail());
    }

    /**
     * @param UserInterface $user
     */
    public function sendResettingEmailMessage(UserInterface $user)
    {
        $template = $this->parameters['template']['resetting'];
        $url = $this->router->generate('frontend_fos_user_resetting_reset', array('token' => $user->getConfirmationToken()), true);
        $context = array(
            'user' => $user,
            'confirmationUrl' => $url,
            'token' => $user->getConfirmationToken(),
        );
        $this->sendMessage($template, $context, $this->parameters['from_email']['resetting'], $user->getEmail());
    }

    public function sendFeedbackMail(UserInterface $user, $message)
    {
        $template = 'WebFactoryUserBundle:Frontend:email.html.twig';
        $context = array(
            'user' => $user,
            'message' => $message
        );

        $this->sendMessage($template, $context, $user->getEmail(), $this->parameters['from_email']['confirmation']);
    }

    /**
     * @param UserInterface $user
     */
    public function sendWelcomeUser(UserInterface $user)
    {
        if (!($user->hasRole('ROLE_STUDENT') || $user->hasRole('ROLE_TEACHER'))) {
            return;
        }

        $template = 'WebFactoryUserBundle:Backend:User/email/welcome.html.twig';
        $context = [
            'user' => $user
        ];

        $this->sendMessage($template, $context, $this->parameters['from_email']['confirmation'], $user->getEmail());
    }
}
