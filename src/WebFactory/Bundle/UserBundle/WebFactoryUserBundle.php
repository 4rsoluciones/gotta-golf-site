<?php

namespace WebFactory\Bundle\UserBundle;

use Symfony\Component\DependencyInjection\ContainerBuilder;
use Symfony\Component\HttpKernel\Bundle\Bundle;
use WebFactory\Bundle\UserBundle\Security\Factory\OAuth2LoginFactory;

class WebFactoryUserBundle extends Bundle
{
    public function build(ContainerBuilder $container)
    {
        parent::build($container);
        $extension = $container->getExtension('security');
        $extension->addSecurityListenerFactory(new OAuth2LoginFactory());
    }

    public function getParent()
    {
        return 'FOSUserBundle';
    }
}
