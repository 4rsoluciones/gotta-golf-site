<?php

namespace WebFactory\Bundle\UserBundle\Security;

use FOS\UserBundle\Security\UserProvider;
use Doctrine\Common\Collections\Criteria;
use WebFactory\Bundle\UserBundle\Entity\User;

class FrontendDatabaseProvider extends UserProvider
{

    /**
     * @inherit
     */
    protected function findUser($username)
    {
        $array = array(
            serialize(array('ROLE_PLAYER')),
            serialize(array('ROLE_STUDENT')),
            serialize(array('ROLE_CUSTOMER')),
            serialize(array(User::ROLE_GROUP_OWNER)),
        );

        $criteria = new Criteria;
        $criteria->where($criteria->expr()->in('roles', $array));

        $orx = $criteria->expr()->orX(
                $criteria->expr()->eq('usernameCanonical', $this->userManager->getCanonicalizedUsername($username)),
                $criteria->expr()->eq('emailCanonical', $this->userManager->getCanonicalizedEmail($username))
        );

        $criteria->andWhere($orx);

        return $this->userManager->findUserByCriteria($criteria);
    }

}