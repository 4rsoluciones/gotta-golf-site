<?php

namespace WebFactory\Bundle\UserBundle\Security;

use Symfony\Component\Security\Core\Exception\UsernameNotFoundException;

class APIProvider extends GottaCenterProvider
{

    /**
     * @param $roles
     */
    protected function checkRoles($roles)
    {
        $allowed = ['ROLE_TEACHER', 'ROLE_STUDENT'];
        foreach ($roles as $role) {
            if (in_array($role, $allowed)) {
                return;
            }
        }

        throw new UsernameNotFoundException();
    }

}
