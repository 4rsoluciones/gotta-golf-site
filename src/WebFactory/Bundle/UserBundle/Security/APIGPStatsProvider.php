<?php

namespace WebFactory\Bundle\UserBundle\Security;

use League\OAuth2\Client\Token\AccessToken;
use Symfony\Component\Security\Core\Exception\UsernameNotFoundException;

class APIGPStatsProvider extends GottaCenterProvider
{

    /**
     * @inherit
     */
    protected function findUser($username)
    {
        if (!$username) {
            return;
        }

        $accessToken = $this->createTokenFromUsername($username);
        $data = $this->query($accessToken);
        if ($data) {
            $user = $this->getUserEntityFromArray($data);

            return $user;
        }
    }

    private function query($accessToken)
    {
        try {
            $r = $this->api->getMe($accessToken);

            return $this->translateLaravelUserJson($r, $accessToken);

        } catch (\Exception $e) {
            if ($this->logger) {
                $this->logger->critical($e->getMessage());
            }
        }
    }

    /**
     * @param $roles
     */
    protected function checkRoles($roles)
    {
        $allowed = ['ROLE_PLAYER'];
        foreach ($roles as $role) {
            if (in_array($role, $allowed)) {
                return;
            }
        }

        throw new UsernameNotFoundException();
    }

    /**
     * @param string $username
     * @return AccessToken
     */
    private function createTokenFromUsername($username)
    {
        $options = [
            'access_token' => $username,
        ];

        return new AccessToken($options);
    }

}
