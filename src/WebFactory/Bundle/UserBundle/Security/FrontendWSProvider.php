<?php

namespace WebFactory\Bundle\UserBundle\Security;

use Symfony\Component\Security\Core\Exception\UsernameNotFoundException;
use WebFactory\Bundle\UserBundle\Entity\User;


class FrontendWSProvider extends GottaCenterProvider
{

    /**
     * @param $roles
     */
    protected function checkRoles($roles)
    {
        $allowed = ['ROLE_PLAYER', 'ROLE_STUDENT', 'ROLE_CUSTOMER', User::ROLE_GROUP_OWNER];
        foreach ($roles as $role) {
            if (in_array($role, $allowed)) {
                return;
            }
        }

        throw new UsernameNotFoundException;
    }

}
