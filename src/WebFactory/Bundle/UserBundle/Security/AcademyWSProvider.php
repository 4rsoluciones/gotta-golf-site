<?php

namespace WebFactory\Bundle\UserBundle\Security;

use Symfony\Component\Security\Core\Exception\UsernameNotFoundException;
use WebFactory\Bundle\UserBundle\Entity\User;


class AcademyWSProvider extends GottaCenterProvider
{

    /**
     * @param $roles
     */
    protected function checkRoles($roles)
    {
        $allowed = ['ROLE_ACADEMY'];
        foreach ($roles as $role) {
            if (in_array($role, $allowed)) {
                return;
            }
        }

        throw new UsernameNotFoundException;
    }

}
