<?php

namespace WebFactory\Bundle\UserBundle\Security;


use Symfony\Component\Security\Core\Exception\UsernameNotFoundException;

class BackendProvider extends GottaCenterProvider
{

    /**
     * @param $roles
     */
    protected function checkRoles($roles)
    {
        $allowed = ['ROLE_ADMIN', /*'ROLE_ACADEMY', */'ROLE_SECRETARY', 'ROLE_SUPER_ADMIN'];
        foreach ($roles as $role) {
            if (in_array($role, $allowed)) {
                return;
            }
        }

        throw new UsernameNotFoundException();
    }

}