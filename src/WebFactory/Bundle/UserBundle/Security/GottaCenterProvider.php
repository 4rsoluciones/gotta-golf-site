<?php

namespace WebFactory\Bundle\UserBundle\Security;


use Doctrine\ORM\EntityManager;
use FOS\UserBundle\Model\UserManagerInterface;
use FOS\UserBundle\Security\UserProvider;
use League\OAuth2\Client\Provider\Exception\IdentityProviderException;
use Psr\Log\LoggerInterface;
use Symfony\Component\HttpFoundation\RequestStack;
use Symfony\Component\Security\Core\Exception\UsernameNotFoundException;
use WebFactory\Bundle\UserBundle\Entity\Profile;
use WebFactory\Bundle\UserBundle\Entity\User;
use WebFactory\Bundle\UserBundle\Service\GottaCenterApiInterface;


abstract class GottaCenterProvider extends UserProvider
{
    /**
     * @var GottaCenterApiInterface
     */
    protected $api;
    /**
     * @var RequestStack
     */
    protected $stack;

    /**
     * @var LoggerInterface
     */
    protected $logger;

    /**
     * FrontendWSProvider constructor.
     * @param UserManagerInterface $userManager
     * @param GottaCenterApiInterface $api
     * @param RequestStack $stack
     */
    public function __construct(UserManagerInterface $userManager, GottaCenterApiInterface $api, RequestStack $stack)
    {
        parent::__construct($userManager);
        $this->api = $api;
        $this->stack = $stack;
    }

    /**
     * @param LoggerInterface $logger
     */
    public function setLogger($logger)
    {
        $this->logger = $logger;
    }

    /**
     * @inherit
     */
    protected function findUser($username)
    {
        $request = $this->stack->getCurrentRequest();
        if (!$request) {
            return;
        }

        if ($request->server->get("PHP_AUTH_USER")) {
            $username = $request->server->get("PHP_AUTH_USER");
            $password = $request->server->get("PHP_AUTH_PW");
        } else {
            $username = $request->request->get('_username');
            $password = $request->request->get('_password');
        }
        $data = $this->query($username, $password);
        if ($data) {
            $user = $this->getUserEntityFromArray($data);

            return $user;
        }
    }

    private function query($_username, $_password)
    {
        try {
            $accessToken = $this->api->getNewAccessToken($_username, $_password);
            $r = $this->api->getMe($accessToken);

            return $this->translateLaravelUserJson($r, $accessToken);

        } catch (\Exception $e) {
            if ($this->logger) {
                $this->logger->critical($e->getMessage());
            }
        }
    }

    /**
     * @param $data
     * @return \FOS\UserBundle\Model\UserInterface
     */
    public function getUserEntityFromArray($data)
    {
        /** @var User $user */
        $user = $this->userManager->findUserByEmail($data['email']);
        if (!$user) {
            $user = $this->userManager->findUserByUsername($data['email']);
            if (!$user) {
                $user = $this->userManager->createUser();
            }
        }

        if (!$user->getProfile()) {
            $profile = new Profile();
            $user->setProfile($profile);
        }

        $user->setPassword($data['password']);
        $user->setUsername($data['username']);
        $user->setEmail($data['email']);

        $user->setRoles($data['roles']);
        $user->setSalt($data['salt']);
        $user->setEnabled($data['enabled']);
        $user->setLocked(false);
        $user->setExpired(false);
        $user->setCredentialsExpired(false);
        if (!empty($data['accessToken'])) {
            $user->setAccessToken($data['accessToken']);
            $this->checkRoles($data['roles']);
        }

        if ($user->getCreatedAt() != $data['createdAt']) {
            $user->setCreatedAt($data['createdAt']);
        }

        if ($user->getUpdatedAt() != $data['updatedAt']) {
            $user->setUpdatedAt($data['updatedAt']);
        }

        $profile = $user->getProfile();
        if ($profile->getBirthDate() != $data['profile']['birth_date']) {
            $profile->setBirthDate($data['profile']['birth_date']);
        }

        $profile->setGender($data['profile']['gender']);
        $profile->setMobile($data['profile']['mobile']);
        $profile->setFirstName($data['profile']['first_name']);
        $profile->setLastName($data['profile']['last_name']);
        /** @var EntityManager $em */
        $em = $this->userManager->getEntityManager();
        $profile->setState($em->getReference('WebFactoryLocationBundle:State', $data['profile']['state']));
        $profile->setCity($data['profile']['city']);
        $profile->setCompanyName($data['profile']['company_name']);
        $this->userManager->updateUser($user);

        return $user;
    }

    /**
     * @param $roles
     */
    abstract protected function checkRoles($roles);

    /**
     * @param $r
     * @param $accessToken
     * @return array
     */
    public function translateLaravelUserJson($r, $accessToken = null)
    {
        $user = $r['success'];
        $profile = $user['profile'];
        return [
            'username' => $user['username'],
            'email' => $user['email'],

            'password' => $user['password'],
            'salt' => $user['salt'],

            'enabled' => $profile['enabled'],
            'roles' => unserialize($profile['roles']),
            'accessToken' => $accessToken,

            'createdAt' => new \DateTime($user['created_at']),
            'updatedAt' => new \DateTime($user['updated_at']),
//                'lastLogin' => $r['success']['last_login'] ? new \DateTime($r['success']['profile']['last_login']) : null,
            'profile' => [
                'first_name' => $profile['first_name'],
                'last_name' => $profile['last_name'],
                'state' => $profile['state_id'],
                'city' => $profile['city'],
                'company_name' => $profile['company_name'],
                'gender' => $profile['gender'],
                'mobile' => $profile['mobile_phonenumber'],
                'birth_date' => $profile['birth_date'] ? new \DateTime($profile['birth_date']) : null,
            ],
        ];
    }

}

