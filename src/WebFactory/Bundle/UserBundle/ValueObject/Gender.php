<?php

namespace WebFactory\Bundle\UserBundle\ValueObject;

/**
 * Class Gender
 * @package WebFactory\Bundle\UserBundle\ValueObject
 */
class Gender
{
    const MALE = 'male';
    const FEMALE = 'female';

    /**
     * @return array
     */
    public static function getOptions()
    {
        return [
            self::MALE => self::MALE,
            self::FEMALE => self::FEMALE,
        ];
    }
}