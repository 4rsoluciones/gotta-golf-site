<?php

namespace WebFactory\Bundle\UserBundle\Manager;

use AndresGotta\Bundle\GolfBundle\Entity\Hole;
use AndresGotta\Bundle\GolfBundle\Entity\Round;
use AndresGotta\Bundle\GolfBundle\ValueObject\RoundType;
use WebFactory\Bundle\UserBundle\Entity\Profile;

/**
 * Class ScoreManager
 * @package WebFactory\Bundle\UserBundle\Manager
 */
class ScoreManager
{
    /**
     * @param Round[] $rounds
     * @return float|int
     */
    public function calculate(array $rounds)
    {
        /**
         * el score de un hoyo es el total de golpes + golpes de penalidad
         * el score de una vuelta, es el total de los 18 hoyos
         * el promedio de score que muestra ese reporte, es el promedio del score de las vueltas
         */

        $strikes = 0;
        $totalRounds = count($rounds);
        foreach ($rounds as $round) {
            $strikes += $round->getStrikesCount();
        }
        $score = $totalRounds == 0 ? 0 : $strikes / $totalRounds;

        return number_format($score, 2);
    }
}