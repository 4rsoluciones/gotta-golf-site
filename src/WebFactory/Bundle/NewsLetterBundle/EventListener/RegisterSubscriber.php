<?php

namespace WebFactory\Bundle\NewsLetterBundle\EventListener;

use Symfony\Component\EventDispatcher\EventSubscriberInterface;
use Doctrine\ORM\EntityManager;
use WebFactory\Bundle\NewsLetterBundle\Entity\Subscription;
use Symfony\Component\Validator\Validator;
use WebFactory\Bundle\UserBundle\Event\FormEvent;

/**
 * Subscribe al newsletter al usuario que se registra
 */
class RegisterSubscriber implements EventSubscriberInterface
{
    /**
     * Entity Manager
     * @var EntityManager
     */
    private $em;

    /**
     * Validador
     * @var Validator
     */
    private $validator;

    /**
     * RegisterSubscriber constructor.
     * @param EntityManager $em
     * @param Validator\RecursiveValidator $validator
     */
    public function __construct(EntityManager $em, Validator\RecursiveValidator $validator)
    {
        $this->em = $em;
        $this->validator = $validator;
    }

    /**
     * @inherit
     */
    public static function getSubscribedEvents()
    {
        return array(
            'fos_user.registration.success' => array('onRegisterSubmitted', -5)
        );
    }

    /**
     * Subscribe al usuario si asi lo ha solicitado
     *
     * @param  FormEvent $event
     */
    public function onRegisterSubmitted(FormEvent $event)
    {

        $user = $event->getForm()->getData();

        $newsletterSubscription = $event->getForm()->get('newsletter_subscription')->getData();

        if ($newsletterSubscription) {
            $subscription = new Subscription($user->getProfile()->getFirstName() . ' ' . $user->getProfile()->getLastName(), $user->getEmail());

            $errors = $this->validator->validate($subscription);
            if (count($errors) === 0) {
                $this->em->persist($subscription);
                $this->em->flush($subscription);
            }
        }
    }
}