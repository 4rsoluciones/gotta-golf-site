<?php

namespace WebFactory\Bundle\NewsLetterBundle\EventListener;


use Symfony\Component\EventDispatcher\EventSubscriberInterface;
use Symfony\Component\Validator\Validator\ValidatorInterface;
use WebFactory\Bundle\ContactBundle\Event\ContactEvents;
use WebFactory\Bundle\ContactBundle\Event\ContactEvent;
use Doctrine\ORM\EntityManager;
use WebFactory\Bundle\NewsLetterBundle\Entity\Subscription;



class ContactSubscriber implements EventSubscriberInterface
{
    private $em;
    private $validator;

    public function __construct(EntityManager $em, ValidatorInterface $validator)
    {
        $this->em = $em;
        $this->validator = $validator;
    }

    public static function getSubscribedEvents()
    {
        return array(
            ContactEvents::SUBMITTED => array('onContactSubmitted', -5)
        );
    }

    public function onContactSubmitted(ContactEvent $event)
    {
        $contact = $event->getContact();

        if ($contact['subscribeToNewsLetter']) {
            $subscription = new Subscription($contact['name'], $contact['email']);

            $errors = $this->validator->validate($subscription);
            if (count($errors) === 0) {
                $this->em->persist($subscription);
                $this->em->flush($subscription);
            }
        }
    }
}