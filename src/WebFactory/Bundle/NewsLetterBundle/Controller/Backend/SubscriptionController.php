<?php

namespace WebFactory\Bundle\NewsLetterBundle\Controller\Backend;

use Symfony\Component\HttpFoundation\Request;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Template;
use WebFactory\Bundle\NewsLetterBundle\Entity\Subscription;
use WebFactory\Bundle\NewsLetterBundle\Form\BackendSubscriptionType as SubscriptionType;
use APY\DataGridBundle\Grid\Source\Entity;
use APY\DataGridBundle\Grid\Action\RowAction;
use APY\DataGridBundle\Grid\Export\CSVExport;

/**
 * Subscription controller.
 *
 * @Route("/subscription")
 */
class SubscriptionController extends Controller
{

    /**
     * Lists all News entities.
     *
     * @Route("/", name="backend_subscription")
     * @Method("GET|POST")
     * @Template()
     */
    public function indexAction()
    {
        /* @var $source \APY\DataGridBundle\Grid\Source\Entity */
        $source = new Entity('WebFactoryNewsLetterBundle:Subscription');
        /* @var $grid \APY\DataGridBundle\Grid\Grid */
        $grid = $this->get('grid');
        $grid->setSource($source);
        $grid->setId('main');
        $grid->setHiddenColumns(array('id'));
        $grid->setDefaultOrder('id', 'asc');
        $grid->addExport(new CSVExport('Export', 'subscriptions-' . date('Y-m-d')));

        $editRowAction = new RowAction('Edit', 'backend_subscription_edit', false, '_self', array('class' => 'btn-action glyphicons pencil btn-success'));
        $grid->addRowAction($editRowAction);
        $deleteRowAction = new RowAction('Delete', 'backend_subscription_delete', true, '_self', array('class' => 'btn-action glyphicons remove_2 btn-danger'));
        $grid->addRowAction($deleteRowAction);

        return $grid->getGridResponse('WebFactoryNewsLetterBundle:Backend/Subscription:index.html.twig');
    }

     /**
     * Creates a new Subscription entity.
     *
     * @Route("/new", name="backend_subscription_create")
     * @Method("POST")
     * @Template("WebFactoryNewsLetterBundle:Backend/Subscription:new.html.twig")
     */
    public function createAction(Request $request)
    {
        $entity = new Subscription();
        $form = $this->createCreateForm($entity);
        $form->handleRequest($request);

        if ($form->isValid()) {
            $em = $this->getDoctrine()->getManager();
            $em->persist($entity);
            $em->flush();

            return $this->redirect($this->generateUrl('backend_subscription_edit', array('id' => $entity->getId())));
        }

        return array(
            'entity' => $entity,
            'form'   => $form->createView(),
        );
    }

    /**
    * Creates a form to create a Subscription entity.
    *
    * @param Subscription $entity The entity
    *
    * @return \Symfony\Component\Form\Form The form
    */
    private function createCreateForm(Subscription $entity)
    {
        $form = $this->createForm(new SubscriptionType(), $entity, array(
            'action' => $this->generateUrl('backend_subscription_create'),
            'method' => 'POST',
        ));

        $form->add('submit', 'submit', array(
            'label' => 'Create',
            'attr' => array(
                'class' => 'btn-primary'
            )
        ));

        return $form;
    }

    /**
     * Displays a form to create a new Subscription entity.
     *
     * @Route("/new", name="backend_subscription_new")
     * @Method("GET")
     * @Template()
     */
    public function newAction()
    {
        $entity = new Subscription();
        $form   = $this->createCreateForm($entity);

        return array(
            'entity' => $entity,
            'form'   => $form->createView(),
        );
    }

    /**
     * Displays a form to edit an existing Subscription entity.
     *
     * @Route("/{id}/edit", name="backend_subscription_edit")
     * @Method("GET")
     * @Template()
     */
    public function editAction($id)
    {
        $em = $this->getDoctrine()->getManager();

        $entity = $em->getRepository('WebFactoryNewsLetterBundle:Subscription')->find($id);

        if (!$entity) {
            throw $this->createNotFoundException('Unable to find Subscription entity.');
        }

        $editForm = $this->createEditForm($entity);
        $deleteForm = $this->createDeleteForm($id);

        return array(
            'entity'      => $entity,
            'edit_form'   => $editForm->createView(),
            'delete_form' => $deleteForm->createView(),
        );
    }

    /**
    * Creates a form to edit a Subscription entity.
    *
    * @param Subscription $entity The entity
    *
    * @return \Symfony\Component\Form\Form The form
    */
    private function createEditForm(Subscription $entity)
    {
        $form = $this->createForm(new SubscriptionType(), $entity, array(
            'action' => $this->generateUrl('backend_subscription_update', array('id' => $entity->getId())),
            'method' => 'PUT',
        ));

        $form->add('submit', 'submit', array('label' => 'Update'));

        return $form;
    }
    /**
     * Edits an existing Subscription entity.
     *
     * @Route("/{id}", name="backend_subscription_update")
     * @Method("PUT")
     * @Template("WebFactoryNewsLetterBundle:Backend/Subscription:edit.html.twig")
     */
    public function updateAction(Request $request, $id)
    {
        $em = $this->getDoctrine()->getManager();

        $entity = $em->getRepository('WebFactoryNewsLetterBundle:Subscription')->find($id);

        if (!$entity) {
            throw $this->createNotFoundException('Unable to find Subscription entity.');
        }

        $deleteForm = $this->createDeleteForm($id);
        $editForm = $this->createEditForm($entity);
        $editForm->handleRequest($request);

        if ($editForm->isValid()) {
            $em->flush();

            return $this->redirect($this->generateUrl('backend_subscription_edit', array('id' => $id)));
        }

        return array(
            'entity'      => $entity,
            'edit_form'   => $editForm->createView(),
            'delete_form' => $deleteForm->createView(),
        );
    }
    /**
     * Deletes a Subscription entity.
     *
     * @Route("/{id}/delete", name="backend_subscription_delete")
     * @Method({"DELETE","GET"})
     */
    public function deleteAction(Request $request, $id)
    {
        $valid = true;
        if ($request->getMethod() === 'DELETE') {
            $form = $this->createDeleteForm($id);
            $form->handleRequest($request);

            $valid = $form->isValid();
        }

        if ($valid) {
            $em = $this->getDoctrine()->getManager();
            $entity = $em->getRepository('WebFactoryNewsLetterBundle:Subscription')->find($id);

            if (!$entity) {
                throw $this->createNotFoundException('Unable to find Subscription entity.');
            }

            $em->remove($entity);
            $em->flush();
        }

        return $this->redirect($this->generateUrl('backend_subscription'));
    }

    /**
     * Creates a form to delete a Subscription entity by id.
     *
     * @param mixed $id The entity id
     *
     * @return \Symfony\Component\Form\Form The form
     */
    private function createDeleteForm($id)
    {
        return $this->createFormBuilder()
            ->setAction($this->generateUrl('backend_subscription_delete', array('id' => $id)))
            ->setMethod('DELETE')
            //->add('submit', 'submit', array('label' => 'Delete'))
            ->getForm()
        ;
    }
}
