<?php

namespace WebFactory\Bundle\NewsLetterBundle\Controller\Frontend;

use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Template;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;
use WebFactory\Bundle\NewsLetterBundle\Entity\Subscription;
use WebFactory\Bundle\NewsLetterBundle\Form\SubscriptionType;

/**
 * @Route("/subscription")
 */
class SubscriptionController extends Controller
{
    /**
     * @Route("/subscribe", name="frontend_newsletter_subscribe")
     * @Method({"GET","POST"})
     * @Template()
     */
    public function subscribeAction(Request $request)
    {
        $entity = new Subscription();
        $form   = $this->createForm(new SubscriptionType(), $entity);

        if($request->getMethod() === 'POST') {
            $form->bind($request);

            if ($form->isValid()) {
                $em = $this->getDoctrine()->getManager();
                $em->persist($entity);
                $em->flush();

                return $this->render('WebFactoryNewsLetterBundle:Frontend/Subscription:success_subscribe.html.twig');
            }
        }

        return array(
            'entity' => $entity,
            'form'   => $form->createView(),
        );
    }

    /**
     * @Route("/unsubscribe/{email}", name="frontend_newsletter_unsubscribe")
     * @Template()
     */
    public function unsubscribeAction($email = null)
    {
        $em = $this->getDoctrine()->getManager();

        $subscription = $em->getRepository('WebFactoryNewsLetterBundle:Subscription')->findOneBy(compact('email'));

        if($subscription) {
            $subscription->setStatus(Subscription::UNSUBSCRIBED);
            $em->flush();

            $message = 'subscription.unsubscribed.success';
        } else {
            $message = 'subscription.unsubscribed.fail';
        }

        return compact('message');
    }
}
