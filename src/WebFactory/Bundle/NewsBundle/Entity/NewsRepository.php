<?php

namespace WebFactory\Bundle\NewsBundle\Entity;

use WebFactory\Bundle\NewsBundle\Entity\Tag;
use WebFactory\Bundle\NewsBundle\Entity\Category;
use Doctrine\ORM\EntityRepository;
use Doctrine\ORM\Query\Expr\Join;

/**
 * NewsRepository
 *
 */
class NewsRepository extends EntityRepository
{

    /**
     * 
     * @param \WebFactory\Bundle\NewsBundle\Entity\Category $category
     * @param \WebFactory\Bundle\NewsBundle\Entity\Tag $tag
     * @return type
     */
    public function createQueryBuilderByCategoryAndTag(Category $category = null, Tag $tag = null)
    {
        $queryBuilder = $this->createQueryBuilder('News')
                ->addSelect('Image')
                ->addSelect('Comment')
                ->addSelect('User')
                ->addSelect('Profile')
                ->addSelect('Tag')
                ->leftJoin('News.mainImage', 'Image')
                ->leftJoin('News.tags', 'Tag')
                ->leftJoin('News.comments', 'Comment', Join::WITH, "Comment.status = 'published'")
                ->leftJoin('Comment.user', 'User')
                ->leftJoin('User.profile', 'Profile')
                ->where('News.active = true')
                ->orderBy('News.createdAt', 'DESC');

        if ($category) {
            $queryBuilder->andWhere('News.category = :category')
                    ->setParameter('category', $category->getId());
        }

        if ($tag) {
            $queryBuilder->innerJoin('News.tags', 'TagFilter')
                    ->andWhere('TagFilter = :tag')
                    ->setParameter('tag', $tag->getId());
        }

        return $queryBuilder;
    }

    /**
     * 
     * @param type $limit
     * @return type
     */
    public function findLastest($limit = 2)
    {
        $queryBuilder = $this->createQueryBuilder('News')
                ->addSelect('Image')
                ->addSelect('Comment')
                ->addSelect('User')
                ->addSelect('Profile')
                ->addSelect('Tag')
                ->leftJoin('News.mainImage', 'Image')
                ->leftJoin('News.tags', 'Tag')
                ->leftJoin('News.comments', 'Comment', Join::WITH, "Comment.status = 'published'")
                ->leftJoin('Comment.user', 'User')
                ->leftJoin('User.profile', 'Profile')
                ->orderBy('News.createdAt', 'DESC')
                ->where('News.highlighted = false');

        return $queryBuilder->setMaxResults($limit)->getQuery()->getResult();
    }

    /**
     * 
     * @param type $limit
     * @return type
     */
    public function findHighlighted($limit = 2)
    {
        $queryBuilder = $this->createQueryBuilder('News')
                ->addSelect('Image')
                ->addSelect('Comment')
                ->addSelect('User')
                ->addSelect('Profile')
                ->addSelect('Tag')
                ->leftJoin('News.mainImage', 'Image')
                ->leftJoin('News.tags', 'Tag')
                ->leftJoin('News.comments', 'Comment', Join::WITH, "Comment.status = 'published'")
                ->leftJoin('Comment.user', 'User')
                ->leftJoin('User.profile', 'Profile')
                ->orderBy('News.createdAt', 'DESC')
                ->where('News.highlighted = true');

        return $queryBuilder->setMaxResults($limit)->getQuery()->getResult();
    }

}
