<?php

namespace WebFactory\Bundle\NewsBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use Gedmo\Mapping\Annotation as Gedmo;
use Symfony\Component\Validator\Constraints as Assert;
use APY\DataGridBundle\Grid\Mapping as Grid;
use WebFactory\Bundle\UserBundle\Entity\User;

/**
 * Comment
 *
 * @ORM\Table(name="news_comments")
 * @ORM\Entity
 * @Grid\Source(columns="id, news.title, user.email, content, createdAt, status")
 */
class Comment
{
    /**
     * @var integer
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     * @Grid\Column(filterable=false)
     */
    protected $id;

    /**
     *
     * @var type
     * @ORM\ManyToOne(targetEntity="WebFactory\Bundle\NewsBundle\Entity\News", inversedBy="comments", cascade={"persist"}, fetch="LAZY")
     * @Grid\Column(title="News", field="news.title")
     */
    protected $news;

    /**
     *
     * @var \WebFactory\Bundle\UserBundle\Entity\User
     * @ORM\ManyToOne(targetEntity="WebFactory\Bundle\UserBundle\Entity\User", inversedBy="newsComments", cascade={"persist"}, fetch="LAZY")
     * @Assert\NotNull()
     * @Grid\Column(title="User", field="user.email")
     */
    protected $user;

    /**
     * @var string
     *
     * @ORM\Column(name="content", type="text")
     * @Assert\NotBlank()
     * @Grid\Column(title="Content")
     */
    protected $content;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="created_at", type="datetime")
     * @Gedmo\Timestampable(on="create")
     * @Grid\Column(title="Created At")
     */
    protected $createdAt;

    /**
     * @var string
     *
     * @ORM\Column(name="status", type="string", length=16)
     * @Grid\Column(title="Status")
     */
    protected $status = 'published';

    public function __construct(User $user = null, $content = null)
    {
        $this->setUser($user);
        $this->content = $content;
    }


    /**
     * Get id
     *
     * @return integer
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set content
     *
     * @param string $content
     * @return Comment
     */
    public function setContent($content)
    {
        $this->content = $content;

        return $this;
    }

    /**
     * Get content
     *
     * @return string
     */
    public function getContent()
    {
        return $this->content;
    }

    /**
     * Set createdAt
     *
     * @param \DateTime $createdAt
     * @return Comment
     */
    public function setCreatedAt($createdAt)
    {
        $this->createdAt = $createdAt;

        return $this;
    }

    /**
     * Get createdAt
     *
     * @return \DateTime
     */
    public function getCreatedAt()
    {
        return $this->createdAt;
    }

    /**
     * Set status
     *
     * @param string $status
     * @return Comment
     */
    public function setStatus($status)
    {
        $this->status = $status;

        return $this;
    }

    /**
     * Get status
     *
     * @return string
     */
    public function getStatus()
    {
        return $this->status;
    }

    public function getNews()
    {
        return $this->news;
    }

    public function setNews($news)
    {
        $this->news = $news;
    }

    public function getUser()
    {
        return $this->user;
    }

    public function setUser(User $user = null)
    {
        $this->user = $user;

        if ($user) {
            $user->addNewsComment($this);
        }
    }

    public function getCreatedAtDiff()
    {
        return date_diff(new \DateTime, $this->createdAt);
    }

}
