<?php

namespace WebFactory\Bundle\NewsBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use Gedmo\Mapping\Annotation as Gedmo;
use Symfony\Component\Validator\Constraints as Assert;
use APY\DataGridBundle\Grid\Mapping as Grid;
use Doctrine\Common\Collections\ArrayCollection;

/**
 * News
 *
 * @ORM\Table(name="news")
 * @ORM\Entity(repositoryClass="WebFactory\Bundle\NewsBundle\Entity\NewsRepository")
 * @GRID\Source(columns="id, highlighted, active, title, abstract, category.name, owner.profile.firstName, owner.profile.lastName, comments.id:count, createdAt, updatedAt", groupBy={"id"})
 */
class News
{

    /**
     * @var integer
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     * @Grid\column(filterable=false)
     */
    private $id;

    /**
     *
     * @var \WebFactory\Bundle\UserBundle\Entity\User
     * @ORM\ManyToOne(targetEntity="WebFactory\Bundle\UserBundle\Entity\User", cascade={"persist"}, fetch="LAZY")
     * @Gedmo\Blameable(on="create")
     * @Gedmo\Blameable(on="update")
     * @Grid\Column(title="Owner", field="owner.profile.firstName", filterable=false)
     * @Grid\Column(title="Owner", field="owner.profile.lastName", visible=false, filterable=false)
     */
    protected $owner;

    /**
     *
     * @var type
     * @ORM\ManyToOne(targetEntity="WebFactory\Bundle\NewsBundle\Entity\Category", inversedBy="newsCollection", cascade={"persist"}, fetch="LAZY")
     * @Assert\NotBlank()
     * @Grid\Column(title="Category", field="category.name")
     */
    protected $category;

    /**
     *
     * @var \Doctrine\Common\Collections\ArrayCollection
     * @ORM\ManyToMany(targetEntity="Tag", cascade={"persist"})
     * @ORM\JoinTable(name="news_tags",
     *      joinColumns={@ORM\JoinColumn(name="news_id", referencedColumnName="id")},
     *      inverseJoinColumns={@ORM\JoinColumn(name="tag_id", referencedColumnName="id")}
     *      )
     */
    protected $tags;

    /**
     *
     * @var type
     * @ORM\OneToMany(targetEntity="WebFactory\Bundle\NewsBundle\Entity\Comment", mappedBy="news", cascade={"all"}, orphanRemoval=true)
     * @Grid\Column(title="Comments", field="comments.id:count")
     */
    protected $comments;

    /**
     *
     * @var Image
     * @ORM\OneToOne(targetEntity="Image", mappedBy="news", cascade={"all"})
     * @Assert\NotBlank()
     * @Assert\Valid()
     */
    protected $mainImage;

    /**
     * @var string
     *
     * @ORM\Column(name="title", type="string", length=255)
     * @Assert\NotBlank()
     * @Grid\Column(title="Title")
     */
    private $title;

    /**
     * @Gedmo\Slug(fields={"title"})
     * @ORM\Column(length=255, unique=true)
     */
    protected $slug;

    /**
     * @var string
     *
     * @ORM\Column(name="abstract", type="string", length=255)
     * @Assert\NotBlank()
     * @Grid\Column(title="Abstract")
     */
    private $abstract;

    /**
     * @var string
     *
     * @ORM\Column(name="content", type="text")
     * @Assert\NotBlank()
     * @Grid\Column(visible=false, title="Content")
     */
    private $content;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="created_at", type="datetime")
     * @Gedmo\Timestampable(on="create")
     * @Grid\Column(title="Created At")
     */
    private $createdAt;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="updated_at", type="datetime")
     * @Gedmo\Timestampable(on="update")
     * @Grid\Column(title="Updated At")
     */
    private $updatedAt;

    /**
     * @var string
     *
     * @ORM\Column(type="boolean")
     * @Grid\Column(title="Highlighted");
     */
    protected $highlighted;

    /**
     * @var boolean
     *
     * @ORM\Column(type="boolean", type="boolean", nullable=true)
     * @Grid\Column(title="Active");
     */
    protected $active;

    /**
     * Constructor
     */
    public function __construct($title = null, $abstract = null, $content = null, Category $category = null)
    {
        $this->title = $title;
        $this->abstract = $abstract;
        $this->content = $content;
        $this->category = $category;
        $this->highlighted = false;
        $this->active = false;
        $this->tags = new ArrayCollection();
        $this->comments = new ArrayCollection();
    }

    /**
     * Get id
     *
     * @return integer
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set title
     *
     * @param string $title
     * @return News
     */
    public function setTitle($title)
    {
        $this->title = $title;

        return $this;
    }

    /**
     * Get title
     *
     * @return string
     */
    public function getTitle()
    {
        return $this->title;
    }

    /**
     * Set abstract
     *
     * @param string $abstract
     * @return News
     */
    public function setAbstract($abstract)
    {
        $this->abstract = $abstract;

        return $this;
    }

    /**
     * Get abstract
     *
     * @return string
     */
    public function getAbstract()
    {
        return $this->abstract;
    }

    /**
     * Set content
     *
     * @param string $content
     * @return News
     */
    public function setContent($content)
    {
        $this->content = $content;

        return $this;
    }

    /**
     * Get content
     *
     * @return string
     */
    public function getContent()
    {
        return $this->content;
    }

    /**
     * Set createdAt
     *
     * @param \DateTime $createdAt
     * @return News
     */
    public function setCreatedAt($createdAt)
    {
        $this->createdAt = $createdAt;

        return $this;
    }

    /**
     * Get createdAt
     *
     * @return \DateTime
     */
    public function getCreatedAt()
    {
        return $this->createdAt;
    }

    /**
     * Set updatedAt
     *
     * @param \DateTime $updatedAt
     * @return News
     */
    public function setUpdatedAt($updatedAt)
    {
        $this->updatedAt = $updatedAt;

        return $this;
    }

    /**
     * Get updatedAt
     *
     * @return \DateTime
     */
    public function getUpdatedAt()
    {
        return $this->updatedAt;
    }

    public function getCategory()
    {
        return $this->category;
    }

    public function setCategory($category)
    {
        $this->category = $category;
    }

    public function getComments()
    {
        return $this->comments;
    }

    public function setComments($comments)
    {
        $this->comments = $comments;
    }

    public function getMainImage()
    {
        return $this->mainImage;
    }

    public function setMainImage(Image $mainImage)
    {
        $this->mainImage = $mainImage;

        $mainImage->setNews($this);
    }

    public function getOwner()
    {
        return $this->owner;
    }

    public function setOwner(\WebFactory\Bundle\UserBundle\Entity\User $owner)
    {
        $this->owner = $owner;
    }

    /**
     * Add tags
     *
     * @param \WebFactory\Bundle\NewsBundle\Entity\Tag $tags
     * @return News
     */
    public function addTag(\WebFactory\Bundle\NewsBundle\Entity\Tag $tags)
    {
        $this->tags[] = $tags;

        return $this;
    }

    /**
     * Remove tags
     *
     * @param \WebFactory\Bundle\NewsBundle\Entity\Tag $tags
     */
    public function removeTag(\WebFactory\Bundle\NewsBundle\Entity\Tag $tags)
    {
        $this->tags->removeElement($tags);
    }

    /**
     * Get tags
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getTags()
    {
        return $this->tags;
    }

    /**
     * Add comments
     *
     * @param \WebFactory\Bundle\NewsBundle\Entity\Comment $comments
     * @return News
     */
    public function addComment(\WebFactory\Bundle\NewsBundle\Entity\Comment $comments)
    {
        $this->comments[] = $comments;

        return $this;
    }

    /**
     * Remove comments
     *
     * @param \WebFactory\Bundle\NewsBundle\Entity\Comment $comments
     */
    public function removeComment(\WebFactory\Bundle\NewsBundle\Entity\Comment $comments)
    {
        $this->comments->removeElement($comments);
    }

    /**
     *
     * @return string
     */
    public function getSlug()
    {
        return $this->slug;
    }

    /**
     * Set highlighted
     *
     * @param string $highlighted
     * @return Package
     */
    public function setHighlighted($highlighted)
    {
        $this->highlighted = $highlighted;

        return $this;
    }

    /**
     * Get highlighted
     *
     * @return string
     */
    public function getHighlighted()
    {
        return $this->highlighted;
    }

    /**
     * @return boolean
     */
    public function isActive()
    {
        return $this->active;
    }

    /**
     * @param boolean $active
     */
    public function setActive($active)
    {
        $this->active = $active;
    }


}