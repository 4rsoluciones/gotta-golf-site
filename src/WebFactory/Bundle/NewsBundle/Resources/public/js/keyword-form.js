$(document).ready(function() {

    function split(val) {
        return val.split(/,\s*/);
    }

    function extractLast(term) {
        return split(term).pop();
    }

    var $input = $('input.keywordField[type=text]');

    $input.tagit({
        allowSpaces: true,
        autocomplete: {
            delay: 0,
            minLength: 2,
            source: function(request, response) {
                var lastTerm = request.term.split(/,\s*/).pop();
                $.getJSON(ajax_keyword, {
                    term: lastTerm
                }, response);
            }
        }
    });

});