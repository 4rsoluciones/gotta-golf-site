<?php

namespace WebFactory\Bundle\NewsBundle\Form;

use Symfony\Component\OptionsResolver\OptionsResolverInterface;
use WebFactory\Bundle\CommonBundle\Form\ImageType as Base;

class ImageType extends Base
{

    public function setDefaultOptions(OptionsResolverInterface $resolver)
    {
        $resolver->setDefaults(array(
            'data_class' => 'WebFactory\Bundle\NewsBundle\Entity\Image'
        ));
    }

    public function getName()
    {
        return 'image';
    }
}
