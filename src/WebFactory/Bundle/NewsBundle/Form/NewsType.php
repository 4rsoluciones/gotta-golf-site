<?php

namespace WebFactory\Bundle\NewsBundle\Form;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolverInterface;

class NewsType extends AbstractType
{

    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
                ->add('title', 'text', array(
                    'attr' => array(
                        'class' => 'span12'
                    )
                ))
                ->add('highlighted', null, array(
                    'required' => false,
                    'render_optional_text' => false,
                    'help_inline'  => 'Highlighted',
                    'widget_checkbox_label' => 'label',
                    'label' => false
                ))
                ->add('active', null, array(
                    'required' => false,
                    'render_optional_text' => false,
                    'help_inline'  => 'Active',
                    'widget_checkbox_label' => 'label',
                    'label' => false
                ))
                ->add('abstract', 'textarea', array(
                    'attr' => array(
                        'class' => 'span12',
                        'rows' => 6
                    )
                ))
                ->add('content', 'ckeditor', array(
                    'config' => array(
						'config_name' => 'my_config',
                        'filebrowserBrowseRoute' => 'elfinder',
                        'height' => '450px',
                    ),
                    'render_optional_text' => false,
                ))
                ->add('category', 'entity', array(
                    'class' => 'WebFactory\Bundle\NewsBundle\Entity\Category',
                    'query_builder' => function(\WebFactory\Bundle\NewsBundle\Entity\CategoryRepository $r) {
                        return $r->createQueryBuilder('Category')->orderBy('Category.name', 'asc');
                    }
                ))
                ->add('tags', 'keyword', array(
                    'required' => true,
                    'attr' => array(
                        'class' => 'keywordField'
                    )
                ))
                ->add('mainImage', new ImageType, array(
                'help_block' => 'Dimensiones recomendadas: 220 x 140 px'
                ))
        ;
    }

    public function setDefaultOptions(OptionsResolverInterface $resolver)
    {
        $resolver->setDefaults(array(
            'data_class' => 'WebFactory\Bundle\NewsBundle\Entity\News'
        ));
    }

    public function getName()
    {
        return 'news';
    }

}
