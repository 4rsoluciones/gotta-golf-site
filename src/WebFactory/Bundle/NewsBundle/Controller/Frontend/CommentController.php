<?php

namespace WebFactory\Bundle\NewsBundle\Controller\Frontend;

use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Template;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;
use WebFactory\Bundle\NewsBundle\Entity\Comment;
use WebFactory\Bundle\NewsBundle\Entity\News;
use WebFactory\Bundle\NewsBundle\Form\CommentType;

/**
 * Comment controller.
 *
 * @Route("/comment")
 */
class CommentController extends Controller
{

    /**
     * Lists all Comment entities.
     *
     * @Route("/", name="frontend_comment")
     * @Method("GET")
     * @Template()
     */
    public function indexAction(News $news)
    {
        $em = $this->getDoctrine()->getManager();

        $entities = $em->getRepository('WebFactoryNewsBundle:Comment')->findBy(array(
            'news' => $news,
            'status' => 'published',
        ), array(
            'createdAt' => 'DESC'
        ));

        return array(
            'entities' => $entities,
            'news' => $news,
        );
    }
    /**
     * Creates a new Comment entity.
     *
     * @Route("/", name="frontend_comment_create")
     * @Method("POST")
     * @Template("WebFactoryNewsBundle:Frontend/Comment:new.html.twig")
     */
    public function createAction(Request $request, News $news)
    {
        $entity  = new Comment();
        $entity->setNews($news);
        $entity->setUser($this->getUser());

        $form = $this->createForm(new CommentType(), $entity);
        $form->bind($request);

        if ($form->isValid()) {
            $em = $this->getDoctrine()->getManager();
            $em->persist($entity);
            $em->flush();

            return $this->redirect($this->generateUrl('frontend_news_show', array('id' => $news->getSlug())));
        }

        return array(
            'entity' => $entity,
            'news' => $news,
            'form'   => $form->createView(),
        );
    }

    /**
     * Displays a form to create a new Comment entity.
     *
     * @Route("/new", name="frontend_comment_new")
     * @Method("GET")
     * @Template()
     */
    public function newAction(News $news)
    {
        $entity = new Comment();
        $form   = $this->createForm(new CommentType(), $entity);

        return array(
            'entity' => $entity,
            'news' => $news,
            'form'   => $form->createView(),
        );
    }

}
