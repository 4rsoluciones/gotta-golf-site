<?php

namespace WebFactory\Bundle\NewsBundle\Controller\Frontend;

use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Template;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use WebFactory\Bundle\NewsBundle\Entity\Category;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\ParamConverter;
use WebFactory\Bundle\NewsBundle\Entity\Tag;
use WebFactory\Bundle\NewsBundle\Entity\News;

/**
 * News controller.
 *
 * @Route("/news")
 */
class NewsController extends Controller
{

    /**
     * Lists all News entities.
     *
     * @Route("/", name="frontend_news")
     * @Route("/category/{category}", name="frontend_news_by_category")
     * @Route("/tag/{tag}", name="frontend_news_by_tag")
     * @Route("/category/{category}/tag/{tag}", name="frontend_news_by_category_and_tag")
     * @ParamConverter("tag", class="WebFactoryNewsBundle:Tag", options={"mapping":{"tag":"slug"}})
     * @ParamConverter("category", class="WebFactoryNewsBundle:Category", options={"mapping":{"category":"slug"}})
     * 
     * @Method("GET")
     * 
     * @Template()
     */
    public function indexAction(Category $category = null, Tag $tag = null)
    {
        $em = $this->getDoctrine()->getManager();
        $queryBuilder = $em->getRepository('WebFactoryNewsBundle:News')->createQueryBuilderByCategoryAndTag($category, $tag);
        
        $paginator  = $this->get('knp_paginator');
        $pagination = $paginator->paginate(
            $queryBuilder,
            $this->get('request')->query->get('page', 1),
            10
        );

        return array(
            'entities' => $pagination,
        );
    }

    /**
     * Finds and displays a News entity.
     *
     * @Route("/{id}", name="frontend_news_show")
     * @ParamConverter("news", class="WebFactoryNewsBundle:News", options={"mapping":{"id":"slug"}})
     * @Method("GET|POST")
     * @Template()
     */
    public function showAction(News $news)
    {
        $response = null;
        if($this->getRequest()->getMethod() === 'POST') {
            $response = $this->forward('WebFactoryNewsBundle:Frontend/Comment:create', array('news' => $news->getId()), array('news' => $news->getId()));

            if($response->isRedirect()) {
                return $response;
            }
        }
        
        return array(
            'entity'      => $news,
            'response' => $response,
        );
    }
}
