<?php

namespace WebFactory\Bundle\NewsBundle\DataFixtures\ORM;

use Closure;
use DateTime;
use Doctrine\Common\DataFixtures\AbstractFixture;
use Doctrine\Common\DataFixtures\OrderedFixtureInterface;
use Doctrine\Common\Persistence\ObjectManager;
use WebFactory\Bundle\NewsBundle\Entity\Category;

class LoadCategories extends AbstractFixture implements OrderedFixtureInterface
{

    /**
     * {@inheritDoc}
     */
    public function load(ObjectManager $manager)
    {
        $category1 = new Category('Category 1');
        $category2 = new Category('Category 2');

        $manager->persist($category1);
        $manager->persist($category2);


        $manager->flush();

        $this->addReference('category-1', $category1);
        $this->addReference('category-2', $category2);
    }

    /**
     * {@inheritDoc}
     */
    public function getOrder()
    {
        return 1;
    }

}