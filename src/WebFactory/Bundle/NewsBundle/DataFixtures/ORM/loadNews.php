<?php

namespace WebFactory\Bundle\NewsBundle\DataFixtures\ORM;

use Closure;
use DateTime;
use Doctrine\Common\DataFixtures\AbstractFixture;
use Doctrine\Common\DataFixtures\OrderedFixtureInterface;
use Doctrine\Common\Persistence\ObjectManager;
use WebFactory\Bundle\NewsBundle\Entity\News;

class LoadNews extends AbstractFixture implements OrderedFixtureInterface
{

    /**
     * {@inheritDoc}
     */
    public function load(ObjectManager $manager)
    {
        $news1 = new News('news1', 'abstract 1', 'The new 1 content is ...', $this->getReference('category-1'));
        $news2 = new News('news2', 'abstract 2', 'The new 2 content is ...', $this->getReference('category-2'));

        $manager->persist($news1);
        $manager->persist($news2);


        $manager->flush();

        $this->addReference('news-1', $news1);
        $this->addReference('news-2', $news2);
    }

    /**
     * {@inheritDoc}
     */
    public function getOrder()
    {
        return 2;
    }

}