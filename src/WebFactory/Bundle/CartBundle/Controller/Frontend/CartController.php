<?php

namespace WebFactory\Bundle\CartBundle\Controller\Frontend;

use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Template;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use WebFactory\Bundle\OrderBundle\Model\OrderStatuses;

/**
 * @Route("/cart")
 */
class CartController extends Controller
{

    /**
     * @Route("/", name="frontend_cart")
     * @Method("GET")
     * @Template
     */
    public function indexAction()
    {
        return array('cart' => $this->get('web_factory_cart.manager')->getCart());
    }

    /**
     * @Route("/add/{product}", name="frontend_cart_add")
     * @Method("GET")
     */
    public function addAction($product)
    {
        $product = $this->get('web_factory_product.manager')->findOneById($product);
        try {
            $this->get('web_factory_cart.manager')->addItem($product);
        } catch (\Exception $e) {
            $this->get('session')->getFlashBag()->add('notice', $e->getMessage());
        }

        return $this->redirect($this->generateUrl('frontend_cart'));
    }

    /**
     * @Route("/clean", name="frontend_cart_clean")
     * @Method("GET")
     */
    public function cleanAction()
    {
        $this->get('web_factory_cart.manager')->removeItems();

        return $this->redirect($this->generateUrl('frontend_cart'));
    }

    /**
     * @Route("/delete", name="frontend_cart_delete_item")
     * @Method("POST")
     */
    public function deleteItemAction(Request $request)
    {
        $product_id = $request->request->get('id');
        $this->get('web_factory_cart.manager')->removeItem($product_id);

        return new Response();
    }

    /**
     * @Route("/confirm", name="frontend_order_create")
     * @Method("GET")
     * @Template()
     */
    public function confirmAction()
    {
        $em = $this->getDoctrine()->getManager();
        $user = $this->getUser();
        $order = $this->get('web_factory_cart.manager')->getOrder($user);

        $em->persist($order);
        $em->flush();
        
        $this->get('web_factory_cart.manager')->removeItems();
        
        if ($order->getTotal() == 0) {
            return $this->redirect($this->generateUrl('webfactory_order_frontend_order_update', array(
                'order' => $order->getId(),
                'status' => OrderStatuses::PAID
                    )));
        }

        $this->get('session')->getFlashBag()->add('notice', 'flash.cart.confirmed');

        return $this->redirect($this->generateUrl('webfactory_order_frontend_order_show', array('id' => $order->getId())));
    }

    /**
     * @Route("/apply-promotional-code", name="webfactory_order_frontend_apply_promotional_code")
     * @Method("GET")
     *
     * @param Request $request
     * @return JsonResponse
     */
    public function applyPromotionalCodeAction(Request $request)
    {
        $user = $this->getUser();
        $cart = $this->get('web_factory_cart.manager')->getCart($user);
        $serviceName = $cart->getItems()[0]->getProduct()->getName();
        $code = $request->query->get('code');

        $service = $this->getDoctrine()->getRepository('AndresGottaServiceBundle:Service')->findOneBy(['name' => $serviceName]);
        if (!$service) {
            return new JsonResponse([
                'error' => 'Invalid service',
            ], 400);
        }

        $promotionalCodeRepository = $this->getDoctrine()->getRepository('AndresGottaServiceBundle:PromotionalCode');
        $promotionalCode = $promotionalCodeRepository->findOneBy(['code' => $code]);
        if (!$promotionalCode) {
            return new JsonResponse([
                'error' => 'Invalid promotional code',
            ], 400);
        }

        // TODO: Esto debería estar en un servicio que verifique si un código promocional se puede seguir usando
        $count = $promotionalCodeRepository->getBurnedCount($code);
        $burned = $promotionalCode->getNumberOfUses() && $count === $promotionalCode->getNumberOfUses();
        if ($burned) {
            return new JsonResponse([
                'error' => 'Code is already burned',
            ], 400);
        }

        if ($promotionalCode->getService() && $promotionalCode->getService() != $service) {
            return new JsonResponse([
                'result' => 'invalid'
            ], 200);
        }

        if (!$cart->getPromotionalCode()) {
            $cart->setPromotionalCode($promotionalCode->getCode());

            $discount = $promotionalCode->getDiscount();
            $cart->setDiscount($discount);

            $orderTotal = 0;
            $itemPrices = [];
            foreach ($cart->getItems() as $key => $item) {
                $itemPrice = $item->getPrice();
                $newPrice = (100 - $discount) * $itemPrice / 100;

                $cart->getItems()[$key]->setPrice($newPrice);

                $orderTotal += $newPrice;
                $itemPrices[] = $newPrice;
            }

            $this->get('webfactory_cart.session.storage')->persist($cart);
        } else {
            $discount = $promotionalCode->getDiscount();

            $orderTotal = 0;
            $itemPrices = [];
            foreach ($cart->getItems() as $key => $item) {
                $itemPrice = $item->getPrice();
                $newPrice = (100 - $discount) * $itemPrice / 100;

                $orderTotal += $newPrice;
                $itemPrices[] = $newPrice;
            }
        }

        return new JsonResponse([
            'result' => 'valid',
            'orderTotal' => $orderTotal,
            'itemPrices' => $itemPrices
        ], 200);
    }
}
