<?php

namespace WebFactory\Bundle\CartBundle\Storage;

use JMS\Serializer\DeserializationContext;
use JMS\Serializer\SerializationContext;
use JMS\Serializer\Serializer;
use Symfony\Component\HttpFoundation\Session\SessionInterface;
use Symfony\Component\Security\Core\User\UserInterface;
use WebFactory\Bundle\CartBundle\Model\Cart;

class CartSessionStorage implements CartStorageInterface
{

    /**
     *
     * @var SessionInterface
     */
    protected $session;

    /**
     * @var Serializer
     */
    protected $serializer;

    /**
     * 
     * @param SessionInterface $session
     */
    public function __construct(SessionInterface $session, Serializer $serializer)
    {
        $this->session = $session;
        $this->serializer = $serializer;
    }

    /**
     * @param Cart $cart
     * @param string $format
     * @return string
     */
    private function serializeCart($cart, $format = 'json')
    {
        return $this->serializer->serialize($cart, $format, SerializationContext::create()->enableMaxDepthChecks());
    }

    /**
     * @param string $cart
     * @param string $format
     * @return Cart
     */
    private function deserializeCart($cart, $format = 'json')
    {
        return $this->serializer->deserialize($cart, 'WebFactory\Bundle\CartBundle\Model\Cart', $format, DeserializationContext::create()->enableMaxDepthChecks());
    }

    /**
     * 
     */
    public function load(UserInterface $user = null)
    {
        $sessionCart = $this->session->get('cart');
        $cart = $this->deserializeCart($sessionCart);

        return $cart;
    }

    /**
     * 
     * @param Cart $cart
     */
    public function persist(Cart $cart)
    {
        $sessionCart = $this->serializeCart($cart);
        
        $this->session->set('cart', $sessionCart);
    }

    /**
     * 
     * @param Cart $cart
     */
    public function remove(Cart $cart)
    {
        $this->session->remove('cart');
    }

}
