<?php

namespace WebFactory\Bundle\CartBundle\Storage;

use Symfony\Component\Security\Core\User\UserInterface;
use WebFactory\Bundle\CartBundle\Model\Cart;

interface CartStorageInterface
{

    public function load(UserInterface $user = null);

    public function persist(Cart $cart);

    public function remove(Cart $cart);
}
