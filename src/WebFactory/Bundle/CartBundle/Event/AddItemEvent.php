<?php

namespace WebFactory\Bundle\CartBundle\Event;

use WebFactory\Bundle\CartBundle\Model\Cart;
use WebFactory\Bundle\OrderBundle\Entity\Item;
use WebFactory\Bundle\ProductBundle\Model\ProductInterface;

class AddItemEvent extends CartEvent
{

    /**
     *
     * @var ProductInterface
     */
    protected $product;

    /**
     *
     * @var integer
     */
    protected $quantity;

    /**
     *
     * @var Item
     */
    protected $item;

    /**
     *
     * @var string
     */
    protected $errorMessage;

    /**
     * 
     * @param Cart $cart
     * @param ProductInterface $product
     * @param integer $quantity
     */
    public function __construct(Cart $cart, ProductInterface $product, $quantity)
    {
        parent::__construct($cart);
        $this->product = $product;
        $this->quantity = $quantity;
    }

    /**
     * 
     * @return ProductInterface
     */
    public function getProduct()
    {
        return $this->product;
    }

    /**
     * 
     * @return integer
     */
    public function getQuantity()
    {
        return $this->quantity;
    }

    /**
     * 
     * @return Item
     */
    public function getItem()
    {
        return $this->item;
    }

    /**
     * 
     * @param Item $item
     * @return AddItemEvent
     */
    public function setItem(Item $item)
    {
        $this->item = $item;

        return $this;
    }

    /**
     * 
     * @param array $errorMessage
     * @return AddItemEvent
     */
    public function addErrorMessage($errorMessage)
    {
        $this->errorMessages[] = $errorMessage;

        return $this;
    }

    /**
     * 
     * @return string
     */
    public function getErrorMessage()
    {
        return $this->errorMessage;
    }

    /**
     * 
     * @param string $errorMessages
     * @return AddItemEvent
     */
    public function setErrorMessage($errorMessage)
    {
        $this->errorMessage = $errorMessage;

        return $this;
    }

}
