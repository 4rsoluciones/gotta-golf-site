<?php

namespace WebFactory\Bundle\CartBundle\Event;

use Symfony\Component\EventDispatcher\Event;
use WebFactory\Bundle\CartBundle\Model\Cart;

class CartEvent extends Event
{

    /**
     *
     * @var Cart
     */
    protected $cart;

    /**
     * 
     * @param Cart $cart
     */
    public function __construct(Cart $cart)
    {
        $this->cart = $cart;
    }

    /**
     * 
     * @return Cart
     */
    public function getCart()
    {
        return $this->cart;
    }

}
