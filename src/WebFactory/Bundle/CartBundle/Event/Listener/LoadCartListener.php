<?php

namespace WebFactory\Bundle\CartBundle\Event\Listener;

use WebFactory\Bundle\CartBundle\Event\CartEvent;
use WebFactory\Bundle\ProductBundle\Model\ProductManager;

class LoadCartListener
{
    /**
     * @var ProductManager
     */
    private $productManager;

    public function load(CartEvent $event)
    {
        $cart = $event->getCart();

        if (!$cart->getItems()) {
            $event->stopPropagation();

            return;
        }

        foreach ($cart->getItems() as $item) {
            $product = $this->productManager->findOneById($item->getProduct()->getId());
            $item->setProduct($product);
        }
    }

    public function setProductManager(ProductManager $productManager)
    {
        $this->productManager = $productManager;
    }
}
