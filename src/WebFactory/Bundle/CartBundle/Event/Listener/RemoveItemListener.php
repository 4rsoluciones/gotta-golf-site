<?php

namespace WebFactory\Bundle\CartBundle\Event\Listener;

use WebFactory\Bundle\CartBundle\Event\RemoveItemEvent;

class RemoveItemListener
{

    public function remove(RemoveItemEvent $event)
    {
        $cart = $event->getCart();
        $productId = $event->getProductId();

        $items = $cart->getItems();
        foreach ($items as $item) {
            if ($item->getProduct()->getId() === (int) $productId) {
                $event->setItem($item);
                break;
            }
        }
    }

}
