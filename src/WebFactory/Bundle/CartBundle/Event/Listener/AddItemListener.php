<?php

namespace WebFactory\Bundle\CartBundle\Event\Listener;

use WebFactory\Bundle\CartBundle\Event\AddItemEvent;
use WebFactory\Bundle\OrderBundle\Entity\Item;
use WebFactory\Bundle\ProductBundle\Model\ProductInterface;
use WebFactory\Bundle\ProductBundle\Model\ProductManager;

class AddItemListener
{

    /**
     *
     * @var ProductManager
     */
    private $productManager;

    /**
     * 
     * @param ProductManager $productManager
     */
    public function __construct(ProductManager $productManager)
    {
        $this->productManager = $productManager;
    }

    /**
     * 
     * @param AddItemEvent $event
     * @return type
     */
    public function add(AddItemEvent $event)
    {
        $cart = $event->getCart();
        $product = $event->getProduct();
        $quantity = $event->getQuantity();

        if (!$product->isAvailable()) {
            return;
        }

        $items = $cart->getItems();
        if ($items) {
            foreach ($items as $item) {
                if ($item->getProduct()->isEquals($product)) {
                    $event->setErrorMessage('Repeated product');// flash.cart.item.repeated
                    $event->stopPropagation();
                    return;
                }
            }
        }

        $item = $this->createItem($product, $quantity);
        $event->setItem($item);
    }

    /**
     * 
     * @param ProductInterface $product
     * @param integer $quantity
     * @return Item
     */
    private function createItem(ProductInterface $product, $quantity)
    {
        $item = new Item();
        $item->setProduct($product);
        $item->setDescription($product->getName() . ' - ' . $product->getDescription());
        $item->setPrice($this->productManager->getPriceByProduct($product));
        $item->setQuantity($quantity);

        return $item;
    }

}
