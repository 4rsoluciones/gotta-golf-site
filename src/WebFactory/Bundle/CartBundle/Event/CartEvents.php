<?php

namespace WebFactory\Bundle\CartBundle\Event;

class CartEvents
{

    const ITEM_ADDED = 'webfactory_cart.item_added';
    const ITEM_REMOVED = 'webfactory_cart.item_removed';
    const CART_RESET = 'webfactory_cart.cart_reset';
    const CART_LOADED = 'webfactory_cart.cart_loaded';

}
