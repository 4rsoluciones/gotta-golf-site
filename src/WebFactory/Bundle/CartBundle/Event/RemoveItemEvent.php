<?php

namespace WebFactory\Bundle\CartBundle\Event;

use WebFactory\Bundle\CartBundle\Model\Cart;
use WebFactory\Bundle\OrderBundle\Entity\Item;
use WebFactory\Bundle\ProductBundle\Model\ProductInterface;

class RemoveItemEvent extends CartEvent
{

    /**
     *
     * @var integer
     */
    protected $productId;
    
    /**
     *
     * @var Item
     */
    protected $item;

    /**
     * 
     * @param Cart $cart
     * @param integer $product
     */
    public function __construct(Cart $cart, $productId)
    {
        parent::__construct($cart);
        $this->productId = $productId;
    }

    /**
     * 
     * @return ProductInterface
     */
    public function getProductId()
    {
        return $this->productId;
    }
    
    /**
     * 
     * @return Item
     */
    public function getItem()
    {
        return $this->item;
    }

    /**
     * 
     * @param Item $item
     * @return AddItemEvent
     */
    public function setItem(Item $item)
    {
        $this->item = $item;
        return $this;
    }

}
