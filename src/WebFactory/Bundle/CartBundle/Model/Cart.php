<?php

namespace WebFactory\Bundle\CartBundle\Model;

use JMS\Serializer\Annotation\MaxDepth;

use Doctrine\Common\Collections\ArrayCollection;
use JMS\Serializer\Annotation\Type;
use WebFactory\Bundle\OrderBundle\Entity\Item;
use WebFactory\Bundle\OrderBundle\Model\AbstractOrder;

class Cart extends AbstractOrder
{
    /**
     * @Type("array<WebFactory\Bundle\OrderBundle\Entity\Item>")
     * @MaxDepth(3)
     */
    protected $items;

    /**
     * @var string
     *
     * @Type("string")
     */
    private $promotionalCode;

    /**
     * @var integer
     *
     * @Type("integer")
     */
    private $discount;
    
    /**
     * Remove item from cart
     * 
     * @param Item $cartItem
     * @return Cart
     */
    public function removeItem(Item $cartItem)
    {
        foreach ($this->items as $key => $item) {
            if ($item->getProduct()->isEquals($cartItem->getProduct())) {
                unset($this->items[$key]);
            }
        }
        
        return $this;
    }
    
    public function __wakeup()
    {
        $this->items = new ArrayCollection;
    }

    /**
     * @return string
     */
    public function getPromotionalCode()
    {
        return $this->promotionalCode;
    }

    /**
     * @param string $promotionalCode
     */
    public function setPromotionalCode($promotionalCode)
    {
        $this->promotionalCode = $promotionalCode;
    }

    /**
     * @return int
     */
    public function getDiscount()
    {
        return $this->discount;
    }

    /**
     * @param int $discount
     */
    public function setDiscount($discount)
    {
        $this->discount = $discount;
    }

}
