<?php

namespace WebFactory\Bundle\CartBundle\Model;

use Doctrine\Common\Collections\ArrayCollection;
use LogicException;
use Symfony\Component\EventDispatcher\EventDispatcherInterface;
use Symfony\Component\Security\Core\User\UserInterface;
use WebFactory\Bundle\CartBundle\Event\AddItemEvent;
use WebFactory\Bundle\CartBundle\Event\CartEvent;
use WebFactory\Bundle\CartBundle\Event\CartEvents;
use WebFactory\Bundle\CartBundle\Event\RemoveItemEvent;
use WebFactory\Bundle\CartBundle\Storage\CartStorageInterface;
use WebFactory\Bundle\OrderBundle\Entity\Order;
use WebFactory\Bundle\ProductBundle\Model\ProductInterface;

class CartManager
{

    /**
     *
     * @var Cart
     */
    private $cart;

    /**
     *
     * @var CartStorageInterface
     */
    private $cartStorage;

    /**
     *
     * @var EventDispatcherInterface
     */
    private $eventDispatcher;

    /**
     * 
     * @param \WebFactory\Bundle\CartBundle\Model\CartStorageInterface $cartStorage
     * @param EventDispatcherInterface $eventDispatcher
     */
    public function __construct(CartStorageInterface $cartStorage, EventDispatcherInterface $eventDispatcher)
    {
        $this->cartStorage = $cartStorage;
        $this->eventDispatcher = $eventDispatcher;
        $this->getCart();
    }

    /**
     * Add item
     * 
     * @param ProductInterface $product
     */
    public function addItem(ProductInterface $product, $quantity = 1)
    {
        $cart = $this->getCart();

        $event = new AddItemEvent($cart, $product, $quantity);
        $this->eventDispatcher->dispatch(CartEvents::ITEM_ADDED, $event);

        $item = $event->getItem();
        if (null === $item) {
            $errorMessage = $event->getErrorMessage() ? $event->getErrorMessage() : 'Item can not be added';
            throw new LogicException($errorMessage);
        }
        if ($cart->getItems()) {
            $errorMessage = $event->getErrorMessage() ? $event->getErrorMessage() : 'No puedes añadir más de un item!';
            throw new LogicException($errorMessage);
        }
        $cart->addItem($item);

        $this->cartStorage->persist($cart);
    }

    /**
     * Remove item
     * 
     * @param int $productId
     */
    public function removeItem($productId)
    {
        $cart = $this->getCart();

        $event = new RemoveItemEvent($cart, $productId);
        $this->eventDispatcher->dispatch(CartEvents::ITEM_REMOVED, $event);
        
        $item = $event->getItem();
        if (null === $item) {
            throw new LogicException('Item can not be removed');
        }
        $cart->removeItem($item);

        $this->cartStorage->persist($cart);
    }

    /**
     * Remove items
     * Elimina todo el carrito
     */
    public function removeItems()
    {
        $cart = $this->getCart();
        $event = new CartEvent($cart);
        $this->eventDispatcher->dispatch(CartEvents::CART_RESET, $event);

        $this->cartStorage->remove($cart);
    }

    /**
     * Get items
     * 
     * @return ArrayCollection
     */
    public function getCart(UserInterface $user = null)
    {
        if (null === $this->cart) {
            $this->cart = $this->cartStorage->load($user);
            
            $event = new CartEvent($this->cart);
            $this->eventDispatcher->dispatch(CartEvents::CART_LOADED, $event);
        }

        return $this->cart;
    }

    /**
     * Get order
     * 
     * @param UserInterface $user
     */
    public function getOrder($user)
    {
        $cart = $this->getCart();

        $order = new Order();
        //TODO: Revisar luego
        foreach ($cart->getItems() as $item) {
            $order->addItem($item);
        }
        $order->setUser($user);
        $order->setPromotionalCode($cart->getPromotionalCode());
        $order->setDiscount($cart->getDiscount());

        return $order;
    }

}
