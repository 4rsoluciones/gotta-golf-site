<?php

namespace WebFactory\Bundle\ProductBundle\DependencyInjection;

use Symfony\Component\Config\Definition\Builder\TreeBuilder;
use Symfony\Component\Config\Definition\ConfigurationInterface;

/**
 * This is the class that validates and merges configuration from your app/config files
 *
 * To learn more see {@link http://symfony.com/doc/current/cookbook/bundles/extension.html#cookbook-bundles-extension-config-class}
 */
class Configuration implements ConfigurationInterface
{
    /**
     * {@inheritDoc}
     */
    public function getConfigTreeBuilder()
    {
        $treeBuilder = new TreeBuilder();
        $rootNode = $treeBuilder->root('web_factory_product');

        $rootNode
                ->children()
                    ->scalarNode('product_manager_service')
                        ->defaultValue('web_factory_product.manager')
                    ->end()
                    ->scalarNode('entity')
                        ->isRequired()
                    ->end()
                ->end();

        return $treeBuilder;
    }
}
