<?php

namespace WebFactory\Bundle\ProductBundle\Model;

use Doctrine\ORM\EntityManager;

class ProductManager
{
    /**
     * @var EntityManager 
     */
    protected $entityManager;
    
    /**
     *
     * @var string
     */
    protected $entityName;

    /**
     * Constructor
     * 
     * @param EntityManager $entityManager
     * @param string $entityName
     */
    public function __construct(EntityManager $entityManager, $entityName)
    {
        $this->entityManager = $entityManager;
        $this->entityName = $entityName;
    }
    
    /**
     * 
     * @param string $entityName
     * @return \Doctrine\ORM\EntityRepository
     */
    protected function getRepository()
    {
        return $this->entityManager->getRepository($this->entityName);
    }
    
    /**
     * Busca las entityName por $name
     * 
     * @param string $name
     * @return ProductInterface
     */
    public function findByName($name)
    {
        return $this->getRepository()->findByName($name);
    }
    
    /**
     * Busca las entityName por $description
     * 
     * @param string $description
     * @return ProductInterface
     */
    public function findByDescription($description)
    {
        return $this->getRepository()->findByDescription($description);
    }

    /**
     * Busca las entityName por $id
     * 
     * @param integer $id
     * @return ProductInterface
     */
    public function findOneById($id)
    {
        return $this->getRepository()->findOneById($id);
    }
    
    /**
     * 
     * @param ProductInterface $product
     * @return float
     */
    public function getPriceByProduct(ProductInterface $product)
    {
        return $product->getPrice();
    }
    
}
