<?php

namespace WebFactory\Bundle\ProductBundle\Model;

interface EquatableInterface
{
    /**
     * Compara el producto con otro
     *
     * @return boolean
     */
    public function isEquals(ProductInterface $product);
}
