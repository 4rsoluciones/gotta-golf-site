<?php

namespace WebFactory\Bundle\ProductBundle\Model;

interface ProductInterface
{
    /**
     * Get product name.
     *
     * @return string
     */
    public function getName();

    /**
     * Set product name.
     *
     * @param string $name
     */
    public function setName($name);
    
    /**
     * Get product name.
     *
     * @return string
     */
    public function getDescription();

    /**
     * Set product description.
     *
     * @param string $description
     */
    public function setDescription($description = null);

    /**
     * Get price
     * 
     * @return string
     */
    public function getPrice();
    
    /**
     * Set price
     * 
     * @param string $price 
     * @return ProductInterface
     */
    public function setPrice($price);
    
    /**
     * @return boolean
     */
    public function isAvailable();
    
    /**
     * @param boolean $available
     */
    public function setAvailable($available);
}
