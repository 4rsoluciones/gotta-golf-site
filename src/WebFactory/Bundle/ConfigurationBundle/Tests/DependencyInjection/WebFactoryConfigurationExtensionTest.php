<?php

namespace WebFactory\Bundle\ConfigurationBundle\Tests\DependencyInjection;

use PHPUnit_Framework_TestCase;
use Symfony\Component\DependencyInjection\ContainerBuilder;
use WebFactory\Bundle\ConfigurationBundle\DependencyInjection\WebFactoryConfigurationExtension;

class WebFactoryConfigurationExtensionTest extends PHPUnit_Framework_TestCase
{

    /**
     * @covers WebFactory\Bundle\ConfigurationBundle\DependencyInjection\WebFactoryConfigurationExtension::load
     * @covers WebFactory\Bundle\ConfigurationBundle\DependencyInjection\Configuration::getConfigTreeBuilder
     */
    public function testLoadMaintainTrue()
    {
        $extension = new WebFactoryConfigurationExtension();
        
        $containerBuilder = new ContainerBuilder();
        $containerBuilder->setParameter('maintain', true);

        $configs = array(
            array (
                'maintain' => true,
            )
        );

        $extension->load($configs, $containerBuilder);
        
        $this->assertEquals('web_factory_configuration.maintain', $extension->getAlias() . '.maintain');

        $this->assertEquals($configs[0]['maintain'], $containerBuilder->getParameter($extension->getAlias() . '.maintain'));
    }
    
    /**
     * @covers WebFactory\Bundle\ConfigurationBundle\DependencyInjection\WebFactoryConfigurationExtension::load
     */
    public function testLoadMaintainFalse()
    {
        $extension = new WebFactoryConfigurationExtension();
        
        $containerBuilder = new ContainerBuilder();
        $containerBuilder->setParameter('maintain', true);

        $configs = array(
            array (
                'maintain' => false,
            )
        );

        $extension->load($configs, $containerBuilder);
        
        $this->assertEquals('web_factory_configuration.maintain', $extension->getAlias() . '.maintain');

        $this->assertEquals($configs[0]['maintain'], $containerBuilder->getParameter($extension->getAlias() . '.maintain'));
    }

    /**
     * @covers WebFactory\Bundle\ConfigurationBundle\DependencyInjection\WebFactoryConfigurationExtension::load
     */
    public function testLoadMaintainDefault()
    {
        $extension = new WebFactoryConfigurationExtension();
        
        $containerBuilder = new ContainerBuilder();
        $containerBuilder->setParameter('maintain', true);

        $configs = array(
            array (
                
            )
        );

        $extension->load($configs, $containerBuilder);
        
        $this->assertEquals('web_factory_configuration.maintain', $extension->getAlias() . '.maintain');

        $this->assertEquals(true, $containerBuilder->getParameter($extension->getAlias() . '.maintain'));
    }

}
