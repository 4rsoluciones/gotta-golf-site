<?php

namespace WebFactory\Bundle\ConfigurationBundle\Tests\Manager;

use PHPUnit_Framework_TestCase;
use WebFactory\Bundle\ConfigurationBundle\Entity\Configuration;
use WebFactory\Bundle\ConfigurationBundle\Manager\ConfigurationManager;

/**
 *
 */
class ConfigurationManagerTest extends PHPUnit_Framework_TestCase
{

    /**
     * @covers WebFactory\Bundle\ConfigurationBundle\Manager\ConfigurationManager::__construct
     * @covers WebFactory\Bundle\ConfigurationBundle\Manager\ConfigurationManager::get
     */
    public function testGetUnavailableConfiguration()
    {
        $repository = $this->getMock('Doctrine\Common\Persistence\ObjectRepository');
        $repository
                ->expects($this->any())
                ->method('findOneBy')
                ->with(array('name' => 'unavailable-configuration'))
                ->will($this->returnValue(null));
        
        $manager = $this->getMock('Doctrine\Common\Persistence\ObjectManager');
        $manager
                ->expects($this->any())
                ->method('getRepository')
                ->with('WebFactoryConfigurationBundle:Configuration')
                ->will($this->returnValue($repository));
        
        $container = $this->getMock('Symfony\Component\DependencyInjection\ContainerInterface');
        $container
                ->expects($this->any())
                ->method('get')
                ->with('doctrine.orm.entity_manager')
                ->will($this->returnValue($manager));

        $configManager = new ConfigurationManager($container);
        
        $result = $configManager->get('unavailable-configuration');
        
        $this->assertNull($result);
    }
    
    /**
     * @covers WebFactory\Bundle\ConfigurationBundle\Entity\Configuration::__construct
     * @covers WebFactory\Bundle\ConfigurationBundle\Entity\Configuration::getValue
     * @covers WebFactory\Bundle\ConfigurationBundle\Manager\ConfigurationManager::__construct
     * @covers WebFactory\Bundle\ConfigurationBundle\Manager\ConfigurationManager::get
     */
    public function testGetAvailableConfiguration()
    {
        $expected = 'available-configuration-value';
        
        $configuration = new Configuration('available-configuration', $expected);
        
        $repository = $this->getMock('Doctrine\Common\Persistence\ObjectRepository');
        $repository
                ->expects($this->any())
                ->method('findOneBy')
                ->with(array('name' => 'available-configuration'))
                ->will($this->returnValue($configuration));
        
        $manager = $this->getMock('Doctrine\Common\Persistence\ObjectManager');
        $manager
                ->expects($this->any())
                ->method('getRepository')
                ->with('WebFactoryConfigurationBundle:Configuration')
                ->will($this->returnValue($repository));
        
        $container = $this->getMock('Symfony\Component\DependencyInjection\ContainerInterface');
        $container
                ->expects($this->any())
                ->method('get')
                ->with('doctrine.orm.entity_manager')
                ->will($this->returnValue($manager));

        $configManager = new ConfigurationManager($container);
        
        $result = $configManager->get('available-configuration');
        
        $this->assertEquals($expected, $result);
    }

}
