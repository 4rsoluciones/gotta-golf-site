<?php

namespace WebFactory\Bundle\ConfigurationBundle\Manager;

/**
*
*/
class ConfigurationManager
{

    private $container;
    private $configurationRepository;

    private $cache = array();

    function __construct($container)
    {
        $this->container = $container;
    }

    public function get($name)
    {

        if(null === $this->configurationRepository) {
            $this->configurationRepository = $this->container
                    ->get('doctrine.orm.entity_manager')
                    ->getRepository('WebFactoryConfigurationBundle:Configuration');
        }

        if(!array_key_exists($name, $this->cache)) {
            $config = $this->configurationRepository->findOneBy(compact('name'));
            $this->cache[$name] = $config ? $config->getValue() : null;
        }

        return $this->cache[$name];
    }
}