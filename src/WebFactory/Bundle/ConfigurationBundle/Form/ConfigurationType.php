<?php

namespace WebFactory\Bundle\ConfigurationBundle\Form;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\Form\FormEvent;
use Symfony\Component\Form\FormEvents;
use Symfony\Component\OptionsResolver\OptionsResolverInterface;
use Symfony\Component\Validator\Constraints\Email;
use Symfony\Component\Validator\Constraints\Url;

class ConfigurationType extends AbstractType
{

    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $factory = $builder->getFormFactory();

        $builder->addEventListener(FormEvents::PRE_SET_DATA, function (FormEvent $event) use ($factory) {
            $data = $event->getData();
            $form = $event->getForm();

            if (!$data || !$data->getId()) {
                return;
            }

            if ($data->getFormType() === 'checkbox') {

                $form->add('value', 'choice', array(
                    'choices' => array(
                        '0' => 'No',
                        '1' => 'Yes',
                    ),
                    'label' => $data->getFormLabel(),
                    'attr' => array(
                        'placeholder' => $data->getValueByDefault(),
                        'title' => $data->getDescription(),
                    )
                ));
            } else {
                $constraints = array();
                if ($data->getFormType() === 'email') {
                    $constraints[] = new Email();
                }
                if ($data->getFormType() === 'url') {
                    $constraints[] = new Url();
                }
                $form->add('value', $data->getFormType(), array(
                    'label' => $data->getFormLabel(),
                    'attr' => array(
                        'placeholder' => $data->getValueByDefault(),
                        'title' => $data->getDescription(),
                    ),
                    'constraints' => $constraints
                ));
            }
        });
    }

    public function setDefaultOptions(OptionsResolverInterface $resolver)
    {
        $resolver->setDefaults(array(
            'data_class' => 'WebFactory\Bundle\ConfigurationBundle\Entity\Configuration'
        ));
    }

    public function getName()
    {
        return 'config';
    }

}
