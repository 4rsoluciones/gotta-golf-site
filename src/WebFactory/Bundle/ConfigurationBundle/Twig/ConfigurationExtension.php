<?php

namespace WebFactory\Bundle\ConfigurationBundle\Twig;

use Symfony\Component\DependencyInjection\Container;
use Twig_Extension;
use Twig_SimpleFunction;

class ConfigurationExtension extends Twig_Extension
{

    /**
     *
     * @var Container
     */
    private $container;

    public function __construct(Container $container)
    {
        $this->container = $container;
    }

    public function getFunctions()
    {
        return array(
            new Twig_SimpleFunction('get_config', array($this, 'getConfiguration')),
        );
    }

    /**
     *
     * @param string $name
     * @return string
     */
    public function getConfiguration($name)
    {
        return $this->container->get('web_factory_configuration.manager')->get($name);
    }

    public function getName()
    {
        return 'configuration_extension';
    }

}