<?php

namespace WebFactory\Bundle\ConfigurationBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use APY\DataGridBundle\Grid\Mapping as Grid;

/**
 * Configuration
 *
 * @ORM\Table(name="configurations")
 * @ORM\Entity(repositoryClass="WebFactory\Bundle\ConfigurationBundle\Entity\ConfigurationRepository")
 * @Grid\Source(columns="id, name, modifiable, formLabel, value, description, formType", groupBy={"id"})
 */
class Configuration
{
    /**
     * @var integer
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     * @Grid\Column(filterable=false)
     */
    private $id;

    /**
     * @var string
     *
     * @ORM\Column(name="name", type="string", length=255)
     * @Grid\Column(filterable=false)
     */
    private $name;

    /**
     * @var string
     *
     * @ORM\Column(name="value", type="text", nullable=true)
     * @Grid\Column(title="Value", filterable=false)
     */
    private $value;

    /**
     * @var string
     *
     * @ORM\Column(name="default_value", type="text", nullable=true)
     * @Grid\Column(title="Default Value")
     */
    private $valueByDefault;

    /**
     * @var boolean
     *
     * @ORM\Column(name="modifiable", type="boolean")
     * @Grid\Column(filterable=false)
     */
    private $modifiable = true;

    /**
     * @var string
     *
     * @ORM\Column(name="label", type="string", length=255)
     * @Grid\Column(title="Configuration", defaultOperator="like", operators={"eq", "neq", "like", "nlike", "rlike", "llike", "isNull", "isNotNull"})
     */
    private $formLabel;

    /**
     * @var string
     *
     * @ORM\Column(name="description", type="text", nullable=true)
     * @Grid\Column(title="Description", defaultOperator="like", operators={"eq", "neq", "like", "nlike", "rlike", "llike", "isNull", "isNotNull"})
     */
    private $description;

    /**
     * @var string
     *
     * @ORM\Column(name="form_type", type="string", length=255)
     * @Grid\Column(title="FormType", visible=false, filterable=false)
     */
    private $formType = 'text';

    public function __construct($name, $value = null, $valueByDefault = null, $modifiable = true, $formLabel = null, $description = null, $formType = 'text')
    {
        $this->name = $name;
        $this->value = $value;
        $this->valueByDefault = $valueByDefault;
        $this->modifiable = $modifiable;
        $this->formLabel = $formLabel ?: $name;
        $this->description = $description;
        $this->formType = $formType;
    }

        /**
     * Get id
     *
     * @return integer
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set name
     *
     * @param string $name
     * @return Configuration
     */
    public function setName($name)
    {
        $this->name = $name;

        return $this;
    }

    /**
     * Get name
     *
     * @return string
     */
    public function getName()
    {
        return $this->name;
    }

    /**
     * Set value
     *
     * @param string $value
     * @return Configuration
     */
    public function setValue($value)
    {
        $this->value = $value;

        return $this;
    }

    /**
     * Get value
     *
     * @return string
     */
    public function getValue()
    {
        return $this->value;
    }

    /**
     * Set valueByDefault
     *
     * @param string $valueByDefault
     * @return Configuration
     */
    public function setValueByDefault($valueByDefault)
    {
        $this->valueByDefault = $valueByDefault;

        return $this;
    }

    /**
     * Get valueByDefault
     *
     * @return string
     */
    public function getValueByDefault()
    {
        return $this->valueByDefault;
    }

    /**
     * Set modifiable
     *
     * @param boolean $modifiable
     * @return Configuration
     */
    public function setModifiable($modifiable)
    {
        $this->modifiable = $modifiable;

        return $this;
    }

    /**
     * Get modifiable
     *
     * @return boolean
     */
    public function isModifiable()
    {
        return $this->modifiable;
    }

    /**
     * Set formLabel
     *
     * @param string $formLabel
     * @return Configuration
     */
    public function setFormLabel($formLabel)
    {
        $this->formLabel = $formLabel;

        return $this;
    }

    /**
     * Get formLabel
     *
     * @return string
     */
    public function getFormLabel()
    {
        return $this->formLabel;
    }

    /**
     * Set formType
     *
     * @param string $formType
     * @return Configuration
     */
    public function setFormType($formType)
    {
        $this->formType = $formType;

        return $this;
    }

    /**
     * Get formType
     *
     * @return string
     */
    public function getFormType()
    {
        return $this->formType;
    }

    /**
     * Set description
     *
     * @param string $description
     * @return Configuration
     */
    public function setDescription($description)
    {
        $this->description = $description;

        return $this;
    }

    /**
     * Get description
     *
     * @return string
     */
    public function getDescription()
    {
        return $this->description;
    }

    /**
     * Get modifiable
     *
     * @return boolean
     */
    public function getModifiable()
    {
        return $this->modifiable;
    }
}