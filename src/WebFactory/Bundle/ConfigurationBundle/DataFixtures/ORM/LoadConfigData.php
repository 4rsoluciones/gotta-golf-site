<?php

namespace WebFactory\Bundle\ConfigurationBundle\DataFixtures\ORM;

use Closure;
use DateTime;
use Doctrine\Common\DataFixtures\FixtureInterface;
use Doctrine\Common\Persistence\ObjectManager;
use WebFactory\Bundle\ConfigurationBundle\Entity\Configuration;

class LoadConfigData implements FixtureInterface
{

    /**
     * {@inheritDoc}
     */
    public function load(ObjectManager $manager)
    {
        $siteName = new Configuration('site_name', 'Sitio Web', '', true, 'Nombre del sitio', 'El nombre del sitio', 'text');
        $manager->persist($siteName);

        $aboutUrl = new Configuration('about_url', 'http://www.sitio.com', 'http://www.sitio.com', true, 'Acerca de (url)', 'La url del enlace en el footer', 'url');
        $manager->persist($aboutUrl);

        $aboutText = new Configuration('about_text', 'WebFactory', 'WebFactory', true, 'Acerca de (texto)', 'El texto del enlace en el footer', 'text');
        $manager->persist($aboutText);

        $contactEmail = new Configuration('contact_email', 'test.and.development@gmail.com', 'info@gmail.com', true, 'Email de contacto', 'La dirección de correo que recibirá los mensajes de contacto', 'email');
        $manager->persist($contactEmail);

        $maintainSystem = new Configuration('maintain_system', '0', '1', true, 'Sitio en mantenimiento', 'Al activarlo el sitio queda en estado mantenimiento', 'checkbox');
        $manager->persist($maintainSystem);

        $maintainAllowedIp = new Configuration('maintain_allowed_ip', '127.0.0.1', '127.0.0.1', true, 'Ip permitidas en modo mantenimiento', 'Ip permitidas en modo mantenimiento', 'textarea');
        $manager->persist($maintainAllowedIp);

        $manager->flush();
    }

}
