Este bundle se encarga de gestionar configuraciones persistentes en base de datos.

Incluye un servicio para conveniente acceso a datos (solo lectura).

Además incluye un controlador para administradores que permite listar y editar configuraciones.