<?php

namespace WebFactory\Bundle\ConfigurationBundle\Controller;

use Symfony\Component\HttpFoundation\Request;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Template;
use WebFactory\Bundle\ConfigurationBundle\Entity\Configuration;
use WebFactory\Bundle\ConfigurationBundle\Form\ConfigurationType;
use APY\DataGridBundle\Grid\Source\Entity;
use APY\DataGridBundle\Grid\Action\RowAction;

/**
 * Configuration controller.
 *
 * @Route("/configuration")
 */
class ConfigurationController extends Controller
{
    /**
     * Lists all Configuration entities.
     *
     * @Route("/", name="configuration")
     * @Method("GET|POST")
     * @Template()
     */
    public function indexAction()
    {
        /* @var $source \APY\DataGridBundle\Grid\Source\Entity */
        $source = new Entity('WebFactoryConfigurationBundle:Configuration');
        /* @var $grid \APY\DataGridBundle\Grid\Grid */
        $grid = $this->get('grid');
        $grid->setSource($source);
        $grid->setId('main');
        $grid->setHiddenColumns(array('id', 'name', 'modifiable'));
        $grid->setDefaultOrder('name', 'asc');

        $editRowAction = new RowAction('Edit', 'configuration_edit', false, '_self', array('class' => 'btn-action glyphicons pencil btn-success'));
        $grid->addRowAction($editRowAction);

        return $grid->getGridResponse('WebFactoryConfigurationBundle:Configuration:index.html.twig');
    }

    /**
     * Displays a form to edit an existing Configuration entity.
     *
     * @Route("/{id}/edit", name="configuration_edit")
     * @Method("GET")
     * @Template()
     */
    public function editAction(Configuration $entity)
    {
        if(!$entity->isModifiable()) {
            $this->get('session')->getFlashBag()->add('error', $this->get('translator')->trans('The configuration is not modifiable!'));
            return $this->redirect($this->generateUrl('configuration'));
        }

        $editForm = $this->createForm(new ConfigurationType(), $entity);

        return array(
            'entity'      => $entity,
            'edit_form'   => $editForm->createView(),
        );
    }

    /**
     * Edits an existing Configuration entity.
     *
     * @Route("/{id}", name="configuration_update")
     * @Method("PUT")
     * @Template("WebFactoryConfigurationBundle:Configuration:edit.html.twig")
     */
    public function updateAction(Request $request, Configuration $entity)
    {
        $translator = $this->get('translator');
        $session = $this->get('session');

        if(!$entity->isModifiable()) {
            $session->getFlashBag()->add('error', $translator->trans('The configuration is not modifiable!'));
            return $this->redirect($this->generateUrl('configuration'));
        }

        $editForm = $this->createForm(new ConfigurationType(), $entity);
        $editForm->bind($request);

        if ($editForm->isValid()) {
            $em = $this->getDoctrine()->getManager();
            try {
                $em->persist($entity);
                $em->flush();

                $session->getFlashBag()->add('success', $translator->trans('Record updated!'));

                return $this->redirect($this->generateUrl('configuration_edit', array('id' => $entity->getId())));
            } catch(\Exception $e) {
                $session->getFlashBag()->add('error', $translator->trans('Configuration error: %message%', array(
                    '%message%' => $e->getMessage()
                )));

            }
        }

        return array(
            'entity'      => $entity,
            'edit_form'   => $editForm->createView(),
        );
    }

}
