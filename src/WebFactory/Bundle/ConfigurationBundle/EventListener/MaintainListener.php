<?php

namespace WebFactory\Bundle\ConfigurationBundle\EventListener;

use Symfony\Component\DependencyInjection\ContainerInterface;
use Symfony\Component\HttpFoundation\RedirectResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpKernel\Event\GetResponseEvent;

class MaintainListener
{

    protected $container;

    public function __construct(ContainerInterface $container)
    {
        $this->container = $container;
    }

    public function onKernelRequest(GetResponseEvent $event)
    {
        $request = $event->getRequest();
        if ($this->mustBeChecked($request)) {

            if (!$this->isClientIpAllowed($request)) {
                $maintainRoute = $this->container->getParameter('maintain_route');
                $url = $this->container->get('router')->generate($maintainRoute);
                $event->setResponse(new RedirectResponse($url));
                $event->stopPropagation();
            }
        }
    }

    private function mustBeChecked(Request $request)
    {
        $maintainSystem = $this->container->get('web_factory_configuration.manager')->get('maintain_system');

        return $maintainSystem && !$this->isCurrentRouteMaintain($request) && !$this->isPathAllowed($request);
    }

    private function isPathAllowed(Request $request)
    {
        $allowedPath = $this->container->getParameter('maintain_allowed_pattern');

        if(preg_match('/^\/_wdt\//i', $request->getPathInfo())) {
            return true;
        }

        return preg_match('/^' . preg_quote($allowedPath, '/') . '/i', $request->getPathInfo());
    }

    private function isCurrentRouteMaintain(Request $request)
    {
        $maintainRoute = $this->container->getParameter('maintain_route');

        return $request->get('_route') === $maintainRoute;
    }

    private function isClientIpAllowed(Request $request)
    {
        $allowedIpConfig = $this->container->get('web_factory_configuration.manager')->get('maintain_allowed_ip');
        $allowedIps = array_map('trim', explode("\n", $allowedIpConfig));

        $clientIp = $request->getClientIp();

        return in_array($clientIp, $allowedIps);
    }

}