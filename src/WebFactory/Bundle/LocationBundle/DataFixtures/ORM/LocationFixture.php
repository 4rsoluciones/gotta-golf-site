<?php

namespace WebFactory\Bundle\LocationBundle\DataFixtures\ORM;

use Doctrine\Common\DataFixtures\FixtureInterface;
use Doctrine\Common\DataFixtures\OrderedFixtureInterface;
use Doctrine\Common\Persistence\ObjectManager;

class LocationFixture implements FixtureInterface, OrderedFixtureInterface
{

    /**
     * {@inheritDoc}
     */
    public function load(ObjectManager $manager)
    {
        $sql = file_get_contents(__DIR__ . '/../../Resources/sql/locations.sql');

        $manager->getConnection()->exec($sql);
    }

    /**
     * 
     * @return int
     */
    public function getOrder()
    {
        return 1;
    }

}
