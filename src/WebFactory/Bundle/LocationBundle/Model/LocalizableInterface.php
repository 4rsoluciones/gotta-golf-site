<?php

namespace WebFactory\Bundle\LocationBundle\Model;

/**
 * Localizable
 */
interface LocalizableInterface
{
    /**
     * Retorna entidad City
     * 
     * @return \WebFactory\Bundle\LocationBundle\Entity\City
     */
    public function getCity();
}
