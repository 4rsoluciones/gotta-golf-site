<?php

namespace WebFactory\Bundle\LocationBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;

class DefaultController extends Controller
{

    public function statesByCountryAction()
    {
        $data = $this->getRequest()->get('country');
        $qb = $this->getDoctrine()->getRepository('WebFactoryLocationBundle:State')->getQueryBuilderByCountry($data);
        $entities = $qb->getQuery()->getResult();

        $messages = array(
            'not_found' => 'No states found for that country',
        );

        return $this->render('WebFactoryLocationBundle:Default:options.html.twig', array('entities' => $entities, 'messages' => $messages));
    }
    
    public function citiesByStateAction()
    {
        $data = $this->getRequest()->get('state');
        $qb = $this->getDoctrine()->getRepository('WebFactoryLocationBundle:City')->getQueryBuilderByState($data);
        $entities = $qb->getQuery()->getResult();

        $messages = array(
            'not_found' => 'No cities found for that state',
        );

        return $this->render('WebFactoryLocationBundle:Default:options.html.twig', array('entities' => $entities, 'messages' => $messages));
    }

}
