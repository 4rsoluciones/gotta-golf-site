<?php

namespace WebFactory\Bundle\LocationBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Validator\Constraints as Assert;
use Symfony\Bridge\Doctrine\Validator\Constraints\UniqueEntity;

use Gedmo\Mapping\Annotation as Gedmo;
use APY\DataGridBundle\Grid\Mapping as Grid;

/**
 * WebFactory\LocationBundle\Entity\State
 *
 * @ORM\Table(name="states")
 * @ORM\Entity(repositoryClass="\WebFactory\Bundle\LocationBundle\Repository\State")
 * @UniqueEntity({"name", "country"})
 * //@Gedmo\SoftDeleteable(fieldName="deletedAt")
 */
class State
{
    /**
     * @var integer $id
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     *  @Grid\Column(filterable=false)
     */
    private $id;

    /**
     * @var string $name
     *
     * @ORM\Column(name="name", type="string", length=64)
     * @Assert\NotBlank()
     * @Assert\Length(max=64)
     * @Grid\Column(operators={"like","eq", "rlike", "llike"})
     */
    private $name;
    
    /**
     * @var Country $country
     * 
     * @ORM\ManyToOne(targetEntity="Country")
     * @ORM\JoinColumn(name="country_id", referencedColumnName="id", nullable=false)
     * @Assert\NotBlank()
     */
    private $country;
    
    /**
     * Momento de eliminación de la entidad
     * Borrado lógico, manejado por SoftDeleteable
     * 
     * @var \DateTime
     * @ORM\Column(name="deleted_at", type="datetime", nullable=true)
     * @Grid\Column(filterable=false, visible=false)
     */
    protected $deletedAt;


    /**
     * Get id
     *
     * @return integer 
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set name
     *
     * @param string $name
     */
    public function setName($name)
    {
        $this->name = $name;
    }

    /**
     * Get name
     *
     * @return string 
     */
    public function getName()
    {
        return $this->name;
    }

    /**
     * Set country
     *
     * @param Country $country
     */
    public function setCountry(Country $country)
    {
        $this->country = $country;
    }

    /**
     * Get country
     *
     * @return Country 
     */
    public function getCountry()
    {
        return $this->country;
    }
    
    /**
     * Representación de la provincia como texto
     * 
     * @return string
     */
    public function __toString() 
    {
        return $this->name;
    }

    /**
     * Representación de la provincia como texto
     * 
     * @return string
     */
    public function getNiceName() 
    {
        return $this->name . ', ' . $this->getCountry()->getName();
    }

    /**
     * Representación de la provincia como texto
     * 
     * @return string
     */
    public function getNiceNameReverse() 
    {
        return $this->getCountry()->getName() . ', ' . $this->name;
    }
    
    /**
     * 
     * @return \DateTime
     */
    public function getDeletedAt()
    {
        return $this->deletedAt;
    }

    /**
     * 
     * @param \DateTime $deletedAt
     */
    public function setDeletedAt($deletedAt)
    {
        $this->deletedAt = $deletedAt;
    }
}