<?php

namespace WebFactory\Bundle\LocationBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Validator\Constraints as Assert;

/**
 * WebFactory\Bundle\LocationBundle\Entity\Address
 *
 * @ORM\Table(name="addresses")
 * @ORM\Entity
 */
class Address implements \WebFactory\Bundle\LocationBundle\Model\LocalizableInterface
{

    /**
     * @var integer $id
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @var string $street
     *
     * @ORM\Column(name="street", type="string", length=128)
     * @Assert\NotBlank()
     * @Assert\Length(max=128)
     */
    private $street;

    /**
     * @var string $number
     *
     * @ORM\Column(name="number", type="string", length=16)
     * @Assert\NotBlank()
     * @Assert\Length(max=16)
     */
    private $number;

    /**
     * @var string $floor
     *
     * @ORM\Column(name="floor", type="string", length=16, nullable=true)
     * @Assert\Length(max=16)
     */
    private $floor;

    /**
     * @var string $dpto
     *
     * @ORM\Column(name="dpto", type="string", length=16, nullable=true)
     * @Assert\Length(max=16)
     */
    private $dpto;

    /**
     * @var string $neighborhood
     *
     * @ORM\Column(name="neighborhood", type="string", length=255, nullable=true)
     * @Assert\Length(max=255)
     */
    private $neighborhood;

    /**
     * @var City $city
     * 
     * @ORM\ManyToOne(targetEntity="City")
     * @ORM\JoinColumn(name="city_id", referencedColumnName="id", nullable=false)
     * @Assert\NotBlank()
     */
    private $city;


    /**
     * Get id
     *
     * @return integer 
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set street
     *
     * @param string $street
     * @return Address
     */
    public function setStreet($street)
    {
        $this->street = $street;

        return $this;
    }

    /**
     * Get street
     *
     * @return string 
     */
    public function getStreet()
    {
        return $this->street;
    }

    /**
     * Set number
     *
     * @param string $number
     * @return Address
     */
    public function setNumber($number)
    {
        $this->number = $number;

        return $this;
    }

    /**
     * Get number
     *
     * @return string 
     */
    public function getNumber()
    {
        return $this->number;
    }

    /**
     * Set floor
     *
     * @param string $floor
     * @return Address
     */
    public function setFloor($floor)
    {
        $this->floor = $floor;

        return $this;
    }

    /**
     * Get floor
     *
     * @return string 
     */
    public function getFloor()
    {
        return $this->floor;
    }

    /**
     * Set dpto
     *
     * @param string $dpto
     * @return Address
     */
    public function setDpto($dpto)
    {
        $this->dpto = $dpto;

        return $this;
    }

    /**
     * Get dpto
     *
     * @return string 
     */
    public function getDpto()
    {
        return $this->dpto;
    }

    /**
     * Set neighborhood
     *
     * @param string $neighborhood
     * @return Address
     */
    public function setNeighborhood($neighborhood)
    {
        $this->neighborhood = $neighborhood;

        return $this;
    }

    /**
     * Get neighborhood
     *
     * @return string 
     */
    public function getNeighborhood()
    {
        return $this->neighborhood;
    }

    /**
     * Set city
     *
     * @param City $city
     * @return Address
     */
    public function setCity(City $city)
    {
        $this->city = $city;

        return $this;
    }

    /**
     * Get city
     *
     * @return City 
     */
    public function getCity()
    {
        return $this->city;
    }

    /**
     * 
     * @return string
     */
    public function __toString()
    {
        return sprintf('%s %s %s %s %s - %s', $this->street, $this->number, $this->dpto, $this->floor, $this->neighborhood, $this->city->getNiceName());
    }

    /**
     * 
     * @return string
     */
    public function getNiceNameReverse()
    {
        $city = $this->getCity()->getNiceNameReverse();

        return sprintf('%s - %s %s %s %s %s', $city, $this->street, $this->number, $this->dpto, $this->floor, $this->neighborhood);
    }

}
