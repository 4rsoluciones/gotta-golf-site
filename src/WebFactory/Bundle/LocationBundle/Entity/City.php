<?php

namespace WebFactory\Bundle\LocationBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Validator\Constraints as Assert;
use APY\DataGridBundle\Grid\Mapping as Grid;
use Symfony\Bridge\Doctrine\Validator\Constraints as DoctrineAssert;
use Gedmo\Mapping\Annotation as Gedmo;

/**
 * WebFactory\Bundle\LocationBundle\Entity\City
 *
 * @ORM\Table(name="cities")
 * @ORM\Entity(repositoryClass="\WebFactory\Bundle\LocationBundle\Repository\City")
 * @Grid\Source(columns="id, state.name, name, zip_code", groupBy={"id"})
 * @DoctrineAssert\UniqueEntity({"zip_code"})
 */
class City
{

    /**
     * @var integer $id
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     * @Grid\Column(filterable=false)
     */
    private $id;

    /**
     * @var string $name
     *
     * @ORM\Column(name="name", type="string", length=128)
     * @Assert\NotBlank()
     * @Assert\Length(max=128)
     * @Grid\Column(title="Name")
     * @Grid\Column(operators={"like","eq", "rlike", "llike"})
     */
    private $name;

    /**
     * @var string $zip_code
     *
     * @ORM\Column(name="zip_code", type="string", length=16, unique=true, nullable=true)
     * @Assert\Length(max=16)
     * @Grid\Column(title="Zip Code")
     */
    private $zip_code;

    /**
     * @var State $state
     * 
     * @ORM\ManyToOne(targetEntity="State")
     * @ORM\JoinColumn(name="state_id", referencedColumnName="id", nullable=false)
     * @Assert\NotBlank()
     * @Grid\Column(title="State", field="state.name")
     */
    private $state;

    /**
     * Momento de eliminación de la entidad
     * Borrado lógico, manejado por SoftDeleteable
     * 
     * @var \DateTime
     * @ORM\Column(name="deleted_at", type="datetime", nullable=true)
     */
    protected $deletedAt;

    /**
     * Get id
     *
     * @return integer
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set name
     *
     * @param string $name
     */
    public function setName($name)
    {
        $this->name = $name;
    }

    /**
     * Get name
     *
     * @return string
     */
    public function getName()
    {
        return $this->name;
    }

    /**
     * Set zip_code
     *
     * @param string $zipCode
     */
    public function setZipCode($zipCode)
    {
        $this->zip_code = $zipCode;
    }

    /**
     * Get zip_code
     *
     * @return string
     */
    public function getZipCode()
    {
        return $this->zip_code;
    }

    public function __toString()
    {
        return $this->name . ' (' . $this->zip_code . ')';
    }

    /**
     * 
     * @return string
     */
    public function getNiceName()
    {
        if ($this->zip_code) {
            $string = sprintf('%s (%s), %s', $this->name, $this->zip_code, $this->getState()->getNiceName());
        } else {
            $string = sprintf('%s, %s', $this->name, $this->getState()->getNiceName());
        }

        return $string;
    }
    /**
     * 
     * @return string
     */
    public function getNiceNameReverse()
    {
        if ($this->zip_code) {
            $string = sprintf('%s, %s (%s)', $this->getState()->getNiceNameReverse(), $this->getName(), $this->zip_code);
        } else {
            $string = sprintf('%s, %s', $this->getState()->getNiceNameReverse(), $this->getName());
        }

        return $string;
    }

    /**
     * Set state
     *
     * @param State $state
     */
    public function setState(State $state)
    {
        $this->state = $state;
    }

    /**
     * Get state
     *
     * @return State 
     */
    public function getState()
    {
        return $this->state;
    }

    /**
     * 
     * @return \DateTime
     */
    public function getDeletedAt()
    {
        return $this->deletedAt;
    }

    /**
     * 
     * @param \DateTime $deletedAt
     */
    public function setDeletedAt($deletedAt)
    {
        $this->deletedAt = $deletedAt;
    }

}