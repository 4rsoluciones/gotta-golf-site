<?php

namespace WebFactory\Bundle\LocationBundle\Repository;

use Doctrine\ORM\EntityRepository;
use WebFactory\Bundle\LocationBundle\Entity\State as StateEntity;

/**
 * City
 */
class City extends EntityRepository
{

    public function getQueryBuilderByState($state)
    {
        $qb = $this->createQueryBuilder('city')
                ->innerJoin('city.state', 'state')
                ->orderBy('city.name', 'asc')
                ;

        if ($state instanceof StateEntity) {
            $qb->where('city.state = :state')
                    ->setParameter('state', $state);
        } elseif (is_numeric($state)) {
            $qb->where('state.id = :state')
                    ->setParameter('state', $state);
        } else {
            $qb->where('state.name = :state')
                    ->setParameter('state', null);
        }

        return $qb;
    }

    public function findByTerms($terms)
    {
        $qb = $this->createQueryBuilder('city')
                ->innerJoin('city.state', 'state');

        if ($state instanceof StateEntity) {
            $qb->where('city.state = :state')
                    ->setParameter('state', $state);
        } elseif (is_numeric($state)) {
            $qb->where('state.id = :state')
                    ->setParameter('state', $state);
        } else {
            $qb->where('state.name = :state')
                    ->setParameter('state', null);
        }

        return $qb;
    }

}
