<?php

namespace WebFactory\Bundle\LocationBundle\Repository;

use Doctrine\ORM\EntityRepository;

/**
 * Address
 */
class Address extends EntityRepository
{

    public function findAllOrderById($returnQueryBuilder = false)
    {
        $qb = $this->createQueryBuilder('address')
              ->orderBy('address.id', 'asc');
        
        if ($returnQueryBuilder) {
            return $qb;
        }
        
        return $qb->getQuery()->getResult();
    }

}
