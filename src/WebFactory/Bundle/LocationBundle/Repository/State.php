<?php

namespace WebFactory\Bundle\LocationBundle\Repository;

use Doctrine\ORM\EntityRepository;
use Doctrine\ORM\NonUniqueResultException;
use WebFactory\Bundle\LocationBundle\Entity\Country as CountryEntity;

/**
 * State
 */
class State extends EntityRepository
{

    public function getQueryBuilderByCountry($country)
    {
        $qb = $this->createQueryBuilder('state')
                ->innerJoin('state.country', 'country')
                ->orderBy('state.name', 'asc')
                ;

        if ($country instanceof CountryEntity) {
            $qb->where('state.country = :country')
                    ->setParameter('country', $country);
        } elseif (is_numeric($country)) {
            $qb->where('country.id = :country')
                    ->setParameter('country', $country);
        } else {
            $qb->where('country.name = :country')
                    ->setParameter('country', null);
        }

        return $qb;
    }
    
    public function findAllOrderByName($returnQueryBuilder = false)
    {
        $qb = $this->createQueryBuilder('state')
              ->orderBy('state.name', 'asc');
        
        if ($returnQueryBuilder) {
            return $qb;
        }
        
        return $qb->getQuery()->getResult();
    }

    /**
     * @param $name
     * @return \WebFactory\Bundle\LocationBundle\Entity\State
     */
    public function findOneByName($name)
    {
        $qb = $this->createQueryBuilder('State')
            ->where('State.name = :name')
            ->setParameter('name', $name);

        try {
            return $qb->getQuery()->getOneOrNullResult();
        } catch (NonUniqueResultException $e) {
            return null;
        }
    }
}
