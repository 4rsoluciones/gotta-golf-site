<?php

namespace WebFactory\Bundle\LocationBundle\Form;

use Doctrine\ORM\EntityRepository;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\Form\FormEvent;
use Symfony\Component\Form\FormEvents;
use WebFactory\Bundle\LocationBundle\Model\LocalizableInterface;
use WebFactory\Bundle\LocationBundle\Entity\State;

class SimpleLocationType extends AbstractType
{

    public function buildForm(FormBuilderInterface $builder, array $options)
    {

        $refreshCountries = function ($form, $country) {
                    $form->add('country', 'entity', array(
                        'class' => 'WebFactory\Bundle\LocationBundle\Entity\Country',
                        'property' => 'name',
                        'mapped' => false,
                        'empty_value' => '-- Select a country --',
                        'data' => $country,
                    ));
                };

        $refreshStates = function ($form, $country, $state) {
                    $form->add('state', 'entity', array(
                        'class' => 'WebFactory\Bundle\LocationBundle\Entity\State',
                        'property' => 'name',
                        'empty_value' => '-- Select a state --',
                        'data' => $state,
                        'query_builder' => function (EntityRepository $repository) use ($country) {
                            return $repository->getQueryBuilderByCountry($country);
                        }
                    ));
                };

        $refreshCountries($builder, null);
        $refreshStates($builder, null, null);

        $builder->addEventListener(FormEvents::PRE_SET_DATA, function (FormEvent $event) use ($refreshStates, $refreshCountries) {
                    $form = $event->getForm();
                    $data = $event->getData();

                    if ($data == null) {
                        return;
                    }

                    if ($data instanceof LocalizableInterface) {
                        $data = $data->getState();
                    }

                    if ($data instanceof State) {
                        $country = ($data->getId()) ? $data->getCountry() : null;
                        $state = ($data->getId()) ? $data : null;
                    } else {
                        $country = null;
                        $state = null;
                    }

                    $refreshCountries($form, $country);
                    $refreshStates($form, $country, $state);
                });

        $builder->addEventListener(FormEvents::PRE_BIND, function (FormEvent $event) use ($refreshStates, $refreshCountries) {
                    $form = $event->getForm();
                    $data = $event->getData();

                    if ($data && array_key_exists('country', $data)) {
                        $country = $data['country'];
                        $state = $data['state'];
                    } else {
                        $country = null;
                        $state = null;
                    }

                    $refreshCountries($form, $country);
                    $refreshStates($form, $country, $state);
                });
    }

    public function getName()
    {
        return 'location';
    }

}
