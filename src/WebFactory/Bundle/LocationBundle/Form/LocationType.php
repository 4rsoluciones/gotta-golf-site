<?php

namespace WebFactory\Bundle\LocationBundle\Form;

use Doctrine\ORM\EntityRepository;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\Form\FormEvent;
use Symfony\Component\Form\FormEvents;
use WebFactory\Bundle\LocationBundle\Entity\City;
use WebFactory\Bundle\LocationBundle\Model\LocalizableInterface;

class LocationType extends AbstractType
{

    public function buildForm(FormBuilderInterface $builder, array $options)
    {

        $refreshCountries = function ($form, $country) {
                    $form->add('country', 'entity', array(
                        'class' => 'WebFactory\Bundle\LocationBundle\Entity\Country',
                        'property' => 'name',
                        'mapped' => false,
                        'empty_value' => '-- Select a country --',
                        'data' => $country,
                    ));
                };

        $refreshStates = function ($form, $country, $state) {
                    $form->add('state', 'entity', array(
                        'class' => 'WebFactory\Bundle\LocationBundle\Entity\State',
                        'property' => 'name',
                        'mapped' => false,
                        'empty_value' => '-- Select a state --',
                        'data' => $state,
                        'query_builder' => function (EntityRepository $repository) use ($country) {
                            return $repository->getQueryBuilderByCountry($country);
                        }
                    ));
                };

        $refreshCities = function ($form, $state, $city) {
                    $form->add('city', 'entity', array(
                        'class' => 'WebFactory\Bundle\LocationBundle\Entity\City',
                        'property' => 'name',
                        'empty_value' => '-- Select a city --',
                        'data' => $city,
                        'query_builder' => function (EntityRepository $repository) use ($state) {
                            return $repository->getQueryBuilderByState($state);
                        }
                    ));
                };

        $refreshCountries($builder, null);
        $refreshStates($builder, null, null);
        $refreshCities($builder, null, null);

        $builder->addEventListener(FormEvents::PRE_SET_DATA, function (FormEvent $event) use ($refreshStates, $refreshCities, $refreshCountries) {
                    $form = $event->getForm();
                    $data = $event->getData();

                    if ($data == null) {
                        return;
                    }

                    if ($data instanceof LocalizableInterface) {
                        $data = $data->getCity();
                    }

                    if ($data instanceof City) {
                        $country = ($data->getId()) ? $data->getState()->getCountry() : null;
                        $state = ($data->getId()) ? $data->getState() : null;
                        $city = ($data->getId()) ? $data : null;
                    } else {
                        $country = null;
                        $state = null;
                        $city = null;
                    }

                    $refreshCountries($form, $country);
                    $refreshStates($form, $country, $state);
                    $refreshCities($form, $state, $city);
                });

        $builder->addEventListener(FormEvents::PRE_BIND, function (FormEvent $event) use ($refreshStates, $refreshCities, $refreshCountries) {
                    $form = $event->getForm();
                    $data = $event->getData();

                    if ($data && array_key_exists('country', $data)) {
                        $country = $data['country'];
                        $state = $data['state'];
                        $city = $data['city'];
                    } else {
                        $country = null;
                        $state = null;
                        $city = null;
                    }

                    $refreshCountries($form, $country);
                    $refreshStates($form, $country, $state);
                    $refreshCities($form, $state, $city);
                });
    }

    public function getName()
    {
        return 'location';
    }

}
