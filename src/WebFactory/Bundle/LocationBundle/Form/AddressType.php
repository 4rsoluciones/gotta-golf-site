<?php

namespace WebFactory\Bundle\LocationBundle\Form;

use WebFactory\Bundle\LocationBundle\Form\LocationType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolverInterface;

class AddressType extends LocationType
{

    /**
     * @param FormBuilderInterface $builder
     * @param array $options
     */
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        
        parent::buildForm($builder, $options);
        $builder
//                ->add('city')
                ->add('street')
                ->add('number')
                ->add('floor')
                ->add('dpto')
                ->add('neighborhood')
        ;
    }

    /**
     * @param OptionsResolverInterface $resolver
     */
    public function setDefaultOptions(OptionsResolverInterface $resolver)
    {
        $resolver->setDefaults(array(
            'data_class' => 'WebFactory\Bundle\LocationBundle\Entity\Address'
        ));
    }

    /**
     * @return string
     */
    public function getName()
    {
        return 'address';
    }

}
