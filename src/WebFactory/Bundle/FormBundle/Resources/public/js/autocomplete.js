
$(function() {
    function formatResponse (response) {

        if (response.loading) return response.text;

        var markup = "<div class='select2-result-repository clearfix'>" +
            "<div class='select2-result-repository__meta'>" +
            "<div class='select2-result-repository__title'>" + response.fullName + "</div>";

        return markup;
    }

    $('.autocomplete').each(function(i, el) {
        var $this = $(el);
        var $target = $('#' +$this.data('id'));
        $this.select2({
            ajax: {
                url: $this.data('url'),
                dataType: 'json',
                data: function (params) {
                    return {
                        q: params.term, // search term
                        page: params.page
                    };
                },
                processResults: function (data, params) {
                    params.page = params.page || 1;

                    return {
                        results: data,
                        pagination: {
                            more: (params.page * 30) < data.total_count
                        }
                    };
                }
            },
            escapeMarkup: function (markup) { return markup; }, // let our custom formatter work
            minimumInputLength: 3,
            templateResult: formatResponse, // omitted for brevity, see the source of this page
            templateSelection: function (response) {return response.fullName || response.text;}
        });
    });
});
