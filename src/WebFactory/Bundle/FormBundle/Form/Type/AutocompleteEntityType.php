<?php

namespace WebFactory\Bundle\FormBundle\Form\Type;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\Form\FormInterface;
use Symfony\Component\Form\FormView;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Doctrine\Common\Persistence\ObjectManager;
use WebFactory\Bundle\FormBundle\Form\DataTransformer\EntityToIdTransformer;

/**
 * Class AutocompleteEntityType
 * @package WebFactory\Bundle\FormBundle\Form\Type
 */
class AutocompleteEntityType extends AbstractType
{

    /**
     * @var ObjectManager
     */
    private $om;

    /**
     * AutocompleteEntityType constructor.
     * @param ObjectManager $om
     */
    public function __construct(ObjectManager $om)
    {
        $this->om = $om;
    }

    /**
     * @param FormBuilderInterface $builder
     * @param array $options
     */
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $class = $options['class'];
        $multiple = $options['multiple'];

        $transformer = new EntityToIdTransformer($this->om, $class, $multiple);
        $builder->addViewTransformer($transformer);
    }

    /**
     * @param FormView $view
     * @param FormInterface $form
     * @param array $options
     */
    public function buildView(FormView $view, FormInterface $form, array $options)
    {
        $view->vars['update_route'] = $options['update_route'];
        $view->vars['property'] = $options['property'];
        $view->vars['multiple'] = $options['multiple'];
        if ($options['multiple'] && is_array($view->vars['value'])) {
            $view->vars['value'] = implode(',', $view->vars['value']);
        }
        
    }

    /**
     * @param OptionsResolver $resolver
     */
    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setRequired(array('class', 'update_route', 'property'));
        $resolver->setDefaults(array('multiple' => false));
    }


    /**
     * @return string
     */
    public function getParent()
    {
        return 'entity';
    }

    /**
     * @return string
     */
    public function getName()
    {
        return 'autocomplete_entity';
    }

}
