<?php

namespace WebFactory\Bundle\FormBundle\Form\DataTransformer;

use Doctrine\Common\Persistence\ObjectManager;
use Symfony\Component\Form\DataTransformerInterface;
use Symfony\Component\Form\Exception\TransformationFailedException;

/**
 * Class EntityToIdTransformer
 * @package WebFactory\Bundle\FormBundle\Form\DataTransformer
 */
class EntityToIdTransformer implements DataTransformerInterface
{

    /**
     * @var ObjectManager
     */
    private $om;

    /**
     * @var string
     */
    private $entityClass;

    /**
     * @var boolean
     */
    private $multiple;

    /**
     * EntityToIdTransformer constructor.
     * @param ObjectManager $om
     * @param string $entityClass
     * @param boolean $multiple
     */
    public function __construct(ObjectManager $om, $entityClass, $multiple)
    {
        $this->om = $om;
        $this->entityClass = $entityClass;
        $this->multiple = $multiple;
    }

    /**
     * @param mixed $entity
     * @return array|string
     */
    public function reverseTransform($entity)
    {
        if (!$entity) {
            return "";
        }

        if ($this->multiple) {
            $ids = array();
            foreach ($entity as $e) {
                $ids[] = $e->getId();
            }

            return $ids;
        }

        return $entity;
    }

    /**
     * @param mixed $id
     * @return array|object
     */
    public function transform($id)
    {
        if (!$id) {
            return "";
        }
        if ($this->multiple) {
            $collection = array();
            foreach (explode(',', $id) as $i) {
                $entity = $this->om->getRepository($this->entityClass)->find($i);
                if (null === $entity) {
                    throw new TransformationFailedException(sprintf('There is no entity of %s with id %s', $this->entityClass, $i));
                }
                $collection[] = $entity;
            }

            return $collection;
        }

        $entity = $this->om->getRepository($this->entityClass)->find($id);

        if (null === $entity) {
            throw new TransformationFailedException(sprintf('There is no entity of %s with id %s', $this->entityClass, $id));
        }

        return $entity;
    }

}
