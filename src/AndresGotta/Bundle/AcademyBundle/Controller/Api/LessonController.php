<?php

namespace AndresGotta\Bundle\AcademyBundle\Controller\Api;

use AndresGotta\Academy\Application\AcademyContext;
use AndresGotta\Academy\Application\Command\SetLessonFeedbackCommand;
use AndresGotta\Academy\Domain\Entity\Lesson;
use AndresGotta\Academy\Domain\Entity\StudentInterface;
use AndresGotta\Bundle\AcademyBundle\Form\AddLessonFeedbackType;
use AndresGotta\Bundle\AcademyBundle\Form\EditLessonType;
use Nelmio\ApiDocBundle\Annotation\ApiDoc;
use AndresGotta\Academy\Application\Command\AddLessonCommand;
use FOS\RestBundle\Controller\Annotations as Rest;
use AndresGotta\Academy\Domain\Entity\TeacherInterface;
use AndresGotta\Academy\Domain\Repository\LessonRepositoryInterface;
use AndresGotta\Bundle\AcademyBundle\Form\AddLessonType;
use FOS\RestBundle\Controller\FOSRestController;
use Symfony\Component\HttpFoundation\Request;
use WebFactory\Bundle\UserBundle\Entity\Repository\UserRepository;
use WebFactory\Bundle\UserBundle\Entity\User;

/**
 * Class LessonController
 */
class LessonController extends FOSRestController
{
    /**
     * Listado de estudiantes del docente
     *
     * @ApiDoc(
     *  section="teacher",
     *  resource=true,
     *  https=true,
     *  authentication=true,
     *  authenticationRoles={"ROLE_TEACHER"},
     *  tags={"students"},
     *  description="Listado de estudiantes del docente",
     *  output= {
     *      "class"="array<WebFactory\Bundle\UserBundle\Entity\User>",
     *      "groups" = {},
     *      "parsers"={
     *          "Nelmio\ApiDocBundle\Parser\JmsMetadataParser"
     *      }
     *  },
     * )
     *
     * @Rest\View(serializerGroups={"info", "login"}, serializerEnableMaxDepthChecks=true)
     * @Rest\QueryParam(name="limit", description="Limit", default="20", requirements="\d+")
     * @Rest\QueryParam(name="offset", description="Offset", default="0", requirements="\d+")
     */
    public function getTeachersStudentsAction(Request $request)
    {
        $teacher = $this->getUser();
        /** @var AcademyContext $context */
        $context = $request->attributes->get('academy_context');
        $academy = $context->getAcademy();
        $offset = $request->get('offset', 0);
        $limit = $request->get('limit', 20);

        /* @var UserRepository $repository */
        $repository = $this->getDoctrine()->getRepository('WebFactoryUserBundle:User');
        $students = $repository->findAllStudentsByTeacher($teacher, $academy, $offset, $limit);

        return compact('students');
    }

    /**
     * Permite recuperar las clases de un docente
     *
     * @ApiDoc(
     *  section="teacher",
     *  resource=true,
     *  https=true,
     *  authentication=true,
     *  authenticationRoles={"ROLE_TEACHER"},
     *  tags={"lessons"},
     *  description="Listado de clases del docente",
     *  output= {
     *      "class"="array<AndresGotta\Academy\Domain\Entity\Lesson>",
     *      "groups" = {"info"},
     *      "parsers"={
     *          "Nelmio\ApiDocBundle\Parser\JmsMetadataParser"
     *      }
     *  },
     * )
     *
     * @Rest\View(serializerGroups={"info", "login"}, serializerEnableMaxDepthChecks=true)
     * @Rest\QueryParam(name="limit", description="Limit", default="20", requirements="\d+")
     * @Rest\QueryParam(name="offset", description="Offset", default="0", requirements="\d+")
     */
    public function getTeacherLessonsAction(Request $request)
    {
        /* @var TeacherInterface $teacher */
        $teacher = $this->getUser();

        /** @var AcademyContext $context */
        $context = $request->attributes->get('academy_context');
        $academy = $context->getAcademy();
        $offset = $request->get('offset', 0);
        $limit = $request->get('limit', 20);

        /* @var LessonRepositoryInterface $lessonRepository */
        $lessonRepository = $this->get('lesson.repository');
        $lessons = $lessonRepository->findAllByTeacher($teacher, $academy, $offset, $limit);

        return compact('lessons');
    }

    /**
     * Permite recuperar las clases de un estudiante
     *
     * @ApiDoc(
     *  section="student",
     *  resource=true,
     *  https=true,
     *  authentication=true,
     *  authenticationRoles={"ROLE_STUDENT"},
     *  tags={"lessons"},
     *  description="Listado de clases del estudiante",
     *  output= {
     *      "class"="array<AndresGotta\Academy\Domain\Entity\Lesson>",
     *      "groups" = {},
     *      "parsers"={
     *          "Nelmio\ApiDocBundle\Parser\JmsMetadataParser"
     *      }
     *  },
     * )
     *
     * @Rest\View(serializerGroups={"info", "login"}, serializerEnableMaxDepthChecks=true)
     * @Rest\QueryParam(name="limit", description="Limit", default="20", requirements="\d+")
     * @Rest\QueryParam(name="offset", description="Offset", default="0", requirements="\d+")
     */
    public function getStudentLessonsAction(Request $request)
    {
        /* @var StudentInterface $student */
        $student = $this->getUser();

        /** @var AcademyContext $context */
        $context = $request->attributes->get('academy_context');
        $academy = $context->getAcademy();
        $offset = $request->get('offset', 0);
        $limit = $request->get('limit', 20);

        /* @var LessonRepositoryInterface $lessonRepository */
        $lessonRepository = $this->get('lesson.repository');
        $lessons = $lessonRepository->findAllByStudent($student, $academy, $offset, $limit);

        return compact('lessons');
    }

    /**
     * Permite recuperar las clases de un estudiante
     *
     * @ApiDoc(
     *  section="teacher",
     *  resource=true,
     *  https=true,
     *  authentication=true,
     *  authenticationRoles={"ROLE_TEACHER"},
     *  tags={"lessons"},
     *  description="Listado de clases del estudiante",
     *  output= {
     *      "class"="array<AndresGotta\Academy\Domain\Entity\Lesson>",
     *      "groups" = {},
     *      "parsers"={
     *          "Nelmio\ApiDocBundle\Parser\JmsMetadataParser"
     *      }
     *  },
     * )
     *
     * @Rest\View(serializerGroups={"info", "login"}, serializerEnableMaxDepthChecks=true)
     * @Rest\QueryParam(name="limit", description="Limit", default="20", requirements="\d+")
     * @Rest\QueryParam(name="offset", description="Offset", default="0", requirements="\d+")
     */
    public function getStudentLessonsForTeacherAction(Request $request, User $student)
    {
        /* @var StudentInterface $student */
        /* @var AcademyContext $context */
        $context = $request->attributes->get('academy_context');
        $academy = $context->getAcademy();
        $offset = $request->get('offset', 0);
        $limit = $request->get('limit', 20);

        /* @var LessonRepositoryInterface $lessonRepository */
        $lessonRepository = $this->get('lesson.repository');
        $lessons = $lessonRepository->findAllByStudent($student, $academy, $offset, $limit);

        return compact('lessons');
    }

    /**
     * Permite agregar una clase
     *
     * @ApiDoc(
     *  section="teacher",
     *  resource=true,
     *  https=true,
     *  authentication=true,
     *  authenticationRoles={"ROLE_TEACHER"},
     *  tags={"lessons"},
     *  description="Agregar una clase",
     *  input= {
     *      "class"="AndresGotta\Bundle\AcademyBundle\Form\AddLessonType"
     *  },
     * )
     *
     * @Rest\View
     */
    public function addLessonAction(Request $request)
    {
        /* @var TeacherInterface $teacher */
        $teacher = $this->getUser();

        $command = new AddLessonCommand('', new \DateTime(), null, $teacher);
        $form = $this->createForm(new AddLessonType(), $command);
        $form->submit($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $command = $form->getData();
            $lessons = $this->get('tactician.commandbus.default')->handle($command);

            $this->get('academy.new_lesson.notifier')->notify($lessons);

            return [];
        }

        return ['form' => $form];
    }

    /**
     * Permite editar una clase
     *
     * @ApiDoc(
     *  section="teacher",
     *  resource=true,
     *  https=true,
     *  authentication=true,
     *  authenticationRoles={"ROLE_TEACHER"},
     *  tags={"lesson"},
     *  description="Edita una clase",
     *  input= {
     *      "class"="AndresGotta\Bundle\AcademyBundle\Form\EditLessonType"
     *  },
     * )
     *
     * @Rest\View(serializerGroups={"info", "login"}, serializerEnableMaxDepthChecks=true)
     */
    public function editLessonAction(Request $request, Lesson $lesson)
    {
        /** @var AcademyContext $context */
        $context = $request->attributes->get('academy_context');

        /* @var TeacherInterface $teacher */
        $teacher = $this->getUser();

        $lessonPlace = $lesson->getPlace();
        $academy = $context->getAcademy();

        if (
            $lessonPlace->getAcademy() !== $academy ||
            $lesson->getTeacher() !== $teacher
        ) {
            throw $this->createAccessDeniedException();
        }

        $form = $this->createForm(new EditLessonType(), $lesson, ['places' => $academy->getPlaces()->toArray()]);
        $form->submit($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $indications = $form->get('indications')->getData();
            $report = $lesson->getReport();
            $report->setIndications($indications);

            $repository = $this->get('lesson.repository');
            $repository->editLesson($lesson);

            return $lesson;
        }

        return ['form' => $form];
    }

    /**
     * Permite completar el feedback de un reporte de una clase
     *
     * @ApiDoc(
     *  section="student",
     *  resource=true,
     *  https=true,
     *  authentication=true,
     *  authenticationRoles={"ROLE_STUDENT"},
     *  tags={"lessons"},
     *  description="Completar feedback de clase",
     *  input= {
     *      "class"="AndresGotta\Bundle\AcademyBundle\Form\AddLessonFeedbackType"
     *  },
     * )
     *
     * @Rest\View
     */
    public function addLessonFeedbackAction(Request $request)
    {
        /* @var StudentInterface $student */
        $student = $this->getUser();

        /** @var AcademyContext $context */
        $context = $request->attributes->get('academy_context');
        $academy = $context->getAcademy();

        /* @var LessonRepositoryInterface $lessonRepository */
        $lessonRepository = $this->get('lesson.repository');
        $lessons = $lessonRepository->findAllByStudent($student, $academy);

        $command = new SetLessonFeedbackCommand();
        $form = $this->createForm(new AddLessonFeedbackType(), $command, [
            'lessons' => $lessons,
        ]);
        $form->submit($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $command = $form->getData();
            $this->get('tactician.commandbus.default')->handle($command);

            return [];
        }

        return ['form' => $form];
    }

    /**
     * Permite eliminar una lección
     *
     * @ApiDoc(
     *  section="teacher",
     *  resource=true,
     *  https=true,
     *  authentication=true,
     *  authenticationRoles={"ROLE_TEACHER"},
     *  tags={"lesson"},
     *  description="Eliminar lección",
     * )
     *
     * @Rest\View()
     */
    public function removeLessonAction(Request $request, Lesson $lesson)
    {
        if ($this->getUser() != $lesson->getTeacher()) {
            throw $this->createAccessDeniedException();
        }

        $em = $this->getDoctrine()->getManager();
        $em->remove($lesson);
        $em->flush();

        return;
    }
}
