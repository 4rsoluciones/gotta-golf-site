<?php

namespace AndresGotta\Bundle\AcademyBundle\Controller\Api;

use AndresGotta\Academy\Application\AcademyContext;
use AndresGotta\Academy\Application\Command\AddRoutineCommand;
use AndresGotta\Academy\Application\Command\SetRoutineFeedbackCommand;
use AndresGotta\Academy\Application\Command\SetRoutineStepStatusCommand;
use AndresGotta\Academy\Domain\Entity\Routine;
use AndresGotta\Academy\Domain\Entity\StudentInterface;
use AndresGotta\Academy\Domain\Repository\RoutineRepositoryInterface;
use AndresGotta\Bundle\AcademyBundle\Form\AddRoutineFeedbackType;
use AndresGotta\Bundle\AcademyBundle\Form\AddRoutineType;
use AndresGotta\Bundle\AcademyBundle\Form\EditRoutineType;
use AndresGotta\Bundle\AcademyBundle\Form\SetRoutineStepStatusType;
use Nelmio\ApiDocBundle\Annotation\ApiDoc;
use FOS\RestBundle\Controller\Annotations as Rest;
use AndresGotta\Academy\Domain\Entity\TeacherInterface;
use FOS\RestBundle\Controller\FOSRestController;
use Symfony\Component\HttpFoundation\Request;
use WebFactory\Bundle\UserBundle\Entity\User;

class RoutineController extends FOSRestController
{

    /**
     * Permite recuperar las rutinas de un docente
     *
     * @ApiDoc(
     *  section="teacher",
     *  resource=true,
     *  https=true,
     *  authentication=true,
     *  authenticationRoles={"ROLE_TEACHER"},
     *  tags={"lessons"},
     *  description="Listado de rutinas del docente",
     *  output= {
     *      "class"="array<AndresGotta\Academy\Domain\Entity\Routine>",
     *      "groups" = {},
     *      "parsers"={
     *          "Nelmio\ApiDocBundle\Parser\JmsMetadataParser"
     *      }
     *  },
     * )
     *
     * @Rest\View(serializerGroups={"info", "login"}, serializerEnableMaxDepthChecks=true)
     * @Rest\QueryParam(name="limit", description="Limit", default="20", requirements="\d+")
     * @Rest\QueryParam(name="offset", description="Offset", default="0", requirements="\d+")
     */
    public function getTeacherRoutinesAction(Request $request)
    {
        /* @var TeacherInterface $teacher */
        $teacher = $this->getUser();

        /** @var AcademyContext $context */
        $context = $request->attributes->get('academy_context');
        $academy = $context->getAcademy();
        $offset = $request->get('offset', 0);
        $limit = $request->get('limit', 20);
        $search = $request->get('search', '');

        /* @var RoutineRepositoryInterface $routineRepository */
        $routineRepository = $this->get('routine.repository');
        $routines = $routineRepository->findAllByTeacher($teacher, $academy, $offset, $limit, $search);

        return compact('routines');
    }


    /**
     * Permite agregar una rutina
     *
     * @ApiDoc(
     *  section="teacher",
     *  resource=true,
     *  https=true,
     *  authentication=true,
     *  authenticationRoles={"ROLE_TEACHER"},
     *  tags={"lessons"},
     *  description="Agregar una rutina",
     *  input= {
     *      "class"="AndresGotta\Bundle\AcademyBundle\Form\AddRoutineType"
     *  },
     * )
     *
     * @Rest\View(serializerGroups={"info", "login"}, serializerEnableMaxDepthChecks=true)
     */
    public function addRoutineAction(Request $request)
    {
        /* @var TeacherInterface $teacher */
        $teacher = $this->getUser();

        /** @var AcademyContext $context */
        $context = $request->attributes->get('academy_context');
        $academy = $context->getAcademy();

        $command = new AddRoutineCommand('', '', $teacher, [], [], $academy);
        $form = $this->createForm(new AddRoutineType(), $command);
        $form->submit($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $command = $form->getData();
            $routines = $this->get('tactician.commandbus.default')->handle($command);

            $this->get('academy.new_routine.notifier')->notify($routines);

            return [];
        }

        return ['form' => $form];
    }

    /**
     * Permite editar una rutina
     *
     * @ApiDoc(
     *  section="teacher",
     *  resource=true,
     *  https=true,
     *  authentication=true,
     *  authenticationRoles={"ROLE_TEACHER"},
     *  tags={"lessons"},
     *  description="Editar una rutina",
     *  input= {
     *      "class"="AndresGotta\Bundle\AcademyBundle\Form\EditRoutineType"
     *  },
     * )
     *
     * @Rest\View(serializerGroups={"info", "login"}, serializerEnableMaxDepthChecks=true)
     */
    public function editRoutineAction(Request $request, Routine $routine)
    {
        if ($this->getUser() != $routine->getTeacher()) {
            throw $this->createAccessDeniedException();
        }

        $form = $this->createForm(new EditRoutineType(), $routine);
        $form->submit($request);

        if ($form->isSubmitted() && $form->isValid()) {

            $em = $this->getDoctrine()->getManager();
            $em->persist($routine);
            $em->flush();

            $this->get('academy.new_routine.notifier')->notify([$routine]);

            return [];
        }

        return ['form' => $form];
    }


    /**
     * Permite recuperar las rutinas de un estudiante
     *
     * @ApiDoc(
     *  section="student",
     *  resource=true,
     *  https=true,
     *  authentication=true,
     *  authenticationRoles={"ROLE_STUDENT"},
     *  tags={"lessons"},
     *  description="Listado de rutinas del estudiante",
     *  output= {
     *      "class"="array<AndresGotta\Academy\Domain\Entity\Routine>",
     *      "groups" = {},
     *      "parsers"={
     *          "Nelmio\ApiDocBundle\Parser\JmsMetadataParser"
     *      }
     *  },
     * )
     *
     * @Rest\View(serializerGroups={"info", "login"}, serializerEnableMaxDepthChecks=true)
     * @Rest\QueryParam(name="limit", description="Limit", default="20", requirements="\d+")
     * @Rest\QueryParam(name="offset", description="Offset", default="0", requirements="\d+")
     */
    public function getStudentRoutinesAction(Request $request)
    {
        /* @var StudentInterface $student */
        $student = $this->getUser();

        /** @var AcademyContext $context */
        $context = $request->attributes->get('academy_context');
        $academy = $context->getAcademy();
        $offset = $request->get('offset', 0);
        $limit = $request->get('limit', 20);

        /* @var RoutineRepositoryInterface $routineRepository */
        $routineRepository = $this->get('routine.repository');
        $routines = $routineRepository->findAllByStudent($student, $academy, $offset, $limit);

        return compact('routines');
    }

    /**
     * Permite recuperar las rutinas de un estudiante
     *
     * @ApiDoc(
     *  section="teacher",
     *  resource=true,
     *  https=true,
     *  authentication=true,
     *  authenticationRoles={"ROLE_TEACHER"},
     *  tags={"lessons"},
     *  description="Listado de rutinas del estudiante",
     *  output= {
     *      "class"="array<AndresGotta\Academy\Domain\Entity\Routine>",
     *      "groups" = {},
     *      "parsers"={
     *          "Nelmio\ApiDocBundle\Parser\JmsMetadataParser"
     *      }
     *  },
     * )
     *
     * @Rest\View(serializerGroups={"info", "login"}, serializerEnableMaxDepthChecks=true)
     * @Rest\QueryParam(name="limit", description="Limit", default="20", requirements="\d+")
     * @Rest\QueryParam(name="offset", description="Offset", default="0", requirements="\d+")
     */
    public function getStudentRoutinesForTeacherAction(Request $request, User $student)
    {
        /* @var StudentInterface $student */
        /* @var AcademyContext $context */
        $context = $request->attributes->get('academy_context');
        $academy = $context->getAcademy();
        $offset = $request->get('offset', 0);
        $limit = $request->get('limit', 20);

        /* @var RoutineRepositoryInterface $routineRepository */
        $routineRepository = $this->get('routine.repository');
        $routines = $routineRepository->findAllByStudent($student, $academy, $offset, $limit);

        return compact('routines');
    }


    /**
     * Permite completar el feedback de una rutina
     *
     * @ApiDoc(
     *  section="student",
     *  resource=true,
     *  https=true,
     *  authentication=true,
     *  authenticationRoles={"ROLE_STUDENT"},
     *  tags={"lessons"},
     *  description="Completar feedback de rutina",
     *  input= {
     *      "class"="AndresGotta\Bundle\AcademyBundle\Form\AddRoutineFeedbackType"
     *  },
     * )
     *
     * @Rest\View(serializerGroups={"info", "login"}, serializerEnableMaxDepthChecks=true)
     */
    public function addRoutineFeedbackAction(Request $request)
    {
        /* @var StudentInterface $student */
        $student = $this->getUser();

        /** @var AcademyContext $context */
        $context = $request->attributes->get('academy_context');
        $academy = $context->getAcademy();

        /* @var RoutineRepositoryInterface $routineRepository */
        $routineRepository = $this->get('routine.repository');
        $routines = $routineRepository->findAllByStudent($student, $academy);

        $command = new SetRoutineFeedbackCommand();
        $form = $this->createForm(new AddRoutineFeedbackType(), $command, [
            'routines' => $routines,
        ]);
        $form->submit($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $command = $form->getData();
            $this->get('tactician.commandbus.default')->handle($command);

            return [];
        }

        return ['form' => $form];
    }

    /**
     * Permite completar el feedback de una rutina
     *
     * @ApiDoc(
     *  section="student",
     *  resource=true,
     *  https=true,
     *  authentication=true,
     *  authenticationRoles={"ROLE_STUDENT"},
     *  tags={"lessons"},
     *  description="Completar feedback de rutina",
     *  input= {
     *      "class"="AndresGotta\Bundle\AcademyBundle\Form\SetRoutineStepStatusType"
     *  },
     * )
     *
     * @Rest\View(serializerGroups={"info", "login"}, serializerEnableMaxDepthChecks=true)
     */
    public function setRoutineStepStatusAction(Request $request)
    {
        /* @var StudentInterface $student */
        $student = $this->getUser();

        /** @var AcademyContext $context */
        $context = $request->attributes->get('academy_context');
        $academy = $context->getAcademy();

        /* @var RoutineRepositoryInterface $routineRepository */
        $routineRepository = $this->get('routine.repository');
        $routines = $routineRepository->findAllByStudent($student, $academy);

        $command = new SetRoutineStepStatusCommand();
        $form = $this->createForm(new SetRoutineStepStatusType(), $command, [
            'routines' => $routines,
        ]);
        $form->submit($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $command = $form->getData();
            $this->get('tactician.commandbus.default')->handle($command);

            return [];
        }

        return ['form' => $form];
    }

    /**
     * Permite eliminar una rutina
     *
     * @ApiDoc(
     *  section="teacher",
     *  resource=true,
     *  https=true,
     *  authentication=true,
     *  authenticationRoles={"ROLE_TEACHER"},
     *  tags={"lessons"},
     *  description="Eliminar rutina",
     * )
     *
     * @Rest\View()
     */
    public function removeRoutineAction(Request $request, Routine $routine)
    {
        if ($this->getUser() != $routine->getTeacher()) {
            throw $this->createAccessDeniedException();
        }

        $em = $this->getDoctrine()->getManager();
        $em->remove($routine);
        $em->flush();

        return;
    }
}
