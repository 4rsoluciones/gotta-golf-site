<?php

namespace AndresGotta\Bundle\AcademyBundle\Controller\Api;

use AndresGotta\Academy\Application\AcademyContext;
use AndresGotta\Academy\Application\Command\AddGoalCommand;
use AndresGotta\Academy\Application\Command\AddIndicationCommand;
use AndresGotta\Academy\Domain\Entity\Indication;
use AndresGotta\Academy\Domain\Entity\StudentInterface;
use AndresGotta\Academy\Domain\Repository\GoalRepositoryInterface;
use AndresGotta\Academy\Domain\Repository\IndicationRepositoryInterface;
use AndresGotta\Bundle\AcademyBundle\Form\AddGoalType;
use AndresGotta\Bundle\AcademyBundle\Form\AddIndicationType;
use AndresGotta\Bundle\AcademyBundle\Form\EditIndicationType;
use Nelmio\ApiDocBundle\Annotation\ApiDoc;
use FOS\RestBundle\Controller\Annotations as Rest;
use AndresGotta\Academy\Domain\Entity\TeacherInterface;
use FOS\RestBundle\Controller\FOSRestController;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;

class IndicationController extends FOSRestController
{
    /**
     * Permite recuperar los access tokens
     *
     * @ApiDoc(
     *  section="teacher",
     *  resource=true,
     *  https=true,
     *  authentication=true,
     *  authenticationRoles={"ROLE_TEACHER"},
     *  tags={"lessons"},
     *  description="Listado de access tokens",
     *  output= {
     *      "class"="array<AndresGotta\Bundle\AcademyBundle\Entity\AccessToken>",
     *      "groups" = {},
     *      "parsers"={
     *          "Nelmio\ApiDocBundle\Parser\JmsMetadataParser"
     *      }
     *  },
     * )
     *
     * @Rest\View(serializerGroups={"access_token"}, serializerEnableMaxDepthChecks=true)
     */
    public function getAccessTokensAction()
    {
        $accessTokenManager = $this->get('academy.access_token.manager');
        $repo = $this->getDoctrine()->getRepository('AndresGottaAcademyBundle:AccessToken');
        $accessTokens = $repo->findAll();

        foreach ($accessTokens as $accessToken) {
            if (!$accessTokenManager->isValid($accessToken)) {
                try {
                    $accessTokenManager->invalidate($accessToken);
                } catch (\RuntimeException $x) {
                    $this->get('logger')->addCritical('No es posible recuperar access token', ['exception' => $x]);

                    return JsonResponse::create(['error' => $x->getMessage()], 502);
                }

            }
        }

        return compact('accessTokens');
    }

    /**
     * Permite recuperar las indicaciones de un docente
     *
     * @ApiDoc(
     *  section="teacher",
     *  resource=true,
     *  https=true,
     *  authentication=true,
     *  authenticationRoles={"ROLE_TEACHER"},
     *  tags={"lessons"},
     *  description="Listado de indicaciones del docente",
     *  output= {
     *      "class"="array<AndresGotta\Academy\Domain\Entity\Indication>",
     *      "groups" = {},
     *      "parsers"={
     *          "Nelmio\ApiDocBundle\Parser\JmsMetadataParser"
     *      }
     *  },
     * )
     *
     * @Rest\View(serializerGroups={"info", "login"}, serializerEnableMaxDepthChecks=true)
     * @Rest\QueryParam(name="limit", description="Limit", default="20", requirements="\d+")
     * @Rest\QueryParam(name="offset", description="Offset", default="0", requirements="\d+")
     * @Rest\QueryParam(name="type", description="Media material type", default="", requirements="media\.(audio|video|image)")
     * @Rest\QueryParam(name="goals", description="Goals separado por coma")
     * @Rest\QueryParam(name="query", description="Query string", default="")
     */
    public function getTeacherIndicationsAction(Request $request)
    {
        /* @var TeacherInterface $teacher */
        $teacher = $this->getUser();

        /** @var AcademyContext $context */
        $context = $request->attributes->get('academy_context');
        $academy = $context->getAcademy();
        $offset = $request->get('offset', 0);
        $limit = $request->get('limit', 20);
        $type = $request->get('type');
        $goals = array_filter(explode(',', $request->get('goals')), 'trim');
        $query = $request->get('query');

        /* @var IndicationRepositoryInterface $indicationRepository */
        $indicationRepository = $this->get('indication.repository');
        $indications = $indicationRepository->findAllByTeacher($type, $teacher, $academy, $goals, $query, $offset, $limit);

        return compact('indications');
    }

    /**
     * Permite recuperar las indicaciones de un alumno
     *
     * @ApiDoc(
     *  section="student",
     *  resource=true,
     *  https=true,
     *  authentication=true,
     *  authenticationRoles={"ROLE_STUDENT"},
     *  tags={"lessons"},
     *  description="Listado de indicaciones del alumno",
     *  output= {
     *      "class"="array<AndresGotta\Academy\Domain\Entity\Indication>",
     *      "groups" = {},
     *      "parsers"={
     *          "Nelmio\ApiDocBundle\Parser\JmsMetadataParser"
     *      }
     *  },
     * )
     *
     * @Rest\View(serializerGroups={"info", "login"}, serializerEnableMaxDepthChecks=true)
     * @Rest\QueryParam(name="limit", description="Limit", default="20", requirements="\d+")
     * @Rest\QueryParam(name="offset", description="Offset", default="0", requirements="\d+")
     * @Rest\QueryParam(name="type", description="Media material type", default="", requirements="media\.(audio|video|image)")
     * @Rest\QueryParam(name="goals", description="Goals separado por coma")
     * @Rest\QueryParam(name="query", description="Query string", default="")
     */
    public function getStudentIndicationsAction(Request $request)
    {
        /* @var StudentInterface $student */
        $student = $this->getUser();

        /** @var AcademyContext $context */
        $context = $request->attributes->get('academy_context');
        $academy = $context->getAcademy();
        $offset = $request->get('offset', 0);
        $limit = $request->get('limit', 20);
        $type = $request->get('type');
        $goals = array_filter(explode(',', $request->get('goals')), 'trim');
        $query = $request->get('query');

        /* @var IndicationRepositoryInterface $indicationRepository */
        $indicationRepository = $this->get('indication.repository');
        $indications = $indicationRepository->findAllByStudent($type, $student, $academy, $goals, $query, $offset, $limit);

        return compact('indications');
    }

    /**
     * Permite agregar una indicacion
     *
     * @ApiDoc(
     *  section="teacher",
     *  resource=true,
     *  https=true,
     *  authentication=true,
     *  authenticationRoles={"ROLE_TEACHER"},
     *  tags={"lessons"},
     *  description="Agregar una indicacion",
     *  input= {
     *      "class"="AndresGotta\Bundle\AcademyBundle\Form\AddIndicationType"
     *  },
     * )
     *
     * @Rest\View(serializerGroups={"info", "login"}, serializerEnableMaxDepthChecks=true)
     */
    public function addIndicationAction(Request $request)
    {
        /** @var AcademyContext $context */
        $context = $request->attributes->get('academy_context');

        /* @var TeacherInterface $teacher */
        $teacher = $this->getUser();

        $command = new AddIndicationCommand($teacher);
        $form = $this->createForm(new AddIndicationType(), $command);
        $form->submit($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $command = $form->getData();
            $command->setAcademy($context->getAcademy());
            $indication = $this->get('tactician.commandbus.default')->handle($command);

            return $indication;
        }

        return ['form' => $form];
    }

    /**
     * Permite editar una indicación
     *
     * @ApiDoc(
     *  section="teacher",
     *  resource=true,
     *  https=true,
     *  authentication=true,
     *  authenticationRoles={"ROLE_TEACHER"},
     *  tags={"indication"},
     *  description="Edita una indicación",
     *  input= {
     *      "class"="AndresGotta\Bundle\AcademyBundle\Form\EditIndicationType"
     *  },
     * )
     *
     * @Rest\View(serializerGroups={"info", "login"}, serializerEnableMaxDepthChecks=true)
     */
    public function editIndicationAction(Request $request, Indication $indication)
    {
        /** @var AcademyContext $context */
        $context = $request->attributes->get('academy_context');

        /* @var TeacherInterface $teacher */
        $teacher = $this->getUser();

        if (
            $indication->getAcademy() !== $context->getAcademy() ||
            $indication->getTeacher() !== $teacher
        ) {
            throw $this->createAccessDeniedException();
        }

        $form = $this->createForm(new EditIndicationType(), $indication);
        $form->submit($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $indicationRepository = $this->get('indication.repository');
            $indicationRepository->editIndication($indication);

            return $indication;
        }

        return ['form' => $form];
    }

    /**
     * Permite recuperar los objetivos
     *
     * @ApiDoc(
     *  section="student",
     *  resource=true,
     *  https=true,
     *  authentication=true,
     *  authenticationRoles={"ROLE_STUDENT"},
     *  tags={"lessons"},
     *  description="Listado de objetivos",
     *  output= {
     *      "class"="array<AndresGotta\Academy\Domain\Entity\Goal>",
     *      "groups" = {},
     *      "parsers"={
     *          "Nelmio\ApiDocBundle\Parser\JmsMetadataParser"
     *      }
     *  },
     * )
     *
     * @Rest\View(serializerGroups={"info", "login"}, serializerEnableMaxDepthChecks=true)
     * @Rest\QueryParam(name="limit", description="Limit", default="20", requirements="\d+")
     * @Rest\QueryParam(name="offset", description="Offset", default="0", requirements="\d+")
     * @Rest\QueryParam(name="query", description="Query", default="")
     */
    public function getStudentGoalsAction(Request $request)
    {
        /* @var StudentInterface $student */
        $student = $this->getUser();

        /** @var AcademyContext $context */
        $context = $request->attributes->get('academy_context');
        $academy = $context->getAcademy();
        $offset = $request->get('offset', 0);
        $limit = $request->get('limit', 20);
        $query = $request->get('query');

        /* @var GoalRepositoryInterface $goalRepository */
        $goalRepository = $this->get('goal.repository');
        $goals = $goalRepository->findAllOrderedByMostUsedByStudent($query, $student, $academy, $offset, $limit);

        return compact('goals');
    }

    /**
     * Permite recuperar los objetivos
     *
     * @ApiDoc(
     *  section="teacher",
     *  resource=true,
     *  https=true,
     *  authentication=true,
     *  authenticationRoles={"ROLE_TEACHER"},
     *  tags={"lessons"},
     *  description="Listado de objetivos",
     *  output= {
     *      "class"="array<AndresGotta\Academy\Domain\Entity\Goal>",
     *      "groups" = {},
     *      "parsers"={
     *          "Nelmio\ApiDocBundle\Parser\JmsMetadataParser"
     *      }
     *  },
     * )
     *
     * @Rest\View(serializerGroups={"info", "login"}, serializerEnableMaxDepthChecks=true)
     * @Rest\QueryParam(name="limit", description="Limit", default="20", requirements="\d+")
     * @Rest\QueryParam(name="offset", description="Offset", default="0", requirements="\d+")
     * @Rest\QueryParam(name="query", description="Query", default="")
     */
    public function getGoalsAction(Request $request)
    {
        /* @var TeacherInterface $teacher */
        $teacher = $this->getUser();

        /** @var AcademyContext $context */
        $context = $request->attributes->get('academy_context');
        $academy = $context->getAcademy();
        $offset = $request->get('offset', 0);
        $limit = $request->get('limit', 20);
        $query = $request->get('query');

        /* @var GoalRepositoryInterface $goalRepository */
        $goalRepository = $this->get('goal.repository');
        $goals = $goalRepository->findAllOrderedByMostUsedBy($query, $teacher, $academy, $offset, $limit);

        return compact('goals');
    }

    /**
     * Permite agregar un objetivo
     *
     * @ApiDoc(
     *  section="teacher",
     *  resource=true,
     *  https=true,
     *  authentication=true,
     *  authenticationRoles={"ROLE_TEACHER"},
     *  tags={"lessons"},
     *  description="Agregar un objetivo",
     *  input= {
     *      "class"="AndresGotta\Bundle\AcademyBundle\Form\AddGoalType"
     *  },
     * )
     *
     * @Rest\View(serializerGroups={"info", "login"}, serializerEnableMaxDepthChecks=true)
     */
    public function addGoalAction(Request $request)
    {
        /** @var AcademyContext $context */
        $context = $request->attributes->get('academy_context');

        /* @var TeacherInterface $teacher */
        $teacher = $this->getUser();

        $command = new AddGoalCommand($teacher);
        $form = $this->createForm(new AddGoalType(), $command);
        $form->submit($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $command = $form->getData();
            $goal = $this->get('tactician.commandbus.default')->handle($command);

            return ['id' => $goal->getId()];
        }

        return ['form' => $form];
    }

    /**
     * Permite eliminar una indicación
     *
     * @ApiDoc(
     *  section="teacher",
     *  resource=true,
     *  https=true,
     *  authentication=true,
     *  authenticationRoles={"ROLE_TEACHER"},
     *  tags={"indications"},
     *  description="Eliminar indicacion",
     * )
     *
     * @Rest\View()
     */
    public function removeIndicationAction(Request $request, Indication $indication)
    {
        if ($this->getUser() != $indication->getTeacher()) {
            throw $this->createAccessDeniedException();
        }

        $em = $this->getDoctrine()->getManager();
        $em->remove($indication);
        $em->flush();

        return;
    }
}
