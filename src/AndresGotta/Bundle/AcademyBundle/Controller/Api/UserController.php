<?php

namespace AndresGotta\Bundle\AcademyBundle\Controller\Api;

use AndresGotta\Academy\Application\AcademyContext;
use AndresGotta\Academy\Application\Command\AddRoutineCommand;
use AndresGotta\Academy\Application\Command\ProcessRecoverAccountCommand;
use AndresGotta\Academy\Application\Command\RequestRecoverAccountCommand;
use AndresGotta\Academy\Application\Command\UserCommand;
use AndresGotta\Academy\Domain\Entity\StudentInterface;
use AndresGotta\Academy\Domain\Repository\IndicationRepositoryInterface;
use AndresGotta\Academy\Domain\Repository\RoutineRepositoryInterface;
use AndresGotta\Bundle\AcademyBundle\Form\AddRoutineType;
use AndresGotta\Bundle\AcademyBundle\Form\ProcessRecoverAccountType;
use AndresGotta\Bundle\AcademyBundle\Form\RequestRecoverAccountType;
use AndresGotta\Bundle\GolfBundle\ValueObject\PlayerCondition;
use GuzzleHttp\Exception\GuzzleException;
use Nelmio\ApiDocBundle\Annotation\ApiDoc;
use FOS\RestBundle\Controller\Annotations as Rest;
use AndresGotta\Academy\Domain\Entity\TeacherInterface;
use FOS\RestBundle\Controller\FOSRestController;
use Symfony\Component\Form\FormError;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use WebFactory\Bundle\UserBundle\Entity\Profile;
use WebFactory\Bundle\UserBundle\Entity\User;

class UserController extends FOSRestController
{

    /**
     * Permite recuperar los datos del usuario logueado
     *
     * @ApiDoc(
     *  section="guest",
     *  resource=true,
     *  https=true,
     *  tags={"users"},
     *  description="Datos del usuario actual",
     *  output= {
     *      "class"="array<WebFactory\Bundle\UserBundle\Entity\User>",
     *      "groups" = {"login"},
     *      "parsers"={
     *          "Nelmio\ApiDocBundle\Parser\JmsMetadataParser"
     *      }
     *  },
     * )
     *
     * @Rest\View(serializerGroups={"login"}, serializerEnableMaxDepthChecks=true)
     */
    public function loginAction()
    {
        $user = $this->getUser();

        return compact('user');
    }

    /**
     * Permite recuperar los datos del usuario logueado
     *
     * @ApiDoc(
     *  section="student",
     *  resource=true,
     *  https=true,
     *  tags={"users"},
     *  description="Datos del usuario actual",
     *  output= {
     *      "class"="array<WebFactory\Bundle\UserBundle\Entity\User>",
     *      "groups" = {"login"},
     *      "parsers"={
     *          "Nelmio\ApiDocBundle\Parser\JmsMetadataParser"
     *      }
     *  },
     * )
     *
     * @Rest\View(serializerGroups={"login"}, serializerEnableMaxDepthChecks=true)
     */
    public function studentAcademiesAction()
    {
        $academies = $this->getUser()->getEnrolledInAcademies();

        return compact('academies');
    }

    /**
     * Permite recuperar los datos del usuario logueado
     *
     * @ApiDoc(
     *  section="teacher",
     *  resource=true,
     *  https=true,
     *  tags={"users"},
     *  description="Datos del usuario actual",
     *  output= {
     *      "class"="array<WebFactory\Bundle\UserBundle\Entity\User>",
     *      "groups" = {"login"},
     *      "parsers"={
     *          "Nelmio\ApiDocBundle\Parser\JmsMetadataParser"
     *      }
     *  },
     * )
     *
     * @Rest\View(serializerGroups={"login"}, serializerEnableMaxDepthChecks=true)
     */
    public function teacherAcademiesAction()
    {
        $academies = $this->getUser()->getTeachesInAcademies();

        return compact('academies');
    }

    /**
     * Permite recuperar una cuenta
     *
     * @ApiDoc(
     *  section="guest",
     *  resource=true,
     *  https=true,
     *  tags={"users"},
     *  description="Recuperar",
     *  filters={
     *      {"name"="email", "dataType"="string"},
     *  }
     * )
     *
     * @Rest\View(serializerGroups={"login"}, serializerEnableMaxDepthChecks=true)
     */
    public function requestRecoverAccountAction(Request $request)
    {
        $email = $request->get('email');
        /* @var User $user */
        $user = $this->getDoctrine()->getRepository('WebFactoryUserBundle:User')->findOneByEmail($email);
        if (!$user) {
            return JsonResponse::create(['message' => 'User not found'], 400);
        }

        $validRole = $user->hasRole('ROLE_STUDENT') || $user->hasRole('ROLE_TEACHER');
        if (!$user->isEnabled() || !$validRole) {
            return JsonResponse::create(['message' => 'User invalid or disabled'], 400);
        }

        $command = new RequestRecoverAccountCommand($user);
        $this->get('tactician.commandbus.default')->handle($command);

        return [];
    }

    /**
     * Permite recuperar una cuenta
     *
     * @ApiDoc(
     *  section="guest",
     *  resource=true,
     *  https=true,
     *  tags={"users"},
     *  description="Recuperar",
     *  input= {
     *      "class"="AndresGotta\Bundle\AcademyBundle\Form\ProcessRecoverAccountType"
     *  },
     * )
     *
     * @Rest\View(serializerGroups={"login"}, serializerEnableMaxDepthChecks=true)
     */
    public function resetPasswordAction(Request $request)
    {
        $command = new ProcessRecoverAccountCommand();
        $form = $this->createForm(new ProcessRecoverAccountType(), $command);
        $form->submit($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $command = $form->getData();
            try {
                $this->get('tactician.commandbus.default')->handle($command);
            } catch (\Exception $e) {
                $form->addError(new FormError('Invalid code'));

                return ['form' => $form];
            }

            return [];
        }

        return ['form' => $form];
    }

    /**
     * Permite cambiar accesos del usuario actual
     *
     * @ApiDoc(
     *  section="student",
     *  resource=true,
     *  https=true,
     *  authentication=true,
     *  authenticationRoles={"ROLE_STUDENT"},
     *  tags={"lessons"},
     *  description="Listado de rutinas del estudiante",
     *  output= {
     *      "class"="array<AndresGotta\Academy\Domain\Entity\Routine>",
     *      "groups" = {},
     *      "parsers"={
     *          "Nelmio\ApiDocBundle\Parser\JmsMetadataParser"
     *      }
     *  },
     * )
     *
     * @Rest\View
     * @Rest\QueryParam(name="new_password", description="new", default="")
     * @Rest\QueryParam(name="old_password", description="old", default="")
     */
    public function changeAccountAccessDataAction(Request $request)
    {
        /* @var StudentInterface $student */
        $user = $this->getUser();

        $newPassword = $request->get('new_password', 0);
        $oldPassword = $request->get('old_password', 0);

        if ($user->getPassword() === $this->get('security.password_encoder')->encodePassword($user, $oldPassword)) {
            $user->setPlainPassword($newPassword);
            $this->get('fos_user.user_manager')->updateUser($user);
        } else {
            return JsonResponse::create(['error' => 'invalid password'], 400);
        }

        return [];
    }

    /**
     * Permite recuperar un usuario por su email
     *
     * @ApiDoc(
     *  section="teacher",
     *  resource=true,
     *  https=true,
     *  authentication=true,
     *  authenticationRoles={"ROLE_TEACHER"},
     *  tags={"lessons"},
     *  description="Datos de un usuario",
     *  output= {
     *      "class"="array<WebFactory\Bundle\UserBundle\Entity\User>",
     *      "groups" = {},
     *      "parsers"={
     *          "Nelmio\ApiDocBundle\Parser\JmsMetadataParser"
     *      }
     *  },
     * )
     *
     * @Rest\View
     * @Rest\QueryParam(name="email", description="email", default="")
     */
    public function findUserByEmailAction(Request $request)
    {
        $email = $request->get('email');

        try {
            $userArray = $this->get('web_factory_user.gotta_center_api')->getUser($email, $this->getUser()->getAccessToken());
        } catch (GuzzleException $e) {
            $userArray = null;
        }
        $translatedUser = $userArray && $userArray['success'] ? $this->get('webfactory.user_provider.backend')->translateLaravelUserJson($userArray) : null;
        $user = $translatedUser ? $this->get('webfactory.user_provider.api')->getUserEntityFromArray($translatedUser) : null;

        return $user;
    }

    /**
     * Permite crear y asignar un usuario al docente y academia actual
     *
     * @ApiDoc(
     *  section="teacher",
     *  resource=true,
     *  https=true,
     *  authentication=true,
     *  authenticationRoles={"ROLE_TEACHER"},
     *  tags={"lessons"},
     *  description="Registrar y asociar usuario",
     *  output= {
     *      "class"="array<WebFactory\Bundle\UserBundle\Entity\User>",
     *      "groups" = {},
     *      "parsers"={
     *          "Nelmio\ApiDocBundle\Parser\JmsMetadataParser"
     *      }
     *  },
     * )
     *
     * @Rest\View(serializerGroups={"info", "login"}, serializerEnableMaxDepthChecks=true)
     * @Rest\QueryParam(name="email", description="email", default="")
     * @Rest\QueryParam(name="password", description="password", default="")
     * @Rest\QueryParam(name="firstName", description="firstName", default="")
     * @Rest\QueryParam(name="lastName", description="lastName", default="")
     */
    public function addStudentAction(Request $request)
    {
        /** @var AcademyContext $context */
        $context = $request->attributes->get('academy_context');
        $academy = $context->getAcademy();

        // Chequear que puede crear/asociar un estudiante más
        try {
            $checker = $this->get('andres_gotta_bundle_academy.service.service_checker');
            $canAddStudent = $checker->canAddStudent($academy, $this->getUser()->getAccessToken());
        } catch (GuzzleException $e) {
            $canAddStudent = false;
        }
        if (!$canAddStudent) {
            $response = JsonResponse::create(['message' => 'Academy student limit reached.'], Response::HTTP_PAYMENT_REQUIRED);

            return $response;
        }

        // Validar ingreso de datos
        $userManager = $this->get('fos_user.user_manager');

        $email = $request->get('email');
        $firstName = $request->get('firstName', '');
        $lastName = $request->get('lastName', '');

        if (!$email || !filter_var($email, FILTER_VALIDATE_EMAIL)) {
            return JsonResponse::create(['error' => 'invalid data'], Response::HTTP_BAD_REQUEST);
        }

        $api = $this->get('web_factory_user.gotta_center_api');

        //Si center no tiene un usuario con ese email, se crea en center
        // 1- ver si el usuario existe en centralizador a partir del email
        try {
            $jsonUser = $api->getUser($email, $this->getUser()->getAccessToken());
        } catch (GuzzleException $e) {
            $jsonUser = false;
        }

        // 2- si no existe, se crea el usuario en centralizador y aca
        if (!$jsonUser || empty($jsonUser['success'])) {

            try {
                $newUser = $this->get('student.factory')->fromEmail($email, $firstName, $lastName);
                $jsonUser = $api->createUser($newUser, $this->getUser()->getAccessToken());
            } catch (GuzzleException $e) {
                $jsonUser = false;
            }

            if (!$jsonUser) {
                return JsonResponse::create(['error' => 'user can not be created, centralizer error'], Response::HTTP_BAD_REQUEST);
            }
        }

        $user = $userManager->findUserByEmail($email);
        if (!$user) {

            $translatedUser = $jsonUser && $jsonUser['success'] ? $this->get('webfactory.user_provider.backend')->translateLaravelUserJson($jsonUser) : null;
            $user = $translatedUser ? $this->get('webfactory.user_provider.api')->getUserEntityFromArray($translatedUser) : null;
        }

        //usar los translators entre sistemas
        $user->setSalt($jsonUser['success']['salt']);

        // 4- vincular el estudiante al docente y a la academia

        /* @var TeacherInterface $teacher */
        $teacher = $this->getUser();

        /* @var StudentInterface $user */
        $user->addEnrolledInAcademy($academy);
        $user->addTrainedByTeacher($teacher);

        $validator = $this->get('validator');
        $errors = $validator->validate($user);//Este es el unico punto critico
        if ($errors->count()) {

            return JsonResponse::create(['error' => 'invalid data: ' . (string) $errors], Response::HTTP_BAD_REQUEST);
        }

        // 3- actualizar el usuario en centralizador como estudiante
        try {
            $api->updateUser($user, $this->getUser()->getAccessToken());
        } catch (GuzzleException $e) {
            return JsonResponse::create(['error' => 'invalid data: ' . $e->getMessage()], Response::HTTP_BAD_REQUEST);
        }

        $userManager->updateUser($user);

        return $user;
    }

}
