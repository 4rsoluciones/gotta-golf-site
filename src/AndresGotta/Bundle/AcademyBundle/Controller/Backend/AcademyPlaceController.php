<?php

namespace AndresGotta\Bundle\AcademyBundle\Controller\Backend;

use AndresGotta\Academy\Domain\Entity\AcademyPlace;
use AndresGotta\Bundle\AcademyBundle\Form\AcademyPlaceType;
use AndresGotta\Bundle\AcademyBundle\Form\Model\AcademyPlaceDTO;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Template;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\RedirectResponse;
use Symfony\Component\HttpFoundation\Request;
use WebFactory\Bundle\UserBundle\Entity\User;

class AcademyPlaceController extends Controller
{
    /**
     * @Template()
     *
     * @param Request $request
     * @param User $academy
     *
     * @return array|RedirectResponse
     */
    public function newAction(Request $request, User $academy)
    {
        if (!in_array('ROLE_ACADEMY', $academy->getRoles())) {
            throw $this->createNotFoundException();
        }

        $placeDTO = new AcademyPlaceDTO();
        $form = $this->createPlaceForm($placeDTO);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $academy->addAcademyPlace($placeDTO->name, $placeDTO->address);

            // todo: Pasar esto a un manager
            $em = $this->getDoctrine()->getManager();
            $em->persist($academy);
            $em->flush();

            return $this->redirect($this->getParameter('gotta_center_api.server') . '/admin/users?success=true');

//            $this->addFlash('success', 'flash.academy.add_place.success');
//
//            return $this->redirect($this->generateUrl('andres_gotta_academy_backend_academy_places', ['id' => $academy->getId()]));
        }

        return ['form' => $form->createView(), 'academy' => $academy];
    }

    /**
     * @Template()
     *
     * @param Request $request
     * @param AcademyPlace $academyPlace
     *
     * @return array|RedirectResponse
     */
    public function editAction(Request $request, AcademyPlace $academyPlace)
    {
        $placeDTO = new AcademyPlaceDTO($academyPlace->getName(), $academyPlace->getAddress());
        $form = $this->createPlaceForm($placeDTO);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $academyPlace->setName($placeDTO->name);
            $academyPlace->setAddress($placeDTO->address);

            // todo: Pasar esto a un manager
            $em = $this->getDoctrine()->getManager();
            $em->persist($academyPlace);
            $em->flush();

            return $this->redirect($this->getParameter('gotta_center_api.server') . '/admin/users?success=true');

//            $this->addFlash('success', 'flash.academy.update_place.success');
//
//            return $this->redirect($this->generateUrl('andres_gotta_academy_backend_academy_places', ['id' => $academyPlace->getAcademy()->getId()]));
        }

        return ['form' => $form->createView(), 'academyPlace' => $academyPlace];
    }

    /**
     * @param AcademyPlaceDTO $placeDTO
     * @return \Symfony\Component\Form\Form
     */
    private function createPlaceForm(AcademyPlaceDTO $placeDTO)
    {
        $form = $this->createForm(new AcademyPlaceType(), $placeDTO, [
            'method' => 'POST'
        ]);

        return $form;
    }
}
