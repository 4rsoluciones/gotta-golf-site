<?php

namespace AndresGotta\Bundle\AcademyBundle\Controller\Backend;

use AndresGotta\Academy\Domain\Entity\Lesson;
use APY\DataGridBundle\Grid\Source\Entity;
use Doctrine\ORM\QueryBuilder;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use APY\DataGridBundle\Grid\Action\RowAction;
use Symfony\Component\HttpFoundation\Response;
use WebFactory\Bundle\UserBundle\Entity\User;

/**
 * Class ReportController
 * Route("/lesson")
 */
class LessonController extends Controller
{
    /**
     * Lists all User entities.
     */
    public function indexAction()
    {
        $user = $this->getUser();

        $grid = $this->getGrid($user);
        $grid->setId('main');
        $grid->setHiddenColumns(array('id'));
        $grid->setDefaultOrder('id', 'asc');

        $editRowAction = new RowAction('Show', 'andres_gotta_academy_backend_lesson_reports_show', false, '_self', array('class' => 'btn-action glyphicons search btn-success'));
        $grid->addRowAction($editRowAction);

        return $grid->getGridResponse('AndresGottaAcademyBundle:Backend/Lesson:index.html.twig');
    }

    /**
     * @param Lesson $lesson
     * @return Response
     */
    public function showAction(Lesson $lesson)
    {
        $user = $this->getUser();
        $academyPlace = $lesson->getPlace();
        if ($user->hasRole('ROLE_ACADEMY') && $academyPlace->getAcademy() !== $user) {
            throw $this->createAccessDeniedException();
        }

        return $this->render('AndresGottaAcademyBundle:Backend/Lesson:show.html.twig', [
            'entity' => $lesson,
        ]);
    }

    /**
     * @param User $user
     * @return \APY\DataGridBundle\Grid\Grid
     */
    private function getGrid(User $user)
    {
        $source = new Entity('GottaUser:Lesson');

        if (!$user->hasRole('ROLE_ADMIN') && $user->hasRole('ROLE_ACADEMY')) {
            $tableAlias = $source->getTableAlias();

            $source->manipulateQuery(
                function (QueryBuilder $query) use ($tableAlias, $user)
                {
                    $query
                        ->join($tableAlias.'.place', 'AcademyPlace')
                        ->andWhere('AcademyPlace.academy = :user')->setParameter('user', $user);
                }
            );
        }


        $source->manipulateRow(function ($row) {
            $name = function (User $user) {
                $profile = $user->getProfile();
                if ($profile && $profile->getLastName() && $profile->getFirstName()) {
                    return sprintf('%s, %s', $profile->getLastName(), $profile->getFirstName());
                }

                return $user->getUsername();
            };

            /** @var Lesson $lesson */
            $lesson = $row->getEntity();

            $teacher = $lesson->getTeacher();
            $student = $lesson->getStudent();

            $row->setField('place', $lesson->getPlace()->getName());
            $row->setField('teacher', $name($teacher));
            $row->setField('student', $name($student));

            return $row;
        });

        /* @var $grid \APY\DataGridBundle\Grid\Grid */
        $grid = $this->get('grid');

        $grid->setSource($source);

        return $grid;
    }
}
