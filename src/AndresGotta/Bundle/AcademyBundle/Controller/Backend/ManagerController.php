<?php

namespace AndresGotta\Bundle\AcademyBundle\Controller\Backend;

use AndresGotta\Academy\Domain\Entity\AcademyPlace;
use AndresGotta\Academy\Domain\Entity\Report;
use AndresGotta\Academy\Domain\Entity\Routine;
use EasyCorp\Bundle\EasyAdminBundle\Controller\AdminController as BaseAdminController;
use EasyCorp\Bundle\EasyAdminBundle\Event\EasyAdminEvents;
use GuzzleHttp\Exception\GuzzleException;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\RedirectResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use WebFactory\Bundle\UserBundle\Entity\Profile;
use WebFactory\Bundle\UserBundle\Entity\User;

/**
 * Class ManagerController
 * @package AndresGotta\Bundle\AcademyBundle\Controller\Backend
 * @todo split
 */
class ManagerController extends BaseAdminController
{
    protected function createListQueryBuilder($entityClass, $sortDirection, $sortField = null, $dqlFilter = null)
    {
        $qb = parent::createListQueryBuilder($entityClass, $sortDirection, $sortField, $dqlFilter);

        $academy = $this->getUser();
        if ($entityClass === User::class) {
            if (preg_match('/STUDENT/', $dqlFilter)) {
                $qb->andWhere(':academy MEMBER OF entity.enrolledInAcademies');
                $qb->setParameter('academy', $academy);
            }
            if (preg_match('/TEACHER/', $dqlFilter)) {
                $qb->andWhere(':academy MEMBER OF entity.teachesInAcademies');
                $qb->setParameter('academy', $academy);
            }
        } elseif ($entityClass === AcademyPlace::class) {
            $qb->andWhere(':academy = entity.academy');
            $qb->setParameter('academy', $academy);
        } elseif ($entityClass === Report::class) {
            $qb->innerJoin('entity.lesson', 'Lesson');
            $qb->innerJoin('Lesson.place', 'Place');
            $qb->andWhere(':academy = Place.academy');
            $qb->setParameter('academy', $academy);
        } elseif ($entityClass === Routine::class) {
            $qb->andWhere(':academy = entity.academy');
            $qb->setParameter('academy', $academy);
        } else {
            throw new \LogicException();
        }

        return $qb;
    }

    /**
     * @Route("/check-email", name="manage_academy_check_email")
     */
    public function checkEmailAction(Request $request)
    {
        $email = $request->get('email');
        try {
            $userArray = $this->get('web_factory_user.gotta_center_api')->getUser($email, $this->getUser()->getAccessToken());
        } catch (GuzzleException $e) {
            $userArray = null;
        }
        $translatedUser = $userArray && $userArray['success'] ? $this->get('webfactory.user_provider.backend')->translateLaravelUserJson($userArray) : null;
        $user = $translatedUser ? $this->get('webfactory.user_provider.api')->getUserEntityFromArray($translatedUser) : null;
        if ($user) {
            return JsonResponse::create([
                'id' => $user->getId(),
                'email' => $user->getEmail(),
                'profile' => [
                    'firstName' => $user->getProfile()->getFirstName(),
                    'lastName' => $user->getProfile()->getLastName(),
                ],
            ]);
        } else {
            return JsonResponse::create([], 404);
        }
    }

    /**
     * @return RedirectResponse
     * @throws \Doctrine\ORM\OptimisticLockException
     */
    public function unlinkAction()
    {
        $id = $this->request->query->get('id');
        $entity = $this->em->getRepository('WebFactoryUserBundle:User')->find($id);
        if ($this->request->query->get('entity') === 'Student') {
            $entity->removeEnrolledInAcademy($this->getUser());
        } else {
            $entity->removeTeachesInAcademy($this->getUser());
        }

        $this->em->flush();

        $this->get('academy.unlink_from_academy.notifier')->notify($entity, $this->getUser());

        $this->addFlash('success', 'El usuario ha sido desvinculado de la académia');

        // redirect to the 'list' view of the given entity
        return $this->redirectToRoute('easyadmin', array(
            'action' => 'list',
            'entity' => $this->request->query->get('entity'),
        ));
    }

    /**
     * @return \FOS\UserBundle\Model\UserInterface
     */
    public function createNewUserEntity()
    {
        return $this->get('fos_user.user_manager')->createUser();
    }

    /**
     * @return AcademyPlace
     */
    public function createNewAcademyPlaceEntity()
    {
        $entity = new AcademyPlace($this->getUser(), '');
        $entity->setName('');

        return $entity;
    }

    /**
     * @param User $user
     */
    public function persistStudentEntity(User $user)
    {
        $api = $this->get('web_factory_user.gotta_center_api');
        $provider = $this->get('webfactory.user_provider.api');
        $notifier = $this->get('academy.link_to_academy.notifier');
        $stateRepository = $this->getDoctrine()->getRepository('WebFactoryLocationBundle:State');

        $email = $user->getEmail();
        /** @var User $storedUser */
        try {
            $userArray = $api->getUser($email, $this->getUser()->getAccessToken());
        } catch (GuzzleException $e) {
            $userArray = null;
        }
        $translatedUser = $userArray && $userArray['success'] ? $provider->translateLaravelUserJson($userArray) : null;
        $storedUser = $translatedUser ? $provider->getUserEntityFromArray($translatedUser) : null;
        $academy = $this->getUser();
        if ($storedUser) {
            $exists = $storedUser->getEnrolledInAcademies()->contains($academy);
            $storedUser->addRole('ROLE_STUDENT');
            $storedUser->addEnrolledInAcademy($academy);
            $this->persistEntity($storedUser);

            try {
                $api->updateUser($storedUser, $academy->getAccessToken());
            } catch (GuzzleException $e) {

            }

            if (!$exists) {
                $notifier->notify($storedUser, $academy);
            }

            return;
        }

        if (!$user->getUsername()) {
            $user->setUsername($user->getEmail());
        }

        if (!$user->getProfile()) {
            $profile = new Profile();
            $state = $stateRepository->findOneByName('Santa Fe');
            $profile->setState($state);
            $user->setProfile($profile);
            //$user->setProfile(new Profile());
        } else {
            $profile = $user->getProfile();
            if (!$profile->getState()) {
                $state = $stateRepository->findOneByName('Santa Fe');
                $profile->setState($state);
                $user->setProfile($profile);
            }
        }

        $user->setEnabled(true);
        $user->addRole('ROLE_STUDENT');
        $exists = $user->getEnrolledInAcademies()->contains($academy);
        $user->addEnrolledInAcademy($academy);
        $password = User::generatePassword();
        $user->setPlainPassword($password);
        $this->get('fos_user.user_manager')->updateUser($user, false);
        $user->setPlainPassword($password);
        try {
            $createdUser = $api->createUser($user, $academy->getAccessToken());
        } catch (GuzzleException $e) {
            $createdUser = null;
        }
        if ($createdUser) {
            $user->eraseCredentials();
            $this->persistEntity($user);
            if (!$exists) {
                $notifier->notify($user, $academy);
            }
        } else {
            throw new \Exception('El usuario ya existe o no puede darse de alta en la plataforma centralizadora');
        }
    }

    /**
     * @param User $user
     */
    public function persistTeacherEntity(User $user)
    {
        $api = $this->get('web_factory_user.gotta_center_api');
        $provider = $this->get('webfactory.user_provider.api');
        $notifier = $this->get('academy.link_to_academy.notifier');

        $email = $user->getEmail();
        try {
            $userArray = $api->getUser($email, $this->getUser()->getAccessToken());
        } catch (GuzzleException $e) {
            $userArray = null;
        }
        $translatedUser = $userArray && $userArray['success'] ? $provider->translateLaravelUserJson($userArray) : null;
        /** @var User $storedUser*/
        $storedUser = $translatedUser ? $provider->getUserEntityFromArray($translatedUser) : null;

        $academy = $this->getUser();
        if ($storedUser) {
            $exists = $storedUser->getTeachesInAcademies()->contains($this->getUser());
            $storedUser->addRole('ROLE_TEACHER');
            $storedUser->addTeachesInAcademy($this->getUser());
            $this->persistEntity($storedUser);

            try {
                $api->updateUser($storedUser, $academy->getAccessToken());
            } catch (GuzzleException $e) {

            }

            if (!$exists) {
                $notifier->notify($storedUser, $this->getUser());
            }

            return;
        }

        if (!$user->getUsername()) {
            $user->setUsername($user->getEmail());
        }

        if (!$user->getProfile()) {
            $profile = new Profile();
            $user->setProfile($profile);
        }

        $profile = $user->getProfile();
        if (!$profile->getState()) {
            $state = $this->get('location.state.repository')->findOneByName('Santa Fe');
            $profile->setState($state);
            $user->setProfile($profile);
        }

        $user->setEnabled(true);
        $user->addRole('ROLE_TEACHER');
        $exists = $user->getTeachesInAcademies()->contains($this->getUser());
        $user->addTeachesInAcademy($this->getUser());
        $password = User::generatePassword();
        $user->setPlainPassword($password);
        $this->get('fos_user.user_manager')->updateUser($user, false);
        $user->setPlainPassword($password);
        try {
            $jsonUser = $api->createUser($user, $this->getUser()->getAccessToken());
        } catch (GuzzleException $e) {
            $jsonUser = null;
        }
        if ($jsonUser) {
            $user->eraseCredentials();
            $this->persistEntity($user);
            if (!$exists) {
                $notifier->notify($user, $this->getUser());
            }
        } else {
            throw new \Exception('El usuario ya existe o no puede darse de alta en la plataforma centralizadora');
        }
    }

    /**
     * The method that is executed when the user performs a 'new' action on an entity.
     *
     * @return Response|RedirectResponse
     */
    protected function newAction()
    {
        /**
         * @var User $academy
         */
        $academy = $this->getUser();

        $checker = $this->get('andres_gotta_bundle_academy.service.service_checker');
        try {
            if ($this->request->query->get('entity') === 'Student') {
                if (!$checker->canAddStudent($academy, $academy->getAccessToken())) {
                    $this->addFlash('warning', 'Alcanzaste el maximo de estudiantes');

                    return $this->redirectToReferrer();
                }
            } elseif ($this->request->query->get('entity') === 'Teacher') {
                if (!$checker->canAddTeacher($academy, $academy->getAccessToken())) {
                    $this->addFlash('warning', 'Alcanzaste el maximo de profesores');

                    return $this->redirectToReferrer();
                }
            } elseif ($this->request->query->get('entity') === 'AcademyPlace') {
                if (!$checker->canAddPlace($academy, $academy->getAccessToken())) {
                    $this->addFlash('warning', 'Alcanzaste el maximo de sedes');

                    return $this->redirectToReferrer();
                }
            }
        } catch (\Exception $e) {
            $this->addFlash('warning', 'Alcanzaste el maximo de sedes, estudiantes o profesores');

            return $this->redirectToReferrer();
        }

        $this->dispatch(EasyAdminEvents::PRE_NEW);

        $entity = $this->executeDynamicMethod('createNew<EntityName>Entity');

        $easyadmin = $this->request->attributes->get('easyadmin');
        $easyadmin['item'] = $entity;
        $this->request->attributes->set('easyadmin', $easyadmin);

        $fields = $this->entity['new']['fields'];

        $newForm = $this->executeDynamicMethod('create<EntityName>NewForm', array($entity, $fields));

        $newForm->handleRequest($this->request);
        if ($newForm->isSubmitted() && $newForm->isValid()) {
            try {
                $this->dispatch(EasyAdminEvents::PRE_PERSIST, array('entity' => $entity));
                $this->executeDynamicMethod('prePersist<EntityName>Entity', array($entity));
                $this->executeDynamicMethod('persist<EntityName>Entity', array($entity));

                $this->dispatch(EasyAdminEvents::POST_PERSIST, array('entity' => $entity));

                return $this->redirectToReferrer();

            } catch (\Exception $e) {
                $this->addFlash('warning', $e->getMessage());
            }

        }

        $this->dispatch(EasyAdminEvents::POST_NEW, array(
            'entity_fields' => $fields,
            'form' => $newForm,
            'entity' => $entity,
        ));

        return $this->render($this->entity['templates']['new'], array(
            'form' => $newForm->createView(),
            'entity_fields' => $fields,
            'entity' => $entity,
        ));
    }

}