<?php

namespace AndresGotta\Bundle\AcademyBundle\Controller\Backend;

use AndresGotta\Bundle\AcademyBundle\Form\TeachesInAcademiesType;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Template;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\RedirectResponse;
use Symfony\Component\HttpFoundation\Request;
use WebFactory\Bundle\UserBundle\Entity\User;

class TeacherController extends Controller
{
    /**
     * @Template()
     *
     * @param Request $request
     * @param User $teacher
     * @return array| RedirectResponse
     */
    public function academiesAction(Request $request, User $teacher)
    {
        if (!in_array('ROLE_TEACHER', $teacher->getRoles())) {
            throw $this->createNotFoundException();
        }

        $form = $this->createForm(new TeachesInAcademiesType(), $teacher, [
            'method' => 'POST',
        ]);
        $form->add('submit', 'submit');
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            // todo: Pasar esto a un manager
            $em = $this->getDoctrine()->getManager();
            $em->persist($teacher);
            $em->flush();

            return $this->redirect($this->getParameter('gotta_center_api.server') . '/admin/users?success=true');

//            $this->addFlash('success', 'flash.academy.update_teachers_and_students.success');
//
//            return $this->redirect($this->generateUrl('andres_gotta_academy_backend_teachers_academies', ['id' => $teacher->getId()]));
        }
        
        return ['form' => $form->createView()];
    }
}
