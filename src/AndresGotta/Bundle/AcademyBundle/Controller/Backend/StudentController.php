<?php

namespace AndresGotta\Bundle\AcademyBundle\Controller\Backend;

use AndresGotta\Bundle\AcademyBundle\Form\StudentEnrolledInAcademiesType;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Template;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\RedirectResponse;
use Symfony\Component\HttpFoundation\Request;
use WebFactory\Bundle\UserBundle\Entity\User;

class StudentController extends Controller
{
    /**
     * @Template()
     *
     * @param Request $request
     * @param User $student
     * @return array| RedirectResponse
     */
    public function academiesAction(Request $request, User $student)
    {
        if (!in_array('ROLE_STUDENT', $student->getRoles())) {
            throw $this->createNotFoundException();
        }

        $form = $this->createForm(new StudentEnrolledInAcademiesType(), $student, [
            'method' => 'POST',
        ]);
        $form->add('submit', 'submit');
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            // todo: Pasar esto a un manager
            $em = $this->getDoctrine()->getManager();
            $em->persist($student);
            $em->flush();

            return $this->redirect($this->getParameter('gotta_center_api.server') . '/admin/users?success=true');

//            $this->addFlash('success', 'flash.academy.update_student_enrolled_in_academies.success');
//
//            return $this->redirect($this->generateUrl('andres_gotta_academy_backend_students_academies', ['id' => $student->getId()]));
        }

        return ['form' => $form->createView()];
    }
}
