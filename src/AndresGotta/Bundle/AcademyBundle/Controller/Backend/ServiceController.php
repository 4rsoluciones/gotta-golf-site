<?php

namespace AndresGotta\Bundle\AcademyBundle\Controller\Backend;

use Symfony\Component\HttpFoundation\Request;
use EasyCorp\Bundle\EasyAdminBundle\Controller\AdminController as BaseAdminController;

class ServiceController extends BaseAdminController
{
    public function indexAction(Request $request)
    {

        return $this->redirect($this->getParameter('gotta_center_api.frontend'));
    }
}