<?php

namespace AndresGotta\Bundle\AcademyBundle\Controller\Backend;

use AndresGotta\Bundle\AcademyBundle\Form\AcademyTeachersStudentsType;
use Doctrine\Common\Util\Debug;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Template;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\RedirectResponse;
use Symfony\Component\HttpFoundation\Request;
use WebFactory\Bundle\UserBundle\Entity\User;

class AcademyController extends Controller
{
    /**
     * @Template()
     *
     * @param User $academy
     *
     * @return array
     */
    public function placesAction(User $academy)
    {
        if (!in_array('ROLE_ACADEMY', $academy->getRoles())) {
            throw $this->createNotFoundException();
        }

        return ['academy' => $academy];
    }

    /**
     * @Template()
     *
     * @param Request $request
     * @param User $academy
     *
     * @return array|RedirectResponse
     */
    public function teachersAndStudentsAction(Request $request, User $academy)
    {
        if (!in_array('ROLE_ACADEMY', $academy->getRoles())) {
            $this->createNotFoundException();
        }

        $form = $this->createForm(new AcademyTeachersStudentsType(), $academy, [
            'method' => 'POST'
        ]);
        $form->add('submit', 'submit');
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            // todo: Pasar esto a un manager
            $em = $this->getDoctrine()->getManager();
            $em->persist($academy);
            $em->flush();

            return $this->redirect($this->getParameter('gotta_center_api.server') . '/admin/users?success=true');

//            $this->addFlash('success', 'flash.academy.update_teachers_and_students.success');
//
//            return $this->redirect($this->generateUrl('andres_gotta_academy_backend_academy_teachers_and_students', ['id' => $academy->getId()]));
        }

        return ['form' => $form->createView(), 'academy' => $academy];
    }
}
