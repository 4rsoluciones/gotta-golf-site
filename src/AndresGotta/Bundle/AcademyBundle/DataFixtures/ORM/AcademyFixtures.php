<?php

namespace WebFactory\Bundle\UserBundle\DataFixtures\ORM;

use AndresGotta\Academy\Domain\Entity\AcademyPlace;
use AndresGotta\Academy\Domain\ValueObject\Sport;
use Closure;
use DateTime;
use Doctrine\Common\DataFixtures\AbstractFixture;
use Doctrine\Common\DataFixtures\OrderedFixtureInterface;
use Doctrine\Common\Persistence\ObjectManager;
use WebFactory\Bundle\UserBundle\Entity\Profile;
use WebFactory\Bundle\UserBundle\Entity\User;

class AcademyFixtures extends AbstractFixture implements OrderedFixtureInterface
{
    
    /**
     * {@inheritDoc}
     */
    public function getOrder()
    {
        return 0;
    }

    /**
     * {@inheritDoc}
     */
    public function load(ObjectManager $manager)
    {
        $academy = new User();
        $academy->addRole('ROLE_ACADEMY');
        $place = new AcademyPlace($academy, 'place', 'place');
        $academy->setAcademyPlaces([$place]);


        $academy->setUsername('academy');
        $academy->setEmail('academy');
        $academy->setPlainPassword('academy');
        $academy->setEnabled(true);

        $profile = new Profile();

        $profile->setFirstName('academy');
        $profile->setLastName('academy');
        $profile->setGender('male');
        $profile->setMobile(null);
        $profile->setBirthDate(new DateTime('-18 year'));
        $profile->setCity('santa fe');
        $profile->setState($manager->getRepository('WebFactoryLocationBundle:State')->findOneBy([]));
        $profile->setHandicap(rand(1, 20));
        $profile->setCondition('player.professional');

        $academy->setProfile($profile);
        $academy->setSport(new Sport('Golf'));

        $manager->persist($academy);
        $this->addReference('academy-academy', $academy);

        $manager->flush();
    }


}