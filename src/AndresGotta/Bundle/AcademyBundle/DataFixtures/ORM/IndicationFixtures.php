<?php

namespace WebFactory\Bundle\UserBundle\DataFixtures\ORM;

use AndresGotta\Academy\Domain\Entity\Indication;
use AndresGotta\Academy\Domain\ValueObject\MediaMaterial;
use Doctrine\Common\DataFixtures\AbstractFixture;
use Doctrine\Common\DataFixtures\OrderedFixtureInterface;
use Doctrine\Common\Persistence\ObjectManager;


class IndicationFixtures extends AbstractFixture implements OrderedFixtureInterface
{
    
    /**
     * {@inheritDoc}
     */
    public function getOrder()
    {
        return 2;
    }

    /**
     * {@inheritDoc}
     */
    public function load(ObjectManager $manager)
    {
        $teacher = $this->getReference('user-teacher');
        $mediaMaterial = new MediaMaterial($teacher, 'http://youtube.com', 'video');

        $indication = new Indication($mediaMaterial, 'Name', 'Description', []);

        $manager->persist($indication);
        $this->addReference('academy-media-material', $mediaMaterial);
        $this->addReference('academy-indication', $indication);

        $manager->flush();
    }


}