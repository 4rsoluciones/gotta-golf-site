<?php

namespace AndresGotta\Bundle\AcademyBundle\Service;

use AndresGotta\Academy\Domain\Entity\Routine;
use WebFactory\Bundle\PushNotificationBundle\Model\HasDevicesInterface;
use WebFactory\Bundle\UserBundle\Entity\User;

class NewRoutineNotifier extends AbstractDualNotifier
{

    /**
     * @param Routine[] $routines
     */
    public function notify($routines)
    {
        /* @var Routine $routine */
        foreach ($routines as $routine) {
            /* @var HasDevicesInterface $student */
            $student = $routine->getStudent();
            if (!$student->getDevices()->isEmpty()) {
                $this->sendPushNotification($routine);
            } else {
                $this->sendEmail($routine);
            }
        }
    }

    /**
     * @param Routine $routine
     */
    private function sendEmail(Routine $routine)
    {
        /* @var User $student */
        $student = $routine->getStudent();
        $email = $student->getEmail();

        $template = 'AndresGottaAcademyBundle:Api/Email:new_routine.html.twig';
        $context = [
            'student' => $student,
        ];

        $this->sendMessage($template, $context, 'info@andresgotta.com.ar', $email);
    }

    /**
     * @param Routine $routine
     */
    private function sendPushNotification(Routine $routine)
    {
        /* @var HasDevicesInterface $student */
        $student = $routine->getStudent();
        $this->sendPush($student->getDevices(), $title = 'Nueva rutina', $text = 'Nueva rutina disponible', $data = [], $extras = []);
    }
}