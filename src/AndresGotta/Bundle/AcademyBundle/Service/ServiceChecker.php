<?php

namespace AndresGotta\Bundle\AcademyBundle\Service;

use League\OAuth2\Client\Token\AccessToken;
use WebFactory\Bundle\UserBundle\Entity\User;
use WebFactory\Bundle\UserBundle\Service\GottaCenterApiInterface;

class ServiceChecker
{
    /**
     * @var GottaCenterApiInterface
     */
    private $api;

    /**
     * @var array
     */
    private $services;

    /**
     * ServiceChecker constructor.
     * @param GottaCenterApiInterface $api
     */
    public function __construct(GottaCenterApiInterface $api)
    {
        $this->api = $api;

        $this->services = [
            [
                'id' => 1,
                'students' => 20,
                'teachers' => 1,
                'places' => 1,
            ],
            [
                'id' => 2,
                'students' => 40,
                'teachers' => 1,
                'places' => 1,
            ],
            [
                'id' => 3,
                'students' => -1,
                'teachers' => 1,
                'places' => 1,
            ],
            [
                'id' => 4,
                'students' => 30,
                'teachers' => 2,
                'places' => 1,
            ],
            [
                'id' => 5,
                'students' => 50,
                'teachers' => 5,
                'places' => 1,
            ],
            [
                'id' => 6,
                'students' => -1,
                'teachers' => 10,
                'places' => 1,
            ],
            [
                'id' => 7,
                'students' => 30,
                'teachers' => 2,
                'places' => 2,
            ],
            [
                'id' => 8,
                'students' => 75,
                'teachers' => 5,
                'places' => 4,
            ],
            [
                'id' => 9,
                'students' => -1,
                'teachers' => 15,
                'places' => 10,
            ],

        ];
    }

    /**
     * @param User $academy
     * @param AccessToken $accessToken
     * @return null
     * @throws \GuzzleHttp\Exception\GuzzleException
     */
    public function getFirstAcademyService(User $academy, AccessToken $accessToken)
    {
        $services = $this->api->getServicesStatus($academy->getEmail(), $accessToken);
        $servicesIds = array_map(function ($i) {
            return $i['id'];
        }, $this->services);

        $serviceId = 0;
        foreach ($services as $service) {
            if (in_array($service['service_id'], $servicesIds)) {
                if ($serviceId < $service['service_id']) {
                    $serviceId = $service['service_id'];
                }
            }
        }

        return $serviceId;
    }

    /**
     * @param User $academy
     * @param AccessToken $userAccessToken
     * @return bool
     * @throws \GuzzleHttp\Exception\GuzzleException
     */
    public function canAddStudent(User $academy, AccessToken $userAccessToken)
    {
        $academyServiceId = $this->getFirstAcademyService($academy, $userAccessToken);
        if (!$academyServiceId) {
            return false;
        }

        foreach ($this->services as $index => $service) {
            if ($service['id'] == $academyServiceId) {
                break;
            }
        }

        $max = $this->services[$index]['students'];

        return !($max && -1 != $max && $academy->getAcademyStudents()->count() >= $max);
    }

    /**
     * @param User $academy
     * @param AccessToken $userAccessToken
     * @return bool
     * @throws \GuzzleHttp\Exception\GuzzleException
     */
    public function canAddTeacher(User $academy, AccessToken $userAccessToken)
    {
        $academyServiceId = $this->getFirstAcademyService($academy, $userAccessToken);
        if (!$academyServiceId) {
            return false;
        }

        foreach ($this->services as $index => $service) {
            if ($service['id'] == $academyServiceId) {
                break;
            }
        }

        $max = $this->services[$index]['teachers'];

        return !($max && -1 != $max && $academy->getAcademyTeachers()->count() >= $max);
    }

    /**
     * @param User $academy
     * @param AccessToken $userAccessToken
     * @return bool
     * @throws \GuzzleHttp\Exception\GuzzleException
     */
    public function canAddPlace(User $academy, AccessToken $userAccessToken)
    {
        $academyServiceId = $this->getFirstAcademyService($academy, $userAccessToken);
        if (!$academyServiceId) {
            return false;
        }

        foreach ($this->services as $index => $service) {
            if ($service['id'] == $academyServiceId) {
                break;
            }
        }

        $max = $this->services[$index]['places'];

        return !($max && -1 != $max && $academy->getAcademyPlaces()->count() >= $max);
    }
}