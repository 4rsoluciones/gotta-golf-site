<?php

namespace AndresGotta\Bundle\AcademyBundle\Service;

use WebFactory\Bundle\PushNotificationBundle\Factory\MessageFactory;
use WebFactory\Bundle\PushNotificationBundle\Manager\NotificationManager;

abstract class AbstractDualNotifier
{
    /**
     * @var \Swift_Mailer
     */
    private $mailer;

    /**
     * @var \Twig_Environment
     */
    private $twig;

    /**
     * @var MessageFactory
     */
    private $messageFactory;

    /**
     * @var NotificationManager
     */
    private $notificationManager;

    /**
     * AbstractNotifier constructor.
     * @param \Swift_Mailer $mailer
     * @param \Twig_Environment $twig
     * @param MessageFactory $messageFactory
     * @param NotificationManager $notificationManager
     */
    public function __construct(\Swift_Mailer $mailer, \Twig_Environment $twig, MessageFactory $messageFactory, NotificationManager $notificationManager)
    {
        $this->mailer = $mailer;
        $this->twig = $twig;
        $this->messageFactory = $messageFactory;
        $this->notificationManager = $notificationManager;
    }

    /**
     * @param string $templateName
     * @param array  $context
     * @param string $fromEmail
     * @param string $toEmail
     */
    protected function sendMessage($templateName, $context, $fromEmail, $toEmail)
    {
        $template = $this->twig->loadTemplate($templateName);
        $subject = $template->renderBlock('subject', $context);
        $textBody = $template->renderBlock('body_text', $context);
        $htmlBody = $template->renderBlock('body_html', $context);

        $message = \Swift_Message::newInstance()
            ->setSubject($subject)
            ->setFrom($fromEmail)
            ->setTo($toEmail);

        if (!empty($htmlBody)) {
            $message->setBody($htmlBody, 'text/html')
                ->addPart($textBody, 'text/plain');
        } else {
            $message->setBody($textBody);
        }

        $this->mailer->send($message);
    }

    /**
     * @param $devices
     * @param string $title
     * @param string $text
     * @param array $data
     * @param array $extras
     */
    protected function sendPush($devices, $title = '', $text = '', $data = [], $extras = [])
    {
        $message = $this->messageFactory->create('', $title, $text, $data, $extras);
        foreach ($devices as $device) {
            $message->addRegisteredDevice($device);
        }
        $this->notificationManager->send($message);
    }
}