<?php

namespace AndresGotta\Bundle\AcademyBundle\Service;

use AndresGotta\Academy\Domain\Entity\Lesson;
use AndresGotta\Academy\Domain\Entity\Routine;
use WebFactory\Bundle\PushNotificationBundle\Model\HasDevicesInterface;
use WebFactory\Bundle\UserBundle\Entity\User;

class NewLessonNotifier extends AbstractDualNotifier
{

    /**
     * @param Lesson[] $lessons
     */
    public function notify($lessons)
    {
        /* @var Routine $lesson */
        foreach ($lessons as $lesson) {
            /* @var HasDevicesInterface $student */
            $student = $lesson->getStudent();
            if (!$student->getDevices()->isEmpty()) {
                $this->sendPushNotification($lesson);
            } else {
                $this->sendEmail($lesson);
            }
        }
    }

    /**
     * @param Lesson $lesson
     */
    private function sendEmail(Lesson $lesson)
    {
        /* @var User $student */
        $student = $lesson->getStudent();
        $email = $student->getEmail();

        $template = 'AndresGottaAcademyBundle:Api/Email:new_lesson.html.twig';
        $context = [
            'student' => $student,
        ];

        $this->sendMessage($template, $context, 'info@andresgotta.com.ar', $email);
    }

    /**
     * @param Lesson $lesson
     */
    private function sendPushNotification(Lesson $lesson)
    {
        /* @var HasDevicesInterface $student */
        $student = $lesson->getStudent();
        $this->sendPush($student->getDevices(), $title = 'Nuevo informe', $text = 'Nuevo informe disponible', $data = [], $extras = []);
    }
}