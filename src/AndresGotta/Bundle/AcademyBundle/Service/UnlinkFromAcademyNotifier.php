<?php

namespace AndresGotta\Bundle\AcademyBundle\Service;

use WebFactory\Bundle\PushNotificationBundle\Model\HasDevicesInterface;
use WebFactory\Bundle\UserBundle\Entity\User;

class UnlinkFromAcademyNotifier extends AbstractDualNotifier
{

    /**
     * @param User $user
     * @param User $academy
     */
    public function notify(User $user, User $academy)
    {
        /* @var HasDevicesInterface $user */
        if (!$user->getDevices()->isEmpty()) {
            $this->sendPushNotification($user, $academy);
        } else {
            $this->sendEmail($user, $academy);
        }
    }

    /**
     * @param User $user
     * @param User $academy
     */
    private function sendEmail(User $user, User $academy)
    {
        $email = $user->getEmail();

        $template = 'AndresGottaAcademyBundle:Api/Email:unlink_from_academy.html.twig';
        $context = [
            'user' => $user,
            'academy' => $academy,
        ];

        $this->sendMessage($template, $context, 'info@andresgotta.com.ar', $email);
    }

    /**
     * @param User $user
     * @param User $academy
     */
    private function sendPushNotification(User $user, User $academy)
    {
        $this->sendPush($user->getDevices(),
            $title = 'Te han desvinculado de una académia',
            $text = 'Te han desvinculado de la académia: ' . $academy->getAcademyName(),
            $data = [], $extras = []);
    }
}