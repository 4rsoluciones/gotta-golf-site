<?php

namespace AndresGotta\Bundle\AcademyBundle\Service;

use AndresGotta\Bundle\AcademyBundle\Entity\AccessToken;
use Doctrine\ORM\EntityManager;
use GuzzleHttp\Client;

class AccessTokenManager
{
    /**
     * @var Client
     */
    private $client;
    /**
     * @var EntityManager
     */
    private $em;

    /**
     * AccessTokenManager constructor.
     * @param Client $client
     * @param EntityManager $em
     */
    public function __construct(Client $client, EntityManager $em)
    {
        $this->client = $client;
        $this->em = $em;
    }

    /**
     * @param AccessToken $accessToken
     * @return bool
     */
    public function isValid(AccessToken $accessToken)
    {
        return $accessToken->isExpired(new \DateTime());
    }

    /**
     * @param AccessToken $accessToken
     */
    public function invalidate(AccessToken $accessToken)
    {
        $result = $this->client->post(AccessToken::urlGetToken,
            [
                'form_params' =>
                    $accessToken->serialize()
                ,
                'headers' => [
                    'Content-Type' => 'application/x-www-form-urlencoded',
                ]
            ]);

        $result = json_decode((string)$result->getBody(), true);
        if (!$result) {
            throw new \RuntimeException('No es posible obtener access token');
        }

        $accessToken->updateAccessToken($result['access_token'], $result['token_type'], $result['expires_in']);

        $this->em->flush();
    }
}