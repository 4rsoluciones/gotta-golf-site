<?php

namespace AndresGotta\Bundle\AcademyBundle\Service;

use WebFactory\PushNotification\DeviceInterface;
use WebFactory\PushNotification\MessageInterface;
use WebFactory\PushNotification\NotificationService\PayloadGenerator\AndroidGCMPayloadGenerator as BaseAndroidGCMPayloadGenerator;

class AndroidGCMPayloadGenerator extends BaseAndroidGCMPayloadGenerator
{
    public function generate(MessageInterface $message)
    {
        $registeredDevices = $message->getRegisteredDevicesByType(DeviceInterface::ANDROID_GCM_TYPE);

        $payload = [
            'registration_ids' => $registeredDevices,
            'data' => [
                'title' => $message->getTitle(),
                'body' => $message->getBody(),
                'sound' => 'default',
                'vibrate' => true,
            ]
        ];

        if ($message->getExtras()) {
            foreach ($message->getExtras() as $key => $value) {
                $payload['data'][$key] = $value;
            }
        }

        if ($message->getData()) {
            foreach ($message->getData() as $key => $value) {
                $payload['data'][$key] = $value;
            }
        }

        $this->setRegisteredDevices($registeredDevices);

        return $payload;
    }
}
