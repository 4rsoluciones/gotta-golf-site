<?php

namespace AndresGotta\Bundle\AcademyBundle\Tests\Controller\Api;

use Symfony\Bundle\FrameworkBundle\Test\WebTestCase;

class RoutineControllerTest extends WebTestCase
{
    const CREDENTIALS = ['PHP_AUTH_USER' => 'teacher', 'PHP_AUTH_PW' => 'teacher'];
    const STUDENT_CREDENTIALS = ['PHP_AUTH_USER' => 'student', 'PHP_AUTH_PW' => 'student'];

    const EXTRA_HEADERS = ['HTTP_SPORT' => 'sport', 'HTTP_ACADEMY' => 1];

    /**
     *
     */
    public function testAddRoutine()
    {
        $client = static::createClient();
        $params = [
            'add_routine' => [
                'title' => 'Title',
                'resume' => 'Resume',
                'students' => [5],
                'steps' => [
                    [
                        'title' => 'title 1',
                        'description' => 'description 1',
                    ],
                    [
                        'title' => 'title 2',
                        'description' => '',
                    ]
                ],
            ],
        ];
        $client->request('POST', '/api/teacher/routine/add', $params, [], self::CREDENTIALS + self::EXTRA_HEADERS);
        $this->assertTrue($client->getResponse()->isOk(), $client->getResponse());

        $client->request('GET', '/api/teacher/routines', $params, [], self::CREDENTIALS + self::EXTRA_HEADERS);
        $this->assertTrue($client->getResponse()->isOk(), $client->getResponse());
        $routines = json_decode($client->getResponse()->getContent(), true);

        $this->assertCount(1, $routines['routines']);

        $params = [
            'feedback' => [
                'routine' => $routines['routines'][0]['id'],
                'value' => 4,
                'notes' => null,
            ],
        ];
        $client->request('POST', '/api/student/routine/feedback/add', $params, [], self::STUDENT_CREDENTIALS + self::EXTRA_HEADERS);
        $this->assertTrue($client->getResponse()->isOk(), $client->getResponse());
    }
}
