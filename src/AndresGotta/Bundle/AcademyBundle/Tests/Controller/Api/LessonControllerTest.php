<?php

namespace AndresGotta\Bundle\AcademyBundle\Tests\Controller\Api;

use Symfony\Bundle\FrameworkBundle\Test\WebTestCase;
use WebFactory\Bundle\UserBundle\Entity\User;

class LessonControllerTest extends WebTestCase
{
    const CREDENTIALS = ['PHP_AUTH_USER' => 'teacher', 'PHP_AUTH_PW' => 'teacher'];

    const EXTRA_HEADERS = ['HTTP_SPORT' => 'sport', 'HTTP_ACADEMY' => 1];

    /**
     *
     */
    public function testTeacherStudents()
    {
        $client = static::createClient();
        $client->request('GET', '/api/teacher/students', [], [], self::CREDENTIALS + self::EXTRA_HEADERS);
        $this->assertTrue($client->getResponse()->isOk(), $client->getResponse());
        $json = json_decode($client->getResponse()->getContent(), true);
        $this->assertArrayHasKey('students', $json);
    }

    /**
     *
     */
    public function testTeacherLessons()
    {
        $client = static::createClient();
        $client->request('GET', '/api/teacher/lessons', [], [], self::CREDENTIALS + self::EXTRA_HEADERS);
        $this->assertTrue($client->getResponse()->isOk(), $client->getResponse());
        $json = json_decode($client->getResponse()->getContent(), true);
        $this->assertArrayHasKey('lessons', $json);
    }

    /**
     *
     */
    public function testStudentLessonsInvalid()
    {
        $client = static::createClient();
        $client->request('GET', '/api/student/lessons', [], [], self::CREDENTIALS + self::EXTRA_HEADERS);
        $this->assertTrue($client->getResponse()->isForbidden());
    }

    /**
     *
     */
    public function testStudentLessons()
    {
        $client = static::createClient();
        $client->request('GET', '/api/student/lessons', [], [], ['PHP_AUTH_USER' => 'student', 'PHP_AUTH_PW' => 'student']+ self::EXTRA_HEADERS);
        $this->assertTrue($client->getResponse()->isOk(), $client->getResponse());
        $json = json_decode($client->getResponse()->getContent(), true);
        $this->assertArrayHasKey('lessons', $json);
    }

    /**
     *
     */
    public function testAddLesson()
    {
        $client = static::createClient();
        $params = [
            'add_lesson' => [
                'title' => 'Title',
                'date' => (new \DateTime())->format('Y-m-d'),
                'place' => 1,
                'students' => [5],
                'resume' => 'Resume',
                'indications' => [1],
            ],
        ];
        $client->request('POST', '/api/teacher/lesson/add', $params, [], self::CREDENTIALS + self::EXTRA_HEADERS);
        $this->assertTrue($client->getResponse()->isOk(), $client->getResponse());
    }
}
