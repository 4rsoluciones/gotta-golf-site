<?php

namespace AndresGotta\Bundle\AcademyBundle\Tests\Controller\Api;

use Symfony\Bundle\FrameworkBundle\Test\WebTestCase;

class UserControllerTest extends WebTestCase
{
    /**
     *
     */
    public function testRequestRecover()
    {
        $client = static::createClient();
        $client->request('GET', '/api/recover/request', [], [], []);
        $this->assertFalse($client->getResponse()->isOk(), $client->getResponse());
        $client->request('GET', '/api/recover/request', ['email' => 'teacher@example.com'], [], []);
        $this->assertTrue($client->getResponse()->isOk(), $client->getResponse());
    }

}
