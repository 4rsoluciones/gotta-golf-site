<?php

namespace AndresGotta\Bundle\AcademyBundle\Repository;

use AndresGotta\Academy\Domain\Entity\AcademyInterface;
use AndresGotta\Academy\Domain\Entity\Goal;
use AndresGotta\Academy\Domain\Entity\StudentInterface;
use AndresGotta\Academy\Domain\Entity\TeacherInterface;
use AndresGotta\Academy\Domain\Repository\GoalRepositoryInterface;
use Doctrine\ORM\EntityRepository;

class GoalRepository extends EntityRepository implements GoalRepositoryInterface
{
    /**
     * @inheritdoc
     */
    public function findAllOrderedByMostUsedByStudent($query, StudentInterface $student, AcademyInterface $academy, $offset = 0, $limit = 20)
    {
        $qb = $this->createQueryBuilder('Goal')
            ->leftJoin('AndresGotta\Academy\Domain\Entity\Indication', 'Indication',
                'WITH', 'Goal MEMBER OF Indication.goals')
            ->setParameter('teachers', $student->getTrainedByTeachers())
            ->leftJoin('Indication.teacher', 'Teacher',
                'WITH', 'Indication.teacher IN (:teachers)')
            ->leftJoin('AndresGotta\Academy\Domain\Entity\AcademyPlace', 'Place',
                'WITH', 'Goal MEMBER OF Indication.goals AND Place.academy = :academy')
            ->andWhere('Goal.name LIKE :query')
            ->setParameter('academy', $academy)
            ->setParameter('query', "%{$query}%")
            ->setFirstResult($offset)
            ->setMaxResults($limit)
            ->addSelect('COUNT(Indication) AS HIDDEN IndicationCount ')
            ->groupBy('Goal')
            ->orderBy('IndicationCount', 'DESC')
        ;

        return $qb->getQuery()->getResult();
    }


    /**
     * @inheritdoc
     */
    public function findAllOrderedByMostUsedBy($query, TeacherInterface $teacher, AcademyInterface $academy, $offset = 0, $limit = 20)
    {
        $qb = $this->createQueryBuilder('Goal')
            ->leftJoin('AndresGotta\Academy\Domain\Entity\Indication', 'Indication',
                'WITH', 'Goal MEMBER OF Indication.goals AND Indication.teacher = :teacher')
            ->setParameter('teacher', $teacher)
            ->leftJoin('AndresGotta\Academy\Domain\Entity\AcademyPlace', 'Place',
                'WITH', 'Goal MEMBER OF Indication.goals AND Place.academy = :academy')
            ->andWhere('Goal.name LIKE :query')
            ->setParameter('academy', $academy)
            ->setParameter('query', "%{$query}%")
            ->setFirstResult($offset)
            ->setMaxResults($limit)
            ->addSelect('COUNT(Indication) AS HIDDEN IndicationCount ')
            ->groupBy('Goal')
            ->orderBy('IndicationCount', 'DESC')
        ;

        return $qb->getQuery()->getResult();
    }

    /**
     * @inheritdoc
     */
    public function addGoal(Goal $goal)
    {
        $this->getEntityManager()->persist($goal);
        $this->getEntityManager()->flush();
    }

    /**
     * @inheritdoc
     */
    public function findOneByName($name)
    {
        return $this->findOneBy(compact('name'));
    }


}