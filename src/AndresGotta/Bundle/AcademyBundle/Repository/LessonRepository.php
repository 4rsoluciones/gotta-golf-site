<?php

namespace AndresGotta\Bundle\AcademyBundle\Repository;

use AndresGotta\Academy\Domain\Entity\AcademyInterface;
use AndresGotta\Academy\Domain\Entity\Lesson;
use AndresGotta\Academy\Domain\Entity\StudentInterface;
use AndresGotta\Academy\Domain\Entity\TeacherInterface;
use AndresGotta\Academy\Domain\Repository\LessonRepositoryInterface;
use Doctrine\ORM\EntityRepository;

class LessonRepository extends EntityRepository implements LessonRepositoryInterface
{

    /**
     * @inheritdoc
     */
    public function findAllByStudent(StudentInterface $student, AcademyInterface $academy, $offset = null, $limit = null)
    {

        return $this->createQueryBuilder('Lesson')
            ->where('Lesson.student = :student')->setParameter('student', $student)
            ->join('Lesson.place', 'AcademyPlace')
            ->join('AcademyPlace.academy', 'LessonAcademy')
            ->join('Lesson.student', 'Student')
            ->join('Student.enrolledInAcademies', 'StudentAcademy')
            ->andWhere('LessonAcademy = :academy')
            ->andWhere('StudentAcademy = :academy')
            ->setParameter('academy', $academy)
            ->setFirstResult($offset)
            ->setMaxResults($limit)
            ->orderBy('Lesson.date', 'DESC')
            ->getQuery()->getResult();
    }

    /**
     * @inheritdoc
     */
    public function findAllByTeacher(TeacherInterface $teacher, AcademyInterface $academy, $offset = null, $limit = null)
    {
        return $this->createQueryBuilder('Lesson')
//            ->where('Lesson.teacher = :teacher')->setParameter('teacher', $teacher)
            ->join('Lesson.place', 'AcademyPlace')
            ->join('AcademyPlace.academy', 'LessonAcademy')
            ->join('Lesson.teacher', 'Teacher')
            ->join('Teacher.teachesInAcademies', 'TeacherAcademy')
            ->andWhere('LessonAcademy = :academy')
            ->andWhere('TeacherAcademy = :academy')
            ->setParameter('academy', $academy)
            ->setFirstResult($offset)
            ->setMaxResults($limit)
            ->orderBy('Lesson.date', 'DESC')
            ->getQuery()->getResult();
    }

    /**
     * @inheritdoc
     */
    public function addLesson(Lesson $lesson)
    {
        $em = $this->getEntityManager();
        $em->persist($lesson);
        $em->flush();
    }

    /**
     * @inheritdoc
     */
    public function editLesson(Lesson $lesson)
    {
        $em = $this->getEntityManager();
        $em->persist($lesson);
        $em->flush();
    }
}