<?php

namespace AndresGotta\Bundle\AcademyBundle\Repository;

use AndresGotta\Academy\Domain\Entity\AcademyInterface;
use AndresGotta\Academy\Domain\Entity\Routine;
use AndresGotta\Academy\Domain\Entity\StudentInterface;
use AndresGotta\Academy\Domain\Entity\TeacherInterface;
use AndresGotta\Academy\Domain\Repository\RoutineRepositoryInterface;
use Doctrine\ORM\EntityRepository;

class RoutineRepository extends EntityRepository implements RoutineRepositoryInterface
{
    /**
     * @inheritdoc
     */
    public function findAllByStudent(StudentInterface $student, AcademyInterface $academy, $offset = 0, $limit = 20)
    {
        return $this->createQueryBuilder('Routine')
            ->join('Routine.student', 'Student')
            ->where('Student = :student')->setParameter('student', $student)
            ->andWhere(':academy MEMBER OF Student.enrolledInAcademies')
            ->andWhere('Routine.academy = :academy')
            ->setParameter('academy', $academy)
            ->setFirstResult($offset)
            ->setMaxResults($limit)
            ->orderBy('Routine.createdAt', 'DESC')
            ->getQuery()->getResult();
    }

    /**
     * @inheritdoc
     */
    public function findAllByTeacher(TeacherInterface $teacher, AcademyInterface $academy, $offset = 0, $limit = 20, $search = '')
    {
        return $this->createQueryBuilder('Routine')
            ->join('Routine.teacher', 'Teacher')
//            ->where('Teacher = :teacher')->setParameter('teacher', $teacher)
            ->andWhere(':academy MEMBER OF Teacher.teachesInAcademies')
            ->andWhere('Routine.title like :search')
            ->andWhere('Routine.academy = :academy')
            ->setParameter('academy', $academy)
            ->setParameter('search', "%$search%")
            ->setFirstResult($offset)
            ->setMaxResults($limit)
            ->orderBy('Routine.createdAt', 'DESC')
            ->getQuery()->getResult();
    }

    /**
     * @inheritdoc
     */
    public function addRoutine(Routine $routine)
    {
        $em = $this->getEntityManager();
        $em->persist($routine);
        $em->flush();
    }

}