<?php

namespace AndresGotta\Bundle\AcademyBundle\Repository;

use AndresGotta\Academy\Domain\Entity\AcademyInterface;
use AndresGotta\Academy\Domain\Entity\Indication;
use AndresGotta\Academy\Domain\Entity\StudentInterface;
use AndresGotta\Academy\Domain\Entity\TeacherInterface;
use AndresGotta\Academy\Domain\Repository\IndicationRepositoryInterface;
use Doctrine\ORM\EntityRepository;

class IndicationRepository extends EntityRepository implements IndicationRepositoryInterface
{
    /**
     * @inheritdoc
     */
    public function findAllByStudent($type, StudentInterface $student, AcademyInterface $academy, $goals = [], $query = '', $offset = 0, $limit = 20)
    {
        $qb = $this->createQueryBuilder('Indication')
            ->innerJoin('Indication.reports', 'Report')
            ->innerJoin('Report.lesson', 'Lesson')
            ->innerJoin('Lesson.place', 'Place')
            ->innerJoin('Lesson.student', 'Student')
            ->innerJoin('Student.enrolledInAcademies', 'StudentAcademy')
            ->andWhere('Student = :student')
            ->andWhere('Place.academy = :academy')
            ->andWhere('StudentAcademy = :academy')
            ->setParameter('student', $student)
            ->setParameter('academy', $academy)
            ->setFirstResult($offset)
            ->setMaxResults($limit)
            ->orderBy('Lesson.date', 'DESC')
        ;

        if ($type) {
            $qb->andWhere('Indication.mediaMaterial.type = :type')
                ->setParameter('type', $type);
        }

        if ($query) {
            $qb->andWhere('(Indication.name LIKE :query OR Indication.description LIKE :query)')
                ->setParameter('query', "%{$query}%");
        }

        if ($goals) {
            $qb->andWhere(':goals MEMBER OF Indication.goals')
                ->setParameter('goals', $goals);
        }

        return $qb->getQuery()->getResult();
    }

    /**
     * @inheritdoc
     */
    public function findAllByTeacher($type, TeacherInterface $teacher, AcademyInterface $academy, $goals = [], $query = '', $offset = 0, $limit = 20)
    {
        $qb = $this->createQueryBuilder('Indication')
            ->where('Indication.academy = :academy')
//            ->innerJoin('Indication.reports', 'Report')
//            ->innerJoin('Report.lesson', 'Lesson')
//            ->innerJoin('Lesson.place', 'Place')
//            ->innerJoin('Lesson.teacher', 'Teacher')
//            ->innerJoin('Teacher.teachesInAcademies', 'TeacherAcademy')
////            ->andWhere('Teacher = :teacher')
//            ->andWhere('Place.academy = :academy')
//            ->andWhere('TeacherAcademy = :academy')
//            ->setParameter('teacher', $teacher)
            ->setParameter('academy', $academy)
            ->setFirstResult($offset)
            ->setMaxResults($limit)
            ->orderBy('Indication.id', 'DESC')
//            ->orderBy('Lesson.date', 'DESC')
        ;

        if ($type) {
            $qb->andWhere('Indication.mediaMaterial.type = :type')
                ->setParameter('type', $type);
        }

        if ($query) {
            $qb->andWhere('(Indication.name LIKE :query OR Indication.description LIKE :query)')
                ->setParameter('query', "%{$query}%");
        }

        if ($goals) {
            $qb->andWhere(':goals MEMBER OF Indication.goals')
                ->setParameter('goals', $goals);
        }

        return $qb->getQuery()->getResult();
    }

    /**
     * @param Indication $indication
     */
    public function addIndication(Indication $indication)
    {
        $em = $this->getEntityManager();
        $em->persist($indication);
        $em->flush();
    }

    /**
     * @param Indication $indication
     */
    public function editIndication(Indication $indication)
    {
        $em = $this->getEntityManager();
        $em->persist($indication);
        $em->flush();
    }
}