<?php

namespace AndresGotta\Bundle\AcademyBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity
 * @ORM\Table(name="access_tokens")
 */
class AccessToken
{
    /**
     *
     */
    const urlGetToken = 'https://accounts.google.com/o/oauth2/token';

    /**
     *
     */
    const ACCESS_TOKEN_TYPE_YOUTUBE = 'youtube';
    /**
     *
     */
    const ACCESS_TOKEN_TYPE_DRIVE = 'drive';

    /**
     * @var
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @var string
     * @ORM\Column(type="string")
     */
    private $type = self::ACCESS_TOKEN_TYPE_YOUTUBE;

    /**
     * @var string
     * @ORM\Column(type="string")
     */
    private $grantType = 'refresh_token';
    /**
     * @var string
     * @ORM\Column(type="string")
     */
    private $refreshToken;
    /**
     * @var string
     * @ORM\Column(type="string")
     */
    private $accessToken;

    /**
     * @var string
     * @ORM\Column(type="string")
     */
    private $accessTokenType;

    /**
     * @var string
     * @ORM\Column(type="string")
     */
    private $clientId;
    /**
     * @var string
     * @ORM\Column(type="string")
     */
    private $clientSecret;

    /**
     * @var integer
     * @ORM\Column(type="string")
     */
    private $expiresIn;

    /**
     * @var \DateTime
     * @ORM\Column(type="datetime")
     */
    private $requestedAt;

    /**
     * AccessToken constructor.
     * @param string $type
     * @param string $grantType
     * @param string $refreshToken
     * @param string $clientId
     * @param string $clientSecret
     */
    public function __construct($type, $grantType, $refreshToken, $clientId, $clientSecret)
    {
        $this->type = $type;
        $this->grantType = $grantType;
        $this->refreshToken = $refreshToken;
        $this->clientId = $clientId;
        $this->clientSecret = $clientSecret;
    }


    /**
     * @return string
     */
    public function getAccessToken()
    {
        return $this->accessToken;
    }

    /**
     * @param string $accessToken
     */
    public function updateAccessToken($accessToken, $accessTokenType, $expiresIn)
    {
        $this->accessToken = $accessToken;
        $this->accessTokenType = $accessTokenType;
        $this->expiresIn = $expiresIn;
        $this->requestedAt = new \DateTime();
    }

    /**
     * @return array
     */
    public function serialize()
    {
        return [
            'client_id' => $this->clientId,
            'client_secret' => $this->clientSecret,
            'refresh_token' => $this->refreshToken,
            'grant_type' => $this->grantType,
        ];
    }

    /**
     * @param \DateTime $when
     * @return bool
     */
    public function isExpired(\DateTime $when)
    {
        return $when < $this->requestedAt->add(new \DateInterval("PT{$this->expiresIn}S"));
    }

}