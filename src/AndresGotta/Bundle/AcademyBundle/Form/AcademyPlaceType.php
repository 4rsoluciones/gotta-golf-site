<?php

namespace AndresGotta\Bundle\AcademyBundle\Form;

use AndresGotta\Bundle\AcademyBundle\Form\Model\AcademyPlaceDTO;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Symfony\Component\OptionsResolver\OptionsResolverInterface;

class AcademyPlaceType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('name')
            ->add('address', null, [
                'required' => false
            ])
        ;
    }

    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults([
            'data_class' => AcademyPlaceDTO::class,
            'csrf_protection' => false,
        ]);
    }

    public function setDefaultOptions(OptionsResolverInterface $resolver)
    {
        $this->configureOptions($resolver);
    }


    public function getBlockPrefix()
    {
        return 'add_lesson';
    }

    public function getName()
    {
        return $this->getBlockPrefix();
    }
}
