<?php

namespace AndresGotta\Bundle\AcademyBundle\Form;

use AndresGotta\Academy\Domain\Entity\AcademyPlace;
use AndresGotta\Academy\Domain\Entity\Indication;
use AndresGotta\Academy\Domain\Entity\Lesson;
use Doctrine\ORM\EntityRepository;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Symfony\Component\OptionsResolver\OptionsResolverInterface;

class EditLessonType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('title')
            ->add('date', 'date', [
                'widget' => 'single_text'
            ])
            ->add('place', 'entity', [
                'class' => AcademyPlace::class,
                'query_builder' => function (EntityRepository $repository) use ($options) {
                    return $repository->createQueryBuilder('Place')
                        ->where('Place IN (:places)')->setParameter('places', $options['places']);
                },
            ])
            ->add('resume', 'text', [
                'property_path' => 'report.resume',
            ])
            ->add('indications', 'entity', [
                'class' => Indication::class,
                'multiple' => true,
                'mapped' => false,
            ]);
    }

    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults([
            'data_class' => Lesson::class,
            'csrf_protection' => false,
            'places' => [],
        ]);
    }

    public function setDefaultOptions(OptionsResolverInterface $resolver)
    {
        $this->configureOptions($resolver);
    }


    public function getBlockPrefix()
    {
        return 'lesson';
    }

    public function getName()
    {
        return $this->getBlockPrefix();
    }
}
