<?php

namespace AndresGotta\Bundle\AcademyBundle\Form;

use Doctrine\ORM\EntityRepository;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Symfony\Component\OptionsResolver\OptionsResolverInterface;
use WebFactory\Bundle\UserBundle\Entity\User;

class AcademyTeachersStudentsType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('academyTeachers', 'entity', [
                'class' => User::class,
                'query_builder' => function (EntityRepository $er) {
                    return $er->createQueryBuilder('Teacher')
                        ->where('Teacher.roles LIKE :role')->setParameter('role', '%ROLE_TEACHER%');
                },
                'required' => false,
                'multiple' => true,
            ])
            ->add('academyStudents', 'entity', [
                'class' => User::class,
                'query_builder' => function (EntityRepository $er) {
                    return $er->createQueryBuilder('Student')
                        ->where('Student.roles LIKE :role')->setParameter('role', '%ROLE_STUDENT%');
                },
                'required' => false,
                'multiple' => true,
            ]);
    }


    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults([
            'data_class' => User::class,
            'csrf_protection' => false,
        ]);
    }

    public function setDefaultOptions(OptionsResolverInterface $resolver)
    {
        $this->configureOptions($resolver);
    }


    public function getBlockPrefix()
    {
        return 'academy_teachers';
    }

    public function getName()
    {
        return $this->getBlockPrefix();
    }
}