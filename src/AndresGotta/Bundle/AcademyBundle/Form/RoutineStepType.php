<?php

namespace AndresGotta\Bundle\AcademyBundle\Form;

use AndresGotta\Academy\Application\Command\DataTransferObject\RoutineStep;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\DataMapperInterface;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Symfony\Component\OptionsResolver\OptionsResolverInterface;

class RoutineStepType extends AbstractType implements DataMapperInterface
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('title')
            ->add('description')
            ->setDataMapper($this);
    }

    public function setDefaultOptions(OptionsResolverInterface $resolver)
    {
        $this->configureOptions($resolver);
    }

    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults([
            'data_class' => RoutineStep::class,
            'empty_data' => null,
            'csrf_protection' => false,
        ]);
    }

    public function getName()
    {
        return $this->getBlockPrefix();
    }

    public function getBlockPrefix()
    {
        return 'step';
    }

    /**
     * @param AddRoutineCommand $data
     * @param FormInterface[]|\Traversable $forms
     */
    public function mapDataToForms($data, $forms)
    {
        $forms = iterator_to_array($forms);
        $forms['title']->setData($data ? $data->getTitle() : '');
        $forms['description']->setData($data ? $data->getDescription() : '');
    }

    public function mapFormsToData($forms, &$data)
    {
        $forms = iterator_to_array($forms);
        $data = new RoutineStep(
            $forms['title']->getData(),
            $forms['description']->getData()
        );
    }
}
