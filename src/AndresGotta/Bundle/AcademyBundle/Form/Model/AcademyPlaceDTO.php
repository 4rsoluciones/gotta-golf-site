<?php

namespace AndresGotta\Bundle\AcademyBundle\Form\Model;

use AndresGotta\Academy\Domain\Entity\AcademyPlace;
use AndresGotta\Academy\Domain\Entity\Goal;
use AndresGotta\Academy\Domain\Entity\MediaMaterial;
use AndresGotta\Academy\Domain\Entity\StudentInterface;
use AndresGotta\Academy\Domain\Entity\TeacherInterface;
use Symfony\Component\Validator\Constraints as Assert;

/**
 * Class AcademyPlaceDTO
 * @package AndresGotta\Bundle\AcademyBundle\Form\Model
 * @deprecated Se utiliza solo en backend, cuando se pueda, quitarlo
 */
class AcademyPlaceDTO
{
    /**
     * @var string
     * @Assert\NotBlank()
     */
    public $name;

    /**
     * @var string
     */
    public $address;

    /**
     * AcademyPlaceDTO constructor.
     * @param string $name
     * @param string $address
     */
    public function __construct($name = null, $address = null)
    {
        $this->name = $name;
        $this->address = $address;
    }
}
