<?php

namespace AndresGotta\Bundle\AcademyBundle\Form;

use AndresGotta\Academy\Application\Command\AddRoutineCommand;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\DataMapperInterface;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\Form\FormInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Symfony\Component\OptionsResolver\OptionsResolverInterface;
use WebFactory\Bundle\UserBundle\Entity\User;

class AddRoutineType extends AbstractType implements DataMapperInterface
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('title')
            ->add('resume')
            ->add('students', 'entity', [
                'class' => User::class,
                'multiple' => true,
            ])
            ->add('steps', 'collection', [
                'type' => new RoutineStepType(),
                'allow_add' => true,
                'allow_delete' => true,
                'error_bubbling' => false,
            ])->setDataMapper($this)
        ;
    }

    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults([
            'data_class' => AddRoutineCommand::class,
            'empty_data' => null,
            'csrf_protection' => false,
        ]);
    }

    public function setDefaultOptions(OptionsResolverInterface $resolver)
    {
        $this->configureOptions($resolver);
    }


    public function getBlockPrefix()
    {
        return 'add_routine';
    }

    public function getName()
    {
        return $this->getBlockPrefix();
    }

    /**
     * @param AddRoutineCommand $data
     * @param FormInterface[]|\Traversable $forms
     */
    public function mapDataToForms($data, $forms)
    {
        $forms = iterator_to_array($forms);
        $forms['title']->setData($data ? $data->getTitle() : '');
        $forms['resume']->setData($data ? $data->getResume() : '');
        $forms['students']->setData($data ? $data->getStudents() : []);
        $forms['steps']->setData($data ? $data->getSteps() : []);

    }

    public function mapFormsToData($forms, &$data)
    {
        $forms = iterator_to_array($forms);
        $data = new AddRoutineCommand(
            $forms['title']->getData(),
            $forms['resume']->getData(),
            $data->getTeacher(),
            is_array($forms['students']->getData()) ? $forms['students']->getData() : $forms['students']->getData()->toArray(),
            $forms['steps']->getData(),
            $data->getAcademy()
        );
    }


}
