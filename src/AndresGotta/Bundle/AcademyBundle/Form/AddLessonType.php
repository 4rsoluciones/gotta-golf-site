<?php

namespace AndresGotta\Bundle\AcademyBundle\Form;

use AndresGotta\Academy\Application\Command\AddLessonCommand;
use AndresGotta\Academy\Domain\Entity\AcademyPlace;
use AndresGotta\Academy\Domain\Entity\Indication;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\DataMapperInterface;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\Form\FormInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Symfony\Component\OptionsResolver\OptionsResolverInterface;
use WebFactory\Bundle\UserBundle\Entity\User;

class AddLessonType extends AbstractType implements DataMapperInterface
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('title')
            ->add('date', 'datetime', [
                'widget' => 'single_text',
            ])
            ->add('place', 'entity', [
                'class' => AcademyPlace::class,
            ])
            ->add('students', 'entity', [
                'class' => User::class,
                'multiple' => true,
            ])
            ->add('resume')
            ->add('indications', 'entity', [
                'class' => Indication::class,
                'multiple' => true,
            ])->setDataMapper($this)
        ;
    }

    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults([
            'data_class' => AddLessonCommand::class,
            'csrf_protection' => false,
        ]);
    }

    public function setDefaultOptions(OptionsResolverInterface $resolver)
    {
        $this->configureOptions($resolver);
    }

    /**
     * @param AddLessonCommand $data
     * @param FormInterface[]|\Traversable $forms
     */
    public function mapDataToForms($data, $forms)
    {
        $forms = iterator_to_array($forms);
        $forms['title']->setData($data ? $data->getTitle() : '');
        $forms['date']->setData($data ? $data->getDate() : new \DateTime());
        $forms['place']->setData($data ? $data->getPlace() : []);
        $forms['students']->setData($data ? $data->getStudents() : []);
        $forms['resume']->setData($data ? $data->getResume() : '');
        $forms['indications']->setData($data ? $data->getIndications() : []);

    }

    public function mapFormsToData($forms, &$data)
    {
        $forms = iterator_to_array($forms);
        $data = new AddLessonCommand(
            $forms['title']->getData(),
            $forms['date']->getData(),
            $forms['place']->getData(),
            $data->getTeacher(),
            $forms['students']->getData()->toArray(),
            $forms['resume']->getData(),
            $forms['indications']->getData()
        );
    }


    public function getBlockPrefix()
    {
        return 'add_lesson';
    }

    public function getName()
    {
        return $this->getBlockPrefix();
    }


}
