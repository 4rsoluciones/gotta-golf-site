<?php

namespace AndresGotta\Bundle\AcademyBundle\Form;

use AndresGotta\Academy\Application\Command\SetRoutineStepStatusCommand;
use AndresGotta\Academy\Domain\Entity\Routine;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\DataMapperInterface;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\Form\FormInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Symfony\Component\OptionsResolver\OptionsResolverInterface;

class SetRoutineStepStatusType extends AbstractType implements DataMapperInterface
{
    /**
     * @param FormBuilderInterface $builder
     * @param array $options
     */
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('routine', 'entity', [
                'class' => Routine::class,
                'choices' => $options['routines'],
                'required' => true,
            ])
            ->add('step', 'integer')
            ->add('completed', 'integer', [
                'required' => false,
            ])
            ->setDataMapper($this);
    }

    /**
     * @param OptionsResolverInterface $resolver
     */
    public function setDefaultOptions(OptionsResolverInterface $resolver)
    {
        $this->configureOptions($resolver);
    }

    /**
     * @param OptionsResolver $resolver
     */
    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults([
            'data_class' => SetRoutineStepStatusCommand::class,
            'routines' => [],
            'csrf_protection' => false,
        ]);
    }

    /**
     * @return string
     */
    public function getName()
    {
        return $this->getBlockPrefix();
    }

    /**
     * @return string
     */
    public function getBlockPrefix()
    {
        return 'routine_step_status';
    }

    /**
     * @param SetRoutineStepStatusCommand $data
     * @param FormInterface[]|\Traversable $forms
     */
    public function mapDataToForms($data, $forms)
    {
        $forms = iterator_to_array($forms);
        $forms['routine']->setData($data ? $data->getRoutine() : null);
        $forms['step']->setData($data ? $data->getStep() : null);
        $forms['completed']->setData($data ? $data->isCompleted() : null);
    }

    /**
     * @param FormInterface[]|\Traversable $forms
     * @param mixed $data
     */
    public function mapFormsToData($forms, &$data)
    {
        $forms = iterator_to_array($forms);
        $data = new SetRoutineStepStatusCommand(
            $forms['routine']->getData(),
            $forms['step']->getData(),
            $forms['completed']->getData()
        );
    }
}
