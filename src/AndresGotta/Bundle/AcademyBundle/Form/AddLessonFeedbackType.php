<?php

namespace AndresGotta\Bundle\AcademyBundle\Form;

use AndresGotta\Academy\Application\Command\SetLessonFeedbackCommand;
use AndresGotta\Academy\Domain\Entity\Lesson;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\DataMapperInterface;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\Form\FormInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Symfony\Component\OptionsResolver\OptionsResolverInterface;

class AddLessonFeedbackType extends AbstractType implements DataMapperInterface
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('lesson', 'entity', [
                'class' => Lesson::class,
                'choices' => $options['lessons'],
                'required' => true,
            ])
            ->add('value')
            ->add('notes')
            ->setDataMapper($this)
        ;
    }

    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults([
            'data_class' => SetLessonFeedbackCommand::class,
            'lessons' => [],
            'csrf_protection' => false,
        ]);
    }

    public function setDefaultOptions(OptionsResolverInterface $resolver)
    {
        $this->configureOptions($resolver);
    }

    /**
     * @param SetLessonFeedbackCommand $data
     * @param FormInterface[]|\Traversable $forms
     */
    public function mapDataToForms($data, $forms)
    {
        $forms = iterator_to_array($forms);
        $forms['lesson']->setData($data ? $data->getLesson() : null);
        $forms['value']->setData($data ? $data->getValue() : null);
        $forms['notes']->setData($data ? $data->getNotes() : null);

    }

    public function mapFormsToData($forms, &$data)
    {
        $forms = iterator_to_array($forms);
        $data = new SetLessonFeedbackCommand(
            $forms['lesson']->getData(),
            $forms['value']->getData(),
            $forms['notes']->getData()
        );
    }


    public function getBlockPrefix()
    {
        return 'feedback';
    }

    public function getName()
    {
        return $this->getBlockPrefix();
    }


}
