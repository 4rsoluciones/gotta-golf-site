<?php

namespace AndresGotta\Bundle\AcademyBundle\Form;

use AndresGotta\Academy\Application\Command\SetRoutineFeedbackCommand;
use AndresGotta\Academy\Domain\Entity\Routine;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\DataMapperInterface;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\Form\FormInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Symfony\Component\OptionsResolver\OptionsResolverInterface;

class AddRoutineFeedbackType extends AbstractType implements DataMapperInterface
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('routine', 'entity', [
                'class' => Routine::class,
                'choices' => $options['routines'],
                'required' => true,
            ])
            ->add('value')
            ->add('notes')
            ->setDataMapper($this);
        ;
    }

    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults([
            'data_class' => SetRoutineFeedbackCommand::class,
            'routines' => [],
            'csrf_protection' => false,
        ]);
    }

    public function setDefaultOptions(OptionsResolverInterface $resolver)
    {
        $this->configureOptions($resolver);
    }


    public function getBlockPrefix()
    {
        return 'feedback';
    }

    public function getName()
    {
        return $this->getBlockPrefix();
    }


    /**
     * @param SetRoutineFeedbackCommand $data
     * @param FormInterface[]|\Traversable $forms
     */
    public function mapDataToForms($data, $forms)
    {
        $forms = iterator_to_array($forms);
        $forms['routine']->setData($data ? $data->getRoutine() : null);
        $forms['value']->setData($data ? $data->getValue() : null);
        $forms['notes']->setData($data ? $data->getNotes() : null);
    }

    /**
     * @param FormInterface[]|\Traversable $forms
     * @param mixed $data
     */
    public function mapFormsToData($forms, &$data)
    {
        $forms = iterator_to_array($forms);
        $data = new SetRoutineFeedbackCommand(
            $forms['routine']->getData(),
            $forms['value']->getData(),
            $forms['notes']->getData()
        );
    }
}
