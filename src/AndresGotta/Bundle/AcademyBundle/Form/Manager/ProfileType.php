<?php

namespace AndresGotta\Bundle\AcademyBundle\Form\Manager;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;
use WebFactory\Bundle\UserBundle\Entity\Profile;

class ProfileType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('firstName')
            ->add('lastName')
        ;
    }

    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults([
            'data_class' => Profile::class,
        ]);
    }

    public function getName()
    {
        return 'andres_gotta_bundle_academy_form_manager_profile_type';
    }
}
