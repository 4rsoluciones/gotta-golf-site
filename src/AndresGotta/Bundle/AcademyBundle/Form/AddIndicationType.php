<?php

namespace AndresGotta\Bundle\AcademyBundle\Form;

use AndresGotta\Academy\Application\Command\AddIndicationCommand;
use AndresGotta\Academy\Domain\Entity\Goal;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\DataMapperInterface;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\Form\FormInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Symfony\Component\OptionsResolver\OptionsResolverInterface;

class AddIndicationType extends AbstractType implements DataMapperInterface
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('mediaMaterial', new MediaMaterialType())
            ->add('name')
            ->add('description')
            ->add('goals', 'entity', [
                'class' => Goal::class,
                'multiple' => true,
            ])
            ->add('requirements')
            ->add('security')
            ->add('alternatives')
            ->setDataMapper($this)
        ;
    }

    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults([
            'data_class' => AddIndicationCommand::class,
            'csrf_protection' => false,
        ]);
    }

    public function setDefaultOptions(OptionsResolverInterface $resolver)
    {
        $this->configureOptions($resolver);
    }


    public function getBlockPrefix()
    {
        return 'indication';
    }

    public function getName()
    {
        return $this->getBlockPrefix();
    }


    /**
     * @param AddIndicationCommand $data
     * @param FormInterface[]|\Traversable $forms
     */
    public function mapDataToForms($data, $forms)
    {
        $forms = iterator_to_array($forms);
        $forms['mediaMaterial']->setData($data ? $data->getMediaMaterial() : null);
        $forms['name']->setData($data ? $data->getName() : '');
        $forms['description']->setData($data ? $data->getDescription() : '');
        $forms['goals']->setData($data ? $data->getGoals() : []);
        $forms['requirements']->setData($data ? $data->getRequirements() : '');
        $forms['security']->setData($data ? $data->getSecurity() : '');
        $forms['alternatives']->setData($data ? $data->getAlternatives() : '');
    }

    public function mapFormsToData($forms, &$data)
    {
        $forms = iterator_to_array($forms);
        $data = new AddIndicationCommand(
            $data->getTeacher(),
            $forms['mediaMaterial']->getData(),
            $forms['name']->getData(),
            $forms['description']->getData(),
            $forms['goals']->getData() ? $forms['goals']->getData() : [],
            $forms['requirements']->getData(),
            $forms['security']->getData(),
            $forms['alternatives']->getData()
        );
    }
}
