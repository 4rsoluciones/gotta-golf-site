<?php

namespace AndresGotta\Bundle\AcademyBundle\Form;

use Doctrine\ORM\EntityRepository;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Symfony\Component\OptionsResolver\OptionsResolverInterface;
use WebFactory\Bundle\UserBundle\Entity\User;

class TeachesInAcademiesType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('teachesInAcademies', 'entity', [
                'class' => User::class,
                'query_builder' => function (EntityRepository $er) {
                    return $er->createQueryBuilder('Academy')
                        ->where('Academy.roles LIKE :role')->setParameter('role', '%ROLE_ACADEMY%');
                },
                'property' => function (User $academy) {
                    return $academy->getAcademyName();
                },
                'by_reference' => false,
                'required' => false,
                'multiple' => true,
                'label' => 'Academies'
            ])
            ->add('teachesToStudents', 'entity', [
                'class' => User::class,
                'query_builder' => function (EntityRepository $er) {
                    return $er->createQueryBuilder('Student')
                        ->where('Student.roles LIKE :role')->setParameter('role', '%ROLE_STUDENT%');
                },
                'required' => false,
                'multiple' => true,
                'label' => 'Students'
            ]);
    }


    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults([
            'data_class' => User::class,
            'csrf_protection' => false,
        ]);
    }

    public function setDefaultOptions(OptionsResolverInterface $resolver)
    {
        $this->configureOptions($resolver);
    }


    public function getBlockPrefix()
    {
        return 'teaches_in_academies';
    }

    public function getName()
    {
        return $this->getBlockPrefix();
    }
}
