<?php

namespace AndresGotta\Bundle\AcademyBundle\Form;

use AndresGotta\Academy\Domain\Entity\Goal;
use AndresGotta\Academy\Domain\Entity\Indication;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Symfony\Component\OptionsResolver\OptionsResolverInterface;

class EditIndicationType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('name')
            ->add('description')
            ->add('goals', 'entity', [
                'class' => Goal::class,
                'multiple' => true,
            ])
            ->add('requirements')
            ->add('security')
            ->add('alternatives')
            ->add('mediaMaterial', new MediaMaterialType())
        ;
    }

    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults([
            'data_class' => Indication::class,
            'csrf_protection' => false,
        ]);
    }

    public function setDefaultOptions(OptionsResolverInterface $resolver)
    {
        $this->configureOptions($resolver);
    }


    public function getBlockPrefix()
    {
        return 'indication';
    }

    public function getName()
    {
        return $this->getBlockPrefix();
    }
}
