<?php

namespace AndresGotta\Bundle\AcademyBundle\Listener;

use AndresGotta\Academy\Application\AcademyContext;
use AndresGotta\Academy\Domain\Entity\AcademyInterface;
use AndresGotta\Academy\Domain\ValueObject\Sport;
use AndresGotta\Bundle\AcademyBundle\Service\ServiceChecker;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\RedirectResponse;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpKernel\Event\GetResponseEvent;
use Symfony\Component\HttpKernel\HttpKernelInterface;
use Symfony\Component\Routing\Generator\UrlGeneratorInterface;
use Symfony\Component\Security\Core\Authentication\Token\Storage\TokenStorageInterface;
use Symfony\Component\Security\Core\Exception\AccessDeniedException;
use Symfony\Component\Security\Core\User\UserInterface;
use WebFactory\Bundle\UserBundle\Entity\User;
use WebFactory\Bundle\UserBundle\Model\UserManager;

/**
 * Class AcademyContextCapturer
 * @package AndresGotta\Bundle\AcademyBundle\Listener
 */
class AcademyMembershipChecker
{
    /**
     *
     */
    const ACADEMY = 'ACADEMY';

    /**
     * @var UserManager
     */
    private $userManager;

    /**
     * @var TokenStorageInterface
     */
    private $tokenStorage;

    /**
     * AcademyContextCapturer constructor.
     * @param UserManager $userManager
     * @param TokenStorageInterface $tokenStorage
     */
    public function __construct(UserManager $userManager, TokenStorageInterface $tokenStorage)
    {
        $this->tokenStorage = $tokenStorage;
        $this->userManager = $userManager;
    }

    /**
     * @param GetResponseEvent $event
     */
    public function onRequest(GetResponseEvent $event)
    {
        if (HttpKernelInterface::MASTER_REQUEST !== $event->getRequestType()) {
            return;
        }

        if (!preg_match('/^\/api\/(teacher|student)/', $event->getRequest()->getPathInfo())) {
            return;
        }

        $academyId = $event->getRequest()->headers->get(self::ACADEMY);

        /* @var AcademyInterface|User $academy */
        $academy = $this->userManager->findUserBy(['id' => $academyId]);
        if (!$academy || !$academy instanceof AcademyInterface) {
            return;
        }

        $isTeacher = preg_match('/^\/api\/teacher/', $event->getRequest()->getPathInfo());
        $isStudent = preg_match('/^\/api\/student/', $event->getRequest()->getPathInfo());

        /** @var User $user */
        $user = $this->getUser();
        if (!$user instanceof UserInterface) {
            return;
        }

        if ($isTeacher && !$user->getTeachesInAcademies()->contains($academy)) {
            $response = JsonResponse::create(['message' => 'Academy membership not found.'], 403);
            $event->setResponse($response);
        }

        if ($isStudent && !$user->getEnrolledInAcademies()->contains($academy)) {
            $response = JsonResponse::create(['message' => 'Academy membership not found.'], 403);
            $event->setResponse($response);
        }
    }

    /**
     * @return mixed|null
     */
    private function getUser()
    {
        $token = $this->tokenStorage->getToken();
        if (!$token) {
            return null;
        }

        return $token->getUser();
    }
}