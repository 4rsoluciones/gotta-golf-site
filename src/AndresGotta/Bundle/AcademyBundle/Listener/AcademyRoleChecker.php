<?php

namespace AndresGotta\Bundle\AcademyBundle\Listener;

use AndresGotta\Academy\Application\AcademyContext;
use AndresGotta\Academy\Domain\Entity\AcademyInterface;
use AndresGotta\Academy\Domain\ValueObject\Sport;
use AndresGotta\Bundle\AcademyBundle\Service\ServiceChecker;
use GuzzleHttp\Exception\GuzzleException;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\RedirectResponse;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpKernel\Event\GetResponseEvent;
use Symfony\Component\HttpKernel\HttpKernelInterface;
use Symfony\Component\Routing\Generator\UrlGeneratorInterface;
use Symfony\Component\Security\Core\Authentication\Token\Storage\TokenStorageInterface;
use Symfony\Component\Security\Core\Exception\AccessDeniedException;
use WebFactory\Bundle\UserBundle\Entity\User;
use WebFactory\Bundle\UserBundle\Model\UserManager;

/**
 * Class AcademyContextCapturer
 * @package AndresGotta\Bundle\AcademyBundle\Listener
 */
class AcademyRoleChecker
{
    /**
     * @var ServiceChecker
     */
    private $checker;
    /**
     * @var TokenStorageInterface
     */
    private $tokenStorage;
    /**
     * @var UrlGeneratorInterface
     */
    private $generator;

    /**
     * AcademyContextCapturer constructor.
     * @param ServiceChecker $checker
     * @param TokenStorageInterface $tokenStorage
     * @param UrlGeneratorInterface $generator
     */
    public function __construct(ServiceChecker $checker, TokenStorageInterface $tokenStorage, UrlGeneratorInterface $generator)
    {
        $this->checker = $checker;
        $this->tokenStorage = $tokenStorage;
        $this->generator = $generator;
    }

    /**
     * @param GetResponseEvent $event
     */
    public function onRequest(GetResponseEvent $event)
    {
        if (!$event->isMasterRequest()) {
            return;
        }
        $isAcademyBackendUrl = preg_match('/^\/sportsintouch/', $event->getRequest()->getPathInfo());
        $isAcademyBackendLoginUrl = preg_match('/^\/sportsintouch\/(login|login\_check)$/', $event->getRequest()->getPathInfo());

        if (!$isAcademyBackendUrl || $isAcademyBackendLoginUrl) {
            return;
        }

        try {
            $serviceId = $this->checker->getFirstAcademyService($this->getUser(), $this->getUser()->getAccessToken());
        } catch (GuzzleException $e) {
            $serviceId = null;
        }
        if (!$serviceId || !$this->getUser()->hasRole('ROLE_ACADEMY')) {
            $event->setResponse(RedirectResponse::create($this->generator->generate('upgrade')));
        }
    }

    /**
     * @return mixed|null
     */
    private function getUser()
    {
        $token = $this->tokenStorage->getToken();
        if (!$token) {
            return null;
        }

        return $token->getUser();
    }
}