<?php

namespace AndresGotta\Bundle\AcademyBundle\Listener;

use AndresGotta\Academy\Application\AcademyContext;
use AndresGotta\Academy\Domain\Entity\AcademyInterface;
use AndresGotta\Bundle\AcademyBundle\Service\ServiceChecker;
use GuzzleHttp\Exception\GuzzleException;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpKernel\Event\GetResponseEvent;
use Symfony\Component\Security\Core\Authentication\Token\Storage\TokenStorageInterface;
use WebFactory\Bundle\UserBundle\Entity\User;
use WebFactory\Bundle\UserBundle\Model\UserManager;

/**
 * Class AcademyContextCapturer
 * @package AndresGotta\Bundle\AcademyBundle\Listener
 */
class AcademyContextCapturer
{
    /**
     *
     */
    const ACADEMY = 'ACADEMY';

    /**
     * @var UserManager
     */
    private $userManager;
    /**
     * @var ServiceChecker
     */
    private $checker;

    /**
     * @var TokenStorageInterface
     */
    private $storage;

    /**
     * AcademyContextCapturer constructor.
     * @param UserManager $userManager
     * @param ServiceChecker $checker
     */
    public function __construct(UserManager $userManager, ServiceChecker $checker, TokenStorageInterface $storage)
    {
        $this->userManager = $userManager;
        $this->checker = $checker;
        $this->storage = $storage;
    }

    /**
     * @param GetResponseEvent $event
     */
    public function onRequest(GetResponseEvent $event)
    {
        if (!$event->isMasterRequest()) {
            return;
        }

        if (!preg_match('/^\/api\/(teacher|student)/', $event->getRequest()->getPathInfo())) {
            return;
        }

        $academyId = $event->getRequest()->headers->get(self::ACADEMY);

        /* @var AcademyInterface|User $academy */
        $academy = $this->userManager->findUserBy(['id' => $academyId]);
        if (!$academy) {
            $response = JsonResponse::create(['message' => 'Invalid request. Academy not found.'], 400);
            $event->setResponse($response);

            return;
        }

        $user = $this->getUser();
        try {
            $serviceId = $this->checker->getFirstAcademyService($academy, $user->getAccessToken());
        } catch (GuzzleException $e) {
            $serviceId = null;
        }

//        if (!$serviceId) {
//            $response = JsonResponse::create(['message' => 'Academy service unavailable.'], 402);
//            $event->setResponse($response);
//
//            return;
//        }

        $context = new AcademyContext($academy, $academy->getSport());

        $event->getRequest()->attributes->set('academy_context', $context);
    }

    /**
     * @return mixed|null
     */
    private function getUser()
    {
        $token = $this->storage->getToken();
        if (!$token) {
            return null;
        }

        return $token->getUser();
    }
}