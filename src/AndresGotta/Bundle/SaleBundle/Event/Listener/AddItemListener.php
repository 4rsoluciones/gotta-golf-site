<?php

namespace AndresGotta\Bundle\SaleBundle\Event\Listener;

use AndresGotta\Bundle\SaleBundle\Manager\SaleManager;
use Symfony\Component\Security\Core\SecurityContextInterface;
use Symfony\Component\Security\Core\User\UserInterface;
use WebFactory\Bundle\CartBundle\Event\AddItemEvent;

class AddItemListener
{

    /**
     *
     * @var SaleManager
     */
    private $saleManager;

    /**
     *
     * @var SecurityContextInterface
     */
    private $securityContext;

    /**
     * 
     * @param SaleManager $saleManager
     * @param SecurityContextInterface $securityContext
     */
    public function __construct(SaleManager $saleManager, SecurityContextInterface $securityContext)
    {
        $this->saleManager = $saleManager;
        $this->securityContext = $securityContext;
    }

    /**
     * 
     * @return UserInterface
     */
    private function getUser()
    {
        $token = $this->securityContext->getToken();

        if (!$token) {
            return null;
        }

        return $token->getUser();
    }

    /**
     * 
     * @param AddItemEvent $event
     * @return type
     */
    public function add(AddItemEvent $event)
    {
        $user = $this->getUser();
        $product = $event->getProduct();
        if ($this->saleManager->hasCurrentService($user, $product)) {
            $event->setErrorMessage('Current product');
            $event->stopPropagation();
        }
    }

}
