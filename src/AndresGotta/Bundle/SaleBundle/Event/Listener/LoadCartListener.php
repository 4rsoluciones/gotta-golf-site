<?php

namespace AndresGotta\Bundle\SaleBundle\Event\Listener;

use AndresGotta\Bundle\SaleBundle\Entity\CustomItem;
use Doctrine\ORM\EntityManagerInterface;
use WebFactory\Bundle\CartBundle\Event\CartEvent;

class LoadCartListener
{

    /**
     *
     * @var EntityManagerInterface
     */
    private $entityManager;
    
    public function __construct(EntityManagerInterface $entityManager)
    {
        $this->entityManager = $entityManager;
    }

        public function load(CartEvent $event)
    {
        $cart = $event->getCart();

        foreach ($cart->getItems() as $item) {
            $product = $item->getProduct();
            if ($product) {
                $customItem = new CustomItem($item, $product->getType(), $product->getPrice(), $product->getDays(), $product->getTrialDays());
                $this->entityManager->persist($customItem);
            }
        }
    }

}
