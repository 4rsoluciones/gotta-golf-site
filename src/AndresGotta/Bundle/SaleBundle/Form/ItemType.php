<?php

namespace AndresGotta\Bundle\SaleBundle\Form;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\Form\FormEvent;
use Symfony\Component\Form\FormEvents;
use Symfony\Component\OptionsResolver\OptionsResolver;

/**
 * Class ItemType
 * @package AndresGotta\Bundle\SaleBundle\Form
 */
class ItemType extends AbstractType
{
    /**
     * @param FormBuilderInterface $builder
     * @param array $options
     */
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('item', 'entity', [
                'class' => 'WebFactory\Bundle\OrderBundle\Entity\Item',
                'property' => 'description'
            ])
            ->add('type')
            ->add('price', 'text', ['disabled' => true])
            ->add('days', 'number', ['disabled' => true])
            ->add('extends', 'date', [
                'datepicker' => true,
                'widget' => 'single_text',
                'attr' => ['class' => 'form-text form-date-input'],
                'format' => 'dd/MM/yyyy',
            ])
        ;

        $builder->addEventListener(FormEvents::POST_SUBMIT, function(FormEvent $event) {
            $data = $event->getData();
            $order = $data->getItem()->getOrder();

            /** @var \DateTime $fromDate */
            $fromDate = $order->getCreatedAt();
            $newDate = $data->getExtends();

            $data->setDays($fromDate->diff($newDate)->days);
        });

    }

    /**
     * @param OptionsResolver $resolver
     */
    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults([
            'data_class' => 'AndresGotta\Bundle\SaleBundle\Entity\CustomItem',
            'attr' => ['novalidate' => true]
        ]);
    }

    /**
     * @return string
     */
    public function getName()
    {
        return 'item_type';
    }

}