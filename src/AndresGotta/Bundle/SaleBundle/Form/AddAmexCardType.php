<?php

namespace AndresGotta\Bundle\SaleBundle\Form;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;

class AddAmexCardType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->setMethod('GET')
            ->add('card_number', 'text');
    }

    public function getName()
    {
        return 'add_amex_card_type';
    }
}
