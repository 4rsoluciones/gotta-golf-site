<?php

namespace AndresGotta\Bundle\SaleBundle\Form;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;

class AddMastercardCardType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->setMethod('GET')
            ->add('card_number', 'text');
    }

    public function getName()
    {
        return 'add_mastercard_card_type';
    }
}
