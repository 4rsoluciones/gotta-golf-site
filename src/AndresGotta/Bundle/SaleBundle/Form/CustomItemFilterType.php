<?php

namespace AndresGotta\Bundle\SaleBundle\Form;

use AndresGotta\Bundle\ServiceBundle\Model\ServiceTypes;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolverInterface;

class CustomItemFilterType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder->add('type', 'filter_choice', array(
            'required' => false,
            'choices' => ServiceTypes::getChoices(),
        ));
    }

    public function getName()
    {
        return 'custom_item_filter';
    }

    public function setDefaultOptions(OptionsResolverInterface $resolver)
    {
        $resolver->setDefaults(array(
            'csrf_protection'   => false,
            'validation_groups' => array('filtering')
        ));
    }
}