<?php

namespace AndresGotta\Bundle\SaleBundle\Form;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolverInterface;

class CustomerCustomPriceType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('customPrices', 'collection', array(
                    'error_bubbling' => false,
                    'type' => new CustomPriceType(),
                    'allow_add' => true,
                    'allow_delete' => true,
                    'prototype' => true,
                    'by_reference' => false,
                    'widget_add_btn' => array(
                        'label' => '',
                        'icon' => 'plus-sign icon-white',
                        'attr' => array(
                            'class' => 'btn btn-primary'
                        )
                    ),
                    'options' => array(
                        'widget_remove_btn' => array(
                            'label' => '',
                            'attr' => array('class' => 'btn btn-danger'),
                            'icon' => 'remove icon-white',
                        ),
                        'attr' => array('class' => 'span3'),
                        'widget_addon' => array(
                            'type' => 'prepend',
                            'text' => '@',
                        ),
                        'widget_control_group' => false,
                        'label' => false,
                        'error_bubbling' => false,
                    )
                ))
        ;
    }

    public function setDefaultOptions(OptionsResolverInterface $resolver)
    {
        $resolver->setDefaults(array(
            'data_class' => 'WebFactory\Bundle\UserBundle\Entity\User',
            'cascade_validation' => true,
        ));
    }

    public function getName()
    {
        return 'andresgotta_bundle_salebundle_user';
    }
}
