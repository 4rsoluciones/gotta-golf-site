<?php

namespace AndresGotta\Bundle\SaleBundle\Controller\Frontend;

use AndresGotta\Bundle\SaleBundle\Event\Events;
use AndresGotta\Bundle\SaleBundle\Event\NotifyExpirationEvent;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;

/**
 * @Route("/expiration")
 */
class ExpirationController extends Controller
{

    /**
     * @Route("/notify/{days}/days", requirements={"days" = "[-]?\d+"})
     * @Method({"GET"})
     */
    public function notifyAction(Request $request, $days)
    {
        $forExpireItems = $this->get('andres_gotta_sale.sale_manager')->findAllForExpireServices($days);
        $emailData = $this->groupItemsByUser($forExpireItems);

        foreach ($emailData as $data) {
            $event = new NotifyExpirationEvent($data['user'], $data['items'], $days);
            $this->get('event_dispatcher')->dispatch(Events::NOTIFY_EXPIRATION, $event);
        }

        return new Response('Envios: ' . count($emailData));
    }

    /**
     * 
     * @param array $forExpireItems
     */
    private function groupItemsByUser($forExpireItems)
    {
        $emailData = array();
        foreach ($forExpireItems as $customItem) {
            $user = $customItem->getItem()->getOrder()->getUser();
            if (!isset($emailData[$user->getId()])) {
                $emailData[$user->getId()] = array('user' => $user, 'items' => array());
            }

            $emailData[$user->getId()]['items'][] = $customItem;
        }
        
        return $emailData;
    }

}
