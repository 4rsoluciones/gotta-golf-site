<?php

namespace AndresGotta\Bundle\SaleBundle\Controller\Frontend;

use AndresGotta\Bundle\SaleBundle\Entity\CustomItem;
use AndresGotta\Bundle\SaleBundle\Form\CustomItemFilterType;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Template;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;

/**
* @Route("/sale")
*/
class SaleController extends Controller
{
    /**
     * @Route("/", name="frontend_sale")
     * @Method("GET|POST")
     * @Template
     */
    public function indexAction(Request $request)
    {
        $customItemRepository = $this->getDoctrine()->getManager()
            ->getRepository('AndresGottaSaleBundle:CustomItem');
        $filterBuilder = $customItemRepository->findPaidCustomItemsByUser($this->getUser(), true);
        $filterForm = $this->createForm(new CustomItemFilterType);

        if ($request->query->has('submit-filter')) {
            $filterForm->submit($request);
            $this->get('lexik_form_filter.query_builder_updater')->addFilterConditions($filterForm, $filterBuilder);
        }

        $pagination = $this->get('knp_paginator')->paginate(
            $filterBuilder, $request->query->get('page', 1), 10
        );

        return array(
            'filter_form' => $filterForm->createView(),
            'pagination' => $pagination,
        );
    }

    /**
     * @param Request $request
     * @param CustomItem $customItem
     * @return \Symfony\Component\HttpFoundation\RedirectResponse
     *
     * @Route("/{id}/cancel", name="frontend_sale_cancel")
     * @Method("GET|POST")
     */
    public function cancelAction(Request $request, CustomItem $customItem)
    {
        $this->get('web_factory_order.order.manager')->cancelOrder($customItem->getItem()->getOrder());

        return $this->redirectToRoute('frontend_sale');
    }
    
}
