<?php

namespace AndresGotta\Bundle\SaleBundle\Controller\Backend;

use AndresGotta\Bundle\SaleBundle\Form\CustomerCustomPriceType;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Template;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;
use WebFactory\Bundle\UserBundle\Entity\User;

/**
* @Route("/custom")
*/
class CustomPriceController extends Controller
{
    /**
     * Permite editar un CustomPrice
     * 
     * @Route("/{id}/edit", name="backend_custom_price_edit")
     * @Method({"GET","POST"})
     * @Template()
     */
    public function editAction(User $entity)
    {
        $editForm = $this->createForm(new CustomerCustomPriceType(), $entity);

        return array(
            'entity' => $entity,
            'edit_form' => $editForm->createView()
        );
    }
    
    
    /**
     * Permite editar un CustomPrice
     *
     * @Route("/{id}", name="backend_custom_price_update")
     * @Method("PUT")
     * @Template("AndresGottaSaleBundle:Backend/CustomPrice:edit.html.twig")
     */
    public function updateAction(Request $request, User $entity)
    {
        $em = $this->getDoctrine()->getManager();
        $editForm = $this->createForm(new CustomerCustomPriceType(), $entity);
        
        $editForm->bind($request);
        if ($editForm->isValid()) {
            $em->persist($entity);
            $em->flush();

            $this->get('session')->getFlashBag()->add('success', $this->get('translator')->trans('The customPrice was updated.'));

            return $this->redirect($this->generateUrl('backend_user'));
        }

        return array(
            'entity' => $entity,
            'edit_form' => $editForm->createView()
        );
    }
}
