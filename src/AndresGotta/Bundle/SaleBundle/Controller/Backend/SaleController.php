<?php

namespace AndresGotta\Bundle\SaleBundle\Controller\Backend;

use AndresGotta\Bundle\SaleBundle\Entity\CustomItem;
use AndresGotta\Bundle\SaleBundle\Form\ItemType;
use APY\DataGridBundle\Grid\Action\RowAction;
use APY\DataGridBundle\Grid\Column\BlankColumn;
use APY\DataGridBundle\Grid\Row;
use APY\DataGridBundle\Grid\Source\Entity;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Template;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;

/**
* @Route("/sale")
*/
class SaleController extends Controller
{
    /**
     * @Route("/", name="backend_sale")
     * @Method("GET|POST")
     * @Template
     */
    public function indexAction()
    {
        $saleManager = $this->get('andres_gotta_sale.sale_manager');
        $expiredDays = new BlankColumn(array('id' => 'days_to_expire', 'title' => 'Days to expire'));
        $trialDays = new BlankColumn(array('id' => 'trial_days', 'title' => 'Trial days'));
        
        /* @var $source \APY\DataGridBundle\Grid\Source\Entity */
        $source = new Entity('AndresGottaSaleBundle:CustomItem');
        
        /* @var $grid \APY\DataGridBundle\Grid\Grid */
        $grid = $this->get('grid');
        $grid->setSource($source);
        $grid->setId('main');
        $grid->setHiddenColumns(array('id'));
        $grid->setDefaultOrder('item.order.createdAt', 'desc');
        $grid->addColumn($trialDays, 4);
        $grid->getColumn('trial_days')->manipulateRenderCell(
            function($value, $row, $router) {
                $customItem = $row->getEntity();
                if ($days = $customItem->getTrialDays()) {
                    return $days.' días';
                } else {
                    return 'Ninguno';
                }
            }
        );
        $grid->addColumn($expiredDays, 5);
        $grid->getColumn('days_to_expire')->manipulateRenderCell(
            function($value, $row, $router) use ($saleManager) {
                $customItem = $row->getEntity();
                if ($saleManager->isCurrentService($customItem) && $customItem->getDays() > 0) {
                    return $saleManager->getElapsedDays($customItem);
                }

                return 'unpaid';
            }
        );

        $editAction = new RowAction('Edit', 'backend_sale_edit', false, '_self', ['class' => 'btn-action glyphicons edit btn-danger']);
        $grid->addRowAction($editAction);

        return $grid->getGridResponse('AndresGottaSaleBundle:Backend/Sale:index.html.twig');
    }

    /**
     * @Route("/{id}/edit", name="backend_sale_edit")
     * @Template()
     */
    public function editAction(Request $request, CustomItem $item)
    {
        $form = $this->createForm(new ItemType(), $item, [
            'action' => $this->generateUrl('backend_sale_edit', ['id' => $item->getId()]),
            'method' => 'PUT'
        ]);
        $form->add('Guardar', 'submit');

        if ($request->getMethod() === Request::METHOD_PUT) {
            $form->handleRequest($request);
            if ($form->isValid()) {
                $this->getDoctrine()->getManager()->persist($item);
                $this->getDoctrine()->getManager()->flush();

                $this->addFlash('success', $this->get('translator')->trans('flash.message.generic.updated'));

                return $this->redirectToRoute('backend_sale');
            } else {
                $this->addFlash('danger', 'Error en los datos enviados.');
            }
        }

        return ['form' => $form->createView(), 'item' => $item];
    }
}
