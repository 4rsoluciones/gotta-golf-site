<?php

namespace AndresGotta\Bundle\SaleBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use Gedmo\Mapping\Annotation as Gedmo;

/**
 * AmexRegistry
 *
 * @ORM\Table(name="amex_registries")
 * @ORM\Entity(repositoryClass="AndresGotta\Bundle\SaleBundle\Repository\AmexRegistryRepository")
 */
class AmexRegistry
{
    /**
     * @var integer
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @var bigint
     *
     * @ORM\Column(name="card_number", type="bigint")
     */
    private $cardNumber;

    /**
     * @var float
     *
     * @ORM\Column(name="amount", type="float")
     */
    private $amount;

    /**
     * @var integer
     *
     * @ORM\Column(name="date", type="integer")
     */
    private $date;

    /**
     * @var integer
     *
     * @ORM\Column(name="voucher_number", type="integer")
     */
    private $voucherNumber;

    /**
     * @var integer
     *
     * @ORM\Column(name="debit_dues", type="integer")
     */
    private $debitDues;

    /**
     * @var integer
     *
     * @ORM\Column(name="trial_days", type="integer")
     */
    private $trialDays;

    /**
     * @var date
     *
     * @ORM\Column(name="created_at", type="date")
     * @Gedmo\Timestampable(on="create")
     */
    private $createdAt;

    /**
     * Get id
     *
     * @return integer
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set cardNumber
     *
     * @param integer $cardNumber
     *
     * @return AmexRegistry
     */
    public function setCardNumber($cardNumber)
    {
        $this->cardNumber = $cardNumber;

        return $this;
    }

    /**
     * Get cardNumber
     *
     * @return integer
     */
    public function getCardNumber()
    {
        return $this->cardNumber;
    }

    /**
     * Set amount
     *
     * @param float $amount
     *
     * @return AmexRegistry
     */
    public function setAmount($amount)
    {
        $this->amount = $amount;

        return $this;
    }

    /**
     * Get amount
     *
     * @return float
     */
    public function getAmount()
    {
        return $this->amount;
    }

    /**
     * Set date
     *
     * @param integer $date
     *
     * @return AmexRegistry
     */
    public function setDate($date)
    {
        $this->date = $date;

        return $this;
    }

    /**
     * Get date
     *
     * @return integer
     */
    public function getDate()
    {
        return $this->date;
    }

    /**
     * Set voucherNumber
     *
     * @param integer $voucherNumber
     *
     * @return AmexRegistry
     */
    public function setVoucherNumber($voucherNumber)
    {
        $this->voucherNumber = $voucherNumber;

        return $this;
    }

    /**
     * Get voucherNumber
     *
     * @return integer
     */
    public function getVoucherNumber()
    {
        return $this->voucherNumber;
    }

    /**
     * Set debitDues
     *
     * @param integer $debitDues
     *
     * @return AmexRegistry
     */
    public function setDebitDues($debitDues)
    {
        $this->debitDues = $debitDues;

        return $this;
    }

    /**
     * Get debitDues
     *
     * @return integer
     */
    public function getDebitDues()
    {
        return $this->debitDues;
    }

    /**
     * Set trialDays
     *
     * @param integer $trialDays
     *
     * @return AmexRegistry
     */
    public function setTrialDays($trialDays)
    {
        $this->trialDays = $trialDays;

        return $this;
    }

    /**
     * Get trialDays
     *
     * @return integer
     */
    public function getTrialDays()
    {
        return $this->trialDays;
    }

    /**
     * Set createdAt
     *
     * @param date $createdAt
     *
     * @return AmexRegistry
     */
    public function setCreatedAt($createdAt)
    {
        $this->createdAt = $createdAt;

        return $this;
    }

    /**
     * Get createdAt
     *
     * @return date
     */
    public function getCreatedAt()
    {
        return $this->createdAt;
    }
}
