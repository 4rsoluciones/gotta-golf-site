<?php

namespace AndresGotta\Bundle\SaleBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use Symfony\Bridge\Doctrine\Validator\Constraints\UniqueEntity;
use Symfony\Component\Validator\Constraints as Assert;
use WebFactory\Bundle\UserBundle\Entity\User;
use AndresGotta\Bundle\SaleBundle\Entity\AmexCard;
use AndresGotta\Bundle\SaleBundle\Entity\MastercardCard;

/**
 * DebitCardProfile
 *
 * @ORM\Table(name="debit_card_profiles")
 * @ORM\Entity(repositoryClass="AndresGotta\Bundle\SaleBundle\Repository\DebitCardProfileRepository")
 * @UniqueEntity({"user"})
 */
class DebitCardProfile
{
    /**
     * @var integer
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @var \WebFactory\Bundle\UserBundle\Entity\User
     *
     * @ORM\OneToOne(targetEntity="\WebFactory\Bundle\UserBundle\Entity\User", inversedBy="debitCardProfile")
     * @ORM\JoinColumn(name="user_id", referencedColumnName="id")
     * @Assert\NotBlank
     */
    private $user;

    /**
     * @var \AndresGotta\Bundle\SaleBundle\Entity\AmexCard
     * 
     * @ORM\OneToMany(targetEntity="AndresGotta\Bundle\SaleBundle\Entity\AmexCard", mappedBy="debitCardProfile", cascade={"all"})
     * @Assert\NotBlank
     */
    private $amexCards;

    /**
     * @var \AndresGotta\Bundle\SaleBundle\Entity\MastercardCard
     * 
     * @ORM\OneToMany(targetEntity="AndresGotta\Bundle\SaleBundle\Entity\MastercardCard", mappedBy="debitCardProfile", cascade={"all"})
     * @Assert\NotBlank
     */
    private $mastercardCards;
    
    /**
     * Constructor
     */
    public function __construct()
    {
        $this->amexCards = new \Doctrine\Common\Collections\ArrayCollection();
        $this->mastercardCards = new \Doctrine\Common\Collections\ArrayCollection();
    }

    /**
     * Get id
     *
     * @return integer
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set user
     *
     * @param \WebFactory\Bundle\UserBundle\Entity\User $user
     *
     * @return DebitCardProfile
     */
    public function setUser(\WebFactory\Bundle\UserBundle\Entity\User $user = null)
    {
        $this->user = $user;

        return $this;
    }

    /**
     * Get user
     *
     * @return \WebFactory\Bundle\UserBundle\Entity\User
     */
    public function getUser()
    {
        return $this->user;
    }

    /**
     * Add amexCard
     *
     * @param \AndresGotta\Bundle\SaleBundle\Entity\AmexCard $amexCard
     *
     * @return DebitCardProfile
     */
    public function addAmexCard(\AndresGotta\Bundle\SaleBundle\Entity\AmexCard $amexCard)
    {
        $this->amexCards[] = $amexCard;

        return $this;
    }

    /**
     * Remove amexCard
     *
     * @param \AndresGotta\Bundle\SaleBundle\Entity\AmexCard $amexCard
     */
    public function removeAmexCard(\AndresGotta\Bundle\SaleBundle\Entity\AmexCard $amexCard)
    {
        $this->amexCards->removeElement($amexCard);
    }

    /**
     * Get amexCards
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getAmexCards()
    {
        return $this->amexCards;
    }

    /**
     * Add mastercardCard
     *
     * @param \AndresGotta\Bundle\SaleBundle\Entity\MastercardCard $mastercardCard
     *
     * @return DebitCardProfile
     */
    public function addMastercardCard(\AndresGotta\Bundle\SaleBundle\Entity\MastercardCard $mastercardCard)
    {
        $this->mastercardCards[] = $mastercardCard;

        return $this;
    }

    /**
     * Remove mastercardCard
     *
     * @param \AndresGotta\Bundle\SaleBundle\Entity\MastercardCard $mastercardCard
     */
    public function removeMastercardCard(\AndresGotta\Bundle\SaleBundle\Entity\MastercardCard $mastercardCard)
    {
        $this->mastercardCards->removeElement($mastercardCard);
    }

    /**
     * Get mastercardCards
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getMastercardCards()
    {
        return $this->mastercardCards;
    }
}
