<?php

namespace AndresGotta\Bundle\SaleBundle\Entity;

use APY\DataGridBundle\Grid\Mapping as Grid;
use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Validator\Constraints as Assert;
use WebFactory\Bundle\OrderBundle\Entity\Item;

/**
 * CustomItem
 *
 * @ORM\Table(name="custom_items")
 * @ORM\Entity(repositoryClass="AndresGotta\Bundle\SaleBundle\Repository\CustomItemRepository")
 * @GRID\Source(columns="id, item.order.createdAt, days, type, item.description, item.order.user.username")
 */
class CustomItem
{

    /**
     * @var integer
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     * @Grid\Column(filterable=false)
     */
    private $id;

    /**
     * @var Item
     * 
     * @ORM\OneToOne(targetEntity="WebFactory\Bundle\OrderBundle\Entity\Item")
     * @ORM\JoinColumn(name="item_id", referencedColumnName="id")
     * @Grid\Column(title="Description", field="item.description")
     * @Grid\Column(title="User", field="item.order.user.username")
     * @Grid\Column(title="Created At", field="item.order.createdAt", type="datetime")
     */
    private $item;

    /**
     * @var string
     * 
     * @ORM\Column(type="string", length=255)
     * @Assert\NotBlank
     * @Assert\Choice(choices = {"academy", "stats", "other"}, message = "Choose a valid service type.")
     * @Grid\Column(title="Type")
     */
    protected $type;

    /**
     * @var float
     * 
     * @ORM\Column(name="price", type="decimal")
     */
    protected $price;

    /**
     * @var integer
     *
     * @ORM\Column(name="days", type="integer")
     * @Assert\NotBlank
     * @Grid\Column(title="Validity", filterable=false)
     */
    protected $days;

    /**
     * @var integer
     *
     * @ORM\Column(name="trial_days", type="integer")
     * @Assert\NotBlank
     * @Grid\Column(title="Trial days", filterable=false)
     */
    protected $trialDays;

    /**
     * @var \DateTime
     */
    protected $extends;

    /**
     * CustomItem constructor.
     * @param Item|null $item
     * @param null $type
     * @param null $price
     * @param null $days
     * @param int|null $trialDays
     */
    public function __construct(Item $item = null, $type = null, $price = null, $days = null, $trialDays = 0)
    {
        $this->item = $item;
        $this->type = $type;
        $this->price = $price;
        $this->days = $days;
        $this->trialDays = $trialDays;
    }

    /**
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @return null|Item
     */
    public function getItem()
    {
        return $this->item;
    }

    /**
     * @return null|string
     */
    public function getType()
    {
        return $this->type;
    }

    /**
     * @return float|null
     */
    public function getPrice()
    {
        return $this->price;
    }

    /**
     * @return int|null
     */
    public function getDays()
    {
        return $this->days;
    }

    /**
     * @return int|null
     */
    public function getTrialDays()
    {
        return $this->trialDays;
    }

    /**
     * @param Item $item
     * @return $this
     */
    public function setItem(Item $item)
    {
        $this->item = $item;
        return $this;
    }

    /**
     * @param $type
     * @return $this
     */
    public function setType($type)
    {
        $this->type = $type;
        return $this;
    }

    /**
     * @param $price
     * @return $this
     */
    public function setPrice($price)
    {
        $this->price = $price;
        return $this;
    }

    /**
     * @param $days
     * @return $this
     */
    public function setDays($days)
    {
        $this->days = $days;

        return $this;
    }

    /**
     * @param $trialDays
     * @return $this
     */
    public function setTrialDays($trialDays)
    {
        $this->trialDays = $trialDays;

        return $this;
    }

    /**
     * @return \DateTime
     */
    public function getExtends()
    {
        return $this->extends;
    }

    /**
     * @param \DateTime $extends
     */
    public function setExtends($extends)
    {
        $this->extends = $extends;
    }

}
