<?php

namespace AndresGotta\Bundle\SaleBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use Symfony\Bridge\Doctrine\Validator\Constraints\UniqueEntity;
use Symfony\Component\Validator\Constraints as Assert;
use AndresGotta\Bundle\SaleBundle\Entity\DebitCardProfile;

/**
 * AmexCard
 *
 * @ORM\Table(name="mastercard_cards")
 * @ORM\Entity(repositoryClass="AndresGotta\Bundle\SaleBundle\Repository\MastercardCardRepository")
 */
class MastercardCard
{
    /**
     * @var integer
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @var \AndresGotta\Bundle\SaleBundle\Entity\DebitCardProfile
     *
     * @ORM\ManyToOne(targetEntity="AndresGotta\Bundle\SaleBundle\Entity\DebitCardProfile", inversedBy="mastercardCards")
     * @ORM\JoinColumn(name="debit_card_profile_id", referencedColumnName="id")
     * @Assert\NotBlank
     */
    private $debitCardProfile;

    /**
     * @var bigint
     *
     * @ORM\Column(name="card_number", type="bigint")
     */
    protected $cardNumber;


    /**
     * Get id
     *
     * @return integer
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set cardNumber
     *
     * @param integer $cardNumber
     *
     * @return MastercardCard
     */
    public function setCardNumber($cardNumber)
    {
        $this->cardNumber = $cardNumber;

        return $this;
    }

    /**
     * Get cardNumber
     *
     * @return integer
     */
    public function getCardNumber()
    {
        return $this->cardNumber;
    }

    /**
     * Set debitCardProfile
     *
     * @param \AndresGotta\Bundle\SaleBundle\Entity\DebitCardProfile $debitCardProfile
     *
     * @return MastercardCard
     */
    public function setDebitCardProfile(\AndresGotta\Bundle\SaleBundle\Entity\DebitCardProfile $debitCardProfile = null)
    {
        $this->debitCardProfile = $debitCardProfile;

        return $this;
    }

    /**
     * Get debitCardProfile
     *
     * @return \AndresGotta\Bundle\SaleBundle\Entity\DebitCardProfile
     */
    public function getDebitCardProfile()
    {
        return $this->debitCardProfile;
    }
}
