<?php

namespace AndresGotta\Bundle\SaleBundle\Entity;

use AndresGotta\Bundle\ServiceBundle\Entity\Service;
use Doctrine\ORM\Mapping as ORM;
use Symfony\Bridge\Doctrine\Validator\Constraints\UniqueEntity;
use Symfony\Component\Validator\Constraints as Assert;
use WebFactory\Bundle\UserBundle\Entity\User;

/**
 * CustomPrice
 *
 * @ORM\Table(name="custom_prices")
 * @ORM\Entity
 * @UniqueEntity({"user", "service"})
 */
class CustomPrice
{
    /**
     * @var integer
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @var integer
     *
     * @ORM\Column(name="percentage", type="integer")
     * @Assert\NotBlank
     */
    private $percentage;

    /**
     * @var \WebFactory\Bundle\UserBundle\Entity\User
     *
     * @ORM\ManyToOne(targetEntity="\WebFactory\Bundle\UserBundle\Entity\User", inversedBy="customPrices")
     * @ORM\JoinColumn(name="user_id", referencedColumnName="id")
     * @Assert\NotBlank
     */
    private $user;

    /**
     * @var \AndresGotta\Bundle\ServiceBundle\Entity\Service
     * 
     * @ORM\ManyToOne(targetEntity="\AndresGotta\Bundle\ServiceBundle\Entity\Service", inversedBy="customPrices")
     * @ORM\JoinColumn(name="service_id", referencedColumnName="id")
     * @Assert\NotBlank
     */
    private $service;


    /**
     * @return string
     */
    public function __toString()
    {
        return $this->getService() . ' ' . $this->getPercentage();
    }
    
    /**
     * Get id
     *
     * @return integer 
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set percentage
     *
     * @param string $percentage
     * @return CustomPrice
     */
    public function setPercentage($percentage)
    {
        $this->percentage = $percentage;

        return $this;
    }

    /**
     * Get percentage
     *
     * @return string 
     */
    public function getPercentage()
    {
        return $this->percentage;
    }

    /**
     * Set user
     *
     * @param User $user
     * @return CustomPrice
     */
    public function setUser(User $user = null)
    {
        $this->user = $user;

        return $this;
    }

    /**
     * Get user
     *
     * @return User 
     */
    public function getUser()
    {
        return $this->user;
    }

    /**
     * Set service
     *
     * @param Service $service
     * @return CustomPrice
     */
    public function setService(Service $service)
    {
        $this->service = $service;

        return $this;
    }

    /**
     * Get service
     *
     * @return Service
     */
    public function getService()
    {
        return $this->service;
    }
}
