<?php

namespace AndresGotta\Bundle\SaleBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use Gedmo\Mapping\Annotation as Gedmo;

/**
 * MastercardRegistry
 *
 * @ORM\Table(name="mastercard_registries")
 * @ORM\Entity(repositoryClass="AndresGotta\Bundle\SaleBundle\Repository\MastercardRegistryRepository")
 */
class MastercardRegistry
{
    /**
     * @var integer
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @var bigint
     *
     * @ORM\Column(name="card_number", type="bigint")
     */
    private $cardNumber;

    /**
     * @var bigint
     *
     * @ORM\Column(name="reference_number", type="bigint")
     */
    private $referenceNumber;

    /**
     * @var float
     *
     * @ORM\Column(name="amount", type="float")
     */
    private $amount;

    /**
     * @var integer
     *
     * @ORM\Column(name="expiration_date", type="integer")
     */
    private $expirationDate;

    /**
     * @var integer
     *
     * @ORM\Column(name="debit_dues", type="integer")
     */
    private $debitDues;

    /**
     * @var integer
     *
     * @ORM\Column(name="trial_days", type="integer")
     */
    private $trialDays;

    /**
     * @var date
     *
     * @ORM\Column(name="created_at", type="date")
     * @Gedmo\Timestampable(on="create")
     */
    private $createdAt;

    /**
     * Get id
     *
     * @return integer
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set cardNumber
     *
     * @param integer $cardNumber
     *
     * @return MastercardRegistry
     */
    public function setCardNumber($cardNumber)
    {
        $this->cardNumber = $cardNumber;

        return $this;
    }

    /**
     * Get cardNumber
     *
     * @return integer
     */
    public function getCardNumber()
    {
        return $this->cardNumber;
    }

    /**
     * Set referenceNumber
     *
     * @param integer $referenceNumber
     *
     * @return MastercardRegistry
     */
    public function setReferenceNumber($referenceNumber)
    {
        $this->referenceNumber = $referenceNumber;

        return $this;
    }

    /**
     * Get referenceNumber
     *
     * @return integer
     */
    public function getReferenceNumber()
    {
        return $this->referenceNumber;
    }

    /**
     * Set amount
     *
     * @param float $amount
     *
     * @return MastercardRegistry
     */
    public function setAmount($amount)
    {
        $this->amount = $amount;

        return $this;
    }

    /**
     * Get amount
     *
     * @return float
     */
    public function getAmount()
    {
        return $this->amount;
    }

    /**
     * Set expirationDate
     *
     * @param float $expirationDate
     *
     * @return MastercardRegistry
     */
    public function setExpirationDate($expirationDate)
    {
        $this->expirationDate = $expirationDate;

        return $this;
    }

    /**
     * Get expirationDate
     *
     * @return float
     */
    public function getExpirationDate()
    {
        return $this->expirationDate;
    }

    /**
     * Set debitDues
     *
     * @param integer $debitDues
     *
     * @return MastercardRegistry
     */
    public function setDebitDues($debitDues)
    {
        $this->debitDues = $debitDues;

        return $this;
    }

    /**
     * Get debitDues
     *
     * @return integer
     */
    public function getDebitDues()
    {
        return $this->debitDues;
    }

    /**
     * Set trialDays
     *
     * @param integer $trialDays
     *
     * @return MastercardRegistry
     */
    public function setTrialDays($trialDays)
    {
        $this->trialDays = $trialDays;

        return $this;
    }

    /**
     * Get trialDays
     *
     * @return integer
     */
    public function getTrialDays()
    {
        return $this->trialDays;
    }

    /**
     * Set createdAt
     *
     * @param date $createdAt
     *
     * @return MastercardRegistry
     */
    public function setCreatedAt($createdAt)
    {
        $this->createdAt = $createdAt;

        return $this;
    }

    /**
     * Get createdAt
     *
     * @return date
     */
    public function getCreatedAt()
    {
        return $this->createdAt;
    }
}
