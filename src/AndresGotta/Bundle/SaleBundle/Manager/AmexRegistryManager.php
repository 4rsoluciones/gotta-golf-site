<?php

namespace AndresGotta\Bundle\SaleBundle\Manager;

use Doctrine\ORM\EntityManagerInterface;
use AndresGotta\Bundle\SaleBundle\Entity\AmexRegistry;
use Symfony\Component\Filesystem\Filesystem;
use WebFactory\Bundle\OrderBundle\Model\OrderStatuses;
use DateTime;

class AmexRegistryManager
{
    private $entityManager;
    private $establishmentNumber;

    public function __construct(EntityManagerInterface $entityManager, $establishmentNumber)
    {
        $this->entityManager = $entityManager;
        $this->establishmentNumber = $establishmentNumber;
    }

    public function newRegistry($cardNumber, $amount, $voucherNumber, $debitDues = 0, $trialDays = 0)
    {
        $date = date('Ymd');
        $this->saveRegistry($cardNumber, $amount, $date, $voucherNumber, $debitDues, $trialDays);

        return null;
    }

    public function dropRegistry($voucherNumber)
    {
        $registry = $this->entityManager->getRepository('AndresGottaSaleBundle:AmexRegistry')->getRegistryByVoucherNumber($voucherNumber);
        $this->entityManager->remove($registry);
        $this->entityManager->flush();

        return null;
    }

    public function dumpRegistries()
    {
        $fs = new Filesystem();
        $registries = $this->entityManager->getRepository('AndresGottaSaleBundle:AmexRegistry')->getAllRegistriesForDump();

        $header = $this->generateHeader($registries).PHP_EOL;
        $file = $header;
        foreach ($registries as $registry) {
            $file .= $this->generateRegistry($registry).PHP_EOL;
        }

        //$this->clearRegistries();

        return $file;
    }

    public function processReturn($return)
    {
        $registries = explode(PHP_EOL, $return);
        $registriesArray = array();
        $orderRepository = $this->entityManager->getRepository('WebFactoryOrderBundle:Order');
        foreach ($registries as $registry) {
            $registryArray['provider'] = 'amex';
            $registryArray['registryType'] = substr($registry, 0, 1);
            if ($registryArray['registryType'] == 3) {
                $registryArray['establishmentNumber'] = substr($registry, 1, 10);
                $registryArray['acceptedAmount'][0] = (int)substr($registry, 11, 11);
                $registryArray['acceptedAmount'][1] = substr($registry, 22, 2);
                $registryArray['acceptedCharges'] = (int)substr($registry, 25, 6);
                $registryArray['date'][0] = substr($registry, 34, 4);
                $registryArray['date'][1] = substr($registry, 38, 2);
                $registryArray['date'][2] = substr($registry, 40, 2);
            }
            elseif ($registryArray['registryType'] == 2) {
                $registryArray['cardNumber'] = substr($registry, 1, 15);
                $registryArray['amount'][0] = (int)substr($registry, 16, 9);
                $registryArray['amount'][1] = substr($registry, 25, 2);
                $registryArray['date'][0] = substr($registry, 34, 4);
                $registryArray['date'][1] = substr($registry, 38, 2);
                $registryArray['date'][2] = substr($registry, 40, 2);
                $registryArray['rejectMark'] = substr($registry, 55, 2);
                $voucherNumber = (int)substr($registry, 42, 8);
                $order = $orderRepository->getOrderById($voucherNumber);
                $userProfile = $order->getUser()->getProfile();
                $registryArray['order'] = $order;
                $registryArray['name'] = $userProfile->getFirstName() . ' ' . $userProfile->getLastName();
                $registryArray['email'] = $order->getUser()->getEmail();;
            }
            array_push($registriesArray, $registryArray);
        }

        return $registriesArray;
    }

    public function checkForDebitDues()
    {
        $registries = $this->entityManager->getRepository('AndresGottaSaleBundle:AmexRegistry')->getAllRegistriesForDump();

        foreach ($registries as $registry) {
            if ($registry->getDebitDues()) {
                $createdAt = $registry->getCreatedAt();
                $now = new DateTime;
                $diff = date_diff($now, $createdAt);
                if ($diff->format('%m')) {
                    $debitDues = $registry->getDebitDues()-1;
                    if ($debitDues) {
                        $registry->setDebitDues($debitDues);
                        $registry->setDate(date('Ymd'));
                    } else {
                        $this->entityManager->remove($registry);
                    }
                    $this->entityManager->flush();
                }
            }
        }
    }

    public function checkForTrialDays()
    {
        $registries = $this->entityManager->getRepository('AndresGottaSaleBundle:AmexRegistry')->getAllRegistriesInTrial();

        foreach ($registries as $registry) {
            $trialDays = $registry->getTrialDays();
            $registry->setTrialDays($trialDays-1);
            $this->entityManager->flush();
        }
    }

    private function saveRegistry($cardNumber, $amount, $date, $voucherNumber, $debitDues, $trialDays)
    {
        $registry = new AmexRegistry();
        $registry->setCardNumber($cardNumber);
        $registry->setAmount($amount);
        $registry->setDate($date);
        $registry->setVoucherNumber($voucherNumber);
        $registry->setDebitDues($debitDues);
        $registry->setTrialDays($trialDays);
        $this->entityManager->persist($registry);
        $this->entityManager->flush();

        return null;
    }

    private function clearRegistries() {
        $registries = $this->entityManager->getRepository('AndresGottaSaleBundle:AmexRegistry')->getAllRegistries();
        foreach ($registries as $registry) {
             $this->entityManager->remove($registry);
        }
        $this->entityManager->flush();

        return null;
    }

    private function generateHeader($registries)
    {
        $registryType = '1';
        $establishmentNumber = $this->fixEstablishmentNumber($this->establishmentNumber);
        $totalAmount = 0;
        foreach ($registries as $registry) {
            $totalAmount += $registry->getAmount();
        }
        $totalAmount = $this->fixHeaderAmount($totalAmount);
        $type = '0';
        $transactions = $this->fixTransactions(count($registries));
        $currency = '000';
        $date = date('Ymd');
        $hour = date('His');
        $reservation = '                                          ';

        return $registryType.$establishmentNumber.$totalAmount.$type.$transactions.$currency.$date.$hour.$reservation;
    }

    private function generateRegistry($registry) {
        $registryType = '2';
        $cardNumber = $this->fixCardNumber($registry->getCardNumber());
        $amount = $this->fixAmount($registry->getAmount());
        $type = '0';
        $currency = '000';
        $plan = '0';
        $dues = '00';
        $date = $this->fixDate($registry->getDate());
        $voucherNumber = $this->fixVoucherNumber($registry->getVoucherNumber());
        $authCode = '00000';
        $rejectMark = '  ';
        $oldPartnerNumber = '000000000000000';
        $clientNumber = '0000000000';
        $reservation = '        ';

        return $registryType.$cardNumber.$amount.$type.$currency.$plan.$dues.$date.$voucherNumber.$authCode.$rejectMark.$oldPartnerNumber.$clientNumber.$reservation;
    }

    private function fixEstablishmentNumber($establishmentNumber)
    {
        return substr(sprintf('%010d', $establishmentNumber), 0, 10);
    }

    private function fixHeaderAmount($amount)
    {
        if (strpos($amount, '.')) {
            $explodeAmount = explode('.', $amount);
            $amount = substr(sprintf('%011d', $explodeAmount[0]), 0, 11).substr(sprintf('%02d', $explodeAmount[1]), 0, 2);
        }
        else {
            $amount = substr(sprintf('%011d', $amount), 0, 11).'00';
        }

        return $amount;
    }

    private function fixTransactions($transactions)
    {
        return substr(sprintf('%06d', $transactions), 0, 6);
    }

    private function fixCardNumber($cardNumber)
    {
        return substr(sprintf('%015d', $cardNumber), 0, 15);
    }

    private function fixAmount($amount)
    {
        if (strpos($amount, '.')) {
            $explodeAmount = explode('.', $amount);
            $amount = substr(sprintf('%09d', $explodeAmount[0]), 0, 9).substr(sprintf('%02d', $explodeAmount[1]), 0, 2);
        }
        else {
            $amount = substr(sprintf('%09d', $amount), 0, 9).'00';
        }

        return $amount;
    }

    private function fixType($type)
    {
        return substr($type, 0, 1);
    }

    private function fixCurrency($currency)
    {
        return sprintf('%03d', $currency);
    }

    private function fixDues($dues)
    {
        return substr(sprintf('%02d', $dues), 0, 2);
    }

    private function fixDate($date)
    {
        return substr(sprintf('%08d', $date), 0, 8);
    }

    private function fixVoucherNumber($voucherNumber)
    {
        return substr(sprintf('%08d', $voucherNumber), 0, 8);
    }

    private function fixClientNumber($clientNumber)
    {
        return substr(sprintf('%010d', $clientNumber), 0, 10);
    }
}
