<?php

namespace AndresGotta\Bundle\SaleBundle\Manager;

use Doctrine\ORM\EntityManagerInterface;
use AndresGotta\Bundle\SaleBundle\Entity\MastercardRegistry;
use Symfony\Component\Filesystem\Filesystem;

class MastercardRegistryManager
{
    private $entityManager;
    private $commerceNumber;

    public function __construct(EntityManagerInterface $entityManager, $commerceNumber)
    {
        $this->entityManager = $entityManager;
        $this->commerceNumber = $commerceNumber;
    }

    public function newRegistry($cardNumber, $amount, $referenceNumber, $debitDues = 0, $trialDays = 0)
    {
        $expirationDate = date('ymd');
        $this->saveRegistry($cardNumber, $referenceNumber, $amount, $expirationDate, $debitDues, $trialDays);
        
        return null;
    }

    public function dropRegistry($referenceNumber)
    {
        $registry = $this->entityManager->getRepository('AndresGottaSaleBundle:MastercardRegistry')->getRegistryByReferenceNumber($referenceNumber);
        $this->entityManager->remove($registry);
        $this->entityManager->flush();
        
        return null;
    }

    public function dumpRegistries()
    {
        $fs = new Filesystem();
        $registries = $this->entityManager->getRepository('AndresGottaSaleBundle:MastercardRegistry')->getAllRegistriesForDump();

        $header = $this->generateHeader($registries).PHP_EOL;
        $file = $header;
        foreach ($registries as $registry) {
            $file .= $this->generateRegistry($registry).PHP_EOL;
        }

        //$this->clearRegistries();

        return $file;
    }

    public function processReturn($return)
    {
        $registries = explode(PHP_EOL, $return);
        $registriesArray = array();
        $orderRepository = $this->entityManager->getRepository('WebFactoryOrderBundle:Order');
        foreach ($registries as $registry) {
            $registryArray['provider'] = 'mastercard';
            $registryArray['registryType'] = substr($registry, 2, 1);
            if ($registryArray['registryType'] == 1) {
                $registryArray['commerceNumber'] = substr($registry, 12, 8);
                $registryArray['date'][0] = substr($registry, 20, 2);
                $registryArray['date'][1] = substr($registry, 22, 2);
                $registryArray['date'][2] = substr($registry, 24, 2);
                $registryArray['acceptedCharges'] = (int)substr($registry, 32, 6);
                $registryArray['acceptedAmount'][0] = (int)substr($registry, 39, 10);
                $registryArray['acceptedAmount'][1] = substr($registry, 49, 2);
            }
            elseif ($registryArray['registryType'] == 2) {
                $registryArray['cardNumber'] = substr($registry, 3, 16);
                $registryArray['referenceNumber'] = (int)substr($registry, 26, 12);
                $registryArray['amount'][0] = (int)substr($registry, 40, 9);
                $registryArray['amount'][1] = substr($registry, 49, 2);
                $registryArray['rejectMark'] = substr($registry, 58, 1);
                $registryArray['date'][0] = substr($registry, 65, 2);
                $registryArray['date'][1] = substr($registry, 67, 2);
                $registryArray['date'][2] = substr($registry, 69, 2);
                $order = $orderRepository->getOrderById($registryArray['referenceNumber']);
                $userProfile = $order->getUser()->getProfile();
                $registryArray['order'] = $order;
                $registryArray['name'] = $userProfile->getFirstName() . ' ' . $userProfile->getLastName();
                $registryArray['email'] = $order->getUser()->getEmail();;
            }
            array_push($registriesArray, $registryArray);
        }

        return $registriesArray;
    }

    public function checkForDebitDues()
    {
        $registries = $this->entityManager->getRepository('AndresGottaSaleBundle:MastercardRegistry')->getAllRegistriesForDump();

        foreach ($registries as $registry) {
            if ($registry->getDebitDues()) {
                $createdAt = $registry->getCreatedAt();
                $now = new DateTime;
                $diff = date_diff($now, $createdAt);
                if ($diff->format('%m')) {
                    $debitDues = $registry->getDebitDues()-1;
                    if ($debitDues) {
                        $registry->setDebitDues($debitDues);
                        $registry->setDate(date('Ymd'));
                    } else {
                        $this->entityManager->remove($registry);
                    }
                    $this->entityManager->flush();
                }
            }
        }
    }

    public function checkForTrialDays()
    {
        $registries = $this->entityManager->getRepository('AndresGottaSaleBundle:MastercardRegistry')->getAllRegistriesInTrial();

        foreach ($registries as $registry) {
            $trialDays = $registry->getTrialDays();
            $registry->setTrialDays($trialDays-1);
            $this->entityManager->flush();
        }
    }

    private function saveRegistry($cardNumber, $referenceNumber, $amount, $expirationDate, $debitDues, $trialDays)
    {
        $registry = new MastercardRegistry();
        $registry->setCardNumber($cardNumber);
        $registry->setReferenceNumber($referenceNumber);
        $registry->setAmount($amount);
        $registry->setExpirationDate($expirationDate);
        $registry->setDebitDues($debitDues);
        $registry->setTrialDays($trialDays);
        $this->entityManager->persist($registry);
        $this->entityManager->flush();

        return null;
    }

    private function clearRegistries() {
        $registries = $this->entityManager->getRepository('AndresGottaSaleBundle:MastercardRegistry')->getAllRegistries();
        foreach ($registries as $registry) {
             $this->entityManager->remove($registry);
        }
        $this->entityManager->flush();

        return null;
    }

    private function generateHeader($registries)
    {
        $commerceNumber = $this->fixCommerceNumber($this->commerceNumber);
        $registryType = '1';
        $date = date('dmy');
        $transactions = $this->fixTransactions(count($registries));
        $amountSign = '0';
        $totalAmount = 0;
        foreach ($registries as $registry) {
            $totalAmount += $registry->getAmount();
        }
        $totalAmount = $this->fixHeaderAmount($totalAmount);
        $filler = '                                                                                           ';
        
        return $commerceNumber.$registryType.$date.$transactions.$amountSign.$totalAmount.$filler;
    }

    public function generateRegistry($registry) {
        $commerceNumber = $this->fixCommerceNumber($this->commerceNumber);
        $registryType = '2';
        $cardNumber = $this->fixCardNumber($registry->getCardNumber());
        $referenceNumber = $this->fixReferenceNumber($registry->getReferenceNumber());
        $dues = '000';
        $planDues = '999';
        $debitFrecuency = '01';
        $amount = $this->fixAmount($registry->getAmount());
        $period = 'CRED ';
        $fillerA = ' ';
        $expirationDate = $this->fixExpirationDate($registry->getExpirationDate());
        $auxData = '                                        ';
        $fillerB = '                    ';
        return $commerceNumber.$registryType.$cardNumber.$referenceNumber.$dues.$planDues.$debitFrecuency.$amount.$period.$fillerA.$expirationDate.$auxData.$fillerB;
    }

    private function fixCommerceNumber($commerceNumber)
    {
        return substr(sprintf('%08d', $commerceNumber), 0, 8);
    }

    private function fixTransactions($transactions)
    {
        return substr(sprintf('%07d', $transactions), 0, 7);
    }

    private function fixHeaderAmount($amount)
    {
        if (strpos($amount, '.')) {
            $explodeAmount = explode('.', $amount);
            $amount = substr(sprintf('%012d', $explodeAmount[0]), 0, 12).substr(sprintf('%02d', $explodeAmount[1]), 0, 2);
        }
        else {
            $amount = substr(sprintf('%012d', $amount), 0, 12).'00';
        }

        return $amount; 
    }

    private function fixCardNumber($cardNumber)
    {
        return substr(sprintf('%016d', $cardNumber), 0, 16);
    }

    private function fixReferenceNumber($referenceNumber)
    {
        return substr(sprintf('%012d', $referenceNumber), 0, 12);
    }

    private function fixDues($dues)
    {
        return substr(sprintf('%03d', $dues), 0, 3);
    }

    private function fixAmount($amount)
    {
        if (strpos($amount, '.')) {
            $explodeAmount = explode('.', $amount);
            $amount = substr(sprintf('%09d', $explodeAmount[0]), 0, 9).substr(sprintf('%02d', $explodeAmount[1]), 0, 2);
        }
        else {
            $amount = substr(sprintf('%09d', $amount), 0, 9).'00';
        }
        
        return $amount; 
    }

    private function fixExpirationDate($expirationDate)
    {
        return substr(sprintf('%06d', $expirationDate), 0, 6);
    }
}
