<?php

namespace AndresGotta\Bundle\SaleBundle\Manager;

use AndresGotta\Bundle\MailBundle\Manager\MailManager;
use AndresGotta\Bundle\SaleBundle\Entity\CustomItem;
use DateTime;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Component\Security\Core\User\UserInterface;
use WebFactory\Bundle\OrderBundle\Manager\OrderManager;
use WebFactory\Bundle\OrderBundle\Model\OrderStatuses;
use WebFactory\Bundle\PaymentBundle\Model\PaymentStatuses;

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

class SaleManager
{

    /**
     *
     * @var EntityManagerInterface
     */
    private $entityManager;

    private $orderManager;

    private $mailManager;

    public function __construct(EntityManagerInterface $entityManager, OrderManager $orderManager, MailManager $mailManager)
    {
        $this->entityManager = $entityManager;
        $this->orderManager = $orderManager;
        $this->mailManager = $mailManager;
    }

    public function hasActivePackageOfTypes(UserInterface $user, $serviceType)
    {
        $qb = $this->entityManager->getRepository('AndresGottaSaleBundle:CustomItem')->createQueryBuilder('CustomItem');

        $qb->innerJoin('CustomItem.item', 'Item');
        $qb->innerJoin('Item.order', 'Orde');

        $qb->andWhere('Orde.user = :user')->setParameter('user', $user);
        $qb->andWhere('Orde.status = :status')->setParameter('status', OrderStatuses::PAID);
        $qb->andWhere('CustomItem.type = :type')->setParameter('type', $serviceType);

        $customItems = $qb->getQuery()->getResult();
        foreach ($customItems as $customItem) {
            $order = $customItem->getItem()->getOrder();
            $payment = $this->getPaymentForOrder($order);
            if ($payment) {
                $days = $customItem->getDays();
                $now = new DateTime;
                $paymentPlusDays = $payment['createdAt']->modify("+ {$days} day");
                $interval = $now->diff($paymentPlusDays);

                if ($interval->days >= 0) {
                    return true;
                }
            }
        }

        return false;
    }

    /**
     *
     * @param Order $order
     * @return Payment
     */
    private function getPaymentForOrder($order)
    {
        $qb = $this->entityManager->getRepository('WebFactoryPaymentBundle:Payment')->createQueryBuilder('Payment');
        $qb->andWhere('Payment.order = :order')->setParameter('order', $order);
        $qb->andWhere('Payment.status = :status')->setParameter('status', PaymentStatuses::PAID);
        $qb->setMaxResults(1);

        $payment = $qb->getQuery()->getArrayResult();

        return count($payment) ? current($payment) : null;
    }

    public function hasCurrentService(UserInterface $user, \AndresGotta\Bundle\ServiceBundle\Entity\Service $service)
    {
        $qb = $this->entityManager->getRepository('AndresGottaSaleBundle:CustomItem')->createQueryBuilder('CustomItem');

        $qb->innerJoin('CustomItem.item', 'Item');
        $qb->innerJoin('Item.order', 'Orde');

        $qb->andWhere('Orde.user = :user')->setParameter('user', $user);
        $qb->andWhere('Orde.status = :status')->setParameter('status', OrderStatuses::PAID);

        $customItems = $qb->getQuery()->getResult();
        foreach ($customItems as $customItem) {
            $order = $customItem->getItem()->getOrder();
            $payment = $this->getPaymentForOrder($order);
            if ($payment) {
                $days = $customItem->getDays();
                $now = new DateTime;
                $paymentPlusDays = $payment['createdAt']->modify("+ {$days} day");
                $interval = $now->diff($paymentPlusDays);

                if ($interval->days >= 0 && $service->isEquals($customItem->getItem()->getProduct())) {
                    return true;
                }
            }
        }

        return false;
    }

    /**
     *
     * Busca los "servicios" que esten por vencer exactamente en $days (del $user)
     *
     * @param integer $days
     * @param \Symfony\Component\Security\Core\User\UserInterface $user
     *
     * @return array<CustomItem>
     */
    public function findAllForExpireServices($days = 0, UserInterface $user = null)
    {
        $qb = $this->entityManager->getRepository('AndresGottaSaleBundle:CustomItem')->createQueryBuilder('CustomItem');

        $qb->innerJoin('CustomItem.item', 'Item');
        $qb->innerJoin('Item.order', 'Orde');
        $qb->andWhere('Orde.status = :status')->setParameter('status', OrderStatuses::PAID);

        if ($user) {
            $qb->andWhere('Orde.user = :user')->setParameter('user', $user);
        }

        $result = array();

        $customItems = $qb->getQuery()->getResult();
        foreach ($customItems as $customItem) {
            $order = $customItem->getItem()->getOrder();
            $payment = $this->getPaymentForOrder($order);
            if ($payment) {
                $itemDays = $customItem->getDays();
                if ($itemDays) {
                    $itemTrialDays = $customItem->getTrialDays();
                    $now = new DateTime;
                    $paymentPlusDays = $payment['createdAt']->modify("+ {$itemDays} day")->modify("+ {$itemTrialDays} day");
                    $interval = $now->diff($paymentPlusDays);

                    if ($interval->days == $days) {
                        $result[] = $customItem;
                    }
                }
            }
        }

        return $result;
    }

    /**
     * Retorna true si el customItem esta sin vencer
     *
     * @param CustomItem $customItems
     * @return boolean
     */
    public function isCurrentService(CustomItem $customItem)
    {
        $order = $customItem->getItem()->getOrder();
        $payment = $this->getPaymentForOrder($order);
        if ($payment) {
            $itemDays = $customItem->getDays();
            $now = new DateTime;
            $paymentPlusDays = $payment['createdAt']->modify("+ {$itemDays} day");
            $interval = $now->diff($paymentPlusDays);

            if ($interval->days >= 0 ) {
                return true;
            }
        }

        return false;
    }

    /**
     * @param CustomItem $customItem
     * @return integer|string
     */
    public function getElapsedDays(CustomItem $customItem)
    {
        if ($this->isPaid($customItem)) {
            $order = $customItem->getItem()->getOrder();
            $payment = $this->getPaymentForOrder($order);
            $itemDays = $customItem->getDays();
            $itemTrialDays = $customItem->getTrialDays();
            $now = new DateTime;
            $paymentPlusDays = $payment['createdAt']->modify("+ {$itemDays} day")->modify("+ {$itemTrialDays} day");
            $interval = $now->diff($paymentPlusDays);

            return $interval->days;
        }

        return 'expired';
    }

    public function checkForExpiry()
    {
        $customItems = $this->findAllForExpireServices();

        foreach ($customItems as $customItem) {
            if ($this->isCurrentService($customItem)) {
                $order = $customItem->getItem()->getOrder();
                $this->orderManager->cancelOrder($order);
                $this->mailManager->notifyServiceExpired($order);
            }
        }
    }

    /**
     * @param CustomItem $customItem
     * @return bool
     */
    public function isPaid(CustomItem $customItem)
    {
        return $customItem->getItem()->getOrder()->getStatus() === OrderStatuses::PAID;
    }

}
