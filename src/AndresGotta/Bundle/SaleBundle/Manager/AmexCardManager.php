<?php

namespace AndresGotta\Bundle\SaleBundle\Manager;

use Doctrine\ORM\EntityManagerInterface;
use AndresGotta\Bundle\SaleBundle\Entity\AmexCard;

class AmexCardManager
{
    private $entityManager;

    public function __construct(EntityManagerInterface $entityManager)
    {
        $this->entityManager = $entityManager;
    }

    public function addCard(\WebFactory\Bundle\UserBundle\Entity\User $user, $cardNumber)
    {
        $card = $this->createCard($user->getDebitCardProfile(), $cardNumber);
        $user->addAmexCard($card);

        $this->entityManager->persist($user);
        $this->entityManager->flush();

        return $card;
    }

    private function createCard(\AndresGotta\Bundle\SaleBundle\Entity\DebitCardProfile $debitCardProfile, $cardNumber)
    {
        $card = new AmexCard;
        $card->setDebitCardProfile($debitCardProfile);
        $card->setCardNumber($cardNumber);

        return $card;
    }

    public function modifyCardById($id, $cardNumber)
    {
        $card = $this->entityManager->getRepository('AndresGottaSaleBundle:AmexCard')->getCardById($id);
        $card->setCardNumber($cardNumber);

        $this->entityManager->persist($card);
        $this->entityManager->flush();
    }
}
