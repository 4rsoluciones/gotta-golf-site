<?php

namespace AndresGotta\Bundle\SaleBundle\Command;

use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;
use Symfony\Bundle\FrameworkBundle\Command\ContainerAwareCommand;

class CheckTrialDaysCommand extends ContainerAwareCommand
{
    protected function configure()
    {
        $this->setName('gotta:trials:check');
        $this->setDescription('Checks for trial days registry.');
        $this->setHelp("Running this command will check and update trial days on both AMEX and MASTERCARD registries.");
    }

    protected function execute(InputInterface $input, OutputInterface $output)
    {
        $this->getContainer()->get('andres_gotta_sale.amex_registry_manager')->checkForTrialDays();
        $this->getContainer()->get('andres_gotta_sale.mastercard_registry_manager')->checkForTrialDays();
    }
}
