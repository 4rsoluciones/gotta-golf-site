<?php

namespace AndresGotta\Bundle\SaleBundle\Command;

use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;
use Symfony\Bundle\FrameworkBundle\Command\ContainerAwareCommand;

class CheckServiceExpiryCommand extends ContainerAwareCommand
{
    protected function configure()
    {
        $this->setName('gotta:service:check');
        $this->setDescription('Checks for the services expiration.');
        $this->setHelp("Running this command will check the services expiry date and suspend those who have expired.");
    }

    protected function execute(InputInterface $input, OutputInterface $output)
    {
        $this->getContainer()->get('andres_gotta_sale.sale_manager')->checkForExpiry();
    }
}
