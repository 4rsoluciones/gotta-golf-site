<?php

namespace AndresGotta\Bundle\SaleBundle\Repository;

use Doctrine\ORM\EntityRepository;

class MastercardRegistryRepository extends EntityRepository
{
    public function getAllRegistriesForDump()
    {
        $query = $this->getEntityManager()->createQuery('SELECT r FROM AndresGottaSaleBundle:MastercardRegistry r WHERE r.trialDays = 0');
        return $query->getResult();
    }

    public function getAllRegistriesInTrial()
    {
        $query = $this->getEntityManager()->createQuery('SELECT r FROM AndresGottaSaleBundle:MastercardRegistry r WHERE r.trialDays > 0');
        return $query->getResult();
    }

    public function getRegistryByReferenceNumber($referenceNumber)
    {
        $query = $this->getEntityManager()->createQuery('SELECT r FROM AndresGottaSaleBundle:MastercardRegistry r WHERE r.referenceNumber = :referenceNumber')->setParameter('referenceNumber', $referenceNumber);
        return $query->getOneOrNullResult();
    }
}
