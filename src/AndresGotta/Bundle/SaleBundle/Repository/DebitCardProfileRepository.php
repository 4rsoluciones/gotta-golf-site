<?php

namespace AndresGotta\Bundle\SaleBundle\Repository;

use Doctrine\ORM\EntityRepository;

class DebitCardProfileRepository extends EntityRepository
{
    public function getDebitCardProfileByUser(\WebFactory\Bundle\UserBundle\Entity\User $user)
    {
        $query = $this->getEntityManager()->createQuery('SELECT p FROM AndresGottaSaleBundle:DebitCardProfile p WHERE p.user = :user')->setParameter('user', $user);
        return $query->getOneOrNullResult();
    }
}
