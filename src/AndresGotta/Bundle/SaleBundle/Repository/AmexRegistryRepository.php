<?php

namespace AndresGotta\Bundle\SaleBundle\Repository;

use Doctrine\ORM\EntityRepository;

class AmexRegistryRepository extends EntityRepository
{
    public function getAllRegistriesForDump()
    {
        $query = $this->getEntityManager()->createQuery('SELECT r FROM AndresGottaSaleBundle:AmexRegistry r WHERE r.trialDays = 0');
        return $query->getResult();
    }

    public function getAllRegistriesInTrial()
    {
        $query = $this->getEntityManager()->createQuery('SELECT r FROM AndresGottaSaleBundle:AmexRegistry r WHERE r.trialDays > 0');
        return $query->getResult();
    }

    public function getRegistryByVoucherNumber($voucherNumber)
    {
        $query = $this->getEntityManager()->createQuery('SELECT r FROM AndresGottaSaleBundle:AmexRegistry r WHERE r.voucherNumber = :voucherNumber')->setParameter('voucherNumber', $voucherNumber);
        return $query->getOneOrNullResult();
    }
}
