<?php

namespace AndresGotta\Bundle\SaleBundle\Repository;

use Doctrine\ORM\EntityRepository;

class MastercardCardRepository extends EntityRepository
{
    public function getCardById($id)
    {
        $query = $this->getEntityManager()->createQuery('SELECT c FROM AndresGottaSaleBundle:MastercardCard c WHERE c.id = :id')->setParameter('id', $id);
        return $query->getOneOrNullResult();
    }
}
