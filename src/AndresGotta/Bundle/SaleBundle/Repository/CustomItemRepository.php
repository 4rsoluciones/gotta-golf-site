<?php

namespace AndresGotta\Bundle\SaleBundle\Repository;

use Doctrine\ORM\EntityRepository;
use FOS\UserBundle\Model\UserInterface;
use WebFactory\Bundle\OrderBundle\Model\OrderStatuses;

class CustomItemRepository extends EntityRepository
{
    /**
     * 
     * @param UserInterface $user
     * @param boolean $returnQueryBuilder
     * @return mixed
     */
    public function findPaidCustomItemsByUser(UserInterface $user = null, $returnQueryBuilder = false)
    {
        $queryBuilder = $this->createQueryBuilder('CustomItem')
                ->join('CustomItem.item', 'Item')
                ->join('Item.order', 'Ord')
                ->join('Ord.user', 'User')
                ->andWhere('Ord.status = :status')
                ->setParameter('status', OrderStatuses::PAID)
        ;
        
        if ($user) {
            $queryBuilder->andWhere('User = :user')
                        ->setParameter('user', $user);
        }
        
        if ($returnQueryBuilder) {
            return $queryBuilder;
        }

        return $queryBuilder->getQuery()->getResult();
    }
}
