<?php

namespace AndresGotta\Bundle\ServiceBundle\DataFixtures\ORM;

use AndresGotta\Bundle\SaleBundle\Entity\CustomItem;
use Doctrine\Common\DataFixtures\AbstractFixture;
use Doctrine\Common\DataFixtures\OrderedFixtureInterface;
use Doctrine\Common\Persistence\ObjectManager;
use Symfony\Component\DependencyInjection\ContainerAwareInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;
use WebFactory\Bundle\OrderBundle\Entity\Item;
use WebFactory\Bundle\OrderBundle\Entity\Order;
use WebFactory\Bundle\OrderBundle\Model\OrderStatuses;
use WebFactory\Bundle\PaymentBundle\Entity\Payment;
use WebFactory\Bundle\PaymentBundle\Model\PaymentStatuses;
use WebFactory\Bundle\ProductBundle\Model\ProductInterface;
use WebFactory\Bundle\UserBundle\Entity\User;

class LoadSales extends AbstractFixture implements OrderedFixtureInterface, ContainerAwareInterface
{
    
    /**
     * @var ContainerInterface
     */
    private $container;
    
    /**
     * {@inheritDoc}
     */
    public function setContainer(ContainerInterface $container = null)
    {
        $this->container = $container;
    }

    /**
     * {@inheritDoc}
     */
    public function getOrder()
    {
        return 5;
    }

    /**
     * {@inheritDoc}
     */
    public function load(ObjectManager $manager)
    {
        $validator = $this->container->get('validator');
        $user = $manager->getRepository('WebFactoryUserBundle:User')->findOneBy(array('username' => 'player'));
        
        $service = $manager->getRepository('AndresGottaServiceBundle:Service')->find(1);
        
//        foreach ($services as $service) {
            $items[] = $this->createItem($service, $manager);
//        }
        
        $payment = new Payment();
        $payment->setStatus(PaymentStatuses::PAID);
        $order = $this->buildOrder($user, $items, $payment);
                
        $errors = $validator->validate($payment);
        if ($errors->count() === 0) {
            $manager->persist($payment);
        } else {
            echo $errors;
        }
        
        $errors = $validator->validate($order);
        if ($errors->count() === 0) {
            $manager->persist($order);
        } else {
            echo $errors;
        }
        
        $manager->flush();
    }
    
    /**
     * Carga una entidad Order
     *
     * @param Order $order
     * @param User $user
     * @param Payment $payment
     * @param array $items
     * @return Order
     */
    private function buildOrder(User $user, $items, Payment $payment = null)
    {
        $order = new Order();
        $order->setUser($user);
        $order->setStatus(OrderStatuses::PAID);
        $order->setPayment($payment);
        $payment->setOrder($order);
        
        foreach ($items as $item) {
            $order->addItem($item);
        }
        
        return $order;
    }
    
    private function createItem(ProductInterface $product, $manager, $quantity = 1)
    {
        $productManager = $this->container->get('andres_gotta_service.product.manager');
        
        $item = new Item();
        $item->setProduct($product);
        $item->setDescription($product->getName() . ' - '. $product->getDescription());
        $item->setPrice($productManager->getPriceByProduct($product));
        $item->setQuantity($quantity);
        $customItem = new CustomItem($item, $product->getType(), $product->getPrice(), $product->getDays(), $product->getTrialDays());
        
        $manager->persist($customItem);

        return $item;
    }
}