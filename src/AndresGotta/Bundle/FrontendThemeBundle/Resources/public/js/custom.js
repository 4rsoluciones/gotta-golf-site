$(function () {
    $('div[id*="_emotionalQuestionnaire_control_group"]').addClass('emotionalQuestionnaire_control_group');
    $('div[id*="_foodQuestionnaire_control_group"]').addClass('foodQuestionnaire_control_group');
    $('div[id*="_physicalQuestionnaire_control_group"]').addClass('physicalQuestionnaire_control_group');

    $('div[id*="_reason_control_group"]').after('<div class="before-strike-title">Antes de ejecutar este golpe:</div>');

    //on/off
    $(".pre-competitive-questionnaire-line-1 .switch--horizontal input").click(function () {
        var currentidradio = ($(this).attr('id'));

        if (currentidradio == 'radio1-a') {
            $("#pre_competitive_questionnaire_type_isWarmedUp_0").trigger("click");
        } else {
            $("#pre_competitive_questionnaire_type_isWarmedUp_1").trigger("click");
        }
    });
    $(".pre-competitive-questionnaire-line-2 .switch--horizontal input").click(function () {
        var currentidradio = ($(this).attr('id'));

        if (currentidradio == 'radio2-a') {
            $("#pre_competitive_questionnaire_type_hasPracticed_0").trigger("click");
        } else {
            $("#pre_competitive_questionnaire_type_hasPracticed_1").trigger("click");
        }
    });
    $(".pre-competitive-questionnaire-line-3 .switch--horizontal input").click(function () {
        var currentidradio = ($(this).attr('id'));

        if (currentidradio == 'radio3-a') {
            $("#pre_competitive_questionnaire_type_wantsToPlay_0").trigger("click");
        } else {
            $("#pre_competitive_questionnaire_type_wantsToPlay_1").trigger("click");
        }
    });
    $(".pre-competitive-questionnaire-line-4 .switch--horizontal input").click(function () {
        var currentidradio = ($(this).attr('id'));

        if (currentidradio == 'radio4-a') {
            $("#pre_competitive_questionnaire_type_hasStrategy_0").trigger("click");
        } else {
            $("#pre_competitive_questionnaire_type_hasStrategy_1").trigger("click");
        }
    });

    //hole form
    $(".radio").click(function () {
        var nexiste = $(this).find('.imagen-in-radio').length;
        if ((nexiste) > 0) {
            var thecontrol = $(this).parent();
            thecontrol.find('.radio').removeClass("active");
            $(this).addClass("active");
        }
    });

    if ($(".radio-in-radio").is(':checked')) {
        var n2existe = $(".radio-in-radio:checked").next().length;
        if ((n2existe) > 0) {
            $(".radio-in-radio:checked").parent().addClass("active");
        }
    }

    $(".btn.btn-tooltips").popover({trigger: 'click'});

    //Switch 
    if (!($.cookie('cookieenter'))) {
        setTimeout(function () {
            $('.toogleClickArea').trigger("click");
        }, 3000);
    }

    $('a.toogleClickArea').toggle(function () {
        $('.span4.nav-left-area').animate({width: 328}, "slow");
        $('.span4.nav-left-area').removeClass('active');
    }, function () {
        $('.span4.nav-left-area').animate({width: 28}, "slow");
        $('.span4.nav-left-area').addClass('active');
        $.cookie("cookieenter", 1, {expires: 1});
    });

    $('#videocarousel').jcarousel({scroll: 1});

    $(window).scroll(function () {
        if ($(this).scrollTop() > 260) {
            $('.newsmenu').css('position', 'fixed');
            $('.newsmenu').css('margin', '0px');
            $('.newsmenu').css('top', '0');
        } else {
            $('.newsmenu').css('position', 'relative');
            $('.newsmenu').css('margin', '20px 0 0 -3px');
        }
    });

    //When page loads...
    $(".tab_content").hide(); //Hide all content
    $("ul.tabs_generic li:first").addClass("active").show(); //Activate first tab
    $(".tab_content:first").show(); //Show first tab content

    //On Click Event
    $("ul.tabs_generic li").click(function () {

        $("ul.tabs_generic li").removeClass("active"); //Remove any "active" class
        $(this).addClass("active"); //Add "active" class to selected tab
        $(".tab_content").hide(); //Hide all tab content

        var activeTab = $(this).find("a").attr("href"); //Find the href attribute value to identify the active tab + content
        $(activeTab).fadeIn(); //Fade in the active ID content
        return false;
    });

    $("#slides").slidesjs({
        navigation: {
            active: false,
            // [boolean] Generates next and previous buttons.
            // You can set to false and use your own buttons.
            // User defined buttons must have the following:
            // previous button: class="slidesjs-previous slidesjs-navigation"
            // next button: class="slidesjs-next slidesjs-navigation"
            effect: "slide"
            // [string] Can be either "slide" or "fade".
        },
        effect: {
            fade: {
                speed: 300,
                // [number] Speed in milliseconds of the fade animation.
                crossfade: true
                // [boolean] Cross-fade the transition.
            }
        },
        pagination: {
            active: true,
            // [boolean] Create pagination items.
            // You cannot use your own pagination. Sorry.
            effect: "slide"
            // [string] Can be either "slide" or "fade".
        },
        play: {
            active: true,
            // [boolean] Generate the play and stop buttons.
            // You cannot use your own buttons. Sorry.
            effect: "slide",
            // [string] Can be either "slide" or "fade".
            interval: 10000,
            // [number] Time spent on each slide in milliseconds.
            auto: true,
            // [boolean] Start playing the slideshow on load.
            swap: true,
            // [boolean] show/hide stop and play buttons
            pauseOnHover: false,
            // [boolean] pause a playing slideshow on hover
            restartDelay: 2500
            // [number] restart delay on inactive slideshow
        }
    });

    // Formulario de subscripciÃ³n por ajax
    var subscriptionFormSuccessHandler = function (responseText, statusText, xhr, $form) {
        $('#subscriptionFormContainer').html(responseText);
        $('#subscriptionFormContainer > form').ajaxForm({
            success: subscriptionFormSuccessHandler
        });
    };

    $('#subscriptionFormContainer > form').ajaxForm({
        success: subscriptionFormSuccessHandler
    });

    //RADIO BUTTOM
    $("#holeForm .controls .radio").click(function () {
        var thecontrol = $(this).parent();
        thecontrol.find('.radio').removeClass("active");
        $(this).addClass("active");
    });
    $("#holeForm .controls .radio input").each(function () {
        var ckbo2x = $(this);
        if (ckbo2x.is(':checked')) {
            $(this).parent().addClass("active");
        }
    });


    //CHECKBOX BUTTOMS
    $("#holeForm .controls .checkbox input").each(function () {
        $('input[type=checkbox]').parent().removeClass("active");
        $('input[type=checkbox]').parent().removeClass("deactive");
        $('input[type=checkbox]:checked').parent().addClass("active");
        $('input[type=checkbox]:disabled').parent().removeClass("active");
        $('input[type=checkbox]:disabled').parent().addClass("deactive");
        $('input[type=checkbox]:checked').parent().removeClass("deactive");
    });

    $("#holeForm .controls .checkbox input").click(function () {
        $("#holeForm .controls .checkbox input").each(function () {
            $('input[type=checkbox]').parent().removeClass("active");
            $('input[type=checkbox]').parent().removeClass("deactive");
            $('input[type=checkbox]:checked').parent().addClass("active");
            $('input[type=checkbox]:disabled').parent().removeClass("active");
            $('input[type=checkbox]:disabled').parent().addClass("deactive");
            $('input[type=checkbox]:checked').parent().removeClass("deactive");
        });
    });


    //EMOJIIIIIIIII
    $('.emotionalQuestionnaire_control_group .controls label.checkbox input[type=checkbox], .foodQuestionnaire_control_group .controls label.checkbox input[type=checkbox], .physicalQuestionnaire_control_group .controls label.checkbox input[type=checkbox]').each(function () {
        //calssessi
        var $select = $(this);
        var $div = $(this).parent().parent().parent();

        var attributes = $select.attr('class');
        $div.attr('class', 'control-group ' + attributes);

    });

    $(".emotionalQuestionnaire_control_group .controls label.checkbox, .foodQuestionnaire_control_group .controls label.checkbox , .physicalQuestionnaire_control_group .controls label.checkbox").click(function () {
        $(this).parent().parent().toggleClass("active");
    });

    $('.emotionalQuestionnaire_control_group .controls label.checkbox input[type=checkbox], .foodQuestionnaire_control_group .controls label.checkbox input[type=checkbox], .physicalQuestionnaire_control_group .controls label.checkbox input[type=checkbox]').each(function () {
        var ckbox = $(this);
        if (ckbox.is(':checked')) {
            $(this).parent().parent().parent().parent().parent().addClass("deactive");
            $(this).parent().parent().parent().addClass("active");
        }
    });

    var mylabelsbox = $(".foodQuestionnaire_control_group .controls .control-group .control-label, .physicalQuestionnaire_control_group .controls .control-group .control-label, .emotionalQuestionnaire_control_group .controls .control-group .control-label");
    $(mylabelsbox).each(function () {
        var namerio = $(this).html();
        $(this).parent().append("<div class='toolterio'>" + namerio + "</div>");
    });

    $(mylabelsbox).hover(
        function () {
            $(this).parent().find('.toolterio').show();
        }, function () {
            $(this).parent().find('.toolterio').hide();
        }
    );

    var hiderbox = $(".control-group.emotionalQuestionnaire_control_group, .foodQuestionnaire_control_group.control-group, .physicalQuestionnaire_control_group.control-group");
    $(hiderbox).hover(
        function () {
            $(this).addClass('reaactive');
            $(this).removeClass('deactive');
        }, function () {
            $(this).removeClass('reaactive');
            if ($(this).is(':checked')) {
                $(this).parent().parent().parent().parent().addClass("deactive");
            }
            $('.emotionalQuestionnaire_control_group .controls label.checkbox input[type=checkbox], .foodQuestionnaire_control_group .controls label.checkbox input[type=checkbox], .physicalQuestionnaire_control_group .controls label.checkbox input[type=checkbox]').each(function () {
                var ckbox = $(this);
                if (ckbox.is(':checked')) {
                    $(this).parent().parent().parent().parent().parent().addClass("deactive");
                    $(this).parent().parent().parent().addClass("active");
                }
            });
        }
    );


    //POST QUESTION EMOJI
    $('#post_competitive_questionnaire_type_feelingStatus .radio input').each(function () {
        //calssessi
        var $select = $(this);
        var $div = $(this).parent();
        var attributes = $select.attr('id');
        $div.attr('class', 'radio ' + attributes);
    });

    //RADIO BUTTOM
    $("#post_competitive_questionnaire_type_feelingStatus .radio").click(function () {
        var thecontrol = $(this).parent();
        thecontrol.find('input').parent().removeClass("active");
        $(this).addClass("active");
    });
    $("#post_competitive_questionnaire_type_feelingStatus .radio input").each(function () {
        var ckbo2x = $(this);
        if (ckbo2x.is(':checked')) {
            $(this).parent().addClass("active");
        }
    });

    //BTN CLEAN
    $(".btn.btn-cancel-generic.btn-clean-generic").on("click", function () {
        $(".craue_formflow_buttons .craue_formflow_button_first").trigger("click");
    });

});