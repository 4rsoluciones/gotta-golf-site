<?php

namespace AndresGotta\Bundle\FrontendThemeBundle\EventListener;

use Symfony\Component\HttpKernel\Event\FilterControllerEvent;

class LayoutResolver
{

    public function onKernelController(FilterControllerEvent $event)
    {
        $pathInfo = $event->getRequest()->getPathInfo();
        
        if(preg_match('/^\/admin/i', $pathInfo)) {
            $event->getRequest()->attributes->set('_layout', 'backend');
        } else {
            $event->getRequest()->attributes->set('_layout', 'frontend');
        }
        
    }

}