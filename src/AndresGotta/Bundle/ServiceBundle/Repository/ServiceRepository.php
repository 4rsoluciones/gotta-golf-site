<?php

namespace AndresGotta\Bundle\ServiceBundle\Repository;

use Doctrine\ORM\EntityRepository;
use Doctrine\ORM\Query\Expr\Join;
use WebFactory\Bundle\OrderBundle\Entity\Order;

/**
 * Class ServiceRepository
 * @package AndresGotta\Bundle\ServiceBundle\Repository
 */
class ServiceRepository extends EntityRepository
{

    /**
     * @param int $limit
     * @return array
     */
    public function findRandom($limit = 3)
    {
        $query = $this->createQueryBuilder('s')
                ->where('s.visible = true')
                ->addSelect('RAND() as HIDDEN rand')
                ->orderBy('rand')
                ->getQuery();

        return $query->setMaxResults($limit)->getResult();
    }

    /**
     * @param int $limit
     * @return array
     */
    public function findHighlighted($limit = 3)
    {
        $query = $this->createQueryBuilder('s')
                ->where('s.visible = true')
                ->andWhere('s.highlighted = true')
                ->getQuery();

        return $query->setMaxResults($limit)->getResult();
    }

    /**
     * @return array
     */
    public function getServiceSoldCountByArea()
    {
        $query = $this->createQueryBuilder('Service')
                ->select('Area.id, Area.name, COUNT(Item.id) as quantity')
                ->join('Service.area', 'Area')
                ->leftJoin('WebFactoryOrderBundle:Item', 'Item', Join::WITH, 'Item.product = Service')
                ->groupBy('Area.id')
                ->orderBy('Area.id', 'ASC')
                ->getQuery();
        
        return $query->getResult();
    }

    /**
     * @return array
     */
    public function getServiceSoldWithPaymentCountByArea()
    {
        $query = $this->createQueryBuilder('Service')
                ->select('Area.id, Area.name, COUNT(Payment.id) as quantity')
                ->join('Service.area', 'Area')
                ->leftJoin('WebFactoryOrderBundle:Item', 'Item', Join::WITH, 'Item.product = Service')
                ->leftJoin('Item.order', 'o')
                ->leftJoin('WebFactoryPaymentBundle:Payment', 'Payment', Join::WITH, 'Payment.order = o')
                ->where('Payment.status = :status')->setParameter('status', \WebFactory\Bundle\PaymentBundle\Model\PaymentStatuses::PAID)
                ->groupBy('Area.id')
                ->orderBy('Area.id', 'ASC')
                ->getQuery();
        
        return $query->getResult();
    }

    /**
     * @return mixed
     */
    public function getImportantServices()
    {
        $qb = $this->createQueryBuilder('s')
            ->select('COUNT(s.id)')
            ->where('s.important = true')
        ;

        return $qb->getQuery()->getSingleScalarResult() ;
    }

    public function getAllServices()
    {
        $query = $this->createQueryBuilder('s')->getQuery();
        
        return $query->getResult();
    }

    public function getByName($name)
    {
        $query = $this->createQueryBuilder('s')
            ->where('s.name = :name')
            ->setParameter('name', $name);

        return $query->getResult();
    }
}