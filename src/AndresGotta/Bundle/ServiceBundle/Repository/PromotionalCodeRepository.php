<?php

namespace AndresGotta\Bundle\ServiceBundle\Repository;

use Doctrine\ORM\EntityRepository;

/**
 * Class PromotionalCodeRepository
 * @package AndresGotta\Bundle\ServiceBundle\Repository
 */
class PromotionalCodeRepository extends EntityRepository
{
    /**
     * @return string
     */
    public function generateNewCode()
    {
        $characters = 'ABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789';
        do {
            $code = '';
            $max = strlen($characters) - 1;
            for ($i = 0; $i < 8; $i++) {
                $code .= $characters[mt_rand(0, $max)];
            }
        } while (count($this->findBy(['code' => $code])));

        return $code;
    }

    /**
     * @param $code
     * @return int
     */
    public function getBurnedCount($code)
    {
        $repo = $this->getEntityManager()->getRepository('WebFactoryOrderBundle:Order');

        return count($repo->findBy(['promotionalCode' => $code]));
    }
}
