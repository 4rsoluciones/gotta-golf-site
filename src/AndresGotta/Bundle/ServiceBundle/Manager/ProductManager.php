<?php

namespace AndresGotta\Bundle\ServiceBundle\Manager;

use Doctrine\ORM\EntityManager;
use Symfony\Component\Security\Core\SecurityContextInterface;
use Symfony\Component\Security\Core\User\UserInterface;
use WebFactory\Bundle\ProductBundle\Model\ProductInterface;
use WebFactory\Bundle\ProductBundle\Model\ProductManager as BaseManager;

class ProductManager extends BaseManager
{

    /**
     *
     * @var SecurityContextInterface
     */
    private $securityContext;

    public function __construct(EntityManager $entityManager, $entityName, SecurityContextInterface $securityContext)
    {
        parent::__construct($entityManager, $entityName);
        $this->securityContext = $securityContext;
    }

    private function getUser()
    {
        $token = $this->securityContext->getToken();

        if ($token) {
            return $token->getUser();
        }

        return null;
    }

    public function getPriceByProduct(ProductInterface $product)
    {
        $user = $this->getUser();
        $percent = 0;
        foreach ($product->getCustomPrices() as $customPrice) {
            if ($customPrice->getUser() === $user) {
                $percent = $customPrice->getPercentage();
                break;
            }
        }

        return $product->getPrice() * (1.0 + $percent / 100.0);
    }

}
