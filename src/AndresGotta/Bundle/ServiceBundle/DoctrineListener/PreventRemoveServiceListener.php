<?php

namespace AndresGotta\Bundle\ServiceBundle\DoctrineListener;

use Doctrine\ORM\Event\LifecycleEventArgs;
use AndresGotta\Bundle\ServiceBundle\Entity\Service;

class PreventRemoveServiceListener
{
    public function preRemove(\Doctrine\ORM\Event\LifecycleEventArgs $eventvArgs)
    {
        $em = $eventvArgs->getEntityManager();
        $entity = $eventvArgs->getEntity();

        if ($entity instanceof Service) {
            $items = $em->getRepository('WebFactoryOrderBundle:Item')->findBy(array('product'=>$entity));
            if (count($items) > 0) {
                throw new \Exception("You can not remove this service", 1);
            }
        }
    }
}