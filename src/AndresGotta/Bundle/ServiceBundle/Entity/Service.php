<?php

namespace AndresGotta\Bundle\ServiceBundle\Entity;

use AndresGotta\Bundle\SaleBundle\Entity\CustomPrice;
use APY\DataGridBundle\Grid\Mapping as Grid;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\ORM\Mapping as ORM;
use Gedmo\Mapping\Annotation as Gedmo;
use Symfony\Bridge\Doctrine\Validator\Constraints\UniqueEntity;
use Symfony\Component\Validator\Constraints as Assert;
use WebFactory\Bundle\ProductBundle\Model\EquatableInterface;
use WebFactory\Bundle\ProductBundle\Model\ProductInterface;
use AndresGotta\Bundle\ServiceBundle\Validator as Custom;

/**
 * Service
 *
 * @ORM\Table(name="services")
 * @ORM\Entity(repositoryClass="AndresGotta\Bundle\ServiceBundle\Repository\ServiceRepository")
 * @UniqueEntity("name")
 */
class Service implements ProductInterface, EquatableInterface
{

    /**
     * @var integer
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     * @Grid\Column(filterable=false);
     */
    protected $id;

    /**
     * @var string
     *
     * @ORM\Column(type="boolean")
     * @Grid\Column(title="Highlighted");
     */
    protected $highlighted = false;

    /**
     * @var string
     *
     * @ORM\Column(name="important", type="boolean")
     * @Grid\Column(title="Important")
     * @Custom\ConstraintServiceValidator
     */
    protected $important = false;

    /**
     * @var string
     *
     * @ORM\Column(name="name", type="string", length=255)
     * @Assert\NotBlank
     * @Grid\Column(title="Name");
     * @Assert\Length(
     *      min = "2",
     *      max = "30"
     * )
     */
    protected $name;

    /**
     * @var string
     *
     * @ORM\Column(name="description", type="text")
     * @Assert\NotBlank
     * @Grid\Column(title="Description");
     * @Assert\Length(
     *      min = "2",
     *      max = "100"
     * )
     */
    protected $description;
    
    /**
     * @var string
     *
     * @ORM\Column(name="long_description", type="text")
     * @Assert\NotBlank
     * @Grid\Column(title="Long Description");
     * @Assert\Length(
     *      min = "10"
     * )
     */
    protected $longDescription;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="created_at", type="datetime")
     * @Gedmo\Timestampable(on="create")
     * @Grid\Column(title="Created At");
     */
    protected $createdAt;

    /**
     * @var boolean
     *
     * @ORM\Column(name="visible", type="boolean")
     * @Grid\Column(title="Visible");
     */
    protected $visible = true;

    /**
     * @var ServiceImage
     *
     * @ORM\OneToOne(targetEntity="ServiceImage", mappedBy="service", cascade={"all"})
     * @Assert\NotBlank
     */
    protected $mainImage;

    /**
     * @var string
     * 
     * @ORM\Column(type="string", length=255)
     * @Assert\NotBlank
     * @Assert\Choice(choices = {"academy", "stats", "other"}, message = "Choose a valid service type.")
     * @Grid\Column(title="Type");
     */
    protected $type;

    /**
     * @var Area
     *
     * @ORM\ManyToOne(targetEntity="Area", inversedBy="services")
     * @Assert\NotBlank
     * @Grid\Column(title="Area", field="area.name");
     */
    protected $area;
    
    /**
     * @var \AndresGotta\Bundle\SaleBundle\Entity\CustomPrice
     * 
     * @ORM\OneToMany(targetEntity="\AndresGotta\Bundle\SaleBundle\Entity\CustomPrice", mappedBy="service", cascade={"all"}) 
     */
    protected $customPrices;
    
    /**
     * @var float
     * 
     * @ORM\Column(name="price", type="decimal")
     * @Grid\Column(title="Price") 
     */
    protected $price;

    /**
     * @var integer
     *
     * @ORM\Column(name="days", type="integer")
     * @Assert\NotBlank
     * @Grid\Column(title="Days")
     */
    protected $days;

    /**
     * @var integer
     *
     * @ORM\Column(name="trial_days", type="integer")
     * @Assert\NotBlank
     * @Grid\Column(title="Trial days")
     */
    protected $trialDays;

    /**
     * @var boolean
     *
     * @ORM\Column(name="is_lifetime", type="boolean")
     * @Grid\Column(title="Is Lifetime")
     */
    protected $isLifetime;

    /**
     * @var integer
     *
     * @ORM\Column(type="integer")
     * @Grid\Column(title="Debit Dues");
     */
    protected $debitDues;


    /**
     * Constructor
     */
    public function __construct($name = null, $description = null, $type = null, Area $area = null)
    {
        $this->name = $name;
        $this->description = $description;
        $this->longDescription = $description;
        $this->type = $type;
        $this->area = $area;
        $this->customPrices = new ArrayCollection();
    }

    /**
     * @return string
     */
    public function __toString()
    {
        return $this->name;
    }

    /**
     * Get id
     *
     * @return integer
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set name
     *
     * @param string $name
     * @return Service
     */
    public function setName($name)
    {
        $this->name = $name;

        return $this;
    }

    /**
     * Get name
     *
     * @return string
     */
    public function getName()
    {
        return $this->name;
    }

    /**
     * Set description
     *
     * @param string $description
     * @return Service
     */
    public function setDescription($description = null)
    {
        $this->description = $description;

        return $this;
    }

    /**
     * Get description
     *
     * @return string
     */
    public function getDescription()
    {
        return $this->description;
    }

    /**
     * @return string
     */
    public function getLongDescription()
    {
        return $this->longDescription;
    }

    /**
     * @param string $longDescription
     */
    public function setLongDescription($longDescription)
    {
        $this->longDescription = $longDescription;
    }

    /**
     * Set createdAt
     *
     * @param \DateTime $createdAt
     * @return Service
     */
    public function setCreatedAt($createdAt)
    {
        $this->createdAt = $createdAt;

        return $this;
    }

    /**
     * Get createdAt
     *
     * @return \DateTime
     */
    public function getCreatedAt()
    {
        return $this->createdAt;
    }

    /**
     * Get mainImage
     *
     * @return ServiceImage
     */
    public function getMainImage()
    {
        return $this->mainImage;
    }

    /**
     * Set mainImage
     * 
     * @param ServiceImage $mainImage
     */
    public function setMainImage(ServiceImage $mainImage)
    {
        $this->mainImage = $mainImage;

        $mainImage->setService($this);
    }

    /**
     * Get visible
     * 
     * @return boolean
     */
    public function getVisible()
    {
        return $this->visible;
    }

    /**
     * Set visible
     * 
     * @param boolean $visible
     */
    public function setVisible($visible)
    {
        $this->visible = $visible;
    }

    /**
     * Get type
     * 
     * @return string
     */
    public function getType()
    {
        return $this->type;
    }

    /**
     * Set type
     * 
     * @param string $type
     * @return Service
     */
    public function setType($type)
    {
        $this->type = $type;
        return $this;
    }

    /**
     * Get area
     * 
     * @return Area
     */
    public function getArea()
    {
        return $this->area;
    }

    /**
     * Set area
     * 
     * @param Area $area
     * @return Service
     */
    public function setArea(Area $area)
    {
        $this->area = $area;
        return $this;
    }

    /**
     * Set highlighted
     *
     * @param string $highlighted Destacado
     * @return Service
     */
    public function setHighlighted($highlighted)
    {
        $this->highlighted = $highlighted;

        return $this;
    }

    /**
     * Get highlighted
     *
     * @return string
     */
    public function getHighlighted()
    {
        return $this->highlighted;
    }

    /**
     * @return string
     */
    public function getImportant()
    {
        return $this->important;
    }

    /**
     * @param bool $important
     * @return Service
     */
    public function setImportant($important)
    {
        $this->important = $important;

        return $this;
    }

    
    /**
     * Get customPrices
     * 
     * @return ArrayCollection
     */
    public function getCustomPrices()
    {
        return $this->customPrices;
    }

    /**
     * Add customPrice
     * 
     * @param CustomPrice $customPrice
     * @return Service
     */
    public function addCustomPrice(CustomPrice $customPrice)
    {
        $this->customPrices[] = $customPrice;
        $customPrice->setService($this);
        
        return $this;
    }

    /**
     * Remove customPrice
     * 
     * @param CustomPrice $customPrice
     * @return Service
     */
    public function removeCustomPrice(CustomPrice $customPrice)
    {
        $this->customPrices->removeElement($customPrice);
        
        return $this;
    }

    /**
     * Get price
     * 
     * @return string
     */
    public function getPrice()
    {
        return $this->price;
    }

    /**
     * Set price
     * 
     * @param string $price
     * @return Service
     */
    public function setPrice($price)
    {
        $this->price = $price;
        
        return $this;
    }

    /**
     * 
     * @return boolean
     */
    public function isAvailable()
    {
        return $this->visible;
    }

    /**
     * @param boolean $available
     */
    public function setAvailable($available)
    {
        $this->visible = $available;
    }

    /**
     * @return integer
     */
    public function getDays()
    {
        return $this->days;
    }

    /**
     * @param integer $days
     */
    public function setDays($days)
    {
        $this->days = $days;
    }

    /**
     * @return integer
     */
    public function getTrialDays()
    {
        return $this->trialDays;
    }

    /**
     * @param integer $trialDays
     */
    public function setTrialDays($trialDays)
    {
        $this->trialDays = $trialDays;
    }

    /**
     * @return integer
     */
    public function getIsLifetime()
    {
        return $this->isLifetime;
    }

    /**
     * @param boolean $isLifetime
     */
    public function setIsLifetime($isLifetime)
    {
        $this->isLifetime = $isLifetime;
    }

    /**
     * @param \WebFactory\Bundle\ProductBundle\Model\ProductInterface $product
     * @return bool
     */
    public function isEquals(ProductInterface $product)
    {
        if ($this->getId() != $product->getId()) {
            return false;
        }
        
        if ($this->getName() != $product->getName()) {
            return false;
        }
        
        if ($this->getDescription() != $product->getDescription()) {
            return false;
        }
        
        return true;
    }

    /**
     * Get debitDues
     * 
     * @return integer
     */
    public function getDebitDues()
    {
        return $this->debitDues;
    }

    /**
     * Set debitDues
     * 
     * @param integer $debitDues
     */
    public function setDebitDues($debitDues)
    {
        $this->debitDues = $debitDues;
    }
}
