<?php

namespace AndresGotta\Bundle\ServiceBundle\Entity;

use APY\DataGridBundle\Grid\Mapping as Grid;
use Doctrine\ORM\Mapping as ORM;
use Gedmo\Mapping\Annotation as Gedmo;
use Symfony\Component\Validator\Constraints as Assert;
use Symfony\Bridge\Doctrine\Validator\Constraints as DoctrineAssert;
use WebFactory\Bundle\CommonBundle\Model\Image as BaseEntity;

/**
 * Format
 *
 * @ORM\Table(name="service_images")
 * @ORM\Entity
 */
class ServiceImage extends BaseEntity
{
    
    const DIRECTORY = 'uploads/service/images';
    
    /**
     * @var integer
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    protected $id;

    /**
     * @var Service
     *
     * @ORM\OneToOne(targetEntity="Service", inversedBy="mainImage", cascade={"persist"}, fetch="EAGER")
     * @ORM\JoinColumn(name="service_id", referencedColumnName="id")
     */
    protected $service;

    
    /**
     * Set service
     *
     * @param Service $service
     * @return ServiceImage
     */
    public function setService(Service $service = null)
    {
        $this->service = $service;

        return $this;
    }

    /**
     * Get service
     *
     * @return Service
     */
    public function getService()
    {
        return $this->service;
    }

}