<?php

namespace AndresGotta\Bundle\ServiceBundle\Entity;

use APY\DataGridBundle\Grid\Mapping as Grid;
use Doctrine\ORM\Mapping as ORM;
use Gedmo\Mapping\Annotation as Gedmo;
use Symfony\Component\Validator\Constraints as Assert;

/**
 * Class PromotionalCode
 * @package AndresGotta\Bundle\ServiceBundle\Entity
 *
 * @ORM\Entity(repositoryClass="AndresGotta\Bundle\ServiceBundle\Repository\PromotionalCodeRepository")
 * @ORM\Table(name="promotional_codes")
 */
class PromotionalCode
{
    /**
     * @var integer
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id()
     * @ORM\GeneratedValue(strategy="AUTO")
     * @Grid\Column(filterable=false)
     */
    private $id;

    /**
     * @var string
     *
     * @ORM\Column(name="code", type="string", unique=true)
     */
    private $code;

    /**
     * @var integer
     *
     * @ORM\Column(name="discount", type="integer")
     */
    private $discount;

    /**
     * @var Service
     *
     * @ORM\ManyToOne(targetEntity="AndresGotta\Bundle\ServiceBundle\Entity\Service")
     * @Grid\Column(field="service.name")
     */
    private $service;

    // TODO: crear un fix para eliminar este campo. Ya no es necesario
    /**
     * @var boolean
     *
     * @ORM\Column(name="single_use", type="boolean")
     * @Grid\Column(visible=false)
     */
    private $singleUse;

    /**
     * @var integer
     *
     * @ORM\Column(name="number_of_uses", type="integer")
     * @Assert\Range(min="0")
     * @Grid\Column(title="Number of uses")
     */
    private $numberOfUses;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="expiration_date", type="date")
     */
    private $expirationDate;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="created_at", type="datetime")
     * @Gedmo\Timestampable(on="create")
     * @Grid\Column(filterable=false)
     */
    protected $createdAt;

    /**
     * PromotionalCode constructor.
     * @param int $numberOfUses
     */
    public function __construct($numberOfUses = 5)
    {
        $this->singleUse = false;
        $this->expirationDate = new \DateTime;
        $this->numberOfUses = $numberOfUses;
    }

    /**
     * @return string
     */
    public function getCode()
    {
        return $this->code;
    }

    /**
     * @param string $code
     */
    public function setCode($code)
    {
        $this->code = $code;
    }

    /**
     * @return int
     */
    public function getDiscount()
    {
        return $this->discount;
    }

    /**
     * @param int $discount
     */
    public function setDiscount($discount)
    {
        $this->discount = $discount;
    }

    /**
     * @return Service
     */
    public function getService()
    {
        return $this->service;
    }

    /**
     * @param Service $service
     */
    public function setService($service)
    {
        $this->service = $service;
    }

    /**
     * @return bool
     */
    public function isSingleUse()
    {
        return $this->singleUse;
    }

    /**
     * @return \DateTime
     */
    public function getExpirationDate()
    {
        return $this->expirationDate;
    }

    /**
     * @param \DateTime $expirationDate
     */
    public function setExpirationDate($expirationDate)
    {
        $this->expirationDate = $expirationDate;
    }

    /**
     * @return int
     */
    public function getNumberOfUses()
    {
        return $this->numberOfUses ?: (integer)$this->isSingleUse();
    }

    /**
     * @param int $numberOfUses
     */
    public function setNumberOfUses($numberOfUses)
    {
        $this->numberOfUses = $numberOfUses;
    }
}
