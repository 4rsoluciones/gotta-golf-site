<?php

namespace AndresGotta\Bundle\ServiceBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use Gedmo\Mapping\Annotation as Gedmo;
use Symfony\Component\Validator\Constraints as Assert;
use APY\DataGridBundle\Grid\Mapping as Grid;
use Symfony\Bridge\Doctrine\Validator\Constraints\UniqueEntity;
use Doctrine\Common\Collections\ArrayCollection;

/**
 * Service
 *
 * @ORM\Table(name="areas")
 * @ORM\Entity
 * @UniqueEntity("name")
 */
class Area
{

    /**
     * @var integer
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     * @Grid\Column(filterable=false);
     */
    protected $id;

    /**
     * @var string
     *
     * @ORM\Column(name="name", type="string", length=255, unique=true)
     * @Assert\NotBlank
     * @Grid\Column(title="Name");
     * @Assert\Length(
     *      min = "2",
     *      max = "30"
     * )
     */
    protected $name;
    
    /**
     * @Gedmo\Slug(fields={"name"})
     * @ORM\Column(length=64, unique=true)
     */
    protected $slug;

    /**
     * @var string
     *
     * @ORM\Column(name="description", type="text")
     * @Assert\NotBlank
     * @Grid\Column(title="Description");
     * @Assert\Length(
     *      min = "2",
     *      max = "100"
     * )
     */
    protected $description;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="created_at", type="datetime")
     * @Gedmo\Timestampable(on="create")
     * @Grid\Column(title="Created At");
     */
    protected $createdAt;
    
    /**
     * @var ArrayCollection
     *
     * @ORM\OneToMany(targetEntity="Service", mappedBy="area")
     */
    protected $services;
    
    
    /**
     * Constructor
     */
    public function __construct($name = null, $description = null)
    {
        $this->name = $name;
        $this->description = $description;
        $this->services = new ArrayCollection();
    }

    /**
     * 
     * @return string
     */
    public function __toString()
    {
        return $this->name;
    }
    
    /**
     * Get id
     *
     * @return integer
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set name
     *
     * @param string $name
     * @return Area
     */
    public function setName($name)
    {
        $this->name = $name;

        return $this;
    }

    /**
     * Get name
     *
     * @return string
     */
    public function getName()
    {
        return $this->name;
    }
    
    /**
     * Get slug
     *
     * @return string
     */
    public function getSlug()
    {
        return $this->slug;
    }

    /**
     * Set description
     *
     * @param string $description
     * @return Area
     */
    public function setDescription($description)
    {
        $this->description = $description;

        return $this;
    }

    /**
     * Get description
     *
     * @return string
     */
    public function getDescription()
    {
        return $this->description;
    }

    /**
     * Set createdAt
     *
     * @param \DateTime $createdAt
     * @return Area
     */
    public function setCreatedAt($createdAt)
    {
        $this->createdAt = $createdAt;

        return $this;
    }

    /**
     * Get createdAt
     *
     * @return \DateTime
     */
    public function getCreatedAt()
    {
        return $this->createdAt;
    }

    /**
     * Add services
     *
     * @param Service $service Servicio
     * @return Area
     */
    public function addService(Service $service)
    {
        $this->services[] = $service;
        
        $service->setArea($this);

        return $this;
    }

    /**
     * Remove services
     *
     * @param Service $service Servicio
     *
     * @return null
     */
    public function removeService(Service $service)
    {
        $this->services->removeElement($service);
    }

    /**
     * Get services
     *
     * @return ArrayCollection
     */
    public function getServices()
    {
        return $this->services;
    }

}