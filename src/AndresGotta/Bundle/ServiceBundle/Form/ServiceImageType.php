<?php

namespace AndresGotta\Bundle\ServiceBundle\Form;

use Symfony\Component\OptionsResolver\OptionsResolverInterface;
use WebFactory\Bundle\CommonBundle\Form\ImageType;

class ServiceImageType extends ImageType
{

    public function setDefaultOptions(OptionsResolverInterface $resolver)
    {
        $resolver->setDefaults(array(
            'data_class' => 'AndresGotta\Bundle\ServiceBundle\Entity\ServiceImage'
        ));
    }

    public function getName()
    {
        return 'image';
    }

}
