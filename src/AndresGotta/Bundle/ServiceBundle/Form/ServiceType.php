<?php

namespace AndresGotta\Bundle\ServiceBundle\Form;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolverInterface;
use AndresGotta\Bundle\ServiceBundle\Model\ServiceTypes;

class ServiceType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('name', 'text', array(
                'attr' => array('class' => 'span8')
            ))
            ->add('description', 'textarea', array(
                'attr' => array(
                    'class' => 'span12',
                    'rows' => '2',
                )
            ))
            ->add('longDescription', 'textarea', array(
                'attr' => array(
                    'class' => 'span12',
                    'rows' => '8',
                )
            ))
            ->add('highlighted', null, array(
                'widget_checkbox_label' => 'label'
            ))
            ->add('important', null, array(
                'widget_checkbox_label' => 'label'
            ))
            ->add('visible', null, array(
                'help_inline'  => 'Is visible for users',
                'widget_checkbox_label' => 'label'
            ))
            ->add('mainImage', new ServiceImageType, array(
                'help_block' => 'Dimensiones recomendadas: 220 x 140 px'
                ))
            ->add('type', 'choice', array(
                'choices' => ServiceTypes::getChoices()
            ))
            ->add('area', 'entity', array(
                'class' => 'AndresGottaServiceBundle:Area',
            ))
            ->add('trialDays')
            ->add('price')
            ->add('debitDues')
            ->add('isLifetime')
            ->add('days')
        ;
    }

    public function setDefaultOptions(OptionsResolverInterface $resolver)
    {
        $resolver->setDefaults(array(
            'data_class' => 'AndresGotta\Bundle\ServiceBundle\Entity\Service'
        ));
    }

    public function getName()
    {
        return 'andresgotta_bundle_servicebundle_servicetype';
    }
}
