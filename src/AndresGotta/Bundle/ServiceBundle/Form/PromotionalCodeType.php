<?php

namespace AndresGotta\Bundle\ServiceBundle\Form;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;

/**
 * Class PromotionalCodeType
 * @package AndresGotta\Bundle\ServiceBundle\Form
 */
class PromotionalCodeType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('code', 'text', [
                'read_only' => true
            ])
            ->add('discount', 'integer', [
                'attr' => [
                    'min' => '1',
                    'max' => '100'
                ]
            ])
            ->add('service', 'entity', [
                'class' => 'AndresGotta\Bundle\ServiceBundle\Entity\Service',
                'choice_label' => 'name',
                'empty_value' => 'Todos',
                'required' => false
            ])
            ->add('numberOfUses', null , [
                'required' => false,
            ])
            ->add('expirationDate', 'date', [
                'years' => range(date('Y'), date('Y') + 5)
            ])
            ->add('save', 'submit')
        ;
    }

    public function getName()
    {
        return 'promotional_code_type';
    }
}
