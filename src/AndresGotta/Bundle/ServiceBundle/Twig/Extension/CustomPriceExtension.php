<?php

namespace AndresGotta\Bundle\ServiceBundle\Twig\Extension;

use Twig_Extension;
use Twig_SimpleFilter;
use WebFactory\Bundle\ProductBundle\Model\ProductInterface;
use WebFactory\Bundle\ProductBundle\Model\ProductManager;

class CustomPriceExtension extends Twig_Extension
{

    private $productManager;

    public function __construct(ProductManager $productManager)
    {
        $this->productManager = $productManager;
    }

    /**
     * {@inheritdoc}
     */
    public function getFilters()
    {
        return array(
            new Twig_SimpleFilter('customPrice', array($this, 'getCustomPrice')),
        );
    }

    public function getCustomPrice(ProductInterface $product)
    {
        return $this->productManager->getPriceByProduct($product);
    }

    /**
     * Returns the name of the extension.
     *
     * @return string The extension name
     */
    public function getName()
    {
        return 'custom_price';
    }

}
