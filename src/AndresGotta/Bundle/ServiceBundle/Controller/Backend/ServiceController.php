<?php

namespace AndresGotta\Bundle\ServiceBundle\Controller\Backend;

use AndresGotta\Bundle\ServiceBundle\Entity\PromotionalCode;
use AndresGotta\Bundle\ServiceBundle\Form\PromotionalCodeType;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Template;
use AndresGotta\Bundle\ServiceBundle\Entity\Service;
use AndresGotta\Bundle\ServiceBundle\Form\ServiceType;
use APY\DataGridBundle\Grid\Source\Entity;
use APY\DataGridBundle\Grid\Action\RowAction;

/**
 * Service controller.
 *
 * @Route("/service")
 */
class ServiceController extends Controller
{

    /**
     * Lists all Service entities.
     *
     * @Route("/", name="backend_service")
     * @Method("GET|POST")
     * @Template()
     */
    public function indexAction()
    {
        /* @var $source \APY\DataGridBundle\Grid\Source\Entity */
        $source = new Entity('AndresGottaServiceBundle:Service');
        /* @var $grid \APY\DataGridBundle\Grid\Grid */
        $grid = $this->get('grid');
        $grid->setSource($source);
        $grid->setId('main');
        $grid->setHiddenColumns(array('id', 'longDescription', 'price', 'debitDues', 'isLifetime', 'days'));
        $grid->setDefaultOrder('createdAt', 'desc');

        $showRowAction = new RowAction('Show', 'backend_service_show', false, '_self', array('class' => 'btn-action glyphicons eye_open btn-info'));
        $grid->addRowAction($showRowAction);
        $editRowAction = new RowAction('Edit', 'backend_service_edit', false, '_self', array('class' => 'btn-action glyphicons pencil btn-success'));
        $grid->addRowAction($editRowAction);
//        $deleteRowAction = new RowAction('Delete', 'backend_service_delete', true, '_self', array('class' => 'btn-action glyphicons remove_2 btn-danger'));
//        $grid->addRowAction($deleteRowAction);

        return $grid->getGridResponse('AndresGottaServiceBundle:Backend/Service:index.html.twig');
    }

    /**
     * @Route("/promotional-codes", name="backend_service_promotional_codes_list")
     * @Template("AndresGottaServiceBundle:Backend/PromotionalCode:list.html.twig")
     */
    public function listPromotionalCodesAction()
    {
        $source = new Entity('AndresGottaServiceBundle:PromotionalCode');
        $grid = $this->get('grid');
        $grid->setSource($source);
        $grid->setId('main');
        $grid->setHiddenColumns(['id', 'createdAt']);
        $grid->setDefaultOrder('createdAt', 'desc');

        $grid->getColumn('discount')->manipulateRenderCell(
            function($value, $row, $router) {
                return $value.'%';
            }
        );

        $grid->getColumn('service.name')->manipulateRenderCell(
            function($value, $row, $router) {
                if (!$value) {
                    return 'Todos';
                } else {
                    return $value;
                }
            }
        );

        $grid->getColumn('numberOfUses')->manipulateRenderCell(
            function($value, $row, $router) {
                if ($value) {
                    return $value;
                }

                return $this->get('translator')->trans('No limits');
            }
        );

        $editRowAction = new RowAction('Edit', 'backend_service_promotional_codes_edit', false, '_self', array('class' => 'btn-action glyphicons pencil btn-success'));
        $grid->addRowAction($editRowAction);
        $deleteRowAction = new RowAction('Delete', 'backend_service_promotional_codes_delete', true, '_self', array('class' => 'btn-action glyphicons remove_2 btn-danger'));
        $grid->addRowAction($deleteRowAction);

        return $grid->getGridResponse('AndresGottaServiceBundle:Backend/PromotionalCode:list.html.twig');
    }

    /**
     * @Route("/promotional-codes/create", name="backend_service_promotional_codes_create")
     * @Template("AndresGottaServiceBundle:Backend/PromotionalCode:create.html.twig")
     */
    public function createPromotionalCodeAction(Request $request)
    {
        $promotionalCode = new PromotionalCode();

        if ($request->getMethod() != 'POST') {
            $promotionalCodeRepository = $this->getDoctrine()->getRepository('AndresGottaServiceBundle:PromotionalCode');
            $promotionalCode->setCode($promotionalCodeRepository->generateNewCode());
        }

        $form = $this->createForm(new PromotionalCodeType(), $promotionalCode);

        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $em = $this->getDoctrine()->getManager();
            $em->persist($promotionalCode);
            $em->flush();

            $request->getSession()->getFlashBag()->add('success', 'flash.message.generic.created');

            return $this->redirect($this->generateUrl('backend_service_promotional_codes_list'));
        }

        return [
            'form' => $form->createView()
        ];
    }

    /**
     * @Route("/promotional-codes/edit/{id}", name="backend_service_promotional_codes_edit")
     * @Template("AndresGottaServiceBundle:Backend/PromotionalCode:edit.html.twig")
     */
    public function editPromotionalCodeAction(Request $request, PromotionalCode $promotionalCode)
    {
        $form = $this->createForm(new PromotionalCodeType(), $promotionalCode);

        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $em = $this->getDoctrine()->getManager();
            $em->persist($promotionalCode);
            $em->flush();

            $request->getSession()->getFlashBag()->add('success', 'flash.message.generic.updated');

            return $this->redirect($this->generateUrl('backend_service_promotional_codes_list'));
        }

        return [
            'form' => $form->createView()
        ];
    }

    /**
     * @Route("/promotional-codes/delete/{id}", name="backend_service_promotional_codes_delete")
     */
    public function deletePromotionalCodeAction(Request $request, PromotionalCode $promotionalCode)
    {
        $em = $this->getDoctrine()->getManager();
        $em->remove($promotionalCode);
        $em->flush();

        $request->getSession()->getFlashBag()->add('success', 'flash.message.generic.deleted');

        return $this->redirect($this->generateUrl('backend_service_promotional_codes_list'));
    }
    
    /**
     * Creates a new Service entity.
     *
     * @Route("/new", name="backend_service_create")
     * @Method("POST")
     * @Template("AndresGottaServiceBundle:Backend/Service:new.html.twig")
     */
    public function createAction(Request $request)
    {
        $entity  = new Service();
        $form = $this->createForm(new ServiceType(), $entity);
        $form->bind($request);

        if ($form->isValid()) {
            $em = $this->getDoctrine()->getManager();
            $em->persist($entity);
            $em->flush();
            
            $request->getSession()->getFlashBag()->add('success', 'flash.message.generic.created');

            return $this->redirect($this->generateUrl('backend_service_show', array('id' => $entity->getId())));
        }

        return array(
            'entity' => $entity,
            'form'   => $form->createView(),
        );
    }

    /**
     * Displays a form to create a new Service entity.
     *
     * @Route("/new", name="backend_service_new")
     * @Method("GET")
     * @Template()
     */
    public function newAction()
    {
        $entity = new Service();
        $form   = $this->createForm(new ServiceType(), $entity);

        return array(
            'entity' => $entity,
            'form'   => $form->createView(),
        );
    }

    /**
     * Finds and displays a Service entity.
     *
     * @Route("/{id}", name="backend_service_show")
     * @Method("GET")
     * @Template()
     */
    public function showAction($id)
    {
        $em = $this->getDoctrine()->getManager();

        $entity = $em->getRepository('AndresGottaServiceBundle:Service')->find($id);

        if (!$entity) {
            throw $this->createNotFoundException('Unable to find Service entity.');
        }

        $deleteForm = $this->createDeleteForm($id);

        return array(
            'entity'      => $entity,
            'delete_form' => $deleteForm->createView(),
        );
    }

    /**
     * Displays a form to edit an existing Service entity.
     *
     * @Route("/{id}/edit", name="backend_service_edit")
     * @Method("GET")
     * @Template()
     */
    public function editAction($id)
    {
        $em = $this->getDoctrine()->getManager();

        $entity = $em->getRepository('AndresGottaServiceBundle:Service')->find($id);

        if (!$entity) {
            throw $this->createNotFoundException('Unable to find Service entity.');
        }

        $editForm = $this->createForm(new ServiceType(), $entity);
        $deleteForm = $this->createDeleteForm($id);

        return array(
            'entity'      => $entity,
            'edit_form'   => $editForm->createView(),
            'delete_form' => $deleteForm->createView(),
        );
    }

    /**
     * Edits an existing Service entity.
     *
     * @Route("/{id}", name="backend_service_update")
     * @Method("PUT")
     * @Template("AndresGottaServiceBundle:Backend/Service:edit.html.twig")
     */
    public function updateAction(Request $request, $id)
    {
        $em = $this->getDoctrine()->getManager();

        $entity = $em->getRepository('AndresGottaServiceBundle:Service')->find($id);

        if (!$entity) {
            throw $this->createNotFoundException('Unable to find Service entity.');
        }

        $deleteForm = $this->createDeleteForm($id);
        $editForm = $this->createForm(new ServiceType(), $entity);
        $editForm->bind($request);

        if ($editForm->isValid()) {
            $em->persist($entity);
            $em->flush();

            $request->getSession()->getFlashBag()->add('success', 'flash.message.generic.updated');

            return $this->redirect($this->generateUrl('backend_service_edit', array('id' => $id)));
        } else {
            foreach ($editForm->getErrors(true) as $error) {
                if ($error->getMessageTemplate() === 'andres_gotta_service.max_important.validator' && count($editForm->getErrors(true)) === 1) {
                    $serviceRepository = $this->getDoctrine()->getRepository('AndresGottaServiceBundle:Service');
                    $services = $serviceRepository->getAllServices();

                    $importantServices = 0;
                    foreach ($services as $service) {
                        if ($service->getImportant() === true) {
                            ++$importantServices;
                        }
                    }

                    if ($importantServices <= 3) {
                        $em->persist($entity);
                        $em->flush();

                        $request->getSession()->getFlashBag()->add('success', 'flash.message.generic.updated');

                        return $this->redirect($this->generateUrl('backend_service_edit', array('id' => $id)));
                    }
                }
            }
        }
        return array(
            'entity'      => $entity,
            'edit_form'   => $editForm->createView(),
            'delete_form' => $deleteForm->createView(),
        );
    }
    /**
     * Deletes a Service entity.
     *
     * @Route("/{id}/delete", name="backend_service_delete")
     * @Method("DELETE|GET")
     */
    public function deleteAction(Request $request, $id)
    {
        $form = $this->createDeleteForm($id);
        $form->bind($request);

        if ($form->isValid()) {
            $em = $this->getDoctrine()->getManager();
            $entity = $em->getRepository('AndresGottaServiceBundle:Service')->find($id);

            if (!$entity) {
                throw $this->createNotFoundException('Unable to find Service entity.');
            }

            $em->remove($entity);
            $em->flush();
            
            $request->getSession()->getFlashBag()->add('success', 'flash.message.generic.deleted');
        }

        return $this->redirect($this->generateUrl('backend_service'));
    }

    /**
     * Creates a form to delete a Service entity by id.
     *
     * @param mixed $id The entity id
     *
     * @return \Symfony\Component\Form\Form The form
     */
    private function createDeleteForm($id)
    {
        return $this->createFormBuilder(array('id' => $id), array('csrf_protection' => false))
            ->add('id', 'hidden')
            ->getForm()
        ;
    }
}
