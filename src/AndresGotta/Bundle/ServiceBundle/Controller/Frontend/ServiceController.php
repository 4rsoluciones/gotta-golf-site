<?php

namespace AndresGotta\Bundle\ServiceBundle\Controller\Frontend;

use AndresGotta\Bundle\ServiceBundle\Entity\Area;
use AndresGotta\Bundle\ServiceBundle\Entity\Service;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\ParamConverter;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Template;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;

/**
 * Service controller.
 *
 * @Route("/service")
 */
class ServiceController extends Controller
{

    /**
     * Lists all Service entities.
     *
     * @Route("/", name="frontend_service")
     * @Route("/{area}", name="frontend_service_by_area")
     * @ParamConverter("area", options={"mapping": {"area": "slug"}})
     * @Method("GET")
     * @Template()
     */
    public function indexAction(Area $area = null)
    {

        $url = $this->container->getParameter('gotta_center_api.frontend') . '/service?platform=' .
        $this->container->getParameter('platform_name') . '&redirectTo=' . $this->getRequest()->headers->get('referer');

        return $this->redirect($url);


        $em = $this->getDoctrine()->getManager();
        $conditions = array('visible' => true, 'highlighted' => false);
        
        if ($area) {
            $conditions['area'] = $area;
        }

        $entities = $em->getRepository('AndresGottaServiceBundle:Service')
            ->findBy($conditions, array('name' => 'ASC'));
        $areas = $em->getRepository('AndresGottaServiceBundle:Area')
            ->findBy(array(), array('name' => 'ASC'));

        $highlighted = $em->getRepository('AndresGottaServiceBundle:Service')
            ->findBy(['important' => true], ['id' => 'ASC']);

        return array(
            'entities' => $entities,
            'highlighted' => $highlighted,
            'areas' => $areas,
            'selected_area' => $area,
        );
    }
    
    /**
     * @Route("/{area}/show_area", name="frontend_service_show_area")
     * @ParamConverter("area", options={"mapping": {"area": "slug"}})
     * @Method("GET")
     * @Template
     */
    public function showAreaAction(Area $area)
    {
        $em = $this->getDoctrine()->getManager();
        $conditions = array(
            'visible' => true,
            'area' => $area,
        );

        $services = $em->getRepository('AndresGottaServiceBundle:Service')->findBy($conditions, array('name' => 'ASC'));

        return array(
            'entities' => $services,
            'selected_area' => $area,
        );
    }
    
    /**
     * @Route("/{service}/show", name="frontend_service_show")
     * @Method("GET")
     * @Template
     */
    public function showAction(Service $service)
    {
        return array('service' => $service);
    }

}
