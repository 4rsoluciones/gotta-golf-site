<?php

namespace AndresGotta\Bundle\ServiceBundle\Command;

use Symfony\Bundle\FrameworkBundle\Command\ContainerAwareCommand;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;

/**
 * Class CreateUserCommand
 * @package AndresGotta\Bundle\ServiceBundle\Command
 * @deprecated todo quitarlo
 */
class CheckPromotionalCodesExpirationCommand extends ContainerAwareCommand
{
    /**
     *
     */
    protected function configure()
    {
        $this
            ->setName('check:promotional-codes')
            ->setDescription('Checks for promotional codes expiration date.')
            ->setHelp('Running this command will check for the current promotional codes expiration date, and remove them that expired.')
        ;
    }

    /**
     * @param InputInterface $input
     * @param OutputInterface $output
     * @return int|null|void
     */
    protected function execute(InputInterface $input, OutputInterface $output)
    {
        $output->writeln('DEPRECATED!');
        return;
        $output->writeln('Looking for expired promotional codes...');

        $em = $this->getContainer()->get('doctrine.orm.entity_manager');
        $promotionalCodes = $em->getRepository('AndresGottaServiceBundle:PromotionalCode')->findAll();

        foreach ($promotionalCodes as $promotionalCode) {
            if ($promotionalCode->getExpirationDate() < new \DateTime()) {
                $em->remove($promotionalCode);
            }
        }

        $em->flush();

        $output->writeln('Promotional codes cleaned up successfully!');
    }
}
