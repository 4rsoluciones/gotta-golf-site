<?php

namespace AndresGotta\Bundle\ServiceBundle\DataFixtures\ORM;

use AndresGotta\Bundle\ServiceBundle\Entity\Service;
use AndresGotta\Bundle\ServiceBundle\Entity\ServiceImage;
use AndresGotta\Bundle\ServiceBundle\Model\ServiceTypes;
use Doctrine\Common\DataFixtures\AbstractFixture;
use Doctrine\Common\DataFixtures\OrderedFixtureInterface;
use Doctrine\Common\Persistence\ObjectManager;

class LoadServices extends AbstractFixture implements OrderedFixtureInterface
{

    /**
     * {@inheritDoc}
     */
    public function load(ObjectManager $manager)
    {

        $service1 = new Service('service1', 'Other service', ServiceTypes::OTHER, $this->getReference('area-1'));
        $service1->setMainImage(new ServiceImage());
        $service1->setTrialDays(10);
        $service1->setIsLifetime(false);
        $service1->setDays(60);
        $service1->setPrice(600);
        $service1->setDebitDues(3);
        $manager->persist($service1);

        $service2 = new Service('service2', 'Stat service', ServiceTypes::STATS, $this->getReference('area-1'));
        $service2->setMainImage(new ServiceImage());
        $service2->setTrialDays(0);
        $service2->setIsLifetime(true);
        $service2->setDays(0);
        $service2->setPrice(500);
        $service2->setDebitDues(3);
        $manager->persist($service2);

        $service3 = new Service('service3', 'Academy service', ServiceTypes::ACADEMY, $this->getReference('area-2'));
        $service3->setMainImage(new ServiceImage());
        $service3->setTrialDays(30);
        $service3->setIsLifetime(false);
        $service3->setDays(90);
        $service3->setPrice(300);
        $service3->setDebitDues(1);
        $manager->persist($service3);

        $manager->flush();

        $this->addReference('service-1', $service1);
        $this->addReference('service-2', $service2);
        $this->addReference('service-3', $service3);
    }

    /**
     * {@inheritDoc}
     */
    public function getOrder()
    {
        return 2;
    }

}