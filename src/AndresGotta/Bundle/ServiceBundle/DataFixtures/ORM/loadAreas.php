<?php

namespace AndresGotta\Bundle\ServiceBundle\DataFixtures\ORM;

use Closure;
use DateTime;
use Doctrine\Common\DataFixtures\AbstractFixture;
use Doctrine\Common\DataFixtures\OrderedFixtureInterface;
use Doctrine\Common\Persistence\ObjectManager;
use AndresGotta\Bundle\ServiceBundle\Entity\Area;

class LoadAreas extends AbstractFixture implements OrderedFixtureInterface
{

    /**
     * {@inheritDoc}
     */
    public function load(ObjectManager $manager)
    {

        $area1 = new Area('Area 1', 'Area 1 Description');
        $manager->persist($area1);
        
        $area2 = new Area('Area 2', 'Area 2 Description');
        $manager->persist($area2);

        $manager->flush();

        $this->addReference('area-1', $area1);
        $this->addReference('area-2', $area2);
    }

    /**
     * {@inheritDoc}
     */
    public function getOrder()
    {
        return 1;
    }

}