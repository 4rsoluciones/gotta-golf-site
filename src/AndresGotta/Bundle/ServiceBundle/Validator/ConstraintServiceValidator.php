<?php

namespace AndresGotta\Bundle\ServiceBundle\Validator;

use Symfony\Component\Validator\Constraint;

/**
 * Class ConstraintMaxServiceImportant
 * @package AndresGotta\Bundle\ServiceBundle\Validator\Constraint
 *
 * @Annotation()
 */
class ConstraintServiceValidator extends Constraint
{
    public $message = 'andres_gotta_service.max_important.validator';

    /**
     * @return string
     */
    public function validatedBy()
    {
        return 'service_validator';
    }

    /**
     * @return string
     */
    public function getTargets()
    {
        return self::PROPERTY_CONSTRAINT;
    }
}