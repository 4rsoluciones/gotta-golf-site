<?php

namespace AndresGotta\Bundle\ServiceBundle\Validator\Constraints;

use Doctrine\ORM\EntityManager;
use Symfony\Component\Translation\TranslatorInterface;
use Symfony\Component\Validator\Constraint;
use Symfony\Component\Validator\ConstraintValidator;

/**
 * Class ServiceImportant
 * @package AndresGotta\Bundle\ServiceBundle\Validator\Constraints
 */
class ServiceImportant extends ConstraintValidator
{
    /**
     * @var EntityManager
     */
    private $entityManager;
    /**
     * @var TranslatorInterface
     */
    private $translator;

    /**
     * ServiceImportant constructor.
     * @param EntityManager $entityManager
     * @param TranslatorInterface $translator
     */
    public function __construct(EntityManager $entityManager, TranslatorInterface $translator)
    {
        $this->entityManager = $entityManager;
        $this->translator = $translator;
    }

    /**
     * @inheritdoc
     */
    public function validate($value, Constraint $constraint)
    {
        if(!$value) {
            return;
        }

        $repo = $this->entityManager->getRepository('AndresGottaServiceBundle:Service');
        $importants = $repo->getImportantServices();

        if (intval($importants) > 2) {
            $this->context->addViolation(
                $this->translator->trans('andres_gotta_service.max_important.validator')
            );
        }

    }
}