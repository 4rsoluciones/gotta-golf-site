<?php

namespace AndresGotta\Bundle\ServiceBundle\Model;

use Doctrine\Common\Collections\Collection;

/**
 * Interface ServiceNeededInterface
 * @package AndresGotta\Bundle\ServiceBundle\Model
 */
interface ServiceNeededInterface
{
    /**
     * @return Collection
     */
    public function getServicesNeeded();
}