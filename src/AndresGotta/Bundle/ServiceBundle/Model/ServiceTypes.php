<?php

namespace AndresGotta\Bundle\ServiceBundle\Model;

/**
 * Service Types
 *
 */
final class ServiceTypes
{

    const OTHER = 'other';
    const ACADEMY = 'academy';
    const STATS = 'stats';

    private function __construct()
    {
        // private
    }

    public static function getChoices()
    {
        return array(
            self::ACADEMY => self::ACADEMY,
            self::OTHER => self::OTHER,
            self::STATS => self::STATS,
        );
    }

}