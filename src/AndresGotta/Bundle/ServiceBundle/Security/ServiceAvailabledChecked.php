<?php

namespace AndresGotta\Bundle\ServiceBundle\Security;

use AndresGotta\Bundle\ServiceBundle\Entity\Service;
use Doctrine\ORM\EntityRepository;
use Symfony\Component\Security\Core\Authentication\Token\Storage\TokenStorage;
use WebFactory\Bundle\UserBundle\Entity\User;

/**
 * Class ServiceAvailableChecked
 * @package AndresGotta\Bundle\ServiceBundle\Security
 */
class ServiceAvailabledChecked
{
    /**
     * @var EntityRepository
     */
    protected $repository;

    /**
     * @var TokenStorage
     */
    protected $token;

    /**
     * ServiceAvailableChecked constructor.
     * @param EntityRepository $repository
     * @param TokenStorage $token
     */
    public function __construct(EntityRepository $repository, TokenStorage $token)
    {
        $this->repository = $repository;
        $this->token = $token;
    }


    public function isAvailable(Service $service)
    {
        /** @var User $user */
        $user = $this->token->getToken()->getUser();
        $userServices = $this->repository->findPlayerActiveServices($user);

        ldd($userServices);
    }
}