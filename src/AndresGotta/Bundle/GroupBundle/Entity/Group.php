<?php

namespace AndresGotta\Bundle\GroupBundle\Entity;

use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Validator\Constraints as Assert;
use APY\DataGridBundle\Grid\Mapping as Grid;

/**
 * Class Group
 * @package AndresGotta\Bundle\GroupBundle\Entity
 *
 * @ORM\Entity(repositoryClass="AndresGotta\Bundle\GroupBundle\Doctrine\ORM\GroupRepository")
 * @ORM\Table(name="groups")
 * @Grid\Source(columns="id, authorized, createdAt, name, minimumQuantity, players.id:count", groupBy={"id"})
 */
class Group //implements ServiceNeededInterface
{
    /**
     * @var int
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id()
     * @ORM\GeneratedValue(strategy="AUTO")
     * @Grid\Column(visible=false, filterable=false)
     */
    protected $id;

    /**
     * @var string
     *
     * @ORM\Column(name="name", type="string", length=255)
     * @Assert\NotBlank()
     * @Grid\Column(title="Name", filterable=true)
     */
    protected $name;

    /**
     * @var boolean
     *
     * @ORM\Column(name="authorized", type="boolean")
     * @Grid\Column(title="Authorized", filterable=true)
     */
    protected $authorized = false;

    /**
     * @var boolean
     *
     * @ORM\Column(name="enabled", type="boolean")
     */
    protected $enabled = true;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="created_at", type="datetime")
     * @Grid\Column(title="Created At", filterable=true)
     */
    protected $createdAt;

    /**
     * @var integer
     *
     * @ORM\Column(name="minimum_quantity", type="integer", nullable=true)
     * @Grid\Column(title="Minimum Quantity", filterable=true)
     */
    protected $minimumQuantity;

    /**
     * @var ArrayCollection
     *
     * @ORM\OneToMany(targetEntity="AndresGotta\Bundle\GroupBundle\Entity\GroupPlayer", mappedBy="group", cascade={"persist"}, orphanRemoval=true)
     * @Assert\Valid()
     * @Grid\Column(title="Players", field="players.id:count", filterable=false)
     */
    protected $players;

//    /**
//     * @ORM\ManyToMany(targetEntity="AndresGotta\Bundle\ServiceBundle\Entity\Service")
//     * @ORM\JoinTable(name="groups_services",
//     *      joinColumns={@ORM\JoinColumn(name="group_id", referencedColumnName="id")},
//     *      inverseJoinColumns={@ORM\JoinColumn(name="service_id", referencedColumnName="id")}
//     *      )
//     */
//    protected $servicesNeeded;

    /**
     * Group constructor.
     */
    public function __construct()
    {
        $this->players = new ArrayCollection();
//        $this->servicesNeeded = new ArrayCollection();
        $this->createdAt = new \DateTime;
        $this->minimumQuantity = 25;
    }

    /**
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @return string
     */
    public function getName()
    {
        return $this->name;
    }

    /**
     * @param string $name
     */
    public function setName($name)
    {
        $this->name = $name;
    }
    
    /**
     * @return boolean
     */
    public function isAuthorized()
    {
        return $this->authorized;
    }

    /**
     * @param boolean $authorized
     */
    public function setAuthorized($authorized)
    {
        $this->authorized = $authorized;
    }

    /**
     * @return boolean
     */
    public function isEnabled()
    {
        return $this->enabled;
    }

    /**
     * @param boolean $enabled
     */
    public function setEnabled($enabled)
    {
        $this->enabled = $enabled;
    }

    /**
     * @return \DateTime
     */
    public function getCreatedAt()
    {
        return $this->createdAt;
    }

    /**
     * @param \DateTime $createdAt
     */
    public function setCreatedAt($createdAt)
    {
        $this->createdAt = $createdAt;
    }

    /**
     * @return int
     */
    public function getMinimumQuantity()
    {
        return $this->minimumQuantity;
    }

    /**
     * @param int $minimumQuantity
     */
    public function setMinimumQuantity($minimumQuantity)
    {
        $this->minimumQuantity = $minimumQuantity;
    }


    /**
     * Get authorized
     *
     * @return boolean
     */
    public function getAuthorized()
    {
        return $this->authorized;
    }

    /**
     * Get enabled
     *
     * @return boolean
     */
    public function getEnabled()
    {
        return $this->enabled;
    }

    /**
     * Add player
     *
     * @param GroupPlayer $player
     *
     * @return Group
     */
    public function addPlayer(GroupPlayer $player)
    {
        $this->players[] = $player;
        $player->setGroup($this);

        return $this;
    }

    /**
     * Remove player
     *
     * @param GroupPlayer $player
     */
    public function removePlayer(GroupPlayer $player)
    {
        $this->players->removeElement($player);
        $player->setGroup(null);
    }

    /**
     * Get players
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getPlayers()
    {
        return $this->players;
    }
//
//    /**
//     * @return ArrayCollection
//     */
//    public function getServicesNeeded()
//    {
//        return $this->servicesNeeded;
//    }
//
//    /**
//     * Add servicesNeeded
//     *
//     * @param Service $servicesNeeded
//     *
//     * @return Group
//     */
//    public function addServicesNeeded(Service $servicesNeeded)
//    {
//        $this->servicesNeeded[] = $servicesNeeded;
//
//        return $this;
//    }
//
//    /**
//     * Remove servicesNeeded
//     *
//     * @param Service $servicesNeeded
//     */
//    public function removeServicesNeeded(Service $servicesNeeded)
//    {
//        $this->servicesNeeded->removeElement($servicesNeeded);
//    }

    /**
     * @return string
     */
    public function getAdmin()
    {
        /** @var GroupPlayer $player */
        foreach ($this->players as $player) {
            if ($player->isAdmin()) {
                return (string) $player->getPlayer()->getProfile();
            }
        }
    }

    /**
     * @return array
     */
    public function getUserAdmins()
    {
        $admins = [];
        foreach ($this->players as $player) {
            if ($player->isAdmin()) {
                $admins[] = $player->getPlayer();
            }
        }

        return $admins;
    }
}
