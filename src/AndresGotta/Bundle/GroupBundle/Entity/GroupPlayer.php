<?php

namespace AndresGotta\Bundle\GroupBundle\Entity;

use AndresGotta\Bundle\GroupBundle\ValueObject\PlayerStatuses;
use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Security\Core\User\UserInterface;
use WebFactory\Bundle\UserBundle\Entity\User;
use Symfony\Component\Validator\Constraints as Assert;

/**
 * Class GroupPlayer
 * @package AndresGotta\Bundle\GroupBundle\Entity
 *
 * @ORM\Entity()
 * @ORM\Table(name="group_players")
 */
class GroupPlayer
{
    /**
     * @var int
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id()
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    protected $id;

    /**
     * @var Group
     *
     * @ORM\ManyToOne(targetEntity="AndresGotta\Bundle\GroupBundle\Entity\Group", inversedBy="players")
     * @ORM\JoinColumn(name="group_id", referencedColumnName="id")
     */
    protected $group;

    /**
     * @var User
     *
     * @ORM\ManyToOne(targetEntity="WebFactory\Bundle\UserBundle\Entity\User", inversedBy="playerGroups")
     * @ORM\JoinColumn(name="player_id", referencedColumnName="id")
     * @Assert\NotNull()
     */
    protected $player;

    /**
     * @var boolean
     *
     * @ORM\Column(name="wanna_be_admin", type="boolean")
     */
    protected $wannaBeAdmin;

    /**
     * @var boolean
     *
     * @ORM\Column(name="is_admin", type="boolean")
     */
    protected $isAdmin = false;

    /**
     * @var boolean
     *
     * @ORM\Column(name="confirmed", type="boolean")
     */
    protected $confirmed = false;

    /**
     * @var string
     *
     * @ORM\Column(name="status", type="string", length=64, nullable=true)
     */
    protected $status;

    /**
     * GroupPlayer constructor.
     */
    public function __construct()
    {
        $this->status = PlayerStatuses::PENDING;
        $this->wannaBeAdmin = false;
    }

    /**
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @return Group
     */
    public function getGroup()
    {
        return $this->group;
    }

    /**
     * @param Group $group
     * @return GroupPlayer
     */
    public function setGroup(Group $group = null)
    {
        $this->group = $group;

        return $this;
    }

    /**
     * @return User
     */
    public function getPlayer()
    {
        return $this->player;
    }

    /**
     * @param UserInterface $player
     * @return $this
     */
    public function setPlayer(UserInterface $player)
    {
        $this->player = $player;

        return $this;
    }

    /**
     * @return boolean
     */
    public function isConfirmed()
    {
        return $this->confirmed;
    }

    /**
     * @param boolean $confirmed
     */
    public function setConfirmed($confirmed)
    {
        $this->confirmed = $confirmed;
    }

    /**
     * @return boolean
     */
    public function isAdmin()
    {
        return $this->isAdmin;
    }

    /**
     * @param boolean $isAdmin
     * @return GroupPlayer|boolean
     * @throws \Exception
     */
    public function setIsAdmin($isAdmin)
    {
        if (!in_array(User::ROLE_GROUP_OWNER, $this->player->getRoles())) {
            throw new \Exception('The player must has a Group Owner role');
        }

        $this->isAdmin = $isAdmin;

        return $this;
    }

    /**
     * @return boolean
     */
    public function getGroupMember()
    {
        if (in_array($this->status, [PlayerStatuses::CONFIRMED, PlayerStatuses::PENDING])) {
            return true;
        }

        return false;
    }

    /**
     * @return string
     */
    public function getStatus()
    {
        return $this->status;
    }

    /**
     * @param string $status
     * @return GroupPlayer
     */
    public function setStatus($status)
    {
        $this->status = $status;

        return $this;
    }

    /**
     * @return bool
     */
    public function getWannaBeAdmin()
    {
        return $this->wannaBeAdmin;
    }

    /**
     * @param $wannaBeAdmin
     */
    public function setWannaBeAdmin($wannaBeAdmin)
    {
        $this->wannaBeAdmin = $wannaBeAdmin;
    }

}