<?php

namespace AndresGotta\Bundle\GroupBundle\Form\Type;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;

/**
 * Class SearchPlayerType
 * @package AndresGotta\Bundle\GroupBundle\Form\Type
 */
class SearchPlayerType extends AbstractType
{
    /**
     * @inheritdoc
     */
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('player', 'entity', [
                'class' => 'WebFactory\Bundle\UserBundle\Entity\User',
                'property' => 'profile',
                'empty_value' => '',
            ])
        ;
    }

    /**
     * @inheritdoc
     */
    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults([
            'data_class' => 'AndresGotta\Bundle\GroupBundle\Entity\GroupPlayer',
            'attr' => ['novalidate' => true],
        ]);
    }

    /**
     * @return string
     */
    public function getName()
    {
        return 'search_player_type';
    }

}