<?php

namespace AndresGotta\Bundle\GroupBundle\Form\Type;

use Doctrine\ORM\EntityRepository;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;

/**
 * Class GroupType
 * @package AndresGotta\Bundle\GroupBundle\Form\Type
 */
class GroupType extends AbstractType
{
    /**
     * @var bool
     */
    private $isAdmin;

    /**
     * GroupType constructor.
     * @param $isAdmin
     */
    public function __construct($isAdmin = false)
    {
        $this->isAdmin = $isAdmin;
    }


    /**
     * @inheritdoc
     */
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder->add('name');

        if ($this->isAdmin) {
            $builder
                ->add('minimumQuantity')
                ->add('authorized')
                ->add('servicesNeeded', 'entity', [
                    'class' => 'AndresGotta\Bundle\ServiceBundle\Entity\Service',
                    'property' => 'name',
                    'multiple' => true,
                    'expanded' => true
                ])
                ->add('players', 'collection', [
                    'type' => new SearchPlayerType(),
                    'allow_add' => true,
                    'allow_delete' => true,
                    'prototype' => true,
                    'by_reference' => false,
                    'widget_add_btn' => array(
                        'label' => '',
                        'icon' => 'plus-sign icon-white',
                        'attr' => array(
                            'class' => 'btn btn-primary'
                        )
                    ),
                    'options' => array(
                        'widget_remove_btn' => array(
                            'label' => '',
                            'attr' => array('class' => 'btn btn-danger'),
                            'icon' => 'remove icon-white',
                        ),
                        'attr' => array('class' => 'span3'),
                        'widget_addon' => array(
                            'type' => 'prepend',
                            'text' => '@',
                        ),
                        'widget_control_group' => false,
                        'label' => false,
                        'error_bubbling' => false,
                    )
                ])
            ;
        } else {
            $builder->add('players', 'collection', [
                'type' => new SearchPlayerType(),
                'allow_add' => true,
                'allow_delete' => true,
                'prototype' => true,
                'label' => false,
                'by_reference' => false,
                'options' => [
                    'label' => false,
                ]
            ]);
        }
    }

    /**
     * @inheritdoc
     */
    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults([
            'data_class' => 'AndresGotta\Bundle\GroupBundle\Entity\Group',
            'attr' => [
                'novalidate' => true
            ]
        ]);
    }

    /**
     * @return string
     */
    public function getName()
    {
        return 'group_type';
    }

}