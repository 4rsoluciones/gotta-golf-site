<?php

namespace AndresGotta\Bundle\GroupBundle\ValueObject;

/**
 * Class PlayerStatusses
 * @package AndresGotta\Bundle\GroupBundle\ValueObject
 */
class PlayerStatuses
{
    /**
     * Aun no confirmó la invitación a participar en el grupo
     */
    const PENDING = 'player.pending';

    /**
     * Jugador confirmado para participar
     */
    const CONFIRMED = 'player.confirmed';

    /**
     * El jugador se des subscribió del grupo, no quizo participar más
     */
    const UNCONFIRMED = 'player.unconfirmed';

    /**
     * El jugador fue quitado por el administrador del grupo
     */
    const REMOVED = 'player.removed';

    /**
     * Al jugador se le venció el servicio u ocurrio alguna otra cosa que hizo que no se pueda
     * tener más en cuenta en el grupo
     */
    const IN_STANDBY = 'player.in_standby';

    /**
     * @return array
     */
    public static function getChoices()
    {
        return [
            self::PENDING => self::PENDING,
            self::CONFIRMED => self::CONFIRMED,
            self::UNCONFIRMED => self::UNCONFIRMED,
            self::REMOVED => self::REMOVED,
            self::IN_STANDBY => self::IN_STANDBY,
        ];
    }

    public static function getChoicesForGroupAdmin()
    {
        return [
            self::CONFIRMED => self::CONFIRMED,
            self::REMOVED => self::REMOVED,
        ];
    }
}