<?php

namespace AndresGotta\Bundle\GolfBundle\DataFixtures\ORM;

use AndresGotta\Bundle\GroupBundle\Entity\Group;
use AndresGotta\Bundle\GroupBundle\Entity\GroupPlayer;
use AndresGotta\Bundle\GroupBundle\ValueObject\PlayerStatuses;
use Doctrine\Common\DataFixtures\AbstractFixture;
use Doctrine\Common\DataFixtures\OrderedFixtureInterface;
use Doctrine\Common\Persistence\ObjectManager;

/**
 * Class LoadRounds
 * @package AndresGotta\Bundle\GolfBundle\DataFixtures\ORM
 */
class LoadGroups extends AbstractFixture implements OrderedFixtureInterface
{
    /**
     * {@inheritDoc}
     */
    public function load(ObjectManager $manager)
    {
        $player = $this->getReference('user-player');
        $customer = $this->getReference('user-customer');
        $student = $this->getReference('user-student');
        $teacher = $this->getReference('user-teacher');

        $group = new Group();
        $group->setAuthorized(true);
        $group->setEnabled(true);
        $group->setName('Grupo de prueba 1');

        $playerGroup = new GroupPlayer();
        $playerGroup->setGroup($group)->setPlayer($player)->setStatus(PlayerStatuses::PENDING);
        $group->addPlayer($playerGroup);

        $playerGroup = new GroupPlayer();
        $playerGroup->setGroup($group)->setPlayer($customer)->setStatus(PlayerStatuses::PENDING);
        $group->addPlayer($playerGroup);

        $playerGroup = new GroupPlayer();
        $playerGroup->setGroup($group)->setPlayer($student)->setStatus(PlayerStatuses::PENDING);
        $group->addPlayer($playerGroup);

        $playerGroup = new GroupPlayer();
        $playerGroup->setGroup($group)->setPlayer($teacher)->setIsAdmin(true);
        $group->addPlayer($playerGroup);

        $manager->persist($group);

        $group2 = new Group();
        $group2->setAuthorized(false);
        $group2->setEnabled(true);
        $group2->setName('Grupo de prueba 2');

        $playerGroup = new GroupPlayer();
        $playerGroup->setGroup($group2)->setPlayer($teacher)->setIsAdmin(true);
        $group2->addPlayer($playerGroup);

        $manager->persist($group2);
        $manager->flush();
    }

    /**
     * {@inheritDoc}
     */
    public function getOrder()
    {
        return 11;
    }

}
