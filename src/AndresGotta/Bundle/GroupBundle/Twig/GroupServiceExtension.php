<?php

namespace AndresGotta\Bundle\GroupBundle\Twig;

use AndresGotta\Bundle\GroupBundle\Entity\Group;
use AndresGotta\Bundle\GroupBundle\Entity\GroupPlayer;
use AndresGotta\Bundle\GroupBundle\Manager\GroupManager;
use WebFactory\Bundle\UserBundle\Entity\User;

/**
 * Class GroupServiceExtension
 * @package AndresGotta\Bundle\GroupBundle\Twig
 */
class GroupServiceExtension extends \Twig_Extension
{
    /**
     * @var GroupManager
     */
    private $groupManager;

    /**
     * GroupServiceExtension constructor.
     * @param GroupManager $groupManager
     */
    public function __construct(GroupManager $groupManager)
    {
        $this->groupManager = $groupManager;
    }

    /**
     * @inheritdoc
     */
    public function getFunctions()
    {
        return [
            new \Twig_SimpleFunction('is_group_manager', [$this, 'isGroupManager']),
            new \Twig_SimpleFunction('member_of_groups', [$this, 'getMemberOfGroups']),
            new \Twig_SimpleFunction('group_user_confirmed', [$this, 'groupUserConfirmed'])
        ];
    }

    /**
     * @param User $user
     * @return bool
     */
    public function isGroupManager(User $user)
    {
        return $user->hasRole(User::ROLE_GROUP_OWNER);
    }

    /**
     * @param User $user
     * @return array
     */
    public function getMemberOfGroups(User $user)
    {
        $groups = [];
        /** @var GroupPlayer $group */
        foreach ($user->getPlayerGroups() as $group) {
            if ($group->getGroup()->getAuthorized()) {
                $groups[] = $group->getGroup();
            }
        }

        return $groups;
    }

    /**
     * @param Group $group
     * @param User $user
     * @return bool
     */
    public function groupUserConfirmed(Group $group, User $user)
    {
        return $this->groupManager->checkIfUserIsConfirmed($group, $user);
    }

    /**
     * @return string
     */
    public function getName()
    {
        return 'group_extension';
    }

}