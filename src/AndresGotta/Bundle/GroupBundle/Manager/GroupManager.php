<?php

namespace AndresGotta\Bundle\GroupBundle\Manager;

use AndresGotta\Bundle\GroupBundle\Entity\Group;
use AndresGotta\Bundle\GroupBundle\Entity\GroupPlayer;
use AndresGotta\Bundle\GroupBundle\ValueObject\PlayerStatuses;
use Doctrine\ORM\EntityManager;
use Symfony\Component\Security\Core\User\UserInterface;
use WebFactory\Bundle\UserBundle\Entity\User;

/**
 * Class GroupManager
 * @package AndresGotta\Bundle\GroupBundle\Manager
 */
class GroupManager
{
    /**
     * @var EntityManager
     */
    protected $em;

    /**
     * GroupManager constructor.
     * @param EntityManager $em
     */
    public function __construct(EntityManager $em)
    {
        $this->em = $em;
    }

    /**
     * @param UserInterface $user
     * @return GroupPlayer
     */
    public function generate(UserInterface $user)
    {
        $userGroup = new GroupPlayer();
        $userGroup
            ->setPlayer($user)
            ->setStatus(null)
            ->setIsAdmin(true)
        ;

        return $userGroup;
    }

    /**
     * @param Group $group
     * @param User $user
     * @return bool
     */
    public function checkUserPaticipation(Group $group, User $user)
    {
        $groups = $user->getPlayerGroups();
        /** @var GroupPlayer $userGroup */
        foreach ($groups as $userGroup) {
            if ($group === $userGroup->getGroup()) {
                return true;
            }
        }

        return false;
    }

    /**
     * @param Group $group
     * @param User $user
     * @return bool
     */
    public function checkIfUserIsConfirmed(Group $group, User $user)
    {
        $groups = $user->getPlayerGroups();
        /** @var GroupPlayer $userGroup */
        foreach ($groups as $userGroup) {
            if ($group === $userGroup->getGroup()) {
                return $userGroup->isConfirmed() && $userGroup->getStatus() === PlayerStatuses::CONFIRMED;
            }
        }

        return false;
    }

    /**
     * @param Group $group
     * @param User $user
     */
    public function toggleUserParticipation(Group $group, User $user)
    {
        $groups = $user->getPlayerGroups();
        /** @var GroupPlayer $userGroup */
        foreach ($groups as $userGroup) {
            if ($group->getId() === $userGroup->getGroup()->getId()) {

                if (!$userGroup->isConfirmed()) {
                    $userGroup->setConfirmed(true);
                    $userGroup->setStatus(PlayerStatuses::CONFIRMED);
                } else {
                    // Si el usuario ya confirmó la solicitud, pero ahora quiere darse de baja del grupo
                    if ($userGroup->getStatus() === PlayerStatuses::CONFIRMED) {
                        $userGroup->setStatus(PlayerStatuses::UNCONFIRMED);
                    } else {
                        $userGroup->setStatus(PlayerStatuses::CONFIRMED);
                    }
                }

                $this->em->flush($userGroup);
            }
        }
    }

    /**
     * @param Group $group
     * @param GroupPlayer $user
     * @param $status
     * @throws \Exception
     */
    public function toggleUserStatus(Group $group, GroupPlayer $user, $status)
    {
        if ($group === $user->getGroup()) {
            $user->setStatus($status);

            $this->em->flush($user);
        } else {
            // TODO lanzar excepciones de dominio
            throw new \Exception('User not found in group');
        }
    }

    /**
     * @param Group $group
     * @param User $user
     */
    public function requestAdmin(Group $group, User $user)
    {
        $player = $this->em->getRepository('AndresGottaGroupBundle:GroupPlayer')->findOneBy(['group' => $group, 'player' => $user]);

        $player->setWannaBeAdmin(true);

        $this->em->flush();
    }
}
