<?php

namespace AndresGotta\Bundle\GroupBundle\Controller\Frontend;

use AndresGotta\Bundle\GroupBundle\Entity\Group;
use AndresGotta\Bundle\GroupBundle\Entity\GroupPlayer;
use AndresGotta\Bundle\GroupBundle\Form\Type\GroupType;
use AndresGotta\Bundle\GroupBundle\ValueObject\PlayerStatuses;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Security;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Template;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use WebFactory\Bundle\UserBundle\Entity\User;

/**
 * Class GroupController
 * @package AndresGotta\Bundle\GroupBundle\Controller\Frontend
 */
class GroupController extends Controller
{
    /**
     * @Route("/", name="frontend_group_index")
     * @Method("GET")
     * @Template()
     */
    public function indexAction()
    {
        /** @var User $user */
        $user = $this->getUser();

        return ['groups' => $user->getPlayerGroups()];
    }

    /**
     * @Route("/{id}/show", name="frontend_group_show")
     * @Method("GET")
     * @Template()
     */
    public function showAction(Group $group)
    {
        if (!$this->get('gotta_group.manager')->checkUserPaticipation($group, $this->getUser())) {
            $this->addFlash('error', $this->get('translator')->trans('User is not in group'));

            return $this->redirectToRoute('frontend_dashboard');
        }

        return compact('group');
    }

    /**
     * @Route("/create", name="frontend_group_create")
     * @Method("GET|POST")
     * @Template()
     */
    public function createAction(Request $request)
    {
        $group = new Group();
        $form = $this->createGroupForm($group);

        if ($request->getMethod() === Request::METHOD_POST) {
            $form->handleRequest($request);
            if ($form->isValid()) {
                $em = $this->getDoctrine()->getManager();
                $player = $this->get('gotta_group.manager')->generate($this->getUser());
                $group->addPlayer($player);

                foreach ($group->getPlayers() as $groupPlayer) {
                    if (!$groupPlayer->getPlayer()) {
                        $group->removePlayer($groupPlayer);
                    }
                }

                $em->persist($group);
                $em->flush();

                $this->addFlash('success', $this->get('translator')->trans('flash.message.generic.created'));

                return $this->redirectToRoute('frontend_group_index');
            }
        }

        return ['form' => $form->createView()];
    }

    /**
     * @Route("/{id}/edit", name="frontend_group_edit")
     * @Method("GET|PUT")
     * @Template("@AndresGottaGroup/Frontend/Group/edit.html.twig")
     */
    public function editAction(Request $request, $id)
    {
        $repo = $this->getDoctrine()->getRepository('AndresGottaGroupBundle:Group');
        $group = $repo->findGroupWithAllPlayers($this->getUser(), $id);

        $form = $this->editGroupForm($group);

        if ($request->getMethod() === Request::METHOD_PUT) {
            $form->handleRequest($request);
            if ($form->isValid()) {
                foreach ($group->getPlayers() as $groupPlayer) {
                    if (!$groupPlayer->getPlayer()) {
                        $group->removePlayer($groupPlayer);
                    }
                }

                $this->getDoctrine()->getManager()->flush();
                $this->addFlash('success', $this->get('translator')->trans('flash.message.generic.updated'));

                return $this->redirectToRoute('frontend_group_index');
            }
        }

        return ['form' => $form->createView()];
    }

    /**
     * @param Group $group
     * @return \Symfony\Component\Form\Form
     */
    private function createGroupForm(Group $group)
    {
        $form = $this->createForm(new GroupType(), $group, [
            'action' => $this->generateUrl('frontend_group_create'),
            'method' => 'POST'
        ]);
        $form->add('Guardar', 'submit');

        return $form;
    }

    /**
     * @param Group $group
     * @return \Symfony\Component\Form\Form
     */
    private function editGroupForm(Group $group)
    {
        $form = $this->createForm(new GroupType(), $group, [
            'action' => $this->generateUrl('frontend_group_edit',['id' => $group->getId()]),
            'method' => 'PUT'
        ]);
        $form->add('Guardar', 'submit');

        return $form;
    }

    /**
     * @Route("/{group}/view/{player}", name="frontend_group_view_user")
     * @Method("GET")
     * @Template("@AndresGottaHomepage/Frontend/Homepage/dashboard_group.html.twig")
     * @Security("has_role('ROLE_PLAYER') and is_granted('access', group)")
     */
    public function viewUserGroupAction(Group $group, GroupPlayer $player)
    {
        if (!$group->getPlayers()->contains($player)) {
            $this->addFlash('error', $this->get('translator')->trans('Player is not member of group'));

            return $this->redirectToRoute('frontend_group_index');
        }

        if ($player->getStatus() !== PlayerStatuses::CONFIRMED) {
            $this->addFlash('error', $this->get('translator')->trans('Player must be confirmed to include in group'));

            if (in_array($this->getUser(), $group->getUserAdmins())) {
                return $this->redirectToRoute('frontend_group_index');
            } else {
                return $this->redirectToRoute('frontend_group_show', ['id' => $group->getId()]);
            }
        }

        $comparatives = $this->getDoctrine()->getRepository('AndresGottaGolfBundle:Comparative')->findBy(['highlighted' => true], ['sort' => 'desc']);

        return compact('comparatives', 'group', 'player');
    }

    /**
     * @param Request $request
     * @return JsonResponse
     *
     * @Route("/search", name="frontend_group_search_player")
     */
    public function searchPlayerAction(Request $request)
    {
        $results = new JsonResponse();
        if ($request->isXmlHttpRequest()) {
            $term = $request->get('q');
            $repo = $this->getDoctrine()->getRepository('WebFactoryUserBundle:User');

            $array = array(
                serialize(array('ROLE_PLAYER')),
                serialize(array('ROLE_STUDENT')),
                serialize(array('ROLE_CUSTOMER')),
            );

            $qb = $repo->createQueryBuilder('p');
            $qb
                ->select("p.id, CONCAT(CONCAT(profile.firstName, ', '), profile.lastName) as fullName")
                ->leftJoin('p.profile', 'profile')
//                ->where($qb->expr()->in('p.roles', $array))
                ->where($qb->expr()->orX(
                    $qb->expr()->like('profile.firstName', ':term'),
                    $qb->expr()->like('profile.lastName', ':term')
                ))->setParameter('term', '%'.$term.'%')
                ->orderBy('fullName', 'asc')
            ;

            $results->setData($qb->getQuery()->getArrayResult());
        }

        return $results;
    }

    /**
     * @Route("/{id}/participation", name="frontend_group_toggle")
     * @Method("GET")
     */
    public function toggleConfirmAction(Group $group)
    {
        $manager = $this->get('gotta_group.manager');
        if (!$manager->checkUserPaticipation($group, $this->getUser())) {
            $this->addFlash('error', $this->get('translator')->trans('Player is not member of group'));

            return $this->redirectToRoute('frontend_dashboard');
        }

        $manager->toggleUserParticipation($group, $this->getUser());
        $this->addFlash('success', $this->get('translator')->trans('Record updated!'));

        return $this->redirectToRoute('frontend_group_show', ['id' => $group->getId()]);
    }

    /**
     * @Route("/{group}/participation/{user}/{status}", name="frontend_user_group_change")
     * @Method("GET")
     */
    public function toggleUserGroupAction(Group $group, GroupPlayer $user, $status)
    {
        $manager = $this->get('gotta_group.manager');
        if (!$manager->checkUserPaticipation($group, $user->getPlayer()) || !array_key_exists($status, PlayerStatuses::getChoices())) {
            $this->addFlash('error', $this->get('translator')->trans('Player is not member of group'));

            return $this->redirectToRoute('frontend_group_index', ['group' => $group->getId()]);
        }

        $manager->toggleUserStatus($group, $user, $status);
        $this->addFlash('success', $this->get('translator')->trans('Record updated!'));

        return $this->redirectToRoute('frontend_group_index', ['id' => $group->getId()]);
    }

    /**
     * @Route("/{id}/request-admin", name="frontend_group_request_admin")
     * @Method("GET")
     */
    public function requestAdminAction(Group $group)
    {
        $manager = $this->get('gotta_group.manager');
        if (!$manager->checkUserPaticipation($group, $this->getUser())) {
            $this->addFlash('error', $this->get('translator')->trans('Player is not member of group'));

            return $this->redirectToRoute('frontend_dashboard');
        }
        if (!$manager->checkIfUserIsConfirmed($group, $this->getUser())) {
            $this->addFlash('error', $this->get('translator')->trans('Player must be confirmed to include in group'));

            if (in_array($this->getUser(), $group->getUserAdmins())) {
                return $this->redirectToRoute('frontend_group_index');
            } else {
                return $this->redirectToRoute('frontend_group_show', ['id' => $group->getId()]);
            }
        }
        if (in_array($this->getUser(), $group->getUserAdmins())) {
            $this->addFlash('error', $this->get('translator')->trans('Already admin'));

            return $this->redirectToRoute('frontend_group_index');
        }

        $manager->requestAdmin($group, $this->getUser());
        $this->addFlash('success', $this->get('translator')->trans('Admin requested'));

        return $this->redirectToRoute('frontend_group_index');
    }
}