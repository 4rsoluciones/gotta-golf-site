<?php

namespace AndresGotta\Bundle\GroupBundle\Controller\Backend;

use AndresGotta\Bundle\GroupBundle\Entity\Group;
use AndresGotta\Bundle\GroupBundle\Entity\GroupPlayer;
use AndresGotta\Bundle\GroupBundle\Form\Type\GroupType;
use APY\DataGridBundle\Grid\Action\RowAction;
use APY\DataGridBundle\Grid\Source\Entity;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Template;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;
use WebFactory\Bundle\UserBundle\Entity\User;

/**
 * Class GroupController
 * @package AndresGotta\Bundle\GroupBundle\Controller\Backend
 */
class GroupController extends Controller
{
    /**
     * @Route("/", name="backend_group_index")
     */
    public function indexAction()
    {
        $source = new Entity('AndresGottaGroupBundle:Group');

        $grid = $this->get('grid');
        $grid->setSource($source);

        $editAction = new RowAction('Authorized', 'backend_group_authorized', false, '_self', array('class' => 'btn-action glyphicons settings btn-success'));
        $grid->addRowAction($editAction);

        $deleteAction = new RowAction('Delete', 'backend_group_delete', true, '_self', array('class' => 'btn-action glyphicons remove btn-danger'));
        $grid->addRowAction($deleteAction);

        $groupPlayerRepository = $this->getDoctrine()->getRepository('AndresGottaGroupBundle:GroupPlayer');
        $wannaBeAdmins = $groupPlayerRepository->findBy(['wannaBeAdmin' => true]);

        $adminRequests = [];
        foreach ($wannaBeAdmins as $wannaBeAdmin) {
            $adminRequest['username'] = $wannaBeAdmin->getPlayer()->getUsername();
            $adminRequest['groupName'] = $wannaBeAdmin->getGroup()->getName();
            $adminRequest['groupId'] = $wannaBeAdmin->getGroup()->getId();
            $adminRequest['groupPlayerId'] = $wannaBeAdmin->getId();
            $adminRequests[] = $adminRequest;
        }

        return $grid->getGridResponse('AndresGottaGroupBundle:Backend\Group:index.html.twig', ['adminRequests' => $adminRequests]);
    }

    /**
     * @Route("/group/{id}/authorized", name="backend_group_authorized")
     * @Template("AndresGottaGroupBundle:Backend/Group:edit.html.twig")
     */
    public function authorizedAction(Request $request, Group $group)
    {
        $form = $this->createForm(new GroupType(true), $group, [
            'action' => $this->generateUrl('backend_group_authorized', ['id' => $group->getId()]),
            'method' => 'PUT'
        ]);
        $form->add('Guardar', 'submit');

        if ($request->getMethod() === Request::METHOD_PUT) {
            $form->handleRequest($request);
            if ($form->isValid()) {
                $this->getDoctrine()->getManager()->persist($group);
                $this->getDoctrine()->getManager()->flush();

                $this->addFlash('success', $this->get('translator')->trans('flash.message.generic.updated'));

                return $this->redirectToRoute('backend_group_index');
            } else {
                $this->addFlash('danger', 'Datos erroneos');
            }
        }

        return ['form' => $form->createView(), 'group' => $group];
    }

    /**
     * @Route("/group/{id}/delete", name="backend_group_delete")
     */
    public function deleteAction(Group $group)
    {
        try {
            $this->getDoctrine()->getManager()->remove($group);
            $this->getDoctrine()->getManager()->flush();

            $this->addFlash('success', $this->get('translator')->trans('flash.message.generic.delete'));
        } catch (\Exception $e) {
            $this->addFlash('danger', $e->getMessage());
        }


        return $this->redirectToRoute('backend_group_index');
    }

    /**
     * @param GroupPlayer $groupPlayer
     * @param boolean $response
     * @return \Symfony\Component\HttpFoundation\RedirectResponse
     *
     * @Route("/admin-request/{groupPlayer}/reply/{response}", name="backend_group_reply_admin_request")
     */
    public function replyAdminRequest(GroupPlayer $groupPlayer, $response)
    {
        if ($response === 'true') {
            $groupPlayer->getPlayer()->addRole(User::ROLE_GROUP_OWNER);
            $groupPlayer->setIsAdmin(true);
        }
        $groupPlayer->setWannaBeAdmin(false);

        $this->getDoctrine()->getManager()->flush();

        return $this->redirectToRoute('backend_group_index');
    }
}