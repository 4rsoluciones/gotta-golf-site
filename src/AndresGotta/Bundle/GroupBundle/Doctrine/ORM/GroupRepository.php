<?php

namespace AndresGotta\Bundle\GroupBundle\Doctrine\ORM;

use Doctrine\ORM\EntityRepository;
use Symfony\Component\Security\Core\User\UserInterface;

/**
 * Class GroupRepository
 * @package AndresGotta\Bundle\GroupBundle\Doctrine\ORM
 */
class GroupRepository extends EntityRepository
{
    /**
     * @param UserInterface $user
     * @param $groupId
     * @return mixed
     */
    public function findGroupWithAllPlayers(UserInterface $user, $groupId)
    {
        return $this->createQueryBuilder('Group1')
            ->addSelect('Players')
            ->addSelect('User')
            ->innerJoin('Group1.players', 'Players')
            ->innerJoin('Players.player', 'User')
            ->where('Group1.id = :id')->setParameter('id', $groupId)
            ->andWhere('User != :currentUser')->setParameter('currentUser', $user)
            ->getQuery()->getOneOrNullResult()
        ;
    }
}