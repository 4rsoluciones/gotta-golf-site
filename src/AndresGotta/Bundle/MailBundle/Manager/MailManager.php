<?php

namespace AndresGotta\Bundle\MailBundle\Manager;

use Doctrine\ORM\EntityManager;
use Symfony\Bridge\Twig\TwigEngine;
use WebFactory\Bundle\OrderBundle\Entity\Order;

class MailManager
{
    private $mailer;
    private $entityManager;
    private $templating;

    public function __construct(\Swift_Mailer $mailer, EntityManager $entityManager, TwigEngine $templating, $from)
    {
        $this->mailer = $mailer;
        $this->entityManager = $entityManager;
        $this->templating = $templating;
        $this->from = $from;
    }

    public function notifyPaymentSuccess(Order $order)
    {
        $services = $this->entityManager->getRepository('AndresGottaServiceBundle:Service')->getAllServices();
        $amexRegistryRepository = $this->entityManager->getRepository('AndresGottaSaleBundle:AmexRegistry');
        $mastercardRegistryRepository = $this->entityManager->getRepository('AndresGottaSaleBundle:MastercardRegistry');

        foreach ($services as $service) {
            if ($service->getName() == $order->getItems()[0]->getProduct()) {
                if ($debitDues = $service->getDebitDues()) {
                    $orderTotal = number_format($order->getTotal()/$debitDues, 2);
                    if ($order->getDebitCard()->getProvider() == 1) {
                        $debitDuesLeft = $amexRegistryRepository->getRegistryByVoucherNumber($order->getId())->getDebitDues();
                        $debitDuesPartial = ($debitDues-$debitDuesLeft+1).'/'.$debitDues;
                    } else if ($order->getDebitCard()->getProvider() == 2) {
                        $debitDuesLeft = $mastercardRegistryRepository->getRegistryByReferenceNumber($order->getId())->getDebitDues();
                        $debitDuesPartial = ($debitDues-$debitDuesLeft+1).'/'.$debitDues;
                    }
                    $reason = 'pago de la cuota '.$debitDuesPartial.' del servicio "'.$service->getName().'"';
                }
                else {
                    $orderTotal = number_format($order->getTotal(), 2);
                    $reason = 'su suscripción mensual al servicio "'.$service->getName().'"';
                }
            }
        }

        $subject = 'Confirmación de pago';
        $to = $order->getUser()->getEmail();
        $body = $this->templating->render('AndresGottaMailBundle::Payment/success.html.twig', [
            'orderTotal' => $orderTotal,
            'reason' => $reason,
        ]);

        $this->sendMail($subject, $to, $body);
    }

    public function notifyPaymentRejected(Order $order)
    {
        $services = $this->entityManager->getRepository('AndresGottaServiceBundle:Service')->getAllServices();
        $amexRegistryRepository = $this->entityManager->getRepository('AndresGottaSaleBundle:AmexRegistry');
        $mastercardRegistryRepository = $this->entityManager->getRepository('AndresGottaSaleBundle:MastercardRegistry');

        foreach ($services as $service) {
            if ($service->getName() == $order->getItems()[0]->getProduct()) {
                if ($debitDues = $service->getDebitDues()) {
                    $orderTotal = number_format($order->getTotal()/$debitDues, 2);
                    if ($order->getDebitCard()->getProvider() == 1) {
                        $debitDuesLeft = $amexRegistryRepository->getRegistryByVoucherNumber($order->getId())->getDebitDues();
                        $debitDuesPartial = ($debitDues-$debitDuesLeft+1).'/'.$debitDues;
                    } else if ($order->getDebitCard()->getProvider() == 2) {
                        $debitDuesLeft = $mastercardRegistryRepository->getRegistryByReferenceNumber($order->getId())->getDebitDues();
                        $debitDuesPartial = ($debitDues-$debitDuesLeft+1).'/'.$debitDues;
                    }
                    $reason = 'la cuota '.$debitDuesPartial.' del servicio "'.$service->getName().'"';
                }
                else {
                    $orderTotal = number_format($order->getTotal(), 2);
                    $reason = 'su suscripción mensual al servicio "'.$service->getName().'"';
                }
            }
        }

        $subject = 'Pago rechazado';
        $to = $order->getUser()->getEmail();
        $body = $this->templating->render('AndresGottaMailBundle::Payment/rejected.html.twig', [
            'orderTotal' => $orderTotal,
            'reason' => $reason,
        ]);

        $this->sendMail($subject, $to, $body);
    }

    public function notifyServiceActivation(Order $order)
    {
        $subject = 'Su servicio ha sido activado';
        $to = $order->getUser()->getEmail();
        $body = $this->templating->render('AndresGottaMailBundle::Service/activation.html.twig', [
            'service' => $order->getItems()[0]->getProduct()->getName(),
        ]);

        $this->sendMail($subject, $to, $body);
    }

    public function notifyServiceExpired($order)
    {
        $subject = 'Su servicio ha expirado';
        $to = $order->getUser()->getEmail();
        $body = $this->templating->render('AndresGottaMailBundle::Service/expired.html.twig', [
            'service' => $order->getItems()[0]->getProduct()->getName(),
        ]);

        $this->sendMail($subject, $to, $body);
    }

    private function sendMail($subject, $to, $body)
    {
        $message = \Swift_Message::newInstance()
            ->setSubject($subject)
            ->setFrom($this->from)
            ->setTo($to)
            ->setBody($body, 'text/html')
        ;
        $this->mailer->send($message);
    }
}
