<?php

namespace AndresGotta\Bundle\VideoBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use Symfony\Bridge\Doctrine\Validator\Constraints\UniqueEntity;
use Symfony\Component\Validator\Constraints as Assert;
use APY\DataGridBundle\Grid\Mapping as Grid;

/**
 * Video
 *
 * @ORM\Table(name="videos")
 * @ORM\Entity
 * @UniqueEntity("url")
 */
class Video
{
    /**
     * @var integer
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     * @Grid\Column(filterable=false);
     */
    private $id;

    /**
     * @var string
     *
     * @ORM\Column(name="type", type="string", length=255)
     * @Assert\NotBlank
     * @Grid\Column(filterable=false, visible=false);
     */
    private $type = 'youtube';

    /**
     * @var string
     *
     * @ORM\Column(name="url", type="string", length=255)
     * @Assert\NotBlank
     * @Assert\Url
     * @Grid\Column(title="Url")
     */
    private $url;
    
    /**
     *
     * @var Category
     * @ORM\ManyToOne(targetEntity="Category", inversedBy="videos", cascade={"persist"}, fetch="LAZY")
     * @Assert\NotBlank
     * @Grid\Column(title="Category", field="category.name")
     */
    private $category;

    /**
     * Get id
     *
     * @return integer 
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set type
     *
     * @param string $type
     * @return Video
     */
    public function setType($type)
    {
        $this->type = $type;

        return $this;
    }

    /**
     * Get type
     *
     * @return string 
     */
    public function getType()
    {
        return $this->type;
    }

    /**
     * Set url
     *
     * @param string $url
     * @return Video
     */
    public function setUrl($url)
    {
        $this->url = $url;

        return $this;
    }

    /**
     * Get url
     *
     * @return string 
     */
    public function getUrl()
    {
        return $this->url;
    }

    /**
     * Set category
     *
     * @param \AndresGotta\Bundle\VideoBundle\Entity\Category $category
     * @return Video
     */
    public function setCategory(\AndresGotta\Bundle\VideoBundle\Entity\Category $category = null)
    {
        $this->category = $category;

        return $this;
    }

    /**
     * Get category
     *
     * @return \AndresGotta\Bundle\VideoBundle\Entity\Category 
     */
    public function getCategory()
    {
        return $this->category;
    }
}
