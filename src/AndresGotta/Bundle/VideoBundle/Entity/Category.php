<?php

namespace AndresGotta\Bundle\VideoBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use Symfony\Bridge\Doctrine\Validator\Constraints\UniqueEntity;
use Symfony\Component\Validator\Constraints as Assert;
use APY\DataGridBundle\Grid\Mapping as Grid;

/**
 * Category
 *
 * @ORM\Table(name="video_categories")
 * @ORM\Entity
 * @UniqueEntity("name")
 * @ORM\HasLifecycleCallbacks
 */
class Category
{
    /**
     * @var integer
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     * @Grid\Column(filterable=false);
     */
    private $id;

    /**
     * @var string
     *
     * @ORM\Column(name="name", type="string", length=255)
     * @Assert\NotBlank
     * @Grid\Column(title="Name")
     * @Assert\Length(
     *      min = "2",
     *      max = "30"
     * )
     */
    private $name;
    
    /**
     *
     * @var ArrayCollection
     * @ORM\OneToMany(targetEntity="Video", mappedBy="category", cascade={"persist"})
     */
    private $videos;


    /**
     * Get id
     *
     * @return integer 
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set name
     *
     * @param string $name
     * @return Category
     */
    public function setName($name)
    {
        $this->name = $name;

        return $this;
    }

    /**
     * Get name
     *
     * @return string 
     */
    public function getName()
    {
        return $this->name;
    }
    /**
     * Constructor
     */
    public function __construct()
    {
        $this->videos = new \Doctrine\Common\Collections\ArrayCollection();
    }

    /**
     * Add videos
     *
     * @param \AndresGotta\Bundle\VideoBundle\Entity\Video $videos
     * @return Category
     */
    public function addVideo(\AndresGotta\Bundle\VideoBundle\Entity\Video $videos)
    {
        $this->videos[] = $videos;

        return $this;
    }

    /**
     * Remove videos
     *
     * @param \AndresGotta\Bundle\VideoBundle\Entity\Video $videos
     */
    public function removeVideo(\AndresGotta\Bundle\VideoBundle\Entity\Video $videos)
    {
        $this->videos->removeElement($videos);
    }

    /**
     * Get videos
     *
     * @return \Doctrine\Common\Collections\Collection 
     */
    public function getVideos()
    {
        return $this->videos;
    }
    
    /**
     * 
     * @return string
     */
    public function __toString()
    {
        return $this->name;
    }

    /**
     * @ORM\PreRemove()
     */
    public function preRemove()
    {
        if (!$this->videos->isEmpty()) {
            throw new \LogicException('The category has videos and can not be deleted. Please, first remove the videos.');
        }
    }
}
