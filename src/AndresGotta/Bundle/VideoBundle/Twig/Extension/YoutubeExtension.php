<?php

namespace AndresGotta\Bundle\VideoBundle\Twig\Extension;

use Twig_Extension;
use Twig_SimpleFilter;

class YoutubeExtension extends Twig_Extension
{

    public function getFilters()
    {
        return array(
            new Twig_SimpleFilter('get_youtube_id', array($this, 'getYoutubeId')),
        );
    }

    public function getName()
    {
        return 'andresgotta.youtube.extension';
    }

    public function getYoutubeId($text)
    {
        $text = preg_replace('~
        # Match non-linked youtube URL in the wild. (Rev:20111012)
        https?://         # Required scheme. Either http or https.
        (?:[0-9A-Z-]+\.)? # Optional subdomain.
        (?:               # Group host alternatives.
          youtu\.be/      # Either youtu.be,
        | youtube\.com    # or youtube.com followed by
          \S*             # Allow anything up to VIDEO_ID,
          [^\w\-\s]       # but char before ID is non-ID char.
        )                 # End host alternatives.
        ([\w\-]{11})      # $1: VIDEO_ID is exactly 11 chars.
        (?=[^\w\-]|$)     # Assert next char is non-ID or EOS.
        (?!               # Assert URL is not pre-linked.
          [?=&+%\w]*      # Allow URL (query) remainder.
          (?:             # Group pre-linked alternatives.
            [\'"][^<>]*>  # Either inside a start tag,
          | </a>          # or inside <a> element text contents.
          )               # End recognized pre-linked alts.
        )                 # End negative lookahead assertion.
        [?=&+%\w-]*        # Consume any URL (query) remainder.
        ~ix', '$1', $text);

        return $text;
    }

}
