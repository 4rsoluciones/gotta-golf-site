<?php

namespace AndresGotta\Bundle\VideoBundle\Controller\Frontend;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Template;

/**
 * Video controller.
 *
 * @Route("/")
 */
class VideoController extends Controller
{

    /**
     * Lists all Video entities.
     *
     * @Route("/ours-videos/{category}", name="ours_videos")
     * @Method("GET")
     * @Template()
     */
    public function indexAction(\AndresGotta\Bundle\VideoBundle\Entity\Category $category = null)
    {
        $qb = $this->getDoctrine()->getRepository('AndresGottaVideoBundle:Video')
                ->createQueryBuilder('Video')
                ->orderBy('Video.id', 'desc');
                
        if ($category) {
            $qb->where('Video.category = :category')
               ->setParameter('category', $category);
        }
        
        
        
        $videos = $qb->getQuery()->getResult();
        
        $qb = $this->getDoctrine()->getRepository('AndresGottaVideoBundle:Category')
                ->createQueryBuilder('Category')
                ->orderBy('Category.name', 'asc');
        
        $categories = $qb->getQuery()->getResult();

        // Verificamos suscripción al servicio #7
        $aa = $this->get('web_factory_user.gotta_center_api')->getServicesStatus($this->getUser()->getEmail(), $this->getUser()->getAccessToken());

        $return = false;
        foreach ($aa as $key => $value) {
            if ($value['service_id'] == 7)
                $return = true;
        }

        return compact('category', 'videos', 'categories', 'return');
    }
    
}
