<?php

namespace AndresGotta\Bundle\GolfBundle\Specification;

use AndresGotta\Bundle\GolfBundle\ValueObject\Country;

class CountryIsValid
{
    /**
     * @param Country $country
     * @return bool
     */
    public function isSatisfiedBy(Country $country)
    {
        return in_array((string)$country, Country::getChoices());
    }
}