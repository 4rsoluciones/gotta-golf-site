<?php

namespace AndresGotta\Bundle\GolfBundle\Specification;

use AndresGotta\Bundle\GolfBundle\ValueObject\Email;

class EmailAddressIsValid
{
    /**
     * @param Email $email
     * @return bool
     */
    public function isSatisfiedBy(Email $email)
    {
        return filter_var($email->getAddress(), FILTER_VALIDATE_EMAIL) !== false;
    }
}