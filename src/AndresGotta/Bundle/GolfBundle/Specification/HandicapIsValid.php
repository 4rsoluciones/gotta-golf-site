<?php

namespace AndresGotta\Bundle\GolfBundle\Specification;

use AndresGotta\Bundle\GolfBundle\ValueObject\Handicap;

class HandicapIsValid
{
    /**
     * @param Handicap $handicap
     * @return bool
     */
    public function isSatisfiedBy(Handicap $handicap)
    {
        return in_array($handicap->getRawValue(), range(36, -5, -1));
    }
}