<?php

namespace AndresGotta\Bundle\GolfBundle\Specification;

use AndresGotta\Bundle\GolfBundle\ValueObject\PlayerCondition;

class PlayerConditionIsValid
{
    /**
     * @param PlayerCondition $condition
     * @return bool
     */
    public function isSatisfiedBy(PlayerCondition $condition)
    {
        return in_array((string)$condition, PlayerCondition::getChoices());
    }
}