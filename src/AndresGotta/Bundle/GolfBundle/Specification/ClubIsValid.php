<?php

namespace AndresGotta\Bundle\GolfBundle\Specification;

use AndresGotta\Bundle\GolfBundle\ValueObject\Club;

class ClubIsValid
{
    /**
     * @param Club $club
     * @return bool
     */
    public function isSatisfiedBy(Club $club)
    {
        return in_array((string)$club, Club::getChoices());
    }
}