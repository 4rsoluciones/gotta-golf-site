<?php

namespace AndresGotta\Bundle\GolfBundle\DataFixtures\ORM;

use AndresGotta\Bundle\GolfBundle\Comparative\RenderStrategy\LegendValue;
use AndresGotta\Bundle\GolfBundle\Entity\Comparative;
use AndresGotta\Bundle\GolfBundle\ValueObject\ComparativeLabels;
use AndresGotta\Bundle\GolfBundle\ValueObject\ComparativeType;
use Doctrine\Common\DataFixtures\AbstractFixture;
use Doctrine\Common\DataFixtures\OrderedFixtureInterface;
use Doctrine\Common\Persistence\ObjectManager;
use Symfony\Component\DependencyInjection\ContainerAwareInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Symfony\Component\Yaml\Yaml;
use Symfony\Component\Validator\Validation;

/**
 * Class LoadComparatives
 * @package AndresGotta\Bundle\GolfBundle\DataFixtures\ORM
 */
class LoadComparatives extends AbstractFixture implements ContainerAwareInterface, OrderedFixtureInterface
{
    /**
     * @var ContainerInterface
     */
    private $container;

    /**
     * @inheritdoc
     */
    public function setContainer(ContainerInterface $container = null)
    {
        $this->container = $container;
    }

    /**
     * @inheritdoc
     */
    public function load(ObjectManager $manager)
    {
        $manager = $this->container->get('doctrine.orm.entity_manager');
        $fixturesPath = realpath(dirname(__FILE__));
        $fixtureComparatives = Yaml::parse(file_get_contents($fixturesPath. '/'. 'comparatives.yml'));
        $validator = Validation::createValidatorBuilder()->enableAnnotationMapping()->getValidator();

        foreach ($fixtureComparatives as $comparative) {
            $entity = new Comparative();
            $entity->setSlug($comparative['slug']);
            $entity->setDescription($comparative['description']);
            $entity->setObservation($comparative['observation']);
            $entity->setName($comparative['name']);
            $entity->setHighlighted(true);

            if (array_key_exists('type', $comparative)) {
                if ($comparative['type'] == 1) {
                    $entity->setType(ComparativeType::DONUT_SINGLE_TYPE);
                    $entity->setPrimaryLabel(ComparativeLabels::GOOD_LABEL);
                    $entity->setSecondaryLabel(ComparativeLabels::BAD_LABEL);
                }

                if ($comparative['type'] == 2) {
                    $entity->setType(ComparativeType::BAR_GRAPHIC_SINGLE_TYPE);
                    $entity->setPrimaryLabel(ComparativeLabels::PLAYER_LABEL);
                    $entity->setSecondaryLabel(ComparativeLabels::GROUP_LABEL);
                }

                if ($comparative['type'] == 3) {
                    $entity->setType(ComparativeType::DONUT_DOBLE_TYPE);
                    $entity->setPrimaryLabel(ComparativeLabels::GOOD_LABEL);
                    $entity->setSecondaryLabel(ComparativeLabels::BAD_LABEL);
                }

                if ($comparative['type'] == 4) {
                    $entity->setType(ComparativeType::DONUT_SINGLE_TYPE);
                    $entity->setPrimaryLabel(LegendValue::POSITIVE_LEGEND);
                    $entity->setSecondaryLabel(LegendValue::NEGATIVE_LEGEND);
                }

            } else {
                $entity->setType(ComparativeType::DONUT_SINGLE_TYPE);
                $entity->setPrimaryLabel(ComparativeLabels::GOOD_LABEL);
                $entity->setSecondaryLabel(ComparativeLabels::BAD_LABEL);
            }

            if (array_key_exists('max', $comparative)) {
                $entity->setMaxLength($comparative['max']);
            }

            $errors = $validator->validate($entity);
            if ($errors->count() == 0) {
                $manager->persist($entity);
            }
        }

        $manager->flush();
    }

    public function getOrder()
    {
        return 10;
    }


}
