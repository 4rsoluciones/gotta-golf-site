<?php

namespace AndresGotta\Bundle\GolfBundle\DataFixtures\ORM;

use AndresGotta\Bundle\GolfBundle\Entity\Field;
use AndresGotta\Bundle\GolfBundle\Entity\Round;
use AndresGotta\Bundle\GolfBundle\Entity\PreCompetitiveQuestionnaire;
use AndresGotta\Bundle\GolfBundle\Entity\PostCompetitiveQuestionnaire;
use AndresGotta\Bundle\GolfBundle\Entity\Strike;
use AndresGotta\Bundle\GolfBundle\ValueObject\BallFlight;
use AndresGotta\Bundle\GolfBundle\ValueObject\Distance;
use AndresGotta\Bundle\GolfBundle\ValueObject\FieldPar;
use AndresGotta\Bundle\GolfBundle\ValueObject\FieldType;
use AndresGotta\Bundle\GolfBundle\ValueObject\GameCondition;
use AndresGotta\Bundle\GolfBundle\ValueObject\HoleDesign;
use AndresGotta\Bundle\GolfBundle\ValueObject\LandSlope;
use AndresGotta\Bundle\GolfBundle\ValueObject\PlayerCondition;
use AndresGotta\Bundle\GolfBundle\ValueObject\Result;
use AndresGotta\Bundle\GolfBundle\ValueObject\RoundType;
use AndresGotta\Bundle\GolfBundle\ValueObject\StrikeLie;
use AndresGotta\Bundle\GolfBundle\ValueObject\StrikeType;
use AndresGotta\Bundle\GolfBundle\ValueObject\TournamentType;
use Doctrine\Common\DataFixtures\AbstractFixture;
use Doctrine\Common\DataFixtures\OrderedFixtureInterface;
use Doctrine\Common\Persistence\ObjectManager;
use Symfony\Component\DependencyInjection\ContainerAwareInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;

class Load18RoundsHoleInOne extends AbstractFixture implements OrderedFixtureInterface, ContainerAwareInterface
{
    /**
     * @var ContainerInterface
     */
    private $container;

    /**
     * @param ContainerInterface|null $container
     */
    public function setContainer(ContainerInterface $container = null)
    {
        $this->container = $container;
    }

    /**
     * {@inheritDoc}
     */
    public function load(ObjectManager $manager)
    {
        foreach (range(1,20) as $j) {
            $round = new Round();
            $round->setPlayerCondition(PlayerCondition::PROFESSIONAL);
            $round->setHandicap(null);
            $round->setTour($this->getReference('tour-1'));
            $round->setRoundType(RoundType::TOURNAMENT);
            $round->setTournamentType(TournamentType::HOLES_72);
            $round->setRoundNumber($j);
            $round->setGameCondition(GameCondition::WINDY);
            $field = new Field();
            $field->setHoleCount(18);
            $field->setCountry('Argentina');
            $field->setSportClubName('18');
            $field->setPar(FieldPar::EQUAL_70);
            $field->setRoute('Amarillo');
            $field->setTopographicMovements(FieldType::MINOR);
            $field->setStartPoint('xxx');
            $field->setType('normal');
            $round->setField($field);
            $round->setPlayerConfirmed(true);

            $preCompetitiveQuestionnaire = new PreCompetitiveQuestionnaire;
            $preCompetitiveQuestionnaire->setRound($round);
            $preCompetitiveQuestionnaire->setIsWarmedUp(false);
            $preCompetitiveQuestionnaire->setHasPracticed(false);
            $preCompetitiveQuestionnaire->setWantsToPlay(false);
            $preCompetitiveQuestionnaire->setHasStrategy(false);
            $round->setPreCompetitiveQuestionnaire($preCompetitiveQuestionnaire);

            $postCompetitiveQuestionnaire = new PostCompetitiveQuestionnaire;
            $postCompetitiveQuestionnaire->setRound($round);
            $postCompetitiveQuestionnaire->setFeelingStatus('Positive');
            $round->setPostCompetitiveQuestionnaire($postCompetitiveQuestionnaire);

            $this->getReference('user-player')->addRound($round);

            $this->container->get('round_persister')->save($round);

            foreach (range(1, 18) as $i) {
                $hole = $round->createNewHole();

                $hole->setDesign(array_rand(HoleDesign::getChoices()));
                $hole->setNumber($i);
                $hole->setPar(4);
                $hole->setPenalityShotsCount(rand(0, 2));
                $hole->setDescription('A simple description of the hole');
                $hole->setRound($round);

                $strike = new Strike();
                $hole->addStrike($strike);
                $strike->setType(StrikeType::TYPE_FIRST);
                $strike->setDistance(Distance::LESS_THAN_100_YDS);
                $strike->setResult(Result::HOLE_OUT);
                $strike->setBallFlight(BallFlight::RIGHT_TO_LEFT);
                $strike->setStrikeLies([StrikeLie::ON_CLIMB]);
                $strike->setLandSlopes([LandSlope::ON_DESCEND]);

                $errors = $this->container->get('validator')->validate($hole);
                if ($errors->count()) {
                    var_dump((string)$errors); exit;
                }

                $this->container->get('hole_persister')->save($hole);
            }
        }
    }

    /**
     * {@inheritDoc}
     */
    public function getOrder()
    {
        return 3;
    }

}
