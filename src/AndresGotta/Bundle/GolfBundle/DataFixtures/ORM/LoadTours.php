<?php

namespace AndresGotta\Bundle\GolfBundle\DataFixtures\ORM;

use Closure;
use DateTime;
use Doctrine\Common\DataFixtures\AbstractFixture;
use Doctrine\Common\DataFixtures\OrderedFixtureInterface;
use Doctrine\Common\Persistence\ObjectManager;
use AndresGotta\Bundle\GolfBundle\Entity\Tour;

class LoadTours extends AbstractFixture implements OrderedFixtureInterface
{

    /**
     * {@inheritDoc}
     */
    public function load(ObjectManager $manager)
    {

        $entity1 = new Tour('Tour regional');
        $manager->persist($entity1);
        
        $entity2 = new Tour('Tour Nacional');
        $manager->persist($entity2);
        
        $entity3 = new Tour('PGA Latinoamericano');
        $manager->persist($entity3);
        
        $entity4 = new Tour('Tour 2º nivel Europeo');
        $manager->persist($entity4);
        
        $entity5 = new Tour('Tour 1º Nivel Europeo');
        $manager->persist($entity5);
        
        $entity6 = new Tour('Tour Senior Europeo');
        $manager->persist($entity6);
        
        $entity7 = new Tour('Tour 2º nivel EEUU');
        $manager->persist($entity7);
        
        $entity8 = new Tour('Tour 1º Nivel EEUU');
        $manager->persist($entity8);
        
        $entity9 = new Tour('Tour Senior EEUU');
        $manager->persist($entity9);
        
        $entity10 = new Tour('LPGA');
        $manager->persist($entity10);

        $manager->flush();

        $this->addReference('tour-1', $entity1);
        $this->addReference('tour-2', $entity2);
        $this->addReference('tour-3', $entity3);
    }

    /**
     * {@inheritDoc}
     */
    public function getOrder()
    {
        return 1;
    }

}