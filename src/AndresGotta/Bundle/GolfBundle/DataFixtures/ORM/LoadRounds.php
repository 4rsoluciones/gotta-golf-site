<?php

namespace AndresGotta\Bundle\GolfBundle\DataFixtures\ORM;

use AndresGotta\Bundle\GolfBundle\Entity\Field;
use AndresGotta\Bundle\GolfBundle\Entity\Round;
use AndresGotta\Bundle\GolfBundle\Entity\PreCompetitiveQuestionnaire;
use AndresGotta\Bundle\GolfBundle\Entity\PostCompetitiveQuestionnaire;
use AndresGotta\Bundle\GolfBundle\ValueObject\FieldPar;
use AndresGotta\Bundle\GolfBundle\ValueObject\FieldType;
use AndresGotta\Bundle\GolfBundle\ValueObject\GameCondition;
use AndresGotta\Bundle\GolfBundle\ValueObject\PlayerCondition;
use AndresGotta\Bundle\GolfBundle\ValueObject\RoundType;
use AndresGotta\Bundle\GolfBundle\ValueObject\TournamentType;
use Doctrine\Common\DataFixtures\AbstractFixture;
use Doctrine\Common\DataFixtures\OrderedFixtureInterface;
use Doctrine\Common\Persistence\ObjectManager;
use Symfony\Component\DependencyInjection\ContainerAwareInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;

class LoadRounds extends AbstractFixture implements OrderedFixtureInterface, ContainerAwareInterface
{
    const COUNT = 10;
    /**
     * @var ContainerInterface
     */
    private $container;

    /**
     * @param ContainerInterface|null $container
     */
    public function setContainer(ContainerInterface $container = null)
    {
        $this->container = $container;
    }

    /**
     * {@inheritDoc}
     */
    public function load(ObjectManager $manager)
    {
        $round = new Round();
        $round->setPlayerCondition(PlayerCondition::PROFESSIONAL);
        $round->setHandicap(null);
        $round->setTour($this->getReference('tour-1'));
        $round->setRoundType(RoundType::TOURNAMENT);
        $round->setTournamentType(TournamentType::HOLES_72);
        $round->setRoundNumber(4);
        $round->setGameCondition(GameCondition::WINDY);
        $field = new Field();
        $field->setHoleCount(18);
        $field->setCountry('Argentina');
        $field->setSportClubName('18');
        $field->setPar(18);
        $field->setRoute(18);
        $field->setTopographicMovements(18);
        $field->setStartPoint(18);
        $field->setType(18);
        $round->setField($field);
//        $round->setFieldType(FieldType::MODERATE);
//        $round->setFieldPar(FieldPar::MORE_72);
//        $round->setHoleCount(18);
        $round->setPlayerConfirmed(true);

        $preCompetitiveQuestionnaire = new PreCompetitiveQuestionnaire;
        $preCompetitiveQuestionnaire->setRound($round);
        $preCompetitiveQuestionnaire->setIsWarmedUp(false);
        $preCompetitiveQuestionnaire->setHasPracticed(false);
        $preCompetitiveQuestionnaire->setWantsToPlay(false);
        $preCompetitiveQuestionnaire->setHasStrategy(false);
        $round->setPreCompetitiveQuestionnaire($preCompetitiveQuestionnaire);

        $postCompetitiveQuestionnaire = new PostCompetitiveQuestionnaire;
        $postCompetitiveQuestionnaire->setRound($round);
        $postCompetitiveQuestionnaire->setFeelingStatus('Positive');
        $round->setPostCompetitiveQuestionnaire($postCompetitiveQuestionnaire);

        $this->getReference('user-player')->addRound($round);

        $this->container->get('round_persister')->save($round);
        $this->addReference("round", $round);


        $round = new Round();
        $round->setPlayerCondition(PlayerCondition::PROFESSIONAL);
        $round->setHandicap(null);
        $round->setTour($this->getReference('tour-1'));
        $round->setRoundType(RoundType::TOURNAMENT);
        $round->setTournamentType(TournamentType::HOLES_72);
        $round->setRoundNumber(3);
        $round->setGameCondition(GameCondition::WINDY);
        $field = new Field();
        $field->setHoleCount(18);
        $field->setCountry('Argentina');
        $field->setSportClubName('18');
        $field->setPar(18);
        $field->setRoute(18);
        $field->setTopographicMovements(18);
        $field->setStartPoint(18);
        $field->setType(18);
        $round->setField($field);
//        $round->setFieldType(FieldType::MODERATE);
//        $round->setFieldPar(FieldPar::MORE_72);
//        $round->setHoleCount(18);
//        $round->setPlayerConfirmed(true);

        $preCompetitiveQuestionnaire = new PreCompetitiveQuestionnaire;
        $preCompetitiveQuestionnaire->setRound($round);
        $preCompetitiveQuestionnaire->setIsWarmedUp(false);
        $preCompetitiveQuestionnaire->setHasPracticed(false);
        $preCompetitiveQuestionnaire->setWantsToPlay(false);
        $preCompetitiveQuestionnaire->setHasStrategy(false);
        $round->setPreCompetitiveQuestionnaire($preCompetitiveQuestionnaire);

        $postCompetitiveQuestionnaire = new PostCompetitiveQuestionnaire;
        $postCompetitiveQuestionnaire->setRound($round);
        $postCompetitiveQuestionnaire->setFeelingStatus('Positive');
        $round->setPostCompetitiveQuestionnaire($postCompetitiveQuestionnaire);

        $this->getReference('user-player')->addRound($round);

        $this->container->get('round_persister')->save($round);
    }

    /**
     * {@inheritDoc}
     */
    public function getOrder()
    {
        return 2;
    }

}
