<?php

namespace AndresGotta\Bundle\GolfBundle\DataFixtures\ORM;

use AndresGotta\Bundle\GolfBundle\Entity\GroupReport;
use AndresGotta\Bundle\GolfBundle\Entity\Report;
use AndresGotta\Bundle\GolfBundle\Entity\Category;
use Doctrine\Common\DataFixtures\AbstractFixture;
use Doctrine\Common\DataFixtures\OrderedFixtureInterface;
use Doctrine\Common\Persistence\ObjectManager;
use Symfony\Component\DependencyInjection\ContainerAwareInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Symfony\Component\Yaml\Yaml;
use Symfony\Component\Validator\Validation;

/**
 * Class LoadReportTypes
 * @package AndresGotta\Bundle\GolfBundle\DataFixtures\ORM
 */
class LoadCategories extends AbstractFixture implements ContainerAwareInterface, OrderedFixtureInterface
{
    /**
     * @var ContainerInterface
     */
    private $container;

    /**
     * @param ContainerInterface|null $container
     */
    public function setContainer(ContainerInterface $container = null)
    {
        $this->container = $container;
    }

    /**
     * @param ObjectManager $manager
     */
    public function load(ObjectManager $manager)
    {
        $fixturesPath = realpath(dirname(__FILE__));
        $fixtureCategories = Yaml::parse(file_get_contents($fixturesPath. '/'. 'categories' . '.yml'));
        
        $validator = Validation::createValidatorBuilder()->enableAnnotationMapping()->getValidator();

        foreach ($fixtureCategories['categories'] as $module  => $data) {
            $category = $this->loadCategory($data);

            $errors = $validator->validate($category);
            if ($errors->count() === 0) {
                $manager->persist($category);

            } else {
                echo $errors;
            }
        }

        $manager->flush();
    }

    /**
     * @param array $data
     * @return Category
     */
    private function loadCategory(array $data)
    {
        $parent = new Category();
        $parent
            ->setName($data['name'])
            ->setDescription($data['description'])
        ;

        foreach ($data['children'] as $childData) {
            $child = new Category();
            $child
                ->setName($childData['name'])
                ->setDescription($childData['description'])
                ->setParent($parent)
            ;

            $parent->addChild($child);

            if (array_key_exists('groups', $childData)) {
                foreach ($childData['groups'] as $groupData) {
                    $group = new GroupReport();
                    $group
                        ->setName($groupData['name'])
                        ->setCategory($child)
                    ;

                    foreach ($groupData['reports'] as $reportCode) {
                        /** @var Report $report */
                        $report = $this->getReference('report-' . $reportCode);
                        $group->addReport($report);

                        $report->addGroup($group);
                    }
                }
            }

        }

        return $parent;
    }

    /**
     * @return int
     */
    public function getOrder()
    {
        return 10;
    }

}
