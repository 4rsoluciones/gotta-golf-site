<?php

namespace AndresGotta\Bundle\GolfBundle\DataFixtures\ORM;

use AndresGotta\Bundle\GolfBundle\Entity\Report;
use Doctrine\Common\DataFixtures\AbstractFixture;
use Doctrine\Common\DataFixtures\OrderedFixtureInterface;
use Doctrine\Common\Persistence\ObjectManager;
use Symfony\Component\DependencyInjection\ContainerAwareInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Symfony\Component\Yaml\Yaml;
use Symfony\Component\Validator\Validation;

/**
 * Class LoadReports
 * @package AndresGotta\Bundle\GolfBundle\DataFixtures\ORM
 */
class LoadReports extends AbstractFixture implements ContainerAwareInterface, OrderedFixtureInterface
{
    /**
     * @var ContainerInterface
     */
    private $container;

    public function setContainer(ContainerInterface $container = null)
    {
        $this->container = $container;
    }

    public function load(ObjectManager $manager)
    {
        $fixturesPath = realpath(dirname(__FILE__));
        $fixtureReports = Yaml::parse(file_get_contents($fixturesPath. '/'. 'reports' . '.yml'));
        
        $validator = Validation::createValidatorBuilder()->enableAnnotationMapping()->getValidator();

        foreach ($fixtureReports['reports'] as $module  => $data) {
            $report = $this->loadReport($data);
            $errors = $validator->validate($report);
            if ($errors->count() === 0) {
                $manager->persist($report);
                $this->addReference('report-' . $report->getReportCode(), $report);
            } else {
                echo $errors;
            }
        }

        $manager->flush();
    }

    /**
     * Carga una entidad Report
     * 
     * @param array $reportData
     * @return \AndresGotta\Bundle\GolfBundle\Entity\Report
     */
    public function loadReport($reportData)
    {
        $translator = $this->container->get('translator');

        $report = new Report();
        $report->setPar($reportData['par']);
        $report->setObjectives($translator->trans($reportData['objectives']));
        $report->setSlug($reportData['slug']);
        $report->setReportCode($reportData['code']);

        return $report;
    }

    /**
     * @return int
     */
    public function getOrder()
    {
        return 9;
    }

}
