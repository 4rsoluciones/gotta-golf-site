<?php

namespace AndresGotta\Bundle\GolfBundle\DataFixtures\ORM;

use AndresGotta\Bundle\GolfBundle\Entity\EmotionalQuestionnaire;
use AndresGotta\Bundle\GolfBundle\Entity\FoodQuestionnaire;
use AndresGotta\Bundle\GolfBundle\Entity\Hole;
use AndresGotta\Bundle\GolfBundle\Entity\PhysicalQuestionnaire;
use AndresGotta\Bundle\GolfBundle\Entity\Round;
use AndresGotta\Bundle\GolfBundle\Entity\Strike;
use AndresGotta\Bundle\GolfBundle\ValueObject\BallFlight;
use AndresGotta\Bundle\GolfBundle\ValueObject\Distance;
use AndresGotta\Bundle\GolfBundle\ValueObject\HoleDesign;
use AndresGotta\Bundle\GolfBundle\ValueObject\LandSlope;
use AndresGotta\Bundle\GolfBundle\ValueObject\Result;
use AndresGotta\Bundle\GolfBundle\ValueObject\StrikeLie;
use AndresGotta\Bundle\GolfBundle\ValueObject\StrikeType;
use Doctrine\Common\DataFixtures\AbstractFixture;
use Doctrine\Common\DataFixtures\OrderedFixtureInterface;
use Doctrine\Common\Persistence\ObjectManager;
use Symfony\Component\DependencyInjection\ContainerAwareInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;

class LoadHoles extends AbstractFixture implements OrderedFixtureInterface, ContainerAwareInterface
{

    /**
     * @var ContainerInterface
     */
    private $container;

    public function setContainer(ContainerInterface $container = null)
    {
        $this->container = $container;
    }

    /**
     * {@inheritDoc}
     */
    public function load(ObjectManager $manager)
    {
        /** @var Round $round */
        $round = $this->getReference("round");

        for ($i = 1; $i <= $round->getField()->getHoleCount(); ++$i) {
            $hole = new Hole();
            $hole->setDesign(array_rand(HoleDesign::getChoices()));
            $hole->setNumber($i);
            $hole->setPar(rand(3, 5));
            $hole->setPenalityShotsCount(rand(0, 2));
            $hole->setDescription('A simple description of the hole');
            $hole->setRound($round);
            $this->loadStrikes($hole);
            $round->addHole($hole);

            $errors = $this->container->get('validator')->validate($hole);

            if ($errors->count()) {
                $messages = array();
                foreach ($errors as $error) {
                    $messages[] = $error->getPropertyPath() . ' -> ' . $error->getMessage();
                }

                throw new \Exception('Invalid hole fixture: ' . PHP_EOL . implode(PHP_EOL, $messages));
            }
            $this->container->get('hole_persister')->save($hole);
        }

    }

    /**
     * Carga diferentes strikes hasta que se logre un buen tiro
     * 
     * @param Hole $hole
     */
    private function loadStrikes(Hole $hole)
    {
        $strike = new Strike();
        $hole->addStrike($strike);
        $strike->setType(StrikeType::TYPE_FIRST);
        $strike->setDistance(Distance::LESS_THAN_100_YDS);
        $strike->setResult(Result::HOLE_OUT);
        $strike->setBallFlight(BallFlight::RIGHT_TO_LEFT);
        $strike->setStrikeLies([StrikeLie::ON_CLIMB]);
        $strike->setLandSlopes([LandSlope::ON_DESCEND]);
        $strike->setEmotionalQuestionnaire($this->generateEmotionalQuestionnaire());
        $strike->setFoodQuestionnaire($this->generateFoodQuestionnaire());
        $strike->setPhysicalQuestionnaire($this->generatePhysicalQuestionnaire());
    }

    /**
     * {@inheritDoc}
     */
    public function getOrder()
    {
        return 3;
    }

    private function generateEmotionalQuestionnaire()
    {
        $emotionalQuestionnaire = new EmotionalQuestionnaire;
        $emotionalQuestionnaire->setFeelsUptight(rand(0, 1));
        $emotionalQuestionnaire->setFeelsDistrustful(rand(0, 1));
        $emotionalQuestionnaire->setFeelsAnnoyed(rand(0, 1));
        $emotionalQuestionnaire->setFeelsFrustrated(rand(0, 1));
        $emotionalQuestionnaire->setFeelsTrustful(rand(0, 1));
        $emotionalQuestionnaire->setFeelsEnjoying(rand(0, 1));
        $emotionalQuestionnaire->setFeelsFocused(rand(0, 1));

        return $emotionalQuestionnaire;
    }

    private function generateFoodQuestionnaire()
    {
        $foodQuestionnaire = new FoodQuestionnaire;
        $foodQuestionnaire->setDrankWater(rand(0, 1));
        $foodQuestionnaire->setDrankSportsDrink(rand(0, 1));
        $foodQuestionnaire->setSwallowedCerealOrNuts(rand(0, 1));
        $foodQuestionnaire->setSwallowedSandwich(rand(0, 1));
        $foodQuestionnaire->setSwallowedFruit(rand(0, 1));

        return $foodQuestionnaire;
    }

    private function generatePhysicalQuestionnaire()
    {
        $physicalQuestionnaire = new PhysicalQuestionnaire;
        $physicalQuestionnaire->setFeelsEnergetic(rand(0, 1));
        $physicalQuestionnaire->setFeelsGettingTired(rand(0, 1));
        $physicalQuestionnaire->setFeelsPainful(rand(0, 1));
        $physicalQuestionnaire->setFeelsTired(rand(0, 1));
        $physicalQuestionnaire->setFeelsExhausted(rand(0, 1));

        return $physicalQuestionnaire;
    }
}
