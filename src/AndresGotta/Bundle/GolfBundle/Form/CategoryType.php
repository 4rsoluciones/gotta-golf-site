<?php

namespace AndresGotta\Bundle\GolfBundle\Form;

use Doctrine\ORM\EntityRepository;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;

/**
 * Class ReportCategoryType
 * @package AndresGotta\Bundle\GolfBundle\Form
 */
class CategoryType extends AbstractType
{
    /**
     * @param FormBuilderInterface $builder
     * @param array $options
     */
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('name')
            ->add('description', 'ckeditor', [
                'config_name' => 'default',
                'required' => false
            ])
            ->add('parent', 'entity', [
                'class' => 'AndresGotta\Bundle\GolfBundle\Entity\Category',
                'property' => 'name',
                'required' => false,
                'help_label' => 'Si no selecciona una categoría padre, esta sería una.',
                'query_builder' => function(EntityRepository $er) {
                    return $er->createQueryBuilder('c')
                        ->where('c.parent IS NULL')->orderBy('c.name', 'ASC');
                }
            ])
            ->add('groups', 'entity', [
//                'type' => new GroupReportType(),
//                'allow_add' => true,
//                'allow_delete' => true,
//                'by_reference' => false,
                'class' => 'AndresGotta\Bundle\GolfBundle\Entity\GroupReport',
                'property' => 'name',
                'multiple' => true,
                'expanded' => true,
//                'widget_add_btn' => [
//                    'label' => 'Agregar',
//                    'icon' => 'plus-sign icon-white',
//                    'attr' => [
//                        'class' => 'btn btn-primary'
//                    ]
//                ],
//
            ])
        ;
    }

    /**
     * @param OptionsResolver $resolver
     */
    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults([
            'data_class' => 'AndresGotta\Bundle\GolfBundle\Entity\Category',
            'attr' => ['novalidate' => true]
        ]);
    }

    /**
     * @return string
     */
    public function getName()
    {
        return 'report_tag_type';
    }

}