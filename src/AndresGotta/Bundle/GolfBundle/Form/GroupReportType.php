<?php

namespace AndresGotta\Bundle\GolfBundle\Form;

use Doctrine\ORM\EntityRepository;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;

/**
 * Class ReportCategoryType
 * @package AndresGotta\Bundle\GolfBundle\Form
 */
class GroupReportType extends AbstractType
{
    /**
     * @param FormBuilderInterface $builder
     * @param array $options
     */
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('name')
            ->add('description')
            ->add('reports', 'entity', [
                'class' => 'AndresGotta\Bundle\GolfBundle\Entity\Report',
                'property' => 'reportCode',
                'query_builder' => function(EntityRepository $er) {
                    return $er->createQueryBuilder('r')->orderBy('r.reportCode', 'ASC');
                },
                'multiple' => false,
                'expanded' => false
            ])
        ;
    }

    /**
     * @param OptionsResolver $resolver
     */
    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults([
            'data_class' => 'AndresGotta\Bundle\GolfBundle\Entity\GroupReport',
            'attr' => ['novalidate' => true]
        ]);
    }

    /**
     * @return string
     */
    public function getName()
    {
        return 'report_category_type';
    }

}