<?php

namespace AndresGotta\Bundle\GolfBundle\Form;

use AndresGotta\Bundle\GolfBundle\ValueObject\PlayerCondition;
use AndresGotta\Bundle\GolfBundle\ValueObject\RoundType;
use Doctrine\ORM\EntityRepository;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolverInterface;

class UserReportFilterType extends AbstractType
{

    /**
     * @param FormBuilderInterface $builder
     * @param array $options
     */
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
                ->add('user', 'entity', array(
                    'class' => 'WebFactory\Bundle\UserBundle\Entity\User',
                    'query_builder' => function (EntityRepository $er) {
                        return $er
                                ->createQueryBuilder('u')
                                ->where('u.roles LIKE :roles')
                                ->setParameter('roles', "%ROLE_PLAYER%")
                                ->orderBy('u.username', 'ASC');
                    },
                ))
                ->add('report', 'entity', array(
                    'class' => 'AndresGotta\Bundle\GolfBundle\Entity\Report',
                    'query_builder' => function (EntityRepository $er) {
                        return $er
                                ->createQueryBuilder('r')
                                ->orderBy('r.reportCode', 'ASC');
                    },
                ))
                ->add('from_date', 'date', array('required' => false))
                ->add('to_date', 'date', array('required' => false))
                ->add('condition', 'choice', array(
                    'choices' => PlayerCondition::getChoices(),
                    'required' => false
                ))
                ->add('roundType', 'choice', array(
                    'choices' => RoundType::getChoices(),
                    'required' => false
                ))
                ->add('submit', 'submit', array('label' => 'Filter'))
        ;
    }

    /**
     * @param OptionsResolverInterface $resolver
     */
    public function setDefaultOptions(OptionsResolverInterface $resolver)
    {
        $resolver->setDefaults(array(
            'data_class' => null,
            'validation_groups' => array(),
        ));
    }

    /**
     * @return string
     */
    public function getName()
    {
        return 'andresgotta_bundle_golfbundle_userreportfilter';
    }

}
