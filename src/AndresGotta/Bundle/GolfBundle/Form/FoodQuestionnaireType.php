<?php

namespace AndresGotta\Bundle\GolfBundle\Form;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;

class FoodQuestionnaireType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('drankWater', 'checkbox', array(
                'label' => 'agua',
                'attr' => array(
                    'class' => 'drankWaterIcon',
                ),
            ))
            ->add('drankSportsDrink', 'checkbox', array(
                'label' => 'bebida deportiva',
                'attr' => array(
                    'class' => 'drankSportsDrinkIcon',
                ),
            ))
            ->add('swallowedCerealOrNuts', 'checkbox', array(
                'label' => 'cereales y/o frutos secos',
                'attr' => array(
                    'class' => 'swallowedCerealOrNutsIcon',
                ),
            ))
            ->add('swallowedSandwich', 'checkbox', array(
                'label' => 'sandwich',
                'attr' => array(
                    'class' => 'swallowedSandwichIcon',
                ),
            ))
            ->add('swallowedFruit', 'checkbox', array(
                'label' => 'frutas',
                'attr' => array(
                    'class' => 'swallowedFruitIcon',
                ),
            ));
    }

    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults(array(
            'data_class' => 'AndresGotta\Bundle\GolfBundle\Entity\FoodQuestionnaire',
            'label' => '¿qué tomaste  o comiste?',
            'attr' => array(
                'novalidate' => 'novalidate',
            ),
        ));
    }

    public function getName()
    {
        return 'food_questionnaire_type';
    }
}
