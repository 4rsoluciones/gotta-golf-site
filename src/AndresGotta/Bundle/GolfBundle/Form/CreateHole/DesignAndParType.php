<?php

namespace AndresGotta\Bundle\GolfBundle\Form\CreateHole;

use AndresGotta\Bundle\GolfBundle\ValueObject\HoleDesign;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolverInterface;
use AndresGotta\Bundle\GolfBundle\Form\EmotionalQuestionnaireType;
use AndresGotta\Bundle\GolfBundle\Form\FoodQuestionnaireType;
use AndresGotta\Bundle\GolfBundle\Form\PhysicalQuestionnaireType;

class DesignAndParType extends AbstractType {

    /**
     * @param FormBuilderInterface $builder
     * @param array $options
     */
    public function buildForm(FormBuilderInterface $builder, array $options) {
        $parChoices = array(
            3 => 3,
            4 => 4,
            5 => 5,
        );

        $builder
            ->add('par', 'choice', array(
                'choices' => $parChoices,
                'expanded' => true,
            ))
            ->add('design', 'choice', array(
                'choices' => HoleDesign::getChoicesForm(),
                'expanded' => true
            ))
            ->add('emotionalQuestionnaire', new EmotionalQuestionnaireType)
            ->add('foodQuestionnaire', new FoodQuestionnaireType)
            ->add('physicalQuestionnaire', new PhysicalQuestionnaireType)
        ;
    }

    /**
     * @param OptionsResolverInterface $resolver
     */
    public function setDefaultOptions(OptionsResolverInterface $resolver) {
        $resolver->setDefaults(array(
            'data_class' => 'AndresGotta\Bundle\GolfBundle\Entity\Hole'
        ));
    }

    /**
     * @return string
     */
    public function getName() {
        return 'hole';
    }

}
