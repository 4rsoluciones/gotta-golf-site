<?php

namespace AndresGotta\Bundle\GolfBundle\Form\CreateHole;

use AndresGotta\Bundle\GolfBundle\ValueObject\HoleDesign;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolverInterface;

class PenalitiesType extends AbstractType
{

    /**
     * @param FormBuilderInterface $builder
     * @param array $options
     */
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $penalityShotsCountChoices = array_combine(range(0, 12), range(0, 12));

        $builder
                ->add('penalityShotsCount', 'choice', array(
                    'choices' => $penalityShotsCountChoices,
                ))
        ;
    }

    /**
     * @param OptionsResolverInterface $resolver
     */
    public function setDefaultOptions(OptionsResolverInterface $resolver)
    {
        $resolver->setDefaults(array(
            'data_class' => 'AndresGotta\Bundle\GolfBundle\Entity\Hole'
        ));
    }

    /**
     * @return string
     */
    public function getName()
    {
        return 'hole';
    }

}
