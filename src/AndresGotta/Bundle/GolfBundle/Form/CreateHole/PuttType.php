<?php

namespace AndresGotta\Bundle\GolfBundle\Form\CreateHole;

use AndresGotta\Bundle\GolfBundle\Form\CreateHole\Custom\PuttType as CustomPuttType;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolverInterface;

class PuttType extends AbstractType
{

    /**
     * @param FormBuilderInterface $builder
     * @param array $options
     */
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder->add('putt', new CustomPuttType, array(
            'data_class' => 'AndresGotta\Bundle\GolfBundle\Entity\Putt',
            'distance_choices' => $options['distance_choices'],
            'result_choices' => $options['result_choices'],
            'reason_choices' => $options['reason_choices'],
            'label_attr' => array('class' => 'visibilityHidden')
        ));
    }

    /**
     * @param OptionsResolverInterface $resolver
     */
    public function setDefaultOptions(OptionsResolverInterface $resolver)
    {
        $resolver->setDefaults(array(
            'data_class' => 'AndresGotta\Bundle\GolfBundle\Entity\Hole',
            'distance_choices' => array(),
            'result_choices' => array(),
            'reason_choices' => array(),
        ));
    }

    /**
     * @return string
     */
    public function getName()
    {
        return 'hole';
    }

}
