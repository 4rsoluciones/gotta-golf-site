<?php

namespace AndresGotta\Bundle\GolfBundle\Form\CreateHole\Custom;

use AndresGotta\Bundle\GolfBundle\Form\StrikeType;
use AndresGotta\Bundle\GolfBundle\ValueObject\Club;
use Symfony\Component\Form\FormBuilderInterface;

class ClubLieBallFlightStrikeType extends LieBallFlightStrikeType
{

    /**
     * @param FormBuilderInterface $builder
     * @param array $options
     */
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
                ->add('club', 'choice', array(
                    'choices' => Club::getChoices(),
                    'data' => Club::DRIVER,
                    'multiple' => false,
                    'expanded' => true,
                ))
        ;
        parent::buildForm($builder, $options);
    }

}
