<?php

namespace AndresGotta\Bundle\GolfBundle\Form\CreateHole\Custom;

use AndresGotta\Bundle\GolfBundle\Form\StrikeType;
use AndresGotta\Bundle\GolfBundle\ValueObject\StrikeLie;
use Symfony\Component\Form\FormBuilderInterface;

/**
 * @todo Rename as LieStrikeType
 */
class LieStrikeType extends StrikeType
{

    /**
     * @param FormBuilderInterface $builder
     * @param array $options
     */
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
                ->add('strikeLies', 'choice', array(
                    'choices' => StrikeLie::getChoices(),
                    'data' => [StrikeLie::BALL_AT_YOUR_FEET],
                    'multiple' => true,
                    'expanded' => true,
                    'help_label_popover' => array(
                        'title' => 'tooltip.strike.lies',
                        'content' => ' ', 
                        'placement' => 'right'
                    ),
                ))
        ;
        parent::buildForm($builder, $options);
    }

}
