<?php

namespace AndresGotta\Bundle\GolfBundle\Form\CreateHole\Custom;

use AndresGotta\Bundle\GolfBundle\ValueObject\BallFlight;
use Symfony\Component\Form\FormBuilderInterface;

class LieBallFlightStrikeType extends LieStrikeType
{

    /**
     * @param FormBuilderInterface $builder
     * @param array $options
     */
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
                ->add('ballFlight', 'choice', array(
                    'choices' => BallFlight::getChoicesForm(),
                    'data' => BallFlight::STRAIGHT,
                    'multiple' => false,
                    'expanded' => true,
                    'label' => 'ball_flight.label.from_strike',
                    'help_label_popover' => array(
                        'title' => 'tooltip.ball.flight',
                        'content' => ' ',
                        'placement' => 'right'
                    ),
                ))
        ;
        parent::buildForm($builder, $options);
    }

}
