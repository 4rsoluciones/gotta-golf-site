<?php

namespace AndresGotta\Bundle\GolfBundle\Form\CreateHole\Custom;

use AndresGotta\Bundle\GolfBundle\Form\StrikeType;
use AndresGotta\Bundle\GolfBundle\ValueObject\LandSlope;
use Symfony\Component\Form\FormBuilderInterface;

class PuttType extends StrikeType
{

    /**
     * @param FormBuilderInterface $builder
     * @param array $options
     */
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
                ->add('landSlopes', 'choice', array(
                    'choices' => LandSlope::getChoices(),
                    'multiple' => true,
                    'expanded' => true,
                    'label' => 'landslopes.label.form',
                ))
        ;
        parent::buildForm($builder, $options);
    }

}
