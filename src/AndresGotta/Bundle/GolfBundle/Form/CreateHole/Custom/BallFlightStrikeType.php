<?php

namespace AndresGotta\Bundle\GolfBundle\Form\CreateHole\Custom;

use AndresGotta\Bundle\GolfBundle\Form\StrikeType;
use AndresGotta\Bundle\GolfBundle\ValueObject\BallFlight;
use AndresGotta\Bundle\GolfBundle\ValueObject\Club;
use Symfony\Component\Form\FormBuilderInterface;

class BallFlightStrikeType extends StrikeType
{

    /**
     * @param FormBuilderInterface $builder
     * @param array $options
     */
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('club', 'choice', array(
                'choices' => Club::getChoices(),
                'data' => Club::IRON,
                'multiple' => false,
                'expanded' => true,
            ))
            ->add('ballFlight', 'choice', array(
                'choices' => BallFlight::getChoicesForm(),
                'data' => BallFlight::STRAIGHT,
                'multiple' => false,
                'expanded' => true,
                'label' => 'ball_flight.label.from_strike',
                'help_label_popover' => array(
                    'title' => 'tooltip.ball.flight',
                    'content' => ' ',
                    'placement' => 'right'
                ),
            ))
        ;
        parent::buildForm($builder, $options);
    }

}
