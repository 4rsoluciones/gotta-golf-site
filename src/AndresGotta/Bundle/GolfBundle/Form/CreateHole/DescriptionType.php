<?php

namespace AndresGotta\Bundle\GolfBundle\Form\CreateHole;

use AndresGotta\Bundle\GolfBundle\ValueObject\HoleDesign;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolverInterface;

class DescriptionType extends AbstractType
{

    /**
     * @param FormBuilderInterface $builder
     * @param array $options
     */
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
                ->add('description', 'textarea', array(

                ))
        ;
    }

    /**
     * @param OptionsResolverInterface $resolver
     */
    public function setDefaultOptions(OptionsResolverInterface $resolver)
    {
        $resolver->setDefaults(array(
            'data_class' => 'AndresGotta\Bundle\GolfBundle\Entity\Hole'
        ));
    }

    /**
     * @return string
     */
    public function getName()
    {
        return 'hole';
    }

}
