<?php

namespace AndresGotta\Bundle\GolfBundle\Form\CreateHole;

use AndresGotta\Bundle\GolfBundle\Entity\ThirdStrikeApproach;
use AndresGotta\Bundle\GolfBundle\Form\CreateHole\Custom\LieBallFlightStrikeType;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\Form\FormEvent;
use Symfony\Component\Form\FormEvents;
use Symfony\Component\OptionsResolver\OptionsResolverInterface;

class ThirdStrikeApproachsType extends AbstractType
{

    /**
     * @param FormBuilderInterface $builder
     * @param array $options
     */
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder->add('thirdStrikeApproachs', 'collection', array(
            'error_bubbling' => false,
            'type' => new LieBallFlightStrikeType(),
            'allow_add' => true,
            'allow_delete' => true,
            'prototype' => true,
            'by_reference' => false,
            'widget_add_btn' => array(
                'label' => 'Add',
                'icon' => 'plus-sign icon-white',
                'attr' => array(
                    'class' => 'btn btn-primary'
                )
            ),
            'options' => array(
                'data_class' => 'AndresGotta\Bundle\GolfBundle\Entity\ThirdStrikeApproach',
                'distance_choices' => $options['distance_choices'],
                'result_choices' => $options['result_choices'],
                'reason_choices' => $options['reason_choices'],
                'widget_remove_btn' => array(
                    'label' => '',
                    'attr' => array('class' => 'btn btn-danger'),
                    'icon' => 'remove icon-white',
                ),
                'attr' => array('class' => 'span3'),
                'widget_addon' => array(
                    'type' => 'prepend',
                    'text' => '@',
                ),
                'widget_control_group' => false,
                'label' => false,
                'error_bubbling' => false,
            ),
            'label_attr' => array('class' => 'visibilityHidden')
        ));
        
        $builder->addEventListener(FormEvents::PRE_SET_DATA, function (FormEvent  $event) {
            $hole = $event->getData();

            if (null === $hole) {
                return;
            }

            if ($hole->getThirdStrikeApproachs()->isEmpty()) {
                $approach = new ThirdStrikeApproach;
                $hole->addThirdStrikeApproach($approach);
            }
        });
    }

    /**
     * @param OptionsResolverInterface $resolver
     */
    public function setDefaultOptions(OptionsResolverInterface $resolver)
    {
        $resolver->setDefaults(array(
            'data_class' => 'AndresGotta\Bundle\GolfBundle\Entity\Hole',
            'distance_choices' => array(),
            'result_choices' => array(),
            'reason_choices' => array(),
        ));
    }

    /**
     * @return string
     */
    public function getName()
    {
        return 'hole';
    }

}
