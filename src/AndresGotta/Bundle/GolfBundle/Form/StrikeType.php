<?php

namespace AndresGotta\Bundle\GolfBundle\Form;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolverInterface;
use AndresGotta\Bundle\GolfBundle\Form\EmotionalQuestionnaireType;
use AndresGotta\Bundle\GolfBundle\Form\FoodQuestionnaireType;
use AndresGotta\Bundle\GolfBundle\Form\PhysicalQuestionnaireType;

class StrikeType extends AbstractType
{

    /**
     * @param FormBuilderInterface $builder
     * @param array $options
     */
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $distanceLabel = $options['data_class'] === 'AndresGotta\Bundle\GolfBundle\Entity\FirstStrike' ? 'distance.label.from_strike' : 'distance.label.to_hole';
        $reasonLabel = ($options['data_class'] === 'AndresGotta\Bundle\GolfBundle\Entity\FirstStrike') ? 'tooltip.reason.start' : 'tooltip.reason';

        $builder
            ->add('distance', 'choice', array(
                'label' => $distanceLabel,
                'choices' => $options['distance_choices'],
                'widget_control_group_attr' => array(
                    'class' => 'hasSliderHorizontal',
                ),
                'help_label_popover' => array(
                    'title' => 'tooltip.distance',
                    'content' => ' ',
                    'placement' => 'right'
                ),
            ))
            ->add('result', 'choice', array(
                'choices' => $options['result_choices'],
                'expanded' => true,
                'widget_control_group_attr' => array(
                    'class' => 'resultControlGroup',
                ),
                'help_label_popover' => array(
                    'title' => 'tooltip.result',
                    'content' => ' ',
                    'placement' => 'right'
                ),
            ))
            ->add('reason', 'choice', array(
                'choices' => $options['reason_choices'],
                'expanded' => true,
                'widget_control_group_attr' => array(
                    'class' => 'reasonControlGroup hidden',
                ),
                'help_label_popover' => array(
                    'title' => $reasonLabel,
                    'content' => ' ',
                    'placement' => 'right'
                ),
            ))
            ->add('emotionalQuestionnaire', new EmotionalQuestionnaireType)
            ->add('foodQuestionnaire', new FoodQuestionnaireType)
            ->add('physicalQuestionnaire', new PhysicalQuestionnaireType);
    }

    /**
     * @param OptionsResolverInterface $resolver
     */
    public function setDefaultOptions(OptionsResolverInterface $resolver)
    {
        $resolver->setDefaults(array(
            'distance_choices' => array(),
            'result_choices' => array(),
            'reason_choices' => array(),
            'par' => null,
        ));
    }

    /**
     * @return string
     */
    public function getName()
    {
        return 'strike';
    }

}
