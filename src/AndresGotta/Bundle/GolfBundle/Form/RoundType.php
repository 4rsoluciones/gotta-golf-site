<?php

namespace AndresGotta\Bundle\GolfBundle\Form;

use AndresGotta\Bundle\GolfBundle\Entity\Round;
use AndresGotta\Bundle\GolfBundle\ValueObject\Handicap;
use AndresGotta\Bundle\GolfBundle\ValueObject\PlayerCondition;
use AndresGotta\Bundle\GolfBundle\ValueObject\TournamentType;
use AndresGotta\Bundle\GolfBundle\ValueObject\RoundType as RoundTypeChoices;
use AndresGotta\Bundle\GolfBundle\ValueObject\GameCondition;
use AndresGotta\Bundle\GolfBundle\ValueObject\FieldType;
use AndresGotta\Bundle\GolfBundle\ValueObject\FieldPar;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolverInterface;
use Symfony\Component\Form\FormEvent;
use Symfony\Component\Form\FormEvents;
use WebFactory\Bundle\UserBundle\Entity\User;

class RoundType extends AbstractType
{

    /**
     * @param FormBuilderInterface $builder
     * @param array $options
     */
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $holeCountChoices = array(9 => 9, 18 => 18);
        $roundNumberChoices = array_combine(range(1, 4), range(1, 4));

        $builder
                ->add('performedAt', 'date')
                ->add('handicap', 'choice', array(
                    'choices' => array_reverse(Handicap::getChoices(), true),
                ))
                ->add('tour', 'entity', array(
                    'class' => 'AndresGottaGolfBundle:Tour',
                    'query_builder' => function ($repo) {
                        return $repo->createQueryBuilder('Tour')->orderBy('Tour.name', 'asc');
                    },
                ))
                ->add('roundType', 'choice', array(
                    'choices' => RoundTypeChoices::getChoices(),
                    'expanded' => true,
                    'data' => RoundTypeChoices::getChoices('default')
                ))
                ->add('tournamentType', 'choice', array(
                    'choices' => TournamentType::getChoices(),
                ))
                //TODO: Agregar validación Este es dependiente del tournamenttype
                ->add('roundNumber', 'choice', array(
                    'choices' => $roundNumberChoices,
                ))
                ->add('gameCondition', 'choice', array(
                    'choices' => GameCondition::getChoices(),
                    'help_label_popover' => array(
                        'title' => 'tooltip.game.condition',
                        'content' => ' ', 
                        'placement' => 'right'
                    ),
                ))
                ->add('fieldType', 'choice', array(
                    'choices' => FieldType::getChoices(),
                    'help_label_popover' => array(
                        'title' => 'tooltip.field.type',
                        'content' => ' ', 
                        'placement' => 'right'
                    ),
                ))
                ->add('fieldPar', 'choice', array(
                    'choices' => FieldPar::getChoices(),
                ))
                ->add('holeCount', 'choice', array(
                    'choices' => $holeCountChoices,
                    'expanded' => true,
                    'label' => 'Holecount.label',
                    'data' => 18
                ))
        ;

        $builder->addEventListener(FormEvents::PRE_SET_DATA, function(FormEvent $event) {
            /** @var Round $round */
            $round = $event->getData();
            if (!$round) {
                return;
            }
            $form = $event->getForm();

            /** @var User $player */
            $player = $round->getPlayer();
            if ($player->getProfile()->getCondition() === PlayerCondition::PROFESSIONAL) {
                $form->remove('handicap');
            } else {
                $form->remove('tour');
            }

            if ($round->getId()) {
                $form->remove('holeCount');
            }
        });
    }

    /**
     * @param OptionsResolverInterface $resolver
     */
    public function setDefaultOptions(OptionsResolverInterface $resolver)
    {
        $resolver->setDefaults(array(
            'data_class' => 'AndresGotta\Bundle\GolfBundle\Entity\Round',
            'validation_groups' => array('OnlyRound'),
        ));
    }

    /**
     * @return string
     */
    public function getName()
    {
        return 'andresgotta_bundle_golfbundle_round';
    }

}
