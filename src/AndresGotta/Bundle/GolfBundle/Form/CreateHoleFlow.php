<?php

namespace AndresGotta\Bundle\GolfBundle\Form;

use AndresGotta\Bundle\GolfBundle\Entity\FirstStrike;
use AndresGotta\Bundle\GolfBundle\Entity\FirstStrikeApproach;
use AndresGotta\Bundle\GolfBundle\Entity\Putt;
use AndresGotta\Bundle\GolfBundle\Entity\PuttApproach;
use AndresGotta\Bundle\GolfBundle\Entity\SecondStrike;
use AndresGotta\Bundle\GolfBundle\Entity\SecondStrikeApproach;
use AndresGotta\Bundle\GolfBundle\Entity\ThirdStrike;
use AndresGotta\Bundle\GolfBundle\Entity\ThirdStrikeApproach;
use AndresGotta\Bundle\GolfBundle\Form\CreateHole\DescriptionType;
use AndresGotta\Bundle\GolfBundle\Form\CreateHole\DesignAndParType;
use AndresGotta\Bundle\GolfBundle\Form\CreateHole\FirstStrikeApproachsType;
use AndresGotta\Bundle\GolfBundle\Form\CreateHole\FirstStrikeType;
use AndresGotta\Bundle\GolfBundle\Form\CreateHole\PenalitiesDescriptionType;
use AndresGotta\Bundle\GolfBundle\Form\CreateHole\PenalitiesType;
use AndresGotta\Bundle\GolfBundle\Form\CreateHole\PuttApproachsType;
use AndresGotta\Bundle\GolfBundle\Form\CreateHole\PuttType;
use AndresGotta\Bundle\GolfBundle\Form\CreateHole\SecondStrikeApproachsType;
use AndresGotta\Bundle\GolfBundle\Form\CreateHole\SecondStrikeType;
use AndresGotta\Bundle\GolfBundle\Form\CreateHole\ThirdStrikeApproachsType;
use AndresGotta\Bundle\GolfBundle\Form\CreateHole\ThirdStrikeType;
use AndresGotta\Bundle\GolfBundle\Form\Helper\StepSkipService;
use Craue\FormFlowBundle\Form\FormFlow;
use Craue\FormFlowBundle\Form\FormFlowInterface;

/**
 * Description of CreateRoundFlow
 *
 * @author gaston
 */
class CreateHoleFlow extends FormFlow
{

    const DESIGN_AND_PAR_STEP = 1;
    const FIRST_STRIKE_STEP = 2;
    const FIRST_STRIKE_APPROACHS_STEP = 3;
    const SECOND_STRIKE_STEP = 4;
    const SECOND_STRIKE_APPROACHS_STEP = 5;
    const THIRD_STRIKE_STEP = 6;
    const THIRD_STRIKE_APPROACHS_STEP = 7;
    const PUTT_STEP = 8;
    const PUTT_APPROACHS_STEP = 9;
    const PENALITIES_STEP = 10;
    const DESCRIPTION_STEP = 11;
    const CONFIRMATION_STEP = 12;

    /**
     * 
     * @return string
     */
    public function getName()
    {
        return 'createHole';
    }

    /**
     * 
     * @return array
     * 
     * @todo Existe un error por isHoleOut que deshabilita todos los pasos, revisar
     */
    protected function loadStepsConfig()
    {
        return array(
            self::DESIGN_AND_PAR_STEP => array(
                'label' => 'Design and Par',
                'type' => new DesignAndParType(),
            ),
            self::FIRST_STRIKE_STEP => array(
                'label' => 'First Strike',
                'type' => new FirstStrikeType(),
            ),
            self::FIRST_STRIKE_APPROACHS_STEP => array(
                'label' => 'First Strike Approachs',
                'type' => new FirstStrikeApproachsType(),
                'skip' => function ($estimatedCurrentStepNumber, FormFlowInterface $flow) {
                    $hole = $flow->getFormData();
                    $canHandleStrike = $hole->canHaveFirstStrikeApproachs();

                    return $estimatedCurrentStepNumber > CreateHoleFlow::DESIGN_AND_PAR_STEP && !$canHandleStrike;
                },
            ),
            self::SECOND_STRIKE_STEP => array(
                'label' => 'Second Strike',
                'type' => new SecondStrikeType(),
                'skip' => function ($estimatedCurrentStepNumber, FormFlowInterface $flow) {
                    $hole = $flow->getFormData();
                    $canHandleStrike = $hole->canHaveSecondStrike();

                    return $estimatedCurrentStepNumber > CreateHoleFlow::DESIGN_AND_PAR_STEP && !$canHandleStrike;
                },
            ),
            self::SECOND_STRIKE_APPROACHS_STEP => array(
                'label' => 'Second Strike Approachs',
                'type' => new SecondStrikeApproachsType(),
                'skip' => function ($estimatedCurrentStepNumber, FormFlowInterface $flow) {
                    $hole = $flow->getFormData();
                    $canHandleStrike = $hole->canHaveSecondStrikeApproachs();

                    return $estimatedCurrentStepNumber > CreateHoleFlow::DESIGN_AND_PAR_STEP && !$canHandleStrike;
                },
            ),
            self::THIRD_STRIKE_STEP => array(
                'label' => 'Third Strike',
                'type' => new ThirdStrikeType(),
                'skip' => function ($estimatedCurrentStepNumber, FormFlowInterface $flow) {
                    $hole = $flow->getFormData();
                    $canHandleStrike = $hole->canHaveThirdStrike();

                    return $estimatedCurrentStepNumber > CreateHoleFlow::DESIGN_AND_PAR_STEP && !$canHandleStrike;
                },
            ),
            self::THIRD_STRIKE_APPROACHS_STEP => array(
                'label' => 'Third Strike Approachs',
                'type' => new ThirdStrikeApproachsType(),
                'skip' => function ($estimatedCurrentStepNumber, FormFlowInterface $flow) {
                    $hole = $flow->getFormData();
                    $canHandleStrike = $hole->canHaveThirdStrikeApproachs();

                    return $estimatedCurrentStepNumber > CreateHoleFlow::DESIGN_AND_PAR_STEP && !$canHandleStrike;
                },
            ),
            self::PUTT_STEP => array(
                'label' => 'Putt',
                'type' => new PuttType(),
                'skip' => function ($estimatedCurrentStepNumber, FormFlowInterface $flow) {
                    $hole = $flow->getFormData();
                    $canHandleStrike = $hole->canHavePuttStrike();

                    return $estimatedCurrentStepNumber > CreateHoleFlow::DESIGN_AND_PAR_STEP && !$canHandleStrike;
                },
            ),
            self::PUTT_APPROACHS_STEP => array(
                'label' => 'Putt Approachs',
                'type' => new PuttApproachsType(),
                'skip' => function ($estimatedCurrentStepNumber, FormFlowInterface $flow) {
                    $hole = $flow->getFormData();
                    $canHandleStrike = $hole->canHavePuttStrikeApproachs();

                    return $estimatedCurrentStepNumber > CreateHoleFlow::DESIGN_AND_PAR_STEP && !$canHandleStrike;
                },
            ),
            self::PENALITIES_STEP => array(
                'label' => 'Penalities and Description',
                'type' => new PenalitiesDescriptionType(),
            ),
            self::CONFIRMATION_STEP => array(
                'label' => 'Confirmation',
            ),
        );
    }

    /**
     * Combina los valores de un array en un nuevo array con mismos keys y values
     *
     * @param array $array
     *
     * @return array
     */
    private function selfCombine($array)
    {
        if (empty($array)) {
            return array();
        }

        return array_combine($array, $array);
    }

    public function getFormOptions($step, array $options = array())
    {
        $options = parent::getFormOptions($step, $options);
        $par = $this->getFormData()->getPar();

        switch ($step) {
            case self::FIRST_STRIKE_STEP:
                $options['distance_choices'] = $this->selfCombine(FirstStrike::getDistanceChoicesByPar($par));
                $options['result_choices'] = $this->selfCombine(FirstStrike::getResultChoicesByPar($par));
                $options['reason_choices'] = $this->selfCombine(FirstStrike::getReasonChoicesByPar($par));
                $options['par'] = $par;
                break;
            case self::FIRST_STRIKE_APPROACHS_STEP:
                $options['distance_choices'] = $this->selfCombine(FirstStrikeApproach::getDistanceChoicesByPar($par));
                $options['result_choices'] = $this->selfCombine(FirstStrikeApproach::getResultChoicesByPar($par));
                $options['reason_choices'] = $this->selfCombine(FirstStrikeApproach::getReasonChoicesByPar($par));
                break;
            case self::SECOND_STRIKE_STEP:
                $options['distance_choices'] = $this->selfCombine(SecondStrike::getDistanceChoicesByPar($par));
                $options['result_choices'] = $this->selfCombine(SecondStrike::getResultChoicesByPar($par));
                $options['reason_choices'] = $this->selfCombine(SecondStrike::getReasonChoicesByPar($par));
                $options['par'] = $par;
                break;
            case self::SECOND_STRIKE_APPROACHS_STEP:
                $options['distance_choices'] = $this->selfCombine(SecondStrikeApproach::getDistanceChoicesByPar($par));
                $options['result_choices'] = $this->selfCombine(SecondStrikeApproach::getResultChoicesByPar($par));
                $options['reason_choices'] = $this->selfCombine(SecondStrikeApproach::getReasonChoicesByPar($par));
                break;
            case self::THIRD_STRIKE_STEP:
                $options['distance_choices'] = $this->selfCombine(ThirdStrike::getDistanceChoicesByPar($par));
                $options['result_choices'] = $this->selfCombine(ThirdStrike::getResultChoicesByPar($par));
                $options['reason_choices'] = $this->selfCombine(ThirdStrike::getReasonChoicesByPar($par));
                break;
            case self::THIRD_STRIKE_APPROACHS_STEP:
                $options['distance_choices'] = $this->selfCombine(ThirdStrikeApproach::getDistanceChoicesByPar($par));
                $options['result_choices'] = $this->selfCombine(ThirdStrikeApproach::getResultChoicesByPar($par));
                $options['reason_choices'] = $this->selfCombine(ThirdStrikeApproach::getReasonChoicesByPar($par));
                break;
            case self::PUTT_STEP:
                $options['distance_choices'] = $this->selfCombine(Putt::getDistanceChoicesByPar($par));
                $options['result_choices'] = $this->selfCombine(Putt::getResultChoicesByPar($par));
                $options['reason_choices'] = $this->selfCombine(Putt::getReasonChoicesByPar($par));
                break;
            case self::PUTT_APPROACHS_STEP:
                $options['distance_choices'] = $this->selfCombine(PuttApproach::getDistanceChoicesByPar($par));
                $options['result_choices'] = $this->selfCombine(PuttApproach::getResultChoicesByPar($par));
                $options['reason_choices'] = $this->selfCombine(PuttApproach::getReasonChoicesByPar($par));
                break;
        }

        return $options;
    }

}
