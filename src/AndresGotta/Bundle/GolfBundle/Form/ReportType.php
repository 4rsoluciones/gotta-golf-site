<?php

namespace AndresGotta\Bundle\GolfBundle\Form;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;

/**
 * Class ReportType
 * @package AndresGotta\Bundle\GolfBundle\Form
 */
class ReportType extends AbstractType
{
    /**
     * @param FormBuilderInterface $builder
     * @param array $options
     */
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('reportCode')
            ->add('par', 'choice', ['choices' => ['', '3', '4', '5']])
            ->add('objectives', 'ckeditor', [
                'config_name' => 'default'
            ])
        ;
    }

    /**
     * @param OptionsResolver $resolver
     */
    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults([
            'data_class' => 'AndresGotta\Bundle\GolfBundle\Entity\Report',
            'attr' => ['novalidate' => 'novalidate']
        ]);
    }

    /**
     * @return string
     */
    public function getName()
    {
        return 'report_type';
    }

}