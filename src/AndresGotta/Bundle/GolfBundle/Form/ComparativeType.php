<?php

namespace AndresGotta\Bundle\GolfBundle\Form;

use Doctrine\ORM\EntityRepository;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;

/**
 * Class ComparativeType
 * @package AndresGotta\Bundle\GolfBundle\Form
 */
class ComparativeType extends AbstractType
{
    /**
     * @inheritdoc
     */
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('highlighted')
            ->add('name')
            ->add('description', 'ckeditor', ['config_name' => 'default'])
            ->add('observation', 'ckeditor', ['config_name' => 'default'])
            ->add('maxLength')
            ->add('sort')
//            ->add('servicesNeeded', 'entity', [
//                'class' => 'AndresGotta\Bundle\ServiceBundle\Entity\Service',
//                'property' => 'name',
//                'multiple' => true,
//                'expanded' => true
//            ])
        ;
    }

    /**
     * @inheritdoc
     */
    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults([
            'data_class' => 'AndresGotta\Bundle\GolfBundle\Entity\Comparative',
            'attr' => ['novalidate' => true]
        ]);
    }

    /**
     * @inheritdoc
     */
    public function getName()
    {
        return 'comparative_type';
    }

}