<?php

namespace AndresGotta\Bundle\GolfBundle\Form;

use AndresGotta\Bundle\GolfBundle\ValueObject\FeelingStatus;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolverInterface;

class PostCompetitiveQuestionnaireType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('feelingStatus', 'choice', array(
                'choices'  => FeelingStatus::getChoices(),
                'choices_as_values' => true,
                'expanded' => true
            ))
            ->add('submit', 'submit')
        ;
    }

    public function setDefaultOptions(OptionsResolverInterface $resolver)
    {
        $resolver->setDefaults(array(
        ));
    }

    public function getName()
    {
        return 'post_competitive_questionnaire_type';
    }
}
