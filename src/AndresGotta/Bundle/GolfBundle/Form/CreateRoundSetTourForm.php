<?php

namespace AndresGotta\Bundle\GolfBundle\Form;

use AndresGotta\Bundle\GolfBundle\ValueObject\PlayerCondition;
use AndresGotta\Bundle\GolfBundle\ValueObject\Handicap;
use AndresGotta\Bundle\GolfBundle\ValueObject\TournamentType;
use AndresGotta\Bundle\GolfBundle\ValueObject\RoundType as RoundTypeChoices;
use AndresGotta\Bundle\GolfBundle\ValueObject\GameCondition;
use AndresGotta\Bundle\GolfBundle\ValueObject\FieldType;
use AndresGotta\Bundle\GolfBundle\ValueObject\FieldPar;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolverInterface;

class CreateRoundSetTourForm extends AbstractType
{

    /**
     * @param FormBuilderInterface $builder
     * @param array $options
     */
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
                ->add('tour', 'entity', array(
                    'class' => 'AndresGottaGolfBundle:Tour',
                    'query_builder' => function ($repo) {
                        return $repo->createQueryBuilder('Tour')->orderBy('Tour.name', 'asc');
                    },
                ))
        ;
    }

    /**
     * @param OptionsResolverInterface $resolver
     */
    public function setDefaultOptions(OptionsResolverInterface $resolver)
    {
        $resolver->setDefaults(array(
            'data_class' => 'AndresGotta\Bundle\GolfBundle\Entity\Round'
        ));
    }

    /**
     * @return string
     */
    public function getName()
    {
        return 'andresgotta_bundle_golfbundle_round';
    }

}
