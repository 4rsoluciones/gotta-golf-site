<?php

namespace AndresGotta\Bundle\GolfBundle\Form;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolverInterface;

class HoleType extends AbstractType
{

    /**
     * @param FormBuilderInterface $builder
     * @param array $options
     */
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
                ->add('number')
                ->add('design', 'choice', array(
                    'choices' => array(
                        'left' => 'left',
                        'straight' => 'straight',
                        'right' => 'right',
                    ),
                    'expanded' => true,
                ))
                ->add('par', 'choice', array(
                    'choices' => array(
                        3 => 3,
                        4 => 4,
                        5 => 5,
                    ),
                    'expanded' => true,
                ))
                
                ->add('penalityShotsCount')
                ->add('description')
        ;
    }

    /**
     * @param OptionsResolverInterface $resolver
     */
    public function setDefaultOptions(OptionsResolverInterface $resolver)
    {
        $resolver->setDefaults(array(
            'data_class' => 'AndresGotta\Bundle\GolfBundle\Entity\Hole'
        ));
    }

    /**
     * @return string
     */
    public function getName()
        {
        return 'andresgotta_bundle_golfbundle_hole';
    }

}
