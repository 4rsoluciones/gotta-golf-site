<?php

namespace AndresGotta\Bundle\GolfBundle\Form\Api;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;

class PhysicalQuestionnaireType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('feelsEnergetic', 'checkbox', array(
                'label' => 'con energías',
                'attr' => array(
                    'class' => 'feelsEnergeticIcon',
                ),
            ))
            ->add('feelsGettingTired', 'checkbox', array(
                'label' => 'empezando a cansarme',
                'attr' => array(
                    'class' => 'feelsGettingTiredIcon',
                ),
            ))
            ->add('feelsPainful', 'checkbox', array(
                'label' => 'con dolores',
                'attr' => array(
                    'class' => 'feelsPainfulIcon',
                ),
            ))
            ->add('feelsTired', 'checkbox', array(
                'label' => 'cansado',
                'attr' => array(
                    'class' => 'feelsTiredIcon',
                ),
            ))
            ->add('feelsExhausted', 'checkbox', array(
                'label' => 'agotado',
                'attr' => array(
                    'class' => 'feelsExhaustedIcon',
                ),
            ));
    }

    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults(array(
            'data_class' => 'AndresGotta\Bundle\GolfBundle\Entity\PhysicalQuestionnaire',
            'label' => '¿Cómo estabas físicamente?',
            'attr' => array(
                'novalidate' => 'novalidate',
                'class' => 'physicalQuestionnaire_control_group',
            ),
        ));
    }

    public function getName()
    {
        return 'physical_questionnaire_type';
    }
}
