<?php

namespace AndresGotta\Bundle\GolfBundle\Form\Api;


use AndresGotta\Bundle\GolfBundle\Entity\DefaultHole;
use AndresGotta\Bundle\GolfBundle\Repository\DefaultFieldRepositoryInterface;
use AndresGotta\Bundle\GolfBundle\ValueObject\HoleDesign;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\Form\FormEvents;
use Symfony\Component\OptionsResolver\OptionsResolverInterface;
use Symfony\Component\Validator\Constraints\NotBlank;
use Symfony\Component\Form\FormEvent;

class DefaultHoleType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('number', 'integer', [
                'required' => true,
            ])
            ->add('design', 'choice', [
                'choices' => HoleDesign::getChoices(),
            ])
            ->add('par', 'integer', [
                'required' => true,
            ])

            ->add('country', 'text', [
                'required' => true,
                'mapped' => false,
                'constraints' => [
                    new NotBlank(),
                ],
            ])
            ->add('route', 'text', [
                'required' => true,
                'mapped' => false,
                'constraints' => [
                    new NotBlank(),
                ],
            ])
            ->add('sportClubName', 'text', [
                'required' => true,
                'mapped' => false,
                'constraints' => [
                    new NotBlank(),
                ],
            ])
            ->add('startPoint', 'text', [
                'required' => true,
                'mapped' => false,
                'constraints' => [
                    new NotBlank(),
                ],
            ])
        ;

        /** @var DefaultFieldRepositoryInterface $repository */
        $repository = $options['default_field_repository'];
        $builder->addEventListener(FormEvents::SUBMIT, function (FormEvent $event) use ($repository) {
            /** @var DefaultHole $data */
            $data = $event->getData();
            if (!$data) {
                return;
            }

            $country = $event->getForm()->get('country')->getData();
            $route = $event->getForm()->get('route')->getData();
            $sportClubName = $event->getForm()->get('sportClubName')->getData();
            $startPoint = $event->getForm()->get('startPoint')->getData();

            $defaultField = $repository->findOneByFieldIdentifiers($country, $route, $sportClubName, $startPoint);
            if ($defaultField) {
                $form = $event->getForm();
                $form->add('defaultField');
                $data = $form->getData();
                $data->setDefaultField($defaultField);
                $event->setData($data);
            }
        });
    }

    public function setDefaultOptions(OptionsResolverInterface $resolver)
    {
        $resolver->setDefaults(array(
            'data_class' => 'AndresGotta\Bundle\GolfBundle\Entity\DefaultHole'
        ));
        $resolver->setRequired([
            'default_field_repository'
        ]);
        $resolver->setAllowedTypes([
            'default_field_repository' => DefaultFieldRepositoryInterface::class
        ]);
    }

    public function getName()
    {
        return 'default_hole';
    }
}
