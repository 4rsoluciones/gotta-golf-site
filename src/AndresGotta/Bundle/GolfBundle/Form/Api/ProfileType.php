<?php

namespace AndresGotta\Bundle\GolfBundle\Form\Api;

use AndresGotta\Bundle\GolfBundle\Entity\Tour;
use AndresGotta\Bundle\GolfBundle\ValueObject\Club;
use AndresGotta\Bundle\GolfBundle\ValueObject\Country;
use AndresGotta\Bundle\GolfBundle\ValueObject\Handicap;
use AndresGotta\Bundle\GolfBundle\ValueObject\PlayerCondition;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\Form\FormEvent;
use Symfony\Component\Form\FormEvents;
use Symfony\Component\OptionsResolver\OptionsResolverInterface;
use WebFactory\Bundle\UserBundle\Entity\Profile;

class ProfileType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('condition', 'choice', array(
                'choices' => Profile::getConditionChoices(),
            ))

            ->add('handicap', 'choice', array(
                'choices' => array_reverse(Handicap::getChoices(), true),
                'required' => false,
            ))
            ->add('country', 'choice', array(
                'choices' => Country::getChoices(),
            ))
            ->add('tour', 'entity', array(
                'class' => Tour::class,
                'required' => false,
            ))
        ;

//        $builder->addEventListener(FormEvents::PRE_SET_DATA, function(FormEvent $event) {
//            /** @var Profile $profile */
//            $profile = $event->getData();
//            if (!$profile) {
//                return;
//            }
//            $form = $event->getForm();
//
//            if ($profile->getCondition() === PlayerCondition::PROFESSIONAL) {
//                $form->remove('handicap');
//            } else {
//                $form->remove('tour');
//            }
//        });

        $builder->addEventListener(FormEvents::PRE_SUBMIT, function(FormEvent $event) {
            /** @var Profile $profile */
            $profile = $event->getData();
            if (!$profile) {
                return;
            }

            if (empty($profile['condition'])) {
                return;
            }

            $form = $event->getForm();

            if ($profile['condition'] === PlayerCondition::PROFESSIONAL) {
                $form->remove('handicap');
            } else {
                $form->remove('tour');
            }
        });
    }

    public function setDefaultOptions(OptionsResolverInterface $resolver)
    {
        $resolver->setDefaults(array(
            'data_class' => 'WebFactory\Bundle\UserBundle\Entity\Profile'
        ));
    }

    public function getName()
    {
        return 'profile';
    }
}
