<?php

namespace AndresGotta\Bundle\GolfBundle\Form\Api;

use AndresGotta\Bundle\GolfBundle\Model\DistanceSelector;
use AndresGotta\Bundle\GolfBundle\Model\ReasonSelector;
use AndresGotta\Bundle\GolfBundle\Model\ResultSelector;
use AndresGotta\Bundle\GolfBundle\ValueObject\BallFlight;
use AndresGotta\Bundle\GolfBundle\ValueObject\Club;
use AndresGotta\Bundle\GolfBundle\ValueObject\LandSlope;
use AndresGotta\Bundle\GolfBundle\ValueObject\StrikeLie;
use AndresGotta\Bundle\GolfBundle\ValueObject\StrikeType as StrikeTypes;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\Form\FormEvent;
use Symfony\Component\Form\FormEvents;
use Symfony\Component\OptionsResolver\OptionsResolverInterface;

class StrikeType extends AbstractType
{

    /**
     * @param FormBuilderInterface $builder
     * @param array $options
     */
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $distanceSelector = new DistanceSelector();
        $resultSelector = new ResultSelector();
        $reasonSelector = new ReasonSelector();

        $builder
            ->add('type', 'choice', [
                'choices' => StrikeTypes::getChoices(),
            ])
            ->add('distance', 'choice', [
                'choices' => $distanceSelector->getChoices(StrikeTypes::TYPE_FIRST, 3),
            ])
            ->add('result', 'choice', [
                'choices' => $resultSelector->getChoices(StrikeTypes::TYPE_FIRST, 3),
            ])
            ->add('reason', 'choice', [
                'choices' => $resultSelector->getChoices(StrikeTypes::TYPE_FIRST, 3),
            ])
            ->add('ballFlight', 'choice', [
                'choices' => BallFlight::getChoices(),
            ])
            ->add('club', 'choice', [
                'choices' => Club::getChoices(),
            ])
            ->add('strikeLies', 'choice', [
                'choices' => StrikeLie::getChoices(),
                'multiple' => true,
            ])
            ->add('landSlopes', 'choice', [
                'choices' => LandSlope::getChoices(),
                'multiple' => true,
            ])
            ->add('emotionalQuestionnaire', new EmotionalQuestionnaireType(), [
                'error_bubbling' => false,
            ])
            ->add('foodQuestionnaire', new FoodQuestionnaireType(), [
                'error_bubbling' => false,
            ])
            ->add('physicalQuestionnaire', new PhysicalQuestionnaireType(), [
                'error_bubbling' => false,
            ]);

        $builder->addEventListener(FormEvents::PRE_SUBMIT, function (FormEvent $event) use ($options, $distanceSelector, $resultSelector, $reasonSelector) {
            $data = $event->getData();
            if (!$data) {
                return;
            }

            $par = $options['par'];
            if (!$par) {
                return;
            }

            $form = $event->getForm();
            if ($data['type'] === StrikeTypes::TYPE_FIRST) {
                $form->remove('strikeLies');
                $form->remove('landSlopes');
                $form->add('distance', 'choice', [
                    'choices' => $distanceSelector->getChoices(StrikeTypes::TYPE_FIRST, $par),
                ]);
                $form->add('result', 'choice', [
                    'choices' => $resultSelector->getChoices(StrikeTypes::TYPE_FIRST, $par),
                ]);
                $form->add('reason', 'choice', [
                    'choices' => $reasonSelector->getChoices(StrikeTypes::TYPE_FIRST, $data['result']),
                ]);
            } elseif ($data['type'] === StrikeTypes::TYPE_OTHER) {
                $form->remove('club');
                $form->remove('landSlopes');
                $form->add('distance', 'choice', [
                    'choices' => $distanceSelector->getChoices(StrikeTypes::TYPE_OTHER, $par),
                ]);
                $form->add('result', 'choice', [
                    'choices' => $resultSelector->getChoices(StrikeTypes::TYPE_OTHER, $par),
                ]);
                $form->add('reason', 'choice', [
                    'choices' => $reasonSelector->getChoices(StrikeTypes::TYPE_OTHER, $data['result']),
                ]);
            } elseif ($data['type'] === StrikeTypes::TYPE_PUTT) {
                $form->remove('club');
                $form->remove('ballFlight');
                $form->remove('strikeLies');
                $form->add('distance', 'choice', [
                    'choices' => $distanceSelector->getChoices(StrikeTypes::TYPE_PUTT, $par),
                ]);
                $form->add('result', 'choice', [
                    'choices' => $resultSelector->getChoices(StrikeTypes::TYPE_PUTT, $par),
                ]);
                $form->add('reason', 'choice', [
                    'choices' => $reasonSelector->getChoices(StrikeTypes::TYPE_PUTT, $data['result']),
                ]);
            }
        });
    }

    /**
     * @param OptionsResolverInterface $resolver
     */
    public function setDefaultOptions(OptionsResolverInterface $resolver)
    {
        $resolver->setDefaults([
            'data_class' => 'AndresGotta\Bundle\GolfBundle\Entity\Strike',
            'par' => null,
        ]);
    }

    /**
     * @return string
     */
    public function getName()
    {
        return 'strike';
    }

}
