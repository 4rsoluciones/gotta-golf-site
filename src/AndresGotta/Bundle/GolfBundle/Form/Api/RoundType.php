<?php

namespace AndresGotta\Bundle\GolfBundle\Form\Api;

use AndresGotta\Bundle\GolfBundle\Entity\Round;
use AndresGotta\Bundle\GolfBundle\ValueObject\GameCondition;
use AndresGotta\Bundle\GolfBundle\ValueObject\Handicap;
use AndresGotta\Bundle\GolfBundle\ValueObject\PlayerCondition;
use AndresGotta\Bundle\GolfBundle\ValueObject\RoundType as RoundTypeChoices;
use AndresGotta\Bundle\GolfBundle\ValueObject\TournamentType;
use Doctrine\ORM\EntityRepository;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\Form\FormEvent;
use Symfony\Component\Form\FormEvents;
use Symfony\Component\OptionsResolver\OptionsResolverInterface;
use WebFactory\Bundle\UserBundle\Entity\User;

class RoundType extends AbstractType
{

    /**
     * @param FormBuilderInterface $builder
     * @param array $options
     */
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('performed_at', 'date', [
                'widget' => 'single_text',
                'property_path' => 'performedAt',
            ])
            ->add('player_condition', 'choice', [
                'choices' => PlayerCondition::getChoices(),
                'property_path' => 'playerCondition',
            ])
            ->add('handicap', 'choice', [
                'choices' => Handicap::getChoices(),
                'required' => false,
            ])
            ->add('tour', 'entity', [
                'required' => false,
                'class' => 'AndresGottaGolfBundle:Tour',
                'query_builder' => function (EntityRepository $repo) {
                    return $repo->createQueryBuilder('Tour')->orderBy('Tour.name', 'asc');
                },
            ])
            ->add('round_type', 'choice', [
                'choices' => RoundTypeChoices::getChoices(),
                'property_path' => 'roundType',
            ])
            ->add('tournament_type', 'choice', [
                'choices' => TournamentType::getChoices(),
                'property_path' => 'tournamentType',
            ])
            ->add('round_number', 'choice', [
                'choices' => array_combine(range(1, 4), range(1, 4)),
                'property_path' => 'roundNumber',
            ])
            ->add('game_condition', 'choice', [
                'choices' => GameCondition::getChoices(),
                'property_path' => 'gameCondition',
            ])
            ->add('field', new FieldType())
            ->add('played_holes', 'choice', [
                'choices' => array_combine([9, 18],[9, 18]),
                'property_path' => 'playedHoles',
            ])
        ;

        $builder->addEventListener(FormEvents::PRE_SET_DATA, function (FormEvent $event) {
            /** @var Round $round */
            $round = $event->getData();
            if (!$round) {
                return;
            }
            $form = $event->getForm();

            if ($round->getId()) {
                $form->remove('playedHoles');
            }
        });

        //todo
        $builder->addEventListener(FormEvents::PRE_SUBMIT, function (FormEvent $event) {
            /** @var Round $round */
            $round = $event->getData();
            if (!$round) {
                return;
            }
            $form = $event->getForm();

            if ($round['player_condition'] === PlayerCondition::PROFESSIONAL) {
                $form->remove('handicap');
            } else {
                $form->remove('tour');
            }
        });
    }

    /**
     * @param OptionsResolverInterface $resolver
     */
    public function setDefaultOptions(OptionsResolverInterface $resolver)
    {
        $resolver->setDefaults([
            'data_class' => 'AndresGotta\Bundle\GolfBundle\Entity\Round',
            'validation_groups' => ['OnlyRound'],
        ]);
    }

    /**
     * @return string
     */
    public function getName()
    {
        return 'round';
    }

}
