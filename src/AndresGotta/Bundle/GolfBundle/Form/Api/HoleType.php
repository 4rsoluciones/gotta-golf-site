<?php

namespace AndresGotta\Bundle\GolfBundle\Form\Api;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\Form\FormEvent;
use Symfony\Component\Form\FormEvents;
use Symfony\Component\OptionsResolver\OptionsResolverInterface;

class HoleType extends AbstractType
{

    /**
     * @param FormBuilderInterface $builder
     * @param array $options
     */
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('number')
            ->add('par')
            ->add('design')
            ->add('strikes', 'collection', [
                'type' => new StrikeType(),
                'allow_add' => true,
                'allow_delete' => true,
                'by_reference' => false,
                'error_bubbling' => false,
            ])
            ->add('penalityShotsCount')
            ->add('description')
        ;

        $builder->addEventListener(FormEvents::PRE_SUBMIT, function (FormEvent $event) {
            $data = $event->getData();
            if (!$data) {
                return;
            }

            $event->getForm()->add('strikes', 'collection', [
                'type' => new StrikeType(),
                'allow_add' => true,
                'allow_delete' => true,
                'by_reference' => false,
                'error_bubbling' => false,
                'options' => [
                    'par' => $data['par'],
                ]
            ]);
        });
    }

    /**
     * @param OptionsResolverInterface $resolver
     */
    public function setDefaultOptions(OptionsResolverInterface $resolver)
    {
        $resolver->setDefaults([
            'data_class' => 'AndresGotta\Bundle\GolfBundle\Entity\Hole',
        ]);
    }

    /**
     * @return string
     */
    public function getName()
    {
        return 'hole';
    }

}
