<?php

namespace AndresGotta\Bundle\GolfBundle\Form\Api;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolverInterface;

class PreCompetitiveQuestionnaireType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('is_warmed_up', 'choice', [
                'choices' => [false, true],
                'property_path' => 'isWarmedUp',
            ])
            ->add('has_practiced', 'choice', [
                'choices' => [false, true],
                'property_path' => 'hasPracticed',
            ])
            ->add('wants_to_play', 'choice', [
                'choices' => [false, true],
                'property_path' => 'wantsToPlay',
            ])
            ->add('has_strategy', 'choice', [
                'choices' => [false, true],
                'property_path' => 'hasStrategy',
            ])
        ;
    }

    public function setDefaultOptions(OptionsResolverInterface $resolver)
    {
        $resolver->setDefaults(array(
            'data_class' => 'AndresGotta\Bundle\GolfBundle\Entity\PreCompetitiveQuestionnaire'
        ));
    }

    public function getName()
    {
        return 'pre_competitive_questionnaire';
    }
}
