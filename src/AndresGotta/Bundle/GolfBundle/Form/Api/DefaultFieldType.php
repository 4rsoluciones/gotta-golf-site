<?php

namespace AndresGotta\Bundle\GolfBundle\Form\Api;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolverInterface;

class DefaultFieldType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('country')
            ->add('type')
            ->add('par')
            ->add('route')
            ->add('hole_count', null, [
                'property_path' => 'holeCount',
            ])
            ->add('sport_club_name', null, [
                'property_path' => 'sportClubName',
            ])
            ->add('start_point', null, [
                'property_path' => 'startPoint',
            ])
            ->add('topographic_movements', null, [
                'property_path' => 'topographicMovements',
            ])
        ;
    }

    public function setDefaultOptions(OptionsResolverInterface $resolver)
    {
        $resolver->setDefaults(array(
            'data_class' => 'AndresGotta\Bundle\GolfBundle\Entity\DefaultField',
            'error_bubbling' => false,
        ));
    }

    public function getName()
    {
        return 'default_field';
    }
}
