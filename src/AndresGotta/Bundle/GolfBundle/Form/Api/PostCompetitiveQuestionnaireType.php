<?php

namespace AndresGotta\Bundle\GolfBundle\Form\Api;

use AndresGotta\Bundle\GolfBundle\ValueObject\FeelingStatus;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolverInterface;

class PostCompetitiveQuestionnaireType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('feeling_status', 'choice', array(
                'choices'  => FeelingStatus::getChoices(),
                'choices_as_values' => true,
                'expanded' => true,
                'property_path' => 'feelingStatus',
            ))
        ;
    }

    public function setDefaultOptions(OptionsResolverInterface $resolver)
    {
        $resolver->setDefaults(array(
            'data_class' => 'AndresGotta\Bundle\GolfBundle\Entity\PostCompetitiveQuestionnaire'
        ));
    }

    public function getName()
    {
        return 'post_competitive_questionnaire';
    }
}
