<?php

namespace AndresGotta\Bundle\GolfBundle\Form\Api;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;

class EmotionalQuestionnaireType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('feelsUptight', 'checkbox', array(
                'label' => 'tenso',
                'attr' => array(
                    'class' => 'feelsUptightIcon',
                ),
            ))
            ->add('feelsDistrustful', 'checkbox', array(
                'label' => 'sin confianza',
                'attr' => array(
                    'class' => 'feelsDistrustfulIcon',
                ),
            ))
            ->add('feelsAnnoyed', 'checkbox', array(
                'label' => 'enojado',
                'attr' => array(
                    'class' => 'feelsAnnoyedIcon',
                ),
            ))
            ->add('feelsFrustrated', 'checkbox', array(
                'label' => 'frustrado',
                'attr' => array(
                    'class' => 'feelsFrustratedIcon',
                ),
            ))
            ->add('feelsTrustful', 'checkbox', array(
                'label' => 'con confianza',
                'attr' => array(
                    'class' => 'feelsTrustfulIcon',
                ),
            ))
            ->add('feelsEnjoying', 'checkbox', array(
                'label' => 'disfrutando',
                'attr' => array(
                    'class' => 'feelsEnjoyingIcon',
                ),
            ))
            ->add('feelsFocused', 'checkbox', array(
                'label' => 'enfocado',
                'attr' => array(
                    'class' => 'feelsFocusedIcon',
                ),
            ));
    }

    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults(array(
            'data_class' => 'AndresGotta\Bundle\GolfBundle\Entity\EmotionalQuestionnaire',
            'label' => '¿Cómo te sentías emocionalmente?',
            'attr' => array(
                'novalidate' => 'novalidate',
            ),
        ));
    }

    public function getName()
    {
        return 'emotional_questionnaire_type';
    }
}
