<?php

namespace AndresGotta\Bundle\GolfBundle\Form;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Symfony\Component\Validator\Constraints\NotNull;

/**
 * Class FeedbackType
 * @package AndresGotta\Bundle\GolfBundle\Form
 */
class FeedbackType extends AbstractType
{
    /**
     * @param FormBuilderInterface $builder
     * @param array $options
     */
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('comment', 'textarea', [
                'constraints' => [new NotNull(['message' => 'Describa el problema que experimentó'])]
            ])
        ;
    }

    /**
     * @param OptionsResolver $resolver
     */
    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults([
            'data_class' => null,
            'label' => false,
            'attr' => ['novalidate' => true]
        ]);
    }

    /**
     * @return string
     */
    public function getName()
    {
        return 'feedback';
    }

}