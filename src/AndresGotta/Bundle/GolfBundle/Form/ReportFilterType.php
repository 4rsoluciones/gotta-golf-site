<?php

namespace AndresGotta\Bundle\GolfBundle\Form;

use AndresGotta\Bundle\GolfBundle\ValueObject\Handicap;
use AndresGotta\Bundle\GolfBundle\ValueObject\RoundType;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;

/**
 * Class ReportFilterType
 * @package AndresGotta\Bundle\GolfBundle\Form
 */
class ReportFilterType extends AbstractType
{

    /**
     * @param FormBuilderInterface $builder
     * @param array $options
     */
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('par', 'hidden')
            ->add('from_date', 'date', array('required' => false))
            ->add('to_date', 'date', array('required' => false))
            ->add('roundType', 'choice', array(
                'choices' => RoundType::getChoices(),
                'required' => false
            ))
            ->add('handicap', 'choice', array('choices' => array_reverse(Handicap::getChoices(), true), 'required' => false))
            ->add('submit', 'submit', array('label' => 'Filter'))
        ;
    }

    /**
     * @param OptionsResolver $resolver
     */
    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults(array(
            'data_class' => null,
            'validation_groups' => array(),
            'attr' => ['novalidate' => true, 'id' => 'report-filter-form']
        ));
    }

    /**
     * @return string
     */
    public function getName()
    {
        return 'andresgotta_bundle_golfbundle_reportfilter';
    }

}
