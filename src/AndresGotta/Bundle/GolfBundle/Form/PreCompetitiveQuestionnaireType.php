<?php

namespace AndresGotta\Bundle\GolfBundle\Form;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolverInterface;

class PreCompetitiveQuestionnaireType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('isWarmedUp', 'choice', array(
                'choices'  => array(
                    'yes' => true,
                    'no' => false,
                ),
                'choices_as_values' => true,
                'expanded' => true,
                'data' => false,
            ))
            ->add('hasPracticed', 'choice', array(
                'choices'  => array(
                    'yes' => true,
                    'no' => false,
                ),
                'choices_as_values' => true,
                'expanded' => true,
                'data' => false,
            ))
            ->add('wantsToPlay', 'choice', array(
                'choices'  => array(
                    'yes' => true,
                    'no' => false,
                ),
                'choices_as_values' => true,
                'expanded' => true,
                'data' => false,
            ))
            ->add('hasStrategy', 'choice', array(
                'choices'  => array(
                    'yes' => true,
                    'no' => false,
                ),
                'choices_as_values' => true,
                'expanded' => true,
                'data' => false,
            ))
            ->add('submit', 'submit')
        ;
    }

    public function setDefaultOptions(OptionsResolverInterface $resolver)
    {
        $resolver->setDefaults(array(
        ));
    }

    public function getName()
    {
        return 'pre_competitive_questionnaire_type';
    }
}
