<?php

namespace AndresGotta\Bundle\GolfBundle\CQRS\Query;

use WebFactory\Bundle\UserBundle\Entity\User;

final class GetUserRoundsQuery
{
    /**
     * @var boolean|null
     */
    private $incomplete;
    /**
     * @var int
     */
    private $limit;
    /**
     * @var int
     */
    private $offset;
    /**
     * @var User
     */
    private $user;

    /**
     * GetMyRoundsQuery constructor.
     * @param User $user
     * @param bool|null $incomplete
     * @param int $limit
     * @param int $offset
     */
    public function __construct(User $user, $incomplete = null, $limit = 20, $offset = 0)
    {
        $this->incomplete = $incomplete;
        $this->limit = $limit;
        $this->offset = $offset;
        $this->user = $user;
    }

    /**
     * @return bool|null
     */
    public function getIncomplete()
    {
        return $this->incomplete;
    }

    /**
     * @return int
     */
    public function getLimit()
    {
        return $this->limit;
    }

    /**
     * @return int
     */
    public function getOffset()
    {
        return $this->offset;
    }

    /**
     * @return User
     */
    public function getUser()
    {
        return $this->user;
    }

}