<?php

namespace AndresGotta\Bundle\GolfBundle\CQRS\Query;

final class GetDefaultFieldsQuery
{
    /**
     * @var string
     */
    private $country;
    /**
     * @var string
     */
    private $terms;

    /**
     * GetDefaultFieldsQuery constructor.
     * @param string $country
     * @param string $terms
     */
    public function __construct($country = null, $terms = null)
    {
        $this->country = $country;
        $this->terms = $terms;
    }

    /**
     * @return string
     */
    public function getCountry()
    {
        return $this->country;
    }

    /**
     * @return string
     */
    public function getTerms()
    {
        return $this->terms;
    }

}