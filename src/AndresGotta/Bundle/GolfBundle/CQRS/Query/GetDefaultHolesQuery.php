<?php

namespace AndresGotta\Bundle\GolfBundle\CQRS\Query;

final class GetDefaultHolesQuery
{
    /**
     * @var string
     */
    private $country;
    /**
     * @var string
     */
    private $sportClubName;
    /**
     * @var string
     */
    private $route;
    /**
     * @var string
     */
    private $startPoint;

    /**
     * GetDefaultHolesQuery constructor.
     * @param string $country
     * @param string $sportClubName
     * @param string $route
     * @param string $startPoint
     */
    public function __construct($country, $sportClubName, $route, $startPoint)
    {
        $this->country = $country;
        $this->sportClubName = $sportClubName;
        $this->route = $route;
        $this->startPoint = $startPoint;
    }

    /**
     * @return string
     */
    public function getCountry()
    {
        return $this->country;
    }

    /**
     * @return string
     */
    public function getSportClubName()
    {
        return $this->sportClubName;
    }

    /**
     * @return string
     */
    public function getRoute()
    {
        return $this->route;
    }

    /**
     * @return string
     */
    public function getStartPoint()
    {
        return $this->startPoint;
    }

}