<?php

namespace AndresGotta\Bundle\GolfBundle\CQRS\Query;

use WebFactory\Bundle\UserBundle\Entity\User;

final class GetUserComparativeStatsQuery
{

    /**
     * @var User
     */
    private $user;

    /**
     * GetUserComparativeStatsQuery constructor.
     * @param User $user
     */
    public function __construct(User $user)
    {
        $this->user = $user;
    }

    /**
     * @return User
     */
    public function getUser()
    {
        return $this->user;
    }

}