<?php

namespace AndresGotta\Bundle\GolfBundle\CQRS\QueryHandler;

use AndresGotta\Bundle\GolfBundle\CQRS\Query\GetUserComparativeStatsQuery;
use AndresGotta\Bundle\GolfBundle\Manager\ComparativeManagerInterface;
use AndresGotta\Bundle\GolfBundle\Repository\ComparativeRepositoryInterface;

final class GetUserComparativeStatsQueryHandler
{
    /**
     * @var ComparativeRepositoryInterface
     */
    private $comparativeRepository;
    /**
     * @var ComparativeManagerInterface
     */
    private $comparativeManager;

    /**
     * GetUserComparativeStatsQueryHandler constructor.
     * @param ComparativeRepositoryInterface $comparativeRepository
     * @param ComparativeManagerInterface $comparativeManager
     */
    public function __construct(ComparativeRepositoryInterface $comparativeRepository, ComparativeManagerInterface $comparativeManager)
    {
        $this->comparativeRepository = $comparativeRepository;
        $this->comparativeManager = $comparativeManager;
    }

    /**
     * @param GetUserComparativeStatsQuery $query
     * @return
     */
    public function handle(GetUserComparativeStatsQuery $query)
    {
        $user = $query->getUser();

        $comparatives = $this->comparativeRepository->findAllHighlighted();
        $stats = [];
        foreach ($comparatives as $comparative) {
            $result = $this->comparativeManager->getComparativeResults($comparative, $user);
            $stats[$comparative->getSlug()] = $result;
        }

        return $stats;
    }


}