<?php

namespace AndresGotta\Bundle\GolfBundle\CQRS\QueryHandler;

use AndresGotta\Bundle\GolfBundle\CQRS\Query\GetDefaultFieldsQuery;
use AndresGotta\Bundle\GolfBundle\Entity\DefaultField;
use AndresGotta\Bundle\GolfBundle\Repository\DefaultFieldRepositoryInterface;

final class GetDefaultFieldsQueryHandler
{
    /**
     * @var DefaultFieldRepositoryInterface
     */
    private $repository;

    /**
     * GetDefaultFieldsQueryHandler constructor.
     * @param DefaultFieldRepositoryInterface $repository
     */
    public function __construct(DefaultFieldRepositoryInterface $repository)
    {
        $this->repository = $repository;
    }

    /**
     * @param GetDefaultFieldsQuery $query
     * @return DefaultField[]
     */
    public function handle(GetDefaultFieldsQuery $query)
    {
        $country = $query->getCountry();
        $terms = $query->getTerms();

        $defaultFields = $this->repository->findByCountryAndTerms($country, $terms);

        return $defaultFields;
    }
}