<?php

namespace AndresGotta\Bundle\GolfBundle\CQRS\QueryHandler;

use AndresGotta\Bundle\GolfBundle\CQRS\Query\GetDefaultFieldsQuery;
use AndresGotta\Bundle\GolfBundle\CQRS\Query\GetDefaultHolesQuery;
use AndresGotta\Bundle\GolfBundle\Entity\DefaultField;
use AndresGotta\Bundle\GolfBundle\Entity\DefaultHole;
use AndresGotta\Bundle\GolfBundle\Repository\DefaultFieldRepositoryInterface;
use AndresGotta\Bundle\GolfBundle\Repository\DefaultHoleRepositoryInterface;

final class GetDefaultHolesQueryHandler
{
    /**
     * @var DefaultHoleRepositoryInterface
     */
    private $repository;

    /**
     * GetDefaultHolesQueryHandler constructor.
     * @param DefaultHoleRepositoryInterface $repository
     */
    public function __construct(DefaultHoleRepositoryInterface $repository)
    {
        $this->repository = $repository;
    }

    /**
     * @param GetDefaultHolesQuery $query
     * @return DefaultHole[]
     */
    public function handle(GetDefaultHolesQuery $query)
    {
        $country = $query->getCountry();
        $route = $query->getRoute();
        $sportClubName = $query->getSportClubName();
        $startPoint = $query->getStartPoint();

        $defaultFields = $this->repository->findByDefaultField($country, $route, $sportClubName, $startPoint);

        return $defaultFields;
    }
}