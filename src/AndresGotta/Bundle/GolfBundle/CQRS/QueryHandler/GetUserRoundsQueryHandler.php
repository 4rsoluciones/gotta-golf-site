<?php

namespace AndresGotta\Bundle\GolfBundle\CQRS\QueryHandler;

use AndresGotta\Bundle\GolfBundle\CQRS\Query\GetUserRoundsQuery;
use AndresGotta\Bundle\GolfBundle\Entity\Round;

final class GetUserRoundsQueryHandler
{
    /**
     * @param GetUserRoundsQuery $query
     * @return Round[]
     */
    public function handle(GetUserRoundsQuery $query)
    {
        $user = $query->getUser();
        $incomplete = $query->getIncomplete();
        $offset = $query->getOffset();
        $limit = $query->getLimit();

        //hoy cuando pido vueltas completas me devuelve las completas por mas que no esten finalizadas.
        //la idea seria que cuando pido vueltas completas me devuelva completas&finalizadas.
        //Y cuando pido incompletas me devuelva tambien las que estan completas pero no finalizadas

        return array_values($user->getRounds()->filter(function (Round $round) use ($incomplete) {
            if (is_null($incomplete)) {
                return $round;
            }

            if ($incomplete === 'true') {
                return !$round->isCompleted() || ($round->isCompleted() && !$round->getPlayerConfirmed());
            } else {
                return $round->isCompleted() && $round->getPlayerConfirmed();
            }
        })->slice($offset, $limit));
    }
}