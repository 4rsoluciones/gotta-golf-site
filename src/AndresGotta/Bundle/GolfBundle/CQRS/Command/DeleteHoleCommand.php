<?php

namespace AndresGotta\Bundle\GolfBundle\CQRS\Command;

class DeleteHoleCommand
{
    /**
     * @var integer
     */
    private $holeId;

    /**
     * DeleteHoleCommand constructor.
     * @param int $holeId
     */
    public function __construct($holeId)
    {
        $this->holeId = $holeId;
    }

    /**
     * @return int
     */
    public function getHoleId()
    {
        return $this->holeId;
    }

}