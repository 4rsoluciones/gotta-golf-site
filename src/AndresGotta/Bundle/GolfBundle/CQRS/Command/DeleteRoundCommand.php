<?php

namespace AndresGotta\Bundle\GolfBundle\CQRS\Command;

class DeleteRoundCommand
{
    /**
     * @var integer
     */
    private $roundId;

    /**
     * DeleteRoundCommand constructor.
     * @param int $roundId
     */
    public function __construct($roundId)
    {
        $this->roundId = $roundId;
    }

    /**
     * @return int
     */
    public function getRoundId()
    {
        return $this->roundId;
    }

}