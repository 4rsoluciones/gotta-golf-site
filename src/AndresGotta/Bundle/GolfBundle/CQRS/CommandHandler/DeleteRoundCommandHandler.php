<?php

namespace AndresGotta\Bundle\GolfBundle\CQRS\CommandHandler;

use AndresGotta\Bundle\GolfBundle\CQRS\Command\DeleteRoundCommand;
use Doctrine\ORM\EntityManagerInterface;

class DeleteRoundCommandHandler
{
    /**
     * @var EntityManagerInterface
     */
    private $em;

    /**
     * DeleteRoundCommandHandler constructor.
     * @param EntityManagerInterface $em
     */
    public function __construct(EntityManagerInterface $em)
    {
        $this->em = $em;
    }

    /**
     * @param DeleteRoundCommand $command
     */
    public function handle(DeleteRoundCommand $command)
    {
        $roundRepository = $this->em->getRepository('AndresGottaGolfBundle:Round');
        $round = $roundRepository->find($command->getRoundId());
        $this->em->remove($round);
        $this->em->flush();
    }

}