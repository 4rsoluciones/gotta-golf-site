<?php

namespace AndresGotta\Bundle\GolfBundle\CQRS\CommandHandler;

use AndresGotta\Bundle\GolfBundle\CQRS\Command\DeleteHoleCommand;
use Doctrine\ORM\EntityManagerInterface;

class DeleteHoleCommandHandler
{
    /**
     * @var EntityManagerInterface
     */
    private $em;

    /**
     * DeleteHoleCommandHandler constructor.
     * @param EntityManagerInterface $em
     */
    public function __construct(EntityManagerInterface $em)
    {
        $this->em = $em;
    }

    /**
     * @param DeleteHoleCommand $command
     */
    public function handle(DeleteHoleCommand $command)
    {
        $holeRepository = $this->em->getRepository('AndresGottaGolfBundle:Hole');
        $hole = $holeRepository->find($command->getHoleId());
        $round = $hole->getRound();
        $round->setPlayerConfirmed(false);
        $this->em->persist($round);
        $this->em->remove($hole);
        $this->em->flush();
    }

}