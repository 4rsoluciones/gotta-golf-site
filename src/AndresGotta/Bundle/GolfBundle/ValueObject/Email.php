<?php

namespace AndresGotta\Bundle\GolfBundle\ValueObject;

use AndresGotta\Bundle\GolfBundle\Specification\EmailAddressIsValid;
use Doctrine\ORM\Mapping as ORM;

/**
 * Class Email
 * @package AndresGotta\Bundle\GolfBundle\ValueObject
 * @ORM\Embeddable()
 */
class Email
{
    /**
     * @var string
     * @ORM\Column(type="string", length=255)
     */
    private $address;

    /**
     * Email constructor.
     * @param string $address
     */
    public function __construct($address)
    {
        $this->address = $address;
        $spec = new EmailAddressIsValid();
        if (!$spec->isSatisfiedBy($this)) {
            throw new \InvalidArgumentException("Invalid email address.");
        }
    }

    /**
     * @return string
     */
    public function getAddress()
    {
        return $this->address;
    }

    /**
     * @return string
     */
    public function __toString()
    {
        return $this->address;
    }


}