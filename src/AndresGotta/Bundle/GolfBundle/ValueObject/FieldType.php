<?php

namespace AndresGotta\Bundle\GolfBundle\ValueObject;

/**
 * Field Type
 *
 * @author gaston
 */
class FieldType
{

    const MINOR = 'topographic.movements.minor';
    const MODERATE = 'topographic.movements.moderate';
    const INTENSE = 'topographic.movements.intense';

    /**
     *
     * @var string
     */
    protected $name;

    /**
     *
     * @param string $name
     */
    public function __construct($name)
    {
        $this->name = $name;
    }

    /**
     *
     * @return string
     */
    public function getName()
    {
        return $this->name;
    }

    /**
     * Choices
     *
     * @return array
     */
    public static function getChoices()
    {
        return array(
            static::MINOR => static::MINOR,
            static::MODERATE => static::MODERATE,
            static::INTENSE => static::INTENSE,
        );
    }

}
