<?php

namespace AndresGotta\Bundle\GolfBundle\ValueObject;

/**
 * Class Reason
 */
final class Reason
{
    const TO_THE_LEFT = 'to.the.left';
    const TO_THE_RIGHT = 'to.the.right';
    const LONG = 'long';
    const SHORT = 'short';

}
