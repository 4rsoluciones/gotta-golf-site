<?php

namespace AndresGotta\Bundle\GolfBundle\ValueObject;

class FeelingStatus
{
    const HAPPY = 'feeling_status.happy';
    const SATISFIED = 'feeling_status.satisfied';
    const POSITIVE = 'feeling_status.positive';
    const FRUSTRATED = 'feeling_status.frustrated';
    const ANNOYED = 'feeling_status.annoyed';
    const UNKNOWN = 'feeling_status.unknown';

    // Valores viejos, no deberán ser utilizados
    const OLD_HAPPY = 'Happy';
    const OLD_SATISFIED = 'Satisfied';
    const OLD_POSITIVE = 'Positive';
    const OLD_FRUSTRATED = 'Frustrated';
    const OLD_ANNOYED = 'Annoyed';
    const OLD_UNKNOWN = 'Unknown';

    /**
     * Choices
     *
     * @return array
     */
    public static function getChoices()
    {
        return array(
            static::HAPPY => static::HAPPY,
            static::SATISFIED => static::SATISFIED,
            static::POSITIVE => static::POSITIVE,
            static::FRUSTRATED => static::FRUSTRATED,
            static::ANNOYED => static::ANNOYED,
            static::UNKNOWN => static::UNKNOWN,
        );
    }

    // Los siguientes metodos no deben ser utilizados por el sistema
    // fueron creados para la migración de datos (old -> new)
    /**
     * Choices
     *
     * @return array
     */
    public static function getOldChoices()
    {
        return array(
            static::OLD_HAPPY => static::OLD_HAPPY,
            static::OLD_SATISFIED => static::OLD_SATISFIED,
            static::OLD_POSITIVE => static::OLD_POSITIVE,
            static::OLD_FRUSTRATED => static::OLD_FRUSTRATED,
            static::OLD_ANNOYED => static::OLD_ANNOYED,
            static::OLD_UNKNOWN => static::OLD_UNKNOWN,
        );
    }

    /**
     * @param string $new
     * @return string
     */
    public static function toOldValue($new)
    {
        $map = [
            static::HAPPY => static::OLD_HAPPY,
            static::SATISFIED => static::OLD_SATISFIED,
            static::POSITIVE => static::OLD_POSITIVE,
            static::FRUSTRATED => static::OLD_FRUSTRATED,
            static::ANNOYED => static::OLD_ANNOYED,
            static::UNKNOWN => static::OLD_UNKNOWN,
        ];

        return $map[$new];
    }

    /**
     * @param string $old
     * @return string
     */
    public static function toNewValue($old)
    {
        $map = [
            static::OLD_HAPPY => static::HAPPY,
            static::OLD_SATISFIED => static::SATISFIED,
            static::OLD_POSITIVE => static::POSITIVE,
            static::OLD_FRUSTRATED => static::FRUSTRATED,
            static::OLD_ANNOYED => static::ANNOYED,
            static::OLD_UNKNOWN => static::UNKNOWN,
        ];

        return $map[$old];
    }
}