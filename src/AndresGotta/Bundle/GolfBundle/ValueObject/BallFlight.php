<?php

namespace AndresGotta\Bundle\GolfBundle\ValueObject;

/**
 * Class BallFlight
 */
class BallFlight
{
    use GetChoicesTrait;

    const RIGHT_TO_LEFT = 'ball_flight.right_to_left';
    const STRAIGHT = 'ball_flight.straight';
    const LEFT_TO_RIGHT = 'ball_flight.left_to_right';

    /**
     * Choices
     *
     * @return array
     */
    public static function getChoicesForm()
    {
        return array(
            static::RIGHT_TO_LEFT => '',
            static::STRAIGHT => '',
            static::LEFT_TO_RIGHT => '',
        );
    }

}
