<?php

namespace AndresGotta\Bundle\GolfBundle\ValueObject;

class StrikeType
{
    use GetChoicesTrait;

    const TYPE_FIRST = 'strike_type.first';
    const TYPE_PUTT = 'strike_type.putt';
    const TYPE_OTHER = 'strike_type.other';
}