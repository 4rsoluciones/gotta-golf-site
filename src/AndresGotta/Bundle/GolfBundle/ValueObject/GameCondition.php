<?php

namespace AndresGotta\Bundle\GolfBundle\ValueObject;

/**
 * Class GameCondition
 */
class GameCondition
{
    use GetChoicesTrait;

    const NORMAL = 'clime.normal';
    const WINDY = 'clime.windy';
    const COLD = 'clime.cold';
    const HOT = 'clime.hot';
    const RAINY = 'clime.rainy';

}
