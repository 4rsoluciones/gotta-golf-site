<?php

namespace AndresGotta\Bundle\GolfBundle\ValueObject;

/**
 * RoundType
 *
 * @author gaston
 */
class HoleDesign
{
    use GetChoicesTrait;

    const DOGLEG_LEFT = 'design.dogleg-left';
    const DOGLEG_RIGHT = 'design.dogleg-right';
    const STRAIGHT = 'design.straight';


    /**
     * Choices
     *
     * @return array
     */
    public static function getChoicesForm()
    {
        return array(
            static::DOGLEG_LEFT => '',
            static::STRAIGHT => '',
            static::DOGLEG_RIGHT => '',
        );
    }

}
