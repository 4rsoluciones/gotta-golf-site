<?php

namespace AndresGotta\Bundle\GolfBundle\ValueObject;

/**
 * Strike Lie
 *
 * @author gaston
 */
class StrikeLie
{
    use GetChoicesTrait;

    const BALL_AT_YOUR_FEET = 'strike_lie.ball_at_your_feet';
    const BALL_ABOVE_FEET = 'strike_lie.ball_above_feet';
    const BALL_BELOW_FEET = 'strike_lie.ball_below_feet';
    const ON_CLIMB = 'strike_lie.on_climb';
    const ON_DESCEND = 'strike_lie.on_descend';
    const PLANE = 'strike_lie.plane';

}
