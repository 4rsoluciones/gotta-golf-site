<?php

namespace AndresGotta\Bundle\GolfBundle\ValueObject;

use AndresGotta\Bundle\GolfBundle\Specification\CountryIsValid;

class Country
{
    use GetChoicesTrait;

    const AFGANISTAN = "Afganistán";
    const ALBANIA = "Albania";
    const ALEMANIA = "Alemania";
    const AMERICAN_SAMOA = "American Samoa";
    const ANDORRA = "Andorra";
    const ANGOLA = "Angola";
    const ANGUILA = "Anguila";
    const ANTARTIDA = "Antártida";
    const ANTIGUA_AND_BARBUDA = "Antigua and Barbuda";
    const ANTILLAS_HOLANDESAS = "Antillas Holandesas";
    const ARABIA_SAUDITA = "Arabia Saudita";
    const ARGELIA = "Argelia";
    const ARGENTINA = "Argentina";
    const ARMENIA = "Armenia";
    const ARUBA = "Aruba";
    const AUSTRALIA = "Australia";
    const AUSTRIA = "Austria";
    const AZERBAIJAN = "Azerbaijan";
    const BAHAMAS = "Bahamas";
    const BAHREIN = "Bahrein";
    const BANGLADESH = "Bangladesh";
    const BARBADOS = "Barbados";
    const BELGICA = "Bélgica";
    const BELICE = "Belice";
    const BENIN = "Benin";
    const BERMUDA = "Bermuda";
    const BIELORRUSIA = "Bielorrusia";
    const BOLIVIA = "Bolivia";
    const BOSNIA_Y_HERZEGOVINA = "Bosnia y Herzegovina";
    const BOTSUANA = "Botsuana";
    const BOUVET_ISLAND = "Bouvet Island";
    const BRASIL = "Brasil";
    const BRITISH_INDIAN_OCEAN_TERRITORY = "British Indian Ocean Territory";
    const BRUNEI_DARUSSALAM = "Brunei Darussalam";
    const BULGARIA = "Bulgaria";
    const BURKINA_FASO = "Burkina Faso";
    const BURUNDI = "Burundi";
    const BUTAN = "Bután";
    const CABO_VERDA = "Cabo Verda";
    const CAMBOYA = "Camboya";
    const CAMERUN = "Camerún";
    const CANADA = "Canadá";
    const CHAD = "Chad";
    const CHILE = "Chile";
    const CHINA = "China";
    const CHIPRE = "Chipre";
    const COLOMBIA = "Colombia";
    const COMORES = "Comores";
    const CONGO = "Congo";
    const COREA_DEL_NORTE = "Corea del Norte";
    const COREA_DEL_SUR = "Corea del Sur";
    const COSTA_RICA = "Costa Rica";
    const COTE_D_IVOIRE = "Cote D Ivoire";
    const CROACIA = "Croacia";
    const CUBA = "Cuba";
    const DINAMARCA = "Dinamarca";
    const DJIBOUTI = "Djibouti";
    const DOMINICA = "Dominica";
    const EAST_TIMOR = "East Timor";
    const ECUADOR = "Ecuador";
    const EGIPTO = "Egipto";
    const EL_SALVADOR = "El Salvador";
    const EL_VATICANO = "El Vaticano";
    const EMIRATOS_ARABES_UNIDOS = "Emiratos Arabes Unidos";
    const ERITREA = "Eritrea";
    const ESLOVAQUIA = "Eslovaquia";
    const ESLOVENIA = "Eslovenia";
    const ESPAÑA = "España";
    const ESTADOS_UNIDOS = "Estados Unidos";
    const ESTONIA = "Estonia";
    const ETIOPIA = "Etiopía";
    const FIJI = "Fiji";
    const FILIPINAS = "Filipinas";
    const FINLANDIA = "Finlandia";
    const FRANCIA = "Francia";
    const FRENCH_GUIANA = "French Guiana";
    const FRENCH_POLYNESIA = "French Polynesia";
    const FRENCH_SOUTHERN_TERRITORIES = "French Southern Territories";
    const GABON = "Gabón";
    const GAMBIA = "Gambia";
    const GEORGIA = "Georgia";
    const GHANA = "Ghana";
    const GIBRALTAR = "Gibraltar";
    const GRANADA = "Granada";
    const GRECIA = "Grecia";
    const GROENLANDIA = "Groenlandia";
    const GUADALUPE = "Guadalupe";
    const GUAM = "Guam";
    const GUATEMALA = "Guatemala";
    const GUINEA = "Guinea";
    const GUINEA_ECUATORIAL = "Guinea Ecuatorial";
    const GUINEA_BISSAU = "Guinea-Bissau";
    const GUYANA = "Guyana";
    const HAITI = "Haití";
    const HEARD_ISLAND_AND_MCDONALD_ISLA = "Heard Island and McDonald Isla";
    const HOLANDA = "Holanda";
    const HONDURAS = "Honduras";
    const HONG_KONG = "Hong Kong";
    const HUNGRIA = "Hungría";
    const INDIA = "India";
    const INDONESIA = "Indonesia";
    const IRAQ = "Iraq";
    const IRLANDA = "Irlanda";
    const ISALAS_COCOS = "Isalas Cocos";
    const ISLA_CHRISTMAS = "Isla Christmas";
    const ISLANDIA = "Islandia";
    const ISLAS_CAIMAN = "Islas Caimán";
    const ISLAS_COOK = "Islas Cook";
    const ISLAS_FEROE = "Islas Feroe";
    const ISLAS_MALVINAS = "Islas Malvinas";
    const ISLAS_MARSHALL = "Islas Marshall";
    const ISLAS_MAURICIO = "Islas Mauricio";
    const ISLAS_SALOMON = "Islas Salomón";
    const ISLAS_SANDWHICH = "Islas Sandwhich";
    const ISLAS_TURKS_Y_CAICOS = "Islas Turks y Caicos";
    const ISLAS_WALLIS_Y_FUTUNA = "Islas Wallis y Futuna";
    const ISRAEL = "Israel";
    const ITALIA = "Italia";
    const JAMAICA = "Jamaica";
    const JAPON = "Japón";
    const JORDANIA = "Jordania";
    const KAZAKHSTAN = "Kazakhstán";
    const KENIA = "Kenia";
    const KIRIBATI = "Kiribati";
    const KUWAIT = "Kuwait";
    const KYRGYZSTAN = "Kyrgyzstán";
    const LAOS = "Laos";
    const LATVIA = "Latvia";
    const LESOTO = "Lesoto";
    const LIBANO = "Líbano";
    const LIBERIA = "Liberia";
    const LIBIA = "Libia";
    const LIECHTENSTEIN = "Liechtenstein";
    const LITUANIA = "Lituania";
    const LUXEMBURGO = "Luxemburgo";
    const MACAO = "Macao";
    const MACEDONIA = "Macedonia";
    const MADAGASCAR = "Madagascar";
    const MALASIA = "Malasia";
    const MALAUI = "Malaui";
    const MALDIVAS = "Maldivas";
    const MALI = "Malí";
    const MALTA = "Malta";
    const MARRUECOS = "Marruecos";
    const MARTINIQUE = "Martinique";
    const MAURITANIA = "Mauritania";
    const MAYOTTE = "Mayotte";
    const MEXICO = "México";
    const MICRONESIA = "Micronesia";
    const MOLDAVIA = "Moldavia";
    const MONACO = "Mónaco";
    const MONGOLIA = "Mongolia";
    const MONTSERRAT = "Montserrat";
    const MOZAMBIQUE = "Mozambique";
    const MYANMAR = "Myanmar";
    const NAMIBIA = "Namibia";
    const NAURU = "Nauru";
    const NEPAL = "Nepal";
    const NICARAGUA = "Nicaragua";
    const NIGER = "Níger";
    const NIGERIA = "Nigeria";
    const NIUE = "Niue";
    const NORFOLK_ISLAND = "Norfolk Island";
    const NORTHERN_MARIANA_ISLANDS = "Northern Mariana Islands";
    const NORUEGA = "Noruega";
    const NUEVA_CALEDONIA = "Nueva Caledonia";
    const NUEVA_ZELANDA = "Nueva Zelanda";
    const OMAN = "Omán";
    const PAKISTAN = "Pakistán";
    const PALAU = "Palau";
    const PALESTINIAN_TERRITORY = "Palestinian Territory";
    const PANAMA = "Panamá";
    const PAPUA_NUEVA_GUINEA = "Papúa Nueva Guinea";
    const PARAGUAY = "Paraguay";
    const PERU = "Perú";
    const PITCAIRN = "Pitcairn";
    const POLONIA = "Polonia";
    const PORTUGAL = "Portugal";
    const PUERTO_RICO = "Puerto Rico";
    const QATAR = "Qatar";
    const REINO_UNIDO = "Reino Unido";
    const REPUBLICA_CENTROAFRICANA = "República Centroafricana";
    const REPUBLICA_CHECA = "República Checa";
    const REPUBLICA_DEMOCRATICA_DEL_CONG = "República Democrática del Cong";
    const REPUBLICA_DOMINICANA = "República Dominicana";
    const REPUBLICA_ISLAMICA_DE_IRAN = "República Islámica de Irán";
    const RUANDA = "Ruanda";
    const RUMANIA = "Rumania";
    const RUSIAN = "Rusian";
    const SAINT_KITTS_AND_NEVIS = "Saint Kitts and Nevis";
    const SAINT_PIERRE_Y_MIQUELON = "Saint Pierre y Miquelon";
    const SAMOA = "Samoa";
    const SAN_MARINO = "San Marino";
    const SAN_VICENTE_Y_LAS_GRANADINAS = "San Vicente y Las Granadinas";
    const SANTA_ELENA = "Santa Elena";
    const SANTA_LUCIA = "Santa Lucía";
    const SAO_TOME_AND_PRINCIPE = "Sao Tome and Principe";
    const SENEGAL = "Senegal";
    const SERBIA_Y_MONTENEGRO = "Serbia y Montenegro";
    const SEYCHELLES = "Seychelles";
    const SIERRA_LEONA = "Sierra Leona";
    const SINGAPUR = "Singapur";
    const SIRIA = "Siria";
    const SOMALIA = "Somalía";
    const SRI_LANKA = "Sri Lanka";
    const SUAZILANDIA = "Suazilandia";
    const SUDAFRICA = "Sudáfrica";
    const SUDAN = "Sudán";
    const SUECIA = "Suecia";
    const SUIZA = "Suiza";
    const SURINAM = "Surinam";
    const SVALBARD_AND_JAN_MAYEN = "Svalbard and Jan Mayen";
    const TAILANDIA = "Tailandia";
    const TAIWAN = "Taiwan";
    const TAJIKISTAN = "Tajikistán";
    const TANZANIA = "Tanzania";
    const TOGO = "Togo";
    const TONGA = "Tonga";
    const TOQUELAU = "Toquelau";
    const TRINIDAD_Y_TOBAGO = "Trinidad y Tobago";
    const TUNEZ = "Túnez";
    const TURKMENISTAN = "Turkmenistán";
    const TURQUIA = "Turquía";
    const TUVALU = "Tuvalu";
    const UCRANIA = "Ucrania";
    const UGANDA = "Uganda";
    const UNITED_STATES_MINOR_OUTLYING_I = "United States Minor Outlying I";
    const URUGUAY = "Uruguay";
    const UZBEKISTAN = "Uzbekistan";
    const VANUATU = "Vanuatu";
    const VENEZUELA = "Venezuela";
    const VIETNAM = "Vietnam";
    const VIRGIN_ISLANDS_BRITISH = "Virgin Islands British";
    const VIRGIN_ISLANDS_US = "Virgin Islands U.S.";
    const WESTERN_SAHARA = "Western Sahara";
    const YEMEN = "Yemen";
    const ZAIRE = "Zaire";
    const ZAMBIA = "Zambia";
    const ZIMBABUE = "Zimbabue";

    /**
     * @var string
     */
    private $name;

    /**
     * Country constructor.
     * @param $name
     */
    public function __construct($name)
    {
        $this->name = $name;
        $spec = new CountryIsValid();
        if (!$spec->isSatisfiedBy($this)) {
            throw new \InvalidArgumentException("Invalid country name.");
        }
    }

    /**
     * @return string
     */
    public function getName()
    {
        return $this->name;
    }

    /**
     * @return string
     */
    public function __toString()
    {
        return $this->name;
    }


}