<?php

namespace AndresGotta\Bundle\GolfBundle\ValueObject;
use Doctrine\ORM\Mapping as ORM;

/**
 * Class FullName
 * @package AndresGotta\Bundle\GolfBundle\ValueObject
 * @ORM\Embeddable()
 */
class FullName
{
    /**
     * @var string
     * @ORM\Column(type="string", length=64, nullable=true, name="first_name")
     */
    private $firstName;

    /**
     * @var string
     * @ORM\Column(type="string", length=64, nullable=true, name="last_name")
     */
    private $lastName;

    /**
     * FullName constructor.
     * @param string $firstName
     * @param string $lastName
     */
    public function __construct($firstName, $lastName)
    {
        $this->firstName = $firstName;
        $this->lastName = $lastName;
    }

    /**
     * @return string
     */
    public function getFirstName()
    {
        return $this->firstName;
    }

    /**
     * @return string
     */
    public function getLastName()
    {
        return $this->lastName;
    }

    /**
     * @return string
     */
    public function __toString()
    {
        return sprintf("%s, %s", $this->lastName, $this->firstName);
    }


}