<?php

namespace AndresGotta\Bundle\GolfBundle\ValueObject;

use AndresGotta\Bundle\GolfBundle\Specification\ClubIsValid;
use Doctrine\ORM\Mapping as ORM;

/**
 * Class Club
 * @ORM\Embeddable()
 */
class Club
{
    use GetChoicesTrait;

    const DRIVER = 'club.driver';
    const WOOD = 'club.wood';
    const HYBRID = 'club.hybrid';
    const IRON = 'club.iron';

    /**
     * @var string
     *
     * @ORM\Column(type="string", length=16)
     */
    private $type;

    /**
     * Club constructor.
     * @param string $type
     */
    public function __construct($type)
    {
        $this->type = $type;
        $spec = new ClubIsValid();
        if (!$spec->isSatisfiedBy($this)) {
            throw new \InvalidArgumentException("Invalid club type.");
        }
    }

    /**
     * @return string
     */
    public function __toString()
    {
        return $this->type;
    }

}
