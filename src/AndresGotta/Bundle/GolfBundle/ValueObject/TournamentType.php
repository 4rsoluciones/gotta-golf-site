<?php

namespace AndresGotta\Bundle\GolfBundle\ValueObject;

/**
 * TournamentType
 *
 * @author gaston
 */
class TournamentType
{
    const HOLES_18 = 18;
    const HOLES_36 = 36;
    const HOLES_54 = 54;
    const HOLES_72 = 72;

    /**
     * Choices
     *
     * @return array
     */
    public static function getChoices()
    {
        $keys = range(18, 72, 18);
        $i = 1;

        $values = array_map(function ($value) use (&$i) {
            if ($i == 1) {
                $message = '%d hoyos (%d vuelta)';
            } else {
                $message = '%d hoyos (%d vueltas)';
            }
            
            return sprintf($message, $value, $i++);
        }, $keys);

        return array_combine($keys, $values);
    }

}
