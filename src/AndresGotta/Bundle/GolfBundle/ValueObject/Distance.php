<?php

namespace AndresGotta\Bundle\GolfBundle\ValueObject;

/**
 * Field Par
 *
 * @author gaston
 */
final class Distance
{

    const LESS_THAN_20_YDS = 'less.than.20.yds';
    const BTW_21_TO_40_YDS = 'btw.21.to.40.yds';
    const BTW_41_TO_60_YDS = 'btw.41.to.60.yds';
    const BTW_61_TO_80_YDS = 'btw.61.to.80.yds';
    const BTW_81_TO_100_YDS = 'btw.81.to.100.yds';
    const BTW_101_TO_120_YDS = 'btw.101.to.120.yds';
    const MORE_THAN_120_YDS = 'more.than.120.yds';
    const LESS_THAN_100_YDS = 'less.than.100.yds';
    const BTW_100_TO_120_YDS = 'btw.100.to.120.yds';
    const BTW_121_TO_140_YDS = 'btw.121.to.140.yds';
    const BTW_141_TO_160_YDS = 'btw.141.to.160.yds';
    const BTW_161_TO_180_YDS = 'btw.161.to.180.yds';
    const BTW_181_TO_200_YDS = 'btw.181.to.200.yds';
    const BTW_201_TO_220_YDS = 'btw.201.to.220.yds';
    const MORE_THAN_200_YDS = 'more.than.200.yds';
    const MORE_THAN_220_YDS = 'more.than.220.yds';
    const BTW_221_TO_240_YDS = 'btw.221.to.240.yds';
    const BTW_241_TO_260_YDS = 'btw.241.to.260.yds';
    const BTW_261_TO_280_YDS = 'btw.261.to.280.yds';
    const BTW_281_TO_300_YDS = 'btw.281.to.300.yds';
    const BTW_301_TO_320_YDS = 'btw.301.to.320.yds';
    const MORE_THAN_320_YDS = 'more.than.320.yds';

    const LESS_THAN_1_MTS = 'less.than.1.mts';
    const BTW_1_TO_2_MTS = 'btw.1.to.2.mts';
    const BTW_2_TO_3_MTS = 'btw.2.to.3.mts';
    const BTW_3_TO_5_MTS = 'btw.3.to.5.mts';
    const BTW_5_TO_7_MTS = 'btw.5.to.7.mts';
    const BTW_7_TO_10_MTS = 'btw.7.to.10.mts';
    const BTW_10_TO_15_MTS = 'btw.10.to.15.mts';
    const MORE_THAN_15_MTS = 'more.than.15.mts';

}
