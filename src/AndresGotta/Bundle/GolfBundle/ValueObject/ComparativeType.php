<?php

namespace AndresGotta\Bundle\GolfBundle\ValueObject;

/**
 * Class ComparativeType
 * @package AndresGotta\Bundle\GolfBundle\ValueObject
 */
class ComparativeType
{
    const DONUT_SINGLE_TYPE = 'donut_single';
    const DONUT_DOBLE_TYPE = 'donut_doble';
    const DONUT_MULTIPLE_TYPE = 'donut_multiple';
    const BAR_GRAPHIC_SINGLE_TYPE = 'bar_graphic_single';
}