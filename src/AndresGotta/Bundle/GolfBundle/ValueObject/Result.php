<?php

namespace AndresGotta\Bundle\GolfBundle\ValueObject;

/**
 * Class Result
 */
final class Result
{

    const INSIDE_GREEN = 'inside.green';//Dentro del Green
    const INSIDE_FAIRWAY = 'inside.fairway';//Dentro del fairway
    const OUTSIDE_FAIRWAY = 'outside.fairway'; //Fuera del fairway
    const HOLE_OUT = 'hole.out';//Embocada
    const CROSS_BUNKER_IN_FAIRWAY = 'cross.bunker.in.fairway'; // Cross bunker en el fairway
    const OUTSIDE_FAIRWAY_INTO_THE_BUNKER = 'outside.fairway.into.the.bunker'; //Fuera del fairway en el bunker
    const OUTSIDE_GREEN_INTO_THE_BUNKER = 'outside.green.into.the.bunker';//Fuera del green en el bunker

    const OUTSIDE_GREEN = 'outside.green';// Se usa en reportes viejos

    const MISSED = 'missed'; // Se usa en reportes viejos
    //const INTO_THE_BUNKER = 'into.the.bunker'; // No se usa

    const BALL_IN_HAZARD = 'ball.in.hazard'; //Pelota en hazard // nuevo
    const OUT_OF_LIMITS = 'out.of.limits';//Fuera del límite // nuevo

}
