<?php

namespace AndresGotta\Bundle\GolfBundle\ValueObject;

/**
 * RoundType
 *
 * @author gaston
 */
class RoundType
{

    const PRACTICE = 'round.type.practice';
    const TOURNAMENT = 'round.type.tournament';

    /**
     *
     * @var integer
     */
    protected $value;

    /**
     *
     * @param integer $value
     */
    public function __construct($value)
    {
        $this->value = $value;
    }

    /**
     *
     * @return integer
     */
    public function getValue()
    {
        return $this->value;
    }

    /**
     * Choices
     *
     * @param bool $getDefault
     * @return array|string
     */
    public static function getChoices($getDefault = false)
    {
        if ($getDefault == 'default') {
            return static::TOURNAMENT;
        }

        return array(
            static::PRACTICE => static::PRACTICE,
            static::TOURNAMENT => static::TOURNAMENT,
        );
    }

}
