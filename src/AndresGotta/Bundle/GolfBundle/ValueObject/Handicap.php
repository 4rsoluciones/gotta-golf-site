<?php

namespace AndresGotta\Bundle\GolfBundle\ValueObject;

use AndresGotta\Bundle\GolfBundle\Specification\HandicapIsValid;
use Doctrine\ORM\Mapping as Embedded;

/**
 * Handicap
 *
 * @author gaston
 * @Embedded\Embeddable()
 */
class Handicap
{
    /**
     * @var integer
     * @Embedded\Column(type="integer", name="raw_value")
     */
    private $rawValue;

    /**
     * Handicap constructor.
     * @param int $rawValue
     */
    public function __construct($rawValue)
    {
        $this->rawValue = $rawValue;

        $spec = new HandicapIsValid();
        if (!$spec->isSatisfiedBy($this)) {
            throw new \InvalidArgumentException("Invalid handicap value.");
        }
    }

    /**
     * @return int
     */
    public function getRawValue()
    {
        return $this->rawValue;
    }

    /**
     * @return string
     */
    public function __toString()
    {
        return str_replace('-', '+', $this->rawValue);
    }

    /**
     * Choices
     *
     * @return array
     */
    public static function getChoices()
    {
        $keys = range(36, -5, -1);

        $values = array_map(function ($value) {
            return str_replace('-', '+', $value);
        }, $keys);
        
        return array_combine($keys, $values);
    }

}
