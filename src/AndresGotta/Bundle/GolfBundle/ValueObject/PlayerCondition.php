<?php

namespace AndresGotta\Bundle\GolfBundle\ValueObject;
use AndresGotta\Bundle\GolfBundle\Specification\PlayerConditionIsValid;
use Doctrine\ORM\Mapping as ORM;

/**
 * Class PlayerCondition
 * @ORM\Embeddable()
 */
class PlayerCondition
{

    use GetChoicesTrait;

    const PROFESSIONAL = 'player.professional';
    const AMATEUR = 'player.amateur';

    /**
     * @var string
     *
     * @ORM\Column(type="string", length=24)
     */
    private $condition;

    /**
     * PlayerCondition constructor.
     * @param string $condition
     */
    public function __construct($condition)
    {
        $this->condition = $condition;
        $spec = new PlayerConditionIsValid();
        if (!$spec->isSatisfiedBy($this)) {
            throw new \InvalidArgumentException("Invalid player condition.");
        }
    }

    /**
     * @return string
     */
    public function __toString()
    {
        return $this->condition;
    }


}
