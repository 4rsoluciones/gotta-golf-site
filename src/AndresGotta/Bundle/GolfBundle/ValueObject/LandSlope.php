<?php

namespace AndresGotta\Bundle\GolfBundle\ValueObject;

/**
 * Land Slope
 *
 * @author gaston
 */
class LandSlope
{
    use GetChoicesTrait;

    const PLANE = 'land_slope.plane';
    const STRAIGHT = 'land_slope.straight';
    const RIGHT_TO_LEFT = 'land_slope.right_to_left';
    const LEFT_TO_RIGHT = 'land_slope.left_to_right';
    const ON_DESCEND = 'land_slope.on_descend';
    const ON_CLIMB = 'land_slope.on_climb';

}
