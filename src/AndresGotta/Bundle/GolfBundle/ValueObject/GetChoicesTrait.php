<?php

namespace AndresGotta\Bundle\GolfBundle\ValueObject;

trait GetChoicesTrait
{
    public static function getChoices()
    {
        $r = new \ReflectionClass(__CLASS__);

        return array_combine($r->getConstants(), $r->getConstants());
    }
}