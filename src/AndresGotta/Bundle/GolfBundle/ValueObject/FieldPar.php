<?php

namespace AndresGotta\Bundle\GolfBundle\ValueObject;

/**
 * Field Par
 *
 * @author gaston
 */
class FieldPar
{
    use GetChoicesTrait;

    const LESS_70 =  'field_par.less.than.70';
    const EQUAL_70 = 'field_par.equal.at.70';
    const EQUAL_71 = 'field_par.equal.at.71';
    const EQUAL_72 = 'field_par.equal.at.72';
    const MORE_72 =  'field_par.more.than.72';

}
