<?php

namespace AndresGotta\Bundle\GolfBundle\ValueObject;

/**
 * Class ComparativeLabels
 * @package AndresGotta\Bundle\GolfBundle\ValueObject
 */
class ComparativeLabels
{
    const PLAYER_LABEL = 'comparative_label.player';
    const GROUP_LABEL = 'comparative_label.group';
    const GOOD_LABEL = 'comparative_label.good';
    const BAD_LABEL = 'comparative_label.bad';

}