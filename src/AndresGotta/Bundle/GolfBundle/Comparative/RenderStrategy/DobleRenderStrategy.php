<?php

namespace AndresGotta\Bundle\GolfBundle\Comparative\RenderStrategy;

use AndresGotta\Bundle\GolfBundle\Comparative\ChartGroup;

/**
 * Class DobleRenderStrategy
 * @package AndresGotta\Bundle\GolfBundle\Comparative\RenderStrategy
 */
class DobleRenderStrategy extends BaseRenderStrategy
{
    /**
     * @inheritdoc
     */
    public function render(ChartGroup $result = null)
    {
        $template = self::BASE_TEMPLATE_PATH . self::DONUT_DOBLE_TEMPLATE_TYPE;

        return $this->templating->render($template, [
            'results' => $result->getResults()->toArray(),
            'comparative' => $this->comparative
        ]);
    }

}