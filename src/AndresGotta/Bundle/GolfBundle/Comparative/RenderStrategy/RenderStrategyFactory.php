<?php

namespace AndresGotta\Bundle\GolfBundle\Comparative\RenderStrategy;

use AndresGotta\Bundle\GolfBundle\Entity\Comparative;
use AndresGotta\Bundle\GolfBundle\ValueObject\ComparativeType;

/**
 * Class RenderStrategyFactory
 * @package AndresGotta\Bundle\GolfBundle\Comparative\RenderStrategy
 */
class RenderStrategyFactory
{
    /**
     * @param Comparative $comparative
     * @return RenderStrategyInterface
     */
    public function create(Comparative $comparative)
    {
        switch ($comparative->getType()) {
            case ComparativeType::DONUT_SINGLE_TYPE:
                $strategy = new SingleRenderStrategy($comparative);
                break;
            case ComparativeType::DONUT_DOBLE_TYPE:
                $strategy = new DobleRenderStrategy($comparative);
                break;
            case ComparativeType::DONUT_MULTIPLE_TYPE:
                $strategy = new MultipleRenderStrategy($comparative);
                break;
            case ComparativeType::BAR_GRAPHIC_SINGLE_TYPE:
                $strategy = new BarGraphicSingleRenderStrategy($comparative);
                break;
            default:
                throw new \InvalidArgumentException('Comparative type is not supported');
        }

        return $strategy;
    }
}