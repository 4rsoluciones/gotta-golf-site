<?php

namespace AndresGotta\Bundle\GolfBundle\Comparative\RenderStrategy;

/**
 * Class LegendValue
 * @package AndresGotta\Bundle\GolfBundle\Comparative\RenderStrategy
 */
class LegendValue
{
    const GENERAL_LEGEND = 'results.legend.general';
    const PLAYER_LEGEND = 'results.legend.player';

    const POSITIVE_LEGEND = 'results.legend.positives';
    const NEGATIVE_LEGEND = 'results.legend.negatives';

    const GOOD_RESULTS_LEGEND = 'results.legend.good';
    const BAD_RESULTS_LEGEND = 'results.legend.bad';

    const PUTTS_LEGEND = 'results.legend.putts_average.%putts%.%green%';
    const PUTTS_AVERAGE_LEGEND = 'results.legend.group_putts_average.%putts%.%green%';

    const PUTTS_FREQUENCY_PLAYER = 'results.legend.%total%_putts_frequency_player';
    const PUTTS_FREQUENCY_GROUP = 'results.legend.%total%_putts_frequency_group';

    const GROSS_PLAYER = 'results.legend.%total%_gross_player';
    const GROSS_GROUP = 'results.legend.%total%_gross_group';
}