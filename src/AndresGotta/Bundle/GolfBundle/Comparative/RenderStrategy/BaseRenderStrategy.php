<?php

namespace AndresGotta\Bundle\GolfBundle\Comparative\RenderStrategy;

use AndresGotta\Bundle\GolfBundle\Comparative\BaseType;
use Symfony\Bundle\TwigBundle\TwigEngine;

/**
 * Class BaseRenderStrategy
 * @package AndresGotta\Bundle\GolfBundle\Comparative\RenderStrategy
 */
abstract class BaseRenderStrategy implements RenderStrategyInterface
{
    /**
     * @var TwigEngine
     */
    protected $templating;

    /**
     * @var BaseType
     */
    protected $comparative;

    /**
     * BaseRenderStrategy constructor.
     * @param BaseType $comparative
     */
    public function __construct($comparative)
    {
        $this->comparative = $comparative;
    }

    /**
     * @param TwigEngine $templating
     */
    public function setTemplating(TwigEngine $templating)
    {
        $this->templating = $templating;
    }

}