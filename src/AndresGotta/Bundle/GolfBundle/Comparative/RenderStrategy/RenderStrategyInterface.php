<?php

namespace AndresGotta\Bundle\GolfBundle\Comparative\RenderStrategy;

use AndresGotta\Bundle\GolfBundle\Comparative\ChartGroup;
use Symfony\Bundle\TwigBundle\TwigEngine;

/**
 * Interface RenderStrategyInterface
 * @package AndresGotta\Bundle\GolfBundle\Comparative\RenderStrategy
 */
interface RenderStrategyInterface
{
    /**
     * Path base donde estan todos los templates
     */
    const BASE_TEMPLATE_PATH = 'AndresGottaGolfBundle:Frontend/Comparative/Types:';

    /**
     * Template tipo dona base (tiene todos los ejemplos)
     */
    const DONUT_TEMPLATE_TYPE = 'donut-base-type.html.twig';

    /**
     * Template tipo dona simple, una sola por renderización
     */
    const DONUT_SINGLE_TEMPLATE_TYPE = 'donut-single-type.html.twig';

    /**
     *
     */
    const DONUT_DOBLE_TEMPLATE_TYPE = 'donut-doble-type.html.twig';

    /**
     * Template tipo barras
     */
    const BAR_GRAPHIC_TEMPLATE_TYPE = 'bar-graphic-base-type.html.twig';

    /**
     * @param TwigEngine $templating
     */
    public function setTemplating(TwigEngine $templating);

    /**
     * @param ChartGroup $result
     * @return string
     */
    public function render(ChartGroup $result = null);
}