<?php

namespace AndresGotta\Bundle\GolfBundle\Comparative;

use Doctrine\Common\Collections\ArrayCollection;

/**
 * Class ChartGroup
 * @package AndresGotta\Bundle\GolfBundle\Comparative
 */
class ChartGroup
{
    /**
     * @var ArrayCollection
     */
    protected $results;

    /**
     * CharsetGroup constructor.
     */
    public function __construct()
    {
        $this->results = new ArrayCollection();
    }

    /**
     * @param ResultResponse $result
     */
    public function add(ResultResponse $result)
    {
        $this->results->add($result);
    }

    /**
     * @param ResultResponse $result
     */
    public function remove(ResultResponse $result)
    {
        $this->results->removeElement($result);
    }

    /**
     * @return ArrayCollection
     */
    public function getResults()
    {
        return $this->results;
    }
}