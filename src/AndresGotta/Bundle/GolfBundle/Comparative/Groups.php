<?php

namespace AndresGotta\Bundle\GolfBundle\Comparative;

/**
 * Class Groups
 * @package AndresGotta\Bundle\GolfBundle\Comparative
 */
final class Groups
{
    const GROUP1 = 'group_1';
    const GROUP2 = 'group_2';
    const GROUP3 = 'group_3';
    const GROUP4 = 'group_4';
    const GROUP5 = 'group_5';
    const GROUP6 = 'group_6';
    const GROUP7 = 'group_7';
    const GROUP8 = 'group_8';
    const GROUP9 = 'group_9';
    const GROUP10 = 'group_10';
    const GROUP11 = 'group_11';
    const GROUP12 = 'group_12';
    const GROUP13 = 'group_13';
    const GROUP14 = 'group_14';
    const GROUP15 = 'group_15';
    const GROUP16 = 'group_16';

    private static $averages = [
        self::GROUP1 => [null, 65.9],
        self::GROUP2 => [66, 67.7],
        self::GROUP3 => [68, 69.9],
        self::GROUP4 => [70, 71.9],
        self::GROUP5 => [72, 73.9],
        self::GROUP6 => [74, 75.9],
        self::GROUP7 => [76, 77.9],
        self::GROUP8 => [78, 79.9],
        self::GROUP9 => [80, 81.9],
        self::GROUP10 => [82, 84.9],
        self::GROUP11 => [85, 89.9],
        self::GROUP12 => [90, 94.9],
        self::GROUP13 => [95, 99.9],
        self::GROUP14 => [100, 109.9],
        self::GROUP15 => [110, 119.9],
        self::GROUP16 => [120, null],
    ];

    /**
     * @param $score
     * @return int|string
     */
    public static function getGroupByScore($score)
    {
        foreach (self::$averages as $group => $average) {
            list($min, $max) = $average;

            if ((!$min && $score <= $max)
                || ($score >= $min && $score <= $max)
                || (!$max && $score >= $min)
            ) {
                return self::$averages[$group];
            }
        }

    }
}