<?php

namespace AndresGotta\Bundle\GolfBundle\Comparative\Types;

use AndresGotta\Bundle\GolfBundle\Comparative\BaseType;
use AndresGotta\Bundle\GolfBundle\Comparative\ChartGroup;
use AndresGotta\Bundle\GolfBundle\Comparative\RenderStrategy\LegendValue;
use AndresGotta\Bundle\GolfBundle\Comparative\ResultResponse;
use AndresGotta\Bundle\GolfBundle\Entity\Hole;

/**
 * Class GrossAverage
 * @package AndresGotta\Bundle\GolfBundle\Comparative\Types
 */
class GrossAverage extends BaseType
{
    /**
     * De cada vuelta de 18 hoyos completa, sacar el promedio total de golpes (incluyendo los golpes de penalidad) de las vueltas.
     * Es el valor del reporte 73
     *
     * @inheritdoc
     */
    public function calculateValues()
    {
        $rounds = $this->getPlayerRounds();
        if (count($rounds) == 0) {
            $player = $others = 0;

            $resultResponse = new ResultResponse(
                $player,
                $others,
                $this->translator->trans(LegendValue::GROSS_PLAYER, ['%total%' => $player]),
                $this->translator->trans(LegendValue::GROSS_GROUP, ['%total%' => $others])
            );

            $result = new ChartGroup();
            $result->add($resultResponse);

            return $result;
        }

        $globalResult = $this->calculateResult();

        $result = new ChartGroup();
        $result->add($globalResult);

        return $result;
    }

    /**
     * @inheritdoc
     */
    protected function calculateResult()
    {
        $average = 0;

        foreach ($this->otherPlayers as $otherPlayer) {
            $average += $otherPlayer['score'];
        }

        $player = $this->player->getProfile()->getScore();
        $others = $average > 0 ? $average / $this->otherPlayers->count() : 0;

        return new ResultResponse(
            $player,
            $others,
            $this->translator->trans(LegendValue::GROSS_PLAYER, ['%total%' => $player]),
            $this->translator->trans(LegendValue::GROSS_GROUP, ['%total%' => $others])
        );
    }

}