<?php

namespace AndresGotta\Bundle\GolfBundle\Comparative\Types;

use AndresGotta\Bundle\GolfBundle\Comparative\BaseType;
use AndresGotta\Bundle\GolfBundle\Comparative\ChartGroup;
use AndresGotta\Bundle\GolfBundle\Comparative\RenderStrategy\LegendValue;
use AndresGotta\Bundle\GolfBundle\Comparative\ResultResponse;
use AndresGotta\Bundle\GolfBundle\Entity\Hole;
use AndresGotta\Bundle\GolfBundle\Entity\Strike;
use AndresGotta\Bundle\GolfBundle\ValueObject\Result;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\ORM\QueryBuilder;

/**
 * Class Scrambling
 * @package AndresGotta\Bundle\GolfBundle\Comparative\Types
 */
class Scrambling extends BaseType
{
    /**
     * Se toman en cuenta únicamente los: Primer tiro en Par 3, segundo tiro en par 4 y tercer tiro en par 5. Esa suma da el total de tiros.
     *
     * Hay que ver cuánto de esos tiros, dio como resultado fuera del Green y ese es el nuevo 100%.
     *
     * De ese nuevo 100%, mostrar el porcentaje que tiene en donde el score final del hoyo (sin contar penalidades) es par
     * o menos (2 o 3 en par 3, 3 o 4 en par 4 o 3, 4 o 5 en par 5).
     *
     * @inheritdoc
     */
    public function calculateValues()
    {
        $queryBuilderFirst = $this->generateBaseQueryBuilder(Strike::class);
        $positiveResults = [Result::INSIDE_GREEN, Result::HOLE_OUT];
        $queryBuilderFirst
            ->andWhere('hole.par = 3')
            ->andWhere('strike.result not in (:result)')
            ->setParameter('result', $positiveResults)
            ->andWhere('strike.sort = 1')
        ;

        $queryBuilderSecond = $this->generateBaseQueryBuilder(Strike::class);
        $queryBuilderSecond
            ->andWhere('hole.par = 4')
            ->andWhere('strike.result not in (:result)')
            ->setParameter('result', $positiveResults)
            ->andWhere('strike.sort in (1, 2)')
        ;

        $queryBuilderThird = $this->generateBaseQueryBuilder(Strike::class);
        $queryBuilderThird
            ->andWhere('hole.par = 5')
            ->andWhere('strike.result not in (:result)')
            ->setParameter('result', $positiveResults)
            ->andWhere('strike.sort in (1, 2, 3)')
        ;

        $total = $this->getSingleResult($queryBuilderFirst);
        $total += $this->getSingleResult($queryBuilderSecond);
        $total += $this->getSingleResult($queryBuilderThird);

        if ($total == 0) {
            $playerTotal = new ResultResponse(
                0,
                0,
                $this->translator->trans(LegendValue::POSITIVE_LEGEND),
                $this->translator->trans(LegendValue::NEGATIVE_LEGEND)
            );
            $results = new ChartGroup();
            $results->add($playerTotal);

            return $results;
        }

        $holes = new ArrayCollection();

        foreach ($this->calculatePlayerCondition($queryBuilderFirst) as $result) {
            $hole = $result->getHole();
            if (!$holes->contains($hole)) {
                $holes->add($hole);
            }
        }
        foreach ($this->calculatePlayerCondition($queryBuilderSecond) as $result) {
            $hole = $result->getHole();
            if (!$holes->contains($hole)) {
                $holes->add($hole);
            }
        }
        foreach ($this->calculatePlayerCondition($queryBuilderThird) as $result) {
            $hole = $result->getHole();
            if (!$holes->contains($hole)) {
                $holes->add($hole);
            }
        }

        $playerTotal = $this->calculateAverageFromTotal($holes->toArray(), $total);

        $results = new ChartGroup();
        $results->add($playerTotal);

        return $results;
    }

    /**
     * @param QueryBuilder $queryBuilder
     * @return int
     */
    private function getSingleResult(QueryBuilder $queryBuilder)
    {
        $newQueryBuilder = clone $queryBuilder;
        $result = $this->calculatePlayerCondition($newQueryBuilder);

        return count($result);
    }

    /**
     * @inheritdoc
     */
    protected function calculateAverageFromTotal($result, $total)
    {
        $good = 0;

        /** @var Hole $hole */
        foreach ($result as $hole) {
            switch ($hole->getPar()) {
                case 3:
                    if ($hole->getScore() === 2 || $hole->getScore() === 3) {
                        $good++;
                    }

                    break;
                case 4:
                    if ($hole->getScore() === 3 || $hole->getScore() === 4) {
                        $good++;
                    }

                    break;
                case 5:
                    if ($hole->getScore() === 4 || $hole->getScore() === 5) {
                        $good++;
                    }

                    break;
            }
        };

        $good = round($good * 100 / $total, 1, PHP_ROUND_HALF_UP);
        $bad = round(100 - $good, 1, PHP_ROUND_HALF_UP);

        return new ResultResponse(
            $good,
            $bad,
            $this->translator->trans(LegendValue::POSITIVE_LEGEND),
            $this->translator->trans(LegendValue::NEGATIVE_LEGEND)
        );
    }
}