<?php

namespace AndresGotta\Bundle\GolfBundle\Comparative\Types;

use AndresGotta\Bundle\GolfBundle\Comparative\BaseType;
use AndresGotta\Bundle\GolfBundle\Comparative\ChartGroup;
use AndresGotta\Bundle\GolfBundle\Comparative\RenderStrategy\LegendValue;
use AndresGotta\Bundle\GolfBundle\Comparative\ResultResponse;
use AndresGotta\Bundle\GolfBundle\Entity\Strike;
use AndresGotta\Bundle\GolfBundle\ValueObject\ComparativeLabels;
use AndresGotta\Bundle\GolfBundle\ValueObject\Result;
use AndresGotta\Bundle\GolfBundle\ValueObject\StrikeType;

/**
 * Class FirstStrikePrecision
 * @package AndresGotta\Bundle\GolfBundle\Comparative\Types
 */
class FirstStrikePrecision extends BaseType
{
    /**
     * De todos los tiros de salida en hoyos par 4 y 5, se suman y se obtiene el 100%.
     * Se muestra en la misma dona, el porcentaje de aciertos (en fairway) y errores (suma de todas las demás opciones).
     *
     * @inheritdoc
     */
    public function calculateValues()
    {

        $queryBuilder = $this->generateBaseQueryBuilder('AndresGottaGolfBundle:Strike');
        $queryBuilder->andWhere('strike.type = :strike_type')->setParameter('strike_type', StrikeType::TYPE_FIRST);
        $queryBuilder->andWhere('hole.par IN (4, 5)');


        //-----------
        $newQuery = clone $queryBuilder;
        $newQuery->andWhere('round.player = :player')->setParameter('player', $this->player->getId());

        $fromPlayer = $newQuery->getQuery()
            ->useQueryCache(true)->useResultCache(true)
            ->getArrayResult();


//        $fromPlayer = $this->calculatePlayerCondition($queryBuilder);

        //-------------

        if (count($fromPlayer) == 0) {
            $playerResult = new ResultResponse(
                0,
                0,
                $this->translator->trans(LegendValue::GOOD_RESULTS_LEGEND),
                $this->translator->trans(LegendValue::BAD_RESULTS_LEGEND)
            );
            $playerResult->setTitle($this->translator->trans(ComparativeLabels::PLAYER_LABEL));
            $generalResult = new ResultResponse(
                0,
                0,
                $this->translator->trans(LegendValue::GOOD_RESULTS_LEGEND),
                $this->translator->trans(LegendValue::BAD_RESULTS_LEGEND)
            );
            $generalResult->setTitle($this->translator->trans(ComparativeLabels::GROUP_LABEL));
            $chartGroup = new ChartGroup();
            $chartGroup->getResults()->set('player', $playerResult);
            $chartGroup->getResults()->set('general', $generalResult);

            return $chartGroup;
        }

        //------------------
        $newQuery = clone $queryBuilder;
        $others = $this->otherPlayers->toArray();

        $result =  $newQuery
            ->andWhere('profile.id IN (:players)')->setParameter('players', $others)
            ->getQuery()
//            ->useQueryCache(true)->useResultCache(true)
            ->getArrayResult()
            ;


//        $result = $this->calculateOtherPlayersCondition($queryBuilder);
        //------------------

        $playerResult = $this->calculateAverageFromTotal($fromPlayer, count($fromPlayer));
        $playerResult->setTitle($this->translator->trans(ComparativeLabels::PLAYER_LABEL));

        $generalResult = $this->calculateAverageFromTotal($result, count($result));
        $generalResult->setTitle($this->translator->trans(ComparativeLabels::GROUP_LABEL));

        $chartGroup = new ChartGroup();
        $chartGroup->getResults()->set('player', $playerResult);
        $chartGroup->getResults()->set('general', $generalResult);
//        $chartGroup->add($playerResult);
//        $chartGroup->add($generalResult);

        return $chartGroup;
    }

    /**
     * @param $result
     * @param $total
     * @return ResultResponse
     */
    protected function calculateAverageFromTotal($result, $total)
    {
        $goodResults = [
            Result::INSIDE_GREEN,
            Result::INSIDE_FAIRWAY,
            Result::CROSS_BUNKER_IN_FAIRWAY,
            Result::HOLE_OUT,
        ];

        $good = 0;
        /** @var Strike $strike */
        foreach ($result as $strike) {
            if (in_array($strike['result'], $goodResults)) {
                $good++;
            }
        }

        $good = round($good * 100 / $total, 1, PHP_ROUND_HALF_UP);
        $bad = round(100 - $good, 1, PHP_ROUND_HALF_UP);

        return new ResultResponse(
            $good,
            $bad,
            $this->translator->trans(LegendValue::GOOD_RESULTS_LEGEND),
            $this->translator->trans(LegendValue::BAD_RESULTS_LEGEND)
        );
    }
}