<?php

namespace AndresGotta\Bundle\GolfBundle\Comparative\Types;

use AndresGotta\Bundle\GolfBundle\Comparative\BaseType;
use AndresGotta\Bundle\GolfBundle\Comparative\ChartGroup;
use AndresGotta\Bundle\GolfBundle\Comparative\RenderStrategy\LegendValue;
use AndresGotta\Bundle\GolfBundle\Comparative\ResultResponse;
use AndresGotta\Bundle\GolfBundle\Entity\Hole;
use AndresGotta\Bundle\GolfBundle\Entity\Strike;
use AndresGotta\Bundle\GolfBundle\ValueObject\StrikeType;

/**
 * Class GreenFrequencyThreePutts
 * @package AndresGotta\Bundle\GolfBundle\Comparative\Types
 */
class GreenFrequencyThreePutts extends BaseType
{
    /**
     * De 18 greens jugados, sacar la frecuencia en los que realiza 3 putts para embocar por green.
     * Ejemplo: si en una vuelta de 18 hoyos hice 3 greens de 3 putts y en otra ninguno, el valor a mostrar seria de
     * 1,5 greens de 3 putts por vuelta.
     * Mostrar el valor del reporte 67
     *
     * @inheritdoc
     */
    public function calculateValues()
    {
        $rounds = $this->getPlayerRounds();

        if (count($rounds) == 0) {
            $globalResult = new ResultResponse(
                0,
                0,
                $this->translator->trans(LegendValue::PUTTS_FREQUENCY_PLAYER, ['%total%' => 0]),
                $this->translator->trans(LegendValue::PUTTS_FREQUENCY_GROUP, ['%total%' => 0])
            );
            $result = new ChartGroup();
            $result->add($globalResult);

            return $result;
        }

        $otherRounds = $this->getOtherPlayersRounds();

        $globalResult = $this->calculateTotals(['user' => $rounds, 'others' => $otherRounds]);

        $result = new ChartGroup();
        $result->add($globalResult);

        return $result;
    }

    /**
     * @inheritdoc
     */
    protected function calculateTotals($result)
    {
        $user = $other = 0;
        foreach ($result['user'] as $round) {
            /** @var Hole $hole */
            foreach ($round->getHoles() as $hole) {
                $putts = $hole->getStrikesByType(StrikeType::TYPE_PUTT);
                if ($putts->count() === 3) {
                    $user++;
                }
            }
        }

        foreach ($result['others'] as $round) {
            /** @var Hole $hole */
            foreach ($round->getHoles() as $hole) {
                $putts = $hole->getStrikesByType(StrikeType::TYPE_PUTT);
                if ($putts->count() === 3) {
                    $other++;
                }
            }
        }

        $user = $user === 0 ? 0 : round($user / count($result['user']), 1, PHP_ROUND_HALF_UP);
        $other = $other === 0 ? 0 : round($other / count($result['others']), 1, PHP_ROUND_HALF_UP);

        return new ResultResponse(
            $user,
            $other,
            $this->translator->trans(LegendValue::PUTTS_FREQUENCY_PLAYER, ['%total%' => $user]),
            $this->translator->trans(LegendValue::PUTTS_FREQUENCY_GROUP, ['%total%' => $other])
        );
    }


}