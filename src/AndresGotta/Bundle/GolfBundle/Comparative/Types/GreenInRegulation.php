<?php

namespace AndresGotta\Bundle\GolfBundle\Comparative\Types;

use AndresGotta\Bundle\GolfBundle\Comparative\BaseType;
use AndresGotta\Bundle\GolfBundle\Comparative\ChartGroup;
use AndresGotta\Bundle\GolfBundle\Comparative\RenderStrategy\LegendValue;
use AndresGotta\Bundle\GolfBundle\Comparative\ResultResponse;
use AndresGotta\Bundle\GolfBundle\Entity\Hole;
use AndresGotta\Bundle\GolfBundle\Entity\Round;
use AndresGotta\Bundle\GolfBundle\ValueObject\ComparativeLabels;
use AndresGotta\Bundle\GolfBundle\ValueObject\Result;
use Doctrine\ORM\QueryBuilder;

/**
 * Class GreenInRegulation
 * @package AndresGotta\Bundle\GolfBundle\Comparative\Types
 */
class GreenInRegulation extends BaseType
{
    /**
     * Se toman en cuenta únicamente los: Primer tiro en Par 3, segundo tiro en par 4 y tercer tiro en par 5.
     * Esa suma da el total de tiros. Luego, de ese total, se muestra el porcentaje de ellos que dieron como
     * resultado en Green o embocada y se muestra el valor en relativo.
     *
     * NOTA: si en un par 4 el primer tiro da como resultado en Green o embocada, se usa ese y no el segundo tiro.
     * Si en un par 5, el segundo tiro da como resultado en Green o embocada, se toma ese y no el tercero.
     *
     * @inheritdoc
     */
    public function calculateValues()
    {
        $playerRound = $this->getPlayerRounds();

        if (count($playerRound) == 0) {
            $playerTotal = new ResultResponse(
                0,
                0,
                $this->translator->trans($this->translator->trans(LegendValue::GOOD_RESULTS_LEGEND)),
                $this->translator->trans($this->translator->trans(LegendValue::BAD_RESULTS_LEGEND))
            );
            $groupTotal = new ResultResponse(
                0,
                0,
                $this->translator->trans($this->translator->trans(LegendValue::GOOD_RESULTS_LEGEND)),
                $this->translator->trans($this->translator->trans(LegendValue::BAD_RESULTS_LEGEND))
            );
            $results = new ChartGroup();
            $results->add($playerTotal);
            $results->add($groupTotal);

            return $results;
        }

        $playerGroups = $this->getOtherPlayersRounds();

        $playerTotal = $this->calculateAverageFromTotal($playerRound, 0);
        $playerTotal->setTitle($this->translator->trans(ComparativeLabels::PLAYER_LABEL));

        $groupTotal = $this->calculateAverageFromTotal($playerGroups, 0);
        $groupTotal->setTitle($this->translator->trans(ComparativeLabels::GROUP_LABEL));

        $results = new ChartGroup();
        $results->add($playerTotal);
        $results->add($groupTotal);

        return $results;
    }

    /**
     * @inheritdoc
     */
    protected function calculateAverageFromTotal($result, $total)
    {
        $good = $total = 0;
        $possibleResults = [Result::INSIDE_GREEN, Result::HOLE_OUT];

        /** @var Round $round */
        foreach ($result as $round) {
            /** @var Hole $hole */
            foreach ($round->getHoles() as $hole) {
                $firstStrike = $hole->getStrikeBySort(1);

                switch ($hole->getPar()) {//depende del par, lo consideramos?????
                    case 3:
                        $total += 1;
                        if (in_array($firstStrike->getResult(), $possibleResults)) {
                            $good++;
                        }

                        break;
                    case 4:
                        $secondStrike = $hole->getStrikeBySort(2);
                        if (!$secondStrike && !in_array($firstStrike->getResult(), $possibleResults)) {
                            continue 2;
                        }

                        $total += !$secondStrike ? 0 : 1;

                        if (in_array($firstStrike->getResult(), $possibleResults) ||
                            ($secondStrike && in_array($secondStrike->getResult(), $possibleResults)))
                        {
                            $good++;
                        }

                        break;
                    case 5:
                        if (!in_array($firstStrike->getResult(), $possibleResults)) {
                            break;
                        }

                        $secondStrike = $hole->getStrikeBySort(2);
                        $thirdStrike = $hole->getStrikeBySort(3);


                        if (!$secondStrike) {
                            break;
                        }

                        if (!$thirdStrike && !in_array($secondStrike->getResult(), $possibleResults)) {
                            break;
                        }

                        //$total += !$thirdStrike ? 0 : 1;
                        $total++;

                        if ( in_array($secondStrike->getResult(), $possibleResults)
                            || ($thirdStrike && in_array($thirdStrike->getResult(), $possibleResults)))
                        {
                            $good++;
                        }

                        break;
                }
            }
        }

        $good = $total > 0 ? round(($good * 100) / $total, 1, PHP_ROUND_HALF_UP) : 0;
        $bad = round(100 - $good, 1, PHP_ROUND_HALF_UP);

        return new ResultResponse(
            $good,
            $bad,
            $this->translator->trans($this->translator->trans(LegendValue::GOOD_RESULTS_LEGEND)),
            $this->translator->trans($this->translator->trans(LegendValue::BAD_RESULTS_LEGEND))
        );
    }

}