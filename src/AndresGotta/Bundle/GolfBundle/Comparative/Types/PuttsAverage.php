<?php

namespace AndresGotta\Bundle\GolfBundle\Comparative\Types;

use AndresGotta\Bundle\GolfBundle\Comparative\BaseType;
use AndresGotta\Bundle\GolfBundle\Comparative\ChartGroup;
use AndresGotta\Bundle\GolfBundle\Comparative\RenderStrategy\LegendValue;
use AndresGotta\Bundle\GolfBundle\Comparative\ResultResponse;
use AndresGotta\Bundle\GolfBundle\Entity\Hole;
use AndresGotta\Bundle\GolfBundle\ValueObject\Result;

/**
 * Class PuttsAverage
 * @package AndresGotta\Bundle\GolfBundle\Comparative\Types
 */
class PuttsAverage extends BaseType
{
    /**
     * A) El promedio de los putts totales (golpes en el Green) que realizó el jugador en vueltas completas de 18 hoyos.
     * ‘Promedio de putts totales’
     *
     * $hoyosConPutt = HOYOS DE VUELTAS DE 18 COMPLETADAS POR EL JUGADOR EN LAS QUE SE EJECUTA AL MENOS UN PUTT
     * $puttsTotales = SUMA TOTAL DE GOLPES PUTT DEL JUGADOR EN $hoyosConPutt
     * $promedioPuttsTotales = $puttsTotales / $hoyosConPutt
     *
     * EJ:
     * 1- SE JUEGA UN HOYO EN LA QUE SE LLEVAN A CABO 3 PUTTS PARA EMBOCAR.
     * 2- SE JUEGA UN HOYO EN LA QUE SE LLEVAN A CABO 2 PUTTS PARA EMBOCAR.
     * 3- SE JUEGA UN HOYO EN LA QUE SE LLEVA A CABO 1 PUTT PARA EMBOCAR.
     * 4- SE JUEGA UN HOYO EN LA QUE SE EMBOCA SIN LLEGAR AL PUTT.
     * 5- SE JUEGA UN HOYO EN LA QUE SE LLEVAN A CABO 3 PUTTS PARA EMBOCAR.
     * 6- SE JUEGA UN HOYO EN LA QUE SE LLEVAN A CABO 4 PUTTS PARA EMBOCAR.
     *
     * $hoyosConPutt = 5 (1, 2, 3, 5, 6)
     * $puttsTotales = 13 (3 + 2 + 1 + 3 + 4)
     * $promedioPuttsTotales = 13 / 5 = 2.6
     *
     * PUTTS TOTALES: 13 (PROMEDIO 2.6 / GREEN)
     *
     * -----------------------------------------------------------------------------------------------------------------
     *
     * B) En todos los hoyos en donde se dio como resultado en Green con el primer tiro en par 3, primer o segundo tiro
     * en par 4, segundo o tercer tiro en par 5, se suman todos los putts ejecutados para embocar en esos greens y se
     * devuelve el valor promedio de los mismos.
     * ‘Promedio de putts en Greens en regulación acertados’
     *
     * $hoyosConPuttAcertados = HOYOS DE VUELTAS DE 18 COMPLETADAS POR EL JUGADOR EN LAS QUE SE EJECUTA AL MENOS UN PUTT
     * HABIENDO QUEDADO LA PELOTA EN EL GREEN EN EL PRIMER TIRO EN PAR 3, EN EL PRIMER O SEGUNDO TIRO EN PAR 4, Y EN EL
     * SEGUNDO O TERCER TIRO EN PAR 5
     * $puttsTotalesAcertados = SUMA TOTAL DE GOLPES PUTT DEL JUGADOR EN $hoyosConPuttAcertados
     * $promedioPuttsTotalesAcertados = $puttsTotalesAcertados / $hoyosConPuttAcertados
     *
     * EJ (EXTENTIENDO A):
     * 1- SE JUEGA UN HOYO PAR 3 EN LA QUE SE LLEVAN A CABO 3 PUTTS PARA EMBOCAR. CAE EN EL GREEN EN EL SEGUNDO GOLPE. ESTE VA EN A.
     * 2- SE JUEGA UN HOYO PAR 4 EN LA QUE SE LLEVAN A CABO 2 PUTTS PARA EMBOCAR. CAE EN EL GREEN EN EL SEGUNDO GOLPE.
     * 3- SE JUEGA UN HOYO PAR 5 EN LA QUE SE LLEVA A CABO 1 PUTT PARA EMBOCAR. CAE EN EL GREEN EN EL TERCER GOLPE.
     * 4- SE JUEGA UN HOYO EN LA QUE SE EMBOCA SIN LLEGAR AL PUTT.
     * 5- SE JUEGA UN HOYO PAR 3 EN LA QUE SE LLEVAN A CABO 3 PUTTS PARA EMBOCAR. CAE FUERA DEL GREEN EN EL PRIMER GOLPE.
     * 6- SE JUEGA UN HOYO PAR 5 EN LA QUE SE LLEVAN A CABO 4 PUTTS PARA EMBOCAR. CAE FUERA DEL GREEN EN EL SEGUNDO GOLPE. SI EL TERCER GOLPE ENTRA, VA A B, SINO A C.
     *
     * $hoyosConPuttAcertados = 2 (2, 3)
     * $puttsTotalesAcertados = 3 (2 + 1)
     * $promedioPuttsTotalesAcertados = 3 / 2 = 1.5
     *
     * PUTTS TOTALES ACERTADOS: 3 (PROMEDIO 1.5 / GREEN)
     *
     * -----------------------------------------------------------------------------------------------------------------
     *
     * C) En todos los hoyos en donde se dio como resultado Fuera del green con el primer tiro en par 3, primer o segundo
     * tiro en par 4, segundo o tercer tiro en par 5, se suman todos los putts ejecutados para embocar en esos greens y
     * se devuelve el valor promedio de los mismos.
     * ‘Promedio de putts en Greens en Regulación errados’
     *
     * $hoyosConPuttErrados = HOYOS DE VUELTAS DE 18 COMPLETADAS POR EL JUGADOR EN LAS QUE SE EJECUTA AL MENOS UN PUTT
     * HABIENDO QUEDADO LA PELOTA FUERA DEL GREEN EN EL PRIMER TIRO EN PAR 3, EN EL PRIMER O SEGUNDO TIRO EN PAR 4, Y EN
     * EL SEGUNDO O TERCER TIRO EN PAR 5
     * $puttsTotalesErrados = SUMA TOTAL DE GOLPES PUTT DEL JUGADOR EN $hoyosConPuttErrados
     * $promedioPuttsTotalesErrados = $puttsTotalesErrados / $hoyosConPuttErrados
     *
     * EJ (EXTENTIENDO A):
     * 1- SE JUEGA UN HOYO PAR 3 EN LA QUE SE LLEVAN A CABO 3 PUTTS PARA EMBOCAR. CAE EN EL GREEN EN EL SEGUNDO GOLPE.
     * 2- SE JUEGA UN HOYO PAR 4 EN LA QUE SE LLEVAN A CABO 2 PUTTS PARA EMBOCAR. CAE EN EL GREEN EN EL SEGUNDO GOLPE.
     * 3- SE JUEGA UN HOYO PAR 5 EN LA QUE SE LLEVA A CABO 1 PUTT PARA EMBOCAR. CAE EN EL GREEN EN EL TERCER GOLPE.
     * 4- SE JUEGA UN HOYO EN LA QUE SE EMBOCA SIN LLEGAR AL PUTT.
     * 5- SE JUEGA UN HOYO PAR 3 EN LA QUE SE LLEVAN A CABO 3 PUTTS PARA EMBOCAR. CAE FUERA DEL GREEN EN EL PRIMER GOLPE.
     * 6- SE JUEGA UN HOYO PAR 5 EN LA QUE SE LLEVAN A CABO 4 PUTTS PARA EMBOCAR. CAE FUERA DEL GREEN EN EL SEGUNDO GOLPE.
     *
     * $hoyosConPuttErrados = 2 (5, 6)
     * $puttsTotalesErrados = 7 (3 + 4)
     * $promedioPuttsTotalesErrados = 7 / 2 = 3.5
     *
     * PUTTS TOTALES ERRADOS: 7 (PROMEDIO 3.5 / GREEN)
     *
     * @inheritdoc
     */
    public function calculateValues()
    {
        $rounds = $this->getPlayerRounds();

        $roundCount = count($rounds);
        if ($roundCount == 0) {
            $resultA = new ResultResponse(
                0,
                0,
                $this->translator->trans(LegendValue::PUTTS_LEGEND, ['%putts%' => 0, '%green%' => 0]),
                $this->translator->trans(LegendValue::PUTTS_AVERAGE_LEGEND, ['%putts%' => 0, '%green%' => 0])
            );
            $resultA->setGeneralLegend('Promedio de putts totales');

            $resultB = new ResultResponse(
                0,
                0,
                $this->translator->trans(LegendValue::PUTTS_LEGEND, ['%putts%' => 0, '%green%' => 0]),
                $this->translator->trans(LegendValue::PUTTS_AVERAGE_LEGEND, ['%putts%' => 0, '%green%' => 0])
            );
            $resultB->setGeneralLegend('Promedio de putts en Greens en regulación acertados');

            $resultC = new ResultResponse(
                0,
                0,
                $this->translator->trans(LegendValue::PUTTS_LEGEND, ['%putts%' => 0, '%green%' => 0]),
                $this->translator->trans(LegendValue::PUTTS_AVERAGE_LEGEND, ['%putts%' => 0, '%green%' => 0])
            );
            $resultC->setGeneralLegend('Promedio de putts en Greens en Regulación errados');

            $result = new ChartGroup();
            $result->add($resultA);
            $result->add($resultB);
            $result->add($resultC);

            return $result;
        }

        $otherRounds = $this->getOtherPlayersRounds();

        $puttsAveragePerRound = round($this->getPuttsFromTotalGreens($rounds)/ $roundCount, 1);
        $totalGreens = $this->getTotalGreens($rounds);
        $puttsAveragePerGreen = round($totalGreens ? $this->getPuttsFromTotalGreens($rounds)/ $totalGreens : 0, 1);
        $otherRoundsCount = count($otherRounds);
        $otherPuttsAveragePerRound = round($otherRoundsCount ? $this->getPuttsFromTotalGreens($otherRounds)/ $otherRoundsCount : 0, 1);
        $totalGreens = $this->getTotalGreens($otherRounds);
        $otherPuttsAveragePerGreen = round($totalGreens ? $this->getPuttsFromTotalGreens($otherRounds)/ $totalGreens : 0, 1);

        $resultA = new ResultResponse(
            $puttsAveragePerRound,
            $otherPuttsAveragePerRound,
            $this->translator->trans(LegendValue::PUTTS_LEGEND, ['%putts%' => $puttsAveragePerRound, '%green%' => $puttsAveragePerGreen]),
            $this->translator->trans(LegendValue::PUTTS_AVERAGE_LEGEND, ['%putts%' => $otherPuttsAveragePerRound, '%green%' => $otherPuttsAveragePerGreen])
        );
        $resultA->setGeneralLegend('Promedio de putts totales');

        $puttsAveragePerRound = round($this->getPuttsFromRightGreens($rounds)/ $roundCount, 1);
        $rightGreens = $this->getRightGreens($rounds);
        $puttsAveragePerGreen = round($rightGreens ? $this->getPuttsFromRightGreens($rounds)/ $rightGreens : 0, 1);
        $otherPuttsAveragePerRound = round($otherRoundsCount ? $this->getPuttsFromRightGreens($otherRounds)/ $otherRoundsCount : 0, 1);
        $rightGreens = $this->getRightGreens($otherRounds);
        $otherPuttsAveragePerGreen = round($rightGreens ? $this->getPuttsFromRightGreens($otherRounds)/ $rightGreens : 0, 1);

        $resultB = new ResultResponse(
            $puttsAveragePerRound,
            $otherPuttsAveragePerRound,
            $this->translator->trans(LegendValue::PUTTS_LEGEND, ['%putts%' => $puttsAveragePerRound, '%green%' => $puttsAveragePerGreen]),
            $this->translator->trans(LegendValue::PUTTS_AVERAGE_LEGEND, ['%putts%' => $otherPuttsAveragePerRound, '%green%' => $otherPuttsAveragePerGreen])
        );
        $resultB->setGeneralLegend('Promedio de putts en Greens en regulación acertados');

        $puttsAveragePerRound = round($this->getPuttsFromMissedGreens($rounds)/ $roundCount, 1);
        $missedGreens = $this->getMissedGreens($rounds);
        $puttsAveragePerGreen = round($missedGreens ? $this->getPuttsFromMissedGreens($rounds)/ $missedGreens : 0, 1);
        $otherPuttsAveragePerRound = round($otherRoundsCount ? $this->getPuttsFromMissedGreens($otherRounds)/ $otherRoundsCount : 0, 1);
        $missedGreens = $this->getMissedGreens($otherRounds);
        $otherPuttsAveragePerGreen = round($missedGreens ? $this->getPuttsFromMissedGreens($otherRounds)/ $missedGreens : 0, 1);

        $resultC = new ResultResponse(
            $puttsAveragePerRound,
            $otherPuttsAveragePerRound,
            $this->translator->trans(LegendValue::PUTTS_LEGEND, ['%putts%' => $puttsAveragePerRound, '%green%' => $puttsAveragePerGreen]),
            $this->translator->trans(LegendValue::PUTTS_AVERAGE_LEGEND, ['%putts%' => $otherPuttsAveragePerRound, '%green%' => $otherPuttsAveragePerGreen])
        );
        $resultC->setGeneralLegend('Promedio de putts en Greens en Regulación errados');

        $result = new ChartGroup();
        $result->add($resultA);
        $result->add($resultB);
        $result->add($resultC);

        return $result;
    }

    /**
     * @param $rounds
     * @return int
     */
    private function getTotalGreens($rounds)
    {
        $totalGreens = 0;
        foreach ($rounds as $round) {
            foreach ($round->getHoles() as $hole) {
                if ($hole->getTotalPutts() > 0) {
                    ++$totalGreens;
                }
            }
        }

        return $totalGreens;
    }

    /**
     * @param $rounds
     * @return int
     */
    private function getRightGreens($rounds)
    {
        $rightGreens = 0;
        foreach ($rounds as $round) {
            foreach ($round->getHoles() as $hole) {
                if ($hole->getTotalPutts() > 0) {
                    switch ($hole->getPar()) {
                        case 3:
                            if ($hole->getStrikeBySort(1)->getResult() === Result::INSIDE_GREEN) {
                                ++$rightGreens;
                            }

                            break;
                        case 4:
                            if (($hole->getStrikeBySort(1)->getResult() === Result::INSIDE_GREEN) || ($hole->getStrikeBySort(2) && $hole->getStrikeBySort(2)->getResult() === Result::INSIDE_GREEN)) {
                                ++$rightGreens;
                            }

                            break;
                        case 5:
                            if (($hole->getStrikeBySort(1)->getResult() === Result::INSIDE_GREEN) || ($hole->getStrikeBySort(2) && $hole->getStrikeBySort(2)->getResult() === Result::INSIDE_GREEN) || ($hole->getStrikeBySort(3) && $hole->getStrikeBySort(3)->getResult() === Result::INSIDE_GREEN)) {
                                ++$rightGreens;
                            }

                            break;
                    }
                }
            }
        }

        return $rightGreens;
    }

    /**
     * @param $rounds
     * @return int
     */
    private function getMissedGreens($rounds)
    {
        $missedGreens = 0;
        foreach ($rounds as $round) {
            foreach ($round->getHoles() as $hole) {
                if ($hole->getTotalPutts() > 0) {
                    switch ($hole->getPar()) {
                        case 3:
                            if ($hole->getStrikeBySort(1)->getResult() !== Result::INSIDE_GREEN) {
                                ++$missedGreens;
                            }

                            break;
                        case 4:
                            if (($hole->getStrikeBySort(1)->getResult() !== Result::INSIDE_GREEN) && ($hole->getStrikeBySort(2) && $hole->getStrikeBySort(2)->getResult() !== Result::INSIDE_GREEN)) {
                                ++$missedGreens;
                            }

                            break;
                        case 5:
                            if (($hole->getStrikeBySort(1)->getResult() !== Result::INSIDE_GREEN) && ($hole->getStrikeBySort(2) && $hole->getStrikeBySort(2)->getResult() !== Result::INSIDE_GREEN) && ($hole->getStrikeBySort(3) && $hole->getStrikeBySort(3)->getResult() !== Result::INSIDE_GREEN)) {
                                ++$missedGreens;
                            }

                            break;
                    }
                }
            }
        }

        return $missedGreens;
    }

    /**
     * @param $rounds
     * @return int
     */
    private function getPuttsFromTotalGreens($rounds)
    {
        $puttsFromTotalGreens = 0;
        foreach ($rounds as $round) {
            foreach ($round->getHoles() as $hole) {
                if ($hole->getTotalPutts() > 0) {
                    $puttsFromTotalGreens += $hole->getTotalPutts();
                }
            }
        }

        return $puttsFromTotalGreens;
    }

    /**
     * @param $rounds
     * @return int
     */
    private function getPuttsFromRightGreens($rounds)
    {
        $puttsFromRightGreens = 0;
        foreach ($rounds as $round) {
            foreach ($round->getHoles() as $hole) {
                if ($hole->getTotalPutts() > 0) {
                    switch ($hole->getPar()) {
                        case 3:
                            if ($hole->getStrikeBySort(1)->getResult() === Result::INSIDE_GREEN) {
                                $puttsFromRightGreens += $hole->getTotalPutts();
                            }

                            break;
                        case 4:
                            if (($hole->getStrikeBySort(1)->getResult() === Result::INSIDE_GREEN) || ($hole->getStrikeBySort(2) && $hole->getStrikeBySort(2)->getResult() === Result::INSIDE_GREEN)) {
                                $puttsFromRightGreens += $hole->getTotalPutts();
                            }

                            break;
                        case 5:
                            if (($hole->getStrikeBySort(1)->getResult() === Result::INSIDE_GREEN) || ($hole->getStrikeBySort(2) && $hole->getStrikeBySort(2)->getResult() === Result::INSIDE_GREEN) || ($hole->getStrikeBySort(3) && $hole->getStrikeBySort(3)->getResult() === Result::INSIDE_GREEN)) {
                                $puttsFromRightGreens += $hole->getTotalPutts();
                            }

                            break;
                    }
                }
            }
        }

        return $puttsFromRightGreens;
    }

    /**
     * @param $rounds
     * @return int
     */
    private function getPuttsFromMissedGreens($rounds)
    {
        $puttsFromMissedGreens = 0;
        foreach ($rounds as $round) {
            foreach ($round->getHoles() as $hole) {
                if ($hole->getTotalPutts() > 0) {
                    switch ($hole->getPar()) {
                        case 3:
                            if ($hole->getStrikeBySort(1)->getResult() !== Result::INSIDE_GREEN) {
                                $puttsFromMissedGreens += $hole->getTotalPutts();
                            }

                            break;
                        case 4:
                            if (($hole->getStrikeBySort(1)->getResult() !== Result::INSIDE_GREEN) && ($hole->getStrikeBySort(2) && $hole->getStrikeBySort(2)->getResult() !== Result::INSIDE_GREEN)) {
                                $puttsFromMissedGreens += $hole->getTotalPutts();
                            }

                            break;
                        case 5:
                            if (($hole->getStrikeBySort(1)->getResult() !== Result::INSIDE_GREEN) && ($hole->getStrikeBySort(2) && $hole->getStrikeBySort(2)->getResult() !== Result::INSIDE_GREEN) && ($hole->getStrikeBySort(3) && $hole->getStrikeBySort(3)->getResult() !== Result::INSIDE_GREEN)) {
                                $puttsFromMissedGreens += $hole->getTotalPutts();
                            }

                            break;
                    }
                }
            }
        }

        return $puttsFromMissedGreens;
    }
}
