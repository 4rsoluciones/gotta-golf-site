<?php

namespace AndresGotta\Bundle\GolfBundle\Comparative;

use Doctrine\Common\Collections\Collection;
use WebFactory\Bundle\UserBundle\Entity\User;

/**
 * Interface BaseComparativeInterface
 * @package AndresGotta\Bundle\GolfBundle\Comparative
 */
interface BaseTypeInterface
{
    /**
     * @param int $from
     */
    public function setFrom($from);

    /**
     * @param User $player
     */
    public function setPlayerToCompare(User $player);

    /**
     * @param Collection $players
     */
    public function setOthersPlayersToCompare(Collection $players);

    /**
     * @return ChartGroup
     */
    public function calculateValues();
}