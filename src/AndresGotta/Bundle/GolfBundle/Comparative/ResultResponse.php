<?php

namespace AndresGotta\Bundle\GolfBundle\Comparative;

use AndresGotta\Bundle\GolfBundle\Comparative\RenderStrategy\LegendValue;

/**
 * Class ResultResponse
 * @package AndresGotta\Bundle\GolfBundle\Comparative
 */
class ResultResponse
{
    /**
     * @var float
     */
    protected $xAxis;

    /**
     * @var string
     */
    protected $xLegend;

    /**
     * @var float
     */
    protected $yAxis;

    /**
     * @var string
     */
    protected $yLegend;

    /**
     * @var string
     */
    protected $generalLegend;

    /**
     * @var string
     */
    protected $title;

    /**
     * ResultResponse constructor.
     * @param float $xAxis
     * @param string $xLegend
     * @param float $yAxis
     * @param string $yLegend
     */
    public function __construct($xAxis, $yAxis, $xLegend, $yLegend)
    {
        $this->xAxis = $xAxis;
        $this->xLegend = $xLegend;
        $this->yAxis = $yAxis;
        $this->yLegend = $yLegend;
    }

    /**
     * @return float
     */
    public function getXAxis()
    {
        return $this->xAxis;
    }

    /**
     * @return string
     */
    public function getXLegend()
    {
        return $this->xLegend;
    }

    /**
     * @return float
     */
    public function getYAxis()
    {
        return $this->yAxis;
    }

    /**
     * @return string
     */
    public function getYLegend()
    {
        return $this->yLegend;
    }

    /**
     * @return string
     */
    public function getGeneralLegend()
    {
        return $this->generalLegend;
    }

    /**
     * @param string $generalLegend
     */
    public function setGeneralLegend($generalLegend)
    {
        $this->generalLegend = $generalLegend;
    }

    /**
     * @return string
     */
    public function getTitle()
    {
        return $this->title;
    }

    /**
     * @param string $title
     */
    public function setTitle($title)
    {
        $this->title = $title;
    }

}