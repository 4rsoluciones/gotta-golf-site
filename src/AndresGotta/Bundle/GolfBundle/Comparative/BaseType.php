<?php

namespace AndresGotta\Bundle\GolfBundle\Comparative;

use AndresGotta\Bundle\GolfBundle\Entity\Round;
use AndresGotta\Bundle\GolfBundle\ValueObject\RoundType;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\EntityManager;
use Doctrine\ORM\QueryBuilder;
use Symfony\Component\Translation\TranslatorInterface;
use WebFactory\Bundle\UserBundle\Entity\Profile;
use WebFactory\Bundle\UserBundle\Entity\User;

/**
 * Class BaseType
 * @package AndresGotta\Bundle\GolfBundle\Comparative\Types
 */
abstract class BaseType implements BaseTypeInterface
{
    /**
     * @var \DateTime
     */
    protected $from;

    /**
     * @var User
     */
    protected $player;

    /**
     * @var Collection|Profile[]
     */
    protected $otherPlayers;

    /**
     * @var EntityManager
     */
    protected $entityManager;

    /**
     * @var TranslatorInterface
     */
    protected $translator;

    /**
     * BaseType constructor.
     * @param TranslatorInterface $translator
     * @param EntityManager $entityManager
     */
    public function __construct(TranslatorInterface $translator, EntityManager $entityManager)
    {
        $this->translator = $translator;
        $this->entityManager = $entityManager;
    }

    /**
     * @param integer $from
     */
    public function setFrom($from)
    {
        $today = new \DateTime();
        $interval = new \DateInterval('P'. $from .'D');

        $this->from = $today->sub($interval);
    }

    /**
     * @param User $player
     */
    public function setPlayerToCompare(User $player)
    {
        $this->player = $player;
    }

    /**
     * @param Collection $otherPlayers
     */
    public function setOthersPlayersToCompare(Collection $otherPlayers)
    {
        $this->otherPlayers = $otherPlayers;
    }

    /**
     * @param string $entityName
     * @return QueryBuilder
     */
    protected function generateBaseQueryBuilder($entityName)
    {
        $qb = $this->entityManager->getRepository($entityName)->createQueryBuilder('strike');
        $qb

            //--------------------------------
//                ->addSelect('hole, round, Field, player, profile')
            //--------------------------------


            ->leftJoin('strike.hole', 'hole')
            ->leftJoin('hole.round', 'round')
            ->leftJoin('round.field', 'Field')
            ->leftJoin('round.player', 'player')
            ->leftJoin('player.profile', 'profile')
            ->where('profile.gender = :gender')->setParameter('gender', $this->player->getProfile()->getGender())
            ->andWhere('round.playerConfirmed = true')
            ->andWhere('round.playedHoles = 18')
            ->andWhere('round.roundType = :roundType')->setParameter('roundType', RoundType::TOURNAMENT)
        ;

        return $qb;
    }

    /**
     * @param QueryBuilder $queryBuilder
     * @return array
     */
    protected function calculatePlayerCondition(QueryBuilder $queryBuilder)
    {
        $newQuery = clone $queryBuilder;
        $newQuery->andWhere('round.player = :player')->setParameter('player', $this->player->getId());

        return $newQuery->getQuery()
//            ->useQueryCache(true)->useResultCache(true)
            ->getResult();
    }

    /**
     * @param QueryBuilder $queryBuilder
     * @return array
     */
    protected function calculateOtherPlayersCondition(QueryBuilder $queryBuilder)
    {
        $newQuery = clone $queryBuilder;
        $others = $this->otherPlayers->toArray();

        return $newQuery
            ->andWhere('profile.id IN (:players)')->setParameter('players', $others)
            ->getQuery()
//            ->useQueryCache(true)->useResultCache(true)
            ->getResult()
        ;
    }

    /**
     * @return Round[]|array
     */
    protected function getPlayerRounds()
    {
        return $this->entityManager->getRepository('AndresGottaGolfBundle:Round')
            ->createQueryBuilder('Round')
            ->addSelect('Field')
            ->join('Round.field', 'Field')

            ->andWhere('Round.player = :player')->setParameter('player', $this->player)
            ->andWhere('Round.playerConfirmed = true')
            ->andWhere('Round.roundType = :round_type')->setParameter('round_type', RoundType::TOURNAMENT)
            ->andWhere('Round.playedHoles = 18')
//            ->andWhere('Field.holeCount = 18')
            ->getQuery()
//            ->useQueryCache(true)->useResultCache(true)
            ->getResult()
            ;
//            ->findBy(['player' => $this->player->getId(), 'playerConfirmed' => 1, 'holeCount' => 18, 'roundType' => RoundType::TOURNAMENT]);
    }

    /**
     * @return array
     */
    protected function getOtherPlayersRounds()
    {
        $qb = $this->entityManager->getRepository('AndresGottaGolfBundle:Round')->createQueryBuilder('r');
        $qb
            ->addSelect('p')
            ->leftJoin('r.player', 'p')
            ->leftJoin('p.profile', 'profile')
            ->where('r.playerConfirmed = true')
            ->andWhere('r.playedHoles = 18')
            ->andWhere('profile.id IN (:players)')->setParameter('players', $this->otherPlayers->toArray())
            ->andWhere('r.roundType = :roundType')->setParameter('roundType', RoundType::TOURNAMENT)
        ;

        return $qb->getQuery()
//            ->useQueryCache(true)->useResultCache(true)
            ->getResult();
    }

    /**
     * @param $total
     * @throws \Exception
     */
    protected function calculateTotals($total)
    {
        throw new \Exception('This method must be implemented');
    }

    /**
     * @param array $result
     * @param int $total
     * @throws \Exception
     */
    protected function calculateAverageFromTotal($result, $total)
    {
        throw new \Exception('This method must be implemented');
    }
}