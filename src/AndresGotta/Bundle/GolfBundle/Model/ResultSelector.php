<?php

namespace AndresGotta\Bundle\GolfBundle\Model;

use AndresGotta\Bundle\GolfBundle\ValueObject\Result;
use AndresGotta\Bundle\GolfBundle\ValueObject\StrikeType;

/**
 * Class ResultSelector
 * @package AndresGotta\Bundle\GolfBundle\Model
 */
class ResultSelector implements ChoiceSelector
{
    /**
     * @inheritDoc
     */
    public function getChoices($type, $par)
    {
        $results = [

            StrikeType::TYPE_FIRST => [
                3 => [
                    Result::BALL_IN_HAZARD,
                    Result::OUT_OF_LIMITS,
                    Result::INSIDE_GREEN,
                    Result::OUTSIDE_GREEN,
                    Result::OUTSIDE_GREEN_INTO_THE_BUNKER,
                    Result::HOLE_OUT,
                ],
                4 => [
                    Result::BALL_IN_HAZARD,
                    Result::OUT_OF_LIMITS,
                    Result::INSIDE_GREEN,
                    Result::INSIDE_FAIRWAY,
                    Result::OUTSIDE_FAIRWAY,
                    Result::OUTSIDE_FAIRWAY_INTO_THE_BUNKER,
                    Result::CROSS_BUNKER_IN_FAIRWAY,
                    Result::HOLE_OUT,
                ],
                5 => [
                    Result::BALL_IN_HAZARD,
                    Result::OUT_OF_LIMITS,
                    Result::INSIDE_GREEN,
                    Result::INSIDE_FAIRWAY,
                    Result::OUTSIDE_FAIRWAY,
                    Result::OUTSIDE_FAIRWAY_INTO_THE_BUNKER,
                    Result::CROSS_BUNKER_IN_FAIRWAY,
                    Result::HOLE_OUT,
                ],
            ],

            StrikeType::TYPE_OTHER => [
                3 => [
                    Result::BALL_IN_HAZARD,
                    Result::OUT_OF_LIMITS,
                    Result::INSIDE_GREEN,
                    Result::OUTSIDE_GREEN,
                    Result::OUTSIDE_GREEN_INTO_THE_BUNKER,
                    Result::HOLE_OUT,
                ],
                4 => [
                    Result::BALL_IN_HAZARD,
                    Result::OUT_OF_LIMITS,
                    Result::INSIDE_GREEN,
                    Result::OUTSIDE_GREEN,
                    Result::OUTSIDE_GREEN_INTO_THE_BUNKER,
                    Result::HOLE_OUT,
                ],
                5 => [
                    Result::BALL_IN_HAZARD,
                    Result::OUT_OF_LIMITS,
                    Result::INSIDE_GREEN,
                    Result::OUTSIDE_GREEN,
                    Result::OUTSIDE_GREEN_INTO_THE_BUNKER,
                    Result::HOLE_OUT,

                    Result::INSIDE_FAIRWAY,
                    Result::CROSS_BUNKER_IN_FAIRWAY,
                    Result::OUTSIDE_FAIRWAY,
                    Result::OUTSIDE_FAIRWAY_INTO_THE_BUNKER,
                ],
            ],

            StrikeType::TYPE_PUTT => [
                3 => [
                    Result::BALL_IN_HAZARD,
                    Result::OUT_OF_LIMITS,
                    Result::MISSED,
                    Result::HOLE_OUT,
                ],
                4 => [
                    Result::BALL_IN_HAZARD,
                    Result::OUT_OF_LIMITS,
                    Result::MISSED,
                    Result::HOLE_OUT,
                ],
                5 => [
                    Result::BALL_IN_HAZARD,
                    Result::OUT_OF_LIMITS,
                    Result::MISSED,
                    Result::HOLE_OUT,
                ],
            ],
        ];

        if (!isset($results[$type][$par])) {
            return [];
        }

        return array_combine($results[$type][$par], $results[$type][$par]);
    }

}