<?php

namespace AndresGotta\Bundle\GolfBundle\Model;

use AndresGotta\Bundle\GolfBundle\ValueObject\Distance;
use AndresGotta\Bundle\GolfBundle\ValueObject\StrikeType;

/**
 * Class DistanceSelector
 * @package AndresGotta\Bundle\GolfBundle\Model
 */
class DistanceSelector implements ChoiceSelector
{
    /**
     * @inheritDoc
     */
    public function getChoices($type, $par)
    {
        $distances = [
            StrikeType::TYPE_FIRST => [
                3 => [
                    Distance::LESS_THAN_100_YDS,
                    Distance::BTW_100_TO_120_YDS,
                    Distance::BTW_121_TO_140_YDS,
                    Distance::BTW_141_TO_160_YDS,
                    Distance::BTW_161_TO_180_YDS,
                    Distance::BTW_181_TO_200_YDS,
                    Distance::BTW_201_TO_220_YDS,
                    Distance::MORE_THAN_220_YDS,
                ],
                4 => [
                    Distance::LESS_THAN_100_YDS,
                    Distance::BTW_100_TO_120_YDS,
                    Distance::BTW_121_TO_140_YDS,
                    Distance::BTW_141_TO_160_YDS,
                    Distance::BTW_161_TO_180_YDS,
                    Distance::BTW_181_TO_200_YDS,
                    Distance::BTW_201_TO_220_YDS,
                    Distance::BTW_221_TO_240_YDS,
                    Distance::BTW_241_TO_260_YDS,
                    Distance::BTW_261_TO_280_YDS,
                    Distance::BTW_281_TO_300_YDS,
                    Distance::BTW_301_TO_320_YDS,
                    Distance::MORE_THAN_320_YDS,
                ],
                5 => [
                    Distance::LESS_THAN_100_YDS,
                    Distance::BTW_100_TO_120_YDS,
                    Distance::BTW_121_TO_140_YDS,
                    Distance::BTW_141_TO_160_YDS,
                    Distance::BTW_161_TO_180_YDS,
                    Distance::BTW_181_TO_200_YDS,
                    Distance::BTW_201_TO_220_YDS,
                    Distance::BTW_221_TO_240_YDS,
                    Distance::BTW_241_TO_260_YDS,
                    Distance::BTW_261_TO_280_YDS,
                    Distance::BTW_281_TO_300_YDS,
                    Distance::BTW_301_TO_320_YDS,
                    Distance::MORE_THAN_320_YDS,
                ],
            ],

            StrikeType::TYPE_OTHER => [
                3 => [
                    Distance::LESS_THAN_20_YDS,
                    Distance::BTW_21_TO_40_YDS,
                    Distance::BTW_41_TO_60_YDS,
                    Distance::BTW_61_TO_80_YDS,
                    Distance::BTW_81_TO_100_YDS,
                    Distance::BTW_101_TO_120_YDS,
                    Distance::MORE_THAN_120_YDS,
                ],
                4 => [
                    Distance::LESS_THAN_20_YDS,
                    Distance::BTW_21_TO_40_YDS,
                    Distance::BTW_41_TO_60_YDS,
                    Distance::BTW_61_TO_80_YDS,
                    Distance::BTW_81_TO_100_YDS,
                    Distance::BTW_101_TO_120_YDS,
                    Distance::BTW_121_TO_140_YDS,
                    Distance::BTW_141_TO_160_YDS,
                    Distance::BTW_161_TO_180_YDS,
                    Distance::BTW_181_TO_200_YDS,
                    Distance::BTW_201_TO_220_YDS,
                    Distance::BTW_221_TO_240_YDS,
                    Distance::BTW_241_TO_260_YDS,
                    Distance::BTW_261_TO_280_YDS,
                    Distance::BTW_281_TO_300_YDS,
                    Distance::BTW_301_TO_320_YDS,
                    Distance::MORE_THAN_320_YDS,
                ],
                5 => [
                    Distance::LESS_THAN_20_YDS,
                    Distance::BTW_21_TO_40_YDS,
                    Distance::BTW_41_TO_60_YDS,
                    Distance::BTW_61_TO_80_YDS,
                    Distance::BTW_81_TO_100_YDS,
                    Distance::BTW_101_TO_120_YDS,
                    Distance::BTW_121_TO_140_YDS,
                    Distance::BTW_141_TO_160_YDS,
                    Distance::BTW_161_TO_180_YDS,
                    Distance::BTW_181_TO_200_YDS,
                    Distance::MORE_THAN_200_YDS,
                ],
            ],

            StrikeType::TYPE_PUTT => [
                3 => [
                    Distance::LESS_THAN_1_MTS,
                    Distance::BTW_1_TO_2_MTS,
                    Distance::BTW_2_TO_3_MTS,
                    Distance::BTW_3_TO_5_MTS,
                    Distance::BTW_5_TO_7_MTS,
                    Distance::BTW_7_TO_10_MTS,
                    Distance::BTW_10_TO_15_MTS,
                    Distance::MORE_THAN_15_MTS,
                ],
                4 => [
                    Distance::LESS_THAN_1_MTS,
                    Distance::BTW_1_TO_2_MTS,
                    Distance::BTW_2_TO_3_MTS,
                    Distance::BTW_3_TO_5_MTS,
                    Distance::BTW_5_TO_7_MTS,
                    Distance::BTW_7_TO_10_MTS,
                    Distance::BTW_10_TO_15_MTS,
                    Distance::MORE_THAN_15_MTS,
                ],
                5 => [
                    Distance::LESS_THAN_1_MTS,
                    Distance::BTW_1_TO_2_MTS,
                    Distance::BTW_2_TO_3_MTS,
                    Distance::BTW_3_TO_5_MTS,
                    Distance::BTW_5_TO_7_MTS,
                    Distance::BTW_7_TO_10_MTS,
                    Distance::BTW_10_TO_15_MTS,
                    Distance::MORE_THAN_15_MTS,
                ],
            ],
        ];

        if (!isset($distances[$type][$par])) {
            return [];
        }

        return array_combine($distances[$type][$par], $distances[$type][$par]);
    }

}