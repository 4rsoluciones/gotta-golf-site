<?php

namespace AndresGotta\Bundle\GolfBundle\Model;

/**
 * Interface ReasonChoiceSelector
 * @package AndresGotta\Bundle\GolfBundle\Model
 */
interface ReasonChoiceSelector extends ChoiceSelector
{
    /**
     * @param string $type
     * @param string $result
     * @return string[]
     */
    public function getChoices($type, $result);
}
