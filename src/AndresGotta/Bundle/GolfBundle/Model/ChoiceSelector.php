<?php

namespace AndresGotta\Bundle\GolfBundle\Model;

/**
 * Interface ChoiceSelector
 * @package AndresGotta\Bundle\GolfBundle\Model
 */
interface ChoiceSelector
{
    /**
     * @param string $type
     * @param string $par
     * @return string[]
     */
    public function getChoices($type, $par);
}
