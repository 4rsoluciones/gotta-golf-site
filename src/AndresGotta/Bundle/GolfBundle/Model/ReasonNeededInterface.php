<?php

namespace AndresGotta\Bundle\GolfBundle\Model;

/**
 * Interface ReasonNeededInterface
 * @package AndresGotta\Bundle\GolfBundle\Model
 */
interface ReasonNeededInterface
{
    /**
     * @param string $type
     * @param string $par
     * @param string $result
     * @return boolean
     */
    public function isNeeded($type, $par, $result);
}