<?php

namespace AndresGotta\Bundle\GolfBundle\Model;

use AndresGotta\Bundle\GolfBundle\ValueObject\Reason;
use AndresGotta\Bundle\GolfBundle\ValueObject\Result;
use AndresGotta\Bundle\GolfBundle\ValueObject\StrikeType;

/**
 * Class ReasonSelector
 * @package AndresGotta\Bundle\GolfBundle\Model
 */
class ReasonSelector implements ReasonChoiceSelector, ReasonNeededInterface
{
    /**
     * @inheritDoc
     */
    public function getChoices($type, $result)
    {
        $reasons = [
            StrikeType::TYPE_FIRST => [
                Result::OUTSIDE_GREEN => [
                    Reason::SHORT,
                    Reason::LONG,
                    Reason::TO_THE_LEFT,
                    Reason::TO_THE_RIGHT,
                ],
                Result::OUTSIDE_GREEN_INTO_THE_BUNKER => [
                    Reason::SHORT,
                    Reason::LONG,
                    Reason::TO_THE_LEFT,
                    Reason::TO_THE_RIGHT,
                ],
                Result::BALL_IN_HAZARD => [
                    Reason::TO_THE_LEFT,
                    Reason::TO_THE_RIGHT,
                ],
                Result::OUT_OF_LIMITS => [
                    Reason::TO_THE_LEFT,
                    Reason::TO_THE_RIGHT,
                ],
                Result::OUTSIDE_FAIRWAY => [
                    Reason::TO_THE_LEFT,
                    Reason::TO_THE_RIGHT,
                ],
                Result::OUTSIDE_FAIRWAY_INTO_THE_BUNKER => [
                    Reason::TO_THE_LEFT,
                    Reason::TO_THE_RIGHT,
                ],
            ],
            StrikeType::TYPE_OTHER => [
                Result::BALL_IN_HAZARD => [
                    Reason::SHORT,
                    Reason::LONG,
                    Reason::TO_THE_LEFT,
                    Reason::TO_THE_RIGHT,
                ],
                Result::OUT_OF_LIMITS => [
                    Reason::SHORT,
                    Reason::LONG,
                    Reason::TO_THE_LEFT,
                    Reason::TO_THE_RIGHT,
                ],
                Result::OUTSIDE_GREEN => [
                    Reason::SHORT,
                    Reason::LONG,
                    Reason::TO_THE_LEFT,
                    Reason::TO_THE_RIGHT,
                ],
                Result::OUTSIDE_GREEN_INTO_THE_BUNKER => [
                    Reason::SHORT,
                    Reason::LONG,
                    Reason::TO_THE_LEFT,
                    Reason::TO_THE_RIGHT,
                ],
            ],

            StrikeType::TYPE_PUTT => [
                Result::BALL_IN_HAZARD => [
                    Reason::SHORT,
                    Reason::LONG,
                    Reason::TO_THE_LEFT,
                    Reason::TO_THE_RIGHT,
                ],
                Result::OUT_OF_LIMITS => [
                    Reason::SHORT,
                    Reason::LONG,
                    Reason::TO_THE_LEFT,
                    Reason::TO_THE_RIGHT,
                ],
                Result::MISSED => [
                    Reason::SHORT,
                    Reason::LONG,
                    Reason::TO_THE_LEFT,
                    Reason::TO_THE_RIGHT,
                ],
            ],
        ];

        if (!isset($reasons[$type][$result])) {
            return [];
        }

        return array_combine($reasons[$type][$result], $reasons[$type][$result]);
    }

    /**
     * @inheritDoc
     */
    public function isNeeded($type, $par, $result)
    {
        switch ($type) {
            case StrikeType::TYPE_FIRST:
                if ($par === 3) {
                    return $result !== Result::HOLE_OUT && $result !== Result::INSIDE_GREEN;
                }

                if ($par === 4 || $par === 5) {
                    return $result !== Result::HOLE_OUT && $result !== Result::INSIDE_GREEN && $result !== Result::INSIDE_FAIRWAY && $result !== Result::CROSS_BUNKER_IN_FAIRWAY;
                }
                break;
            case StrikeType::TYPE_OTHER:
                return $result !== Result::HOLE_OUT && $result !== Result::INSIDE_GREEN && $result !== Result::INSIDE_FAIRWAY;
            case StrikeType::TYPE_PUTT:
                return $result !== Result::HOLE_OUT;
        }

        return false;
    }

}