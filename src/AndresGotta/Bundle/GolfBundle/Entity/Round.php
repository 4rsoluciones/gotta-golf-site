<?php

namespace AndresGotta\Bundle\GolfBundle\Entity;

use AndresGotta\Bundle\GolfBundle\ValueObject\PlayerCondition;
use AndresGotta\Bundle\GolfBundle\ValueObject\RoundType;
use DateTime;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Validator\Constraints as Assert;
use WebFactory\Bundle\UserBundle\Entity\User;
use Symfony\Component\Validator\Context\ExecutionContext;
use AndresGotta\Bundle\GolfBundle\ValueObject\TournamentType;
use AndresGotta\Bundle\GolfBundle\Entity\PreCompetitiveQuestionnaire;
use AndresGotta\Bundle\GolfBundle\Entity\PostCompetitiveQuestionnaire;

/**
 * Round
 *
 * @ORM\Table(name="rounds")
 * @ORM\Entity(repositoryClass="AndresGotta\Bundle\GolfBundle\Repository\RoundRepository")
 * @Assert\Callback(methods={"isValid"}, groups={"Default"})
 */
class Round
{

    /**
     * @var integer
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     *
     * @var \WebFactory\Bundle\UserBundle\Entity\User
     * @ORM\ManyToOne(targetEntity="WebFactory\Bundle\UserBundle\Entity\User", inversedBy="rounds", cascade={"persist"}, fetch="LAZY")
     * @ORM\JoinColumn(name="player_id", referencedColumnName="id")
     * @Assert\NotBlank(groups={"Default", "OnlyRound"})
     */
    private $player;

    /**
     * @var Field
     * @ORM\OneToOne(targetEntity="AndresGotta\Bundle\GolfBundle\Entity\Field", mappedBy="round", cascade={"all"})
     * @Assert\NotNull(groups={"Default", "OnlyRound"})
     * @Assert\Valid()
     */
    private $field;

    /**
     * @var string
     *
     * @ORM\Column(name="player_confirmed", type="boolean")
     * @Assert\NotNull(groups={"Default"})
     */
    private $playerConfirmed = false;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="performed_at", type="date")
     */
    private $performedAt;

    /**
     * @var string
     *
     * @ORM\Column(name="player_condition", type="string", length=255)
     * @Assert\NotBlank(groups={"Default", "OnlyRound"})
     */
    private $playerCondition;

    /**
     * @var string
     *
     * @ORM\Column(name="handicap", type="integer", nullable=true)
     */
    private $handicap;

    /**
     *
     * @var Tour
     * @ORM\ManyToOne(targetEntity="Tour", inversedBy="rounds", cascade={"persist"}, fetch="LAZY")
     * @ORM\JoinColumn(name="tour_id", referencedColumnName="id")
     */
    private $tour;

    /**
     * @var string
     *
     * @ORM\Column(name="type", type="string", length=255)
     * @Assert\NotBlank(groups={"Default", "OnlyRound"})
     */
    private $roundType;

    /**
     * @var string
     *
     * @ORM\Column(name="tournament_type", type="string", length=255, nullable=true)
     */
    private $tournamentType;

    /**
     * @var integer
     *
     * @ORM\Column(name="number", type="integer", nullable=true)
     */
    private $roundNumber;

    /**
     * @var string
     *
     * @ORM\Column(name="game_condition", type="string", length=255)
     * @Assert\NotBlank(groups={"Default", "OnlyRound"})
     */
    private $gameCondition;

    //------------------------------------------------------------------------------------------------------------------
    // Remover
    //------------------------------------------------------------------------------------------------------------------
    /**
     * @var string
     *
     * @ORM\Column(name="field_type", type="string", length=255, nullable=true)
     * //@Assert\NotBlank(groups={"Default", "OnlyRound"})
     *
     * @deprecated
     */
    private $fieldType;

    /**
     * @var string
     *
     * @ORM\Column(name="field_par", type="string", length=255, nullable=true)
     * //@Assert\NotBlank(groups={"Default", "OnlyRound"})
     *
     * @deprecated
     */
    private $fieldPar;

    /**
     * @var integer
     *
     * @ORM\Column(name="hole_count", type="integer", nullable=true)
     * //@Assert\NotBlank(groups={"Default", "OnlyRound"})
     *
     * @deprecated
     */
    private $holeCount;
    //------------------------------------------------------------------------------------------------------------------

    /**
     * @var ArrayCollection
     *
     * @ORM\OneToMany(targetEntity="Hole", mappedBy="round", cascade={"all"}, orphanRemoval=true)
     * @Assert\Valid()
     */
    private $holes;

    /**
     * @var PreCompetitiveQuestionnaire
     *
     * @ORM\OneToOne(targetEntity="PreCompetitiveQuestionnaire", mappedBy="round", cascade={"all"})
     */
    private $preCompetitiveQuestionnaire;

    /**
     * @var PostCompetitiveQuestionnaire
     *
     * @ORM\OneToOne(targetEntity="PostCompetitiveQuestionnaire", mappedBy="round", cascade={"all"})
     */
    private $postCompetitiveQuestionnaire;

    /**
     * @var integer
     *
     * @ORM\Column(name="played_holes", type="integer", nullable=true)
     * @Assert\NotBlank(groups={"Default", "OnlyRound"})
     */
    private $playedHoles;

    /**
     * Constructor
     * @param User|null $player
     */
    public function __construct(User $player = null)
    {
        $this->holes = new ArrayCollection();
        $this->performedAt = new \DateTime();
        $this->player = $player;
        //Actualmente no los crea, los voy a mantener de esta manera
        //$this->preCompetitiveQuestionnaire = new PreCompetitiveQuestionnaire($this);
        //$this->postCompetitiveQuestionnaire = new PostCompetitiveQuestionnaire($this);
    }

    /**
     * Get id
     *
     * @return integer
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @return User
     */
    public function getPlayer()
    {
        return $this->player;
    }

    /**
     *
     * @param User $player
     * @return Round
     */
    public function setPlayer(User $player)
    {
        $this->player = $player;
        $this->playerCondition = $player->getProfile()->getCondition();

        return $this;
    }

    /**
     * 
     * @return DateTime
     */
    public function getPerformedAt()
    {
        return $this->performedAt;
    }

    /**
     * 
     * @param \DateTime $performedAt
     * @return Round
     */
    public function setPerformedAt(\DateTime $performedAt)
    {
        $this->performedAt = $performedAt;

        return $this;
    }

    /**
     * Set type
     *
     * @param string $roundType
     * @return Round
     */
    public function setRoundType($roundType)
    {
        $this->roundType = $roundType;

        return $this;
    }

    /**
     * Get type
     *
     * @return string
     */
    public function getRoundType()
    {
        return $this->roundType;
    }

    /**
     * Set player condition
     *
     * @param string $playerCondition
     * @return Round
     */
    public function setPlayerCondition($playerCondition)
    {
        $this->playerCondition = $playerCondition;

        return $this;
    }

    /**
     * Get player condition
     *
     * @return string
     */
    public function getPlayerCondition()
    {
        return $this->playerCondition;
    }

    /**
     * Set handicap
     *
     * @param string $handicap
     * @return Round
     */
    public function setHandicap($handicap = null)
    {
        $this->handicap = $handicap;

        return $this;
    }

    /**
     * Get handicap
     *
     * @return string
     */
    public function getHandicap()
    {
        return $this->handicap;
    }

    /**
     * Set tournamentType
     *
     * @param string $tournamentType
     * @return Round
     */
    public function setTournamentType($tournamentType = null)
    {
        $this->tournamentType = $tournamentType;

        return $this;
    }

    /**
     * Get tournamentType
     *
     * @return string
     */
    public function getTournamentType()
    {
        return $this->tournamentType;
    }

    /**
     * Set number
     *
     * @param integer $roundNumber
     * @return Round
     */
    public function setRoundNumber($roundNumber = null)
    {
        $this->roundNumber = $roundNumber;

        return $this;
    }

    /**
     * Get number
     *
     * @return integer
     */
    public function getRoundNumber()
    {
        return $this->roundNumber;
    }

    /**
     * Set gameCondition
     *
     * @param string $gameCondition
     * @return Round
     */
    public function setGameCondition($gameCondition)
    {
        $this->gameCondition = $gameCondition;

        return $this;
    }

    /**
     * Get gameCondition
     *
     * @return string
     */
    public function getGameCondition()
    {
        return $this->gameCondition;
    }

    /**
     * Set fieldType
     *
     * @param string $fieldType
     * @return Round
     * @deprecated
     */
    public function setFieldType($fieldType)
    {
//        trigger_error('Deprecated method, must not be called.');
        $this->fieldType = $fieldType;

        return $this;
    }

    /**
     * Get fieldType
     *@deprecated
     * @return string
     */
    public function getFieldType()
    {
//        trigger_error('Deprecated method, must not be called.');
        return $this->fieldType;
    }

    /**
     * Set fieldPar@deprecated
     *
     * @param string $fieldPar
     * @return Round
     */
    public function setFieldPar($fieldPar)
    {
//        trigger_error('Deprecated method, must not be called.');
        $this->fieldPar = $fieldPar;

        return $this;
    }

    /**
     * Get fieldPar
     *@deprecated
     * @return string
     */
    public function getFieldPar()
    {
//        trigger_error('Deprecated method, must not be called.');
        return $this->fieldPar;
    }

    /**
     * Set holeCount
     *@deprecated
     * @param integer $holeCount
     * @return Round
     */
    public function setHoleCount($holeCount)
    {
//        trigger_error('Deprecated method, must not be called.');
        $this->holeCount = $holeCount;

        return $this;
    }

    /**
     * Get holeCount
     *@deprecated
     * @return integer
     */
    public function getHoleCount()
    {
//        trigger_error('Deprecated method, must not be called.');
        return $this->holeCount;
    }

    /**
     * Set tour
     *
     * @param string $tour
     * @return Round
     */
    public function setTour($tour)
    {
        $this->tour = $tour;

        return $this;
    }

    /**
     * Get tour
     *
     * @return string
     */
    public function getTour()
    {
        return $this->tour;
    }

    /**
     * @return string
     */
    public function getPlayerConfirmed()
    {
        return $this->playerConfirmed;
    }

    /**
     * @param $playerConfirmed
     * @return $this
     */
    public function setPlayerConfirmed($playerConfirmed)
    {
        $this->playerConfirmed = $playerConfirmed;
        return $this;
    }

    /**
     * @return mixed
     */
    public function detailTournamentType()
    {
        $choices = TournamentType::getChoices();
        
        return $choices[$this->tournamentType];
    }

    /**
     * @return int
     * @deprecated
     */
    public function getNextHoleNumber()
    {
        $hole = $this->holes->last();

        return !$hole ? 1 : $hole->getNumber() + 1;
    }

    /**
     * @return int
     * @deprecated Solo se utiliza en una vista de frontend que ahora está bloqueada y se va a reemplazar
     */
    public function calculateNextHoleNumber()
    {
        $number = 1;
        foreach ($this->getHoles() as $key => $hole) {
            $iteration = $key + 1;
            if ($iteration % $hole->getNumber() > 0) {
                $number = $hole->getNumber()-1;
                break;
            }

            $number = $hole->getNumber() + 1;
        }

        return $number;
    }

    /**
     *
     * @param ExecutionContext $context
     */
    public function isValid(ExecutionContext $context)
    {
        if ($this->playerCondition === PlayerCondition::AMATEUR && $this->tour !== null) {
            $context->buildViolation('Invalid round: player condition is amateur so tour must be null.')
                ->atPath('tour')->addViolation();
        }

        if ($this->playerCondition === PlayerCondition::PROFESSIONAL && $this->handicap !== null) {
            $context->buildViolation('Invalid round: player condition is professional so handicap must be null.')
                ->atPath('handicap')->addViolation();
        }

        if ($this->roundType === RoundType::PRACTICE && $this->tournamentType !== null) {
            $context->buildViolation('Invalid round: round type is practice so tournament type be null.')
                ->atPath('tournamentType')->addViolation();
        }

        if ($this->roundType === RoundType::PRACTICE && $this->getRoundNumber() !== null) {
            $context->buildViolation('Invalid round: round type is practice so round number be null.')
                ->atPath('roundNumber')->addViolation();
        }
    }


    /**
     * Add hole
     *
     * @param Hole $hole
     *
     * @return Round
     */
    public function addHole(Hole $hole)
    {
        $hole->setRound($this);
        $this->holes[] = $hole;

        return $this;
    }

    /**
     * Remove hole
     *
     * @param Hole $hole
     */
    public function removeHole(Hole $hole)
    {
        $this->holes->removeElement($hole);
    }

    /**
     * Get holes
     *
     * @return Collection
     */
    public function getHoles()
    {
        $iterator = $this->holes->getIterator();
        $iterator->uasort(function (Hole $a, Hole $b) {
            return ($a->getNumber() < $b->getNumber()) ? -1 : 1;
        });

        return $iterator;
    }

    /**
     * @return array
     */
    public function getRemainingHoles()
    {
        $remaining = [];
        $number = 1;
        // key seria el numero de hoyo que deberia de ser
        // si el numero del hoyo es mayor que key es porque en el medio faltan cargar hoyos

        foreach ($this->getHoles() as $hole) {
            if ($hole->getNumber() > $number) {
                $quantity = $hole->getNumber() - $number;
                do {
                    $remaining[] = $number;
                    $quantity--;
                    $number++;
                } while ($quantity > 0);
            }
            $number++;
        }

        return array_reverse($remaining);
    }

    /**
     * @return int
     */
    public function getStrikesCount()
    {
        $strikes = 0;
        /** @var Hole $hole */
        foreach ($this->holes as $hole) {
            $strikes += $hole->getTotalStrikes();
        }

        return $strikes;
    }

    /**
     * @return PreCompetitiveQuestionnaire
     */
    public function getPreCompetitiveQuestionnaire()
    {
        return $this->preCompetitiveQuestionnaire ?: $this->preCompetitiveQuestionnaire = new PreCompetitiveQuestionnaire($this);
    }

    /**
     * @param PreCompetitiveQuestionnaire $preCompetitiveQuestionnaire
     * @return Round
     */
    public function setPreCompetitiveQuestionnaire(PreCompetitiveQuestionnaire $preCompetitiveQuestionnaire)
    {
        $this->preCompetitiveQuestionnaire = $preCompetitiveQuestionnaire;

        return $this;
    }

    /**
     * @return PostCompetitiveQuestionnaire
     */
    public function getPostCompetitiveQuestionnaire()
    {
        return $this->postCompetitiveQuestionnaire ?: $this->postCompetitiveQuestionnaire = new PostCompetitiveQuestionnaire($this);
    }

    /**
     * @param PostCompetitiveQuestionnaire $postCompetitiveQuestionnaire
     * @return Round
     */
    public function setPostCompetitiveQuestionnaire(PostCompetitiveQuestionnaire $postCompetitiveQuestionnaire)
    {
        $this->postCompetitiveQuestionnaire = $postCompetitiveQuestionnaire;

        return $this;
    }

    /**
     * @return Field
     */
    public function getField()
    {
        return $this->field;
    }

    /**
     * @param Field $field
     */
    public function setField($field)
    {
        $field->setRound($this);
        $this->field = $field;
    }

    /**
     * @return bool
     */
    public function isCompleted()
    {
        return $this->getHoles()->count() === $this->getPlayedHoles() && $this->getPreCompetitiveQuestionnaire() && $this->getPostCompetitiveQuestionnaire();
    }

    /**
     * @return int
     */
    public function getPlayedHoles()
    {
        return $this->playedHoles;
    }

    /**
     * @param int $playedHoles
     */
    public function setPlayedHoles($playedHoles)
    {
        $this->playedHoles = $playedHoles;
    }

    /**
     * @return Hole
     */
    public function createNewHole()
    {
        $hole = new Hole();
        $this->addHole($hole);

        return $hole;
    }

}
