<?php

namespace AndresGotta\Bundle\GolfBundle\Entity;

use AndresGotta\Bundle\GolfBundle\Repository\TourRepository;
use DateTime;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;
use Gedmo\Mapping\Annotation as Gedmo;
use Symfony\Bridge\Doctrine\Validator\Constraints\UniqueEntity;
use Symfony\Component\Validator\Constraints as Assert;

/**
 * Tour
 *
 * @ORM\Table(name="tours")
 * @ORM\Entity()
 * @UniqueEntity("name")
 */
class Tour
{

    /**
     * @var integer
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @var string
     *
     * @ORM\Column(name="name", type="string", length=255)
     * @Assert\NotBlank
     */
    private $name;

    /**
     *
     * @var ArrayCollection
     * @ORM\OneToMany(targetEntity="Round", mappedBy="tour", cascade={"persist"})
     * @Assert\Valid
     */
    protected $rounds;

    /**
     * @var DateTime
     *
     * @ORM\Column(name="created_at", type="datetime")
     * @Gedmo\Timestampable(on="create")
     */
    protected $createdAt;

    /**
     * Constructor
     */
    public function __construct($name = null)
    {
        $this->name = $name;
        $this->rounds = new ArrayCollection();
        $this->createdAt = new DateTime();
    }

    /**
     * Get id
     *
     * @return integer 
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set name
     *
     * @param string $name
     * @return Tour
     */
    public function setName($name)
    {
        $this->name = $name;

        return $this;
    }

    /**
     * Get name
     *
     * @return string 
     */
    public function getName()
    {
        return $this->name;
    }

    /**
     * Set createdAt
     *
     * @param DateTime $createdAt
     * @return Tour
     */
    public function setCreatedAt($createdAt)
    {
        $this->createdAt = $createdAt;

        return $this;
    }

    /**
     * Get createdAt
     *
     * @return DateTime 
     */
    public function getCreatedAt()
    {
        return $this->createdAt;
    }

    /**
     * Add rounds
     *
     * @param Round $rounds
     * @return Tour
     */
    public function addRound(Round $rounds)
    {
        $this->rounds[] = $rounds;

        return $this;
    }

    /**
     * Remove rounds
     *
     * @param Round $rounds
     */
    public function removeRound(Round $rounds)
    {
        $this->rounds->removeElement($rounds);
    }

    /**
     * Get rounds
     *
     * @return Collection 
     */
    public function getRounds()
    {
        return $this->rounds;
    }

    /**
     * @return string
     */
    public function __toString()
    {
        return $this->name;
    }

}
