<?php

namespace AndresGotta\Bundle\GolfBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use APY\DataGridBundle\Grid\Mapping as Grid;

/**
 * Class Comparative
 * @package AndresGotta\Bundle\GolfBundle\Entity
 *
 * @ORM\Entity(repositoryClass="AndresGotta\Bundle\GolfBundle\Repository\ComparativeRepository")
 * @ORM\Table(name="comparatives")
 * @Grid\Source(columns="id, highlighted, sort, name, report.name, description, maxLength", groupBy={"id"})
 */
class Comparative //implements ServiceNeededInterface
{
    /**
     * @var integer
     *
     * @ORM\Column(type="integer")
     * @ORM\Id()
     * @ORM\GeneratedValue(strategy="AUTO")
     * @Grid\Column(visible=false, filterable=false)
     */
    protected $id;

    /**
     * @var boolean
     *
     * @ORM\Column(name="highlighted", type="boolean")
     * @Grid\Column(title="Highlighted")
     */
    protected $highlighted = false;

    /**
     * @var string
     *
     * @ORM\Column(name="slug", type="string", length=255)
     */
    protected $slug;

    /**
     * @var string
     *
     * @ORM\Column(name="name", type="string", length=255)
     * @Grid\Column(title="Name")
     */
    protected $name;

    /**
     * @var string
     *
     * @ORM\Column(name="description", type="text", nullable=true)
     * @Grid\Column(title="Description")
     */
    protected $description;

    /**
     * @var string
     *
     * @ORM\Column(name="observation", type="text", nullable=true)
     */
    protected $observation;

    /**
     * @var string
     *
     * @ORM\Column(name="type", type="string")
     */
    protected $type;

    /**
     * @var integer
     *
     * @ORM\Column(name="max_length", type="integer", nullable=true)
     * @Grid\Column(title="Max length")
     */
    protected $maxLength;

    /**
     * @var integer
     *
     * @ORM\Column(name="sort", type="integer", nullable=true)
     * @Grid\Column(title="Sort")
     */
    protected $sort;

//    /**
//     * @ORM\ManyToMany(targetEntity="AndresGotta\Bundle\ServiceBundle\Entity\Service")
//     * @ORM\JoinTable(name="comparative_services",
//     *      joinColumns={@ORM\JoinColumn(name="comparative_id", referencedColumnName="id")},
//     *      inverseJoinColumns={@ORM\JoinColumn(name="service_id", referencedColumnName="id")}
//     *      )
//     */
//    protected $servicesNeeded;

    /**
     * @var string
     *
     * @ORM\Column(name="primary_label", type="string", length=255)
     */
    protected $primaryLabel;

    /**
     * @var string
     *
     * @ORM\Column(name="secondary_label", type="string", length=255)
     */
    protected $secondaryLabel;

    /**
     * Comparative constructor.
     */
    public function __construct()
    {
//        $this->servicesNeeded = new ArrayCollection();
    }

    /**
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

//    /**
//     * @return ArrayCollection
//     */
//    public function getServicesNeeded()
//    {
//        return $this->servicesNeeded;
//    }
//
//    /**
//     * Add servicesNeeded
//     *
//     * @param Service $servicesNeeded
//     *
//     * @return Report
//     */
//    public function addServicesNeeded(Service $servicesNeeded)
//    {
//        $this->servicesNeeded[] = $servicesNeeded;
//
//        return $this;
//    }
//
//    /**
//     * Remove servicesNeeded
//     *
//     * @param Service $servicesNeeded
//     */
//    public function removeServicesNeeded(Service $servicesNeeded)
//    {
//        $this->servicesNeeded->removeElement($servicesNeeded);
//    }
//
//    /**
//     *
//     */
//    public function removeAllServicesNeeded()
//    {
//        foreach ($this->servicesNeeded as $service) {
//            $this->removeServicesNeeded($service);
//        }
//    }

    /**
     * @return string
     */
    public function getSlug()
    {
        return $this->slug;
    }

    /**
     * @param string $slug
     * @return Comparative
     */
    public function setSlug($slug)
    {
        $this->slug = $slug;

        return $this;
    }

    /**
     * @return mixed
     */
    public function getDescription()
    {
        return $this->description;
    }

    /**
     * @param mixed $description
     */
    public function setDescription($description)
    {
        $this->description = $description;
    }

    /**
     * @return mixed
     */
    public function getObservation()
    {
        return $this->observation;
    }

    /**
     * @param mixed $observation
     */
    public function setObservation($observation)
    {
        $this->observation = $observation;
    }

    /**
     * @return boolean
     */
    public function isHighlighted()
    {
        return $this->highlighted;
    }

    /**
     * @param boolean $highlighted
     */
    public function setHighlighted($highlighted)
    {
        $this->highlighted = $highlighted;
    }

    /**
     * @return string
     */
    public function getName()
    {
        return $this->name;
    }

    /**
     * @param string $name
     */
    public function setName($name)
    {
        $this->name = $name;
    }

    /**
     * @return string
     */
    public function getType()
    {
        return $this->type;
    }

    /**
     * @param string $type
     */
    public function setType($type)
    {
        $this->type = $type;
    }

    /**
     * @return int
     */
    public function getMaxLength()
    {
        return $this->maxLength;
    }

    /**
     * @param int $maxLength
     */
    public function setMaxLength($maxLength)
    {
        $this->maxLength = $maxLength;
    }

    /**
     * @return int
     */
    public function getSort()
    {
        return $this->sort;
    }

    /**
     * @param int $sort
     */
    public function setSort($sort)
    {
        $this->sort = $sort;
    }

    /**
     * @return string
     */
    public function getPrimaryLabel()
    {
        return $this->primaryLabel;
    }

    /**
     * @param string $primaryLabel
     */
    public function setPrimaryLabel($primaryLabel)
    {
        $this->primaryLabel = $primaryLabel;
    }

    /**
     * @return string
     */
    public function getSecondaryLabel()
    {
        return $this->secondaryLabel;
    }

    /**
     * @param string $secondaryLabel
     */
    public function setSecondaryLabel($secondaryLabel)
    {
        $this->secondaryLabel = $secondaryLabel;
    }

}