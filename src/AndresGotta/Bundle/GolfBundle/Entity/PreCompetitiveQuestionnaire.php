<?php

namespace AndresGotta\Bundle\GolfBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Table(name="pre_competitive_questionnaires")
 * @ORM\Entity
 */

class PreCompetitiveQuestionnaire
{
    /**
     * @var integer
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @var Round
     *
     * @ORM\OneToOne(targetEntity="Round", inversedBy="preCompetitiveQuestionnaire")
     * @ORM\JoinColumn(name="round_id", referencedColumnName="id")
     */
    private $round;

    /**
     * @var boolean
     *
     * @ORM\Column(name="is_warmed_up", type="boolean")
     */
    private $isWarmedUp;

    /**
     * @var boolean
     *
     * @ORM\Column(name="has_practiced", type="boolean")
     */
    private $hasPracticed;

    /**
     * @var boolean
     *
     * @ORM\Column(name="wants_to_play", type="boolean")
     */
    private $wantsToPlay;

    /**
     * @var boolean
     *
     * @ORM\Column(name="has_strategy", type="boolean")
     */
    private $hasStrategy;

    /**
     * PreCompetitiveQuestionnaire constructor.
     * @param Round $round
     */
    public function __construct(Round $round = null)
    {
        $this->round = $round;
        $this->isWarmedUp = false;
        $this->hasPracticed = false;
        $this->wantsToPlay = false;
        $this->hasStrategy = false;
    }


    /**
     * Get id
     *
     * @return integer
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set isWarmedUp
     *
     * @param boolean $isWarmedUp
     *
     * @return PreCompetitiveQuestionnaire
     */
    public function setIsWarmedUp($isWarmedUp)
    {
        $this->isWarmedUp = (boolean)$isWarmedUp;

        return $this;
    }

    /**
     * Get isWarmedUp
     *
     * @return boolean
     */
    public function getIsWarmedUp()
    {
        return $this->isWarmedUp;
    }

    /**
     * Set hasPracticed
     *
     * @param boolean $hasPracticed
     *
     * @return PreCompetitiveQuestionnaire
     */
    public function setHasPracticed($hasPracticed)
    {
        $this->hasPracticed = (boolean)$hasPracticed;

        return $this;
    }

    /**
     * Get hasPracticed
     *
     * @return boolean
     */
    public function getHasPracticed()
    {
        return $this->hasPracticed;
    }

    /**
     * Set wantsToPlay
     *
     * @param boolean $wantsToPlay
     *
     * @return PreCompetitiveQuestionnaire
     */
    public function setWantsToPlay($wantsToPlay)
    {
        $this->wantsToPlay = (boolean)$wantsToPlay;

        return $this;
    }

    /**
     * Get wantsToPlay
     *
     * @return boolean
     */
    public function getWantsToPlay()
    {
        return $this->wantsToPlay;
    }

    /**
     * Set hasStrategy
     *
     * @param boolean $hasStrategy
     *
     * @return PreCompetitiveQuestionnaire
     */
    public function setHasStrategy($hasStrategy)
    {
        $this->hasStrategy = (boolean)$hasStrategy;

        return $this;
    }

    /**
     * Get hasStrategy
     *
     * @return boolean
     */
    public function getHasStrategy()
    {
        return $this->hasStrategy;
    }

    /**
     * Set round
     *
     * @param Round $round
     *
     * @return PreCompetitiveQuestionnaire
     */
    public function setRound(Round $round = null)
    {
        $this->round = $round;

        return $this;
    }

    /**
     * Get round
     *
     * @return Round
     */
    public function getRound()
    {
        return $this->round;
    }
}
