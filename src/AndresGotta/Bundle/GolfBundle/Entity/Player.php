<?php

namespace AndresGotta\Bundle\GolfBundle\Entity;

use AndresGotta\Bundle\GolfBundle\ValueObject\Club;
use AndresGotta\Bundle\GolfBundle\ValueObject\Email;
use AndresGotta\Bundle\GolfBundle\ValueObject\FullName;
use AndresGotta\Bundle\GolfBundle\ValueObject\Handicap;
use AndresGotta\Bundle\GolfBundle\ValueObject\PlayerCondition;
use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity(repositoryClass="AndresGotta\Bundle\GolfBundle\Repository\PlayerRepository")
 * @ORM\Table(name="gpstats_players")
 */
class Player
{
    /**
     * @var string
     *
     * @ORM\Id()
     * @ORM\GeneratedValue(strategy="UUID")
     * @ORM\Column(type="guid")
     */
    private $id;

    /**
     * @var Email
     * @ORM\Embedded(class="AndresGotta\Bundle\GolfBundle\ValueObject\Email")
     */
    private $email;

    /**
     * @var FullName
     * @ORM\Embedded(class="AndresGotta\Bundle\GolfBundle\ValueObject\FullName")
     */
    private $name;

    /**
     * @var PlayerCondition
     * @ORM\Embedded(class="AndresGotta\Bundle\GolfBundle\ValueObject\PlayerCondition")
     */
    private $condition;

    /**
     * @var Handicap
     * @ORM\Embedded(class="AndresGotta\Bundle\GolfBundle\ValueObject\Handicap")
     */
    private $handicap;

    /**
     * @var Tour
     * @ORM\ManyToOne(targetEntity="AndresGotta\Bundle\GolfBundle\Entity\Tour")
     */
    private $tour;

    /**
     * @var string
     * @ORM\Column(type="string", length=255, nullable=true)
     */
    private $country;

    /**
     * Player constructor.
     *
     * @param Email $email
     * @param FullName $name
     * @param PlayerCondition $condition
     * @param Handicap $handicap
     * @param Tour $tour
     */
    public function __construct(Email $email, FullName $name, PlayerCondition $condition, Handicap $handicap, Tour $tour = null)
    {
        $this->email = $email;
        $this->name = $name;
        $this->condition = $condition;
        $this->handicap = $handicap;
        $this->tour = $tour;
    }

    /**
     * @return string
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @return Email
     */
    public function getEmail()
    {
        return $this->email;
    }

    /**
     * @return FullName
     */
    public function getName()
    {
        return $this->name;
    }

    /**
     * @return PlayerCondition
     */
    public function getCondition()
    {
        return $this->condition;
    }

    /**
     * @param PlayerCondition $condition
     */
    public function setCondition($condition)
    {
        $this->condition = $condition;
    }

    /**
     * @return Handicap
     */
    public function getHandicap()
    {
        return $this->handicap;
    }

    /**
     * @param Handicap $handicap
     */
    public function setHandicap($handicap)
    {
        $this->handicap = $handicap;
    }

    /**
     * @return Tour
     */
    public function getTour()
    {
        return $this->tour;
    }

    /**
     * @param Tour $tour
     */
    public function setTour($tour)
    {
        $this->tour = $tour;
    }

    /**
     * @return string
     */
    public function getCountry()
    {
        return $this->country;
    }

    /**
     * @param string $country
     */
    public function setCountry($country)
    {
        $this->country = $country;
    }

}