<?php

namespace AndresGotta\Bundle\GolfBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity
 * @ORM\Table(name="physical_questionnaires")
 */

class PhysicalQuestionnaire
{
    /**
     * @var integer
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @var Strike
     *
     * @ORM\OneToOne(targetEntity="Strike", mappedBy="physicalQuestionnaire")
     */
    private $strike;

    /**
     * @var boolean
     *
     * @ORM\Column(name="feels_energetic", type="boolean", nullable=true)
     */
    private $feelsEnergetic;

    /**
     * @var boolean
     *
     * @ORM\Column(name="feels_getting_tired", type="boolean", nullable=true)
     */
    private $feelsGettingTired;

    /**
     * @var boolean
     *
     * @ORM\Column(name="feels_painful", type="boolean", nullable=true)
     */
    private $feelsPainful;

    /**
     * @var boolean
     *
     * @ORM\Column(name="feels_tired", type="boolean", nullable=true)
     */
    private $feelsTired;

    /**
     * @var boolean
     *
     * @ORM\Column(name="feels_exhausted", type="boolean", nullable=true)
     */
    private $feelsExhausted;

    /**
     * Get id
     *
     * @return integer
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set feelsEnergetic
     *
     * @param boolean $feelsEnergetic
     *
     * @return PhysicalQuestionnaire
     */
    public function setFeelsEnergetic($feelsEnergetic)
    {
        $this->feelsEnergetic = $feelsEnergetic;

        return $this;
    }

    /**
     * Get feelsEnergetic
     *
     * @return boolean
     */
    public function getFeelsEnergetic()
    {
        return $this->feelsEnergetic;
    }

    /**
     * Set feelsGettingTired
     *
     * @param boolean $feelsGettingTired
     *
     * @return PhysicalQuestionnaire
     */
    public function setFeelsGettingTired($feelsGettingTired)
    {
        $this->feelsGettingTired = $feelsGettingTired;

        return $this;
    }

    /**
     * Get feelsGettingTired
     *
     * @return boolean
     */
    public function getFeelsGettingTired()
    {
        return $this->feelsGettingTired;
    }

    /**
     * Set feelsPainful
     *
     * @param boolean $feelsPainful
     *
     * @return PhysicalQuestionnaire
     */
    public function setFeelsPainful($feelsPainful)
    {
        $this->feelsPainful = $feelsPainful;

        return $this;
    }

    /**
     * Get feelsPainful
     *
     * @return boolean
     */
    public function getFeelsPainful()
    {
        return $this->feelsPainful;
    }

    /**
     * Set feelsTired
     *
     * @param boolean $feelsTired
     *
     * @return PhysicalQuestionnaire
     */
    public function setFeelsTired($feelsTired)
    {
        $this->feelsTired = $feelsTired;

        return $this;
    }

    /**
     * Get feelsTired
     *
     * @return boolean
     */
    public function getFeelsTired()
    {
        return $this->feelsTired;
    }

    /**
     * Set feelsExhausted
     *
     * @param boolean $feelsExhausted
     *
     * @return PhysicalQuestionnaire
     */
    public function setFeelsExhausted($feelsExhausted)
    {
        $this->feelsExhausted = $feelsExhausted;

        return $this;
    }

    /**
     * Get feelsExhausted
     *
     * @return boolean
     */
    public function getFeelsExhausted()
    {
        return $this->feelsExhausted;
    }

    /**
     * Set strike
     *
     * @param Strike $strike
     *
     * @return PhysicalQuestionnaire
     */
    public function setStrike(Strike $strike = null)
    {
        $this->strike = $strike;

        return $this;
    }

    /**
     * Get strike
     *
     * @return Strike
     */
    public function getStrike()
    {
        return $this->strike;
    }
}
