<?php

namespace AndresGotta\Bundle\GolfBundle\Entity;

use AndresGotta\Bundle\GolfBundle\ValueObject\FeelingStatus;
use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Table(name="post_competitive_questionnaires")
 * @ORM\Entity
 */

class PostCompetitiveQuestionnaire
{
    /**
     * @var integer
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @var Round
     *
     * @ORM\OneToOne(targetEntity="Round", inversedBy="postCompetitiveQuestionnaire")
     * @ORM\JoinColumn(name="round_id", referencedColumnName="id")
     */
    private $round;

    /**
     * @var boolean
     *
     * @ORM\Column(name="feeling_status", type="string")
     */
    private $feelingStatus;

    /**
     * PostCompetitiveQuestionnaire constructor.
     * @param Round $round
     */
    public function __construct(Round $round = null)
    {
        $this->round = $round;
        $this->feelingStatus = FeelingStatus::UNKNOWN;
    }


    /**
     * Get id
     *
     * @return integer
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set feelingStatus
     *
     * @param string $feelingStatus
     *
     * @return PostCompetitiveQuestionnaire
     */
    public function setFeelingStatus($feelingStatus)
    {
        $this->feelingStatus = $feelingStatus;

        return $this;
    }

    /**
     * Get feelingStatus
     *
     * @return string
     */
    public function getFeelingStatus()
    {
        return $this->feelingStatus;
    }

    /**
     * Set round
     *
     * @param Round $round
     *
     * @return PostCompetitiveQuestionnaire
     */
    public function setRound(Round $round = null)
    {
        $this->round = $round;

        return $this;
    }

    /**
     * Get round
     *
     * @return Round
     */
    public function getRound()
    {
        return $this->round;
    }
}
