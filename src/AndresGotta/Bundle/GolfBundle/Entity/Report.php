<?php

namespace AndresGotta\Bundle\GolfBundle\Entity;

use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Validator\Constraints as Assert;
use APY\DataGridBundle\Grid\Mapping as Grid;

/**
 * @ORM\Table(name="reports")
 * @ORM\Entity(repositoryClass="AndresGotta\Bundle\GolfBundle\Repository\ReportRepository")
 * @Grid\Source(columns="id, reportCode, objectives, par", groupBy={"id"})
 */
class Report
{
    /**
     * @var integer
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     * @Grid\Column(visible=false, filterable=false)
     */
    private $id;

    /**
     * @var string
     *
     * @ORM\Column(name="report_code", type="string", length=12, unique=true, nullable=false)
     * @Assert\NotBlank()
     * @Grid\Column(title="Report Code")
     */
    private $reportCode;

    /**
     * @var string
     *
     * @ORM\Column(name="objectives", type="text")
     * @Grid\Column(title="Objectives")
     */
    private $objectives;

    /**
     * @var string
     *
     * @ORM\Column(name="slug", type="string", length=255)
     * @Assert\NotBlank
     */
    private $slug;

    /**
     * @var integer
     *
     * @ORM\Column(name="par", type="integer", nullable=true)
     * @Assert\Choice(choices = {"3", "4", "5"})
     * @Grid\Column(title="Par")
     */
    private $par;

    /**
     * @var ArrayCollection
     *
     * @ORM\ManyToMany(targetEntity="AndresGotta\Bundle\GolfBundle\Entity\GroupReport", inversedBy="reports", cascade={"persist"})
     * @ORM\JoinTable(name="reports_groups")
     */
    private $groups;

    /**
     * Report constructor.
     */
    public function __construct()
    {
        $this->groups = new ArrayCollection();
    }

    /**
     * Get id
     *
     * @return integer
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * 
     * @return integer
     */
    public function getPar()
    {
        return $this->par;
    }

    /**
     * 
     * @param integer $par
     * @return Category
     */
    public function setPar($par)
    {
        $this->par = $par;
        
        return $this;
    }

    /**
     * 
     * @param string $slug
     * @return Category
     */
    public function setSlug($slug)
    {
        $this->slug = $slug;
        
        return $this;
    }
    
    /**
     * 
     * @return string
     */
    public function getSlug()
    {
        return $this->slug;
    }

    /**
     * @return string
     */
    public function getReportCode()
    {
        return $this->reportCode;
    }

    /**
     * @param $reportCode
     * @return Report
     */
    public function setReportCode($reportCode)
    {
        $this->reportCode = $reportCode;

        return $this;
    }

    /**
     * Set objectives
     *
     * @param string $objectives
     *
     * @return GroupReport
     */
    public function setObjectives($objectives)
    {
        $this->objectives = $objectives;

        return $this;
    }

    /**
     * Get objectives
     *
     * @return string
     */
    public function getObjectives()
    {
        return $this->objectives;
    }

    /**
     * Add group
     *
     * @param GroupReport $group
     *
     * @return Report
     */
    public function addGroup(GroupReport $group)
    {
        $this->groups[] = $group;

        return $this;
    }

    /**
     * Remove group
     *
     * @param GroupReport $group
     */
    public function removeGroup(GroupReport $group)
    {
        $this->groups->removeElement($group);
    }

    /**
     * Get groups
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getGroups()
    {
        return $this->groups;
    }

    /**
     * @return string
     */
    public function getPublicName()
    {
        return $this->par ? 'Par ' . $this->par : 'Sin par especifico';
    }

    /**
     * @return string
     * @deprecated
     */
    public function getName()
    {
        return '';
    }

    /**
     * @return string
     */
    public function __toString()
    {
        return sprintf('(%s) %s', $this->reportCode, $this->slug);
    }


}
