<?php

namespace AndresGotta\Bundle\GolfBundle\Entity;


use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Validator\Constraints as Assert;

/**
 * @ORM\MappedSuperclass()
 */
abstract class AbstractField
{

    /**
     * @var string
     * @ORM\Column(type="string")
     * @Assert\NotBlank(groups={"Default", "OnlyRound"})
     * @Assert\Length(min="3", max="255", groups={"Default", "OnlyRound"})
     */
    protected $sportClubName;
    /**
     * @var string
     * @ORM\Column(type="string")
     * @Assert\NotBlank(groups={"Default", "OnlyRound"})
     * @Assert\Choice(callback={"AndresGotta\Bundle\GolfBundle\ValueObject\FieldPar", "getChoices"}, groups={"Default", "OnlyRound"})
     */
    protected $par;
    /**
     * @var string
     * @ORM\Column(type="string")
     * @Assert\NotBlank(groups={"Default", "OnlyRound"})
     * @Assert\Choice(callback={"AndresGotta\Bundle\GolfBundle\ValueObject\Country", "getChoices"}, groups={"Default", "OnlyRound"})
     */
    protected $country;
    /**
     * @var string
     * @ORM\Column(type="integer")
     * @Assert\NotBlank(groups={"Default", "OnlyRound"})
     * @Assert\Choice(choices={"9", "18"}, groups={"Default", "OnlyRound"})
     */
    protected $holeCount;
    /**
     * @var string
     * @ORM\Column(type="string")
     * @Assert\NotBlank(groups={"Default", "OnlyRound"})
     */
    protected $route;
    /**
     * @var string
     * @ORM\Column(type="string")
     * @Assert\NotBlank()
     * @Assert\Choice(callback={"AndresGotta\Bundle\GolfBundle\ValueObject\FieldType", "getChoices"}, groups={"Default", "OnlyRound"})
     */
    protected $topographicMovements;
    /**
     * @var string
     * @ORM\Column(type="string")
     * @Assert\NotBlank(groups={"Default", "OnlyRound"})
     */
    protected $startPoint;
    /**
     * @var string
     * @ORM\Column(type="string")
     * @Assert\NotBlank(groups={"Default", "OnlyRound"})
     * @Assert\Choice(choices={"field.type.standard", "field.type.par3"}, groups={"Default", "OnlyRound"})
     */
    protected $type;

    /**
     * @return string
     */
    public function getSportClubName()
    {
        return $this->sportClubName;
    }

    /**
     * @param string $sportClubName
     */
    public function setSportClubName($sportClubName)
    {
        $this->sportClubName = $sportClubName;
    }

    /**
     * @return string
     */
    public function getPar()
    {
        return $this->par;
    }

    /**
     * @param string $par
     */
    public function setPar($par)
    {
        $this->par = $par;
    }

    /**
     * @return string
     */
    public function getCountry()
    {
        return $this->country;
    }

    /**
     * @param string $country
     */
    public function setCountry($country)
    {
        $this->country = $country;
    }

    /**
     * @return string
     */
    public function getHoleCount()
    {
        return $this->holeCount;
    }

    /**
     * @param string $holeCount
     */
    public function setHoleCount($holeCount)
    {
        $this->holeCount = $holeCount;
    }

    /**
     * @return string
     */
    public function getRoute()
    {
        return $this->route;
    }

    /**
     * @param string $route
     */
    public function setRoute($route)
    {
        $this->route = $route;
    }

    /**
     * @return string
     */
    public function getTopographicMovements()
    {
        return $this->topographicMovements;
    }

    /**
     * @param string $topographicMovements
     */
    public function setTopographicMovements($topographicMovements)
    {
        $this->topographicMovements = $topographicMovements;
    }

    /**
     * @return string
     */
    public function getStartPoint()
    {
        return $this->startPoint;
    }

    /**
     * @param string $startPoint
     */
    public function setStartPoint($startPoint)
    {
        $this->startPoint = $startPoint;
    }

    /**
     * @return string
     */
    public function getType()
    {
        return $this->type;
    }

    /**
     * @param string $type
     */
    public function setType($type)
    {
        $this->type = $type;
    }

}