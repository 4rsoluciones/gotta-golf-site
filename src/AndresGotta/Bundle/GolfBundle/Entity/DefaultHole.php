<?php

namespace AndresGotta\Bundle\GolfBundle\Entity;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\ORM\Mapping as ORM;
use Symfony\Bridge\Doctrine\Validator\Constraints\UniqueEntity;
use Symfony\Component\Validator\Constraints as Assert;

/**
 * Class DefaultHole
 * @package AndresGotta\Bundle\GolfBundle\Entity
 * @ORM\Entity(repositoryClass="AndresGotta\Bundle\GolfBundle\Repository\DefaultHoleRepository")
 * @ORM\Table(name="default_holes")
 * @UniqueEntity(fields={"number", "defaultField"}, errorPath="number")
 */
class DefaultHole extends AbstractHole
{
    /**
     * @var integer
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @var DefaultField
     * @ORM\ManyToOne(targetEntity="AndresGotta\Bundle\GolfBundle\Entity\DefaultField", inversedBy="defaultHoles")
     * @Assert\NotNull(message="Default Field not found, please verify: country, route, sportClubName and startPoint combination value.")
     */
    private $defaultField;

    /**
     * @var Hole[]
     * @ORM\OneToMany(targetEntity="AndresGotta\Bundle\GolfBundle\Entity\Hole", mappedBy="defaultHole")
     */
    private $holes;

    /**
     * DefaultHole constructor.
     */
    public function __construct()
    {
        $this->holes = new ArrayCollection();
    }

    /**
     * @param Hole $hole
     * @return DefaultHole
     */
    public static function createFromHole(Hole $hole)
    {
        $defaultField = $hole->getRound()->getField()->getDefaultField();
        $defaultHole = new DefaultHole();
        $defaultHole->setNumber($hole->getNumber());
        $defaultHole->setPar($hole->getPar());
        $defaultHole->setDesign($hole->getDesign());
        $defaultHole->setDefaultField($defaultField);

        return $defaultHole;
    }

    /**
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @return DefaultField
     */
    public function getDefaultField()
    {
        return $this->defaultField;
    }

    /**
     * @param DefaultField $defaultField
     */
    public function setDefaultField($defaultField)
    {
        $defaultField->addDefaultHole($this);
        $this->defaultField = $defaultField;
    }

    /**
     * @return Hole[]
     */
    public function getHoles()
    {
        return $this->holes;
    }

    /**
     * @param Hole[] $holes
     */
    public function setHoles($holes)
    {
        $this->holes = $holes;
    }

    /**
     * @param Hole $hole
     */
    public function addHole(Hole $hole)
    {
        $this->holes[] = $hole;
    }

}