<?php

namespace AndresGotta\Bundle\GolfBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity
 * @ORM\Table(name="food_questionnaires")
 */

class FoodQuestionnaire
{
    /**
     * @var integer
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @var Strike
     *
     * @ORM\OneToOne(targetEntity="Strike", mappedBy="foodQuestionnaire")
     */
    private $strike;

    /**
     * @var boolean
     *
     * @ORM\Column(name="drank_water", type="boolean", nullable=true)
     */
    private $drankWater;

    /**
     * @var boolean
     *
     * @ORM\Column(name="drank_sports_drink", type="boolean", nullable=true)
     */
    private $drankSportsDrink;

    /**
     * @var boolean
     *
     * @ORM\Column(name="swallowed_cereal_or_nuts", type="boolean", nullable=true)
     */
    private $swallowedCerealOrNuts;

    /**
     * @var boolean
     *
     * @ORM\Column(name="swallowed_sandwich", type="boolean", nullable=true)
     */
    private $swallowedSandwich;

    /**
     * @var boolean
     *
     * @ORM\Column(name="swallowed_fruit", type="boolean", nullable=true)
     */
    private $swallowedFruit;

    /**
     * Get id
     *
     * @return integer
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set drankWater
     *
     * @param boolean $drankWater
     *
     * @return FoodQuestionnaire
     */
    public function setDrankWater($drankWater)
    {
        $this->drankWater = $drankWater;

        return $this;
    }

    /**
     * Get drankWater
     *
     * @return boolean
     */
    public function getDrankWater()
    {
        return $this->drankWater;
    }

    /**
     * Set drankSportsDrink
     *
     * @param boolean $drankSportsDrink
     *
     * @return FoodQuestionnaire
     */
    public function setDrankSportsDrink($drankSportsDrink)
    {
        $this->drankSportsDrink = $drankSportsDrink;

        return $this;
    }

    /**
     * Get drankSportsDrink
     *
     * @return boolean
     */
    public function getDrankSportsDrink()
    {
        return $this->drankSportsDrink;
    }

    /**
     * Set swallowedCerealOrNuts
     *
     * @param boolean $swallowedCerealOrNuts
     *
     * @return FoodQuestionnaire
     */
    public function setSwallowedCerealOrNuts($swallowedCerealOrNuts)
    {
        $this->swallowedCerealOrNuts = $swallowedCerealOrNuts;

        return $this;
    }

    /**
     * Get swallowedCerealOrNuts
     *
     * @return boolean
     */
    public function getSwallowedCerealOrNuts()
    {
        return $this->swallowedCerealOrNuts;
    }

    /**
     * Set swallowedSandwich
     *
     * @param boolean $swallowedSandwich
     *
     * @return FoodQuestionnaire
     */
    public function setSwallowedSandwich($swallowedSandwich)
    {
        $this->swallowedSandwich = $swallowedSandwich;

        return $this;
    }

    /**
     * Get swallowedSandwich
     *
     * @return boolean
     */
    public function getSwallowedSandwich()
    {
        return $this->swallowedSandwich;
    }

    /**
     * Set swallowedFruit
     *
     * @param boolean $swallowedFruit
     *
     * @return FoodQuestionnaire
     */
    public function setSwallowedFruit($swallowedFruit)
    {
        $this->swallowedFruit = $swallowedFruit;

        return $this;
    }

    /**
     * Get swallowedFruit
     *
     * @return boolean
     */
    public function getSwallowedFruit()
    {
        return $this->swallowedFruit;
    }

    /**
     * Set strike
     *
     * @param Strike $strike
     *
     * @return FoodQuestionnaire
     */
    public function setStrike(Strike $strike = null)
    {
        $this->strike = $strike;

        return $this;
    }

    /**
     * Get strike
     *
     * @return Strike
     */
    public function getStrike()
    {
        return $this->strike;
    }
}
