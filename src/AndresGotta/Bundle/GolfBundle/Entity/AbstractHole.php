<?php

namespace AndresGotta\Bundle\GolfBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Validator\Constraints as Assert;

/**
 * @ORM\MappedSuperclass()
 */
abstract class AbstractHole
{
    /**
     * @var integer
     *
     * @ORM\Column(name="number", type="integer")
     * @Assert\NotBlank()
     */
    protected $number;

    /**
     * @var integer
     *
     * @ORM\Column(name="par", type="integer")
     * @Assert\NotBlank()
     * @Assert\Choice(choices = {"3", "4", "5"})
     */
    protected $par;

    /**
     * @var string
     *
     * @ORM\Column(name="design", type="string", length=255)
     * @Assert\NotBlank()
     * @Assert\Choice(choices = {"design.dogleg-left", "design.dogleg-right", "design.straight"})
     */
    protected $design;

    /**
     * Set number
     *
     * @param integer $number
     */
    public function setNumber($number)
    {
        $this->number = $number;
    }

    /**
     * Get number
     *
     * @return integer
     */
    public function getNumber()
    {
        return $this->number;
    }

    /**
     * Set par
     *
     * @param integer $par
     */
    public function setPar($par)
    {
        $this->par = $par;
    }

    /**
     * Get par
     *
     * @return integer
     */
    public function getPar()
    {
        return $this->par;
    }

    /**
     * Set design
     *
     * @param string $design
     */
    public function setDesign($design)
    {
        $this->design = $design;
    }

    /**
     * Get design
     *
     * @return string
     */
    public function getDesign()
    {
        return $this->design;
    }
}