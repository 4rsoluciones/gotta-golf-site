<?php

namespace AndresGotta\Bundle\GolfBundle\Entity;

use AndresGotta\Bundle\GolfBundle\ValueObject\Result;
use AndresGotta\Bundle\GolfBundle\ValueObject\StrikeType;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;
use Symfony\Bridge\Doctrine\Validator\Constraints\UniqueEntity;
use Symfony\Component\Validator\Constraints as Assert;
use Symfony\Component\Validator\Context\ExecutionContextInterface;

/**
 * Hole
 *
 * @ORM\Table(name="holes")
 * @ORM\Entity(repositoryClass="AndresGotta\Bundle\GolfBundle\Repository\HoleRepository")
 * @Assert\Callback(methods={"isValidHole"})
 * @UniqueEntity(fields={"number", "round"}, errorPath="number")
 */
class Hole extends AbstractHole
{

    /**
     * @var integer
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @var DefaultHole
     * @ORM\ManyToOne(targetEntity="AndresGotta\Bundle\GolfBundle\Entity\DefaultHole", inversedBy="holes")
     */
    protected $defaultHole;

    /**
     *
     * @var ArrayCollection
     * @ORM\OneToMany(targetEntity="AndresGotta\Bundle\GolfBundle\Entity\Strike", mappedBy="hole", cascade={"all"}, orphanRemoval=true)
     * @Assert\Valid()
     * @Assert\Count(min = "1")
     */
    protected $strikes;

    /**
     * @var integer
     *
     * @ORM\Column(name="penality_shots_count", type="integer")
     * @Assert\NotBlank()
     * @Assert\Range(min = 0, max = 12)
     */
    private $penalityShotsCount;

    /**
     * @var string
     *
     * @ORM\Column(name="description", type="text", nullable=true)
     */
    private $description;

    /**
     *
     * @var Round
     * @ORM\ManyToOne(targetEntity="Round", inversedBy="holes", cascade={"persist"}, fetch="LAZY")
     * @ORM\JoinColumn(name="round_id", referencedColumnName="id")
     * @Assert\NotBlank()
     */
    private $round;


    /**
     * Hole constructor.
     */
    public function __construct()
    {
        $this->strikes = new ArrayCollection();
    }

    /**
     * Get id
     *
     * @return integer
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set penalityShotsCount
     *
     * @param integer $penalityShotsCount
     * @return Hole
     */
    public function setPenalityShotsCount($penalityShotsCount)
    {
        $this->penalityShotsCount = $penalityShotsCount;

        return $this;
    }

    /**
     * Get penalityShotsCount
     *
     * @return integer
     */
    public function getPenalityShotsCount()
    {
        return $this->penalityShotsCount;
    }

    /**
     * Set description
     *
     * @param string $description
     * @return Hole
     */
    public function setDescription($description)
    {
        $this->description = $description;

        return $this;
    }

    /**
     * Get description
     *
     * @return string
     */
    public function getDescription()
    {
        return $this->description;
    }

    /**
     * Set round
     *
     * @param Round $round
     * @return Hole
     */
    public function setRound(Round $round = null)
    {
        $this->round = $round;

        return $this;
    }

    /**
     * Get round
     *
     * @return Round
     */
    public function getRound()
    {
        return $this->round;
    }
    
    /**
     * Determina si el hoyo fue embocado
     *
     * @return boolean
     */
    public function isHoleOut()
    {
        return $this->strikes->last()->getResult() === Result::HOLE_OUT;
    }

    /**
     * @return int
     */
    public function getTotalStrikes()
    {
        return $this->strikes->count();
    }

    /**
     * @return int
     */
    public function getScore()
    {
        return $this->getTotalStrikes() - $this->penalityShotsCount;
    }

    /**
     * @return int
     */
    public function getTotalPutts()
    {
        return $this->strikes->filter(function (Strike $strike) {
            return $strike->getType() === StrikeType::TYPE_PUTT;
        })->count();
    }

    /**
     *
     */
    public function clearStrikes()
    {
        $this->strikes = new ArrayCollection();
    }

    /**
     * @return Strike[]|Collection
     */
    public function getStrikes()
    {
        return $this->strikes;
    }

    /**
     * @param Strike $strike
     */
    public function addStrike(Strike $strike)
    {
        $strike->setHole($this);
        $this->strikes->add($strike);
    }

    /**
     * @param Strike $strike
     */
    public function removeStrike(Strike $strike)
    {
        $strike->setHole(null);
        $this->strikes->removeElement($strike);
    }

    /**
     * @param $sort
     * @return mixed
     */
    public function getStrikeBySort($sort)
    {
        return $this->strikes[$sort-1];
    }

    /**
     * @return DefaultHole
     */
    public function getDefaultHole()
    {
        return $this->defaultHole;
    }

    /**
     * @param DefaultHole $defaultHole
     */
    public function setDefaultHole($defaultHole)
    {
        $this->defaultHole = $defaultHole;
    }

    /**
     * @param ExecutionContextInterface $context
     */
    public function isValidHole(ExecutionContextInterface $context)
    {
        if ($this->strikes->isEmpty()) {
            return;
        }

        if ($this->strikes->first()->getType() !== StrikeType::TYPE_FIRST) {
            $context->buildViolation('Invalid hole: First strike must be type {1}.', [
                '{1}' => StrikeType::TYPE_FIRST,
            ])->atPath('strikes')->addViolation();
            return;
        }

        $puttDetected = false;
        /* @var Strike $strike */
        foreach ($this->strikes->slice(1) as $strike) {
            if ($strike->getType() === StrikeType::TYPE_FIRST) {
                $context->buildViolation('Invalid hole: Second first strike detected.')
                    ->atPath('strikes')->addViolation();
                return;
            }
            if ($strike->getType() === StrikeType::TYPE_PUTT) {
                $puttDetected = true;
            }
            if ($strike->getType() === StrikeType::TYPE_OTHER && $puttDetected) {
                $context->buildViolation('Invalid hole: Other strike after putt strike detected.')
                    ->atPath('strikes')->addViolation();
                return;
            }
        }

        if ($this->strikes->last()->getResult() !== Result::HOLE_OUT) {
            $context->buildViolation('Invalid hole: Last strike must be hole out.')
                ->atPath('strikes')->addViolation();
            return;
        }

        if ($this->strikes->last()->getReason()) {
            $context->buildViolation('Invalid hole: Last strike has fail reason, must be empty.')
                ->atPath('strikes')->addViolation();
            return;
        }
    }

    /**
     * @param string $type
     * @return Strike[]|Collection
     */
    public function getStrikesByType($type)
    {
        return $this->strikes->filter(function (Strike $strike) use ($type) {
            return $strike->getType() === $type;
        });
    }
}
