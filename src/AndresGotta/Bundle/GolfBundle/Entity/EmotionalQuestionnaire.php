<?php

namespace AndresGotta\Bundle\GolfBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity
 * @ORM\Table(name="emotional_questionnaires")
 */

class EmotionalQuestionnaire
{
    /**
     * @var integer
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @var Strike
     *
     * @ORM\OneToOne(targetEntity="Strike", mappedBy="emotionalQuestionnaire")
     */
    private $strike;

    /**
     * @var boolean
     *
     * @ORM\Column(name="feels_uptight", type="boolean", nullable=true)
     */
    private $feelsUptight;

    /**
     * @var boolean
     *
     * @ORM\Column(name="feels_distrustful", type="boolean", nullable=true)
     */
    private $feelsDistrustful;

    /**
     * @var boolean
     *
     * @ORM\Column(name="feels_annoyed", type="boolean", nullable=true)
     */
    private $feelsAnnoyed;

    /**
     * @var boolean
     *
     * @ORM\Column(name="feels_frustrated", type="boolean", nullable=true)
     */
    private $feelsFrustrated;

    /**
     * @var boolean
     *
     * @ORM\Column(name="feels_trustful", type="boolean", nullable=true)
     */
    private $feelsTrustful;

    /**
     * @var boolean
     *
     * @ORM\Column(name="feels_enjoying", type="boolean", nullable=true)
     */
    private $feelsEnjoying;

    /**
     * @var boolean
     *
     * @ORM\Column(name="feels_focused", type="boolean", nullable=true)
     */
    private $feelsFocused;

    /**
     * Get id
     *
     * @return integer
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set feelsUptight
     *
     * @param boolean $feelsUptight
     *
     * @return EmotionalQuestionnaire
     */
    public function setFeelsUptight($feelsUptight)
    {
        $this->feelsUptight = $feelsUptight;

        return $this;
    }

    /**
     * Get feelsUptight
     *
     * @return boolean
     */
    public function getFeelsUptight()
    {
        return $this->feelsUptight;
    }

    /**
     * Set feelsDistrustful
     *
     * @param boolean $feelsDistrustful
     *
     * @return EmotionalQuestionnaire
     */
    public function setFeelsDistrustful($feelsDistrustful)
    {
        $this->feelsDistrustful = $feelsDistrustful;

        return $this;
    }

    /**
     * Get feelsDistrustful
     *
     * @return boolean
     */
    public function getFeelsDistrustful()
    {
        return $this->feelsDistrustful;
    }

    /**
     * Set feelsAnnoyed
     *
     * @param boolean $feelsAnnoyed
     *
     * @return EmotionalQuestionnaire
     */
    public function setFeelsAnnoyed($feelsAnnoyed)
    {
        $this->feelsAnnoyed = $feelsAnnoyed;

        return $this;
    }

    /**
     * Get feelsAnnoyed
     *
     * @return boolean
     */
    public function getFeelsAnnoyed()
    {
        return $this->feelsAnnoyed;
    }

    /**
     * Set feelsFrustrated
     *
     * @param boolean $feelsFrustrated
     *
     * @return EmotionalQuestionnaire
     */
    public function setFeelsFrustrated($feelsFrustrated)
    {
        $this->feelsFrustrated = $feelsFrustrated;

        return $this;
    }

    /**
     * Get feelsFrustrated
     *
     * @return boolean
     */
    public function getFeelsFrustrated()
    {
        return $this->feelsFrustrated;
    }

    /**
     * Set feelsTrustful
     *
     * @param boolean $feelsTrustful
     *
     * @return EmotionalQuestionnaire
     */
    public function setFeelsTrustful($feelsTrustful)
    {
        $this->feelsTrustful = $feelsTrustful;

        return $this;
    }

    /**
     * Get feelsTrustful
     *
     * @return boolean
     */
    public function getFeelsTrustful()
    {
        return $this->feelsTrustful;
    }

    /**
     * Set feelsEnjoying
     *
     * @param boolean $feelsEnjoying
     *
     * @return EmotionalQuestionnaire
     */
    public function setFeelsEnjoying($feelsEnjoying)
    {
        $this->feelsEnjoying = $feelsEnjoying;

        return $this;
    }

    /**
     * Get feelsEnjoying
     *
     * @return boolean
     */
    public function getFeelsEnjoying()
    {
        return $this->feelsEnjoying;
    }

    /**
     * Set feelsFocused
     *
     * @param boolean $feelsFocused
     *
     * @return EmotionalQuestionnaire
     */
    public function setFeelsFocused($feelsFocused)
    {
        $this->feelsFocused = $feelsFocused;

        return $this;
    }

    /**
     * Get feelsFocused
     *
     * @return boolean
     */
    public function getFeelsFocused()
    {
        return $this->feelsFocused;
    }

    /**
     * Set strike
     *
     * @param Strike $strike
     *
     * @return EmotionalQuestionnaire
     */
    public function setStrike(Strike $strike = null)
    {
        $this->strike = $strike;

        return $this;
    }

    /**
     * Get strike
     *
     * @return Strike
     */
    public function getStrike()
    {
        return $this->strike;
    }
}
