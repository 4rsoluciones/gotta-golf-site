<?php

namespace AndresGotta\Bundle\GolfBundle\Entity;

use AndresGotta\Bundle\GolfBundle\Model\DistanceSelector;
use AndresGotta\Bundle\GolfBundle\Model\ReasonSelector;
use AndresGotta\Bundle\GolfBundle\Model\ResultSelector;
use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Validator\Constraints as Assert;
use Symfony\Component\Validator\Context\ExecutionContextInterface;

/**
 * Strike
 *
 * @ORM\Table(name="strikes")
 * @ORM\Entity
 *
 * @Assert\Callback(methods={"isDistanceValid", "isResultValid", "isReasonValid"}
 * )
 */
class Strike
{
    /**
     * @var integer
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;
    /**
     * @var string
     *
     * @ORM\Column(name="distance", type="string", length=255)
     * @Assert\NotBlank()
     */
    private $distance;
    /**
     * @var string
     *
     * @ORM\Column(name="result", type="string", length=255)
     * @Assert\NotBlank()
     */
    private $result;
    /**
     * @var string
     * @ORM\Column(name="reason", type="string", length=255, nullable=true)
     */
    private $reason;
    /**
     * @var Hole
     * @ORM\ManyToOne(targetEntity="AndresGotta\Bundle\GolfBundle\Entity\Hole", inversedBy="strikes")
     * @Assert\NotNull()
     */
    private $hole;
    /**
     * @var string
     * @ORM\Column(name="ball_flight", type="string", length=255, nullable=true)
     */
    private $ballFlight;
    /**
     * @var string
     * @ORM\Column(name="club", type="string", length=255, nullable=true)
     */
    private $club;
    /**
     * @var string
     * @ORM\Column(name="strike_lies", type="simple_array", nullable=true)
     */
    private $strikeLies;
    /**
     * @var string
     * @ORM\Column(name="land_slopes", type="simple_array", nullable=true)
     */
    private $landSlopes;
    /**
     * @var string
     * @ORM\Column()
     * @Assert\NotBlank()
     */
    private $type;
    /**
     * @var EmotionalQuestionnaire
     * @ORM\OneToOne(targetEntity="EmotionalQuestionnaire", inversedBy="strike", cascade={"all"})
     * @ORM\JoinColumn(name="emotional_questionnaire_id", referencedColumnName="id")
     */
    private $emotionalQuestionnaire;
    /**
     * @var FoodQuestionnaire
     * @ORM\OneToOne(targetEntity="FoodQuestionnaire", inversedBy="strike", cascade={"all"})
     * @ORM\JoinColumn(name="food_questionnaire_id", referencedColumnName="id")
     */
    private $foodQuestionnaire;
    /**
     * @var PhysicalQuestionnaire
     * @ORM\OneToOne(targetEntity="PhysicalQuestionnaire", inversedBy="strike", cascade={"all"})
     * @ORM\JoinColumn(name="physical_questionnaire_id", referencedColumnName="id")
     */
    private $physicalQuestionnaire;

    /**
     * @var integer
     * @ORM\Column(type="integer")
     */
    private $sort;

    /**
     * Get id
     *
     * @return integer
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Get hole
     *
     * @return Hole
     */
    public function getHole()
    {
        return $this->hole;
    }

    /**
     * Set hole
     *
     * @param Hole $hole
     */
    public function setHole(Hole $hole = null)
    {
        $this->hole = $hole;
        $this->setSort($hole->getStrikes()->last() ? $hole->getStrikes()->last()->getSort() + 1 : 1);
    }

    /**
     * @return int
     */
    public function getSort()
    {
        return $this->sort;
    }

    /**
     * @param int $sort
     */
    public function setSort($sort)
    {
        $this->sort = $sort;
    }

    /**
     * Get reason
     *
     * @return string
     */
    public function getReason()
    {
        return $this->reason;
    }

    /**
     * Set reason
     *
     * @param string $reason
     */
    public function setReason($reason)
    {
        $this->reason = $reason;
    }

    /**
     * Get distance
     *
     * @return string
     */
    public function getDistance()
    {
        return $this->distance;
    }

    /**
     * Set distance
     *
     * @param string $distance
     */
    public function setDistance($distance)
    {
        $this->distance = $distance;
    }

    /**
     * Get result
     *
     * @return string
     */
    public function getResult()
    {
        return $this->result;
    }

    /**
     * Set result
     *
     * @param string $result
     * @return Strike
     */
    public function setResult($result)
    {
        $this->result = $result;

        return $this;
    }

    /**
     * Get emotionalQuestionnaire
     *
     * @return EmotionalQuestionnaire
     */
    public function getEmotionalQuestionnaire()
    {
        return $this->emotionalQuestionnaire;
    }

    /**
     * Set emotionalQuestionnaire
     *
     * @param EmotionalQuestionnaire $emotionalQuestionnaire
     */
    public function setEmotionalQuestionnaire(EmotionalQuestionnaire $emotionalQuestionnaire = null)
    {
        $emotionalQuestionnaire->setStrike($this);
        $this->emotionalQuestionnaire = $emotionalQuestionnaire;
    }

    /**
     * Get foodQuestionnaire
     *
     * @return FoodQuestionnaire
     */
    public function getFoodQuestionnaire()
    {
        return $this->foodQuestionnaire;
    }

    /**
     * Set foodQuestionnaire
     *
     * @param FoodQuestionnaire $foodQuestionnaire
     */
    public function setFoodQuestionnaire(FoodQuestionnaire $foodQuestionnaire = null)
    {
        $foodQuestionnaire->setStrike($this);
        $this->foodQuestionnaire = $foodQuestionnaire;
    }

    /**
     * Get physicalQuestionnaire
     *
     * @return PhysicalQuestionnaire
     */
    public function getPhysicalQuestionnaire()
    {
        return $this->physicalQuestionnaire;
    }

    /**
     * Set physicalQuestionnaire
     *
     * @param PhysicalQuestionnaire $physicalQuestionnaire
     */
    public function setPhysicalQuestionnaire(PhysicalQuestionnaire $physicalQuestionnaire = null)
    {
        $physicalQuestionnaire->setStrike($this);
        $this->physicalQuestionnaire = $physicalQuestionnaire;
    }

    /**
     * @return string
     */
    public function getBallFlight()
    {
        return $this->ballFlight;
    }

    /**
     * @param string $ballFlight
     */
    public function setBallFlight($ballFlight)
    {
        $this->ballFlight = $ballFlight;
    }

    /**
     * @return string
     */
    public function getClub()
    {
        return $this->club;
    }

    /**
     * @param string $club
     */
    public function setClub($club)
    {
        $this->club = $club;
    }

    /**
     * @return string
     */
    public function getStrikeLies()
    {
        return $this->strikeLies;
    }

    /**
     * @param string[] $strikeLies
     */
    public function setStrikeLies($strikeLies)
    {
        $this->strikeLies = $strikeLies;
    }

    /**
     * @return string
     */
    public function getLandSlopes()
    {
        return $this->landSlopes;
    }

    /**
     * @param string[] $landSlopes
     */
    public function setLandSlopes($landSlopes)
    {
        $this->landSlopes = $landSlopes;
    }

    /**
     * @return string
     */
    public function getType()
    {
        return $this->type;
    }

    /**
     * @param string $type
     */
    public function setType($type)
    {
        $this->type = $type;
    }


    /**
     * @param ExecutionContextInterface $context
     */
    public function isDistanceValid(ExecutionContextInterface $context)
    {
        $distanceSelector = new DistanceSelector();
        $distances = $distanceSelector->getChoices($this->getType(), $this->getHole()->getPar());
        if (!in_array($this->distance, $distances)) {
            $context->buildViolation('Invalid distance value: {1}. Possible values: {2}.', [
                '{1}' => $this->distance,
                '{2}' => implode(', ', $distances),
            ])->atPath('distance')->addViolation();
        }
    }

    /**
     *
     * @param ExecutionContextInterface $context
     */
    public function isResultValid(ExecutionContextInterface $context)
    {
        $resultSelector = new ResultSelector();
        $results = $resultSelector->getChoices($this->getType(), $this->getHole()->getPar());
        if ($this->result && !in_array($this->result, $results)) {
            $context->buildViolation('Invalid result value: {1}. Possible values: {2}.', [
                '{1}' => $this->result,
                '{2}' => implode(', ', $results),
            ])->atPath('result')->addViolation();
        }
    }

    /**
     *
     * @param ExecutionContextInterface $context
     */
    public function isReasonValid(ExecutionContextInterface $context)
    {
        $reasonSelector = new ReasonSelector();
        $reasons = $reasonSelector->getChoices($this->getType(), $this->getResult());
        $reasonNeeded = $reasonSelector->isNeeded($this->getType(), $this->getHole()->getPar(), $this->getResult());

        if ($reasonNeeded && empty($reasons)) {
            $context->buildViolation('Invalid reason value for type {1} and result {2}', [
                '{1}' => $this->getType(),
                '{2}' => $this->getResult(),
            ])->atPath('reason')->addViolation();
            return;
        }

        if ($this->reason && !$reasonNeeded) {
            $context->buildViolation('Invalid reason value, must be empty')
                ->atPath('reason')->addViolation();
        } elseif ($reasonNeeded && !in_array($this->reason, $reasons)) {
            $context->buildViolation('Invalid reason value: {1}. Possible values: {2}.', [
                '{1}' => $this->reason,
                '{2}' => implode(', ', $reasons),
            ])->atPath('reason')->addViolation();
        }
    }
}
