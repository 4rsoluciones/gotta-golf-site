<?php

namespace AndresGotta\Bundle\GolfBundle\Entity;

use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Inflector\Inflector;
use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Validator\Constraints as Assert;
use APY\DataGridBundle\Grid\Mapping as Grid;

/**
 * @ORM\Table(name="report_categories")
 * @ORM\Entity(repositoryClass="AndresGotta\Bundle\GolfBundle\Repository\CategoryRepository")
 * @Grid\Source(columns="id, name, parent.name, description, groups.id:count", groupBy={"id"})
 */
class Category
{
    /**
     * @var integer
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     * @Grid\Column(visible=false, filterable=false)
     */
    private $id;

    /**
     * @var string
     *
     * @ORM\Column(name="name", type="string", length=255)
     * @Grid\Column(title="Name")
     * @Assert\NotBlank()
     */
    private $name;

    /**
     * @var string
     *
     * @ORM\Column(name="description", type="text", nullable=true)
     * @Grid\Column(title="Description")
     */
    private $description;

    /**
     * @var ArrayCollection
     *
     * @ORM\OneToMany(targetEntity="AndresGotta\Bundle\GolfBundle\Entity\GroupReport", mappedBy="category", cascade={"all"})
     * @Grid\Column(title="Groups", field="reportCategories.id:count", filterable=false)
     */
    private $groups;

    /**
     * @var Category
     *
     * @ORM\ManyToOne(targetEntity="AndresGotta\Bundle\GolfBundle\Entity\Category", inversedBy="children", cascade={"persist"})
     * @ORM\JoinColumn(name="parent_id", referencedColumnName="id")
     * @Grid\Column(title="Parent", field="parent.name", filter="select", selectFrom="source", operators={"like"})
     */
    private $parent;

    /**
     * @var ArrayCollection
     *
     * @ORM\OneToMany(targetEntity="AndresGotta\Bundle\GolfBundle\Entity\Category", mappedBy="parent", cascade={"persist"})
     */
    private $children;

    /**
     * 
     */
    public function __construct()
    {
        $this->children = new ArrayCollection();
        $this->reports = new ArrayCollection();
    }

    /**
     * @return string
     */
    public function __toString()
    {
        return $this->name;
    }

    /**
     * Get id
     *
     * @return integer
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * 
     * @return string
     */
    public function getName()
    {
        return $this->name;
    }

    /**
     * 
     * @return string
     */
    public function getDescription()
    {
        return $this->description;
    }

    /**
     * 
     * @param string $name
     * @return Category
     */
    public function setName($name)
    {
        $this->name = $name;
        
        return $this;
    }

    /**
     * 
     * @param string $description
     * @return Category
     */
    public function setDescription($description)
    {
        $this->description = $description;
        
        return $this;
    }

    /**
     * @return Category
     */
    public function getParent()
    {
        return $this->parent;
    }

    /**
     * @param $parent
     * @return Category
     */
    public function setParent($parent = null)
    {
        $this->parent = $parent;

        return $this;
    }

    /**
     * @param null $par
     * @return bool
     */
    public function haveReportPar($par = null)
    {
        foreach ($this->reports as $report) {
            if ($report->getPar() === $par) {
                return true;
            }
        }
        
        foreach ($this->children as $child) {
            if ($child->haveReportPar($par)) {
                return true;
            }
        }
    }

    /**
     * Add child
     *
     * @param Category $child
     *
     * @return Category
     */
    public function addChild(Category $child)
    {
        $child->setParent($this);
        $this->children[] = $child;

        return $this;
    }

    /**
     * Remove child
     *
     * @param Category $child
     */
    public function removeChild(Category $child)
    {
        $child->setParent();
        $this->children->removeElement($child);
    }

    /**
     * @return ArrayCollection
     */
    public function getChildren()
    {
        return $this->children;
    }

    /**
     * Add group
     *
     * @param GroupReport $group
     *
     * @return Category
     */
    public function addGroup(GroupReport $group)
    {
        $this->groups[] = $group;

        return $this;
    }

    /**
     * Remove group
     *
     * @param GroupReport $group
     */
    public function removeGroup(GroupReport $group)
    {
        $this->groups->removeElement($group);
    }

    /**
     * Get groups
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getGroups()
    {
        return $this->groups;
    }

    /**
     * @return string
     */
    public function getSlug()
    {
        $name = str_replace(' - ', '', $this->name);
        $name = str_replace(',', ' ', $name);
        $name = str_replace('/', '', $name);

        return $this->id . strtolower(str_replace(' ', '-', Inflector::classify($name)));
    }

    /**
     * @return array
     */
    public function getChildrenSlug()
    {
        $slugs = [];
        foreach ($this->children as $child) {
            $slugs[] = '.' . $child->getSlug();
        }

        return implode(' ', array_unique($slugs));
    }
}
