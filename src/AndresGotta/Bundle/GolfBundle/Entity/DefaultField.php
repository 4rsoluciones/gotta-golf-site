<?php

namespace AndresGotta\Bundle\GolfBundle\Entity;

use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;
use Symfony\Bridge\Doctrine\Validator\Constraints\UniqueEntity;

/**
 * @ORM\Entity(repositoryClass="AndresGotta\Bundle\GolfBundle\Repository\DefaultFieldRepository")
 * @ORM\Table(name="default_course_fields",
 *     uniqueConstraints={
 *     @ORM\UniqueConstraint(name="unique_field_idx", columns={"country","sportClubName","route","startPoint"})
 * })
 * @UniqueEntity(fields={"country","sportClubName","route","startPoint"}, message="La cancha ya existe")
 */
class DefaultField extends AbstractField
{
    /**
     * @var integer
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @var DefaultHole[]|Collection
     * @ORM\OneToMany(targetEntity="AndresGotta\Bundle\GolfBundle\Entity\DefaultHole", mappedBy="defaultField")
     */
    private $defaultHoles;

    /**
     * DefaultField constructor.
     */
    public function __construct()
    {
        $this->defaultHoles = new ArrayCollection();
    }


    /**
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @param Field $field
     * @return DefaultField
     */
    public static function createFromField(Field $field)
    {
        $defaultField = new DefaultField();
        $defaultField->setType($field->getType());
        $defaultField->setTopographicMovements($field->getTopographicMovements());
        $defaultField->setStartPoint($field->getStartPoint());
        $defaultField->setSportClubName($field->getSportClubName());
        $defaultField->setRoute($field->getRoute());
        $defaultField->setPar($field->getPar());
        $defaultField->setCountry($field->getCountry());
        $defaultField->setHoleCount($field->getHoleCount());

        return $defaultField;
    }

    /**
     * @param DefaultHole $defaultHole
     */
    public function addDefaultHole(DefaultHole $defaultHole)
    {
        $this->defaultHoles[] = $defaultHole;
    }

}