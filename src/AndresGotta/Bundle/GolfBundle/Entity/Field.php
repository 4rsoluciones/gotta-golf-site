<?php

namespace AndresGotta\Bundle\GolfBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Validator\Constraints as Assert;

/**
 * @ORM\Entity
 * @ORM\Table(name="course_fields")
 */
class Field extends AbstractField
{
    /**
     * @var integer
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @var Round
     * @ORM\OneToOne(targetEntity="AndresGotta\Bundle\GolfBundle\Entity\Round", inversedBy="field")
     * @Assert\NotNull(groups={"Default", "OnlyRound"})
     */
    private $round;

    /**
     * @var DefaultField
     * @ORM\ManyToOne(targetEntity="AndresGotta\Bundle\GolfBundle\Entity\DefaultField", cascade={"persist"})
     */
    private $defaultField;

    /**
     * @param Round $round
     * @return Field
     */
    public static function createFromRound(Round $round)
    {
        $field = new Field();
        $field->setRound($round);
        $field->setHoleCount($round->getHoleCount());
        $field->setCountry('Argentina');
        $field->setPar($round->getFieldPar());
        $field->setRoute('unknown');
        $field->setSportClubName('unknown');
        $field->setStartPoint('unknown');
        $field->setTopographicMovements($round->getFieldType());
        $field->setType('unknown');

        return $field;
    }

    /**
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @return Round
     */
    public function getRound()
    {
        return $this->round;
    }

    /**
     * @param Round $round
     */
    public function setRound($round)
    {
        $this->round = $round;
    }

    /**
     * @return DefaultField
     */
    public function getDefaultField()
    {
        return $this->defaultField;
    }

    /**
     * @param DefaultField $defaultField
     */
    public function setDefaultField($defaultField)
    {
        $this->defaultField = $defaultField;
    }

}