<?php

namespace AndresGotta\Bundle\GolfBundle\Entity;

use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\ORM\Mapping as ORM;
use APY\DataGridBundle\Grid\Mapping as Grid;

/**
 * Class ReportGroup
 * @package AndresGotta\Bundle\GolfBundle\Entity
 *
 * @ORM\Entity()
 * @ORM\Table(name="group_reports")
 */
class GroupReport // implements ServiceNeededInterface
{
    /**
     * @var integer
     *
     * @ORM\Id()
     * @ORM\GeneratedValue(strategy="AUTO")
     * @ORM\Column(name="id", type="integer")
     */
    protected $id;

    /**
     * @var string
     *
     * @ORM\Column(name="name", type="string", length=255)
     * @Grid\Column(title="Name")
     */
    private $name;

    /**
     * @var string
     *
     * @ORM\Column(name="description", type="text", nullable=true)
     */
    private $description;

    /**
     * @var Category
     *
     * @ORM\ManyToOne(targetEntity="AndresGotta\Bundle\GolfBundle\Entity\Category", inversedBy="groups")
     * @ORM\JoinColumn(name="category_id", referencedColumnName="id")
     */
    private $category;

//    /**
//     * @ORM\ManyToMany(targetEntity="AndresGotta\Bundle\ServiceBundle\Entity\Service")
//     * @ORM\JoinTable(name="report_services",
//     *      joinColumns={@ORM\JoinColumn(name="report_id", referencedColumnName="id")},
//     *      inverseJoinColumns={@ORM\JoinColumn(name="service_id", referencedColumnName="id")}
//     *      )
//     */
//    private $servicesNeeded;

    /**
     * @var ArrayCollection
     *
     * @ORM\ManyToMany(targetEntity="AndresGotta\Bundle\GolfBundle\Entity\Report", mappedBy="groups", cascade={"persist"})
     */
    protected $reports;

    /**
     * @var boolean
     *
     * @ORM\Column(type="boolean", nullable=true)
     */
    private $enabled = false;

    /**
     * Report constructor.
     */
    public function __construct()
    {
//        $this->servicesNeeded = new ArrayCollection();
        $this->reports = new ArrayCollection();
    }

    /**
     * Get id
     *
     * @return integer
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set name
     *
     * @param string $name
     *
     * @return GroupReport
     */
    public function setName($name)
    {
        $this->name = $name;

        return $this;
    }

    /**
     * Get name
     *
     * @return string
     */
    public function getName()
    {
        return $this->name;
    }

    /**
     * Add report
     *
     * @param Report $report
     *
     * @return GroupReport
     */
    public function addReport(Report $report)
    {
        $this->reports[] = $report;

        return $this;
    }

    /**
     * Remove report
     *
     * @param Report $report
     */
    public function removeReport(Report $report)
    {
        $this->reports->removeElement($report);
    }

    /**
     * Get reports
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getReports()
    {
        return $this->reports;
    }


//    /**
//     * @return ArrayCollection
//     */
//    public function getServicesNeeded()
//    {
//        return $this->servicesNeeded;
//    }
//
//    /**
//     * Add servicesNeeded
//     *
//     * @param Service $servicesNeeded
//     *
//     * @return Report
//     */
//    public function addServicesNeeded(Service $servicesNeeded)
//    {
//        $this->servicesNeeded[] = $servicesNeeded;
//
//        return $this;
//    }
//
//    /**
//     * Remove servicesNeeded
//     *
//     * @param Service $servicesNeeded
//     */
//    public function removeServicesNeeded(Service $servicesNeeded)
//    {
//        $this->servicesNeeded->removeElement($servicesNeeded);
//    }
//
//    /**
//     *
//     */
//    public function removeAllServicesNeeded()
//    {
//        foreach ($this->servicesNeeded as $service) {
//            $this->removeServicesNeeded($service);
//        }
//    }

    /**
     * Set description
     *
     * @param string $description
     *
     * @return GroupReport
     */
    public function setDescription($description)
    {
        $this->description = $description;

        return $this;
    }

    /**
     * Get description
     *
     * @return string
     */
    public function getDescription()
    {
        return $this->description;
    }


    /**
     * Set category
     *
     * @param Category $category
     *
     * @return GroupReport
     */
    public function setCategory(Category $category = null)
    {
        $this->category = $category;

        return $this;
    }

    /**
     * Get category
     *
     * @return Category
     */
    public function getCategory()
    {
        return $this->category;
    }

    /**
     * @return boolean
     */
    public function isEnabled()
    {
        return $this->enabled;
    }

    /**
     * @param boolean $enabled
     */
    public function setEnabled($enabled)
    {
        $this->enabled = $enabled;
    }
}
