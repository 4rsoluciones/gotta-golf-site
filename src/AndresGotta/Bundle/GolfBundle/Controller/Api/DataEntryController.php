<?php

namespace AndresGotta\Bundle\GolfBundle\Controller\Api;

use AndresGotta\Bundle\GolfBundle\Comparative\ChartGroup;
use AndresGotta\Bundle\GolfBundle\CQRS\Command\DeleteHoleCommand;
use AndresGotta\Bundle\GolfBundle\CQRS\Command\DeleteRoundCommand;
use AndresGotta\Bundle\GolfBundle\CQRS\Query\GetDefaultFieldsQuery;
use AndresGotta\Bundle\GolfBundle\CQRS\Query\GetDefaultHolesQuery;
use AndresGotta\Bundle\GolfBundle\CQRS\Query\GetUserComparativeStatsQuery;
use AndresGotta\Bundle\GolfBundle\CQRS\Query\GetUserRoundsQuery;
use AndresGotta\Bundle\GolfBundle\Entity\DefaultField;
use AndresGotta\Bundle\GolfBundle\Entity\DefaultHole;
use AndresGotta\Bundle\GolfBundle\Entity\Hole;
use AndresGotta\Bundle\GolfBundle\Entity\Player;
use AndresGotta\Bundle\GolfBundle\Entity\Round;
use AndresGotta\Bundle\GolfBundle\Event\GolfEvents;
use AndresGotta\Bundle\GolfBundle\Event\RoundEvent;
use AndresGotta\Bundle\GolfBundle\Form\Api\DefaultFieldType;
use AndresGotta\Bundle\GolfBundle\Form\Api\DefaultHoleType;
use AndresGotta\Bundle\GolfBundle\Form\Api\HoleType;
use AndresGotta\Bundle\GolfBundle\Form\Api\PostCompetitiveQuestionnaireType;
use AndresGotta\Bundle\GolfBundle\Form\Api\PreCompetitiveQuestionnaireType;
use AndresGotta\Bundle\GolfBundle\Form\Api\ProfileType;
use AndresGotta\Bundle\GolfBundle\Form\Api\RoundType;
use AndresGotta\Bundle\GolfBundle\ValueObject\Club;
use AndresGotta\Bundle\GolfBundle\ValueObject\Country;
use AndresGotta\Bundle\GolfBundle\ValueObject\Email;
use AndresGotta\Bundle\GolfBundle\ValueObject\FullName;
use AndresGotta\Bundle\GolfBundle\ValueObject\Handicap;
use AndresGotta\Bundle\GolfBundle\ValueObject\PlayerCondition;
use FOS\RestBundle\Controller\Annotations as Rest;
use FOS\RestBundle\Controller\FOSRestController;
use Nelmio\ApiDocBundle\Annotation\ApiDoc;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Security;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpKernel\HttpKernelInterface;
use WebFactory\Bundle\UserBundle\Entity\User;

/**
 * Class DataEntryController
 * @Security("is_granted('GPSTATS_ACCESS', user)")
 */
class DataEntryController extends FOSRestController
{
    /**
     * Permite recuperar las vueltas del usuario logueado
     *
     * @ApiDoc(
     *  section="player",
     *  resource=true,
     *  https=true,
     *  tags={"round"},
     *  description="",
     *  output= {
     *      "class"="array<AndresGotta\Bundle\GolfBundle\Entity\Round>",
     *      "groups" = {"my_rounds"},
     *      "parsers"={
     *          "Nelmio\ApiDocBundle\Parser\JmsMetadataParser"
     *      }
     *  },
     * )
     *
     * @Rest\View(serializerGroups={"my_rounds"}, serializerEnableMaxDepthChecks=true)
     * @Rest\QueryParam(name="incomplete", description="Incomplete round", strict=true, nullable=true, requirements="true|false")
     * @Rest\QueryParam(name="limit", description="Limit", default="20", requirements="\d+")
     * @Rest\QueryParam(name="offset", description="Offset", default="0", requirements="\d+")
     * @Rest\Get("/rounds")
     * @param string $incomplete
     * @param int $limit
     * @param int $offset
     * @return Round[]
     */
    public function getUserRoundsAction($incomplete = null, $limit = 20, $offset = 0)
    {
        /* @var User $user */
        $user = $this->getUser();
        $query = new GetUserRoundsQuery($user, $incomplete, $limit, $offset);
        $rounds = $this->get('tactician.commandbus')->handle($query);

        return $rounds;
    }

    /**
     * Permite recuperar el detalle de una vuelta del usuario logueado
     *
     * @ApiDoc(
     *  section="player",
     *  resource=true,
     *  https=true,
     *  tags={"round"},
     *  description="",
     *  output= {
     *      "class"="AndresGotta\Bundle\GolfBundle\Entity\Round",
     *      "groups" = {"get_round"},
     *      "parsers"={
     *          "Nelmio\ApiDocBundle\Parser\JmsMetadataParser"
     *      }
     *  },
     * )
     *
     * @Rest\View(serializerGroups={"get_round"}, serializerEnableMaxDepthChecks=true)
     * @Rest\Get("/round/{round}")
     * @Security("user == round.getPlayer()")
     * @param Round $round
     * @return Round
     */
    public function getRoundAction(Round $round)
    {
        return $round;
    }

    /**
     * Permite guardar/actualizar el cuestionario pre competitivo de una vuelta
     *
     * @ApiDoc(
     *  section="player",
     *  resource=true,
     *  https=true,
     *  tags={"round"},
     *  description="",
     *  input= {
     *      "class"="AndresGotta\Bundle\GolfBundle\Form\Api\PreCompetitiveQuestionnaireType"
     *  },
     *  output= {
     *      "class"="AndresGotta\Bundle\GolfBundle\Entity\PreCompetitiveQuestionnaire",
     *      "groups" = {"get_round"},
     *      "parsers"={
     *          "Nelmio\ApiDocBundle\Parser\JmsMetadataParser"
     *      }
     *  },
     * )
     *
     * @Rest\View(serializerGroups={"get_round"}, serializerEnableMaxDepthChecks=true)
     * @Rest\Put("/round/pre-competitive-questionnaire/save/{round}", name="pre-competitive-questionnaire-save-put")
     * @Rest\Patch("/round/pre-competitive-questionnaire/save/{round}", name="pre-competitive-questionnaire-save-patch")
     * @Security("user == round.getPlayer()")
     * @param Request $request
     * @param Round $round
     * @return Round|array
     */
    public function savePreCompetitiveQuestionnaireAction(Request $request, Round $round)
    {
        $questionnaire = $round->getPreCompetitiveQuestionnaire();
        $form = $this->createForm(new PreCompetitiveQuestionnaireType(), $questionnaire, [
            'method' => $request->getMethod(),
            'csrf_protection' => false,
        ]);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $this->get('round_persister')->save($round);

            return $round;
        }

        return ['form' => $form,];
    }

    /**
     * Permite guardar/actualizar el cuestionario post competitivo de una vuelta
     *
     * @ApiDoc(
     *  section="player",
     *  resource=true,
     *  https=true,
     *  tags={"round"},
     *  description="",
     *  input= {
     *      "class"="AndresGotta\Bundle\GolfBundle\Form\Api\PostCompetitiveQuestionnaireType"
     *  },
     *  output= {
     *      "class"="AndresGotta\Bundle\GolfBundle\Entity\PostCompetitiveQuestionnaire",
     *      "groups" = {"get_round"},
     *      "parsers"={
     *          "Nelmio\ApiDocBundle\Parser\JmsMetadataParser"
     *      }
     *  },
     * )
     *
     * @Rest\View(serializerGroups={"get_round"}, serializerEnableMaxDepthChecks=true)
     * @Rest\Put("/round/post-competitive-questionnaire/save/{round}", name="post-competitive-questionnaire-save-put")
     * @Rest\Patch("/round/post-competitive-questionnaire/save/{round}", name="post-competitive-questionnaire-save-patch")
     * @Security("user == round.getPlayer()")
     */
    public function savePostCompetitiveQuestionnaireAction(Request $request, Round $round)
    {
        $questionnaire = $round->getPostCompetitiveQuestionnaire();
        $form = $this->createForm(new PostCompetitiveQuestionnaireType(), $questionnaire, [
            'method' => $request->getMethod(),
            'csrf_protection' => false,
        ]);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $this->get('round_persister')->save($round);

            return $round;
        }

        return ['form' => $form,];
    }

    /**
     * Permite guardar una vuelta sin el detalle de sus hoyos
     *
     * @ApiDoc(
     *  section="player",
     *  resource=true,
     *  https=true,
     *  tags={"round"},
     *  description="",
     *  input= {
     *      "class"="AndresGotta\Bundle\GolfBundle\Form\Api\RoundType"
     *  },
     *  output= {
     *      "class"="AndresGotta\Bundle\GolfBundle\Entity\Round",
     *      "groups" = {"get_round"},
     *      "parsers"={
     *          "Nelmio\ApiDocBundle\Parser\JmsMetadataParser"
     *      }
     *  },
     * )
     *
     * @Rest\View(serializerGroups={"get_round"}, serializerEnableMaxDepthChecks=true)
     * @Rest\Post("/round/save")
     * @Rest\Put("/round/save/{round}", name="saveRoundWithoutHolesActionPut")
     * @Rest\Patch("/round/save/{round}", name="saveRoundWithoutHolesActionPatch")
     * @Security("!round || user == round.getPlayer()")
     */
    public function saveRoundWithoutHolesAction(Request $request, Round $round = null)
    {
        $round = $round ? $round : new Round($this->getUser());
        $form = $this->createForm(new RoundType(), $round, [
            'method' => $request->getMethod(),
            'csrf_protection' => false,
        ]);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $this->get('round_persister')->save($round);

            $this->dispatchUpdateScoreEvent($round);

            return $round;
        }

        return ['form' => $form,];
    }

    /**
     * Permite eliminar una vuelta completa
     *
     * @ApiDoc(
     *  section="player",
     *  resource=true,
     *  https=true,
     *  tags={"round"},
     *  description="",
     * )
     *
     * @Rest\View(serializerGroups={"get_round"}, serializerEnableMaxDepthChecks=true)
     * @Rest\Delete("/round/delete/{round}")
     * @Security("user == round.getPlayer()")
     */
    public function deleteRoundAction(Round $round)
    {
        $command = new DeleteRoundCommand($round->getId());
        $this->get('tactician.commandbus')->handle($command);

        $this->dispatchUpdateScoreEvent($round);

        return [];
    }

    /**
     * Permite recuperar las vueltas del usuario logueado
     *
     * @ApiDoc(
     *  section="player",
     *  resource=true,
     *  https=true,
     *  tags={"round"},
     *  description="",
     *  output= {
     *      "class"="array<AndresGotta\Bundle\GolfBundle\Entity\Tour>",
     *      "groups" = {"my_rounds"},
     *      "parsers"={
     *          "Nelmio\ApiDocBundle\Parser\JmsMetadataParser"
     *      }
     *  },
     * )
     *
     * @Rest\View(serializerGroups={"my_rounds"}, serializerEnableMaxDepthChecks=true)
     * @Rest\Get("/tours")
     */
    public function getToursAction()
    {
        $tours = $this->getDoctrine()->getRepository('AndresGottaGolfBundle:Tour')->findAll();

        return $tours;
    }

    /**
     * Permite recuperar un listado de canchas por defecto filtrables por país y términos de búsqueda
     *
     * @ApiDoc(
     *  section="player",
     *  resource=true,
     *  https=true,
     *  tags={"fields"},
     *  description="",
     *  output= {
     *      "class"="array<AndresGotta\Bundle\GolfBundle\Entity\DefaultField>",
     *      "groups" = {"default_fields"},
     *      "parsers"={
     *          "Nelmio\ApiDocBundle\Parser\JmsMetadataParser"
     *      }
     *  },
     * )
     *
     * @Rest\View(serializerGroups={"default_fields"}, serializerEnableMaxDepthChecks=true)
     * @Rest\Get("/default-fields")
     * @Rest\QueryParam(name="country", description="Country", default="")
     * @Rest\QueryParam(name="terms", description="Search terms", default="")
     * @param string $country
     * @param string $terms
     * @return DefaultField[]
     */
    public function getDefaultFieldsAction($country = null, $terms = null)
    {
        $query = new GetDefaultFieldsQuery($country, $terms);
        $defaultFields = $this->get('tactician.commandbus')->handle($query);

        return $defaultFields;
    }

    /**
     * Permite recuperar una cancha por defecto
     *
     * @ApiDoc(
     *  section="player",
     *  resource=true,
     *  https=true,
     *  tags={"fields"},
     *  description="",
     *  output= {
     *      "class"="AndresGotta\Bundle\GolfBundle\Entity\DefaultField",
     *      "groups" = {"default_field"},
     *      "parsers"={
     *          "Nelmio\ApiDocBundle\Parser\JmsMetadataParser"
     *      }
     *  },
     * )
     *
     * @Rest\View(serializerGroups={"default_field"}, serializerEnableMaxDepthChecks=true)
     * @Rest\Get("/default-field/{defaultField}")
     * @param DefaultField $defaultField
     * @return DefaultField
     */
    public function getDefaultFieldAction(DefaultField $defaultField)
    {
        return $defaultField;
    }

    /**
     * Permite guardar una cancha por defecto directamente
     *
     * @ApiDoc(
     *  section="player",
     *  resource=true,
     *  https=true,
     *  tags={"field"},
     *  description="",
     *  input= {
     *      "class"="AndresGotta\Bundle\GolfBundle\Form\Api\DefaultFieldType"
     *  },
     *  output= {
     *      "class"="AndresGotta\Bundle\GolfBundle\Entity\DefaultField",
     *      "groups" = {"default_fields"},
     *      "parsers"={
     *          "Nelmio\ApiDocBundle\Parser\JmsMetadataParser"
     *      }
     *  },
     * )
     *
     * @Rest\View(serializerGroups={"default_fields"}, serializerEnableMaxDepthChecks=true)
     * @Rest\Post("/round/default-field")
     */
    public function createDefaultFieldAction(Request $request)
    {
        $defaultField = new DefaultField();
        $form = $this->createForm(new DefaultFieldType(), $defaultField, [
            'method' => Request::METHOD_POST,
            'csrf_protection' => false,
        ]);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $em = $this->getDoctrine()->getManager();
            $em->persist($defaultField);
            $em->flush();

            return $defaultField;
        }

        return ['form' => $form,];
    }

    /**
     * Permite eliminar un hoyo completo de una vuelta
     *
     * @ApiDoc(
     *  section="player",
     *  resource=true,
     *  https=true,
     *  tags={"hole"},
     *  description="",
     * )
     *
     * @Rest\View(serializerGroups={"get_round"}, serializerEnableMaxDepthChecks=true)
     * @Rest\Delete("/round/{round}/delete/hole/{number}")
     * @Security("user == round.getPlayer()")
     */
    public function deleteHoleAction(Round $round, $number)
    {
        $holeRepository = $this->getDoctrine()->getRepository('AndresGottaGolfBundle:Hole');
        $hole = $holeRepository->findOneByRoundAndNumber($round, $number);
        if (!$hole) {
            throw $this->createNotFoundException();
        }

        $command = new DeleteHoleCommand($hole->getId());
        $this->get('tactician.commandbus')->handle($command);

        $this->dispatchUpdateScoreEvent($hole->getRound());

        return [];
    }

    /**
     * Permite guardar un nuevo hoyo para una vuelta dada
     *
     * @ApiDoc(
     *  section="player",
     *  resource=true,
     *  https=true,
     *  tags={"hole"},
     *  description="",
     *  input= {
     *      "class"="AndresGotta\Bundle\GolfBundle\Form\Api\HoleType"
     *  },
     *  output= {
     *      "class"="AndresGotta\Bundle\GolfBundle\Entity\Hole",
     *      "groups" = {"get_round"},
     *      "parsers"={
     *          "Nelmio\ApiDocBundle\Parser\JmsMetadataParser"
     *      }
     *  },
     * )
     *
     * @Rest\View(serializerGroups={"get_round"}, serializerEnableMaxDepthChecks=true)
     * @Rest\Post("/round/{round}/hole")
     *
     * @param Request $request
     * @param Round $round
     * @return array|Hole
     * @Security("user == round.getPlayer()")
     */
    public function saveHoleAction(Request $request, Round $round)
    {
        $hole = $round->createNewHole();

        $form = $this->createForm(new HoleType(), $hole, [
            'method' => Request::METHOD_POST,
            'csrf_protection' => false,
        ]);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $this->get('hole_persister')->save($hole);

            $this->dispatchUpdateScoreEvent($round);

            return $hole;
        }

        return ['form' => $form,];
    }

    /**
     * Permite recuperar un listado de hoyos por defecto por cancha por defecto
     *
     * @ApiDoc(
     *  section="player",
     *  resource=true,
     *  https=true,
     *  tags={"holes"},
     *  description="",
     *  output= {
     *      "class"="array<AndresGotta\Bundle\GolfBundle\Entity\DefaultHole>",
     *      "groups" = {"default_holes"},
     *      "parsers"={
     *          "Nelmio\ApiDocBundle\Parser\JmsMetadataParser"
     *      }
     *  },
     * )
     *
     * @Rest\View(serializerGroups={"default_holes"}, serializerEnableMaxDepthChecks=true)
     * @Rest\Get("/default-holes")
     * @Rest\QueryParam(name="country", description="Country", default="")
     * @Rest\QueryParam(name="sportClubName", description="Sport club name", default="")
     * @Rest\QueryParam(name="route", description="Route", default="")
     * @Rest\QueryParam(name="startPoint", description="Start point", default="")
     * @param string $country
     * @param string $sportClubName
     * @param string $route
     * @param string $startPoint
     * @return DefaultHole[]
     */
    public function getDefaultHolesAction($country = null, $sportClubName = null, $route = null, $startPoint = null)
    {
        $query = new GetDefaultHolesQuery($country, $sportClubName, $route, $startPoint);
        $defaultHoles = $this->get('tactician.commandbus')->handle($query);

        return $defaultHoles;
    }

    /**
     * Permite recuperar un hoyo por defecto
     *
     * @ApiDoc(
     *  section="player",
     *  resource=true,
     *  https=true,
     *  tags={"holes"},
     *  description="",
     *  output= {
     *      "class"="AndresGotta\Bundle\GolfBundle\Entity\DefaultHole",
     *      "groups" = {"default_hole"},
     *      "parsers"={
     *          "Nelmio\ApiDocBundle\Parser\JmsMetadataParser"
     *      }
     *  },
     * )
     *
     * @Rest\View(serializerGroups={"default_hole"}, serializerEnableMaxDepthChecks=true)
     * @Rest\Get("/default-hole/{defaultHole}")
     * @param DefaultHole $defaultHole
     * @return DefaultHole
     */
    public function getDefaultHoleAction(DefaultHole $defaultHole)
    {
        return $defaultHole;
    }

    /**
     * Permite guardar un hoyo por defecto directamente
     *
     * @ApiDoc(
     *  section="player",
     *  resource=true,
     *  https=true,
     *  tags={"holes"},
     *  description="",
     *  input= {
     *      "class"="AndresGotta\Bundle\GolfBundle\Form\Api\DefaultHoleType"
     *  },
     *  output= {
     *      "class"="AndresGotta\Bundle\GolfBundle\Entity\DefaultHole",
     *      "groups" = {"default_holes"},
     *      "parsers"={
     *          "Nelmio\ApiDocBundle\Parser\JmsMetadataParser"
     *      }
     *  },
     * )
     *
     * @Rest\View(serializerGroups={"default_holes"}, serializerEnableMaxDepthChecks=true)
     * @Rest\Post("/round/default-hole")
     */
    public function createDefaultHoleAction(Request $request)
    {
        $defaultHole = new DefaultHole();
        $form = $this->createForm(new DefaultHoleType(), $defaultHole, [
            'method' => Request::METHOD_POST,
            'csrf_protection' => false,
            'default_field_repository' => $this->getDoctrine()->getRepository('AndresGottaGolfBundle:DefaultField'),
        ]);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $em = $this->getDoctrine()->getManager();
            $em->persist($defaultHole);
            $em->flush();

            return $defaultHole;
        }

        return ['form' => $form,];
    }

    /**
     * Permite recuperar las estadísticas comparativas del usuario logueado
     *
     * @ApiDoc(
     *  section="player",
     *  resource=true,
     *  https=true,
     *  tags={"stats"},
     *  description="",
     *  output= {
     *      "class"="AndresGotta\Bundle\GolfBundle\Comparative\ChartGroup",
     *      "groups" = {"comparatives"},
     *      "parsers"={
     *          "Nelmio\ApiDocBundle\Parser\JmsMetadataParser"
     *      }
     *  },
     * )
     *
     * @Rest\View(serializerGroups={"comparatives"}, serializerEnableMaxDepthChecks=true)
     * @Rest\Get("/comparatives")
     * @return ChartGroup
     */
    public function showComparativesAction()
    {
        /* @var User $user */
        $user = $this->getUser();
        $query = new GetUserComparativeStatsQuery($user);
        $stats = $this->get('tactician.commandbus')->handle($query);

        return $stats;
    }

    /**
     * Permite recuperar datos de perfil del usuario logueado
     *
     * @ApiDoc(
     *  section="player",
     *  resource=true,
     *  https=true,
     *  tags={"profile"},
     *  description="",
     *  output= {
     *      "class"="WebFactory\Bundle\UserBundle\Entity\User",
     *      "groups" = {"gpstats_profile"},
     *      "parsers"={
     *          "Nelmio\ApiDocBundle\Parser\JmsMetadataParser"
     *      }
     *  },
     * )
     *
     * @Rest\View(serializerGroups={"gpstats_profile"}, serializerEnableMaxDepthChecks=true)
     * @Rest\Get("/profile")
     * @return User
     */
    public function getProfileAction()
    {
        /* @var User $user */
        $user = $this->getUser();

        try {
            $em = $this->getDoctrine()->getManager();
            $playerRepository = $em->getRepository('AndresGottaGolfBundle:Player');
            $player = $playerRepository->getByEmail(new Email($user->getEmail()));
            if ($player) {
                $profile = $user->getProfile();
                $profile->setCondition((string)$player->getCondition());
                $profile->setHandicap($player->getHandicap()->getRawValue());
                $profile->setTour($player->getTour());
                $profile->setCountry((string)$player->getCountry());
            }

        } catch (\InvalidArgumentException $e) {
            $this->get('logger')->addError(__METHOD__ . ': ' . $e->getMessage(), $e->getTrace());
        } catch (\Exception $e) {
            $this->get('logger')->addError(__METHOD__ . ': ' . $e->getMessage(), $e->getTrace());
        }

        return $user;
    }

    /**
     * Permite guardar/actualizar el perfil del usuario logueado
     *
     * @ApiDoc(
     *  section="player",
     *  resource=true,
     *  https=true,
     *  tags={"profile"},
     *  description="",
     *  input= {
     *      "class"="AndresGotta\Bundle\GolfBundle\Form\Api\ProfileType"
     *  },
     *  output= {
     *      "class"="WebFactory\Bundle\UserBundle\Entity\User",
     *      "groups" = {"gpstats_profile"},
     *      "parsers"={
     *          "Nelmio\ApiDocBundle\Parser\JmsMetadataParser"
     *      }
     *  },
     * )
     *
     * @Rest\View(serializerGroups={"gpstats_profile"}, serializerEnableMaxDepthChecks=true)
     * @Rest\Put("/profile", name="update-profile-put")
     * @Rest\Patch("/profile", name="update-profile-patch")
     * @param Request $request
     * @return User|array
     */
    public function updateProfileAction(Request $request)
    {
        /** @var User */
        $user = $this->getUser();
        $form = $this->createForm(new ProfileType(), $user->getProfile(), [
            'method' => $request->getMethod(),
            'csrf_protection' => false,
        ]);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $em = $this->getDoctrine()->getManager();
            $em->persist($user);
            $em->flush();

            try {
                $playerRepository = $em->getRepository('AndresGottaGolfBundle:Player');
                $player = $playerRepository->getByEmail(new Email($user->getEmail()));
                if (!$player) {
                    $player = new Player(
                        new Email($user->getEmail()),
                        new FullName($user->getProfile()->getFirstName(), $user->getProfile()->getLastName()),
                        new PlayerCondition($user->getProfile()->getCondition()),
                        new Handicap($user->getProfile()->getHandicap()),
                        $user->getProfile()->getTour()
                    );
                }

                $player->setCondition(new PlayerCondition($user->getProfile()->getCondition()));
                $player->setHandicap(new Handicap($user->getProfile()->getHandicap()));
                $player->setCountry(new Country($user->getProfile()->getCountry()));
                $player->setTour($user->getProfile()->getTour());

                $playerRepository->add($player);
            } catch (\InvalidArgumentException $e) {
                $this->get('logger')->addError(__METHOD__ . ': ' . $e->getMessage(), $e->getTrace());
            } catch (\Exception $e) {
                $this->get('logger')->addError(__METHOD__ . ': ' . $e->getMessage(), $e->getTrace());
            }

            return $user;
        }

        return ['form' => $form,];
    }

    /**
     * @param Round $round
     */
    private function dispatchUpdateScoreEvent(Round $round)
    {
        $event = new RoundEvent($round);
        $this->get('event_dispatcher')->dispatch(GolfEvents::UPDATE_SCORE_EVENT, $event);
    }
}
