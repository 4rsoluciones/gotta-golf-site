<?php

namespace AndresGotta\Bundle\GolfBundle\Controller\Backend;

use AndresGotta\Bundle\GolfBundle\Entity\Report;
use AndresGotta\Bundle\GolfBundle\Form\ReportType;
use AndresGotta\Bundle\GolfBundle\Form\UserReportFilterType;
use APY\DataGridBundle\Grid\Action\RowAction;
use APY\DataGridBundle\Grid\Source\Entity;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Template;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;

/**
 * @Route("/golf/reports")
 */
class ReportController extends Controller
{

    /**
     * @Route("/", name="backend_report_index")
     * @Method("GET|POST")
     * @Template
     */
    public function indexAction()
    {
        /* @var $source \APY\DataGridBundle\Grid\Source\Entity */
        $source = new Entity('AndresGottaGolfBundle:Report');

        /* @var $grid \APY\DataGridBundle\Grid\Grid */
        $grid = $this->get('grid');
        $grid->setSource($source);

        $showRowAction = new RowAction('Edit', 'backend_report_edit', false, '_self', array('class' => 'btn-action glyphicons pencil btn-success'));
        $grid->addRowAction($showRowAction);

        return $grid->getGridResponse('AndresGottaGolfBundle:Backend\Report:index.html.twig', compact('services'));
    }

    /**
     * @Route("/{id}/edit", name="backend_report_edit")
     * @Template()
     */
    public function editAction(Request $request, Report $report)
    {
        $form = $this->createForm(new ReportType(), $report, [
            'action' => $this->generateUrl('backend_report_edit', ['id' => $report->getId()]),
            'method' => 'PUT'
        ]);
        $form->add('submit', 'submit', ['label' => 'Guardar', 'attr' => ['class' => 'btn btn-primary']]);

        $form->handleRequest($request);
        if ($form->isValid()) {
            $this->getDoctrine()->getManager()->flush();
            $this->addFlash('success', $this->get('translator')->trans('flash.message.generic.updated'));

            return $this->redirectToRoute('backend_report_index');
        }

        return ['form' => $form->createView(), 'report' => $report];
    }

    /**
     * @Route("/user", name="backend_report_user")
     * @Method("GET|POST")
     * @Template
     */
    public function userAction(Request $request)
    {
        $data = null;
        $report = null;
        $filterForm = $this->createForm(new UserReportFilterType);
        $filterForm->add('submit', 'submit', array('label' => 'Filter'));

        if ($request->getMethod() == 'POST') {
            $filterForm->handleRequest($request);

            if ($filterForm->isValid()) {
                $form_data = $filterForm->getData();
                $report = $form_data['report'];
                $filters = array(
                    'user' => $form_data['user'],
                    'from_date' => $form_data['from_date'],
                    'to_date' => $form_data['to_date'],
                    'condition' => $form_data['condition'],
                    'roundType' => $form_data['roundType'],
                );

                $manager = $this->get('andres_gotta_golf.statistic.manager');
                $data = $manager->generate($report, $filters);
            }
        }

        $filter_form = $filterForm->createView();

        return compact('data', 'report', 'filter_form');
    }

    /**
     * @Route("/questionnaires", name="backend_report_questionnaires")
     * @Method("GET|POST")
     * @Template
     */
    public function questionnairesAction(Request $request)
    {
        ini_set('memory_limit', '2048M');
        set_time_limit(120);

        $questionnairesReportManager = $this->get('andres_gotta.questionnaires_report_manager');

        $holesEmotionalAverage = $questionnairesReportManager->getHolesEmotionalAverage();
        $holesFoodAverage = $questionnairesReportManager->getHolesFoodAverage();
        $holesPhysicalAverage = $questionnairesReportManager->getHolesPhysicalAverage();
        $emotionalRates = $questionnairesReportManager->getEmotionalRates();
        $foodRates = $questionnairesReportManager->getFoodRates();
        $physicalRates = $questionnairesReportManager->getPhysicalRates();
        $preCompetitiveQuestionnairesAverage = $questionnairesReportManager->getPreCompetitiveQuestionnairesAverage();

        return compact(
            'holesEmotionalAverage',
            'holesFoodAverage',
            'holesPhysicalAverage',
            'emotionalRates',
            'foodRates',
            'physicalRates',
            'preCompetitiveQuestionnairesAverage'
        );
    }
}
