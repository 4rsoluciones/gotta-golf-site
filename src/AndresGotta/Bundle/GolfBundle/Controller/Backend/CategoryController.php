<?php

namespace AndresGotta\Bundle\GolfBundle\Controller\Backend;

use AndresGotta\Bundle\GolfBundle\Entity\Category;
use AndresGotta\Bundle\GolfBundle\Form\CategoryType;
use APY\DataGridBundle\Grid\Action\MassAction;
use APY\DataGridBundle\Grid\Action\RowAction;
use APY\DataGridBundle\Grid\Row;
use APY\DataGridBundle\Grid\Source\Entity;
use Doctrine\ORM\QueryBuilder;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Template;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;

/**
 * @Route("/golf/reports/category")
 */
class CategoryController extends Controller
{
    /**
     * @Route("/", name="backend_report_categorys")
     * @Method("GET|POST")
     * @Template
     */
    public function indexAction()
    {
        /* @var $source \APY\DataGridBundle\Grid\Source\Entity */
        $source = new Entity('AndresGottaGolfBundle:Category');

        /* @var $grid \APY\DataGridBundle\Grid\Grid */
        $grid = $this->get('grid');

        $source->manipulateRow(function  (Row $row) {
            $category = $row->getEntity();
            if ($category->getParent()) {
                $row->setClass($category->getParent()->getSlug());
                $row->setColor('#f1f1f1');
            } else {
                $row->setClass($category->getSlug());
            }

            return $row;
        });

        $grid->setSource($source);

        $showRowAction = new RowAction('Edit', 'backend_report_category_edit', false, '_self', array('class' => 'btn-action glyphicons pencil btn-success'));
        $grid->addRowAction($showRowAction);
        $deleteRowAction = new RowAction('Delete', 'backend_report_category_delete', true, '_self', array('class' => 'btn-action glyphicons remove_2 btn-danger'));
        $grid->addRowAction($deleteRowAction);

//        $em = $this->getDoctrine()->getManager();
//        $services = $em->getRepository('AndresGottaServiceBundle:Service')->findAll();
//
//        foreach ($services as $service) {
//            $action = new MassAction($this->get('translator')->trans('Add service %name%', ['%name%' => $service->getName()]), function($primaryKeys) use ($em, $service) {
//                foreach ($primaryKeys as $key) {
//                    $report = $em->getRepository('AndresGottaGolfBundle:Report')->find($key);
//                    if ($report) {
//                        $report->addServicesNeeded($service);
//                        $em->persist($report);
//                    }
//                }
//
//                $em->flush();
//                $this->addFlash('info', $this->get('translator')->trans('flash.message.generic.updated'));
//            }, true);
//            $grid->addMassAction($action);
//        }
//
//        $action = new MassAction($this->get('translator')->trans('Remove all services'), function ($primaryKeys) use ($em) {
//            foreach ($primaryKeys as $key) {
//                $report = $em->getRepository('AndresGottaGolfBundle:Report')->find($key);
//                $report->removeAllServicesNeeded();
//                $em->persist($report);
//            }
//            $em->flush();
//        });
//        $grid->addMassAction($action);

        return $grid->getGridResponse('AndresGottaGolfBundle:Backend\Category:index.html.twig');
    }

    /**
     * @Route("/crete/category", name="backend_report_category_create")
     * @Template()
     */
    public function createAction(Request $request)
    {
        $reportCategory = new Category();
        $form = $this->createForm(new CategoryType(), $reportCategory, [
            'action' => $this->generateUrl('backend_report_category_create'),
            'method' => 'POST'
        ]);
        $form->add('submit', 'submit', ['label' => 'Guardar', 'attr' => ['class' => 'btn btn-primary']]);

        $form->handleRequest($request);
        if ($form->isValid()) {
            $this->getDoctrine()->getManager()->persist($reportCategory);
            $this->getDoctrine()->getManager()->flush();
            $this->addFlash('success', $this->get('translator')->trans('flash.message.generic.created'));

            return $this->redirectToRoute('backend_report_categorys');
        }

        return ['form' => $form->createView()];
    }


    /**
     * @Route("/{id}/edit", name="backend_report_category_edit")
     * @Template()
     */
    public function editAction(Request $request, Category $reportCategory)
    {
        $form = $this->createForm(new CategoryType(), $reportCategory, [
            'action' => $this->generateUrl('backend_report_category_edit', ['id' => $reportCategory->getId()]),
            'method' => 'PUT'
        ]);
        $form->add('submit', 'submit', ['label' => 'Guardar', 'attr' => ['class' => 'btn btn-primary']]);

        $form->handleRequest($request);
        if ($form->isValid()) {
            foreach ($reportCategory->getReports() as $report) {
                $report->completeSlug();
                $this->getDoctrine()->getManager()->persist($report);
            }

            $this->getDoctrine()->getManager()->flush();
            $this->addFlash('success', $this->get('translator')->trans('flash.message.generic.updated'));

            return $this->redirectToRoute('backend_report_categorys');
        }

        return ['form' => $form->createView(), 'report' => $reportCategory];
    }

    /**
     * @Route("/{id}/delete", name="backend_report_category_delete")
     */
    public function deleteAction(Category $category)
    {
        if ($category->getChildren()->count() > 0) {
            $this->addFlash('error', $this->get('translator')->trans('flash.message.generic.has_children'));
        } else {
            $this->getDoctrine()->getManager()->remove($category);
            $this->getDoctrine()->getManager()->flush();

            $this->addFlash('success', $this->get('translator')->trans('flash.message.generic.delete'));
        }


        return $this->redirectToRoute('backend_report_categorys');
    }

    /**
     * @param Request $request
     * @return JsonResponse
     * @Route("/ajax_call", name="ajax_report_categorys")
     * @Method("GET")
     */
    public function categorySearchAction(Request $request)
    {
        $value = $request->get('term');

        $em = $this->getDoctrine()->getEntityManager();
        $categorys = $em->getRepository('AndresGottaGolfBundle:Category')->createQueryBuilder('t')
            ->where("t.name like :name")->setParameter('name', "%{$value}%")
            ->getQuery()->getResult();

        $json = array();
        foreach ($categorys as $category) {
            $json[] = array(
                'label' => $category->getName(),
                'value' => $category->getName()
            );
        }

        return new JsonResponse($json);
    }

}
