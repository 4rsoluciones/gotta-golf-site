<?php

namespace AndresGotta\Bundle\GolfBundle\Controller\Backend;

use AndresGotta\Bundle\GolfBundle\Entity\Comparative;
use AndresGotta\Bundle\GolfBundle\Form\ComparativeType;
use APY\DataGridBundle\Grid\Action\MassAction;
use APY\DataGridBundle\Grid\Action\RowAction;
use APY\DataGridBundle\Grid\Source\Entity;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Template;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;

/**
 * Class ComparativeController
 * @package AndresGotta\Bundle\GolfBundle\Controller\Frontend
 *
 * @Route("/comparatives")
 */
class ComparativeController extends Controller
{
    /**
     * @Route("/", name="backend_comparative_index")
     */
    public function indexAction()
    {
        /* @var $source \APY\DataGridBundle\Grid\Source\Entity */
        $source = new Entity('AndresGottaGolfBundle:Comparative');

        /* @var $grid \APY\DataGridBundle\Grid\Grid */
        $grid = $this->get('grid');
        $grid->setSource($source);

        $showRowAction = new RowAction('Edit', 'backend_comparative_edit', false, '_self', array('class' => 'btn-action glyphicons pencil btn-success'));
        $grid->addRowAction($showRowAction);

//        $em = $this->getDoctrine()->getManager();
//        $services = $em->getRepository('AndresGottaServiceBundle:Service')->findAll();
//
//        foreach ($services as $service) {
//            $action = new MassAction($this->get('translator')->trans('Add service %name%', ['%name%' => $service->getName()]), function($primaryKeys) use ($em, $service) {
//                foreach ($primaryKeys as $key) {
//                    $report = $em->getRepository('AndresGottaGolfBundle:Comparative')->find($key);
//                    if ($report) {
//                        $report->addServicesNeeded($service);
//                        $em->persist($report);
//                    }
//                }
//
//                $em->flush();
//                $this->addFlash('info', $this->get('translator')->trans('flash.message.generic.updated'));
//            }, true);
//            $grid->addMassAction($action);
//        }
//
//        $action = new MassAction($this->get('translator')->trans('Remove all services'), function ($primaryKeys) use ($em) {
//            foreach ($primaryKeys as $key) {
//                $report = $em->getRepository('AndresGottaGolfBundle:Comparative')->find($key);
//                $report->removeAllServicesNeeded();
//                $em->persist($report);
//            }
//            $em->flush();
//        });
//        $grid->addMassAction($action);

        return $grid->getGridResponse('AndresGottaGolfBundle:Backend\Comparative:index.html.twig', compact('services'));
    }

    /**
     * @Route("/{id}/edit", name="backend_comparative_edit")
     * @Template()
     */
    public function editAction(Request $request, Comparative $comparative)
    {
        $form = $this->createForm(new ComparativeType(), $comparative, [
            'action' => $this->generateUrl('backend_comparative_edit', ['id' => $comparative->getId()]),
            'method' => 'PUT'
        ]);
        $form->add('submit', 'submit', ['label' => 'Guardar', 'attr' => ['class' => 'btn btn-primary']]);

        $form->handleRequest($request);
        if ($form->isValid()) {
            $this->getDoctrine()->getManager()->flush();
            $this->addFlash('success', $this->get('translator')->trans('flash.message.generic.updated'));

            return $this->redirectToRoute('backend_comparative_index');
        }

        return ['form' => $form->createView(), 'comparative' => $comparative];
    }
}