<?php

namespace AndresGotta\Bundle\GolfBundle\Controller\Frontend;

use AndresGotta\Bundle\GolfBundle\Entity\GroupReport;
use AndresGotta\Bundle\GolfBundle\Entity\Report;
use AndresGotta\Bundle\GolfBundle\Form\FeedbackType;
use AndresGotta\Bundle\GolfBundle\Form\ReportFilterType;
use AndresGotta\Bundle\GolfBundle\ValueObject\RoundType;
use DateTime;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Security;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Template;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;

/**
 * Controladores de carga de datos
 *
 * @Route("/golf/reports")
 */
class ReportController extends Controller
{

    /**
     * Listado de tipos de reportes (y reportes) de este tipo de reporte
     *
     * @Route("/category", name="frontend_report_list")
     * @Method("GET")
     * @Template
     * @Security("has_role('ROLE_PLAYER')")
     */
    public function indexAction()
    {
        $em = $this->getDoctrine()->getManager();
        $repository = $em->getRepository('AndresGottaGolfBundle:Category');

        $categories = $repository->findBy(['parent' => null]);
        $children = $repository->findChildCategories();

        return compact('children', 'categories');
    }

    /**
     * @param GroupReport $groupReport
     * @return array
     *
     * @Route("/group/{id}/show", name="frontend_report_group_show")
     * @Method("GET")
     * @Template()
     * //@Security("is_granted('access', groupReport)")
     */
    public function showGroupAction(GroupReport $groupReport)
    {
        return compact('groupReport');
    }

    /**
     * @Route("/{id}/show", name="frontend_report_show")
     * @Method({"GET", "POST"})
     * @Template
     */
    public function showAction(Request $request, Report $report)
    {
        $filters = array(
            'par' => $report->getPar(),
            'from_date' => null,
            'to_date' => null,
            'condition' => $this->getUser()->getProfile()->getCondition(),
            'roundType' => RoundType::TOURNAMENT,
        );

        $form = $this->createForm(new ReportFilterType(), $filters, [
            'action' => $this->generateUrl('frontend_report_show', ['id' => $report->getId()] ),
            'method' => 'POST'
        ]);

        if ($request->getMethod() === Request::METHOD_POST) {
            $form->handleRequest($request);

            //TODO: Agregar validación al formulario
            if ($form->isValid()) {
                $filters = array_merge($filters, $form->getData());
            }
        }

        // selecciono el report segun el par que venga en el form
        $formData = $form->getData();
        $par = $formData['par'];

        $manager = $this->get('andres_gotta_golf.statistic.manager');
        $data = $manager->generate($report, $filters);

        $filterForm = $form->createView();

        return compact('data', 'report', 'filterForm', 'par');
    }
    
    /**
     * Detalle real del reporte
     *
     * @Template
     */
    public function detailAction(Report $report, array $filters)
    {
        $manager = $this->get('andres_gotta_golf.statistic.manager');
        $data = $manager->generate($report, $filters);

        return compact('data', 'report');
    }

    /**
     * Muestra el menu para reportes personalizados
     *
     * @Template
     */
    public function customizedReportsMenuAction()
    {
        return array();
    }

    /**
     * Detalle de vuelta
     *
     * @Route("/show/custom", name="frontend_report_show_custom")
     * @Method({"GET","POST"})
     * @Template
     */
    public function showCustomizedReportsAction(Request $request)
    {
        $repository = $this->getDoctrine()->getRepository('AndresGottaGolfBundle:Report');

        $reports_ids = explode(',', $request->cookies->get('custom_reports'));

        $reports = $repository->findAllByIds($reports_ids);
        
        $filters = array(
            'from_date' => null,
            'to_date' => new DateTime,
            'condition' => null,
        );

        $form = $this->createForm(new ReportFilterType(), $filters);
        $form->add('submit', 'submit', array('label' => 'Filter'));

        $request = $this->get('request');
        $form->handleRequest($request);

        //TODO: Agregar validación al formulario
        if ($form->isValid()) {
            $filters = array_merge($filters, $form->getData());
        }
        $filterForm = $form->createView();

        return compact('reports', 'filterForm', 'filters');
    }

    /**
     * @return array
     *
     * @Route("/feedback")
     * @Template()
     */
    public function feedbackAction(Request $request)
    {
        $form = $this->createForm(new FeedbackType(), null, [
            'action' => $this->generateUrl('andresgotta_golf_frontend_report_feedback'),
            'method' => 'POST'
        ]);
        $form->handleRequest($request);

        if ($request->getMethod() === Request::METHOD_POST) {
            if ($form->isValid()) {
                $this->get('fos_user.mailer')->sendFeedbackMail($this->getUser(), $form->get('comment')->getData());

                return new JsonResponse(['ok' => true]);
            } else {
                return new JsonResponse(['ok' => false]);
            }
        }

        return ['form' => $form->createView()];
    }

}
