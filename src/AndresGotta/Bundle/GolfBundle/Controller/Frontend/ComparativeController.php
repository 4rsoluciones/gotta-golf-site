<?php

namespace AndresGotta\Bundle\GolfBundle\Controller\Frontend;

use AndresGotta\Bundle\GolfBundle\Entity\Comparative;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Security;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Template;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;

/**
 * Class ComparativeController
 * @package AndresGotta\Bundle\GolfBundle\Controller\Frontend
 *
 * @Route("/comparative")
 */
class ComparativeController extends Controller
{
    /**
     * @param Request $request
     * @param Comparative $comparative
     * @return \Symfony\Component\HttpFoundation\RedirectResponse
     *
     * @Route("/{id}/show", name="frontend_comparative_show")
     * @Method("GET")
     * @Template
     * //@Security("has_role('ROLE_PLAYER') and is_granted('access', comparative)")
     */
    public function showAction(Request $request, Comparative $comparative)
    {
        $manager = $this->get('comparative_manager');
    }
}