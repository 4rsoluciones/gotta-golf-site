<?php

namespace AndresGotta\Bundle\GolfBundle\Controller\Frontend;

use AndresGotta\Bundle\GolfBundle\Entity\Hole;
use AndresGotta\Bundle\GolfBundle\Entity\Round;
use AndresGotta\Bundle\GolfBundle\Entity\PreCompetitiveQuestionnaire;
use AndresGotta\Bundle\GolfBundle\Entity\PostCompetitiveQuestionnaire;
use AndresGotta\Bundle\GolfBundle\Event\GolfEvents;
use AndresGotta\Bundle\GolfBundle\Event\RoundEvent;
use AndresGotta\Bundle\GolfBundle\Form\RoundType;
use AndresGotta\Bundle\GolfBundle\Form\PreCompetitiveQuestionnaireType;
use AndresGotta\Bundle\GolfBundle\Form\PostCompetitiveQuestionnaireType;
use AndresGotta\Bundle\GolfBundle\Security\PlayerServicesVoter;
use AndresGotta\Bundle\GolfBundle\ValueObject\RoundType as RoundTypeValueObject;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Security;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Template;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Security\Core\Exception\AccessDeniedException;

/**
 * Controladores de carga de datos
 *
 * @Route("/golf/data-entry")
 */
class DataEntryController extends Controller
{

    /**
     * Listado de las vueltas del usuario actual
     *
     * @Route("/", name="frontend_round")
     * @Method("GET")
     * @Template()
     * @Security("has_role('ROLE_PLAYER')")
     */
    public function indexAction(Request $request)
    {
//        if (false === $this->get('security.context')->isGranted(PlayerServicesVoter::ROLE_ACTIVE_PLAYER)) {
//            throw new AccessDeniedException();
//        }

        $em = $this->getDoctrine()->getManager();
        $repo = $em->getRepository('AndresGottaGolfBundle:Round');
        /* @var $queryBuilder \Doctrine\ORM\QueryBuilder */
        $queryBuilder = $repo->createQueryBuilderByPlayer($this->getUser());
        
        $queryBuilder->addSelect($queryBuilder->expr()->count('Hole') . ' AS holeCount');
        $queryBuilder->leftJoin('Round.holes', 'Hole');
        $queryBuilder->addGroupBy('Round.id');

        $page = $request->query->get('page', 1);
        $paginator = $this->get('knp_paginator');
        $entities = $paginator->paginate($queryBuilder, $page, 10);

        return compact('entities');
    }

    /**
     * Detalle de vuelta
     *
     * @Route("/{id}/show", name="frontend_round_show")
     * @Method("GET")
     * @Template()
     * //@Security("is_granted('access', round)")
     */
    public function showRoundAction(Round $round)
    {
//        if (false === $this->get('security.context')->isGranted(PlayerServicesVoter::ROLE_ACTIVE_PLAYER, $round)) {
//            throw new AccessDeniedException();
//        }

        if (!$round->getPreCompetitiveQuestionnaire()) {
            return $this->redirectToRoute('frontend_round_pre_competitive_questionnaire', array(
                'id' => $round->getId(),
            ));
        }
        
        return compact('round');
    }

    /**
     * Cuestionario antes de comenzar la vuelta
     *
     * @Route("/{id}/pre-competitive-questionnaire", name="frontend_round_pre_competitive_questionnaire")
     * @Method({"GET","POST"})
     * @Template()
     */
    public function preCompetitiveQuestionnaireAction(Request $request, Round $round)
    {
        if (false === $this->get('security.context')->isGranted(PlayerServicesVoter::ROLE_ACTIVE_PLAYER)) {
            throw new AccessDeniedException();
        }

        if ($round->getPreCompetitiveQuestionnaire()) {
            return $this->redirectToRoute('frontend_round_show', array(
                'id' => $round->getId(),
            ));
        }

        $preCompetitiveQuestionnaire = new PreCompetitiveQuestionnaire;
        $preCompetitiveQuestionnaireForm = $this->createForm(new PreCompetitiveQuestionnaireType, $preCompetitiveQuestionnaire);

        $preCompetitiveQuestionnaireForm->handleRequest($request);
        if ($preCompetitiveQuestionnaireForm->isSubmitted() && $preCompetitiveQuestionnaireForm->isValid()) {
            $preCompetitiveQuestionnaire->setRound($round);
            $round->setPreCompetitiveQuestionnaire($preCompetitiveQuestionnaire);

            $this->getDoctrine()->getManager()->flush();

            return $this->redirectToRoute('frontend_round_pre_competitive_questionnaire', array(
                'id' => $round->getId(),
            ));
        }

        return array(
            'round' => $round,
            'preCompetitiveQuestionnaireForm' => $preCompetitiveQuestionnaireForm->createView(),
        );
    }

    /**
     * Cuestionario luego de terminada la vuelta
     *
     * @Route("/{id}/post-competitive-questionnaire", name="frontend_round_post_competitive_questionnaire")
     * @Method({"GET","POST"})
     * @Template()
     */
    public function postCompetitiveQuestionnaireAction(Request $request, Round $round)
    {
        if (false === $this->get('security.context')->isGranted(PlayerServicesVoter::ROLE_ACTIVE_PLAYER)) {
            throw new AccessDeniedException();
        }

        if ($round->getPostCompetitiveQuestionnaire()) {
            return $this->redirectToRoute('frontend_round_show', array(
                'id' => $round->getId(),
            ));
        }

        $postCompetitiveQuestionnaire = new PostCompetitiveQuestionnaire;
        $postCompetitiveQuestionnaireForm = $this->createForm(new PostCompetitiveQuestionnaireType, $postCompetitiveQuestionnaire);

        $postCompetitiveQuestionnaireForm->handleRequest($request);
        if ($postCompetitiveQuestionnaireForm->isSubmitted() && $postCompetitiveQuestionnaireForm->isValid()) {
            $postCompetitiveQuestionnaire->setRound($round);
            $round->setPostCompetitiveQuestionnaire($postCompetitiveQuestionnaire);

            $this->getDoctrine()->getManager()->flush();

            return $this->redirectToRoute('frontend_round_pre_competitive_questionnaire', array(
                'id' => $round->getId(),
            ));
        }

        return array(
            'round' => $round,
            'postCompetitiveQuestionnaireForm' => $postCompetitiveQuestionnaireForm->createView(),
        );
    }

    /**
     * Carga de nueva vuelta sin hoyos
     *
     * @Route("/new", name="frontend_round_new")
     * @Method({"GET","POST"})
     * @Template()
     */
    public function newRoundAction(Request $request)
    {
        if (false === $this->get('security.context')->isGranted(PlayerServicesVoter::ROLE_ACTIVE_PLAYER)) {
            throw new AccessDeniedException();
        }

        $round = new Round();
        $round->setPlayer($this->getUser());
        $round->setPlayerCondition($this->getUser()->getProfile()->getCondition());
        $round->setRoundType(RoundTypeValueObject::PRACTICE);

        $form = $this->getCreateRoundForm($round);

        $form->handleRequest($request);

        if ($form->isValid()) {
            $em = $this->getDoctrine()->getManager();
            $em->persist($round);
            $em->flush();

            $this->get('session')->getFlashBag()->add('success', $this->get('translator')->trans('The round was saved.'));

            return $this->redirect($this->generateUrl('frontend_round_show', array('id' => $round->getId())));
        }

        return array(
            'entity' => $round,
            'form' => $form->createView(),
        );
    }

    /**
     * @Route("/{id}/edit", name="frontend_round_edit")
     * @Method({"GET","PUT"})
     * @Template()
     */
    public function editRoundAction(Request $request, Round $round)
    {
        if (false === $this->get('security.authorization_checker')->isGranted(PlayerServicesVoter::ROLE_ACTIVE_PLAYER, $round)) {
            throw new AccessDeniedException();
        }

        $em = $this->getDoctrine()->getManager();

        $form = $this->getUpdateRoundForm($round);
        $form->handleRequest($request);

        if ($form->isSubmitted()) {

            if ($form->isValid()) {
                $em->flush();
                $this->addFlash('success', $this->get('translator')->trans('The round was saved.'));

                return $this->redirectToRoute('frontend_round_show', ['id' => $round->getId()]);
            } else {
                $errors = $this->get('validator')->validate($round, array('Default'));

                if ($errors->count()) {
                    foreach ($errors as $error) {
                        $this->addFlash('error', $error->getMessage());
                    }
                }
            }

        }

        return ['form' => $form->createView()];
    }

    /**
     * Permite cargar un nuevo hoyo para una vuelta existente
     * @Route("/new/{id}/hole", name="frontend_hole_new")
     * @Method({"GET","POST"})
     * @Template()
     */
    public function newHoleAction(Request $request, Round $round)
    {
        if ($round->getHoleCount() < $round->getHoles()->count() + 1) {
            $this->addFlash('error', $this->get('translator')->trans('All holes needed for this round are was loaded.'));
            $redirection = $request->headers->get('referer');
            $redirection = $redirection ? $redirection : $this->generateUrl('frontend_round');

            return $this->redirect($redirection);
        }

        $hole = new Hole();
        $hole->setRound($round);
        if ($request->get('_number')) {
            $hole->setNumber($request->get('_number'));
        }

        $flow = $this->get('andres_gotta_golf.hole.flow');
        $flow->bind($hole);
        $form = $flow->createForm();

        if ($flow->isValid($form)) {
            $flow->saveCurrentStepData($form);

            $em = $this->getDoctrine()->getManager();
            if ($flow->nextStep()) {
                $form = $flow->createForm();
                // Persistimos para disparar listeners (pero no grabamos)
                $em->persist($hole);
            }
            else {
                $em->persist($hole);
                $em->flush();

                $this->addFlash('success', $this->get('translator')->trans('The hole was saved.'));
                $this->updatePlayerScore($hole->getRound());

                /* Si es el último hoyo, mostramos el PostCompetitiveQuestionnaire */
                if (($hole->getRound()->getHoleCount() == 9 && $hole->getRound()->getNextHoleNumber() == 9) || ($hole->getRound()->getHoleCount() == 18 && $hole->getRound()->getNextHoleNumber() == 18)) {
                    $hole->getRound()->setPlayerConfirmed(true);
                    $em->persist($hole);
                    $em->flush();

                    return $this->redirectToRoute('frontend_round_post_competitive_questionnaire', array(
                        'id' => $round->getId(),
                    ));
                } else {
                    return $this->redirectToRoute('frontend_round_show', array(
                        'id' => $round->getId(),
                    ));
                }
            }
        }

        return array(
            'form' => $form->createView(),
            'flow' => $flow,
            'hole' => $hole,
            'round' => $round,
        );
    }

    /**
     * @param $round
     * @return \Symfony\Component\Form\Form
     */
    private function getCreateRoundForm(Round $round)
    {
        $form = $this->createForm(new RoundType(), $round, array(
            'action' => $this->generateUrl('frontend_round_new'),
            'method' => 'POST',
        ));
        $form->add('submit', 'submit', array('label' => 'Create'));

        return $form;
    }

    /**
     * @param $round
     * @return \Symfony\Component\Form\Form
     */
    private function getUpdateRoundForm(Round $round)
    {
        $form = $this->createForm(new RoundType(), $round, array(
            'action' => $this->generateUrl('frontend_round_edit', ['id' => $round->getId()]),
            'method' => 'PUT',
        ));
        $form->add('submit', 'submit', array('label' => 'Update'));

        return $form;
    }

    /**
     * @Route("/round/{id}/delete", name="frontend_round_delete")
     * @Method("GET")
     */
    public function deleteRoundAction(Round $round)
    {
        if (false === $this->get('security.authorization_checker')->isGranted(PlayerServicesVoter::ROLE_ACTIVE_PLAYER, $round)) {
            throw new AccessDeniedException();
        }

        $em = $this->getDoctrine()->getManager();
        $em->remove($round);
        $em->flush();

        $this->updatePlayerScore($round);
        $this->addFlash('success', $this->get('translator')->trans('The operation was successful'));

        return $this->redirectToRoute('frontend_round');
    }

    /**
     * @Route("/hole/{id}/delete", name="frontend_hole_delete")
     * @Method("GET")
     */
    public function deleteHoleAction(Hole $hole)
    {
        if (false === $this->get('security.authorization_checker')->isGranted(PlayerServicesVoter::ROLE_ACTIVE_PLAYER, $hole)) {
            throw new AccessDeniedException();
        }

        $em = $this->getDoctrine()->getManager();
        $em->remove($hole);
        $hole->getRound()->setPlayerConfirmed(false);

        $em->flush();

        $this->updatePlayerScore($hole->getRound());
        $this->addFlash('success', $this->get('translator')->trans('The operation was successful'));

        return $this->redirectToRoute('frontend_round_show', ['id' => $hole->getRound()->getId()]);
    }

    /**
     * @param Round $round
     */
    private function updatePlayerScore(Round $round)
    {
        $event = new RoundEvent($round);
        $this->get('event_dispatcher')->dispatch(GolfEvents::UPDATE_SCORE_EVENT, $event);
    }
    
}
