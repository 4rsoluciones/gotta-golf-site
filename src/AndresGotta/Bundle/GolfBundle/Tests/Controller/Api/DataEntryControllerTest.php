<?php

namespace AndresGotta\Bundle\GolfBundle\Tests\Controller\Api;

use AndresGotta\Bundle\GolfBundle\ValueObject\BallFlight;
use AndresGotta\Bundle\GolfBundle\ValueObject\Club;
use AndresGotta\Bundle\GolfBundle\ValueObject\Distance;
use AndresGotta\Bundle\GolfBundle\ValueObject\HoleDesign;
use AndresGotta\Bundle\GolfBundle\ValueObject\LandSlope;
use AndresGotta\Bundle\GolfBundle\ValueObject\Reason;
use AndresGotta\Bundle\GolfBundle\ValueObject\Result;
use AndresGotta\Bundle\GolfBundle\ValueObject\StrikeLie;
use AndresGotta\Bundle\GolfBundle\ValueObject\StrikeType;
use Symfony\Bundle\FrameworkBundle\Test\WebTestCase;

class DataEntryControllerTest extends WebTestCase
{
    private static $apiMock;

    /**
     *
     */
    public function testAccessToken()
    {
        $client = $this->initializeClient();

        $params = [];
        $accessToken = [
            'HTTP_AUTHORIZATION' => 'Bearer 123',
        ];
        $client->request('GET', '/api/gpstats/player/rounds', $params, [], $accessToken);
        $this->assertTrue($client->getResponse()->isOk(), $client->getResponse());
    }

    protected static function createClient(array $options = array(), array $server = array())
    {
        $client = parent::createClient($options, $server);

        $container = self::$kernel->getContainer();
        $container->set("web_factory_user.gotta_center_api", self::$apiMock);

        return $client;
    }

    /**
     *
     */
    public function testSaveRoundWithoutHolesAction()
    {
        $accessToken = [
            'HTTP_AUTHORIZATION' => 'Bearer 123',
        ];

        $client = $this->initializeClient();
        $params = [];
        $client->request('GET', '/api/gpstats/player/default-fields?country=Argentina&terms=golf', $params, [], $accessToken);
        $this->assertTrue($client->getResponse()->isOk(), $client->getResponse());

        $defaultFields = json_decode($client->getResponse()->getContent(), true);
        $this->assertCount(0, $defaultFields);


        $client = $this->initializeClient();

        $params = [];
        $client->request('GET', '/api/gpstats/player/rounds?incomplete=true', $params, [], $accessToken);
        $this->assertTrue($client->getResponse()->isOk(), $client->getResponse());


        $client = $this->initializeClient();

        $params = [
            'round' => [
                'performed_at' => date('Y-m-d'),
                'player_condition' => 'player.professional',
                'handicap' => 5, // no debe
                'tour' => null,
                'round_type' => 'round.type.tournament',
                'tournament_type' => 18,
                'round_number' => 2,
                'game_condition' => 'clime.windy',
                'played_holes' => 9,
                'field' => [
                    'country' => 'Argentina',
                    'type' => 'field.type.standard',
                    'hole_count' => 18,
                    'par' => 'field_par.equal.at.71',
                    'route' => 'ruta uno',
                    'sport_club_name' => 'Golf club',
                    'start_point' => 'amarillo',
                    'topographic_movements' => 'topographic.movements.minor',
                ],
            ],
        ];
        $client->request('POST', '/api/gpstats/player/round/save', $params, [], $accessToken);
        $this->assertTrue($client->getResponse()->isClientError(), $client->getResponse());


        $client = $this->initializeClient();

        $params = [
            'round' => [
                'performed_at' => date('Y-m-d'),
                'player_condition' => 'player.professional',
//                'handicap' => 5,
                'tour' => null,
                'round_type' => 'round.type.tournament',
                'tournament_type' => 18,
                'round_number' => 2,
                'game_condition' => 'clime.windy',
                'played_holes' => 9,
                'field' => [
                    'country' => 'Argentina',
                    'type' => 'field.type.standard',
                    'hole_count' => 18,
                    'par' => 'field_par.equal.at.71',
                    'route' => 'ruta uno',
                    'sport_club_name' => 'Golf club',
                    'start_point' => 'amarillo',
                    'topographic_movements' => 'topographic.movements.minor',
                ],
            ],
        ];
        $client->request('POST', '/api/gpstats/player/round/save', $params, [], $accessToken);
        $this->assertTrue($client->getResponse()->isOk(), $client->getResponse());

        $round = json_decode($client->getResponse()->getContent(), true);


        $client = $this->initializeClient();
        $params['round']['field']['country'] = 'Chile';
        $client->request('PATCH', '/api/gpstats/player/round/save/' . $round['id'], $params, [], $accessToken);
        $this->assertTrue($client->getResponse()->isOk(), $client->getResponse());


        $client = $this->initializeClient();
        $params = [];
        $client->request('GET', '/api/gpstats/player/round/' . $round['id'], $params, [], $accessToken);
        $this->assertTrue($client->getResponse()->isOk(), $client->getResponse());

        $round = json_decode($client->getResponse()->getContent(), true);
        $this->assertEquals('Chile', $round['field']['country']);


        $client = $this->initializeClient();

        $params = [
            'pre_competitive_questionnaire' => [
                'is_warmed_up2' => true,
                'wants_to_play' => '18',
            ],
        ];

        $client->request('PATCH', '/api/gpstats/player/round/pre-competitive-questionnaire/save/' . $round['id'], $params, [], $accessToken);
        $this->assertTrue($client->getResponse()->isClientError(), $client->getResponse());

        $client = $this->initializeClient();

        $params = [
            'pre_competitive_questionnaire' => [
                'is_warmed_up' => true,
                'wants_to_play' => true,
            ],
        ];

        $client->request('PATCH', '/api/gpstats/player/round/pre-competitive-questionnaire/save/' . $round['id'], $params, [], $accessToken);
        $this->assertTrue($client->getResponse()->isOk(), $client->getResponse());


        $client = $this->initializeClient();

        $params = [
            'post_competitive_questionnaire' => [
                'feeling_status' => 'feeling_status.annoyed2',
            ],
        ];

        $client->request('PUT', '/api/gpstats/player/round/post-competitive-questionnaire/save/' . $round['id'], $params, [], $accessToken);
        $this->assertTrue($client->getResponse()->isClientError(), $client->getResponse());


        $client = $this->initializeClient();

        $params = [
            'post_competitive_questionnaire' => [
                'feeling_status' => 'feeling_status.annoyed',
            ],
        ];

        $client->request('PUT', '/api/gpstats/player/round/post-competitive-questionnaire/save/' . $round['id'], $params, [], $accessToken);
        $this->assertTrue($client->getResponse()->isOk(), $client->getResponse());


        $client = $this->initializeClient();
        $params = [];
        $client->request('DELETE', '/api/gpstats/player/round/delete/' . $round['id'], $params, [], $accessToken);
        $this->assertTrue($client->getResponse()->isOk(), $client->getResponse());


        $client = $this->initializeClient();
        $params = [];
        $client->request('GET', '/api/gpstats/player/tours', $params, [], $accessToken);
        $this->assertTrue($client->getResponse()->isOk(), $client->getResponse());


        $client = $this->initializeClient();
        $params = [];
        $client->request('GET', '/api/gpstats/player/default-fields?country=Argentina&terms=golf', $params, [], $accessToken);
        $this->assertTrue($client->getResponse()->isOk(), $client->getResponse());

        $defaultFields = json_decode($client->getResponse()->getContent(), true);
        $this->assertCount(1, $defaultFields);


        $client = $this->initializeClient();
        $params = [];
        $client->request('GET', '/api/gpstats/player/default-field/' . $defaultFields[0]['id'], $params, [], $accessToken);
        $this->assertTrue($client->getResponse()->isOk(), $client->getResponse());


        $client = $this->initializeClient();

        $params = [
            'default_field' => [
                'country' => 'Argentina',
                'type' => 'field.type.standard',
                'hole_count' => 18,
                'par' => 'field_par.equal.at.71',
                'route' => 'ruta uno',
                'sport_club_name' => 'Golf club',
                'start_point' => 'amarillo',
                'topographic_movements' => 'topographic.movements.minor',
            ],
        ];

        $client->request('POST', '/api/gpstats/player/round/default-field', $params, [], $accessToken);
        $this->assertTrue($client->getResponse()->isClientError(), $client->getResponse());


        $client = $this->initializeClient();

        $params = [
            'default_field' => [
                'country' => 'Bolivia',
                'type' => 'field.type.standard',
                'hole_count' => 18,
                'par' => 'field_par.equal.at.71',
                'route' => 'ruta uno',
                'sport_club_name' => 'Golf club',
                'start_point' => 'amarillo',
                'topographic_movements' => 'topographic.movements.minor',
            ],
        ];

        $client->request('POST', '/api/gpstats/player/round/default-field', $params, [], $accessToken);
        $this->assertTrue($client->getResponse()->isSuccessful(), $client->getResponse());
    }

    /**
     *
     */
    public function testSaveHoleAction()
    {
        $accessToken = [
            'HTTP_AUTHORIZATION' => 'Bearer 123',
        ];

        $client = $this->initializeClient();

        $params = [
            'hole' => [
                'number' => 2,
                'par' => 3,
                'design' => HoleDesign::DOGLEG_LEFT,
                'strikes' => [
                    [
                        'type' => StrikeType::TYPE_FIRST,
                        'distance' => Distance::MORE_THAN_220_YDS,
                        'result' => Result::OUT_OF_LIMITS,
                        'reason' => Reason::TO_THE_LEFT,
                        'ballFlight' => BallFlight::LEFT_TO_RIGHT,
                        'club' => Club::HYBRID,
//                        'strikeLies' => [StrikeLie::ON_CLIMB,],
//                        'landSlopes' => [LandSlope::ON_CLIMB, LandSlope::RIGHT_TO_LEFT,],
                        'emotionalQuestionnaire' => [
                            'feelsUptight' => true,
                            'feelsDistrustful' => true,
                            'feelsAnnoyed' => true,
                            'feelsFrustrated' => true,
                            'feelsTrustful' => true,
                            'feelsEnjoying' => true,
                            'feelsFocused' => true,
                        ],
                        'foodQuestionnaire' => [
                            'drankWater' => true,
                            'drankSportsDrink' => true,
                            'swallowedCerealOrNuts' => true,
                            'swallowedSandwich' => true,
                            'swallowedFruit' => true
                        ],
                        "physicalQuestionnaire" => [
                            'feelsEnergetic' => true,
                            'feelsGettingTired' => true,
                            'feelsPainful' => true,
                            'feelsTired' => true,
                            'feelsExhausted' => true
                        ]

                    ],
                    [
                        'type' => StrikeType::TYPE_OTHER,
                        'distance' => Distance::MORE_THAN_120_YDS,
                        'result' => Result::OUTSIDE_GREEN,
                        'reason' => Reason::TO_THE_LEFT,
                        'ballFlight' => BallFlight::LEFT_TO_RIGHT,
//                        'club' => Club::HYBRID,
                        'strikeLies' => [StrikeLie::ON_CLIMB,],
//                        'landSlopes' => [LandSlope::ON_CLIMB, LandSlope::RIGHT_TO_LEFT,],
                        'emotionalQuestionnaire' => [
                            'feelsUptight' => true,
                            'feelsDistrustful' => true,
                            'feelsAnnoyed' => true,
                            'feelsFrustrated' => true,
                            'feelsTrustful' => true,
                            'feelsEnjoying' => true,
                            'feelsFocused' => true,
                        ],
                        'foodQuestionnaire' => [
                            'drankWater' => true,
                            'drankSportsDrink' => true,
                            'swallowedCerealOrNuts' => true,
                            'swallowedSandwich' => true,
                            'swallowedFruit' => true
                        ],
                        "physicalQuestionnaire" => [
                            'feelsEnergetic' => true,
                            'feelsGettingTired' => true,
                            'feelsPainful' => true,
                            'feelsTired' => true,
                            'feelsExhausted' => true
                        ]

                    ],
                    [
                        'type' => StrikeType::TYPE_OTHER,
                        'distance' => Distance::LESS_THAN_20_YDS,
                        'result' => Result::INSIDE_GREEN,
                        'reason' => null,
                        'ballFlight' => BallFlight::LEFT_TO_RIGHT,
//                        'club' => Club::HYBRID,
                        'strikeLies' => [StrikeLie::ON_CLIMB,],
//                        'landSlopes' => [LandSlope::ON_CLIMB, LandSlope::RIGHT_TO_LEFT,],
                        'emotionalQuestionnaire' => [
                            'feelsUptight' => true,
                            'feelsDistrustful' => true,
                            'feelsAnnoyed' => true,
                            'feelsFrustrated' => true,
                            'feelsTrustful' => true,
                            'feelsEnjoying' => true,
                            'feelsFocused' => true,
                        ],
                        'foodQuestionnaire' => [
                            'drankWater' => true,
                            'drankSportsDrink' => true,
                            'swallowedCerealOrNuts' => true,
                            'swallowedSandwich' => true,
                            'swallowedFruit' => true
                        ],
                        "physicalQuestionnaire" => [
                            'feelsEnergetic' => true,
                            'feelsGettingTired' => true,
                            'feelsPainful' => true,
                            'feelsTired' => true,
                            'feelsExhausted' => true
                        ]

                    ],
                    [
                        'type' => StrikeType::TYPE_PUTT,
                        'distance' => Distance::BTW_3_TO_5_MTS,
                        'result' => Result::HOLE_OUT,
                        'reason' => null,
//                        'ballFlight' => BallFlight::LEFT_TO_RIGHT,
//                        'club' => Club::HYBRID,
//                        'strikeLies' => [StrikeLie::ON_CLIMB,],
                        'landSlopes' => [LandSlope::ON_CLIMB, LandSlope::RIGHT_TO_LEFT,],
                        'emotionalQuestionnaire' => [
                            'feelsUptight' => true,
                            'feelsDistrustful' => true,
                            'feelsAnnoyed' => true,
                            'feelsFrustrated' => true,
                            'feelsTrustful' => true,
                            'feelsEnjoying' => true,
                            'feelsFocused' => true,
                        ],
                        'foodQuestionnaire' => [
                            'drankWater' => true,
                            'drankSportsDrink' => true,
                            'swallowedCerealOrNuts' => true,
                            'swallowedSandwich' => true,
                            'swallowedFruit' => true
                        ],
                        "physicalQuestionnaire" => [
                            'feelsEnergetic' => true,
                            'feelsGettingTired' => true,
                            'feelsPainful' => true,
                            'feelsTired' => true,
                            'feelsExhausted' => true
                        ]

                    ],
                ],
                'penalityShotsCount' => 3,
                'description' => null,
            ]
        ];

        $client->request('POST', '/api/gpstats/player/round/2/hole', $params, [], $accessToken);
        $this->assertTrue($client->getResponse()->isSuccessful(), $client->getResponse());

        $client = $this->initializeClient();
        $client->request('GET', '/api/gpstats/player/default-holes', $params, [], $accessToken);
        $this->assertTrue($client->getResponse()->isSuccessful(), $client->getResponse());
        $defaultHoles = json_decode($client->getResponse()->getContent(), true);

        $this->assertTrue(count($defaultHoles) > 0);
    }

    public function testCreateDefaultHole()
    {
        $accessToken = [
            'HTTP_AUTHORIZATION' => 'Bearer 123',
        ];

        $client = $this->initializeClient();
        $params = [];
        $client->request('GET', '/api/gpstats/player/default-fields?country=Argentina&terms=golf', $params, [], $accessToken);
        $this->assertTrue($client->getResponse()->isOk(), $client->getResponse());

        $defaultFields = json_decode($client->getResponse()->getContent(), true);
        $this->assertCount(1, $defaultFields);
        $defaultField = $defaultFields[0];

        $client = $this->initializeClient();

        $params = [
            'default_hole' => [
                'number' => 2,
                'par' => 3,
                'design' => HoleDesign::DOGLEG_LEFT,

                'country' => $defaultField['country'],
                'route' => $defaultField['route'],
                'sportClubName' => $defaultField['sport_club_name'],
                'startPoint' => $defaultField['start_point'] . ' no valido',
            ]
        ];

        $client->request('POST', '/api/gpstats/player/round/default-hole', $params, [], $accessToken);
        $this->assertTrue($client->getResponse()->isClientError(), $client->getResponse());

        $client = $this->initializeClient();

        $params = [
            'default_hole' => [
                'number' => 120,
                'par' => 3,
                'design' => HoleDesign::DOGLEG_LEFT,
                'country' => $defaultField['country'],
                'route' => $defaultField['route'],
                'sportClubName' => $defaultField['sport_club_name'],
                'startPoint' => $defaultField['start_point'],
            ]
        ];

        $client->request('POST', '/api/gpstats/player/round/default-hole', $params, [], $accessToken);
        $this->assertTrue($client->getResponse()->isOk(), $client->getResponse());
    }


    public function testGetComparative()
    {
        $accessToken = [
            'HTTP_AUTHORIZATION' => 'Bearer 123',
        ];

        $client = $this->initializeClient();

        $params = [];

        $client->request('GET', '/api/gpstats/player/comparatives', $params, [], $accessToken);
        $this->assertTrue($client->getResponse()->isOk(), $client->getResponse());

        $comparatives = json_decode($client->getResponse()->getContent(), true);

        $this->assertCount(7, $comparatives);
        $this->assertNotEmpty($comparatives['first_strike_precision']);
        $this->assertEquals(100, $comparatives['first_strike_precision']['results']['player']['y_axis']);//100% de acierto (hoyos en uno)
//        $this->assertEquals(-99.200000000000003, $comparatives['green_in_regulation']['results'][0]['y_axis']);
        $this->assertEquals(100, $comparatives['scrambling']['results'][0]['y_axis']);
        $this->assertEquals(null, $comparatives['putts_average']['results'][0]['y_axis']);
        $this->assertEquals(0, $comparatives['green_frequency_three_putts']['results'][0]['y_axis']);
        $this->assertEquals(0, $comparatives['green_frequency_one_putt']['results'][0]['y_axis']);
        $this->assertEquals(0, $comparatives['gross_average']['results'][0]['y_axis']);
    }

    public function testProfile()
    {
        $accessToken = [
            'HTTP_AUTHORIZATION' => 'Bearer 123',
        ];

        $client = $this->initializeClient();
        $params = [];
        $client->request('GET', '/api/gpstats/player/profile', $params, [], $accessToken);
        $this->assertTrue($client->getResponse()->isOk(), $client->getResponse());

        $profile = json_decode($client->getResponse()->getContent(), true);

        $client = $this->initializeClient();

        $params = [
            'profile' => [
                'handicap' => 2,
                'country' => 'Argentinas',
            ]
        ];

        $client->request('PATCH', '/api/gpstats/player/profile', $params, [], $accessToken);
        $this->assertTrue($client->getResponse()->isClientError(), $client->getResponse());


        $client = $this->initializeClient();

        $params = [
            'profile' => [
                'handicap' => 2,
                'country' => 'Argentina',
            ]
        ];

        $client->request('PATCH', '/api/gpstats/player/profile', $params, [], $accessToken);
        $this->assertTrue($client->getResponse()->isOk(), $client->getResponse());
    }

    /**
     * @return \Symfony\Bundle\FrameworkBundle\Client
     */
    private function initializeClient()
    {
        self::$apiMock = $this->getMockBuilder("WebFactory\Bundle\UserBundle\Service\GottaCenterApi")
            ->disableOriginalConstructor()
            ->getMock();

        self::$apiMock
            ->expects($this->once())
            ->method("getMe")
            ->will($this->returnValue([
                'success' => [
                    'username' => 'player@example.com',
                    'email' => 'player@example.com',
                    'password' => 'player@example.com',
                    'salt' => 'player@example.com',
                    'accessToken' => '',
                    'created_at' => '12-12-12',
                    'updated_at' => '12-12-12',
                    'profile' => [
                        'first_name' => '',
                        'last_name' => '',
                        'state_id' => '1',
                        'city' => '',
                        'company_name' => '',
                        'gender' => '',
                        'mobile_phonenumber' => '',
                        'birth_date' => '',
                        'enabled' => true,
                        'roles' => 'a:1:{i:0;s:11:"ROLE_PLAYER";}',
                    ],
                ],
            ]));


        $client = static::createClient();
        return $client;
    }
}
