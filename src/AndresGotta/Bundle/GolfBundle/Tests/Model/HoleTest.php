<?php

namespace AndresGotta\Bundle\GolfBundle\Tests\Model;

use AndresGotta\Bundle\GolfBundle\Entity\Round;
use AndresGotta\Bundle\GolfBundle\Entity\Strike;
use AndresGotta\Bundle\GolfBundle\ValueObject\BallFlight;
use AndresGotta\Bundle\GolfBundle\ValueObject\Distance;
use AndresGotta\Bundle\GolfBundle\ValueObject\HoleDesign;
use AndresGotta\Bundle\GolfBundle\ValueObject\LandSlope;
use AndresGotta\Bundle\GolfBundle\ValueObject\Reason;
use AndresGotta\Bundle\GolfBundle\ValueObject\Result;
use AndresGotta\Bundle\GolfBundle\ValueObject\StrikeLie;
use AndresGotta\Bundle\GolfBundle\ValueObject\StrikeType;
use Symfony\Bundle\FrameworkBundle\Test\WebTestCase;

class HoleTest extends WebTestCase
{
    public function testDefault()
    {
        $client = static::createClient();
        $container = $client->getContainer();

        $validator = $container->get('validator');
        $manager = $container->get('doctrine.orm.entity_manager');
        /* @var Round $round*/
        $round = $manager->getRepository('AndresGottaGolfBundle:Round')->find(2);
        $this->assertTrue($round instanceof Round);

        $hole = $round->createNewHole();
        $round->addHole($hole);

        $hole->setPar(3);
        $hole->setDesign(HoleDesign::DOGLEG_LEFT);
        $hole->setNumber(6);
        $hole->setPenalityShotsCount(0);
        $this->assertEquals(1, $validator->validate($hole)->count(), (string)$validator->validate($hole));

        $firstStrike = new Strike();
        $firstStrike->setType(StrikeType::TYPE_FIRST);
        $firstStrike->setHole($hole);
        $firstStrike->setBallFlight(BallFlight::LEFT_TO_RIGHT);

        $firstStrike->setStrikeLies([StrikeLie::ON_DESCEND]);
        $firstStrike->setLandSlopes([LandSlope::ON_CLIMB]);

        $firstStrike->setDistance(Distance::BTW_121_TO_140_YDS);
        $firstStrike->setResult(Result::OUT_OF_LIMITS);
        $firstStrike->setReason(Reason::TO_THE_RIGHT);

        $this->assertEquals(0, $validator->validate($firstStrike)->count(), (string)$validator->validate($firstStrike));

        $hole->addStrike($firstStrike);

        $firstStrikeApproach = new Strike();
        $firstStrikeApproach->setType(StrikeType::TYPE_OTHER);
        $firstStrikeApproach->setHole($hole);
        $firstStrikeApproach->setDistance(Distance::MORE_THAN_120_YDS);
        $firstStrikeApproach->setResult(Result::BALL_IN_HAZARD);
        $firstStrikeApproach->setReason(Reason::SHORT);
        $firstStrikeApproach->setStrikeLies([StrikeLie::BALL_AT_YOUR_FEET]);

        $firstStrikeApproach->setBallFlight(BallFlight::LEFT_TO_RIGHT);
        $firstStrikeApproach->setLandSlopes([LandSlope::ON_CLIMB]);

        $hole->addStrike($firstStrikeApproach);


        $this->assertEquals(0, $validator->validate($firstStrikeApproach)->count(), (string)$validator->validate($firstStrikeApproach));
        $this->assertTrue($hole->getStrikes()->first() instanceof Strike);
        $this->assertEquals(2, $hole->getStrikes()->count());


        $firstStrikeApproach = new Strike();
        $firstStrikeApproach->setType(StrikeType::TYPE_OTHER);
        $firstStrikeApproach->setHole($hole);
        $firstStrikeApproach->setDistance(Distance::BTW_221_TO_240_YDS);
        $firstStrikeApproach->setResult(Result::OUTSIDE_GREEN);
        $firstStrikeApproach->setReason(Reason::SHORT);
        $firstStrikeApproach->setStrikeLies([StrikeLie::BALL_AT_YOUR_FEET]);

        $hole->addStrike($firstStrikeApproach);

        $this->assertEquals(3, $hole->getStrikes()->count());

        $secondStrike = new Strike();
        $secondStrike->setType(StrikeType::TYPE_OTHER);
        $secondStrike->setHole($hole);
        $secondStrike->setDistance(Distance::BTW_100_TO_120_YDS);
        $secondStrike->setBallFlight(BallFlight::LEFT_TO_RIGHT);
        $secondStrike->setResult(Result::INSIDE_GREEN);

        $hole->addStrike($secondStrike);

        $putt = new Strike();
        $putt->setType(StrikeType::TYPE_PUTT);
        $putt->setHole($hole);
        $putt->setDistance(Distance::BTW_3_TO_5_MTS);
        $putt->setResult(Result::OUT_OF_LIMITS);
        $putt->setReason(Reason::SHORT);

        $putt->setStrikeLies([StrikeLie::ON_DESCEND]);
        $putt->setBallFlight(BallFlight::RIGHT_TO_LEFT);

        $putt->setLandSlopes([LandSlope::PLANE, LandSlope::STRAIGHT]);

        $this->assertEquals(0, $validator->validate($putt)->count(), (string)$validator->validate($putt));

        $hole->addStrike($putt);

        $puttApproach = new Strike();
        $puttApproach->setType(StrikeType::TYPE_PUTT);
        $puttApproach->setHole($hole);
        $puttApproach->setDistance(Distance::BTW_3_TO_5_MTS);
        $puttApproach->setResult(Result::MISSED);
        $puttApproach->setReason(Reason::SHORT);
        $puttApproach->setLandSlopes([LandSlope::PLANE, LandSlope::STRAIGHT]);

        $puttApproach->setBallFlight(BallFlight::LEFT_TO_RIGHT);
        $puttApproach->setStrikeLies([StrikeLie::ON_DESCEND]);

        $this->assertEquals(0, $validator->validate($puttApproach)->count(), (string)$validator->validate($puttApproach));

        $hole->addStrike($puttApproach);

        $puttApproach = new Strike();
        $puttApproach->setType(StrikeType::TYPE_PUTT);
        $puttApproach->setHole($hole);
        $puttApproach->setDistance(Distance::BTW_1_TO_2_MTS);
        $puttApproach->setResult(Result::OUT_OF_LIMITS);
        $puttApproach->setReason(Reason::SHORT);

        $puttApproach->setBallFlight(BallFlight::LEFT_TO_RIGHT);
        $puttApproach->setStrikeLies([StrikeLie::ON_DESCEND]);

        $puttApproach->setLandSlopes([LandSlope::PLANE, LandSlope::STRAIGHT]);

        $this->assertEquals(0, $validator->validate($puttApproach)->count(), (string)$validator->validate($puttApproach));

        $hole->addStrike($puttApproach);

        $puttApproach = new Strike();
        $puttApproach->setType(StrikeType::TYPE_PUTT);
        $puttApproach->setHole($hole);
        $puttApproach->setDistance(Distance::BTW_1_TO_2_MTS);
        $puttApproach->setResult(Result::HOLE_OUT);
        $puttApproach->setReason(null);

        $puttApproach->setBallFlight(BallFlight::LEFT_TO_RIGHT);
        $puttApproach->setStrikeLies([StrikeLie::ON_DESCEND]);

        $puttApproach->setLandSlopes([LandSlope::PLANE, LandSlope::STRAIGHT]);

        $this->assertEquals(0, $validator->validate($puttApproach)->count(), (string)$validator->validate($puttApproach));
    }

}