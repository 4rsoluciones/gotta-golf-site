<?php

namespace AndresGotta\Bundle\GolfBundle\Repository;

use Doctrine\ORM\EntityRepository;

/**
 * Report Repository
 */
class ReportRepository extends EntityRepository
{

    public function findAllByIds(array $ids)
    {
        if (!$ids) {
            return array();
        }

        return $this->createQueryBuilder('Report')
                        ->where('Report.id in (:ids)')
                        ->setParameter('ids', $ids)
                        ->getQuery()
                        ->getResult()
        ;
    }

}
