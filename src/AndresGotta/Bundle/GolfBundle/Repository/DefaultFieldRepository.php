<?php

namespace AndresGotta\Bundle\GolfBundle\Repository;


use AndresGotta\Bundle\GolfBundle\Entity\AbstractField;
use Doctrine\ORM\EntityRepository;

class DefaultFieldRepository extends EntityRepository implements DefaultFieldRepositoryInterface
{
    /**
     * @inheritdoc
     */
    public function findOneByFieldIdentifiers($country, $route, $sportClubName, $startPoint)
    {
        $qb = $this->createQueryBuilder('DefaultField');
        $qb
            ->andWhere('DefaultField.country = :country')->setParameter('country', $country)
            ->andWhere('DefaultField.route = :route')->setParameter('route', $route)
            ->andWhere('DefaultField.sportClubName = :sportClubName')->setParameter('sportClubName', $sportClubName)
            ->andWhere('DefaultField.startPoint = :startPoint')->setParameter('startPoint', $startPoint)
        ;

        return $qb->getQuery()->getOneOrNullResult();
    }

    /**
     * @inheritdoc
     */
    public function findOneByField(AbstractField $field)
    {
        return $this->findOneByFieldIdentifiers($field->getCountry(), $field->getRoute(), $field->getSportClubName(), $field->getStartPoint());
    }

    /**
     * @inheritdoc
     */
    public function findByCountryAndTerms($country, $terms)
    {
        $qb = $this->createQueryBuilder('DefaultField');
        if ($country) {
            $qb
                ->andWhere('DefaultField.country = :country')
                ->setParameter('country', $country)
            ;
        }

        if ($terms) {
            $termsExpression = $qb->expr()->orX(
                'DefaultField.route LIKE :terms',
                'DefaultField.sportClubName LIKE :terms',
                'DefaultField.startPoint LIKE :terms'
            );
            $qb
                ->andWhere($termsExpression)
                ->setParameter('terms', "%{$terms}%")
            ;
        }

        return $qb->getQuery()->getResult();
    }
}