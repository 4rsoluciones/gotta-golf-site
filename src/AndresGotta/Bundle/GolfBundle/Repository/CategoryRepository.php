<?php

namespace AndresGotta\Bundle\GolfBundle\Repository;

use Doctrine\ORM\EntityRepository;

/**
 * Class CategoryRepository
 * @package AndresGotta\Bundle\GolfBundle\Repository
 */
class CategoryRepository extends EntityRepository
{
    /**
     * @return array
     */
    public function findChildCategories()
    {
        return $this->createQueryBuilder('c')
            ->where('c.parent IS NOT NULL')
            ->getQuery()
            ->getResult()
        ;
    }

}
