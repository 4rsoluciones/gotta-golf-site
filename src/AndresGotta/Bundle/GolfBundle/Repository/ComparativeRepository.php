<?php

namespace AndresGotta\Bundle\GolfBundle\Repository;

use AndresGotta\Bundle\GolfBundle\Entity\Comparative;
use Doctrine\ORM\EntityRepository;

/**
 * Class ComparativeRepository
 * @package AndresGotta\Bundle\GolfBundle\Repository
 */
class ComparativeRepository extends EntityRepository implements ComparativeRepositoryInterface
{
    public function findGeneralComparativeValues()
    {

    }

    /**
     * Busca los que están disponibles para un reporte
     *
     * @param $reportType
     */
    public function findByReportType($reportType)
    {

    }

    /**
     * Busca todos a los que puede acceder el usuario
     *
     * @param $user
     */
    public function findVisibleForUser($user)
    {

    }

    /**
     * @return Comparative[]
     */
    public function findAllHighlighted()
    {
        return $this->findBy(['highlighted' => true], ['sort' => 'desc']);
    }


}