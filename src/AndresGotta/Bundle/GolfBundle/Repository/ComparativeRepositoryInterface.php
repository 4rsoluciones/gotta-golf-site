<?php

namespace AndresGotta\Bundle\GolfBundle\Repository;

use AndresGotta\Bundle\GolfBundle\Entity\Comparative;

interface ComparativeRepositoryInterface
{
    /**
     * @return Comparative[]
     */
    public function findAllHighlighted();
}