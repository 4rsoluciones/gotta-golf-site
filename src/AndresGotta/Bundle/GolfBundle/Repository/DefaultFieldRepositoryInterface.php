<?php

namespace AndresGotta\Bundle\GolfBundle\Repository;

use AndresGotta\Bundle\GolfBundle\Entity\AbstractField;
use AndresGotta\Bundle\GolfBundle\Entity\DefaultField;

interface DefaultFieldRepositoryInterface
{

    /**
     * @param $country
     * @param $route
     * @param $sportClubName
     * @param $startPoint
     * @return DefaultField|null
     * @throws \Doctrine\ORM\NonUniqueResultException
     */
    public function findOneByFieldIdentifiers($country, $route, $sportClubName, $startPoint);
    /**
     * @param AbstractField $field
     * @return DefaultField|null
     * @throws \Doctrine\ORM\NonUniqueResultException
     */
    public function findOneByField(AbstractField $field);

    /**
     * @param $country
     * @param $terms
     * @return DefaultField[]
     */
    public function findByCountryAndTerms($country, $terms);
}