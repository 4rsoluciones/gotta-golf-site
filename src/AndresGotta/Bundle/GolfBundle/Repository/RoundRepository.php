<?php

namespace AndresGotta\Bundle\GolfBundle\Repository;

use Doctrine\ORM\EntityRepository;
use Symfony\Component\Security\Core\User\UserInterface;

/**
 * RoundRepository
 */
class RoundRepository extends EntityRepository
{

    public function createQueryBuilderByPlayer(UserInterface $user)
    {
        $queryBuilder = $this->createQueryBuilder('Round')
                ->where('Round.player = :player')
                ->setParameter('player', $user)
                ->orderBy('Round.performedAt', 'desc')
        ;

        return $queryBuilder;
    }
    
    public function getRoundCount()
    {
        $result = $this->createQueryBuilder('Round')
                ->select('COUNT(Round.id)')
                ->getQuery()
                ->getSingleScalarResult();
        
        return $result;
    }

}
