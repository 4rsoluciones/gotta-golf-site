<?php

namespace AndresGotta\Bundle\GolfBundle\Repository;

use AndresGotta\Bundle\GolfBundle\Entity\Round;
use Doctrine\ORM\EntityRepository;

/**
 * HoleRepository
 */
class HoleRepository extends EntityRepository
{

    public function getHoleCount()
    {
        $qb = $this->createQueryBuilder('Hole');
        $result = $qb->select('COUNT(DISTINCT Hole.id) as quantity')
                ->getQuery()
                ->getScalarResult();
        
        return $result[0]['quantity'];
    }

    /**
     * @param Round $round
     * @param $number
     * @return mixed
     * @throws \Doctrine\ORM\NonUniqueResultException
     */
    public function findOneByRoundAndNumber(Round $round, $number)
    {
        return $this
            ->createQueryBuilder('Hole')
            ->andWhere('Hole.round = :round')->setParameter('round', $round)
            ->andWhere('Hole.number = :number')->setParameter('number', $number)
            ->getQuery()->getOneOrNullResult();
    }

}
