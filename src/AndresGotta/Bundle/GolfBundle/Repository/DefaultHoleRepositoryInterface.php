<?php

namespace AndresGotta\Bundle\GolfBundle\Repository;

use AndresGotta\Bundle\GolfBundle\Entity\AbstractField;
use AndresGotta\Bundle\GolfBundle\Entity\DefaultField;
use AndresGotta\Bundle\GolfBundle\Entity\DefaultHole;
use AndresGotta\Bundle\GolfBundle\Entity\Hole;

interface DefaultHoleRepositoryInterface
{
    /**
     * @param $country
     * @param $route
     * @param $sportClubName
     * @param $startPoint
     * @return DefaultHole[]
     */
    public function findByDefaultField($country, $route, $sportClubName, $startPoint);

    /**
     * @param Hole $hole
     * @return DefaultHole
     */
    public function findOneByHole(Hole $hole);
}