<?php

namespace AndresGotta\Bundle\GolfBundle\Repository;

use AndresGotta\Bundle\GolfBundle\Entity\Player;
use AndresGotta\Bundle\GolfBundle\ValueObject\Email;
use Doctrine\ORM\EntityRepository;

class PlayerRepository extends EntityRepository
{
    /**
     * @param Email $email
     * @return Player
     */
    public function getByEmail(Email $email) {
        return $this->findOneBy(['email.address' => $email->getAddress()]);
    }

    /**
     * @param Player $player
     * @throws \Doctrine\ORM\OptimisticLockException
     */
    public function add(Player $player) {
        $em = $this->getEntityManager();

        $em->persist($player);
        $em->flush();
    }
}