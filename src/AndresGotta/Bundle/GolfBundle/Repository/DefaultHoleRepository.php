<?php

namespace AndresGotta\Bundle\GolfBundle\Repository;

use AndresGotta\Bundle\GolfBundle\Entity\DefaultHole;
use AndresGotta\Bundle\GolfBundle\Entity\Hole;
use Doctrine\ORM\EntityRepository;

class DefaultHoleRepository extends EntityRepository implements DefaultHoleRepositoryInterface
{
    /**
     * @inheritdoc
     */
    public function findByDefaultField($country, $route, $sportClubName, $startPoint)
    {
        $qb = $this->createQueryBuilder('DefaultHole');
        $qb->innerJoin('DefaultHole.defaultField', 'DefaultField');
        if ($country) {
            $qb->andWhere('DefaultField.country = :country')->setParameter('country', $country);
        }
        if ($route) {
            $qb->andWhere('DefaultField.route = :route')->setParameter('route', $route);
        }
        if ($sportClubName) {
            $qb->andWhere('DefaultField.sportClubName = :sportClubName')->setParameter('sportClubName', $sportClubName);
        }
        if ($startPoint) {
            $qb->andWhere('DefaultField.startPoint = :startPoint')->setParameter('startPoint', $startPoint);
        }

        return $qb->getQuery()->getResult();
    }

    /**
     * @param Hole $hole
     * @return DefaultHole
     * @throws \Doctrine\ORM\NonUniqueResultException
     */
    public function findOneByHole(Hole $hole)
    {
        $defaultField = $hole->getRound()->getField()->getDefaultField();
        if (!$defaultField) {
            throw new \DomainException('Default field for round is hole field not found');
        }
        $country = $defaultField->getCountry();
        $route = $defaultField->getRoute();
        $sportClubName = $defaultField->getSportClubName();
        $startPoint = $defaultField->getStartPoint();

        $qb = $this->createQueryBuilder('DefaultHole');
        $qb
            ->innerJoin('DefaultHole.defaultField', 'DefaultField')
            ->andWhere('DefaultField.country = :country')->setParameter('country', $country)
            ->andWhere('DefaultField.route = :route')->setParameter('route', $route)
            ->andWhere('DefaultField.sportClubName = :sportClubName')->setParameter('sportClubName', $sportClubName)
            ->andWhere('DefaultField.startPoint = :startPoint')->setParameter('startPoint', $startPoint)
            ->andWhere('DefaultHole.number = :number')->setParameter('number', $hole->getNumber())
            ->andWhere('DefaultHole.par = :par')->setParameter('par', $hole->getPar())
            ->andWhere('DefaultHole.design = :design')->setParameter('design', $hole->getDesign())
        ;

        return $qb->getQuery()->getOneOrNullResult();
    }
}