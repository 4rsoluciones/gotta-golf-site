<?php

namespace AndresGotta\Bundle\GolfBundle\Event;

/**
 * Class GolfEvents
 * @package AndresGotta\Bundle\GolfBundle\Event
 */
class GolfEvents
{
    /**
     * Cuando se elimina una vuelta
     */
    const UPDATE_SCORE_EVENT = 'update.score';
}