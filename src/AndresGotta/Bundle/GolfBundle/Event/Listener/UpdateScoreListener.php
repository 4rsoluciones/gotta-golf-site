<?php

namespace AndresGotta\Bundle\GolfBundle\Event\Listener;

use AndresGotta\Bundle\GolfBundle\Event\RoundEvent;
use Doctrine\ORM\EntityManager;
use WebFactory\Bundle\UserBundle\Manager\ScoreManager;

/**
 * Class UpdateScoreListener
 * @package AndresGotta\Bundle\GolfBundle\Event\Listener
 */
class UpdateScoreListener
{
    /**
     * @var ScoreManager
     */
    protected $scoreManager;

    /**
     * @var EntityManager
     */
    protected $entityManager;

    /**
     * UpdateScoreListener constructor.
     * @param ScoreManager $scoreManager
     * @param EntityManager $entityManager
     */
    public function __construct(ScoreManager $scoreManager, EntityManager $entityManager)
    {
        $this->scoreManager = $scoreManager;
        $this->entityManager = $entityManager;
    }

    /**
     * @param RoundEvent $event
     * @throws \Doctrine\ORM\OptimisticLockException
     */
    public function onUpdateScore(RoundEvent $event)
    {
        $player = $event->getRound()->getPlayer();

        $repo = $this->entityManager->getRepository('AndresGottaGolfBundle:Round');//No considera los de 9?
        $rounds = $repo->findBy(['player' => $player->getId(), 'holeCount' => 18, 'playerConfirmed' => true]);

        $score = $this->scoreManager->calculate($rounds);
        $player->getProfile()->setScore($score);

        $this->entityManager->persist($player);
        $this->entityManager->flush();
    }
}