<?php

namespace AndresGotta\Bundle\GolfBundle\Event;

use AndresGotta\Bundle\GolfBundle\Entity\Round;
use Symfony\Component\EventDispatcher\Event;

/**
 * Class RoundEvent
 * @package AndresGotta\Bundle\GolfBundle\Event
 */
class RoundEvent extends Event
{
    /**
     * @var Round
     */
    private $round;

    /**
     * RoundEvent constructor.
     * @param Round $round
     */
    public function __construct(Round $round)
    {
        $this->round = $round;
    }

    /**
     * @return Round
     */
    public function getRound()
    {
        return $this->round;
    }

}