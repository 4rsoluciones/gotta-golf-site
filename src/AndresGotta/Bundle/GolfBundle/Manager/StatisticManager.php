<?php

namespace AndresGotta\Bundle\GolfBundle\Manager;

use AndresGotta\Bundle\GolfBundle\Entity\Report;
use AndresGotta\Bundle\GolfBundle\Report\ReportBuilder;
use AndresGotta\Bundle\GolfBundle\Report\ReportStrategyFactory;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Component\Security\Core\SecurityContextInterface;

class StatisticManager
{

    /**
     *
     * @var SecurityContextInterface
     */
    protected $securityContext;

    /**
     *
     * @var EntityManagerInterface
     */
    protected $entityManager;

    /**
     * Constructor
     *
     * @param SecurityContextInterface $securityContext
     * @param EntityManagerInterface $entityManager
     */
    public function __construct(SecurityContextInterface $securityContext, EntityManagerInterface $entityManager)
    {
        $this->securityContext = $securityContext;
        $this->entityManager = $entityManager;
    }

    /**
     * Get a user from the Security Context
     *
     * @return mixed
     */
    private function getUser()
    {
        if (null === $token = $this->securityContext->getToken()) {
            return null;
        }

        if (!is_object($user = $token->getUser())) {
            return null;
        }

        return $user;
    }

    /**
     * Se encarga de generar el reporte solicitado
     *
     * @param string $report
     * @return array
     */
    public function generate(Report $report, $filters = array())
    {

        $queryBuilder = $this->entityManager->createQueryBuilder();

        if (!isset($filters['user'])) {
            $filters['user'] = $this->getUser();
        }

        $strategy = ReportStrategyFactory::create($report, $queryBuilder, $filters);
        $reportBuilder = new ReportBuilder($report, $strategy);

        return $reportBuilder->generate();
    }

}
