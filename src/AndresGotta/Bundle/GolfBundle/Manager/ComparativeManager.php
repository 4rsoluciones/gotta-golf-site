<?php

namespace AndresGotta\Bundle\GolfBundle\Manager;

use AndresGotta\Bundle\GolfBundle\Comparative\Groups;
use AndresGotta\Bundle\GolfBundle\Comparative\BaseType;
use AndresGotta\Bundle\GolfBundle\Entity\Comparative;
use AndresGotta\Bundle\GroupBundle\Entity\Group;
use AndresGotta\Bundle\GroupBundle\Entity\GroupPlayer;
use AndresGotta\Bundle\GroupBundle\ValueObject\PlayerStatuses;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Util\Inflector;
use Doctrine\ORM\EntityManager;
use Symfony\Component\Security\Core\Authentication\Token\Storage\TokenStorage;
use Symfony\Component\Translation\TranslatorInterface;
use WebFactory\Bundle\UserBundle\Entity\User;

/**
 * Class ComparativeManager
 * @package AndresGotta\Bundle\GolfBundle\Manager
 */
class ComparativeManager implements ComparativeManagerInterface
{
    /**
     * @var TokenStorage
     */
    private $tokenStorage;

    /**
     * @var EntityManager
     */
    private $entityManager;

    /**
     * @var TranslatorInterface
     */
    private $translator;

    /**
     * ComparativeManager constructor.
     * @param TokenStorage $tokenStorage
     * @param EntityManager $entityManager
     * @param TranslatorInterface $translator
     */
    public function __construct(TokenStorage $tokenStorage, EntityManager $entityManager, TranslatorInterface $translator)
    {
        $this->tokenStorage = $tokenStorage;
        $this->entityManager = $entityManager;
        $this->translator = $translator;
    }

    /**
     * @inheritdoc
     */
    public function getComparativeResults(Comparative $comparative, User $user = null)
    {
        /** @var User $player */
        $player = $user ? $user : $this->tokenStorage->getToken()->getUser();
        $score = $player->getProfile()->getScore();

        /** @var BaseType $comparativeType */
        $comparativeType = $this->factoryClassGuesser($comparative->getSlug());
        $comparativeType->setPlayerToCompare($player);

        list($min, $max) = Groups::getGroupByScore($score);
        $min = !$min ? 0 : $min;

        $repo = $this->entityManager->getRepository('WebFactoryUserBundle:Profile');
        $queryBuilder = $repo->createQueryBuilder('p');
        $queryBuilder
            ->select('p.id, p.score')
            ->leftJoin('p.user', 'user')
            ->where($queryBuilder->expr()->like('user.roles', $queryBuilder->expr()->literal('%'. User::ROLE_PLAYER . '%')))
            ->andWhere('p.gender = :gender')->setParameter('gender',  $player->getProfile()->getGender())
        ;

        if (!$max) {
            $queryBuilder
                ->andWhere( 'p.score > :min')
                ->setParameter('min', $min)
            ;
        } else {
            $queryBuilder
                ->andWhere( 'p.score BETWEEN :min AND :max')
                ->setParameter('min', $min)
                ->setParameter('max', $max)
            ;
        }

        $otherPlayers = $queryBuilder->getQuery()
//            ->useQueryCache(true)
//            ->useResultCache(true)
            ->getResult();

        $collection = new ArrayCollection($otherPlayers);
        $comparativeType->setOthersPlayersToCompare($collection);
        $values = $comparativeType->calculateValues();

        return $values;
    }

    /**
     * @param Comparative $comparative
     * @param Group $group
     * @return \AndresGotta\Bundle\GolfBundle\Comparative\ChartGroup
     */
    public function getComparativeResultsForGroup(Comparative $comparative, Group $group, User $user = null)
    {
        /** @var User $player */
        $player = $user ? $user : $this->tokenStorage->getToken()->getUser();

        /** @var BaseType $comparativeType */
        $comparativeType = $this->factoryClassGuesser($comparative->getSlug());
        $comparativeType->setPlayerToCompare($player);

        $collection = new ArrayCollection();
        /** @var GroupPlayer $groupPlayer */
        foreach ($group->getPlayers() as $groupPlayer) {
            if ($groupPlayer->getStatus() === PlayerStatuses::CONFIRMED) {
                $collection->add($groupPlayer->getPlayer()->getProfile()->getId());
            }
        }
        $comparativeType->setOthersPlayersToCompare($collection);
        $values = $comparativeType->calculateValues();

        return $values;
    }

    /**
     * @param string $slug
     * @return object
     */
    private function factoryClassGuesser($slug)
    {
        $class = Inflector::classify($slug);
        $class = '\\AndresGotta\\Bundle\\GolfBundle\\Comparative\\Types\\' . $class;

        return new $class($this->translator, $this->entityManager);
    }
}