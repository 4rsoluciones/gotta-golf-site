<?php

namespace AndresGotta\Bundle\GolfBundle\Manager;

use AndresGotta\Bundle\GolfBundle\Comparative\ChartGroup;
use AndresGotta\Bundle\GolfBundle\Entity\Comparative;
use AndresGotta\Bundle\GroupBundle\Entity\Group;
use WebFactory\Bundle\UserBundle\Entity\User;


/**
 * Class ComparativeManager
 * @package AndresGotta\Bundle\GolfBundle\Manager
 */
interface ComparativeManagerInterface
{
    /**
     * @param Comparative $comparative
     * @param User|null $user
     * @return mixed
     */
    public function getComparativeResults(Comparative $comparative, User $user = null);

    /**
     * @param Comparative $comparative
     * @param Group $group
     * @param User|null $user
     * @return ChartGroup
     */
    public function getComparativeResultsForGroup(Comparative $comparative, Group $group, User $user = null);
}