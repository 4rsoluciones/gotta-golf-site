<?php

namespace AndresGotta\Bundle\GolfBundle\Manager;

use AndresGotta\Bundle\GolfBundle\Entity\FirstStrike;
use Doctrine\ORM\EntityManager;

class QuestionnairesReportManager
{
    /**
     * @var EntityManager
     */
    private $entityManager;

    /**
     * QuestionnairesReportManager constructor.
     * @param EntityManager $entityManager
     */
    public function __construct(EntityManager $entityManager)
    {
        $this->entityManager = $entityManager;
    }

    /**
     * @return array
     */
    public function getHolesEmotionalAverage()
    {
        $holes = $this->entityManager->getRepository('AndresGottaGolfBundle:Hole')->findAll();

        $holesEmotionalAverage = array(
            'designAndPairEmotionalAverage' => $this->calculateDesignAndPairEmotionalAverage($holes),
            'firstStrikeEmotionalAverage' => $this->calculateFirstStrikeEmotionalAverage($holes),
            'strikesEmotionalAverage' => $this->calculateStrikesEmotionalAverage($holes),
            'puttEmotionalAverage' => $this->calculatePuttEmotionalAverage($holes),
        );

        return $holesEmotionalAverage;
    }

    /**
     * @return array
     */
    public function getHolesFoodAverage()
    {
        $holes = $this->entityManager->getRepository('AndresGottaGolfBundle:Hole')->findAll();

        $holesFoodAverage = array(
            'designAndPairFoodAverage' => $this->calculateDesignAndPairFoodAverage($holes),
            'firstStrikeFoodAverage' => $this->calculateFirstStrikeFoodAverage($holes),
            'strikesFoodAverage' => $this->calculateStrikesFoodAverage($holes),
            'puttFoodAverage' => $this->calculatePuttFoodAverage($holes),
        );

        return $holesFoodAverage;
    }

    /**
     * @return array
     */
    public function getHolesPhysicalAverage()
    {
        $holes = $this->entityManager->getRepository('AndresGottaGolfBundle:Hole')->findAll();

        $holesPhysicalAverage = array(
            'designAndPairPhysicalAverage' => $this->calculateDesignAndPairPhysicalAverage($holes),
            'firstStrikePhysicalAverage' => $this->calculateFirstStrikePhysicalAverage($holes),
            'strikesPhysicalAverage' => $this->calculateStrikesPhysicalAverage($holes),
            'puttPhysicalAverage' => $this->calculatePuttPhysicalAverage($holes),
        );

        return $holesPhysicalAverage;
    }

    /**
     * @return array
     */
    public function getEmotionalRates()
    {
        $emotionalQuestionnaires = $this->entityManager->getRepository('AndresGottaGolfBundle:EmotionalQuestionnaire')->findAll();

        $feelsUptightCount = 0;
        $feelsDistrustfulCount = 0;
        $feelsAnnoyedCount = 0;
        $feelsFrustratedCount = 0;
        $feelsTrustfulCount = 0;
        $feelsEnjoyingCount = 0;
        $feelsFocusedCount = 0;
        $emotionalCount = 0;

        foreach ($emotionalQuestionnaires as $emotionalQuestionnaire) {
            if ($emotionalQuestionnaire->getFeelsUptight()) {
                $feelsUptightCount++;
                $emotionalCount++;
            }
            if ($emotionalQuestionnaire->getFeelsDistrustful()) {
                $feelsDistrustfulCount++;
                $emotionalCount++;
            }
            if ($emotionalQuestionnaire->getFeelsAnnoyed()) {
                $feelsAnnoyedCount++;
                $emotionalCount++;
            }
            if ($emotionalQuestionnaire->getFeelsFrustrated()) {
                $feelsFrustratedCount++;
                $emotionalCount++;
            }
            if ($emotionalQuestionnaire->getFeelsTrustful()) {
                $feelsTrustfulCount++;
                $emotionalCount++;
            }
            if ($emotionalQuestionnaire->getFeelsEnjoying()) {
                $feelsEnjoyingCount++;
                $emotionalCount++;
            }
            if ($emotionalQuestionnaire->getFeelsFocused()) {
                $feelsFocusedCount++;
                $emotionalCount++;
            }
        }

        $emotionalRates = array(
            'feelsUptightRate' => $feelsUptightCount*100/$emotionalCount,
            'feelsDistrustfulRate' => $feelsDistrustfulCount*100/$emotionalCount,
            'feelsAnnoyedRate' => $feelsAnnoyedCount*100/$emotionalCount,
            'feelsFrustratedRate' => $feelsFrustratedCount*100/$emotionalCount,
            'feelsTrustfulRate' => $feelsTrustfulCount*100/$emotionalCount,
            'feelsEnjoyingRate' => $feelsEnjoyingCount*100/$emotionalCount,
            'feelsFocusedRate' => $feelsFocusedCount*100/$emotionalCount,
        );
        arsort($emotionalRates);

        return $emotionalRates;
    }

    /**
     * @return array
     */
    public function getFoodRates()
    {
        $foodQuestionnaires = $this->entityManager->getRepository('AndresGottaGolfBundle:FoodQuestionnaire')->findAll();

        $drankWater = 0;
        $drankSportsDrink = 0;
        $swallowedCerealOrNuts = 0;
        $swallowedSandwich = 0;
        $swallowedFruit = 0;
        $foodCount = 0;

        foreach ($foodQuestionnaires as $foodQuestionnaire) {
            if ($foodQuestionnaire->getDrankWater()) {
                $drankWater++;
                $foodCount++;
            }
            if ($foodQuestionnaire->getDrankSportsDrink()) {
                $drankSportsDrink++;
                $foodCount++;
            }
            if ($foodQuestionnaire->getSwallowedCerealOrNuts()) {
                $swallowedCerealOrNuts++;
                $foodCount++;
            }
            if ($foodQuestionnaire->getSwallowedSandwich()) {
                $swallowedSandwich++;
                $foodCount++;
            }
            if ($foodQuestionnaire->getSwallowedFruit()) {
                $swallowedFruit++;
                $foodCount++;
            }
        }

        $foodRates = array(
            'drankWaterRate' => $drankWater*100/$foodCount,
            'drankSportsDrinkRate' => $drankSportsDrink*100/$foodCount,
            'swallowedCerealOrNutsRate' => $swallowedCerealOrNuts*100/$foodCount,
            'swallowedSandwichRate' => $swallowedSandwich*100/$foodCount,
            'swallowedFruitRate' => $swallowedFruit*100/$foodCount,
        );
        arsort($foodRates);

        return $foodRates;
    }

    /**
     * @return array
     */
    public function getPhysicalRates()
    {
        $physicalQuestionnaires = $this->entityManager->getRepository('AndresGottaGolfBundle:PhysicalQuestionnaire')->findAll();

        $feelsEnergetic = 0;
        $feelsGettingTired = 0;
        $feelsPainful = 0;
        $feelsTired = 0;
        $feelsExhausted = 0;
        $physicalCount = 0;

        foreach ($physicalQuestionnaires as $physicalQuestionnaire) {
            if ($physicalQuestionnaire->getFeelsEnergetic()) {
                $feelsEnergetic++;
                $physicalCount++;
            }
            if ($physicalQuestionnaire->getFeelsGettingTired()) {
                $feelsGettingTired++;
                $physicalCount++;
            }
            if ($physicalQuestionnaire->getFeelsPainful()) {
                $feelsPainful++;
                $physicalCount++;
            }
            if ($physicalQuestionnaire->getFeelsTired()) {
                $feelsTired++;
                $physicalCount++;
            }
            if ($physicalQuestionnaire->getFeelsExhausted()) {
                $feelsExhausted++;
                $physicalCount++;
            }
        }

        $physicalRates = array(
            'feelsEnergeticRate' => $feelsEnergetic*100/$physicalCount,
            'feelsGettingTiredRate' => $feelsGettingTired*100/$physicalCount,
            'feelsPainfulRate' => $feelsPainful*100/$physicalCount,
            'feelsTiredRate' => $feelsTired*100/$physicalCount,
            'feelsExhaustedRate' => $feelsExhausted*100/$physicalCount,
        );
        arsort($physicalRates);

        return $physicalRates;
    }

    /**
     * @return array
     */
    public function getPreCompetitiveQuestionnairesAverage()
    {
        $preCompetitiveQuestionnaires = $this->entityManager->getRepository('AndresGottaGolfBundle:PreCompetitiveQuestionnaire')->findAll();

        $isWarmedUp = 0;
        $hasPracticed = 0;
        $wantsToPlay = 0;
        $hasStrategy = 0;

        foreach ($preCompetitiveQuestionnaires as $preCompetitiveQuestionnaire) {
            if ($preCompetitiveQuestionnaire->getIsWarmedUp()) $isWarmedUp++;
            if ($preCompetitiveQuestionnaire->getHasPracticed()) $hasPracticed++;
            if ($preCompetitiveQuestionnaire->getWantsToPlay()) $wantsToPlay++;
            if ($preCompetitiveQuestionnaire->getHasStrategy()) $hasStrategy++;
        }

        $preCompetitiveQuestionnairesAverage = array(
            'isWarmedUp' => $isWarmedUp*100/count($preCompetitiveQuestionnaires),
            'hasPracticed' => $hasPracticed*100/count($preCompetitiveQuestionnaires),
            'wantsToPlay' => $wantsToPlay*100/count($preCompetitiveQuestionnaires),
            'hasStrategy' => $hasStrategy*100/count($preCompetitiveQuestionnaires),
        );

        return $preCompetitiveQuestionnairesAverage;
    }

    /**
     * @param array $holes
     * @return float|int
     */
    private function calculateDesignAndPairEmotionalAverage(array $holes)
    {
        $rank = 0;
        foreach ($holes as $hole) {
            if ($emotionalQuestionnaire = $hole->getEmotionalQuestionnaire()) {
                if ($emotionalQuestionnaire->getFeelsUptight()) $rank--;
                if ($emotionalQuestionnaire->getFeelsDistrustful()) $rank--;
                if ($emotionalQuestionnaire->getFeelsAnnoyed()) $rank--;
                if ($emotionalQuestionnaire->getFeelsFrustrated()) $rank--;
                if ($emotionalQuestionnaire->getFeelsTrustful()) $rank++;
                if ($emotionalQuestionnaire->getFeelsEnjoying()) $rank++;
                if ($emotionalQuestionnaire->getFeelsFocused()) $rank++;
            }
        }

        $rankAverage = $rank/count($holes);

        return $rankAverage;
    }

    /**
     * @param array $holes
     * @return float|int
     */
    private function calculateDesignAndPairFoodAverage(array $holes)
    {
        $rank = 0;
        foreach ($holes as $hole) {
            if ($foodQuestionnaire = $hole->getFoodQuestionnaire()) {
                if ($foodQuestionnaire->getDrankWater()) $rank++;
                if ($foodQuestionnaire->getDrankSportsDrink()) $rank++;
                if ($foodQuestionnaire->getSwallowedCerealOrNuts()) $rank++;
                if ($foodQuestionnaire->getSwallowedSandwich()) $rank++;
                if ($foodQuestionnaire->getSwallowedFruit()) $rank++;
            }
        }

        $rankAverage = $rank/count($holes);

        return $rankAverage;
    }

    /**
     * @param array $holes
     * @return float|int
     */
    private function calculateDesignAndPairPhysicalAverage(array $holes)
    {
        $rank = 0;
        foreach ($holes as $hole) {
            if ($physicalQuestionnaire = $hole->getPhysicalQuestionnaire()) {
                if ($physicalQuestionnaire->getFeelsEnergetic()) $rank++;
                if ($physicalQuestionnaire->getFeelsGettingTired()) $rank--;
                if ($physicalQuestionnaire->getFeelsPainful()) $rank--;
                if ($physicalQuestionnaire->getFeelsTired()) $rank--;
                if ($physicalQuestionnaire->getFeelsExhausted()) $rank--;
            }
        }

        $rankAverage = $rank/count($holes);

        return $rankAverage;
    }

    /**
     * @param array $holes
     * @return float|int
     */
    private function calculateFirstStrikeEmotionalAverage(array $holes)
    {
        $rank = 0;
        foreach ($holes as $hole) {
            if ($firstStrike = $hole->getFirstStrike()) {
                if ($emotionalQuestionnaire = $firstStrike->getEmotionalQuestionnaire()) {
                    if ($emotionalQuestionnaire->getFeelsUptight()) $rank--;
                    if ($emotionalQuestionnaire->getFeelsDistrustful()) $rank--;
                    if ($emotionalQuestionnaire->getFeelsAnnoyed()) $rank--;
                    if ($emotionalQuestionnaire->getFeelsFrustrated()) $rank--;
                    if ($emotionalQuestionnaire->getFeelsTrustful()) $rank++;
                    if ($emotionalQuestionnaire->getFeelsEnjoying()) $rank++;
                    if ($emotionalQuestionnaire->getFeelsFocused()) $rank++;
                }
            }
        }

        $rankAverage = $rank/count($holes);

        return $rankAverage;
    }

    /**
     * @param array $holes
     * @return float|int
     */
    private function calculateFirstStrikeFoodAverage(array $holes)
    {
        $rank = 0;
        foreach ($holes as $hole) {
            if ($firstStrike = $hole->getFirstStrike()) {
                if ($foodQuestionnaire = $firstStrike->getFoodQuestionnaire()) {
                    if ($foodQuestionnaire->getDrankWater()) $rank++;
                    if ($foodQuestionnaire->getDrankSportsDrink()) $rank++;
                    if ($foodQuestionnaire->getSwallowedCerealOrNuts()) $rank++;
                    if ($foodQuestionnaire->getSwallowedSandwich()) $rank++;
                    if ($foodQuestionnaire->getSwallowedFruit()) $rank++;
                }
            }
        }

        $rankAverage = $rank/count($holes);

        return $rankAverage;
    }

    /**
     * @param array $holes
     * @return float|int
     */
    private function calculateFirstStrikePhysicalAverage(array $holes)
    {
        $rank = 0;
        foreach ($holes as $hole) {
            if ($firstStrike = $hole->getFirstStrike()) {
                if ($physicalQuestionnaire = $firstStrike->getPhysicalQuestionnaire()) {
                    if ($physicalQuestionnaire->getFeelsEnergetic()) $rank++;
                    if ($physicalQuestionnaire->getFeelsGettingTired()) $rank--;
                    if ($physicalQuestionnaire->getFeelsPainful()) $rank--;
                    if ($physicalQuestionnaire->getFeelsTired()) $rank--;
                    if ($physicalQuestionnaire->getFeelsExhausted()) $rank--;
                }
            }
        }

        $rankAverage = $rank/count($holes);

        return $rankAverage;
    }

    /**
     * @param array $holes
     * @return float
     */
    private function calculateStrikesEmotionalAverage(array $holes)
    {
        $count = 0;
        $rank = 0;
        foreach ($holes as $hole) {
            foreach ($hole->getFirstStrikeApproachs() as $approach) {
                $count++;
                if ($emotionalQuestionnaire = $approach->getEmotionalQuestionnaire()) {
                    if ($emotionalQuestionnaire->getFeelsUptight()) $rank--;
                    if ($emotionalQuestionnaire->getFeelsDistrustful()) $rank--;
                    if ($emotionalQuestionnaire->getFeelsAnnoyed()) $rank--;
                    if ($emotionalQuestionnaire->getFeelsFrustrated()) $rank--;
                    if ($emotionalQuestionnaire->getFeelsTrustful()) $rank++;
                    if ($emotionalQuestionnaire->getFeelsEnjoying()) $rank++;
                    if ($emotionalQuestionnaire->getFeelsFocused()) $rank++;
                }
            }
            if ($strike = $hole->getSecondStrike()) {
                $count++;
                if ($emotionalQuestionnaire = $strike->getEmotionalQuestionnaire()) {
                    if ($emotionalQuestionnaire->getFeelsUptight()) $rank--;
                    if ($emotionalQuestionnaire->getFeelsDistrustful()) $rank--;
                    if ($emotionalQuestionnaire->getFeelsAnnoyed()) $rank--;
                    if ($emotionalQuestionnaire->getFeelsFrustrated()) $rank--;
                    if ($emotionalQuestionnaire->getFeelsTrustful()) $rank++;
                    if ($emotionalQuestionnaire->getFeelsEnjoying()) $rank++;
                    if ($emotionalQuestionnaire->getFeelsFocused()) $rank++;
                }
            }
            foreach ($hole->getSecondStrikeApproachs() as $approach) {
                $count++;
                if ($emotionalQuestionnaire = $approach->getEmotionalQuestionnaire()) {
                    if ($emotionalQuestionnaire->getFeelsUptight()) $rank--;
                    if ($emotionalQuestionnaire->getFeelsDistrustful()) $rank--;
                    if ($emotionalQuestionnaire->getFeelsAnnoyed()) $rank--;
                    if ($emotionalQuestionnaire->getFeelsFrustrated()) $rank--;
                    if ($emotionalQuestionnaire->getFeelsTrustful()) $rank++;
                    if ($emotionalQuestionnaire->getFeelsEnjoying()) $rank++;
                    if ($emotionalQuestionnaire->getFeelsFocused()) $rank++;
                }
            }
            if ($strike = $hole->getThirdStrike()) {
                $count++;
                if ($emotionalQuestionnaire = $strike->getEmotionalQuestionnaire()) {
                    if ($emotionalQuestionnaire->getFeelsUptight()) $rank--;
                    if ($emotionalQuestionnaire->getFeelsDistrustful()) $rank--;
                    if ($emotionalQuestionnaire->getFeelsAnnoyed()) $rank--;
                    if ($emotionalQuestionnaire->getFeelsFrustrated()) $rank--;
                    if ($emotionalQuestionnaire->getFeelsTrustful()) $rank++;
                    if ($emotionalQuestionnaire->getFeelsEnjoying()) $rank++;
                    if ($emotionalQuestionnaire->getFeelsFocused()) $rank++;
                }
            }
            foreach ($hole->getThirdStrikeApproachs() as $approach) {
                $count++;
                if ($emotionalQuestionnaire = $approach->getEmotionalQuestionnaire()) {
                    if ($emotionalQuestionnaire->getFeelsUptight()) $rank--;
                    if ($emotionalQuestionnaire->getFeelsDistrustful()) $rank--;
                    if ($emotionalQuestionnaire->getFeelsAnnoyed()) $rank--;
                    if ($emotionalQuestionnaire->getFeelsFrustrated()) $rank--;
                    if ($emotionalQuestionnaire->getFeelsTrustful()) $rank++;
                    if ($emotionalQuestionnaire->getFeelsEnjoying()) $rank++;
                    if ($emotionalQuestionnaire->getFeelsFocused()) $rank++;
                }
            }
        }

        $rankAverage = ($rank/$count)/count($hole);

        return $rankAverage;
    }

    /**
     * @param array $holes
     * @return float
     */
    private function calculateStrikesFoodAverage(array $holes)
    {
        $count = 0;
        $rank = 0;
        foreach ($holes as $hole) {
            foreach ($hole->getFirstStrikeApproachs() as $approach) {
                $count++;
                if ($foodQuestionnaire = $approach->getFoodQuestionnaire()) {
                    if ($foodQuestionnaire->getDrankWater()) $rank++;
                    if ($foodQuestionnaire->getDrankSportsDrink()) $rank++;
                    if ($foodQuestionnaire->getSwallowedCerealOrNuts()) $rank++;
                    if ($foodQuestionnaire->getSwallowedSandwich()) $rank++;
                    if ($foodQuestionnaire->getSwallowedFruit()) $rank++;
                }
            }
            if ($strike = $hole->getSecondStrike()) {
                $count++;
                if ($foodQuestionnaire = $strike->getFoodQuestionnaire()) {
                    if ($foodQuestionnaire->getDrankWater()) $rank++;
                    if ($foodQuestionnaire->getDrankSportsDrink()) $rank++;
                    if ($foodQuestionnaire->getSwallowedCerealOrNuts()) $rank++;
                    if ($foodQuestionnaire->getSwallowedSandwich()) $rank++;
                    if ($foodQuestionnaire->getSwallowedFruit()) $rank++;
                }
            }
            foreach ($hole->getSecondStrikeApproachs() as $approach) {
                $count++;
                if ($foodQuestionnaire = $approach->getFoodQuestionnaire()) {
                    if ($foodQuestionnaire->getDrankWater()) $rank++;
                    if ($foodQuestionnaire->getDrankSportsDrink()) $rank++;
                    if ($foodQuestionnaire->getSwallowedCerealOrNuts()) $rank++;
                    if ($foodQuestionnaire->getSwallowedSandwich()) $rank++;
                    if ($foodQuestionnaire->getSwallowedFruit()) $rank++;
                }
            }
            if ($strike = $hole->getThirdStrike()) {
                $count++;
                if ($foodQuestionnaire = $strike->getFoodQuestionnaire()) {
                    if ($foodQuestionnaire->getDrankWater()) $rank++;
                    if ($foodQuestionnaire->getDrankSportsDrink()) $rank++;
                    if ($foodQuestionnaire->getSwallowedCerealOrNuts()) $rank++;
                    if ($foodQuestionnaire->getSwallowedSandwich()) $rank++;
                    if ($foodQuestionnaire->getSwallowedFruit()) $rank++;
                }
            }
            foreach ($hole->getThirdStrikeApproachs() as $approach) {
                $count++;
                if ($foodQuestionnaire = $approach->getFoodQuestionnaire()) {
                    if ($foodQuestionnaire->getDrankWater()) $rank++;
                    if ($foodQuestionnaire->getDrankSportsDrink()) $rank++;
                    if ($foodQuestionnaire->getSwallowedCerealOrNuts()) $rank++;
                    if ($foodQuestionnaire->getSwallowedSandwich()) $rank++;
                    if ($foodQuestionnaire->getSwallowedFruit()) $rank++;
                }
            }
        }

        $rankAverage = ($rank/$count)/count($hole);

        return $rankAverage;
    }

    /**
     * @param array $holes
     * @return float
     */
    private function calculateStrikesPhysicalAverage(array $holes)
    {
        $count = 0;
        $rank = 0;
        foreach ($holes as $hole) {
            foreach ($hole->getFirstStrikeApproachs() as $approach) {
                $count++;
                if ($physicalQuestionnaire = $approach->getPhysicalQuestionnaire()) {
                    if ($physicalQuestionnaire->getFeelsEnergetic()) $rank++;
                    if ($physicalQuestionnaire->getFeelsGettingTired()) $rank--;
                    if ($physicalQuestionnaire->getFeelsPainful()) $rank--;
                    if ($physicalQuestionnaire->getFeelsTired()) $rank--;
                    if ($physicalQuestionnaire->getFeelsExhausted()) $rank--;
                }
            }
            if ($strike = $hole->getSecondStrike()) {
                $count++;
                if ($physicalQuestionnaire = $strike->getPhysicalQuestionnaire()) {
                    if ($physicalQuestionnaire->getFeelsEnergetic()) $rank++;
                    if ($physicalQuestionnaire->getFeelsGettingTired()) $rank--;
                    if ($physicalQuestionnaire->getFeelsPainful()) $rank--;
                    if ($physicalQuestionnaire->getFeelsTired()) $rank--;
                    if ($physicalQuestionnaire->getFeelsExhausted()) $rank--;
                }
            }
            foreach ($hole->getSecondStrikeApproachs() as $approach) {
                $count++;
                if ($physicalQuestionnaire = $approach->getPhysicalQuestionnaire()) {
                    if ($physicalQuestionnaire->getFeelsEnergetic()) $rank++;
                    if ($physicalQuestionnaire->getFeelsGettingTired()) $rank--;
                    if ($physicalQuestionnaire->getFeelsPainful()) $rank--;
                    if ($physicalQuestionnaire->getFeelsTired()) $rank--;
                    if ($physicalQuestionnaire->getFeelsExhausted()) $rank--;
                }
            }
            if ($strike = $hole->getThirdStrike()) {
                $count++;
                if ($physicalQuestionnaire = $strike->getPhysicalQuestionnaire()) {
                    if ($physicalQuestionnaire->getFeelsEnergetic()) $rank++;
                    if ($physicalQuestionnaire->getFeelsGettingTired()) $rank--;
                    if ($physicalQuestionnaire->getFeelsPainful()) $rank--;
                    if ($physicalQuestionnaire->getFeelsTired()) $rank--;
                    if ($physicalQuestionnaire->getFeelsExhausted()) $rank--;
                }
            }
            foreach ($hole->getThirdStrikeApproachs() as $approach) {
                $count++;
                if ($physicalQuestionnaire = $approach->getPhysicalQuestionnaire()) {
                    if ($physicalQuestionnaire->getFeelsEnergetic()) $rank++;
                    if ($physicalQuestionnaire->getFeelsGettingTired()) $rank--;
                    if ($physicalQuestionnaire->getFeelsPainful()) $rank--;
                    if ($physicalQuestionnaire->getFeelsTired()) $rank--;
                    if ($physicalQuestionnaire->getFeelsExhausted()) $rank--;
                }
            }
        }

        $rankAverage = ($rank/$count)/count($hole);

        return $rankAverage;
    }

    /**
     * @param array $holes
     * @return float
     */
    private function calculatePuttEmotionalAverage(array $holes)
    {
        $count = 0;
        $rank = 0;
        foreach ($holes as $hole) {
            if ($putt = $hole->getPutt()) {
                $count++;
                if ($emotionalQuestionnaire = $putt->getEmotionalQuestionnaire()) {
                    if ($emotionalQuestionnaire->getFeelsUptight()) $rank--;
                    if ($emotionalQuestionnaire->getFeelsDistrustful()) $rank--;
                    if ($emotionalQuestionnaire->getFeelsAnnoyed()) $rank--;
                    if ($emotionalQuestionnaire->getFeelsFrustrated()) $rank--;
                    if ($emotionalQuestionnaire->getFeelsTrustful()) $rank++;
                    if ($emotionalQuestionnaire->getFeelsEnjoying()) $rank++;
                    if ($emotionalQuestionnaire->getFeelsFocused()) $rank++;
                }
            }
            foreach ($hole->getPuttApproachs() as $approach) {
                $count++;
                if ($emotionalQuestionnaire = $approach->getEmotionalQuestionnaire()) {
                    if ($emotionalQuestionnaire->getFeelsUptight()) $rank--;
                    if ($emotionalQuestionnaire->getFeelsDistrustful()) $rank--;
                    if ($emotionalQuestionnaire->getFeelsAnnoyed()) $rank--;
                    if ($emotionalQuestionnaire->getFeelsFrustrated()) $rank--;
                    if ($emotionalQuestionnaire->getFeelsTrustful()) $rank++;
                    if ($emotionalQuestionnaire->getFeelsEnjoying()) $rank++;
                    if ($emotionalQuestionnaire->getFeelsFocused()) $rank++;
                }
            }
        }

        $rankAverage = ($rank/$count)/count($holes);

        return $rankAverage;
    }

    /**
     * @param array $holes
     * @return float
     */
    private function calculatePuttFoodAverage(array $holes)
    {
        $count = 0;
        $rank = 0;
        foreach ($holes as $hole) {
            if ($putt = $hole->getPutt()) {
                $count++;
                if ($foodQuestionnaire = $putt->getFoodQuestionnaire()) {
                    if ($foodQuestionnaire->getDrankWater()) $rank++;
                    if ($foodQuestionnaire->getDrankSportsDrink()) $rank++;
                    if ($foodQuestionnaire->getSwallowedCerealOrNuts()) $rank++;
                    if ($foodQuestionnaire->getSwallowedSandwich()) $rank++;
                    if ($foodQuestionnaire->getSwallowedFruit()) $rank++;
                }
            }
            foreach ($hole->getPuttApproachs() as $approach) {
                $count++;
                if ($foodQuestionnaire = $approach->getFoodQuestionnaire()) {
                    if ($foodQuestionnaire->getDrankWater()) $rank++;
                    if ($foodQuestionnaire->getDrankSportsDrink()) $rank++;
                    if ($foodQuestionnaire->getSwallowedCerealOrNuts()) $rank++;
                    if ($foodQuestionnaire->getSwallowedSandwich()) $rank++;
                    if ($foodQuestionnaire->getSwallowedFruit()) $rank++;
                }
            }
        }

        $rankAverage = ($rank/$count)/count($holes);

        return $rankAverage;
    }

    /**
     * @param array $holes
     * @return float
     */
    private function calculatePuttPhysicalAverage(array $holes)
    {
        $count = 0;
        $rank = 0;
        foreach ($holes as $hole) {
            if ($putt = $hole->getPutt()) {
                $count++;
                if ($physicalQuestionnaire = $putt->getPhysicalQuestionnaire()) {
                    if ($physicalQuestionnaire->getFeelsEnergetic()) $rank++;
                    if ($physicalQuestionnaire->getFeelsGettingTired()) $rank--;
                    if ($physicalQuestionnaire->getFeelsPainful()) $rank--;
                    if ($physicalQuestionnaire->getFeelsTired()) $rank--;
                    if ($physicalQuestionnaire->getFeelsExhausted()) $rank--;
                }
            }
            foreach ($hole->getPuttApproachs() as $approach) {
                $count++;
                if ($physicalQuestionnaire = $approach->getPhysicalQuestionnaire()) {
                    if ($physicalQuestionnaire->getFeelsEnergetic()) $rank++;
                    if ($physicalQuestionnaire->getFeelsGettingTired()) $rank--;
                    if ($physicalQuestionnaire->getFeelsPainful()) $rank--;
                    if ($physicalQuestionnaire->getFeelsTired()) $rank--;
                    if ($physicalQuestionnaire->getFeelsExhausted()) $rank--;
                }
            }
        }

        $rankAverage = ($rank/$count)/count($holes);

        return $rankAverage;
    }
}
