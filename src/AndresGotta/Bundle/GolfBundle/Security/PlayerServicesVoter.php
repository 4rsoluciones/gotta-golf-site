<?php

namespace AndresGotta\Bundle\GolfBundle\Security;

use GuzzleHttp\Exception\GuzzleException;
use League\OAuth2\Client\Token\AccessToken;
use Symfony\Component\Security\Core\Authorization\Voter\AbstractVoter;
use Symfony\Component\Security\Core\User\UserInterface;
use WebFactory\Bundle\UserBundle\Entity\User;
use WebFactory\Bundle\UserBundle\Service\GottaCenterApiInterface;

/**
 * Class PlayerServicesVoter
 * @package AndresGotta\Bundle\GolfBundle\Security
 */
class PlayerServicesVoter extends AbstractVoter
{
    const ROLE_ACTIVE_PLAYER = 'ROLE_PLAYER';//este se usa solo en el front web de gpstats (que estaria deshabilitado)

    const GPSTATS_ACCESS = 'GPSTATS_ACCESS';//Unico servicio al momento

    /**
     * @var GottaCenterApiInterface
     */
    private $api;

    /**
     * PlayerServicesVoter constructor.
     * @param GottaCenterApiInterface $api
     */
    public function __construct(GottaCenterApiInterface $api)
    {
        $this->api = $api;
    }

    /**
     * @inheritdoc
     */
    protected function getSupportedClasses()
    {
        return [UserInterface::class];
    }

    /**
     * @inheritdoc
     */
    protected function getSupportedAttributes()
    {
        return [self::GPSTATS_ACCESS];
    }

    /**
     * @inheritdoc
     */
    protected function isGranted($attribute, $object, $user = null)
    {
        if (!$user instanceof User) {
            return false;
        }

        /** @var $object UserInterface */
        switch ($attribute) {
            case self::GPSTATS_ACCESS:
                return $this->checkGottaCenterService($user->getEmail(), $user->getAccessToken());
            default:
                throw new \InvalidArgumentException("Invalid attribute " . $attribute);
        }
    }

    /**
     * @param $email
     * @param AccessToken $accessToken
     * @return bool
     */
    private function checkGottaCenterService($email, AccessToken $accessToken)
    {//return true;
        $platform = 'gpstats';
        try {
            $service = $this->api->getServiceStatusByPlatform($platform, $email, $accessToken);
//            $service['id']
//            $service['name']
//            $service['pack']
//            $service['pack']['students']

            // a futuro
            //$service['show_stats'] === 1;

            if ($service) {
                return true;
            }

        } catch (GuzzleException $e) {
            return false;
        }

        return false;
    }

}
