<?php

namespace AndresGotta\Bundle\GolfBundle\Service;

use AndresGotta\Bundle\GolfBundle\Entity\Hole;

interface HolePersisterInterface
{
    /**
     * @param Hole $hole
     */
    public function save(Hole $hole);
}