<?php

namespace AndresGotta\Bundle\GolfBundle\Service;

use AndresGotta\Bundle\GolfBundle\Entity\DefaultHole;
use AndresGotta\Bundle\GolfBundle\Entity\Hole;
use Doctrine\ORM\EntityManagerInterface;
use Doctrine\ORM\NonUniqueResultException;

class HolePersister implements HolePersisterInterface
{
    /**
     * @var EntityManagerInterface
     */
    private $em;


    /**
     * RoundPersister constructor.
     * @param EntityManagerInterface $em
     */
    public function __construct(EntityManagerInterface $em)
    {
        $this->em = $em;
    }

    /**
     * @param Hole $hole
     */
    public function save(Hole $hole)
    {
        try {
            $this->createDefaultHoleFrom($hole);
        } catch (NonUniqueResultException $e) {

        }

        $round = $hole->getRound();
        $pre = (bool)$round->getPreCompetitiveQuestionnaire();
        $post = (bool)$round->getPostCompetitiveQuestionnaire();
        if ($pre && $post && $round->getPlayedHoles() === $round->getHoles()->count()) {
            $round->setPlayerConfirmed(true);
            $this->em->persist($round);
        }

        $this->em->persist($hole);
        $this->em->flush();
    }

    /**
     * @param Hole $hole
     * @throws NonUniqueResultException
     */
    private function createDefaultHoleFrom(Hole $hole)
    {
        $defaultHoleRepository = $this->em->getRepository('AndresGottaGolfBundle:DefaultHole');
        $defaultHole = $defaultHoleRepository->findOneByHole($hole);
        if (!$defaultHole) {
            $defaultHole = DefaultHole::createFromHole($hole);
        }
        $hole->setDefaultHole($defaultHole);
        $this->em->persist($defaultHole);
    }

}