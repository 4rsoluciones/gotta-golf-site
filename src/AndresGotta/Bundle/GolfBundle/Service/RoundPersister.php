<?php

namespace AndresGotta\Bundle\GolfBundle\Service;

use AndresGotta\Bundle\GolfBundle\Entity\DefaultField;
use AndresGotta\Bundle\GolfBundle\Entity\Field;
use AndresGotta\Bundle\GolfBundle\Entity\Round;
use Doctrine\ORM\EntityManagerInterface;
use Doctrine\ORM\NonUniqueResultException;

class RoundPersister implements RoundPersisterInterface
{
    /**
     * @var EntityManagerInterface
     */
    private $em;


    /**
     * RoundPersister constructor.
     * @param EntityManagerInterface $em
     */
    public function __construct(EntityManagerInterface $em)
    {
        $this->em = $em;
    }

    /**
     * @param Round $round
     */
    public function save(Round $round)
    {
        $field = $round->getField();
        try {
            $this->createDefaultFieldFrom($field);
        } catch (NonUniqueResultException $e) {

        }

        $pre = (bool)$round->getPreCompetitiveQuestionnaire();
        $post = (bool)$round->getPostCompetitiveQuestionnaire();
        if ($pre && $post && $round->getPlayedHoles() === $round->getHoles()->count()) {
            $round->setPlayerConfirmed(true);
        }

        $this->em->persist($round);
        $this->em->flush();
    }

    /**
     * @param Field $field
     * @throws NonUniqueResultException
     */
    private function createDefaultFieldFrom(Field $field)
    {
        $defaultFieldRepository = $this->em->getRepository('AndresGottaGolfBundle:DefaultField');
        $defaultField = $defaultFieldRepository->findOneByField($field);
        if (!$defaultField) {
            $defaultField = DefaultField::createFromField($field);
        }
        $field->setDefaultField($defaultField);
        $this->em->persist($defaultField);
    }

}