<?php

namespace AndresGotta\Bundle\GolfBundle\Service;

use AndresGotta\Bundle\GolfBundle\Entity\Round;

interface RoundPersisterInterface
{
    /**
     * @param Round $round
     */
    public function save(Round $round);
}