<?php

namespace AndresGotta\Bundle\GolfBundle\Report;

use AndresGotta\Bundle\GolfBundle\Entity\FirstStrike;
use AndresGotta\Bundle\GolfBundle\ValueObject\BallFlight;
use AndresGotta\Bundle\GolfBundle\ValueObject\Club;
use AndresGotta\Bundle\GolfBundle\ValueObject\Reason;
use AndresGotta\Bundle\GolfBundle\ValueObject\Result;

class ReportTrendsTeeErrorsPar4Strategy extends BaseStrategy
{
    /**
     * REPORTE 8.2 - PAR 4
     * 
     * De la estadística 8 PAR 4, tomar solo el valor 
     * obtenido en el resultado fuera de fairway para 
     * cada grupo, y calcular el porcentaje que en esos 
     * tiros tuvo cada vuelo de pelota.
     * Mostrar según vuelo de pelota, por palo utilizado 
     * y distancia ejecutada
     */
    public function generateStats()
    {
        $par = 4;
        $reasons = array(Reason::TO_THE_LEFT, Reason::TO_THE_RIGHT);
        $clubs = Club::getChoices();
        $ballFlights = BallFlight::getChoices();
        $distances = FirstStrike::getDistanceChoicesByPar($par);
        $result = array();
        $total = 0;
        foreach ($reasons as $reason) {
            foreach ($clubs as $club) {
                foreach ($ballFlights as $ballFlight) {
                    foreach ($distances as $distance) {
                        $sum = $this->generateOutsideFairwayTotalsByBallFlight($par, $club, $distance, $reason, $ballFlight);
                        $result[$reason][$club][$ballFlight][$distance] = $sum;
                        $total += $sum;
                    }
                }
            }
        }
        $this->applyPercents($result, $total);

        return $result;
    }
    
    public function generateOutsideFairwayTotalsByBallFlight($par, $club, $distance, $reason, $ballFlight)
    {
        $this->preprocessFilters();
        $result = Result::OUTSIDE_FAIRWAY;
        $qb = $this->queryBuilder
            ->select('COUNT(FirstStrike.id) as quantity')
            ->from('AndresGottaGolfBundle:FirstStrike', 'FirstStrike')
            ->innerJoin('FirstStrike.hole', 'Hole')
            ->innerJoin('Hole.round', 'Round')
            ->andWhere('Hole.par = :par')->setParameter('par', $par)
            ->andWhere('FirstStrike.result = :result')->setParameter('result', $result)
            ->andWhere('FirstStrike.club = :club')->setParameter('club', $club)
            ->andWhere('FirstStrike.distance = :distance')->setParameter('distance', $distance)
            ->andWhere('FirstStrike.reason = :reason')->setParameter('reason', $reason)
            ->andWhere('FirstStrike.ballFlight = :ballFlight')->setParameter('ballFlight', $ballFlight)
        ;

        return (int) $qb->getQuery()->getSingleScalarResult();
    }

    /**
     * @param $data
     * @param $sum
     */
    private function applyPercents(&$data, $sum)
    {
        foreach ($data as &$reason) {
            foreach ($reason as &$club) {
                foreach ($club as &$ballFlight) {
                    foreach ($ballFlight as &$item) {
                        $item *= $sum ? 100 / $sum : 0;
                    }
                    unset($item);
                }
            }
        }
    }
}
