<?php

namespace AndresGotta\Bundle\GolfBundle\Report;

use AndresGotta\Bundle\GolfBundle\ValueObject\Result;

class ReportFirstStrikePar3HoleOutStrategy extends BaseStrategy
{
    /**
     * REPORTE 2 - PAR 3
     * 
     * Procesa el reporte de vueltas
     * 
     * Resultado (en porcentaje) del tiro de salida en cada hoyo par 3 en cada rango de distancia (8 en total)
     * ( porcentajes de cada resultado obtenido desde cada distancia)
     * a) embocada (hoyo en uno) **
     * b) en green – 
     * c) fuera del green y sus derivados (5 en total) , 
     *
     * @return array
     */
    public function generateStats()
    {
        $this->preprocessFilters();

        $par = 3;

        $data = $this->queryBuilder
                ->select('FirstStrike.distance, COUNT(FirstStrike.id) as quantity')
                ->from('AndresGottaGolfBundle:FirstStrike', 'FirstStrike')
                ->innerJoin('FirstStrike.hole', 'Hole')
                ->innerJoin('Hole.round', 'Round')
                ->andWhere('Hole.par = :par')
                ->setParameter('par', $par)
                ->andWhere('FirstStrike.result = :result')
                ->setParameter('result', Result::HOLE_OUT)
                ->addGroupBy('FirstStrike.distance')
                ->orderBy('quantity', 'desc')
                ->getQuery()
                ->getResult();
        //TODO: Optimizar

        $sum = 0;
        foreach ($data as $item) {
            $sum += $item['quantity'];
        }
        foreach ($data as &$item) {
            $item['quantity'] *= $sum ?  100 / $sum : 0;
        }
        unset($item);

        return $data;
    }
}
