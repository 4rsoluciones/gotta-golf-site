<?php

namespace AndresGotta\Bundle\GolfBundle\Report;

use AndresGotta\Bundle\GolfBundle\Entity\Putt;
use AndresGotta\Bundle\GolfBundle\ValueObject\LandSlope;
use AndresGotta\Bundle\GolfBundle\ValueObject\Result;

class ReportTrendsPuttsWrongDistanceSlopePar5Strategy extends BaseStrategy
{
    /**
     * REPORTE 7 - PAR 5
     */
    public function generateStats()
    {
        $par = 5;
        $landSlopes = array(LandSlope::ON_CLIMB, LandSlope::ON_DESCEND, LandSlope::STRAIGHT);
        $deviations = array(LandSlope::RIGHT_TO_LEFT, LandSlope::STRAIGHT, LandSlope::LEFT_TO_RIGHT);
        $distances = Putt::getDistanceChoicesByPar($par);
        
        foreach ($distances as $distance) {
            foreach ($landSlopes as $landSlope) {
                if ($landSlope != LandSlope::STRAIGHT) {
                    foreach ($deviations as $deviation) {
                        $putt = $this->generatePuttTotalByLandSlope($par, $landSlope, $distance, $deviation);
                        $puttApproach = $this->generatePuttApproachTotalByLandSlope($par, $landSlope, $distance, $deviation);
                        $result[$landSlope][$deviation][$distance] = $putt + $puttApproach;
                    }
                } else {
                    $putt = $this->generatePuttTotalByLandSlope($par, $landSlope, $distance);
                    $puttApproach = $this->generatePuttApproachTotalByLandSlope($par, $landSlope, $distance);
                    $result[$landSlope][$distance] = $putt + $puttApproach;
                }
            }
        }
       
        $this->applyPercents($result);
        
        return $result;
    }
    
    private function generatePuttTotalByLandSlope($par, $landSlope, $distance, $deviation = null)
    {
        $this->preprocessFilters();
        
        $data = $this->queryBuilder
                ->select('COUNT(Putt.id) as quantity')
                ->from('AndresGottaGolfBundle:Putt', 'Putt')
                ->innerJoin('Putt.hole', 'Hole')
                ->innerJoin('Hole.round', 'Round')
                ->andWhere('Hole.par = :par')
                ->setParameter('par', $par)
                ->andWhere('Putt.distance = :distance')
                ->setParameter('distance', $distance)
                ->andWhere('Putt.result = :result')
                ->setParameter('result', Result::MISSED)
                ->andWhere('Putt.landSlopes LIKE :landSlope')
                ;
        $deviation 
                ? $data->setParameter('landSlope', "{$deviation},{$landSlope}")
                : $data->setParameter('landSlope', "{$landSlope}");
        
        return $data->getQuery()->getSingleScalarResult();
    }
    
    private function generatePuttApproachTotalByLandSlope($par, $landSlope, $distance, $deviation = null)
    {
        $this->preprocessFilters();
        
        $data = $this->queryBuilder
                ->select('COUNT(PuttApproach.id) as quantity')
                ->from('AndresGottaGolfBundle:PuttApproach', 'PuttApproach')
                ->innerJoin('PuttApproach.hole', 'Hole')
                ->innerJoin('Hole.round', 'Round')
                ->andWhere('Hole.par = :par')
                ->setParameter('par', $par)
                ->andWhere('PuttApproach.distance = :distance')
                ->setParameter('distance', $distance)
                ->andWhere('PuttApproach.result = :result')
                ->setParameter('result', Result::MISSED)
                ->andWhere('PuttApproach.landSlopes LIKE :landSlope')
                ;
        $deviation 
                ? $data->setParameter('landSlope', "{$deviation},{$landSlope}")
                : $data->setParameter('landSlope', "{$landSlope}");
        
        return $data->getQuery()->getSingleScalarResult();
    }
    
    private function sumQuantity($data)
    {
        $sum = 0;
        foreach ($data as $key => $item) {
            foreach ($item as $value) {
                if ($key == LandSlope::STRAIGHT) {
                    $sum += $value;
                } else {
                    foreach ($value as $it) {
                        $sum += $it;
                    }
                }
            }
        }

        return $sum;
    }

    private function applyPercents(&$data)
    {
        $sum = $this->sumQuantity($data);
        
        foreach ($data as $key => &$item) {
            foreach ($item as &$value) {
                if ($key == LandSlope::STRAIGHT) {
                    $value *= $sum ?  100 / $sum : 0;
                } else {
                    foreach ($value as &$it) {
                        $it *= $sum ?  100 / $sum : 0;
                    }
                }
            }
        }
        unset($item);
        unset($it);
    }
    
}
