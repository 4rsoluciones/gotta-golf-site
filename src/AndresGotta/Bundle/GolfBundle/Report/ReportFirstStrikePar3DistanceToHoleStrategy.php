<?php

namespace AndresGotta\Bundle\GolfBundle\Report;

use AndresGotta\Bundle\GolfBundle\Entity\FirstStrike;
use AndresGotta\Bundle\GolfBundle\Entity\Putt;
use AndresGotta\Bundle\GolfBundle\ValueObject\Result;

class ReportFirstStrikePar3DistanceToHoleStrategy extends BaseStrategy
{
    /**
     * REPORTE 3 - PAR 3
     * 
     * Procesa el reporte de vueltas
     * 
     * Qué porcentaje, del total de tiros de salida de par 3, desde cada rango 
     * de distancia (8 en total), que tienen como resultado en green, queda en 
     * cada una de las  4 opciones de distancias ofrecidas:
     *   a)menos de 1 metro, 
     *   b) 1 a 2 mts
     *   c) 2 a 3 mts
     *   d) mas de 3 metros.
     *
     * Agrupadas por distancias del primer putt se seleccionan las distancias 
     * y se calcula las cantidades totales y porcentajes de las mismas. Se 
     * consideran los golpes de salida de hoyos par 3 que den como resultado en 
     * Green únicamente.
     * Mostrar según distancia al hoyo (distancia del putt).
     * Chequear las opciones de distancia que muestra el slider
     */
    public function generateStats()
    {
        $par = 3;
        $distances = FirstStrike::getDistanceChoicesByPar($par);
        $puttDistances = Putt::getDistanceChoicesByPar($par);
        $results = array();
        foreach ($puttDistances as $puttDistance) {
            foreach ($distances as $distance) {
                $results[$puttDistance][$distance] = $this->getQuantityDistanceToHoleOut($par, $puttDistance, $distance);
            }
        }
        $this->applyPercents($results);

        return $results;
    }
    
    private function getQuantityDistanceToHoleOut($par, $puttDistance, $distance)
    {
        $this->preprocessFilters();
        
        $query = $this->queryBuilder
                ->select('COUNT(FirstStrike.id)')
                ->from('AndresGottaGolfBundle:FirstStrike', 'FirstStrike')
                ->innerJoin('FirstStrike.hole', 'Hole')
                ->innerJoin('Hole.round', 'Round')
                ->innerJoin('AndresGottaGolfBundle:Putt', 'Putt', 'WITH', 'Putt.hole = Hole')
                ->andWhere('Hole.par = :par')
                ->setParameter('par', $par)
                ->andWhere('FirstStrike.result = :result')
                ->setParameter('result', Result::INSIDE_GREEN)
                ->andWhere('FirstStrike.distance = :distance')
                ->setParameter('distance', $distance)
                ->andWhere('Putt.distance = :puttDistance')
                ->setParameter('puttDistance', $puttDistance)
                ->getQuery()
                ->getSingleScalarResult();
        
        return $query;
    }
        
    private function sumQuantity($data)
    {
        $sum = 0;
        foreach ($data as $item) {
            foreach ($item as $value) {
                $sum += $value;
            }
        }

        return $sum;
    }

    private function applyPercents(&$data)
    {
        $sum = $this->sumQuantity($data);
        foreach ($data as &$item) {
            foreach ($item as &$value) {
                $value *= $sum ?  100 / $sum : 0;
            }
        }
        unset($value);
        unset($item);
    }
    
}
