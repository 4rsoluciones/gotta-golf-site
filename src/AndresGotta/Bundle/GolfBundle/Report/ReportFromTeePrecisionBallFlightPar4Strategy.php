<?php

namespace AndresGotta\Bundle\GolfBundle\Report;

use AndresGotta\Bundle\GolfBundle\Entity\FirstStrike;
use AndresGotta\Bundle\GolfBundle\ValueObject\BallFlight;
use AndresGotta\Bundle\GolfBundle\ValueObject\Club;
use AndresGotta\Bundle\GolfBundle\ValueObject\Result;

class ReportFromTeePrecisionBallFlightPar4Strategy extends BaseStrategy
{
    /**
     * REPORTE 8.1 - PAR 4
     * 
     * De la estadística 8 PAR 4, tomar solo el valor obtenido 
     * en el resultado en fairway para cada grupo, y calcular 
     * el porcentaje que en esos tiros tuvo cada vuelo de pelota.
     * Mostrar según vuelo de pelota, por palo utilizado y 
     * distancia ejecutada. 
     */
    public function generateStats()
    {
        $par = 4;
        $clubs = Club::getChoices();
        $ballFlights = BallFlight::getChoices();
        $distances = FirstStrike::getDistanceChoicesByPar($par);
        
        foreach ($clubs as $club) {
            foreach ($ballFlights as $ballFlight) {
                foreach ($distances as $distance) {
                    $data[$club][$ballFlight][$distance] = $this->getFirstStrikeCount($par, $club, $ballFlight, $distance);
                }
            }
            $this->applyPercents($data[$club]);
        }

        return $data;
    }
    
    public function getFirstStrikeCount($par, $club, $ballFlight, $distance)
    {
        $this->preprocessFilters();

        $results = $this->queryBuilder
                ->select('COUNT(FirstStrike.id)')
                ->from('AndresGottaGolfBundle:FirstStrike', 'FirstStrike')
                ->innerJoin('FirstStrike.hole', 'Hole')
                ->innerJoin('Hole.round', 'Round')
                ->andWhere('Hole.par = :par')
                ->setParameter('par', $par)
                ->andWhere('FirstStrike.club = :club')
                ->setParameter('club', $club)
                ->andWhere('FirstStrike.result = :result')
                ->setParameter('result', Result::INSIDE_FAIRWAY)
                ->andWhere('FirstStrike.distance = :distance')
                ->setParameter('distance', $distance)
                ->andWhere('FirstStrike.ballFlight = :ballFlight')
                ->setParameter('ballFlight', $ballFlight)
                ->getQuery()
                ->getSingleScalarResult();
        
        return $results;
    }
   
    private function sumQuantity($data)
    {
        $sum = 0;
        foreach ($data as $item) {
            foreach ($item as $value) {
                $sum += $value;
            }
        }

        return $sum;
    }

    private function applyPercents(&$data)
    {
        $sum = $this->sumQuantity($data);
        foreach ($data as &$item) {
            foreach ($item as &$value) {
                $value *= $sum ?  100 / $sum : 0;
            }
        }
        unset($value);
        unset($item);
    }
}
