<?php

namespace AndresGotta\Bundle\GolfBundle\Report;

use AndresGotta\Bundle\GolfBundle\ValueObject\FieldPar;
use AndresGotta\Bundle\GolfBundle\ValueObject\Result;
use Doctrine\ORM\Query\Expr\Join;

class ReportGeneralsAverageScoreFieldParStrategy extends BaseStrategy
{
    /**
     * REPORTE 19.1
     * 
     * IDEM mi consideración en el reporte 19, salvo que ahora se 
     * hace ese promedio agrupando según el par de la cancha. De 
     * esta manera, devolvemos al jugador la información sobre qué 
     * promedio de golpes ± el par que tiene, en cada par de cancha.
     */
    public function generateStats()
    {
        $result = [];
        foreach (FieldPar::getChoices() as $par) {
            $result[$par] = array(
                'total' => $this->generateTotalRoundPlayed($par),
                'diff' => ($this->generateTotalStrikes($par) - $this->generateTotalRoundStrikes($par)),
            ); 
        }
        
        return $result;
    }

    /**
     * @param $fieldPar
     * @return integer
     */
    private function generateTotalRoundPlayed($fieldPar)
    {
        $this->preprocessFilters();
        
        $result = $this->queryBuilder
            ->select('COUNT(Round.id)')
            ->from('AndresGottaGolfBundle:Round', 'Round')
            ->andWhere('Round.fieldPar = :fieldPar')->setParameter('fieldPar', $fieldPar)
            ->getQuery()
            ->getSingleScalarResult()
        ;
        
        return $result;
    }
    
    private function generateTotalStrikes($fieldPar)
    {
        $this->preprocessFilters();
        
        $result = $this->queryBuilder
            ->select('Round.id, COUNT(Strike.id) as strikes')
            ->from('AndresGottaGolfBundle:Hole', 'Hole')
            ->innerJoin('Hole.round', 'Round')
            ->innerJoin('AndresGottaGolfBundle:Strike', 'Strike')
            ->andWhere('Round.fieldPar = :fieldPar')->setParameter('fieldPar', $fieldPar)
            ->addGroupBy('Round')
            ->getQuery()
            ->getResult()
        ;
        
        return $this->getTotalStrikes($result);
    }
    
    private function generateTotalRoundStrikes($fieldPar)
    {
        $this->preprocessFilters();
        
        $result = $this->queryBuilder
            ->select('Round.id, SUM(Hole.par) as strikes')
            ->from('AndresGottaGolfBundle:Hole', 'Hole')
            ->innerJoin('Hole.round', 'Round')
            ->andWhere('Round.fieldPar = :fieldPar')->setParameter('fieldPar', $fieldPar)
            ->addGroupBy('Round')
            ->getQuery()
            ->getResult()
        ;
        
        return $this->getTotalStrikes($result);
    }
    
    private function getTotalStrikes($result) 
    {
        $strikes = 0;
        foreach ($result as $round) {
            $strikes += $round['strikes'];
        }
        
        return $strikes;
    }
}
