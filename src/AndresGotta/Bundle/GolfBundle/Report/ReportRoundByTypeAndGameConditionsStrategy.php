<?php

namespace AndresGotta\Bundle\GolfBundle\Report;

class ReportRoundByTypeAndGameConditionsStrategy extends BaseStrategy
{

    /**
     * REPORTE 1
     * 
     * Procesa el reporte de vueltas
     *
     * @return array
     * 
     * Del total de vueltas ingresadas en su historial, porcentajes de vueltas según cambiar el término por Condición de la Vuelta (torneo, practica y sus derivados)
     *  y condiciones de juego. Ordenar por porcentajes mayores a menores
     * 
     */
    public function generateStats()
    {
        $roundTypes = $this->generateRoundTypesStats();
        $gameConditions = $this->generateGameConditionsStats();

        return compact('roundTypes', 'gameConditions');
    }

    private function generateRoundTypesStats()
    {
        $this->preprocessFilters();

        $roundTypes = $this->queryBuilder
                ->select('Round.roundType, COUNT(Round.id) as quantity')
                ->from('AndresGottaGolfBundle:Round', 'Round')
                ->groupBy('Round.roundType')
                ->orderBy('quantity', 'desc')
                ->getQuery()
                ->getResult();

        $this->applyPercents($roundTypes);

        return $roundTypes;
    }

    private function generateGameConditionsStats()
    {
        $this->preprocessFilters();
        $gameConditions = $this->queryBuilder
                ->select('Round.gameCondition, COUNT(Round.id) as quantity')
                ->from('AndresGottaGolfBundle:Round', 'Round')
                ->groupBy('Round.gameCondition')
                ->orderBy('quantity', 'desc')
                ->getQuery()
                ->getResult();

        $this->applyPercents($gameConditions);

        return $gameConditions;
    }

    private function sumQuantity($data)
    {
        $sum = 0;
        foreach ($data as $item) {
            $sum += $item['quantity'];
        }

        return $sum;
    }

    private function applyPercents(&$data)
    {
        $sum = $this->sumQuantity($data);
        foreach ($data as &$item) {
            $item['quantity'] *= $sum ? 100 / $sum : 0;
        }
        unset($item);
    }

}
