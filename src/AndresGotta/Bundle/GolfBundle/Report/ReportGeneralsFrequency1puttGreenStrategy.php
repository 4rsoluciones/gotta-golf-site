<?php

namespace AndresGotta\Bundle\GolfBundle\Report;

use Doctrine\ORM\Query\Expr\Join;

class ReportGeneralsFrequency1puttGreenStrategy extends BaseStrategy
{
    /**
     * REPORTE 14
     * 
     * De todos los greens jugados se totalizan los 
     * que necesitan solo un putt para embocar, con 
     * este valor y el total de greens jugados se 
     * muestra su relación.
     */
    public function generateStats()
    {
        $count = 1;
        $totalGreenPlayedWith1Putt = $this->generateTotalGreenPlayed($count);
        $totalGreenPlayed = $this->generateTotalGreenPlayed();
        
        return compact('totalGreenPlayed', 'totalGreenPlayedWith1Putt');
    }
    
    private function generateTotalGreenPlayed($count = null)
    {
        $this->preprocessFilters();
        
        $result = $this->queryBuilder
                ->select('COUNT(Hole.id)')
                ->from('AndresGottaGolfBundle:Hole', 'Hole')
                ->innerJoin('Hole.round', 'Round')
                ;
        if ($count) {
            $result->andWhere($this->queryBuilder->expr()->notIn(
                           'Hole',
                           $this->queryBuilder->getEntityManager()
                              ->getRepository('AndresGottaGolfBundle:Hole')
                              ->createQueryBuilder('Hol')
                              ->innerJoin('AndresGottaGolfBundle:PuttApproach', 'PuttApproach', Join::WITH, 'PuttApproach.hole = Hol')
                              ->addGroupBy('Hol.id')
                              ->having("COUNT(Hole.id) >= $count")         
                              ->getDQL())
                    );
        }
        
        return $result->getQuery()->getSingleScalarResult();
    }
}
