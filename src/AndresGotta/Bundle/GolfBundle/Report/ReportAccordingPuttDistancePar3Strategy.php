<?php

namespace AndresGotta\Bundle\GolfBundle\Report;

use AndresGotta\Bundle\GolfBundle\ValueObject\Result;
use Doctrine\ORM\Query\Expr\Join;

class ReportAccordingPuttDistancePar3Strategy extends BaseStrategy
{
    /**
     * REPORTE 4.2.1 - PAR 3
     * 
     * Del total de golpes usados para la estadística 4.2 PAR 3, 
     * mostrar el porcentaje que obtuvo cada distancia del putt 
     * con el que se embocó la pelota. 
     */
    public function generateStats()
    {
        return $this->generateTotalStrikesByOutsideGreenReasons();
    }
    
    private function generateTotalStrikesByOutsideGreenReasons()
    {
        $this->preprocessFilters();
        $resultFirstStrike = Result::OUTSIDE_GREEN;
        $par = 3;
        $results = $this->queryBuilder
                ->select('Putt.distance, COUNT(FirstStrike.id) as quantity')
                ->from('AndresGottaGolfBundle:FirstStrike', 'FirstStrike')
                ->innerJoin('FirstStrike.hole', 'Hole')
                ->innerJoin('Hole.round', 'Round')
                ->innerJoin('AndresGottaGolfBundle:FirstStrikeApproach', 'FirstStrikeApproach', Join::WITH, 'FirstStrikeApproach.hole = Hole')
                ->innerJoin('AndresGottaGolfBundle:Putt', 'Putt', Join::WITH, 'Putt.hole = Hole')
                ->andWhere('Hole.par = :par')
                ->setParameter('par', $par)
                ->andWhere('FirstStrike.result LIKE :resultFirstStrike')
                ->setParameter('resultFirstStrike', "%{$resultFirstStrike}%")
                ->andWhere('FirstStrikeApproach.result = :resultFirstStrikeApproach')
                ->setParameter('resultFirstStrikeApproach', Result::INSIDE_GREEN)
                ->andWhere('Putt.result = :resultPutt')
                ->setParameter('resultPutt', Result::HOLE_OUT)
                ->addGroupBy('Putt.distance')
                ->getQuery()
                ->getResult()
                ;
                
        $this->applyPercents($results);
        
        return $results;
    }
    
    private function sumQuantity($data)
    {
        $sum = 0;
        foreach ($data as $item) {
            $sum += $item['quantity'];
        }

        return $sum;
    }

    private function applyPercents(&$data)
    {
        $sum = $this->sumQuantity($data);
        foreach ($data as &$item) {
            $item['quantity'] *= $sum ?  100 / $sum : 0;
        }
        unset($item);
    }
}
