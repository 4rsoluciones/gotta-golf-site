<?php

namespace AndresGotta\Bundle\GolfBundle\Report;

use AndresGotta\Bundle\GolfBundle\Entity\FirstStrike;
use AndresGotta\Bundle\GolfBundle\ValueObject\Reason;
use AndresGotta\Bundle\GolfBundle\ValueObject\Result;

class ReportFirstStrikePar3OutsideGreenReasonStrategy extends BaseStrategy
{
    /**
     * REPORTE 4 - PAR 3
     * 
     * Procesa el reporte de vueltas
     * 
     * Del total de tiros de salida en hoyos PAR 3 cargados fuera del green, que porcentaje tiene cada opción  desde cada rango de distancia (12 o 6 en total). No confundir con el 2, este reporte toma como tiros totales a los que fueron fuera del green unicamente) :
     *   a) corta, 
     *   b) larga, 
     *   c) por la derecha, 
     *   d) por la izquierda, 
     *   e) bunker
     *
     * @return array
     */
    public function generateStats()
    {
        $par = 3;
        $reasons = array(
            Reason::SHORT,
            Reason::LONG,
            Reason::TO_THE_RIGHT,
            Reason::TO_THE_LEFT,
        );
        $distances = FirstStrike::getDistanceChoicesByPar($par);
        
        foreach ($reasons as $reason) {
            foreach ($distances as $distance) {
                $results[$reason][$distance] = $this->generateOutsideGreenReasonStats($par, $reason, $distance);
            }
        }
        $this->applyPercents($results);
        
        return $results;
    }

    private function generateOutsideGreenReasonStats($par, $reason, $distance)
    {
        $this->preprocessFilters();
        $result = Result::OUTSIDE_GREEN;
        $results = $this->queryBuilder
                ->select('COUNT(FirstStrike.id)')
                ->from('AndresGottaGolfBundle:FirstStrike', 'FirstStrike')
                ->innerJoin('FirstStrike.hole', 'Hole')
                ->innerJoin('Hole.round', 'Round')
                ->andWhere('Hole.par = :par')
                ->setParameter('par', $par)
                ->andWhere('FirstStrike.result LIKE :result')
                ->setParameter('result', "%{$result}%")
                ->andWhere('FirstStrike.reason = :reason')
                ->setParameter('reason', $reason)
                ->andWhere('FirstStrike.distance = :distance')
                ->setParameter('distance', $distance)
                ->getQuery()
                ->getSingleScalarResult();

        return $results;
    }
        
    private function sumQuantity($data)
    {
        $sum = 0;
        foreach ($data as $item) {
            foreach ($item as $value) {
                $sum += $value;
            }
        }

        return $sum;
    }

    private function applyPercents(&$data)
    {
        $sum = $this->sumQuantity($data);
        foreach ($data as &$item) {
            foreach ($item as &$value) {
                $value *= $sum ?  100 / $sum : 0;
            }
        }
        unset($value);
        unset($item);
    }
    
}
