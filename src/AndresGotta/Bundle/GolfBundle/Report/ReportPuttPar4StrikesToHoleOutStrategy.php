<?php

namespace AndresGotta\Bundle\GolfBundle\Report;

use AndresGotta\Bundle\GolfBundle\Entity\Putt;
use AndresGotta\Bundle\GolfBundle\ValueObject\Result;
use Doctrine\ORM\Query\Expr\Join;

class ReportPuttPar4StrikesToHoleOutStrategy extends BaseStrategy
{
    /**
     * REPORTE 6 - PAR 4
     * 
     * Conocer el porcentaje de greens en regulación
     *
     * @return array
     */
    public function generateStats()
    {
        $par = 4;
        $distances = Putt::getDistanceChoicesByPar($par);
        
        foreach ($distances as $distance) {
            $puttHoleOutResults = $this->generatePuttHoleOutStats($par, $distance);
            $TwoPuttApproachsHoleOutResults = $this->generatePuttApproachsHoleOutStats($par, 2, $distance);
            $ThreePuttApproachsHoleOutResults = $this->generatePuttApproachsHoleOutStats($par, 3, $distance);
            $FourPuttApproachsHoleOutResults = $this->generatePuttApproachsHoleOutMoreThanStrikesStats($par, 4, $distance);
            
            $result['one_putt'][$distance] = (int)$puttHoleOutResults[0]['quantity'];
            $result['two_putt'][$distance] = count($TwoPuttApproachsHoleOutResults);
            $result['three_putt'][$distance] = count($ThreePuttApproachsHoleOutResults);
            $result['four_putt'][$distance] = count($FourPuttApproachsHoleOutResults);
        }       
        
        $this->applyPercents($result);
        
        return compact('result');
    }
    
    private function generatePuttHoleOutStats($par, $distance)
    {
        $this->preprocessFilters();
        $result = Result::HOLE_OUT;
        
        $results = $this->queryBuilder
                ->select('COUNT(Putt.id) as quantity')
                ->from('AndresGottaGolfBundle:Putt', 'Putt')
                ->innerJoin('Putt.hole', 'Hole')
                ->innerJoin('Hole.round', 'Round')
                ->andWhere("Putt.result = '{$result}'")
                ->andWhere("Hole.par = {$par}")
                ->andWhere("Putt.distance = '{$distance}'")
                ->getQuery()
                ->getResult();
        
        return $results;
    }
    
    private function generatePuttApproachsHoleOutStats($par, $quantityStrikes, $distance)
    {
        $this->preprocessFilters();
        
        $results = $this->queryBuilder
                ->select('Hole.id, COUNT(Hole.id) as quantity')
                ->from('AndresGottaGolfBundle:PuttApproach', 'PuttApproach')
                ->innerJoin('PuttApproach.hole', 'Hole')
                ->innerJoin('AndresGottaGolfBundle:Putt', 'Putt', Join::WITH, 'Putt.hole = Hole')
                ->innerJoin('Hole.round', 'Round')
                ->andWhere("Hole.par = {$par}")
                ->andWhere("Putt.distance = '{$distance}'")
                ->groupBy('Hole.id')
                ->having("quantity = {$quantityStrikes}")
                ->getQuery()
                ->getResult();
                
        return $results;
    }
    
    private function generatePuttApproachsHoleOutMoreThanStrikesStats($par, $quantityStrikes, $distance)
    {
        $this->preprocessFilters();
        
        $results = $this->queryBuilder
                ->select('Hole.id, COUNT(Hole.id) as quantity')
                ->from('AndresGottaGolfBundle:PuttApproach', 'PuttApproach')
                ->innerJoin('PuttApproach.hole', 'Hole')
                ->innerJoin('AndresGottaGolfBundle:Putt', 'Putt', Join::WITH, 'Putt.hole = Hole')
                ->innerJoin('Hole.round', 'Round')
                ->andWhere("Hole.par = {$par}")
                ->andWhere("Putt.distance = '{$distance}'")
                ->groupBy('Hole.id')
                ->having("quantity >= {$quantityStrikes}")
                ->getQuery()
                ->getResult();
                
        return $results;
    }
    
    
    private function sumQuantity($data)
    {
        $sum = 0;
        foreach ($data as $item) {
            foreach ($item as $value) {
                $sum += $value;
            }
        }

        return $sum;
    }

    private function applyPercents(&$data)
    {
        $sum = $this->sumQuantity($data);
        foreach ($data as &$item) {
            foreach ($item as &$value) {
                $value *= $sum ? 100 / $sum : 0;
            }
        }
        unset($value);
    }
    
}
