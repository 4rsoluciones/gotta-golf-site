<?php

namespace AndresGotta\Bundle\GolfBundle\Report;

interface ReportStrategyInterface
{
    /**
     * Genera las estadisticas
     * 
     * @return array
     */
    public function generateStats();
}
