<?php

namespace AndresGotta\Bundle\GolfBundle\Report;

use AndresGotta\Bundle\GolfBundle\Entity\SecondStrike;
use AndresGotta\Bundle\GolfBundle\ValueObject\Result;
use Doctrine\ORM\Query\Expr\Join;

class ReportAccordingToAreaAndDistancePar4Strategy extends BaseStrategy
{
    /**
     * REPORTE 4.2 - PAR 4
     */
    public function generateStats()
    {
        $par = 4;
        $reasons = SecondStrike::getReasonChoicesByPar($par);
        $distances = SecondStrike::getDistanceChoicesByPar($par);
        
        foreach ($reasons as $reason) {
            foreach ($distances as $distance) {
                $results[$reason][$distance] = 0;
            }
        }
        $this->newTotal($results);
        $this->applyPercents($results);
        
        return compact('results');
    }

    /**
     * @param $data
     */
    private function newTotal(&$data)
    {
        $query = $this->generateSubQuery('SecondStrike');

        /** @var SecondStrike $result */
        foreach ($query->getResult() as $result) {
            $hole = $result->getHole();
            if ($hole->getScore() === 3 || $hole->getScore() === 4) {
                $data[$result->getReason()][$result->getDistance()] += 1;
            }
        }
    }

    /**
     * @param $class
     * @param int $par
     * @return \Doctrine\ORM\Query
     */
    private function generateSubQuery($class, $par = 4)
    {
        $query = clone $this->queryBuilder;
        $query
            ->select('strike')
            ->from("AndresGottaGolfBundle:{$class}", "strike")
            ->innerJoin("strike.hole", 'Hole')
            ->innerJoin('Hole.round', 'Round')
            ->andWhere('Hole.par = :par')->setParameter('par', $par)
            ->andWhere('strike.result != :result AND strike.result != :result2')
            ->setParameter('result', Result::INSIDE_GREEN)->setParameter('result2', Result::HOLE_OUT)
        ;

        return $query->getQuery();
    }

    /**
     * @param $data
     * @return int
     */
    private function sumQuantity($data)
    {
        $sum = 0;
        foreach ($data as $item) {
            foreach ($item as $value) {
                $sum += $value;
            }
        }

        return $sum;
    }

    /**
     * @param $data
     */
    private function applyPercents(&$data)
    {
        $sum = $this->sumQuantity($data);
        foreach ($data as &$item) {
            foreach ($item as &$value) {
                $value *= $sum ? 100 / $sum : 0;
            }
        }
        unset($value);
    }
}
