<?php

namespace AndresGotta\Bundle\GolfBundle\Report;

use AndresGotta\Bundle\GolfBundle\ValueObject\Result;
use Doctrine\ORM\Query\Expr\Join;
use Doctrine\ORM\QueryBuilder;

class ReportGeneralsAverageResultsParStrategy extends BaseStrategy
{
    /**
     * REPORTE 20
     * 
     * Teniendo en cuenta cada tipo de resultado (hoyo en uno, etc, 
     * según el par y la cantidad de golpes realizados, ver la 
     * tabla para cada PAR) se totalizan estos y se calcula el 
     * promedio de cada uno sobre el total de vueltas. Es decir, que 
     * el reporte debería devolver un valor (promedio en este caso) 
     * en cada lugar de la tabla donde ahora se informa la cantidad 
     * de golpes que refiere cada término.
     */
    public function generateStats()
    {
//        $holesCounts = array(9, 18);
//        foreach ($holesCounts as $holeCount) {
//            $total[$holeCount] = intval($this->generateTotalRoundPlayed($holeCount));
//        }

        $holeCount = 18;
        $total[18] = intval($this->generateTotalRoundPlayed(18));
//        foreach ($holesCounts as $holeCount) {
            $result[18] = array(
                'Hoyo en uno' => array(
                    3 => $this->getTotalHoleOutStrikesByPar(3, 1, $holeCount),
                    4 => $this->getTotalHoleOutStrikesByPar(4, 1, $holeCount),
                    5 => $this->getTotalHoleOutStrikesByPar(5, 1, $holeCount)
                ),
                'Albatros' => array(
                    3 => null,
                    4 => null,
                    5 => $this->getTotalHoleOutStrikesByPar(5, 2, $holeCount)
                ),
                'Águila' => array(
                    3 => null,
                    4 => $this->getTotalHoleOutStrikesByPar(4, 2, $holeCount),
                    5 => $this->getTotalHoleOutStrikesByPar(5, 3, $holeCount)
                ),
                'Birdie' => array(
                    3 => $this->getTotalHoleOutStrikesByPar(3, 2, $holeCount),
                    4 => $this->getTotalHoleOutStrikesByPar(4, 3, $holeCount),
                    5 => $this->getTotalHoleOutStrikesByPar(5, 4, $holeCount)
                ),
                'Par' => array(
                    3 => $this->getTotalHoleOutStrikesByPar(3, 3, $holeCount),
                    4 => $this->getTotalHoleOutStrikesByPar(4, 4, $holeCount),
                    5 => $this->getTotalHoleOutStrikesByPar(5, 5, $holeCount)
                ),
                'Bogey' => array(
                    3 => $this->getTotalHoleOutStrikesByPar(3, 4, $holeCount),
                    4 => $this->getTotalHoleOutStrikesByPar(4, 5, $holeCount),
                    5 => $this->getTotalHoleOutStrikesByPar(5, 6, $holeCount)
                ),
                'Doble bogey o mas' => array(
                    3 => $this->getTotalHoleOutStrikesByPar(3, 5, $holeCount, true),
                    4 => $this->getTotalHoleOutStrikesByPar(4, 6, $holeCount, true),
                    5 => $this->getTotalHoleOutStrikesByPar(5, 7, $holeCount, true)
                )
            );
//        }

        return compact('result', 'total');
    }

    /**
     * @param $class
     * @param $par
     * @return mixed
     */
    private function getCountStrikesByClassAndHole($hole)
    {
        $strikes = array('FirstStrike', 'FirstStrikeApproach', 'PuttApproach', 'SecondStrike', 'SecondStrikeApproach', 'ThirdStrike', 'ThirdStrike');
        $count = 0;
        $queryBuilder = new QueryBuilder($this->queryBuilder->getEntityManager());
        foreach ($strikes as $strike) {
            $class = $strike;
            $queryBuilder->resetDQLParts();
            $queryBuilder->getParameters()->clear();

            $count += $queryBuilder->select("COUNT({$class}.id)")
                ->from("AndresGottaGolfBundle:{$class}", "{$class}")
                ->leftJoin("{$class}.hole", 'Hole')
                ->andWhere('Hole.id = :hole')->setParameter('hole', $hole['id'])
                ->getQuery()
                ->getSingleScalarResult()
            ;
        }

        return $count;
    }

    // retorna la cantidad de veces que completa 
    // un hoyo de $par en $strikes cantidad de golpes
    private function getTotalHoleOutStrikesByPar($par, $strikes, $holeCount, $dobleBogey = false)
    {
        $this->preprocessFilters();
        $holes = $this->queryBuilder
            ->select('Hole.id')
            ->from('AndresGottaGolfBundle:Hole', 'Hole')
            ->leftJoin('Hole.round', 'Round')
            ->andWhere('Hole.par = :par')->setParameter('par', $par)
            ->andWhere('Round.holeCount = :holeCount')->setParameter('holeCount', $holeCount)
            ->getQuery()
            ->getResult()
        ;

        $count = 0;
        foreach ($holes as $hole) {
            $holeStrikes = $this->getCountStrikesByClassAndHole($hole, $holeCount);
            if (!$dobleBogey && $holeStrikes == $strikes) {
                $count++;
            } elseif ($dobleBogey && $holeStrikes >= $strikes) {
                $count++;
            }
        }

        $holeQuantity = count($holes);
        $result = $holeQuantity > 0 ? $count / $holeQuantity * 100 : 0;

        return $result;
    }

    
    // retorna el total de vueltas jugadas
    private function generateTotalRoundPlayed($holeCount)
    {
        $this->preprocessFilters();

        $result = $this->queryBuilder
            ->select('COUNT(Round.id)')
            ->from('AndresGottaGolfBundle:Round', 'Round')
            ->andWhere('Round.holeCount = :holeCount')->setParameter('holeCount', $holeCount)
            ->getQuery()
            ->getSingleScalarResult()
        ;

        return $result;
    }


}
