<?php

namespace AndresGotta\Bundle\GolfBundle\Report;

use AndresGotta\Bundle\GolfBundle\ValueObject\Result;
use Doctrine\ORM\Query\Expr\Join;

class ReportGeneralsAverageTotalPuttsStrategy extends BaseStrategy
{
    /**
     * REPORTE 17
     * 
     * Se suman los putts jugados y las vueltas cargadas, 
     * luego se calcula el promedio entre estos valores.
     */
    public function generateStats()
    {
        $holesCounts = array(9, 18);
        foreach ($holesCounts as $holeCount) {
            $totalPutts[$holeCount] = $this->generateTotalPutts($holeCount);
            $totalRoundPlayed[$holeCount] = $this->generateTotalRoundPlayed($holeCount);
        }
        
        return compact('totalRoundPlayed', 'totalPutts');
    }
    
    private function generateTotalRoundPlayed($holeCount)
    {
        $this->preprocessFilters();
        
        $result = $this->queryBuilder
            ->select('COUNT(Round.id)')
            ->from('AndresGottaGolfBundle:Round', 'Round')
            ->andWhere('Round.holeCount = :holeCount')->setParameter('holeCount', $holeCount)
            ->getQuery()
            ->getSingleScalarResult()
        ;
        
        return $result;
    }
    
    private function generateTotalPutts($holeCount)
    {
        $this->preprocessFilters();
        
        $totalPutt = $this->queryBuilder
            ->select('COUNT(Putt.id)')
            ->from('AndresGottaGolfBundle:Putt', 'Putt')
            ->innerJoin('Putt.hole', 'Hole')
            ->innerJoin('Hole.round', 'Round')
            ->andWhere('Round.holeCount = :holeCount')->setParameter('holeCount', $holeCount)
            ->getQuery()
            ->getSingleScalarResult()
        ;
        
        $this->preprocessFilters();
        
        $totalPuttApproach = $this->queryBuilder
            ->select('COUNT(PuttApproach.id)')
            ->from('AndresGottaGolfBundle:PuttApproach', 'PuttApproach')
            ->innerJoin('PuttApproach.hole', 'Hole')
            ->innerJoin('Hole.round', 'Round')
            ->andWhere('Round.holeCount = :holeCount')->setParameter('holeCount', $holeCount)
            ->getQuery()
            ->getSingleScalarResult()
        ;
        
        return $totalPutt + $totalPuttApproach;
    }
}
