<?php

namespace AndresGotta\Bundle\GolfBundle\Report;

use AndresGotta\Bundle\GolfBundle\ValueObject\Result;

class ReportThirdStrikePar5InsideGreenStrategy extends BaseStrategy
{
    /**
     * REPORTE 2 - PAR 5 b)
     * 
     * Resultado (en porcentaje) del tercer tiro en cada hoyo par 5 en cada rango de distancia (8 en total) ( porcentajes desde cada distancia,  
     *   a)embocada (Águila)
     *   b) en green – 
     *   c) fuera del green y sus derivados (5 en total) , 
     *
     * @return array
     */
    public function generateStats()
    {
        $this->preprocessFilters();

        $par = 5;

        $data = $this->queryBuilder
                ->select('ThirdStrike.distance, COUNT(ThirdStrike.id) as quantity')
                ->from('AndresGottaGolfBundle:ThirdStrike', 'ThirdStrike')
                ->innerJoin('ThirdStrike.hole', 'Hole')
                ->innerJoin('Hole.round', 'Round')
                ->andWhere('Hole.par = :par')
                ->setParameter('par', $par)
                ->andWhere('ThirdStrike.result = :result')
                ->setParameter('result', Result::INSIDE_GREEN)
                ->addGroupBy('ThirdStrike.distance')
                ->orderBy('quantity', 'desc')
                ->getQuery()
                ->getResult();
        //TODO: Optimizar

        $sum = 0;
        foreach ($data as $item) {
            $sum += $item['quantity'];
        }
        foreach ($data as &$item) {
            $item['quantity'] *= $sum ? 100 / $sum : 0;
        }
        unset($item);

        return $data;
    }
}
