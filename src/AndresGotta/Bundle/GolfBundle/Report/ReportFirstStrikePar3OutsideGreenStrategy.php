<?php

namespace AndresGotta\Bundle\GolfBundle\Report;

use AndresGotta\Bundle\GolfBundle\ValueObject\Result;

class ReportFirstStrikePar3OutsideGreenStrategy extends BaseStrategy
{
    /**
     * REPORTE 2 - PAR 3
     * 
     * Procesa el reporte de vueltas
     * 
     * Resultado (en porcentaje) del tiro de salida en cada hoyo par 3 en cada rango de distancia (8 en total) ( porcentajes de cada resultado obtenido desde cada distancia)
     * c) fuera del green y sus derivados (5 en total) , 
     *
     * @return array
     */
    public function generateStats()
    {
        $par = 3;

        $distancesResults = $this->generateDistanceOutsideGreenStats($par);
        $reasonResults = $this->generateReasonOutsideGreenStats($par);

        return compact('distancesResults', 'reasonResults');
    }
    
    private function generateDistanceOutsideGreenStats($par)
    {
        $this->preprocessFilters();
        $result = Result::OUTSIDE_GREEN;
        $results = $this->queryBuilder
                ->select('FirstStrike.distance, COUNT(FirstStrike.id) as quantity')
                ->from('AndresGottaGolfBundle:FirstStrike', 'FirstStrike')
                ->innerJoin('FirstStrike.hole', 'Hole')
                ->innerJoin('Hole.round', 'Round')
                ->andWhere('Hole.par = :par')
                ->setParameter('par', $par)
                ->andWhere('FirstStrike.result LIKE :result')
                ->setParameter('result', "%{$result}%")
                ->addGroupBy('FirstStrike.distance')
                ->orderBy('quantity', 'desc')
                ->getQuery()
                ->getResult();
        
        $this->applyPercents($results);
        
        return $results;
    }
    
    private function generateReasonOutsideGreenStats($par)
    {
        $this->preprocessFilters();
        $result = Result::OUTSIDE_GREEN;
        $results = $this->queryBuilder
                ->select('FirstStrike.reason, COUNT(FirstStrike.id) as quantity')
                ->from('AndresGottaGolfBundle:FirstStrike', 'FirstStrike')
                ->innerJoin('FirstStrike.hole', 'Hole')
                ->innerJoin('Hole.round', 'Round')
                ->andWhere('Hole.par = :par')
                ->setParameter('par', $par)
                ->andWhere('FirstStrike.result LIKE :result')
                ->setParameter('result', "%{$result}%")
                ->groupBy('FirstStrike.reason')
                ->orderBy('quantity', 'desc')
                ->getQuery()
                ->getResult();
        
        $this->applyPercents($results);
        
        return $results;
    }
    
    private function sumQuantity($data)
    {
        $sum = 0;
        foreach ($data as $item) {
            $sum += $item['quantity'];
        }

        return $sum;
    }

    private function applyPercents(&$data)
    {
        $sum = $this->sumQuantity($data);
        foreach ($data as &$item) {
            $item['quantity'] *= $sum ?  100 / $sum : 0;
        }
        unset($item);
    }
    
}
