<?php

namespace AndresGotta\Bundle\GolfBundle\Report;

use Doctrine\ORM\Query\Expr\Join;
use Doctrine\ORM\QueryBuilder;

class ReportGeneralsBirdiesFrequencyParBogueysStrategy extends BaseStrategy
{
    /**
     * REPORTE 21
     * 
     * Tomando el total de birdies, pares y bogueys 
     * (ver tabla anterior para saber qué cantidad 
     * de golpes es cada uno en cada PAR y el total 
     * de vueltas se saca un promedio de estos y se 
     * devuelve un valor en Birdies, Pares o Bogey.
     */
    public function generateStats()
    {
        $holesCounts = array(9, 18);
        foreach ($holesCounts as $holeCount) {
            $total[$holeCount] = $this->generateTotalRoundPlayed($holeCount);
        }

        foreach ($holesCounts as $holeCount) {
            $birdie = $this->getTotalHoleOutStrikesByPar(3, 2, $holeCount)
                    + $this->getTotalHoleOutStrikesByPar(4, 3, $holeCount)
                    + $this->getTotalHoleOutStrikesByPar(5, 4, $holeCount);
            $par = $this->getTotalHoleOutStrikesByPar(3, 3, $holeCount) 
                    + $this->getTotalHoleOutStrikesByPar(4, 4, $holeCount) 
                    + $this->getTotalHoleOutStrikesByPar(5, 5, $holeCount);
            $bogey = $this->getTotalHoleOutStrikesByPar(3, 4, $holeCount) 
                    + $this->getTotalHoleOutStrikesByPar(4, 5, $holeCount) 
                    + $this->getTotalHoleOutStrikesByPar(5, 6, $holeCount);

            $result[$holeCount] = array(
                'Birdie' => $birdie,
                'Par' => $par,
                'Bogey' => $bogey
            );
        }
        
        return compact('total', 'result');
    }

    /**
     * @param $class
     * @param $par
     * @return mixed
     */
    private function getCountStrikesByClassAndHole($hole)
    {
        $strikes = array('FirstStrike', 'FirstStrikeApproach', 'PuttApproach', 'SecondStrike', 'SecondStrikeApproach', 'ThirdStrike', 'ThirdStrike');
        $count = 0;
        $queryBuilder = new QueryBuilder($this->queryBuilder->getEntityManager());
        foreach ($strikes as $strike) {
            $class = $strike;
            $queryBuilder->resetDQLParts();
            $queryBuilder->getParameters()->clear();

            $count += $queryBuilder->select("COUNT({$class}.id)")
                ->from("AndresGottaGolfBundle:{$class}", "{$class}")
                ->leftJoin("{$class}.hole", 'Hole')
                ->andWhere('Hole.id = :hole')->setParameter('hole', $hole['id'])
                ->getQuery()
                ->getSingleScalarResult()
            ;
        }

        return $count;

    }

    // retorna la cantidad de veces que completa 
    // un hoyo de $par en $strikes cantidad de golpes
    private function getTotalHoleOutStrikesByPar($par, $strikes, $holeCount, $dobleBogey = false)
    {
        $this->preprocessFilters();
        $holes = $this->queryBuilder
            ->select('Hole.id')
            ->from('AndresGottaGolfBundle:Hole', 'Hole')
            ->leftJoin('Hole.round', 'Round')
            ->andWhere('Hole.par = :par')->setParameter('par', $par)
            ->andWhere('Round.holeCount = :holeCount')->setParameter('holeCount', $holeCount)
            ->getQuery()
            ->getResult()
        ;

        $count = 0;
        foreach ($holes as $hole) {
            $holeStrikes = $this->getCountStrikesByClassAndHole($hole, $holeCount);
            if (!$dobleBogey && $holeStrikes == $strikes) {
                $count++;
            } elseif ($dobleBogey && $holeStrikes >= $strikes) {
                $count++;
            }
        }

        return $count;
    }

    // retorna el total de vueltas jugadas
    private function generateTotalRoundPlayed($holeCount)
    {
        $this->preprocessFilters();
        
        $result = $this->queryBuilder
                ->select('COUNT(Round.id)')
                ->from('AndresGottaGolfBundle:Round', 'Round')
                ->andWhere('Round.holeCount = :holeCount')->setParameter('holeCount', $holeCount)
                ->getQuery()
                ->getSingleScalarResult()
                ;
        
        return $result;
    }
}
