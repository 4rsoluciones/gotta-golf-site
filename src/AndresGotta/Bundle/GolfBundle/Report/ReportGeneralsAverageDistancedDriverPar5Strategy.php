<?php

namespace AndresGotta\Bundle\GolfBundle\Report;

use AndresGotta\Bundle\GolfBundle\ValueObject\BallFlight;
use AndresGotta\Bundle\GolfBundle\ValueObject\Club;
use AndresGotta\Bundle\GolfBundle\ValueObject\Distance;

class ReportGeneralsAverageDistancedDriverPar5Strategy extends BaseStrategy
{
    /**
     * REPORTE 11 - PAR 5
     */
    public function generateStats()
    {
        foreach (BallFlight::getChoices() as $ballFlight) {
            $firstStrikeDistances = $this->getFirstStrikeDistanceQuantityByBallFlight($ballFlight);
            $result[$ballFlight] = $this->calculateDistanceAverage($firstStrikeDistances, $this->sumQuantity($firstStrikeDistances));
        }

        return $result;
    }
    
    private function getFirstStrikeDistanceQuantityByBallFlight($ballFlight)
    {
        $this->preprocessFilters();
        
        $par = 5;
        $results = $this->queryBuilder
            ->select('FirstStrike.distance, COUNT(FirstStrike.id) as quantity')
            ->from('AndresGottaGolfBundle:FirstStrike', 'FirstStrike')
            ->innerJoin('FirstStrike.hole', 'Hole')
            ->innerJoin('Hole.round', 'Round')
            ->andWhere('Hole.par = :par')->setParameter('par', $par)
            ->andWhere('FirstStrike.club = :club')->setParameter('club', Club::DRIVER)
            ->andWhere('FirstStrike.ballFlight = :ballFlight')->setParameter('ballFlight', $ballFlight)
            ->addGroupBy('FirstStrike.distance')
            ->getQuery()
            ->getResult()
        ;
        
        return $results;
    }
    
    private function sumQuantity($data)
    {
        $sum = 0;
        foreach ($data as $item) {
            $sum += intval($item['quantity']);
        }

        return $sum;
    }
    
    private function calculateDistanceAverage($data, $total)
    {
        $distances = array(
            Distance::LESS_THAN_180_YDS  => array('value' => 170, 'min' => 0, 'max' => 180),
            Distance::BTW_181_TO_200_YDS => array('value' => 190, 'min' => 181, 'max' => 200),
            Distance::BTW_201_TO_220_YDS => array('value' => 210, 'min' => 201, 'max' => 220),
            Distance::BTW_221_TO_240_YDS => array('value' => 230, 'min' => 221, 'max' => 240),
            Distance::BTW_241_TO_260_YDS => array('value' => 250, 'min' => 241, 'max' => 260),
            Distance::BTW_261_TO_280_YDS => array('value' => 270, 'min' => 261, 'max' => 280),
            Distance::BTW_281_TO_300_YDS => array('value' => 290, 'min' => 281, 'max' => 300),
            Distance::BTW_301_TO_320_YDS => array('value' => 310, 'min' => 301, 'max' => 320),
            Distance::BTW_321_TO_340_YDS => array('value' => 330, 'min' => 321, 'max' => 340),
            Distance::MORE_THAN_340_YDS => array('value' => 350, 'min' => 340, 'max' => 360),
        );
        
        $sum = 0;
        foreach ($data as $item) {
            if (array_key_exists($item['distance'], $distances)) {
                $sum += $distances[$item['distance']]['value'] * $item['quantity'];
            }
        }
        $average = $total ? $sum / $total : 0;
        $distanceKey =  'Ninguno';
        if ($average) {
            foreach ($distances as $key => $distance) {
                if ($distance['min'] <= $average && $distance['max'] >= $average) {
                    $distanceKey = $key;
                    break;
                }
            }
        }

        return array('total' => $total, 'distance' => $distanceKey);
    }

}
