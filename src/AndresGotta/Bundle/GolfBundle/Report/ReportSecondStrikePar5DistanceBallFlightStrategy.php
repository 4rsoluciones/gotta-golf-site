<?php

namespace AndresGotta\Bundle\GolfBundle\Report;

use AndresGotta\Bundle\GolfBundle\Entity\SecondStrike;
use AndresGotta\Bundle\GolfBundle\ValueObject\BallFlight;
use AndresGotta\Bundle\GolfBundle\ValueObject\Club;
use AndresGotta\Bundle\GolfBundle\ValueObject\Reason;
use AndresGotta\Bundle\GolfBundle\ValueObject\Result;

class ReportSecondStrikePar5DistanceBallFlightStrategy extends BaseStrategy
{
    /**
     * REPORTE 9.1 - PAR 5
     * 
     * A la estadística 9 PAR 5, le agrego ahora 
     * mostrarlo según el vuelo de pelota en cada 
     * grupo de resultado, palo y distancia.
     */
    public function generateStats()
    {
        $par = 5;
        $data = array();
        $clubs = Club::getChoices();
        $distances = SecondStrike::getDistanceChoicesByPar($par);
        $results = array(Result::HOLE_OUT, Result::INSIDE_GREEN, Result::INSIDE_FAIRWAY, Result::OUTSIDE_FAIRWAY);
        $reasons = array(Reason::TO_THE_LEFT, Reason::TO_THE_RIGHT);
        $ballFlights = BallFlight::getChoices();
        
        foreach ($clubs as $club) {
            foreach ($ballFlights as $ballFlight) {
                foreach ($distances as $distance) {
                    foreach ($results as $result) {
                        if ($result != Result::OUTSIDE_FAIRWAY) {
                            $data[$club][$ballFlight][$distance][$result] = $this->generateSecondStrikePar5DistanceResultStats($par, $ballFlight, $distance, $club, $result);
                        }
                    }
                }
            }
        }
        foreach ($clubs as $club) {
            foreach ($ballFlights as $ballFlight) {
                foreach ($distances as $distance) {
                    foreach ($results as $result) {
                        if ($result == Result::OUTSIDE_FAIRWAY) {
                            $data[$club][$ballFlight][$distance][$reasons[0]] = $this->generateSecondStrikePar5DistanceResultReasonStats($par, $ballFlight, $distance, $club, $result, $reasons[0]);
                            $data[$club][$ballFlight][$distance][$reasons[1]] = $this->generateSecondStrikePar5DistanceResultReasonStats($par, $ballFlight, $distance, $club, $result, $reasons[1]);
                        }
                    }
                }
            }
        }
        $this->applyPercents($data);
        
        return $data;
    }
    
    private function generateSecondStrikePar5DistanceResultStats($par, $ballFlight, $distance, $club, $result)
    {        
        $this->preprocessFilters();
        
        $results = $this->queryBuilder
                ->select('COUNT(SecondStrike.id) as quantity')
                ->from('AndresGottaGolfBundle:SecondStrike', 'SecondStrike')
                ->innerJoin('SecondStrike.hole', 'Hole')
                ->innerJoin('Hole.round', 'Round')
                ->andWhere('Hole.par = :par')
                ->setParameter('par', $par)
                ->andWhere('SecondStrike.club = :club')
                ->setParameter('club', $club)
                ->andWhere('SecondStrike.distance = :distance')
                ->setParameter('distance', $distance)
                ->andWhere('SecondStrike.result LIKE :result')
                ->setParameter('result', "%{$result}%")
                ->andWhere('SecondStrike.ballFlight = :ballFlight')
                ->setParameter('ballFlight', $ballFlight)
                ;
        
        return $results->getQuery()->getSingleScalarResult();
    }
    
    private function generateSecondStrikePar5DistanceResultReasonStats($par, $ballFlight, $distance, $club, $result, $reason)
    {        
        $this->preprocessFilters();
        
        $results = $this->queryBuilder
                ->select('COUNT(SecondStrike.id) as quantity')
                ->from('AndresGottaGolfBundle:SecondStrike', 'SecondStrike')
                ->innerJoin('SecondStrike.hole', 'Hole')
                ->innerJoin('Hole.round', 'Round')
                ->andWhere('Hole.par = :par')
                ->setParameter('par', $par)
                ->andWhere('SecondStrike.club = :club')
                ->setParameter('club', $club)
                ->andWhere('SecondStrike.distance = :distance')
                ->setParameter('distance', $distance)
                ->andWhere('SecondStrike.result LIKE :result')
                ->setParameter('result', "%{$result}%")
                ->andWhere('SecondStrike.reason = :reason')
                ->setParameter('reason', $reason)
                ->andWhere('SecondStrike.ballFlight = :ballFlight')
                ->setParameter('ballFlight', $ballFlight)
                ;
        
        return $results->getQuery()->getSingleScalarResult();
    }

    private function sumQuantity($data)
    {
        $sum = 0;
        foreach ($data as $items) {
            foreach ($items as $item) {
                $sum += $item;
            }
        }

        return $sum;
    }

    private function applyPercents(&$data)
    {
        foreach ($data as &$club) {
            foreach ($club as &$ballFlight) {
            $sum = $this->sumQuantity($ballFlight);
                foreach ($ballFlight as &$distance) {
                    foreach ($distance as &$result) {
                        $result *= $sum ? 100 / $sum : 0;
                    }
                    unset($result);
                }
            }
        }
    }
    
}
