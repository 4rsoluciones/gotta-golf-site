<?php

namespace AndresGotta\Bundle\GolfBundle\Report;

use AndresGotta\Bundle\GolfBundle\ValueObject\Distance;
use AndresGotta\Bundle\GolfBundle\ValueObject\Result;
use AndresGotta\Bundle\GolfBundle\ValueObject\StrikeLie;
use Doctrine\ORM\Query\Expr\Join;

class ReportTrendsLieShotDistanceResultsStrategy extends BaseStrategy
{
    /**
     * REPORTE 22 PAR 5
     *
     * Se suman todos los golpes en donde se le pregunto
     * el lie del tiro y se los agrupa según la distancia.
     * Mostrar los porcentajes desde cada distancia que
     * obtuvo cada resultado del tiro y sus motivos.
     */
    public function generateStats()
    {
        $strikeLies = array(
            StrikeLie::BALL_AT_YOUR_FEET,
            StrikeLie::BALL_ABOVE_FEET . ',' . StrikeLie::ON_CLIMB,
            StrikeLie::BALL_ABOVE_FEET . ',' . StrikeLie::ON_DESCEND,
            StrikeLie::BALL_BELOW_FEET . ',' . StrikeLie::ON_CLIMB,
            StrikeLie::BALL_BELOW_FEET . ',' . StrikeLie::ON_DESCEND,
        );
        $distances = array(
            Distance::LESS_THAN_20_YDS,
            Distance::BTW_21_TO_40_YDS,
            Distance::BTW_41_TO_60_YDS,
            Distance::BTW_61_TO_80_YDS,
            Distance::BTW_81_TO_100_YDS,
            Distance::BTW_100_TO_120_YDS,
            Distance::BTW_121_TO_140_YDS,
            Distance::BTW_141_TO_160_YDS,
            Distance::BTW_161_TO_180_YDS,
            Distance::BTW_181_TO_200_YDS,
            Distance::BTW_201_TO_220_YDS,
            Distance::BTW_221_TO_240_YDS,
            Distance::BTW_241_TO_260_YDS,
            Distance::BTW_261_TO_280_YDS,
            Distance::BTW_281_TO_300_YDS,
            Distance::BTW_301_TO_320_YDS,
            Distance::MORE_THAN_320_YDS,
        );
        $results = array(
            Result::INSIDE_GREEN,
            Result::OUTSIDE_GREEN,
            Result::INSIDE_FAIRWAY,
            Result::OUTSIDE_FAIRWAY,
            Result::HOLE_OUT,
        );

        $data = [];
        foreach ($strikeLies as $strikeLie) {
            foreach ($results as $result) {
                foreach ($distances as $distance) {
                    $data[$strikeLie][$result][$distance] = $this->getTotalBy($strikeLie, $result, $distance);
                }
            }
            $this->applyPercents($data[$strikeLie]);
        }

        return $data;
    }

    private function getTotalBy($strikeLie, $result, $distance)
    {
        $this->preprocessFilters();
        $secondStrikeResult = $this->queryBuilder
            ->select('COUNT(SecondStrike.id)')
            ->from('AndresGottaGolfBundle:Hole', 'Hole')
            ->innerJoin('AndresGottaGolfBundle:Round', 'Round', Join::WITH, 'Hole.round = Round')
            ->innerJoin('AndresGottaGolfBundle:SecondStrike', 'SecondStrike', Join::WITH, 'SecondStrike.hole = Hole')
            ->andWhere('SecondStrike.strikeLies = :strikeLie')->setParameter('strikeLie', $strikeLie)
            ->andWhere('SecondStrike.result LIKE :result')->setParameter('result', "%{$result}%")
            ->andWhere('SecondStrike.distance = :distance')->setParameter('distance', $distance)
            ->getQuery()
            ->getSingleScalarResult()
        ;

        $this->preprocessFilters();
        $thirdStrikeResult = $this->queryBuilder
            ->select('COUNT(ThirdStrike.id)')
            ->from('AndresGottaGolfBundle:Hole', 'Hole')
            ->innerJoin('AndresGottaGolfBundle:Round', 'Round', Join::WITH, 'Hole.round = Round')
            ->innerJoin('AndresGottaGolfBundle:ThirdStrike', 'ThirdStrike', Join::WITH, 'ThirdStrike.hole = Hole')
            ->andWhere('ThirdStrike.strikeLies = :strikeLie')->setParameter('strikeLie', $strikeLie)
            ->andWhere('ThirdStrike.result LIKE :result')->setParameter('result', "%{$result}%")
            ->andWhere('ThirdStrike.distance = :distance')->setParameter('distance', $distance)
            ->getQuery()
            ->getSingleScalarResult()
        ;

        $this->preprocessFilters();
        $thirdStrikeApproachResult = $this->queryBuilder
            ->select('COUNT(ThirdStrikeApproach.id)')
            ->from('AndresGottaGolfBundle:Hole', 'Hole')
            ->innerJoin('AndresGottaGolfBundle:Round', 'Round', Join::WITH, 'Hole.round = Round')
            ->innerJoin('AndresGottaGolfBundle:ThirdStrikeApproach', 'ThirdStrikeApproach', Join::WITH, 'ThirdStrikeApproach.hole = Hole')
            ->andWhere('ThirdStrikeApproach.strikeLies = :strikeLie')->setParameter('strikeLie', $strikeLie)
            ->andWhere('ThirdStrikeApproach.result LIKE :result')->setParameter('result', "%{$result}%")
            ->andWhere('ThirdStrikeApproach.distance = :distance')->setParameter('distance', $distance)
            ->getQuery()
            ->getSingleScalarResult()
        ;

        return $secondStrikeResult + $thirdStrikeResult + $thirdStrikeApproachResult;
    }

    private function sumQuantity($data)
    {
        $sum = 0;
        foreach ($data as $item) {
            foreach ($item as $value) {
                $sum += $value;
            }
        }

        return $sum;
    }

    private function applyPercents(&$data)
    {
        $sum = $this->sumQuantity($data);
        foreach ($data as &$item) {
            foreach ($item as &$value) {
                $value *= $sum ?  100 / $sum : 0;
            }
        }
        unset($value);
    }
}
