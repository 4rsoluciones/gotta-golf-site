<?php

namespace AndresGotta\Bundle\GolfBundle\Report;

use AndresGotta\Bundle\GolfBundle\ValueObject\Result;
use Doctrine\ORM\Query\Expr\Join;

class ReportAccordingPuttDistancePar5Strategy extends BaseStrategy
{
    /**
     * REPORTE 4.2.1 - PAR 5
     */
    public function generateStats()
    {
        return $this->generateTotalStrikesByOutsideGreenReasons();
    }
    
    private function generateTotalStrikesByOutsideGreenReasons()
    {
        $this->preprocessFilters();
        $resultThirdStrike = Result::OUTSIDE_GREEN;
        $par = 5;
        $results = $this->queryBuilder
                ->select('Putt.distance, COUNT(ThirdStrike.id) as quantity')
                ->from('AndresGottaGolfBundle:ThirdStrike', 'ThirdStrike')
                ->innerJoin('ThirdStrike.hole', 'Hole')
                ->innerJoin('Hole.round', 'Round')
                ->innerJoin('AndresGottaGolfBundle:ThirdStrikeApproach', 'ThirdStrikeApproach', Join::WITH, 'ThirdStrikeApproach.hole = Hole')
                ->innerJoin('AndresGottaGolfBundle:Putt', 'Putt', Join::WITH, 'Putt.hole = Hole')
                ->andWhere('Hole.par = :par')
                ->setParameter('par', $par)
                ->andWhere('ThirdStrike.result LIKE :resultThirdStrike')
                ->setParameter('resultThirdStrike', "%{$resultThirdStrike}%")
                ->andWhere('ThirdStrikeApproach.result = :resultThirdStrikeApproach')
                ->setParameter('resultThirdStrikeApproach', Result::INSIDE_GREEN)
                ->andWhere('Putt.result = :resultPutt')
                ->setParameter('resultPutt', Result::HOLE_OUT)
                ->addGroupBy('Putt.distance')
                ->getQuery()
                ->getResult()
                ;
                
        $this->applyPercents($results);
        
        return $results;
    }
    
    private function sumQuantity($data)
    {
        $sum = 0;
        foreach ($data as $item) {
            $sum += $item['quantity'];
        }

        return $sum;
    }

    private function applyPercents(&$data)
    {
        $sum = $this->sumQuantity($data);
        foreach ($data as &$item) {
            $item['quantity'] *= $sum ?  100 / $sum : 0;
        }
        unset($item);
    }
}
