<?php

namespace AndresGotta\Bundle\GolfBundle\Report;

use AndresGotta\Bundle\GolfBundle\Entity\Report;
use AndresGotta\Bundle\GolfBundle\Report\ReportDTO;

/**
 * 
 */
class ReportBuilder
{

    /**
     *
     * @var Report
     */
    private $report;

    /**
     *
     * @var ReportStrategyInterface
     */
    private $strategy;

    /**
     * 
     * @param Report $report
     * @param ReportStrategyInterface $strategy
     */
    public function __construct(Report $report, ReportStrategyInterface $strategy)
    {
        $this->report = $report;
        $this->strategy = $strategy;
    }

    /**
     * 
     * @return ReportDTO
     */
    public function generate()
    {
        $data = $this->strategy->generateStats();
        
        $report = new ReportDTO($this->report, $data);
        
        return $report;
    }

}
