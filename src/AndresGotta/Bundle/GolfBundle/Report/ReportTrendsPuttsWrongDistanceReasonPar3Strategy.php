<?php

namespace AndresGotta\Bundle\GolfBundle\Report;

use AndresGotta\Bundle\GolfBundle\Entity\Putt;
use AndresGotta\Bundle\GolfBundle\ValueObject\Reason;
use AndresGotta\Bundle\GolfBundle\ValueObject\Result;

class ReportTrendsPuttsWrongDistanceReasonPar3Strategy extends BaseStrategy
{
    /**
     * REPORTE 7.1 - PAR 3
     * 
     * En los hoyos par 3, teniendo en cuenta solo los putts que 
     * tuvieron como resultado errado (más de un putt, sin 
     * considerar el putt que embocó) y agrupados por distancia 
     * de golpe, calculo el porcentaje que tiene cada uno de 
     * los valores de motivos, en cada grupo de distancia de juego.
     */
    public function generateStats()
    {
        $par = 3;
        $distances = Putt::getDistanceChoicesByPar($par);
        $reasons = array(
            Reason::SHORT,
            Reason::LONG,
            Reason::TO_THE_RIGHT,
            Reason::TO_THE_LEFT
        );
        
        foreach ($reasons as $reason) {
            foreach ($distances as $distance) {
                $puttTotal = $this->generatePuttTotalByReason($par, $reason, $distance);
                $puttApproachTotal = $this->generatePuttApproachTotalByReason($par, $reason, $distance);
                $results[$reason][$distance] = $puttTotal + $puttApproachTotal;
            }
        }
        $this->applyPercents($results);
                
        return $results;
    }
    
    // calculo el total de putt errados
    // agrupado por razones
    private function generatePuttTotalByReason($par, $reason, $distance)
    {
        $this->preprocessFilters();

        $data = $this->queryBuilder
                ->select('COUNT(Putt.id)')
                ->from('AndresGottaGolfBundle:Putt', 'Putt')
                ->innerJoin('Putt.hole', 'Hole')
                ->innerJoin('Hole.round', 'Round')
                ->andWhere('Hole.par = :par')
                ->setParameter('par', $par)
                ->andWhere('Putt.result = :result')
                ->setParameter('result', Result::MISSED)
                ->andWhere('Putt.reason = :reason')
                ->setParameter('reason', $reason)
                ->andWhere('Putt.distance = :distance')
                ->setParameter('distance', $distance)
                ->getQuery()
                ->getSingleScalarResult();
        
        return $data;
    }
    
    // calculo el total de putt-approach errados
    // agrupado por razones
    private function generatePuttApproachTotalByReason($par, $reason, $distance)
    {
        $this->preprocessFilters();

        $data = $this->queryBuilder
                ->select('COUNT(PuttApproach.id)')
                ->from('AndresGottaGolfBundle:PuttApproach', 'PuttApproach')
                ->innerJoin('PuttApproach.hole', 'Hole')
                ->innerJoin('Hole.round', 'Round')
                ->andWhere('Hole.par = :par')
                ->setParameter('par', $par)
                ->andWhere('PuttApproach.result = :result')
                ->setParameter('result', Result::MISSED)
                ->andWhere('PuttApproach.reason = :reason')
                ->setParameter('reason', $reason)
                ->andWhere('PuttApproach.distance = :distance')
                ->setParameter('distance', $distance)
                ->getQuery()
                ->getSingleScalarResult();

        return $data;
    }
    
      
    private function sumQuantity($data)
    {
        $sum = 0;
        foreach ($data as $item) {
            foreach ($item as $value) {
                $sum += $value;
            }
        }

        return $sum;
    }

    private function applyPercents(&$data)
    {
        $sum = $this->sumQuantity($data);
        foreach ($data as &$item) {
            foreach ($item as &$value) {
                $value *= $sum ?  100 / $sum : 0;
            }
        }
        unset($value);
        unset($item);
    }
}
