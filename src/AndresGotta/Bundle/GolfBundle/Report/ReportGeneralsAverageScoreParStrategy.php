<?php

namespace AndresGotta\Bundle\GolfBundle\Report;

/**
 * Class ReportGeneralsAverageScoreParStrategy
 * @package AndresGotta\Bundle\GolfBundle\Report
 */
class ReportGeneralsAverageScoreParStrategy extends BaseStrategy
{
    /**
     * REPORTE 19
     * 
     * Se suman todos los golpes de todas las vueltas completadas 
     * y se lo resta al total de los pares de las vueltas. , Luego 
     * se calcula el promedio sobre el total de vueltas y esa 
     * diferencia. Ejemplo: si juega 3 vueltas de 18 hoyos en una 
     * cancha PAR 70, y realiza 71 golpes en cada vuelta, tiene un 
     * promedio de +1 en relación al PAR. Recordar que el PAR de la
     *  cancha, es la suma de los 18 pares de sus hoyos. También 
     * este valor se ingresa al configurar la vuelta antes de cargar 
     * los hoyos.
     */
    public function generateStats()
    {
        // golpes totales
        $totalStrikes = (int)$this->getTotalStrikes();
        // total de pares
        $totalPar = (int)$this->getTotalPar();

        $diff = $totalStrikes - $totalPar;
        
        // vueltas completas
        $totalRoundPlayed = (int)$this->getTotalRounds();
        
        return compact('totalRoundPlayed', 'diff');
    }

    /**
     * @return int
     */
    private function getTotalStrikes()
    {
        $this->preprocessFilters();
        
        $holes = $this->queryBuilder
            ->select('Hole')
            ->from('AndresGottaGolfBundle:Hole', 'Hole')
            ->join('Hole.round', 'Round')
            ->getQuery()
            ->getResult()
        ;

        $s = 0;
        foreach($holes as $hole) {
            $s += $hole->getTotalStrikes();
        }
        
        return $s;
    }

    /**
     * @return integer
     */
    private function getTotalRounds()
    {
        $this->queryBuilder->resetDQLParts();
        $this->preprocessFilters();
        
        $result = $this->queryBuilder
            ->select('COUNT(Round.id)')
            ->from('AndresGottaGolfBundle:Round', 'Round')
            ->getQuery()
            ->getSingleScalarResult()
        ;
        
        return $result;
    }

    /**
     * @return integer
     */
    private function getTotalPar()
    {
        $this->queryBuilder->resetDQLParts();
        $this->preprocessFilters();
        $query = $this->queryBuilder
            ->select('SUM(Hole.par)')
            ->from('AndresGottaGolfBundle:Hole', 'Hole')
            ->innerJoin('Hole.round', 'Round')
            ->getQuery()
        ;
        
        return $query->getSingleScalarResult();
    }
    
}
