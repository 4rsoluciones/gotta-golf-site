<?php

namespace AndresGotta\Bundle\GolfBundle\Report;

use AndresGotta\Bundle\GolfBundle\ValueObject\Handicap;
use AndresGotta\Bundle\GolfBundle\ValueObject\PlayerCondition;
use AndresGotta\Bundle\GolfBundle\ValueObject\RoundType;
use Doctrine\ORM\QueryBuilder;
use Symfony\Component\Security\Core\User\UserInterface;
use WebFactory\Bundle\UserBundle\Entity\User;

abstract class BaseStrategy implements ReportStrategyInterface
{

    /**
     *
     * @var QueryBuilder
     */
    protected $queryBuilder;

    /**
     *
     * @var array
     */
    protected $filters;

    /**
     * 
     * @param QueryBuilder $queryBuilder
     * @param array $filters
     */
    public function __construct(QueryBuilder $queryBuilder, array $filters)
    {
        $this->queryBuilder = $queryBuilder;
        $this->filters = $filters;
    }

    /**
     * 
     */
    protected function preprocessFilters()
    {
        $this->queryBuilder->resetDQLParts();

        $this->queryBuilder
                ->andWhere('Round.playerConfirmed = :playerConfirmed')
                ->setParameter('playerConfirmed', true);

        if (!empty($this->filters['roundType'])) {
            $this->queryBuilder
                ->andWhere('Round.roundType = :roundType')
                ->setParameter('roundType', $this->filters['roundType']);
        }

        if (isset($this->filters['user']) && $this->filters['user'] instanceof UserInterface) {
            $this->queryBuilder
                ->andWhere('Round.player = :user')
                ->setParameter('user', $this->filters['user'])
            ;
        }

        if (isset($this->filters['from_date']) && $this->filters['from_date'] instanceof \DateTime) {
            $this->queryBuilder
                ->andWhere('Round.performedAt >= :from_date')
                ->setParameter('from_date', $this->filters['from_date'])
            ;
        }
        if (isset($this->filters['to_date']) && $this->filters['to_date'] instanceof \DateTime) {
            $this->queryBuilder
                ->andWhere('Round.performedAt <= :to_date')
                ->setParameter('to_date', $this->filters['to_date'])
            ;
        }

        if (isset($this->filters['condition']) && $this->filters['condition']) {
            $this->queryBuilder
                ->andWhere('Round.playerCondition = :condition')
                ->setParameter('condition', $this->filters['condition'])
            ;
        } else {
            /** @var User $user */
            $user = $this->filters['user'];
            $this->queryBuilder
                ->andWhere('Round.playerCondition = :condition')
                ->setParameter('condition', $user->getProfile()->getCondition())
            ;
        }

        if (isset($this->filters['handicap'])) {
            $value = $this->filters['handicap'];
            $minHandicap = $value - 5 < -5 ? - 5 : $value - 5;
            $maxHandicap = $value + 5 > 36 ? 36 : $value + 5;
            
            $this->queryBuilder
                    ->andWhere('Round.handicap >= :minHandicap AND Round.handicap <= :maxHandicap')
                    ->setParameter('minHandicap', $minHandicap)
                    ->setParameter('maxHandicap', $maxHandicap)
            ;
        }
    }

}
