<?php

namespace AndresGotta\Bundle\GolfBundle\Report;

use AndresGotta\Bundle\GolfBundle\Entity\FirstStrike;
use AndresGotta\Bundle\GolfBundle\ValueObject\Result;
use Doctrine\ORM\Query\Expr\Join;

/**
 * Class ReportAccordingToAreaAndDistancePar3Strategy
 * @package AndresGotta\Bundle\GolfBundle\Report
 */
class ReportAccordingToAreaAndDistancePar3Strategy extends BaseStrategy
{
    /**
     * REPORTE 4.2 - PAR 3
     * 
     * De los tiros de PAR 3 que den como resultado fuera del Green 
     * y a la opción siguiente den como resultado green y a la opción 
     * siguiente se emboquen con el primer putt, se calcula ese total 
     * agrupando por cada opción de fuera del green y distancia desde 
     * donde se jugó y luego el porcentaje de cada uno sobre ese total 
     * de golpes referidos.
     */
    public function generateStats()
    {
        $par = 3;
        $reasons = FirstStrike::getReasonChoicesByPar($par);
        $distances = FirstStrike::getDistanceChoicesByPar($par);

        foreach ($reasons as $reason) {
            foreach ($distances as $distance) {
                $results[$reason][$distance] = 0;
            }
        }
        $this->newTotal($results);
        $this->applyPercents($results);

        return compact('results');
    }

    /**
     * @return int
     */
    private function newTotal(&$data)
    {
        $query = $this->generateSubQuery('FirstStrike');

        /** @var FirstStrike $result */
        foreach ($query->getResult() as $result) {
            $hole = $result->getHole();
            if ($hole->getScore() === 3 || $hole->getScore() === 2) {
                $data[$result->getReason()][$result->getDistance()] += 1;
            }
        }
    }

    /**
     * @param $class
     * @param int $par
     * @return \Doctrine\ORM\Query
     */
    private function generateSubQuery($class, $par = 3)
    {
        $query = clone $this->queryBuilder;
        $query
            ->select('strike')
            ->from("AndresGottaGolfBundle:{$class}", "strike")
            ->innerJoin("strike.hole", 'Hole')
            ->innerJoin('Hole.round', 'Round')
            ->andWhere('Hole.par = :par')->setParameter('par', $par)
            ->andWhere('strike.result != :result AND strike.result != :result2')
            ->setParameter('result', Result::INSIDE_GREEN)->setParameter('result2', Result::HOLE_OUT)
        ;

        return $query->getQuery();
    }

    /**
     * @param $data
     * @return int
     */
    private function sumQuantity($data)
    {
        $sum = 0;
        foreach ($data as $item) {
            foreach ($item as $value) {
                $sum += $value;
            }
        }

        return $sum;
    }

    /**
     * @param $data
     */
    private function applyPercents(&$data)
    {
        $sum = $this->sumQuantity($data);
        foreach ($data as &$item) {
            foreach ($item as &$value) {
                $value *= $sum ? 100 / $sum : 0;
            }
        }
        unset($value);
    }
}
