<?php

namespace AndresGotta\Bundle\GolfBundle\Report;

use AndresGotta\Bundle\GolfBundle\ValueObject\Result;
use Doctrine\ORM\Query\Expr\Join;

class ReportGeneralsAveragePenaltiesStrategy extends BaseStrategy
{
    /**
     * REPORTE 16
     * 
     * Se suman las penalidades y las vueltas cargadas, luego 
     * se calcula el promedio entre estos valores.
     */
    public function generateStats()
    {
        $totalPenalties = $this->generateTotalPenalties();
        $totalRoundPlayed = $this->generateTotalRoundPlayed();


        return compact('totalRoundPlayed', 'totalPenalties');
    }
    
    private function generateTotalRoundPlayed()
    {
//        $subQuery2 = $this->getSubQuery2();
//        $subQuery = $this->generateRoundSubQuery($subQuery2);
        $this->preprocessFilters();
        
        $result = $this->queryBuilder
            ->select('COUNT(Round.id)')
            ->from('AndresGottaGolfBundle:Round', 'Round')
            ->andWhere('Round.playerConfirmed = :confirm')->setParameter('confirm', true)
//                ->andWhere($this->queryBuilder->expr()->in('Round', $subQuery))
            ->getQuery()
            ->getSingleScalarResult()
        ;
        
        return $result;
    }
    
    private function generateRoundSubQuery($subQuery)
    {
        $this->preprocessFilters();   
        $subQuery = $this->queryBuilder
            ->select('R.id')
            ->from('AndresGottaGolfBundle:Hole', 'H')
            ->join('AndresGottaGolfBundle:Round', 'R', Join::WITH, 'H.round = R')
            ->where($this->queryBuilder->expr()->in('R', $subQuery))    
            ->groupBy('R.id')
            ->having('COUNT(H.id) = 9 OR COUNT(H.id) = 18')
            ->getDQL();
        
        return $subQuery;
    }
    
    private function getSubQuery2()
    {
        $this->preprocessFilters();
        $query = $this->queryBuilder
            ->select('R1.id')
            ->from('AndresGottaGolfBundle:Hole', 'H1')
            ->join('AndresGottaGolfBundle:Round', 'R1', Join::WITH, 'H1.round = R1')
            ->join('AndresGottaGolfBundle:Strike', 'S', Join::WITH, 'S.hole = H1')
            ->where('S.result = :result')->setParameter('result', Result::HOLE_OUT)
            ->getDQL();
        
        return $query;
    }
    
    private function generateTotalPenalties()
    {
//        $subQuery2 = $this->getSubQuery2();
//        $subQuery = $this->generateRoundSubQuery($subQuery2);
        $this->preprocessFilters();
        
        $result = $this->queryBuilder
            ->select('SUM(Hole.penalityShotsCount)')
            ->from('AndresGottaGolfBundle:Hole', 'Hole')
            ->innerJoin('Hole.round', 'Round')
            ->andWhere('Round.playerConfirmed = :confirm')->setParameter('confirm', true)
//                ->andWhere($this->queryBuilder->expr()->in('Round', $subQuery))
//                ->getQuery()
//                ->getSingleScalarResult()
        ;
        
        return $result->getQuery()->getSingleScalarResult();
    }
}
