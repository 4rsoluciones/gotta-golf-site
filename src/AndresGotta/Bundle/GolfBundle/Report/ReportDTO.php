<?php

namespace AndresGotta\Bundle\GolfBundle\Report;

use AndresGotta\Bundle\GolfBundle\Entity\Report;

/**
 * 
 */
class ReportDTO
{

    /**
     *
     * @var Report
     */
    private $report;

    /**
     *
     * @var array
     */
    private $columns;

    /**
     *
     * @var array
     */
    private $values;

    public function __construct(Report $report = null, $values = array())
    {
        $this->report = $report;
        $this->values = $values;
        $this->columns = $this->extractColumns($values);
    }

    public function getTitle()
    {
        return $this->report->getTitle();
    }

    public function getDescription()
    {
        return $this->report->getDescription();
    }

    public function getGoals()
    {
        return $this->report->getObjectives();
    }

    public function getColumns()
    {
        return $this->columns;
    }

    public function getValues()
    {
        return $this->values;
    }

    public function setColumns($columns)
    {
        $this->columns = $columns;
        return $this;
    }

    private function extractColumns($data)
    {
        $columns = array();
        
        if (is_array(current($data))) {
            $columns = array_keys(current($data));
        }
        
        return $columns;
    }
    
    public function getReport()
    {
        return $this->report;
    }
}
