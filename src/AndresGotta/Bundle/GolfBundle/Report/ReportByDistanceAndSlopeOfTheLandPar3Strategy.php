<?php

namespace AndresGotta\Bundle\GolfBundle\Report;

use AndresGotta\Bundle\GolfBundle\Entity\Putt;
use AndresGotta\Bundle\GolfBundle\ValueObject\LandSlope;
use AndresGotta\Bundle\GolfBundle\ValueObject\Result;

class ReportByDistanceAndSlopeOfTheLandPar3Strategy extends BaseStrategy
{
    /**
     * REPORTE 5.2 - PAR 3
     * 
     * Del total de putts de par 3 que dan como resultado embocado, 
     * se muestra el porcentaje que tiene cada inclinación del 
     * terreno que compone el total del porcentaje que tenía cada 
     * distancia.
     */
    public function generateStats()
    {
        $par = 3;
        $landSlopes = array(LandSlope::ON_CLIMB, LandSlope::ON_DESCEND, LandSlope::STRAIGHT);
        $deviations = array(LandSlope::RIGHT_TO_LEFT, LandSlope::STRAIGHT, LandSlope::LEFT_TO_RIGHT);
        $distances = Putt::getDistanceChoicesByPar($par);
        
        foreach ($distances as $distance) {
            foreach ($landSlopes as $landSlope) {
                if ($landSlope != LandSlope::STRAIGHT) {
                    foreach ($deviations as $deviation) {
                        $result[$landSlope][$deviation][$distance] = $this->generatePuttTotalByLandSlope($par, $landSlope, $distance, $deviation);
                    }
                } else {
                    $result[$landSlope][$distance] = $this->generatePuttTotalByLandSlope($par, $landSlope, $distance);
                }
            }
        }
        
        $this->applyPercents($result);
        
        return $result;
    }
    
    private function generatePuttTotalByLandSlope($par, $landSlope, $distance, $deviation = null)
    {
        $this->preprocessFilters();
        
        $data = $this->queryBuilder
                ->select('COUNT(Putt.id) as quantity')
                ->from('AndresGottaGolfBundle:Putt', 'Putt')
                ->innerJoin('Putt.hole', 'Hole')
                ->innerJoin('Hole.round', 'Round')
                ->andWhere('Hole.par = :par')
                ->setParameter('par', $par)
                ->andWhere('Putt.distance = :distance')
                ->setParameter('distance', $distance)
                ->andWhere('Putt.result = :result')
                ->setParameter('result', Result::HOLE_OUT)
                ->andWhere('Putt.landSlopes LIKE :landSlope')
                ;
        $deviation 
                ? $data->setParameter('landSlope', "{$deviation},{$landSlope}")
                : $data->setParameter('landSlope', "{$landSlope}");
        
        return $data->getQuery()->getSingleScalarResult();
    }
    
    private function sumQuantity($data)
    {
        $sum = 0;
        foreach ($data as $key => $item) {
            foreach ($item as $value) {
                if ($key == LandSlope::STRAIGHT) {
                    $sum += $value;
                } else {
                    foreach ($value as $it) {
                        $sum += $it;
                    }
                }
            }
        }

        return $sum;
    }

    private function applyPercents(&$data)
    {
        $sum = $this->sumQuantity($data);
        
        foreach ($data as $key => &$item) {
            foreach ($item as &$value) {
                if ($key == LandSlope::STRAIGHT) {
                    $value *= $sum ? 100 / $sum : 0;
                } else {
                    foreach ($value as &$it) {
                        $it *= $sum ? 100 / $sum : 0;
                    }
                }
            }
        }
        unset($item);
        unset($it);
    }
    
}
