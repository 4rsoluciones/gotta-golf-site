<?php

namespace AndresGotta\Bundle\GolfBundle\Report;

use AndresGotta\Bundle\GolfBundle\ValueObject\Distance;
use AndresGotta\Bundle\GolfBundle\ValueObject\Result;
use Doctrine\ORM\Query\Expr\Join;

class ReportGeneralsAverageDistance1puttStrategy extends BaseStrategy
{
    /**
     * REPORTE 18
     * 
     * Se suman todas las distancias del primer putt por vuelta, 
     * luego se calcula el promedio con respecto a la cantidad 
     * de vueltas cargadas.
     */
    public function generateStats()
    {
        $puttDistances = $this->getPuttDistanceQuantity();
        $puttDistances2 = $this->getPuttApproachDistanceQuantity();
        $totalRoundPlayed = $this->generateTotalRoundPlayed();
        
        return $this->calculateDistanceAverage($puttDistances, $puttDistances2, $totalRoundPlayed);
    }
    
    private function generateTotalRoundPlayed()
    {
        $this->preprocessFilters();
        
        $result = $this->queryBuilder
            ->select('COUNT(Round.id)')
            ->from('AndresGottaGolfBundle:Round', 'Round')
            ->getQuery()
            ->getSingleScalarResult()
        ;
        
        return $result;
    }
    
    private function getPuttDistanceQuantity()
    {
        $this->preprocessFilters();
        
        $result = $this->queryBuilder
            ->select('Putt.distance, COUNT(Putt.id) as quantity')
            ->from('AndresGottaGolfBundle:Putt', 'Putt')
            ->innerJoin('Putt.hole', 'Hole')
            ->innerJoin('Hole.round', 'Round')
            ->addGroupBy('Putt.distance')
            ->getQuery()
            ->getResult()
        ;

        return $result;
    }

    private function getPuttApproachDistanceQuantity()
    {
        $this->preprocessFilters();

        $result = $this->queryBuilder
            ->select('PuttApproach.distance, COUNT(PuttApproach.id) as quantity')
            ->from('AndresGottaGolfBundle:PuttApproach', 'PuttApproach')
            ->innerJoin('PuttApproach.hole', 'Hole')
            ->innerJoin('Hole.round', 'Round')
            ->addGroupBy('PuttApproach.distance')
            ->getQuery()
            ->getResult()
        ;

        return $result;
    }
        
    private function calculateDistanceAverage($data, $data2, $total)
    {
        $distances = array(
            Distance::LESS_THAN_1_MTS => array(
                'value' => 0.5,
                'min' => 0,
                'max' => 1
            ),
            Distance::BTW_1_TO_2_MTS => array(
                'value' => 1.5,
                'min' => 1,
                'max' => 2
            ),
            Distance::BTW_2_TO_3_MTS => array(
                'value' => 2.5,
                'min' => 2,
                'max' => 3
            ),
            Distance::BTW_3_TO_5_MTS=> array(
                'value' => 3.5,
                'min' => 3,
                'max' => 5
            ),
            Distance::BTW_5_TO_7_MTS=> array(
                'value' => 5.5,
                'min' => 5,
                'max' => 7
            ),
            Distance::BTW_7_TO_10_MTS=> array(
                'value' => 7.5,
                'min' => 7,
                'max' => 10
            ),
            Distance::BTW_10_TO_15_MTS=> array(
                'value' => 10.5,
                'min' => 10,
                'max' => 15
            ),
            Distance::MORE_THAN_15_MTS => array(
                'value' => 15.5,
                'min' => 15,
                'max' => 20
            ),
        );
        
        $sum = 0;
        foreach ($data as $item) {
            $sum += $distances[$item['distance']]['value'] * $item['quantity'];
        }
        foreach ($data2 as $item) {
            $sum += $distances[$item['distance']]['value'] * $item['quantity'];
        }
        
        $average = $total ? $sum / $total : 0;
        $key = 0;
        if ($average) {
            foreach ($distances as $key => $distance) {
                if ($distance['min'] <= $average && $distance['max'] >= $average) {
                    break;
                }
            }
        }
        
        return array('total' => $total, 'distance' => $key);
    }
    
}
