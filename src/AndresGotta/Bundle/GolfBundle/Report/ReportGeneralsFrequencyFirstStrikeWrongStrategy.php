<?php

namespace AndresGotta\Bundle\GolfBundle\Report;

use AndresGotta\Bundle\GolfBundle\ValueObject\Result;
use Doctrine\ORM\Query\Expr\Join;

class ReportGeneralsFrequencyFirstStrikeWrongStrategy extends BaseStrategy
{
    /**
     * REPORTE 15
     * 
     * De todos los hoyos par 4 y par 5 jugados, se 
     * totalizan solo los que dan como resultado fuera 
     * de fairway, con este valor y el total de hoyos 
     * par 4 y par 5 jugados se muestra su relación.
     */
    public function generateStats()
    {
        $totalGreenPlayed = $this->generateTotalGreenPlayed();
        $totalPar4AndPar5OutsideFairway = $this->generateTotalGreenPlayed(Result::OUTSIDE_FAIRWAY);

        return compact('totalGreenPlayed', 'totalPar4AndPar5OutsideFairway');
    }
    
    private function generateTotalGreenPlayed($strikeResult = null)
    {
        $this->preprocessFilters();

        $result = $this->queryBuilder
            ->select('COUNT(Hole.id)')
            ->from('AndresGottaGolfBundle:Hole', 'Hole')
            ->innerJoin('Hole.round', 'Round')
            ->andWhere('Hole.par IN (4, 5)')
        ;
        
        if ($strikeResult) {
            $result
                ->innerJoin('AndresGottaGolfBundle:FirstStrike', 'FirstStrike', Join::WITH, 'FirstStrike.hole = Hole')
                ->andWhere('FirstStrike.result LIKE :result')->setParameter('result', "%{$strikeResult}%")
            ;
        }
        
        return $result->getQuery()->getSingleScalarResult();
    }
}
