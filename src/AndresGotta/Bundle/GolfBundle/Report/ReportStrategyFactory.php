<?php

namespace AndresGotta\Bundle\GolfBundle\Report;

use AndresGotta\Bundle\GolfBundle\Entity\Report;
use Doctrine\ORM\QueryBuilder;
use Symfony\Component\Security\Core\User\UserInterface;

class ReportStrategyFactory
{
    /**
     * 
     * @param Report $report
     * @param UserInterface $user
     * @param QueryBuilder $queryBuilder
     * 
     * @return ReportStrategyInterface
     */
    public static function create(Report $report, QueryBuilder $queryBuilder, array $filters)
    {
        $slug = str_replace(' ', '', ucwords(str_replace('-', ' ', $report->getSlug())));
        $class = __NAMESPACE__ . '\Report' . $slug . 'Strategy';

        return new $class($queryBuilder, $filters);
    }
}
