<?php

namespace AndresGotta\Bundle\GolfBundle\Report;

use AndresGotta\Bundle\GolfBundle\Entity\Hole;
use AndresGotta\Bundle\GolfBundle\ValueObject\Result;
use Doctrine\ORM\Query\Expr\Join;

/**
 * Class ReportGeneralsAverageScoreRoundParStrategy
 * @package AndresGotta\Bundle\GolfBundle\Report
 */
class ReportGeneralsAverageScoreRoundParStrategy extends BaseStrategy
{
    /**
     * REPORTE 19.2
     *
     * Se suman todos los golpes de todas las vueltas completadas y se

    lo resta al total de los pares de las vueltas. Luego se calcula el

    promedio sobre el total de vueltas y esa diferencia.

    Ejemplo: si juega 3 vueltas de 18 hoyos en una cancha PAR 70, y

    realiza 71 golpes en cada vuelta, tiene un promedio de +1 en

    relación al PAR.

    Recordar que el PAR de la cancha, es la suma de los 18 pares de

    sus hoyos. También este valor se ingresa al configurar la vuelta

    antes de cargar los hoyos.
     *
     * IDEM a mi consideración en el reporte 19, salvo que ahora
     * se hace ese promedio agrupando según lo realizado en los
     * hoyos 1 a 9 o 10 a 18, vs el PAR total de ese grupo de
     * hoyos.
     */
    public function generateStats()
    {
        $totalRoundPlayed = $this->generateTotalRoundPlayed();

        $diffInf = $this->generateInferiorTotalStrikes() - $this->generateInferiorTotalRoundStrikes();
        $diffSup = $this->generateSuperiorTotalStrikes() - $this->generateSuperiorTotalRoundStrikes();

        return compact('totalRoundPlayed', 'diffInf', 'diffSup');
    }

    /**
     * @return mixed
     */
    private function generateTotalRoundPlayed()
    {
        $this->preprocessFilters();

        $result = $this->queryBuilder
            ->select('COUNT(Round.id)')
            ->from('AndresGottaGolfBundle:Round', 'Round')
            ->getQuery()
            ->getSingleScalarResult()
        ;

        return $result;
    }

    /**
     * @return int
     */
    private function generateSuperiorTotalStrikes()
    {
        $this->preprocessFilters();

        $holes = $this->queryBuilder
            ->select('Hole')
            ->from('AndresGottaGolfBundle:Hole', 'Hole')
            ->join('Hole.round', 'Round')
            ->andWhere($this->queryBuilder->expr()->between('Hole.number', ':inferior', ':superior'))
            ->setParameter('inferior', 10)
            ->setParameter('superior', 18)
            ->getQuery()->getResult()
        ;

        $s = 0;
        /** @var Hole $hole */
        foreach($holes as $hole) {
            $s += $hole->getTotalStrikes();
        }

        return $s;
    }

    /**
     * @return int
     */
    private function generateInferiorTotalStrikes()
    {
        $this->preprocessFilters();

        $holes = $this->queryBuilder
            ->select('Hole')
            ->from('AndresGottaGolfBundle:Hole', 'Hole')
            ->join('Hole.round', 'Round')
            ->andWhere($this->queryBuilder->expr()->between('Hole.number', ':inferior', ':superior'))
            ->setParameter('inferior', 1)
            ->setParameter('superior', 9)
            ->getQuery()->getResult()
        ;

        $s = 0;
        /** @var Hole $hole */
        foreach($holes as $hole) {
            $s += $hole->getTotalStrikes();
        }

        return $s;
    }

    /**
     * @return int
     */
    private function generateSuperiorTotalRoundStrikes()
    {
        $this->preprocessFilters();

        $result = $this->queryBuilder
            ->select('SUM(Hole.par) as strikes')
            ->from('AndresGottaGolfBundle:Hole', 'Hole')
            ->innerJoin('Hole.round', 'Round')
            ->andWhere($this->queryBuilder->expr()->between('Hole.number', ':inferior', ':superior'))
            ->setParameter('inferior', 10)
            ->setParameter('superior', 18)
            ->getQuery()->getResult()
        ;

        return $this->getTotalStrikes($result);
    }

    /**
     * @return int
     */
    private function generateInferiorTotalRoundStrikes()
    {
        $this->preprocessFilters();
        $result = $this->queryBuilder
            ->select('SUM(Hole.par) as strikes')
            ->from('AndresGottaGolfBundle:Hole', 'Hole')
            ->innerJoin('Hole.round', 'Round')
            ->andWhere($this->queryBuilder->expr()->between('Hole.number', ':inferior', ':superior'))
            ->setParameter('inferior', 1)
            ->setParameter('superior', 9)
            ->getQuery()->getResult()
        ;

        return $this->getTotalStrikes($result);
    }

    /**
     * @param $result
     * @return int
     */
    private function getTotalStrikes($result)
    {
        $strikes = 0;
        foreach ($result as $round) {
            $strikes += $round['strikes'];
        }

        return $strikes;
    }
}
