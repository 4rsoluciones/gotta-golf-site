<?php

namespace AndresGotta\Bundle\GolfBundle\Report;

use AndresGotta\Bundle\GolfBundle\Entity\FirstStrike;
use AndresGotta\Bundle\GolfBundle\Entity\FirstStrikeApproach;
use AndresGotta\Bundle\GolfBundle\Entity\SecondStrike;
use AndresGotta\Bundle\GolfBundle\Entity\ThirdStrike;
use AndresGotta\Bundle\GolfBundle\ValueObject\Result;
use Doctrine\ORM\Query\Expr\Join;
use Proxies\__CG__\AndresGotta\Bundle\GolfBundle\Entity\PuttApproach;
use Proxies\__CG__\AndresGotta\Bundle\GolfBundle\Entity\SecondStrikeApproach;
use Proxies\__CG__\AndresGotta\Bundle\GolfBundle\Entity\ThirdStrikeApproach;

class ReportGeneralsAverageScoreHoleParStrategy extends BaseStrategy
{
    /**
     * REPORTE 19.3
     * 
     * Se agrupan los hoyos PAR 3, PAR 4 o PAR 5 jugados. 
     * Se suman los golpes totales realizados en cada 
     * grupo de hoyos. A ese total se le resta el total 
     * de la suma de los pares de cada grupo. A esa 
     * diferencia, se le hace el promedio según la 
     * cantidad de hoyos que tenía el grupo. EJ: si jugó 
     * 3 hoyos PAR 3, y realizó 12 golpes en total, tiene 
     * un promedio de +1 en hoyos PAR 3.
     */
    public function generateStats()
    {
        $pars = array(3, 4, 5);
        foreach ($pars as $par) {
            $result[$par] = array(
                'strikes' => $this->getTotalStrikesByPar($par) - $this->getTotalParHolesByPar($par),
                'total' => $this->getTotalHolesByPar($par)
                );
        }

        return $result;
    }
    
    // retorna la cantidad de hoyos segun el par
    private function getTotalHolesByPar($par)
    {
        $this->preprocessFilters();
        
        $result = $this->queryBuilder
            ->select('COUNT(Hole.id)')
            ->from('AndresGottaGolfBundle:Hole', 'Hole')
            ->innerJoin('AndresGottaGolfBundle:Round', 'Round', Join::WITH, 'Round = Hole.round')
            ->andWhere('Hole.par = :par')->setParameter('par', $par)
            ->andWhere('Round.holeCount = 18')
            ->getQuery()
            ->getSingleScalarResult()
        ;

        return $result;
    }

    /**
     * @param $class
     * @param $par
     * @return mixed
     */
    private function getCountStrikesByClass($class, $par)
    {
        $this->preprocessFilters();
        $result = $this->queryBuilder
            ->select("COUNT({$class}.id)")
            ->from("AndresGottaGolfBundle:{$class}", "{$class}")
            ->innerJoin("{$class}.hole", 'Hole')
            ->innerJoin('Hole.round', 'Round')
            ->andWhere('Round.holeCount = 18')
            ->andWhere('Hole.par = :par')->setParameter('par', $par)
        ;

        return $result->getQuery()->getSingleScalarResult();
    }

    // retorna la cantidad de strikes por par
    private function getTotalStrikesByPar($par)
    {
        $total = 0;
        $total += $this->getCountStrikesByClass('FirstStrike', $par);
        $total += $this->getCountStrikesByClass('FirstStrikeApproach', $par);
        $total += $this->getCountStrikesByClass('PuttApproach', $par);
        $total += $this->getCountStrikesByClass('SecondStrike', $par);
        $total += $this->getCountStrikesByClass('SecondStrikeApproach', $par);
        $total += $this->getCountStrikesByClass('ThirdStrike', $par);
        $total += $this->getCountStrikesByClass('ThirdStrike', $par);

        return $total;
    }
    
    // retorna la cantidad de golpes "ideal" (suma de pares de los hoyos) segun el par
    private function getTotalParHolesByPar($par)
    {
        $this->preprocessFilters();
        
        $result = $this->queryBuilder
                ->select('SUM(Hole.par)')
                ->from('AndresGottaGolfBundle:Hole', 'Hole')
                ->innerJoin('Hole.round', 'Round', Join::WITH, 'Round = Hole.round')
                ->andWhere('Hole.par = :par')->setParameter('par', $par)
            ->andWhere('Round.holeCount = 18')
                ->getQuery()
                ->getSingleScalarResult()
                ;
        
        return $result;
    }
    
}
