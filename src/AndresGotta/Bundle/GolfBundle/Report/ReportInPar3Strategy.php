<?php

namespace AndresGotta\Bundle\GolfBundle\Report;

use AndresGotta\Bundle\GolfBundle\Entity\FirstStrike;
use AndresGotta\Bundle\GolfBundle\ValueObject\Result;
use Doctrine\ORM\Query\Expr\Join;

class ReportInPar3Strategy extends BaseStrategy
{
    /**
     * Se toman en cuenta únicamente los: Primer tiro en Par 3, segundo tiro en par 4 y tercer tiro en par 5. Esa suma da el total de tiros.
     *
     * Hay que ver cuánto de esos tiros, dio como resultado fuera del Green y ese es el nuevo 100%.
     *
     * De ese nuevo 100%, mostrar el porcentaje que tiene en donde el score final del hoyo (sin contar penalidades) es par
     * o menos (2 o 3 en par 3, 3 o 4 en par 4 o 3, 4 o 5 en par 5).
     *
     */
    public function generateStats()
    {
        $this->preprocessFilters();

        $total = $this->generateParStrikesStats(); // total de golpes por usuario donde dio el resultado
        $newTotal = $this->newTotal(); // donde el score del hoyo es par o menos
        
        return compact('total', 'newTotal');
    }

    /**
     * @return int
     */
    private function generateParStrikesStats()
    {
        $query = $this->generateSubQuery('FirstStrike');

        return intval(count($query->getResult()));
    }

    /**
     * @return int
     */
    private function newTotal()
    {
        $query = $this->generateSubQuery('FirstStrike');

        $scrambling = 0;
        /** @var FirstStrike $result */
        foreach ($query->getResult() as $result) {
            if ($result->getHole()->getScore() === 3 || $result->getHole()->getScore() === 2) {
                $scrambling++;
            }
        }

        return $scrambling;
    }

    /**
     * @param $class
     * @param int $par
     * @return \Doctrine\ORM\Query
     */
    private function generateSubQuery($class, $par = 3)
    {
        $query = clone $this->queryBuilder;
        $query
            ->select('strike')
            ->from("AndresGottaGolfBundle:{$class}", "strike")
            ->innerJoin("strike.hole", 'Hole')
            ->innerJoin('Hole.round', 'Round')
            ->andWhere('Hole.par = :par')->setParameter('par', $par)
            ->andWhere('strike.result != :result AND strike.result != :result2')
            ->setParameter('result', Result::INSIDE_GREEN)->setParameter('result2', Result::HOLE_OUT)
        ;

        return $query->getQuery();
    }
}
