<?php

namespace AndresGotta\Bundle\GolfBundle\Report;

use AndresGotta\Bundle\GolfBundle\Entity\Putt;
use AndresGotta\Bundle\GolfBundle\ValueObject\Result;
use Doctrine\ORM\Query\Expr\Join;

class ReportPuttPar5StrikesToHoleOutStrategy extends BaseStrategy
{
    /**
     * REPORTE 6 - PAR 5
     * 
     * Conocer el porcentaje de greens en regulación
     *
     * @return array
     */
    public function generateStats()
    {
        $par = 5;
        $distances = Putt::getDistanceChoicesByPar($par);
        
        foreach ($distances as $distance) {
            // Embocados con un solo putt
            $puttHoleOutResults = $this->generatePuttHoleOutStats($par, $distance);

            // Tiros utilizados para embocar desde el green desde una distancia determinada
            $count = $this->generatePuttApproachsHoleOutStats($par, $distance);
            $TwoPuttApproachsHoleOutResults = 0;
            $ThreePuttApproachsHoleOutResults = 0;
            $FourPuttApproachsHoleOutResults = 0;
            switch ($count) {
                case 2:
                    $TwoPuttApproachsHoleOutResults += 1;
                    break;
                case 3:
                    $ThreePuttApproachsHoleOutResults += 1;
                    break;
                default:
                    $FourPuttApproachsHoleOutResults += 1;
                    break;
            }
            
            $result['one_putt'][$distance] = (int)$puttHoleOutResults;
            $result['two_putt'][$distance] = $TwoPuttApproachsHoleOutResults;
            $result['three_putt'][$distance] = $ThreePuttApproachsHoleOutResults;
            $result['four_putt'][$distance] = $FourPuttApproachsHoleOutResults;
        }
        
        $this->applyPercents($result);
        
        return compact('result');
    }

    /**
     * Cantidad de hoyos embocados con el primer putt
     *
     * @param $par
     * @param $distance
     * @return array
     */
    private function generatePuttHoleOutStats($par, $distance)
    {
        $this->preprocessFilters();
        
        $results = $this->queryBuilder
            ->select('COUNT(Putt.id) as quantity')
            ->from('AndresGottaGolfBundle:Putt', 'Putt')
            ->innerJoin('Putt.hole', 'Hole')
            ->innerJoin('Hole.round', 'Round')
            ->andWhere("Putt.result = :result")->setParameter('result', Result::HOLE_OUT)
            ->andWhere("Hole.par = :par")->setParameter('par', $par)
            ->andWhere("Putt.distance = :distance")->setParameter('distance', $distance)
            ->getQuery()
            ->getSingleScalarResult()
        ;

        return $results;
    }

    /**
     * @param $par
     * @param $quantityStrikes
     * @param $distance
     * @return array
     */
    private function generatePuttApproachsHoleOutStats($par, $distance)
    {
        $this->queryBuilder->getParameters()->clear();
        $this->preprocessFilters();
        
        $results = $this->queryBuilder
            ->select('COUNT(PuttApproach.id) as quantity')
            ->from('AndresGottaGolfBundle:PuttApproach', 'PuttApproach')
            ->innerJoin('PuttApproach.hole', 'Hole')
            ->innerJoin('Hole.round', 'Round')
            ->andWhere("Hole.par = :par")->setParameter('par', $par)
            ->andWhere("PuttApproach.distance = :distance")->setParameter('distance', $distance)
            ->getQuery()
            ->getSingleScalarResult()
        ;

        return $results + 1;
    }

    /**
     * @param $data
     */
    private function applyPercents(&$data)
    {
        foreach ($data as &$item) {
            $sum = 0;
            foreach ($item as &$value) {
                // Total de hoyos embocados con x cantidad de putt
                $sum += $value;
            }

            foreach ($item as &$value) {
                $value *= $sum ? 100 / $sum : 0;
            }
        }
        unset($value);
    }
    
}
