<?php

namespace AndresGotta\Bundle\GolfBundle\Report;

use AndresGotta\Bundle\GolfBundle\Entity\SecondStrike;
use AndresGotta\Bundle\GolfBundle\ValueObject\BallFlight;
use AndresGotta\Bundle\GolfBundle\ValueObject\Reason;
use AndresGotta\Bundle\GolfBundle\ValueObject\Result;

class ReportPrecisionBallFlightPar4Strategy extends BaseStrategy
{
    /**
     * REPORTE 2.1 - PAR 4
     * 
     * Porcentaje en los tiros tomados para la estadística anterior, según los 3 vuelos de pelota, 
     * que dieron como resultado (ordenar por vuelo de Pelota):
     *   a) En Green
     *   b) fuera del green y sus derivados (5 en total)
     * 
     * a – Agrupadas por distancia de golpe de salida y vuelo de pelota, por cada uno de estos se 
     * calcula el porcentaje sobre el total. Se consideran los golpes que dan como resultado en el Green.
     * 
     * b – Idem anterior pero el golpe de salida da como resultado fuera del Green  y sus derivados 
     * (mostrar valor cada opción).
     *
     * Se ordenan según el vuelo de pelota, y dentro de cada vuelo, según la distancia desde donde se 
     * jugó y el resultado que se obtuvo a cada opción de resultado. Es igual a la anterior 
     * pero ahora el primer criterio de agrupabilidad es el vuelo de la pelota.
     */
    public function generateStats()
    {
        $par = 4;
        $distances = SecondStrike::getDistanceChoicesByPar($par);
        $ballFlights = BallFlight::getChoices();
        $reasons = Reason::getChoices();

        foreach ($reasons as $reason) {
            foreach ($ballFlights as $ballFlight) {
                foreach ($distances as $distance) {
                    $insideGreenResults[$ballFlight][$distance] = $this->generateResults(Result::INSIDE_GREEN, $par, $distance, $ballFlight, null);
                    $outsideGreenResults[$reason][$ballFlight][$distance] = $this->generateResults(Result::OUTSIDE_GREEN, $par, $distance, $ballFlight, $reason);
                }
            }
            $this->applyPercents($outsideGreenResults[$reason]);
        }
        $this->applyPercents($insideGreenResults);

        return compact('insideGreenResults', 'outsideGreenResults');
    }

    private function generateResults($result, $par, $distance, $ballFlight, $reason)
    {
        $this->preprocessFilters();

        $qb = $this->queryBuilder
            ->select('COUNT(SecondStrike.id)')
            ->from('AndresGottaGolfBundle:SecondStrike', 'SecondStrike')
            ->innerJoin('SecondStrike.hole', 'Hole')
            ->innerJoin('Hole.round', 'Round')
            ->andWhere('Hole.par = :par')->setParameter('par', $par)
            ->andWhere('SecondStrike.result LIKE :result')->setParameter('result', "%{$result}%")
            ->andWhere('SecondStrike.distance = :distance')->setParameter('distance', $distance)
            ->andWhere('SecondStrike.ballFlight = :ballFlight')->setParameter('ballFlight', $ballFlight)
            ->andWhere('SecondStrike.reason = :reason')->setParameter('reason', $reason)
        ;

        return $qb->getQuery()->getSingleScalarResult();
    }
     
    private function sumQuantity($data)
    {
        $sum = 0;
        foreach ($data as $item) {
            foreach ($item as $value) {
                $sum += $value;
            }
        }

        return $sum;
    }

    private function applyPercents(&$data)
    {
        $sum = $this->sumQuantity($data);
        foreach ($data as &$item) {
            foreach ($item as &$value) {
                $value *= $sum ? 100 / $sum : 0;
            }
        }
        unset($value);
    }
}
