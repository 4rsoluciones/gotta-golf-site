<?php

namespace AndresGotta\Bundle\GolfBundle\Report;

use AndresGotta\Bundle\GolfBundle\ValueObject\Distance;
use AndresGotta\Bundle\GolfBundle\ValueObject\Result;

class ReportAverageDistancePar5Strategy extends BaseStrategy
{
    /**
     * REPORTE 5.1 - PAR 5
     */
    public function generateStats()
    {
        $puttDistances = $this->getPuttDistanceQuantity();
        
        return $this->calculateDistanceAverage($puttDistances);
    }
    
    public function getPuttDistanceQuantity()
    {
        $data = $this->getCountPuttsByDistance();

        return $data;
    }

    /**
     * Cuenta cantidade de golpes embocados en el green agrupados por distancia
     * @return array
     */
    private function getCountPuttsByDistance()
    {
        $strikes = ['Putt', 'PuttApproach'];
        $result = array();

        foreach ($strikes as $class) {
            $this->preprocessFilters();

            $qb = $this->queryBuilder
                ->select("{$class}.distance, COUNT({$class}.id) as quantity")
                ->from("AndresGottaGolfBundle:{$class}", "{$class}")
                ->innerJoin("{$class}.hole", 'Hole')
                ->innerJoin('Hole.round', 'Round')
                ->andWhere('Hole.par = :par')->setParameter('par', 5)
                ->andWhere("{$class}.result LIKE :result")->setParameter('result', '%' . Result::HOLE_OUT . '%')
                ->addGroupBy("{$class}.distance")
                ->orderBy('quantity', 'desc')
            ;

            $resultStrike = $qb->getQuery()->getResult();
            foreach ($resultStrike as $res) {
                $result[] = $res;
            }
        }

        return $result;
    }
        
    private function sumQuantity($data)
    {
        $sum = 0;
        foreach ($data as $item) {
            $sum += $item['quantity'];
        }

        return $sum;
    }
    
    private function calculateDistanceAverage($data)
    {
        $distances = array(
            Distance::LESS_THAN_1_MTS => array(
                'value' => 0.5,
                'min' => 0,
                'max' => 1
            ),
            Distance::BTW_1_TO_2_MTS => array(
                'value' => 1.5,
                'min' => 1,
                'max' => 2
            ),
            Distance::BTW_2_TO_3_MTS => array(
                'value' => 2.5,
                'min' => 2,
                'max' => 3
            ),
            Distance::BTW_3_TO_5_MTS=> array(
                'value' => 3.5,
                'min' => 3,
                'max' => 5
            ),
            Distance::BTW_5_TO_7_MTS=> array(
                'value' => 5.5,
                'min' => 5,
                'max' => 7
            ),
            Distance::BTW_7_TO_10_MTS=> array(
                'value' => 7.5,
                'min' => 7,
                'max' => 10
            ),
            Distance::BTW_10_TO_15_MTS=> array(
                'value' => 10.5,
                'min' => 10,
                'max' => 15
            ),
            Distance::MORE_THAN_15_MTS => array(
                'value' => 15.5,
                'min' => 15,
                'max' => 20
            ),
        );
        
        $sumQuantity = $this->sumQuantity($data);
        $sum = 0;
        foreach ($data as $item) {
            $sum += $distances[$item['distance']]['value'] * $item['quantity'];
        }
        
        $average = $sumQuantity ? $sum / $sumQuantity : 0;
        $key = 0;
        if ($average) {
            foreach ($distances as $key => $distance) {
                if ($distance['min'] <= $average && $distance['max'] >= $average) {
                    break;
                }
            }
        }
        
        return array('total' => $sumQuantity, 'distance' => $key);
    }
    
}
