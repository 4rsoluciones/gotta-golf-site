<?php

namespace AndresGotta\Bundle\GolfBundle\Report;

use AndresGotta\Bundle\GolfBundle\ValueObject\Result;

class ReportTotalStrikesInsideGreenStrategy extends BaseStrategy
{
    /**
     * Se toman en cuenta únicamente los: Primer tiro en Par 3, segundo tiro en par 4 y tercer tiro en par 5.
     * Esa suma da el total de tiros. Luego, de ese total, se muestra el porcentaje de ellos que dieron como
     * resultado en Green o embocada y se muestra el valor en relativo.
     *
     * NOTA: si en un par 4 el primer tiro da como resultado en Green o embocada, se usa ese y no el segundo tiro.
     * Si en un par 5, el segundo tiro da como resultado en Green o embocada, se toma ese y no el tercero.
     *
     * @return array
     */
    public function generateStats()
    {
        $firstStrikesResults = $this->generateFirstStrikesPar3InsideGreenStats();
        $secondStrikesResults = $this->generateSecondStrikesPar4InsideGreenStats();
        $thirdStrikesResults = $this->generateThirdStrikesPar5InsideGreenStats();

        // 
        $result[] = array('strike' => 'first_strike_par3', 'quantity' => $firstStrikesResults);
        $result[] = array('strike' => 'second_strike_par4', 'quantity' => $secondStrikesResults);
        $result[] = array('strike' => 'third_strike_par5', 'quantity' => $thirdStrikesResults[0]['quantity']);
        
        $this->applyPercents($result);
        
        return compact('result');
    }
    
    private function generateFirstStrikesPar3InsideGreenStats()
    {
        $par = 3;
        $this->preprocessFilters();
        
        $results = $this->queryBuilder
            ->select('COUNT(FirstStrike.id) as quantity')
            ->from('AndresGottaGolfBundle:FirstStrike', 'FirstStrike')
            ->innerJoin('FirstStrike.hole', 'Hole')
            ->innerJoin('Hole.round', 'Round')
            ->andWhere('Hole.par = :par')
            ->setParameter('par', $par)
            ->andWhere('FirstStrike.result IN (:result)')
            ->setParameter('result', [Result::INSIDE_GREEN, Result::HOLE_OUT])
            ->getQuery()
            ->getSingleScalarResult()
        ;

        return $results;
    }
    
    private function generateSecondStrikesPar4InsideGreenStats()
    {
        $par = 4;
        $this->preprocessFilters();
        
        $results = $this->queryBuilder
            ->select('COUNT(FirstStrike.id) as quantity')
            ->from('AndresGottaGolfBundle:FirstStrike', 'FirstStrike')
            ->innerJoin('FirstStrike.hole', 'Hole')
            ->innerJoin('Hole.round', 'Round')
            ->andWhere('Hole.par = :par')
            ->setParameter('par', $par)
            ->andWhere('FirstStrike.result IN (:result)')
            ->setParameter('result', [Result::INSIDE_GREEN, Result::HOLE_OUT])
            ->getQuery()
            ->getSingleScalarResult()
        ;

        $this->preprocessFilters();

        $results += $this->queryBuilder
            ->select('COUNT(SecondStrike.id) as quantity')
            ->from('AndresGottaGolfBundle:SecondStrike', 'SecondStrike')
            ->innerJoin('SecondStrike.hole', 'Hole')
            ->innerJoin('Hole.round', 'Round')
            ->andWhere('Hole.par = :par')
            ->setParameter('par', $par)
            ->andWhere('SecondStrike.result IN (:result)')
            ->setParameter('result', [Result::INSIDE_GREEN, Result::HOLE_OUT])
            ->getQuery()
            ->getSingleScalarResult()
        ;

        return $results;
    }
    
    private function generateThirdStrikesPar5InsideGreenStats()
    {
        $par = 5;

        $this->preprocessFilters();
        $results = $this->queryBuilder
            ->select('COUNT(SecondStrike.id) as quantity')
            ->from('AndresGottaGolfBundle:SecondStrike', 'SecondStrike')
            ->innerJoin('SecondStrike.hole', 'Hole')
            ->innerJoin('Hole.round', 'Round')
            ->andWhere('Hole.par = :par')
            ->setParameter('par', $par)
            ->andWhere('SecondStrike.result IN (:result)')
            ->setParameter('result', [Result::INSIDE_GREEN, Result::HOLE_OUT])
            ->getQuery()
            ->getSingleScalarResult()
        ;

        $this->preprocessFilters();
        $results += $this->queryBuilder
            ->select('COUNT(ThirdStrike.id) as quantity')
            ->from('AndresGottaGolfBundle:ThirdStrike', 'ThirdStrike')
            ->innerJoin('ThirdStrike.hole', 'Hole')
            ->innerJoin('Hole.round', 'Round')
            ->andWhere('Hole.par = :par')
            ->setParameter('par', $par)
            ->andWhere('ThirdStrike.result IN (:result)')
            ->setParameter('result', [Result::INSIDE_GREEN, Result::HOLE_OUT])
            ->getQuery()
            ->getSingleScalarResult()
        ;

        return $results;
    }
    
    private function sumQuantity($data)
    {
        $sum = 0;
        foreach ($data as $item) {
            $sum += $item['quantity'];
        }

        return $sum;
    }

    private function applyPercents(&$data)
    {
        $sum = $this->sumQuantity($data);
        foreach ($data as &$item) {
            $item['quantity'] *= $sum ? 100 / $sum : 0;
        }
        unset($item);
    }
    
}
