<?php

namespace AndresGotta\Bundle\GolfBundle\Report;

use AndresGotta\Bundle\GolfBundle\ValueObject\Result;

class ReportSecondStrikePar4OutsideGreenStrategy extends BaseStrategy
{
    /**
     * REPORTE 2 - PAR 4 c)
     * 
     * Procesa el reporte de vueltas
     * 
     * Resultado (en porcentaje) del segundo tiro en cada hoyo par 4 en cada rango de distancia (11 en total)  porcentajes de cada resultado obtenido desde cada distancia,  
     *   a)embocada (Águila)
     *   b) en green – 
     *   c) fuera del green y sus derivados (5 en total) 
     *
     * @return array
     */
    public function generateStats()
    {
        $par = 4;

        $distancesResults = $this->generateDistanceOutsideGreenStats($par);
        $reasonResults = $this->generateReasonOutsideGreenStats($par);

        return compact('distancesResults', 'reasonResults');
    }
    
    private function generateDistanceOutsideGreenStats($par)
    {
        $this->preprocessFilters();
        $result = Result::OUTSIDE_GREEN;
        $results = $this->queryBuilder
                ->select('SecondStrike.distance, COUNT(SecondStrike.id) as quantity')
                ->from('AndresGottaGolfBundle:SecondStrike', 'SecondStrike')
                ->innerJoin('SecondStrike.hole', 'Hole')
                ->innerJoin('Hole.round', 'Round')
                ->andWhere('Hole.par = :par')
                ->setParameter('par', $par)
                ->andWhere('SecondStrike.result LIKE :result')
                ->setParameter('result', "%{$result}%")
                ->addGroupBy('SecondStrike.distance')
                ->orderBy('quantity', 'desc')
                ->getQuery()
                ->getResult();
        
        $this->applyPercents($results);
        
        return $results;
    }
    
    private function generateReasonOutsideGreenStats($par)
    {
        $this->preprocessFilters();
        $result = Result::OUTSIDE_GREEN;
        $results = $this->queryBuilder
                ->select('SecondStrike.reason, COUNT(SecondStrike.id) as quantity')
                ->from('AndresGottaGolfBundle:SecondStrike', 'SecondStrike')
                ->innerJoin('SecondStrike.hole', 'Hole')
                ->innerJoin('Hole.round', 'Round')
                ->andWhere('Hole.par = :par')
                ->setParameter('par', $par)
                ->andWhere('SecondStrike.result LIKE :result')
                ->setParameter('result', "%{$result}%")
                ->groupBy('SecondStrike.reason')
                ->orderBy('quantity', 'desc')
                ->getQuery()
                ->getResult();
        
        $this->applyPercents($results);
        
        return $results;
    }
    
    private function sumQuantity($data)
    {
        $sum = 0;
        foreach ($data as $item) {
            $sum += $item['quantity'];
        }

        return $sum;
    }

    private function applyPercents(&$data)
    {
        $sum = $this->sumQuantity($data);
        foreach ($data as &$item) {
            $item['quantity'] *= $sum ? 100 / $sum : 0;
        }
        unset($item);
    }
    
}
