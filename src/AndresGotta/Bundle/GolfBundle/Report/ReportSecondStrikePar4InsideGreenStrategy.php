<?php

namespace AndresGotta\Bundle\GolfBundle\Report;

use AndresGotta\Bundle\GolfBundle\ValueObject\Result;

class ReportSecondStrikePar4InsideGreenStrategy extends BaseStrategy
{
    /**
     * REPORTE 2 - PAR 4 b)
     * 
     * Procesa el reporte de vueltas
     * 
     * Resultado (en porcentaje) del segundo tiro en cada hoyo par 4 en cada rango de distancia (11 en total)  porcentajes de cada resultado obtenido desde cada distancia,  
     *   a)embocada (Águila)
     *   b) en green – 
     *   c) fuera del green y sus derivados (5 en total) 
     *
     * @return array
     */
    public function generateStats()
    {
        $this->preprocessFilters();

        $par = 4;

        $data = $this->queryBuilder
                ->select('SecondStrike.distance, COUNT(SecondStrike.id) as quantity')
                ->from('AndresGottaGolfBundle:SecondStrike', 'SecondStrike')
                ->innerJoin('SecondStrike.hole', 'Hole')
                ->innerJoin('Hole.round', 'Round')
                ->andWhere('Hole.par = :par')
                ->setParameter('par', $par)
                ->andWhere('SecondStrike.result = :result')
                ->setParameter('result', Result::INSIDE_GREEN)
                ->addGroupBy('SecondStrike.distance')
                ->orderBy('quantity', 'desc')
                ->getQuery()
                ->getResult();
        //TODO: Optimizar

        $sum = 0;
        foreach ($data as $item) {
            $sum += $item['quantity'];
        }
        foreach ($data as &$item) {
            $item['quantity'] *= $sum ? 100 / $sum : 0;
        }
        unset($item);

        return $data;
    }
}
