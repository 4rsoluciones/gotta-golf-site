<?php

namespace AndresGotta\Bundle\GolfBundle\Report;

use AndresGotta\Bundle\GolfBundle\ValueObject\Result;

class ReportPuttPar4HoleOutStrategy extends BaseStrategy
{
    /**
     * REPORTE 5 - PAR 4
     * 
     * Efectividad con el Putter en PAR 4
     * 
     * Del total de golpes cargados sobre el green en hoyos PAR 4, porcentaje de embocados en un intento de:
     *   a)menos de 1 metro, 
     *   b) 1 a 2 mts
     *   c) 2 a 3 mts
     *   d) mas de 3 metros.
     *
     * @return array
     */
    public function generateStats()
    {
        $this->preprocessFilters();

        $par = 4;

        $data = $this->queryBuilder
                ->select('Putt.distance, COUNT(Putt.id) as quantity')
                ->from('AndresGottaGolfBundle:Putt', 'Putt')
                ->innerJoin('Putt.hole', 'Hole')
                ->innerJoin('Hole.round', 'Round')
                ->andWhere('Hole.par = :par')
                ->setParameter('par', $par)
                ->andWhere('Putt.result = :result')
                ->setParameter('result', Result::HOLE_OUT)
                ->addGroupBy('Putt.distance')
                ->orderBy('quantity', 'desc')
                ->getQuery()
                ->getResult();

        $sum = 0;
        foreach ($data as $item) {
            $sum += $item['quantity'];
        }
        foreach ($data as &$item) {
            $item['quantity'] *= $sum ? 100 / $sum : 0;
        }
        unset($item);

        return $data;
    }
}
