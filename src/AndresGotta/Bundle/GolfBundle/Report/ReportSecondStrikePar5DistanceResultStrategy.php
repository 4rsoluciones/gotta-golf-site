<?php

namespace AndresGotta\Bundle\GolfBundle\Report;

use AndresGotta\Bundle\GolfBundle\Entity\SecondStrike;
use AndresGotta\Bundle\GolfBundle\ValueObject\Club;
use AndresGotta\Bundle\GolfBundle\ValueObject\Reason;
use AndresGotta\Bundle\GolfBundle\ValueObject\Result;

class ReportSecondStrikePar5DistanceResultStrategy extends BaseStrategy
{
    /**
     * REPORTE 9 - PAR 5
     * 
     * Tomando únicamente los segundos tiros de PAR 5 y 
     * agrupando por palo utilizado, distancia de golpe, 
     * calculo el porcentaje de cada resultado sobre el 
     * total de esos segundos golpes en PAR 5.
     */
    public function generateStats()
    {
        $par = 5;
        $data = array();
        $clubs = Club::getChoices();
        $distances = SecondStrike::getDistanceChoicesByPar($par);
        $results = array(
            Result::HOLE_OUT,
            Result::INSIDE_GREEN,
            Result::INSIDE_FAIRWAY,
            Result::OUTSIDE_FAIRWAY,
        );
        $reasons = array(
            Reason::TO_THE_LEFT,
            Reason::TO_THE_RIGHT,
        );
        foreach ($clubs as $club) {
            foreach ($distances as $distance) {
                foreach ($results as $result) {
                    if ($result != Result::OUTSIDE_FAIRWAY) {
                        $data[$club][$distance][$result] = $this->generateSecondStrikePar5DistanceResultStats($par, $distance, $club, $result);
                    }
                }
            }
        }
        foreach ($clubs as $club) {
            foreach ($distances as $distance) {
                foreach ($results as $result) {
                    if ($result == Result::OUTSIDE_FAIRWAY) {
                        $data[$club][$distance][$reasons[0]] = $this->generateSecondStrikePar5DistanceResultReasonStats($par, $distance, $club, $result, $reasons[0]);
                        $data[$club][$distance][$reasons[1]] = $this->generateSecondStrikePar5DistanceResultReasonStats($par, $distance, $club, $result, $reasons[1]);
                    }
                }
            }
        }
        
        $this->applyPercents($data);
        
        return $data;
    }
    
    private function generateSecondStrikePar5DistanceResultStats($par, $distance, $club, $result)
    {        
        $this->preprocessFilters();
        
        $results = $this->queryBuilder
                ->select('COUNT(SecondStrike.id) as quantity')
                ->from('AndresGottaGolfBundle:SecondStrike', 'SecondStrike')
                ->innerJoin('SecondStrike.hole', 'Hole')
                ->innerJoin('Hole.round', 'Round')
                ->andWhere('Hole.par = :par')
                ->setParameter('par', $par)
                ->andWhere('SecondStrike.club = :club')
                ->setParameter('club', $club)
                ->andWhere('SecondStrike.distance = :distance')
                ->setParameter('distance', $distance)
                ->andWhere('SecondStrike.result LIKE :result')
                ->setParameter('result', "%{$result}%")
                ;
        
        return $results->getQuery()->getSingleScalarResult();
    }
    
    private function generateSecondStrikePar5DistanceResultReasonStats($par, $distance, $club, $result, $reason)
    {        
        $this->preprocessFilters();
        
        $results = $this->queryBuilder
                ->select('COUNT(SecondStrike.id) as quantity')
                ->from('AndresGottaGolfBundle:SecondStrike', 'SecondStrike')
                ->innerJoin('SecondStrike.hole', 'Hole')
                ->innerJoin('Hole.round', 'Round')
                ->andWhere('Hole.par = :par')
                ->setParameter('par', $par)
                ->andWhere('SecondStrike.club = :club')
                ->setParameter('club', $club)
                ->andWhere('SecondStrike.distance = :distance')
                ->setParameter('distance', $distance)
                ->andWhere('SecondStrike.result LIKE :result')
                ->setParameter('result', "%{$result}%")
                ->andWhere('SecondStrike.reason = :reason')
                ->setParameter('reason', $reason)
                ;
        
        return $results->getQuery()->getSingleScalarResult();
    }

    private function applyPercents(&$data)
    {
        $sum = 0;
        foreach ($data as $items) {
            foreach ($items as $item) {
                foreach ($item as $value) {
                    $sum += $value;
                }
            }
        }
        
        foreach ($data as &$items) {
            foreach ($items as &$item) {
                foreach ($item as &$value) {
                    $value *= $sum ? 100 / $sum : 0;
                }
            }
            unset($value);
        }
    }
    
}
