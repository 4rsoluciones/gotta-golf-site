<?php

namespace AndresGotta\Bundle\GolfBundle\Report;

use AndresGotta\Bundle\GolfBundle\Entity\Putt;
use AndresGotta\Bundle\GolfBundle\Entity\SecondStrike;
use AndresGotta\Bundle\GolfBundle\ValueObject\Result;

class ReportSecondStrikePar4DistanceToHoleStrategy extends BaseStrategy
{
    /**
     * REPORTE 3 - PAR 4
     */
    public function generateStats()
    {
        $par = 4;
        $distances = SecondStrike::getDistanceChoicesByPar($par);
        $puttDistances = Putt::getDistanceChoicesByPar($par);
        $results = array();
        foreach ($puttDistances as $puttDistance) {
            foreach ($distances as $distance) {
                $results[$puttDistance][$distance] = $this->getQuantityDistanceToHoleOut($par, $puttDistance, $distance);
            }
        }
        $this->applyPercents($results);

        return $results;
    }
    
    private function getQuantityDistanceToHoleOut($par, $puttDistance, $distance)
    {
        $this->preprocessFilters();
        
        $query = $this->queryBuilder
                ->select('COUNT(SecondStrike.id)')
                ->from('AndresGottaGolfBundle:SecondStrike', 'SecondStrike')
                ->innerJoin('SecondStrike.hole', 'Hole')
                ->innerJoin('Hole.round', 'Round')
                ->innerJoin('AndresGottaGolfBundle:Putt', 'Putt', 'WITH', 'Putt.hole = Hole')
                ->andWhere('Hole.par = :par')
                ->setParameter('par', $par)
                ->andWhere('SecondStrike.result = :result')
                ->setParameter('result', Result::INSIDE_GREEN)
                ->andWhere('SecondStrike.distance = :distance')
                ->setParameter('distance', $distance)
                ->andWhere('Putt.distance = :puttDistance')
                ->setParameter('puttDistance', $puttDistance)
                ->getQuery()
                ->getSingleScalarResult();
        
        return $query;
    }
        
    private function sumQuantity($data)
    {
        $sum = 0;
        foreach ($data as $item) {
            foreach ($item as $value) {
                $sum += $value;
            }
        }

        return $sum;
    }

    private function applyPercents(&$data)
    {
        $sum = $this->sumQuantity($data);
        foreach ($data as &$item) {
            foreach ($item as &$value) {
                $value *= $sum ?  100 / $sum : 0;
            }
        }
        unset($value);
        unset($item);
    }
    
}
