<?php

namespace AndresGotta\Bundle\GolfBundle\Report;

use AndresGotta\Bundle\GolfBundle\Entity\FirstStrike;
use AndresGotta\Bundle\GolfBundle\ValueObject\BallFlight;
use AndresGotta\Bundle\GolfBundle\ValueObject\Club;
use AndresGotta\Bundle\GolfBundle\ValueObject\Reason;
use AndresGotta\Bundle\GolfBundle\ValueObject\Result;

class ReportTrendsTeeErrorsPar5Strategy extends BaseStrategy
{
    /**
     * REPORTE 8.2 - PAR 5
     */
    public function generateStats()
    {
        $par = 5;
        $reasons = array(Reason::TO_THE_LEFT, Reason::TO_THE_RIGHT);
        $clubs = Club::getChoices();
        $ballFlights = BallFlight::getChoices();
        $distances = FirstStrike::getDistanceChoicesByPar($par);
        $result = array();
        $total = 0;
        foreach ($reasons as $reason) {
            foreach ($clubs as $club) {
                foreach ($ballFlights as $ballFlight) {
                    foreach ($distances as $distance) {
                        $sum = $this->generateOutsideFairwayTotalsByBallFlight($par, $club, $distance, $reason, $ballFlight);
                        $result[$reason][$club][$ballFlight][$distance] = $sum;
                        $total += $sum;
                    }
                }
            }
        }
        $this->applyPercents($result, $total);

        return $result;
    }
    
    public function generateOutsideFairwayTotalsByBallFlight($par, $club, $distance, $reason, $ballFlight)
    {
        $this->preprocessFilters();
        $result = Result::OUTSIDE_FAIRWAY;
        $results = $this->queryBuilder
            ->select('COUNT(FirstStrike.id) as quantity')
            ->from('AndresGottaGolfBundle:FirstStrike', 'FirstStrike')
            ->innerJoin('FirstStrike.hole', 'Hole')
            ->innerJoin('Hole.round', 'Round')
            ->andWhere('Hole.par = :par')->setParameter('par', $par)
            ->andWhere('FirstStrike.result = :result')->setParameter('result', $result)
            ->andWhere('FirstStrike.club = :club')->setParameter('club', $club)
            ->andWhere('FirstStrike.distance = :distance')->setParameter('distance', $distance)
            ->andWhere('FirstStrike.reason = :reason')->setParameter('reason', $reason)
            ->andWhere('FirstStrike.ballFlight = :ballFlight')->setParameter('ballFlight', $ballFlight)
            ->getQuery()
            ->getSingleScalarResult()
        ;


        return $results;
    }

    /**
     * @param $data
     * @param $sum
     */
    private function applyPercents(&$data, $sum)
    {
        foreach ($data as &$reason) {
            foreach ($reason as &$club) {
                foreach ($club as &$ballFlight) {
                    foreach ($ballFlight as &$item) {
                        $item *= $sum ? 100 / $sum : 0;
                    }
                    unset($item);
                }
            }
        }
    }
}
