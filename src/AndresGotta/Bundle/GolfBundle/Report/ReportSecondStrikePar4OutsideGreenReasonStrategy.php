<?php

namespace AndresGotta\Bundle\GolfBundle\Report;

use AndresGotta\Bundle\GolfBundle\Entity\SecondStrike;
use AndresGotta\Bundle\GolfBundle\ValueObject\Reason;
use AndresGotta\Bundle\GolfBundle\ValueObject\Result;

class ReportSecondStrikePar4OutsideGreenReasonStrategy extends BaseStrategy
{
    /**
     * REPORTE 4 - PAR 4
     */
    public function generateStats()
    {
        $par = 4;
        $reasons = array(
            Reason::SHORT,
            Reason::LONG,
            Reason::TO_THE_RIGHT,
            Reason::TO_THE_LEFT,
        );
        $distances = SecondStrike::getDistanceChoicesByPar($par);
        
        foreach ($reasons as $reason) {
            foreach ($distances as $distance) {
                $results[$reason][$distance] = $this->generateOutsideGreenReasonStats($par, $reason, $distance);
            }
        }
        $this->applyPercents($results);
        
        return $results;
    }
    
    private function generateOutsideGreenReasonStats($par, $reason, $distance)
    {
        $this->preprocessFilters();
        $result = Result::OUTSIDE_GREEN;
        $results = $this->queryBuilder
                ->select('COUNT(SecondStrike.id)')
                ->from('AndresGottaGolfBundle:SecondStrike', 'SecondStrike')
                ->innerJoin('SecondStrike.hole', 'Hole')
                ->innerJoin('Hole.round', 'Round')
                ->andWhere('Hole.par = :par')
                ->setParameter('par', $par)
                ->andWhere('SecondStrike.result LIKE :result')
                ->setParameter('result', "%{$result}%")
                ->andWhere('SecondStrike.reason = :reason')
                ->setParameter('reason', $reason)
                ->andWhere('SecondStrike.distance = :distance')
                ->setParameter('distance', $distance)
                ->getQuery()
                ->getSingleScalarResult();
        
        return $results;
    }
        
    private function sumQuantity($data)
    {
        $sum = 0;
        foreach ($data as $item) {
            foreach ($item as $value) {
                $sum += $value;
            }
        }

        return $sum;
    }

    private function applyPercents(&$data)
    {
        $sum = $this->sumQuantity($data);
        foreach ($data as &$item) {
            foreach ($item as &$value) {
                $value *= $sum ?  100 / $sum : 0;
            }
        }
        unset($value);
        unset($item);
    }
    
}
