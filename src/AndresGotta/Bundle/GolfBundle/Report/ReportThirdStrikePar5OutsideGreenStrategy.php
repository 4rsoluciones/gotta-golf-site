<?php

namespace AndresGotta\Bundle\GolfBundle\Report;

use AndresGotta\Bundle\GolfBundle\ValueObject\Result;

class ReportThirdStrikePar5OutsideGreenStrategy extends BaseStrategy
{
    /**
     * REPORTE 2 - PAR 5 c)
     * 
     * Resultado (en porcentaje) del tercer tiro en cada hoyo par 5 en cada rango de distancia (8 en total) ( porcentajes desde cada distancia,  
     *   a)embocada (Águila)
     *   b) en green – 
     *   c) fuera del green y sus derivados (5 en total) , 
     *
     * @return array
     */
    public function generateStats()
    {
        $par = 5;

        $distancesResults = $this->generateDistanceOutsideGreenStats($par);
        $reasonResults = $this->generateReasonOutsideGreenStats($par);

        return compact('distancesResults', 'reasonResults');
    }
    
    private function generateDistanceOutsideGreenStats($par)
    {
        $this->preprocessFilters();
        $result = Result::OUTSIDE_GREEN;
        $results = $this->queryBuilder
                ->select('ThirdStrike.distance, COUNT(ThirdStrike.id) as quantity')
                ->from('AndresGottaGolfBundle:ThirdStrike', 'ThirdStrike')
                ->innerJoin('ThirdStrike.hole', 'Hole')
                ->innerJoin('Hole.round', 'Round')
                ->andWhere('Hole.par = :par')
                ->setParameter('par', $par)
                ->andWhere('ThirdStrike.result LIKE :result')
                ->setParameter('result', "%{$result}%")
                ->addGroupBy('ThirdStrike.distance')
                ->orderBy('quantity', 'desc')
                ->getQuery()
                ->getResult();
        
        $this->applyPercents($results);
        
        return $results;
    }
    
    private function generateReasonOutsideGreenStats($par)
    {
        $this->preprocessFilters();
        $result = Result::OUTSIDE_GREEN;
        $results = $this->queryBuilder
                ->select('ThirdStrike.reason, COUNT(ThirdStrike.id) as quantity')
                ->from('AndresGottaGolfBundle:ThirdStrike', 'ThirdStrike')
                ->innerJoin('ThirdStrike.hole', 'Hole')
                ->innerJoin('Hole.round', 'Round')
                ->andWhere('Hole.par = :par')
                ->setParameter('par', $par)
                ->andWhere('ThirdStrike.result LIKE :result')
                ->setParameter('result', "%{$result}%")
                ->groupBy('ThirdStrike.reason')
                ->orderBy('quantity', 'desc')
                ->getQuery()
                ->getResult();
        
        $this->applyPercents($results);
        
        return $results;
    }
    
    private function sumQuantity($data)
    {
        $sum = 0;
        foreach ($data as $item) {
            $sum += $item['quantity'];
        }

        return $sum;
    }

    private function applyPercents(&$data)
    {
        $sum = $this->sumQuantity($data);
        foreach ($data as &$item) {
            $item['quantity'] *= $sum ? 100 / $sum : 0;
        }
        unset($item);
    }
    
}
