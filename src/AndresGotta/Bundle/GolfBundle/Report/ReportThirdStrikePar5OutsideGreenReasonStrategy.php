<?php

namespace AndresGotta\Bundle\GolfBundle\Report;

use AndresGotta\Bundle\GolfBundle\Entity\ThirdStrike;
use AndresGotta\Bundle\GolfBundle\ValueObject\Reason;
use AndresGotta\Bundle\GolfBundle\ValueObject\Result;

class ReportThirdStrikePar5OutsideGreenReasonStrategy extends BaseStrategy
{
    /**
     * REPORTE 4 - PAR 5
     */
    public function generateStats()
    {
        $par = 5;
        $reasons = array(
            Reason::SHORT,
            Reason::LONG,
            Reason::TO_THE_RIGHT,
            Reason::TO_THE_LEFT,
        );
        $distances = ThirdStrike::getDistanceChoicesByPar($par);
        
        foreach ($reasons as $reason) {
            foreach ($distances as $distance) {
                $results[$reason][$distance] = $this->generateOutsideGreenReasonStats($par, $reason, $distance);
            }
        }
        $this->applyPercents($results);
        
        return $results;
    }
    
    private function generateOutsideGreenReasonStats($par, $reason, $distance)
    {
        $this->preprocessFilters();
        $result = Result::OUTSIDE_GREEN;
        $results = $this->queryBuilder
                ->select('COUNT(ThirdStrike.id)')
                ->from('AndresGottaGolfBundle:ThirdStrike', 'ThirdStrike')
                ->innerJoin('ThirdStrike.hole', 'Hole')
                ->innerJoin('Hole.round', 'Round')
                ->andWhere('Hole.par = :par')
                ->setParameter('par', $par)
                ->andWhere('ThirdStrike.result LIKE :result')
                ->setParameter('result', "%{$result}%")
                ->andWhere('ThirdStrike.reason = :reason')
                ->setParameter('reason', $reason)
                ->andWhere('ThirdStrike.distance = :distance')
                ->setParameter('distance', $distance)
                ->getQuery()
                ->getSingleScalarResult();
        
        return $results;
    }
        
    private function sumQuantity($data)
    {
        $sum = 0;
        foreach ($data as $item) {
            foreach ($item as $value) {
                $sum += $value;
            }
        }

        return $sum;
    }

    private function applyPercents(&$data)
    {
        $sum = $this->sumQuantity($data);
        foreach ($data as &$item) {
            foreach ($item as &$value) {
                $value *= $sum ?  100 / $sum : 0;
            }
        }
        unset($value);
        unset($item);
    }
    
}
