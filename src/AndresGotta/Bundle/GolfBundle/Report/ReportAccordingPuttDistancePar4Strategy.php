<?php

namespace AndresGotta\Bundle\GolfBundle\Report;

use AndresGotta\Bundle\GolfBundle\ValueObject\Result;
use Doctrine\ORM\Query\Expr\Join;

class ReportAccordingPuttDistancePar4Strategy extends BaseStrategy
{
    /**
     * REPORTE 4.2.1 - PAR 4
     */
    public function generateStats()
    {
        return $this->generateTotalStrikesByOutsideGreenReasons();
    }
    
    private function generateTotalStrikesByOutsideGreenReasons()
    {
        $this->preprocessFilters();
        $resultSecondStrike = Result::OUTSIDE_GREEN;
        $par = 4;
        $results = $this->queryBuilder
                ->select('Putt.distance, COUNT(SecondStrike.id) as quantity')
                ->from('AndresGottaGolfBundle:SecondStrike', 'SecondStrike')
                ->innerJoin('SecondStrike.hole', 'Hole')
                ->innerJoin('Hole.round', 'Round')
                ->innerJoin('AndresGottaGolfBundle:SecondStrikeApproach', 'SecondStrikeApproach', Join::WITH, 'SecondStrikeApproach.hole = Hole')
                ->innerJoin('AndresGottaGolfBundle:Putt', 'Putt', Join::WITH, 'Putt.hole = Hole')
                ->andWhere('Hole.par = :par')
                ->setParameter('par', $par)
                ->andWhere('SecondStrike.result LIKE :resultSecondStrike')
                ->setParameter('resultSecondStrike', "%{$resultSecondStrike}%")
                ->andWhere('SecondStrikeApproach.result = :resultSecondStrikeApproach')
                ->setParameter('resultSecondStrikeApproach', Result::INSIDE_GREEN)
                ->andWhere('Putt.result = :resultPutt')
                ->setParameter('resultPutt', Result::HOLE_OUT)
                ->addGroupBy('Putt.distance')
                ->getQuery()
                ->getResult()
                ;
                
        $this->applyPercents($results);
        
        return $results;
    }
    
    private function sumQuantity($data)
    {
        $sum = 0;
        foreach ($data as $item) {
            $sum += $item['quantity'];
        }

        return $sum;
    }

    private function applyPercents(&$data)
    {
        $sum = $this->sumQuantity($data);
        foreach ($data as &$item) {
            $item['quantity'] *= $sum ?  100 / $sum : 0;
        }
        unset($item);
    }
}
