<?php

namespace AndresGotta\Bundle\GolfBundle\Report;

use AndresGotta\Bundle\GolfBundle\ValueObject\Distance;
use AndresGotta\Bundle\GolfBundle\ValueObject\Result;
use Doctrine\ORM\Query\Expr\Join;

class ReportAverageDistanceToBirdiePar3Strategy extends BaseStrategy
{
    /**
     * REPORTE 3.1 - PAR 3
     * 
     * De los tiros de salida en hoyos PAR 3 de los que tienen como 
     * resultado en Green, se suman las distancias (se obtienen desde 
     * la distancia que jugó el primer putt) y se saca un promedio 
     * sobre el total de tiros de salida en par 3.
     */
    public function generateStats()
    {
        $puttDistances = $this->getPuttDistanceQuantity();
        
        return $this->calculateDistanceAverage($puttDistances);
    }
    
    private function getPuttDistanceQuantity()
    {
        $this->preprocessFilters();
        
        $par = 3;
        $results = $this->queryBuilder
            ->select('Putt.distance, COUNT(FirstStrike.id) as quantity')
            ->from('AndresGottaGolfBundle:FirstStrike', 'FirstStrike')
            ->innerJoin('FirstStrike.hole', 'Hole')
            ->innerJoin('Hole.round', 'Round')
            ->innerJoin('AndresGottaGolfBundle:Putt', 'Putt', Join::WITH, 'Putt.hole = Hole')
            ->andWhere('Hole.par = :par')
            ->setParameter('par', $par)
            ->andWhere('FirstStrike.result = :resultFirstStrike')
            ->setParameter('resultFirstStrike', Result::INSIDE_GREEN)
            ->addGroupBy('Putt.distance')
            ->getQuery()
            ->getResult()
        ;
        
        return $results;
    }
    
    private function sumQuantity($data)
    {
        $sum = 0;
        foreach ($data as $item) {
            $sum += $item['quantity'];
        }

        return $sum;
    }
    
    private function calculateDistanceAverage($data)
    {
        $distances = array(
            Distance::LESS_THAN_1_MTS => array(
                'value' => 0.5,
                'min' => 0,
                'max' => 1
            ),
            Distance::BTW_1_TO_2_MTS => array(
                'value' => 1.5,
                'min' => 1,
                'max' => 2
            ),
            Distance::BTW_2_TO_3_MTS => array(
                'value' => 2.5,
                'min' => 2,
                'max' => 3
            ),
            Distance::BTW_3_TO_5_MTS=> array(
                'value' => 3.5,
                'min' => 3,
                'max' => 5
            ),
            Distance::BTW_5_TO_7_MTS=> array(
                'value' => 5.5,
                'min' => 5,
                'max' => 7
            ),
            Distance::BTW_7_TO_10_MTS=> array(
                'value' => 7.5,
                'min' => 7,
                'max' => 10
            ),
            Distance::BTW_10_TO_15_MTS=> array(
                'value' => 10.5,
                'min' => 10,
                'max' => 15
            ),
            Distance::MORE_THAN_15_MTS => array(
                'value' => 15.5,
                'min' => 15,
                'max' => 20
            ),
        );
        
        $sumQuantity = $this->sumQuantity($data);
        $sum = 0;
        foreach ($data as $item) {
            $sum += $distances[$item['distance']]['value'] * $item['quantity'];
        }
        
        $average = $sumQuantity ? $sum / $sumQuantity : 0;
        $key = 0;
        if ($average) {
            foreach ($distances as $key => $distance) {
                if ($distance['min'] <= $average && $distance['max'] >= $average) {
                    break;
                }
            }
        }
        
        return array('total' => $sumQuantity, 'distance' => $key);
    }
    
}
