<?php

namespace AndresGotta\Bundle\GolfBundle\Twig;

use AndresGotta\Bundle\GolfBundle\Entity\FirstStrike;
use AndresGotta\Bundle\GolfBundle\Entity\FirstStrikeApproach;
use AndresGotta\Bundle\GolfBundle\Entity\Putt;
use AndresGotta\Bundle\GolfBundle\Entity\PuttApproach;
use AndresGotta\Bundle\GolfBundle\Entity\SecondStrike;
use AndresGotta\Bundle\GolfBundle\Entity\SecondStrikeApproach;
use AndresGotta\Bundle\GolfBundle\Entity\ThirdStrike;
use AndresGotta\Bundle\GolfBundle\Entity\ThirdStrikeApproach;
use Twig_Extension;
use Twig_SimpleFunction;

/**
 * Description of StrikeGoodResultExtension
 *
 * @author gaston
 */
class StrikeGoodResultsExtension extends Twig_Extension
{

    public function getFunctions()
    {
        return array(
            new Twig_SimpleFunction('getFirstStrikeGoodResultChoicesByPar', array($this, 'getFirstStrikeGoodResultChoicesByPar')),
            new Twig_SimpleFunction('getFirstStrikeApproachGoodResultChoicesByPar', array($this, 'getFirstStrikeApproachGoodResultChoicesByPar')),
            new Twig_SimpleFunction('getSecondStrikeGoodResultChoicesByPar', array($this, 'getSecondStrikeGoodResultChoicesByPar')),
            new Twig_SimpleFunction('getSecondStrikeApproachGoodResultChoicesByPar', array($this, 'getSecondStrikeApproachGoodResultChoicesByPar')),
            new Twig_SimpleFunction('getThirdStrikeGoodResultChoicesByPar', array($this, 'getThirdStrikeGoodResultChoicesByPar')),
            new Twig_SimpleFunction('getThirdStrikeApproachGoodResultChoicesByPar', array($this, 'getThirdStrikeApproachGoodResultChoicesByPar')),
            new Twig_SimpleFunction('getPuttGoodResultChoicesByPar', array($this, 'getPuttGoodResultChoicesByPar')),
            new Twig_SimpleFunction('getPuttApproachGoodResultChoicesByPar', array($this, 'getPuttApproachGoodResultChoicesByPar')),
        );
    }

    public function getFirstStrikeGoodResultChoicesByPar($par)
    {
        return FirstStrike::getGoodResultChoicesByPar($par);
    }

    public function getFirstStrikeApproachGoodResultChoicesByPar($par)
    {
        return FirstStrikeApproach::getGoodResultChoicesByPar($par);
    }

    public function getSecondStrikeGoodResultChoicesByPar($par)
    {
        return SecondStrike::getGoodResultChoicesByPar($par);
    }

    public function getSecondStrikeApproachGoodResultChoicesByPar($par)
    {
        return SecondStrikeApproach::getGoodResultChoicesByPar($par);
    }

    public function getThirdStrikeGoodResultChoicesByPar($par)
    {
        return ThirdStrike::getGoodResultChoicesByPar($par);
    }

    public function getThirdStrikeApproachGoodResultChoicesByPar($par)
    {
        return ThirdStrikeApproach::getGoodResultChoicesByPar($par);
    }

    public function getPuttGoodResultChoicesByPar($par)
    {
        return Putt::getGoodResultChoicesByPar($par);
    }

    public function getPuttApproachGoodResultChoicesByPar($par)
    {
        return PuttApproach::getGoodResultChoicesByPar($par);
    }

    public function getName()
    {
        return 'andres_gotta.twig.strike_good_results_extension';
    }

}
