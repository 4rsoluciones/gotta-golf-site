<?php

namespace AndresGotta\Bundle\GolfBundle\Twig;

use AndresGotta\Bundle\GolfBundle\Entity\Comparative;
use AndresGotta\Bundle\GroupBundle\Entity\Group;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Class ComparativeExtension
 * @package AndresGotta\Bundle\GolfBundle\Twig
 */
class ComparativeExtension extends \Twig_Extension
{
    /**
     * @var ContainerInterface
     */
    protected $container;

    /**
     * ComparativeExtension constructor.
     * @param ContainerInterface $container
     */
    public function __construct(ContainerInterface $container)
    {
        $this->container = $container;
    }

    /**
     * @inheritdoc
     */
    public function getFunctions()
    {
        return [
            new \Twig_SimpleFunction('comparativeGraphic', [$this, 'getComparativeGraphic'], ['is_safe' => ['html']]),
            new \Twig_SimpleFunction('comparativeGraphicForGroup', [$this, 'getComparativeGraphicForGroup'], ['is_safe' => ['html']])
        ];
    }

    /**
     * @param Comparative $comparative
     * @return string
     */
    public function getComparativeGraphic(Comparative $comparative)
    {
        $manager = $this->container->get('comparative_manager');
        $result = $manager->getComparativeResults($comparative);

        if (!$result) {
            return $this->container->get('translator')->trans('no data');
        }

        $strategy = $this->container->get('render_strategy_factory')->create($comparative);
        $strategy->setTemplating($this->container->get('templating'));

        return $strategy->render($result);
    }

    /**
     * @param Comparative $comparative
     * @param Group $group
     * @return string
     */
    public function getComparativeGraphicForGroup(Comparative $comparative, Group $group)
    {
        $manager = $this->container->get('comparative_manager');
        $result = $manager->getComparativeResultsForGroup($comparative, $group);

        if (!$result) {
            return $this->container->get('translator')->trans('no data');
        }

        $strategy = $this->container->get('render_strategy_factory')->create($comparative);
        $strategy->setTemplating($this->container->get('templating'));

        return $strategy->render($result);
    }


    /**
     * @inheritdoc
     */
    public function getName()
    {
        return 'comparative_extension';
    }

}