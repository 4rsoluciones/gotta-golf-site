<?php

namespace AndresGotta\Bundle\GolfBundle\Twig;

use AndresGotta\Bundle\GolfBundle\Entity\Report;
use AndresGotta\Bundle\GolfBundle\Entity\Category;
use AndresGotta\Bundle\GolfBundle\Entity\ReportCategory;
use AndresGotta\Bundle\GolfBundle\Report\ReportDTO;
use Symfony\Component\Translation\DataCollectorTranslator;
use Symfony\Component\Translation\Translator;
use Twig_Extension;
use Twig_SimpleFunction;

/**
 * Description of StrikeGoodResultExtension
 *
 * @author gaston
 */
class ReportExtension extends Twig_Extension
{

    /**
     *
     * @var DataCollectorTranslator
     */
    private $translator;

    /**
     * ReportExtension constructor.
     * @param DataCollectorTranslator $translator
     */
    public function __construct($translator)
    {
        $this->translator = $translator;
    }

    /**
     * Functiones publicadas
     *
     * @return array
     */
    public function getFunctions()
    {
        return array(
            new Twig_SimpleFunction('getPercentPieData', array($this, 'getPercentPieData')),
            new Twig_SimpleFunction('get_filterable', array($this, 'getFilterableClass')),
        );
    }

    /**
     * 
     * Como condición la primer columna será el nombre y la segunda el porcentaje
     * 
     * @param ReportDTO $report
     * @param string $subCollection
     * @return array
     */
    public function getPercentPieData(ReportDTO $report, $subCollection = null)
    {
        $reportValues = $report->getValues();
        if ($subCollection) {
            $reportValues = $reportValues[$subCollection];
        }

        $values = array();
        foreach ($reportValues as $value) {
            $c = $this->translator->trans(current($value));
            $n = (double) number_format(next($value), 2);
            $values[] = array($c, $n);
        }

        return json_encode($values);
    }

    /**
     * @param Category $category
     * @return string
     */
    public function getFilterableClass(Category $category)
    {
        $categories = [];
        $categories[] = $category->getSlug();

        if ($category->getParent()) {
            $categories[] = $category->getParent()->getSlug();
        }

        return implode(' ', array_unique($categories));
    }

    /**
     * Nombre de la extensión
     *
     * @return string
     */
    public function getName()
    {
        return 'andres_gotta.twig.report_extension';
    }

}
