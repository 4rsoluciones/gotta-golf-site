<?php

namespace AndresGotta\Bundle\GolfBundle\Command;

use AndresGotta\Bundle\GolfBundle\Entity\DefaultField;
use AndresGotta\Bundle\GolfBundle\Entity\Field;
use AndresGotta\Bundle\GolfBundle\Entity\Round;
use Symfony\Bundle\FrameworkBundle\Command\ContainerAwareCommand;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;

class NormalizeRoundFieldsCommand extends ContainerAwareCommand
{
    /**
     * {@inheritdoc}
     */
    protected function configure()
    {
        $this
            ->setName('andres_gotta_golf:normalize_round_fields')
            ->setDescription('Realizar una migración de datos sobre vueltas y canchas');
    }

    /**
     * {@inheritdoc}
     */
    protected function execute(InputInterface $input, OutputInterface $output)
    {
        $validator = $this->getContainer()->get('validator');
        $em = $this->getContainer()->get('doctrine.orm.entity_manager');
        $rounds = $em->getRepository('AndresGottaGolfBundle:Round')->createQueryBuilder('Round')
            ->leftJoin('Round.field', 'Field')
            ->where('Field.id IS NULL')
            ->orderBy('Round.id', 'ASC')
            ->getQuery()->getResult()
        ;
        $c = 0;
        /** @var Round[] $rounds */
        foreach ($rounds as $round) {
            $field = Field::createFromRound($round);
            $defaultField = $this->getDefaultFieldFromField($field);
            $field->setDefaultField($defaultField);

            $errors = $validator->validate($field);
            if ((boolean) $errors->count()) {
                $output->writeln((string) $errors);
                continue;
            }
            $em->persist($field);
            $em->flush();
            $c++;
        }

        $output->writeln($c . ' fields created.');
        $output->writeln('Done!');
    }

    /**
     * @param Field $field
     * @return DefaultField
     * @throws \Doctrine\ORM\NonUniqueResultException
     */
    private function getDefaultFieldFromField(Field $field)
    {
        $validator = $this->getContainer()->get('validator');
        $defaultFieldRepository = $this->getContainer()->get('doctrine.orm.entity_manager')
            ->getRepository('AndresGottaGolfBundle:DefaultField');

        $defaultField = $defaultFieldRepository->findOneByField($field);

        if (!$defaultField) {
            $defaultField = DefaultField::createFromField($field);
            $errors = $validator->validate($defaultField);
            if ((boolean) $errors->count()) {
                return null;
            }
        }

        return $defaultField;
    }
}
