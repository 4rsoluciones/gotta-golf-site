<?php

namespace AndresGotta\Bundle\GolfBundle\Command;

use AndresGotta\Bundle\GolfBundle\Entity\PostCompetitiveQuestionnaire;
use AndresGotta\Bundle\GolfBundle\ValueObject\FeelingStatus;
use Symfony\Bundle\FrameworkBundle\Command\ContainerAwareCommand;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;

class UpdateFeelingStatusCommand extends ContainerAwareCommand
{
    /**
     * {@inheritdoc}
     */
    protected function configure()
    {
        $this
            ->setName('andres_gotta_golf:update_feeling_status')
            ->setDescription('Realizar una migración de datos de sensaciones');
    }

    /**
     * {@inheritdoc}
     */
    protected function execute(InputInterface $input, OutputInterface $output)
    {
        $em = $this->getContainer()->get('doctrine.orm.entity_manager');
        $repo = $em->getRepository('AndresGottaGolfBundle:PostCompetitiveQuestionnaire');
        $query = $repo->createQueryBuilder('Questionnaire')
            ->where('Questionnaire.feelingStatus IN (:statuses)')
            ->setParameter('statuses', array_values(FeelingStatus::getOldChoices()))
            ->getQuery();
        /** @var PostCompetitiveQuestionnaire $questionnaire */
        foreach ($query->getResult() as $questionnaire) {
            $questionnaire->setFeelingStatus(FeelingStatus::toNewValue($questionnaire->getFeelingStatus()));
        }

        $em->flush();
        $output->writeln('Done!');
    }

}
