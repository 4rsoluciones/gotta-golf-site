<?php

namespace AndresGotta\Bundle\HomepageBundle\Controller\Frontend;

use JMS\SecurityExtraBundle\Annotation\Secure;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Security;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Template;
use Symfony\Component\Security\Core\User\UserInterface;

class HomepageController extends Controller
{
    /**
     * @Route("/", name="frontend_homepage")
     * @Template()
     */
    public function indexAction()
    {
//        $services = $this->getDoctrine()->getRepository('AndresGottaServiceBundle:Service')->findHighlighted(3);
        $lastestNews = $this->getDoctrine()->getRepository('WebFactoryNewsBundle:News')->findLastest(20);
        $highlightedNews = $this->getDoctrine()->getRepository('WebFactoryNewsBundle:News')->findHighlighted(20);

        return compact('lastestNews', 'highlightedNews'/*, 'services'*/);
    }

    /**
     * Listado de tipos de reportes (y reportes) de este tipo de reporte
     *
     * @Route("/player/dashboard", name="frontend_dashboard")
     * @Method("GET")
     * @Template
     * @Security("has_role('ROLE_PLAYER')")
     */
    public function dashboardAction()
    {

        $aa = $this->get('web_factory_user.gotta_center_api')->getServicesStatus($this->getUser()->getEmail(), $this->getUser()->getAccessToken());

        $return = false;
        foreach ($aa as $key => $value) {
            if ($value['service_id'] == 6)
                $return = true;
        }

        $return = true;

        $comparatives = $this->getDoctrine()->getRepository('AndresGottaGolfBundle:Comparative')->findBy(['highlighted' => true], ['sort' => 'desc']);

        return compact('comparatives', 'return');
    }
}
