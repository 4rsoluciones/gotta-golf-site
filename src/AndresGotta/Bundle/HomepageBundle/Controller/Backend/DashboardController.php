<?php

namespace AndresGotta\Bundle\HomepageBundle\Controller\Backend;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Template;

class DashboardController extends Controller
{
    /**
     * @Route("/", name="backend_dashboard")
     * @Template()
     */
    public function indexAction()
    {
        if ($this->getUser()->hasRole('ROLE_ACADEMY')) {
            $usersTypeQuantity = $this->getDoctrine()->getRepository('WebFactoryUserBundle:User')
                ->getUserCountByTypeForAcademy($this->getUser());

            return $this->render('@AndresGottaHomepage/Backend/Dashboard/academy.html.twig',
                compact('usersTypeQuantity'));
        }

        $holeQuantity = $this->getDoctrine()->getRepository('AndresGottaGolfBundle:Hole')
                ->getHoleCount();
        $usersQuantity = $this->getDoctrine()->getRepository('WebFactoryUserBundle:User')
                ->getUserCount();
        $usersTypeQuantity = $this->getDoctrine()->getRepository('WebFactoryUserBundle:User')
                ->getUserCountByType();
        $roundQuantity = $this->getDoctrine()->getRepository('AndresGottaGolfBundle:Round')
                ->getRoundCount();

        return compact('holeQuantity', 'usersQuantity', 'roundQuantity', 'usersTypeQuantity');
    }
}
