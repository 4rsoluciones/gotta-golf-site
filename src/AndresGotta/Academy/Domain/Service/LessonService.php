<?php

namespace AndresGotta\Academy\Domain\Service;

use AndresGotta\Academy\Domain\Entity\AcademyPlace;
use AndresGotta\Academy\Domain\Entity\Lesson;
use AndresGotta\Academy\Domain\Entity\StudentInterface;
use AndresGotta\Academy\Domain\Entity\TeacherInterface;
use AndresGotta\Academy\Domain\Factory\LessonFactory;
use AndresGotta\Academy\Domain\Repository\LessonRepositoryInterface;

class LessonService
{
    /**
     * @var LessonFactory
     */
    private $lessonFactory;

    /**
     * @var LessonRepositoryInterface
     */
    private $lessonRepository;

    /**
     * @param $title
     * @param \DateTime        $date
     * @param AcademyPlace     $place
     * @param TeacherInterface $teacher
     * @param StudentInterface $student
     * @param $resume
     *
     * @return Lesson
     *
     * @todo faltan adjuntar los materiales
     * @todo debería ser encerrado en una transacción
     */
    public function createLesson($title, \DateTime $date, AcademyPlace $place, TeacherInterface $teacher, StudentInterface $student, $resume)
    {
        $lesson = $this->lessonFactory->create($title, $date, $place, $teacher, $student, $resume);

        $this->lessonRepository->addLesson($lesson);

        return $lesson;
    }

    /**
     * @param $title
     * @param \DateTime        $date
     * @param AcademyPlace     $place
     * @param TeacherInterface $teacher
     * @param $students
     * @param $resume
     *
     * @return Lesson[]
     *
     * @todo faltan adjuntar los materiales
     * @todo debería ser encerrado en una transacción
     */
    public function createBulkLessons($title, \DateTime $date, AcademyPlace $place, TeacherInterface $teacher, $students, $resume)
    {
        $lessons = [];
        foreach ($students as $student) {
            $lessons[] = $lesson = $this->lessonFactory->create($title, $date, $place, $teacher, $student, $resume);

            $this->lessonRepository->addLesson($lesson);
        }

        return $lessons;
    }
}
