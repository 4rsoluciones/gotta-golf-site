<?php

namespace AndresGotta\Academy\Domain\Entity;

use AndresGotta\Academy\Domain\ValueObject\Feedback;

interface HasFeedbackInterface
{
    /**
     * @param $value
     * @param $notes
     *
     * @return Feedback
     */
    public function applyFeedback($value, $notes = null);

    /**
     *
     */
    public function clearFeedback();
}
