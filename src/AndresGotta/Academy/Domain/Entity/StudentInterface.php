<?php

namespace AndresGotta\Academy\Domain\Entity;

interface StudentInterface
{
    /**
     * @return TeacherInterface[]
     */
    public function getTrainedByTeachers();

    /**
     * @return AcademyInterface[]
     */
    public function getEnrolledInAcademies();

    /**
     * @param AcademyInterface $academy
     */
    public function addEnrolledInAcademy(AcademyInterface $academy);

    /**
     * @param TeacherInterface $teacher
     */
    public function addTrainedByTeacher(TeacherInterface $teacher);
}
