<?php

namespace AndresGotta\Academy\Domain\Entity;

interface TeacherInterface
{
    /**
     * @return StudentInterface[]
     */
    public function getTeachesToStudents();

    /**
     * @return AcademyInterface[]
     */
    public function getTeachesInAcademies();

}
