<?php

namespace AndresGotta\Academy\Domain\Entity;

use AndresGotta\Academy\Domain\ValueObject\Feedback;

class Report
{
    /**
     * @var int
     */
    private $id;

    /**
     * @var Lesson
     */
    private $lesson;

    /**
     * @var \DateTime
     */
    private $publishedAt;

    /**
     * @var string
     */
    private $resume;

    /**
     * @var Indication[]
     */
    private $indications;

    /**
     * @var Feedback
     */
    private $feedback;

    /**
     * Report constructor.
     * @param Lesson $lesson
     * @param \DateTime $publishedAt
     * @param string $resume
     */
    public function __construct(Lesson $lesson, \DateTime $publishedAt, $resume)
    {
        $this->lesson = $lesson;
        $this->publishedAt = $publishedAt;
        $this->resume = $resume;
        $this->indications = [];
    }

    /**
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @param Indication $indication
     * @todo aplicar UL
     */
    public function addIndication(Indication $indication)
    {
        $indication->useInReport($this);
        $this->indications[] = $indication;
    }

    /**
     * @param Indication $indication
     * @return bool
     */
    public function hasIndication(Indication $indication)
    {
        foreach ($this->indications as $key => $item) {
            if ($indication->getId() === $item->getId()) {
                return true;
            }
        }
        return false;
    }

    public function removeIndication(Indication $indication)
    {
        foreach ($this->indications as $key => $item) {
            if ($this->hasIndication($indication)) {
                $indication->removeReport($this);
                unset($this->indications[$key]);
            }
        }
    }

    /**
     * @param Indication[] $indications
     */
    public function setIndications($indications)
    {
        foreach ($this->indications as $item) {
            $this->removeIndication($item);
        }

        if (empty($indications)) {
            return;
        }

        foreach ($indications as $item) {
            $this->addIndication($item);
        }
    }

    /**
     * @param Feedback|null $feedback
     *
     * @todo aplicar UL
     */
    public function setFeedback(Feedback $feedback = null)
    {
        $this->feedback = $feedback;
    }

    /**
     * @param string $resume
     */
    public function setResume($resume)
    {
        $this->resume = $resume;
    }

    /**
     * @return string
     */
    public function getResume()
    {
        return $this->resume;
    }

    /**
     * @return Indication[]
     */
    public function getIndications()
    {
        return $this->indications;
    }

    /**
     * @return Feedback
     */
    public function getFeedback()
    {
        return $this->feedback;
    }

    /**
     * @return Lesson
     */
    public function getLesson()
    {
        return $this->lesson;
    }

    /**
     * @return \DateTime
     */
    public function getPublishedAt()
    {
        return $this->publishedAt;
    }

}
