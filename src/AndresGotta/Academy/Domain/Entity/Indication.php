<?php

namespace AndresGotta\Academy\Domain\Entity;

use AndresGotta\Academy\Domain\ValueObject\MediaMaterial;

class Indication
{
    /**
     * @var int
     */
    private $id;

    /**
     * @var MediaMaterial
     */
    private $mediaMaterial;

    /**
     * @var string
     */
    private $name;

    /**
     * @var string
     */
    private $description;

    /**
     * @var Goal[]
     */
    private $goals;

    /**
     * @var string
     */
    private $requirements;

    /**
     * @var string
     */
    private $security;

    /**
     * @var string
     */
    private $alternatives;

    /**
     * @var TeacherInterface
     */
    private $teacher;

    /**
     * @var AcademyInterface
     */
    private $academy;

    /**
     * @var Report[]
     */
    private $reports;

    /**
     * Indication constructor.
     *
     * @param MediaMaterial $mediaMaterial
     * @param string $name
     * @param string $description
     * @param Goal[] $goals
     */
    public function __construct(MediaMaterial $mediaMaterial, $name, $description, array $goals)
    {
        $this->name = $name;
        $this->mediaMaterial = $mediaMaterial;
        $this->description = $description;
        $this->goals = $goals;
        $this->reports = [];
    }

    /**
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @return TeacherInterface
     */
    public function getTeacher()
    {
        return $this->teacher;
    }

    /**
     * @param TeacherInterface $teacher
     */
    public function setTeacher($teacher)
    {
        $this->teacher = $teacher;
    }

    /**
     * @param string $requirements
     */
    public function setRequirements($requirements)
    {
        $this->requirements = $requirements;
    }

    /**
     * @param string $security
     */
    public function setSecurity($security)
    {
        $this->security = $security;
    }

    /**
     * @param string $alternatives
     */
    public function setAlternatives($alternatives)
    {
        $this->alternatives = $alternatives;
    }

    /**
     * @return MediaMaterial
     */
    public function getMediaMaterial()
    {
        return $this->mediaMaterial;
    }

    /**
     * @return string
     */
    public function getDescription()
    {
        return $this->description;
    }

    /**
     * @return Goal[]
     */
    public function getGoals()
    {
        return $this->goals;
    }

    /**
     * @return string
     */
    public function getRequirements()
    {
        return $this->requirements;
    }

    /**
     * @return string
     */
    public function getSecurity()
    {
        return $this->security;
    }

    /**
     * @return string
     */
    public function getAlternatives()
    {
        return $this->alternatives;
    }

    /**
     * @param MediaMaterial $mediaMaterial
     */
    public function setMediaMaterial($mediaMaterial)
    {
        $this->mediaMaterial = $mediaMaterial;
    }

    /**
     * @param Report $report
     */
    public function useInReport(Report $report)
    {
        $this->reports[] = $report;
    }

    /**
     * @return string
     */
    public function getName()
    {
        return $this->name;
    }

    /**
     * @param string $name
     */
    public function setName($name)
    {
        $this->name = $name;
    }

    /**
     * @param string $description
     */
    public function setDescription($description)
    {
        $this->description = $description;
    }

    /**
     * @return AcademyInterface
     */
    public function getAcademy()
    {
        return $this->academy;
    }

    /**
     * @param AcademyInterface $academy
     */
    public function setAcademy($academy)
    {
        $this->academy = $academy;
    }

    /**
     * @return bool
     */
    public function hasReports()
    {
        return (boolean)count($this->reports);
    }

    public function removeReport(Report $report)
    {
        foreach ($this->reports as $key => $item) {
            if ($report->getId() === $item->getId()) {
                unset($this->reports[$key]);
            }
        }
    }
}
