<?php

namespace AndresGotta\Academy\Domain\Entity;

use AndresGotta\Academy\Domain\ValueObject\Feedback;

/**
 * Class Routine.
 */
class Routine implements HasFeedbackInterface
{
    /**
     * @var int
     */
    private $id;

    /**
     * @var string
     */
    private $title;

    /**
     * @var string
     */
    private $resume;

    /**
     * @var Step[]
     */
    private $steps;

    /**
     * @var StudentInterface
     */
    private $student;

    /**
     * @var TeacherInterface
     */
    private $teacher;

    /**
     * @var Feedback
     */
    private $feedback;

    /**
     * @var \DateTime
     */
    private $createdAt;

    /**
     * @var AcademyInterface
     */
    private $academy;

    /**
     * Routine constructor.
     *
     * @param $title
     * @param $resume
     * @param TeacherInterface $teacher
     */
    public function __construct($title, $resume, TeacherInterface $teacher)
    {
        $this->title = $title;
        $this->resume = $resume;
        $this->teacher = $teacher;
        $this->steps = [];
        $this->createdAt = new \DateTime();
    }

    /**
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @return string
     */
    public function getTitle()
    {
        return $this->title;
    }

    /**
     * @return string
     */
    public function getResume()
    {
        return $this->resume;
    }

    /**
     * @return StudentInterface
     */
    public function getStudent()
    {
        return $this->student;
    }

    /**
     * @return TeacherInterface
     */
    public function getTeacher()
    {
        return $this->teacher;
    }

    /**
     * @param StudentInterface $student
     */
    public function assignStudent($student)
    {
        $this->student = $student;
    }

    /**
     * @param TeacherInterface $teacher
     */
    public function changeTeacher($teacher)
    {
        $this->teacher = $teacher;
    }

    /**
     * @param $title
     * @param $description
     * @return Step
     */
    public function createStep($title, $description)
    {
        $number = count($this->steps) + 1;
        $step = new Step($this, $number, $title, $description);
        $this->steps[] = $step;

        return $step;
    }

    /**
     * @param int $number
     *
     * @return Step
     */
    public function removeStepByNumber($number)
    {
        $i = 0;
        $found = null;
        $steps = [];
        foreach ($this->steps as $step) {
            if ($step->getNumber() === $number) {
                $found = $step;
            } else {
                $step->setNumber(++$i);
                $steps[] = $step;
            }
        }

        if (!$found) {
            throw new \InvalidArgumentException('Step not found');
        }

        $this->steps = $steps;

        return $found;
    }

    /**
     * @param $number
     */
    public function completeStep($number)
    {
        $step = $this->getStepByNumber($number);
        $step->complete();
    }

    /**
     * @param $number
     *
     * @return Step
     */
    public function getStepByNumber($number)
    {
        $found = null;
        foreach ($this->steps as $step) {
            if ($step->getNumber() === $number) {
                $found = $step;
            }
        }

        if (!$found) {
            throw new \InvalidArgumentException('Step with number not found');
        }

        return $found;
    }

    /**
     * @param $number
     */
    public function clearStep($number)
    {
        $step = $this->getStepByNumber($number);
        $step->clear();
    }

    /**
     * @param $number
     * @return bool
     */
    public function hasStep($number)
    {
        try {
            $this->getStepByNumber($number);
            return true;
        } catch (\InvalidArgumentException $x) {
            return false;
        }
    }

    /**
     * @return Step[]
     */
    public function getSteps()
    {
        return $this->steps;
    }

    /**
     * @param Step $step
     */
    public function addStep(Step $step)
    {
        $this->steps[] = $step;
    }

    /**
     * @param Step $step
     */
    public function removeStep(Step $step)
    {
        $this->removeStepByNumber($step->getNumber());
    }

    /**
     * @param $value
     * @param $notes
     *
     * @return Feedback
     */
    public function applyFeedback($value, $notes = null)
    {
        $this->feedback = new Feedback($value, $notes);

        return $this->feedback;
    }

    /**
     *
     */
    public function clearFeedback()
    {
        $this->feedback = null;
    }

    /**
     * @param $academy
     */
    public function setAcademy($academy)
    {
        $this->academy = $academy;
    }

    /**
     * @return bool
     */
    public function isStepComplete()
    {
        foreach ($this->steps as $step) {
            if (!$step->isCompleted()) {
                return false;
            }
        }

        return true;
    }

    /**
     * @param string $title
     */
    public function setTitle($title)
    {
        $this->title = $title;
    }

    /**
     * @param string $resume
     */
    public function setResume($resume)
    {
        $this->resume = $resume;
    }

    /**
     * @param Step[] $steps
     */
    public function setSteps($steps)
    {
        $this->steps = $steps;
    }

    /**
     * @param StudentInterface $student
     */
    public function setStudent($student)
    {
        $this->student = $student;
    }

    /**
     * @param TeacherInterface $teacher
     */
    public function setTeacher($teacher)
    {
        $this->teacher = $teacher;
    }

    /**
     * @param Feedback $feedback
     */
    public function setFeedback($feedback)
    {
        $this->feedback = $feedback;
    }

    /**
     * @param \DateTime $createdAt
     */
    public function setCreatedAt($createdAt)
    {
        $this->createdAt = $createdAt;
    }


}
