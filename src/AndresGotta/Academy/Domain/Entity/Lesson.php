<?php

namespace AndresGotta\Academy\Domain\Entity;

use AndresGotta\Academy\Domain\ValueObject\Feedback;
use APY\DataGridBundle\Grid\Mapping as Grid;

/**
 * Class Lesson
 *
 * @Grid\Source(columns="id, place, teacher, student, title, date")
 */
class Lesson implements HasFeedbackInterface
{
    /**
     * @var int
     *
     * @Grid\Column(title="ID")
     */
    private $id;

    /**
     * @var string
     * @Grid\Column(title="Report")
     */
    private $title;

    /**
     * @var \DateTime
     * @Grid\Column(title="Date")
     */
    private $date;

    /**
     * @var AcademyPlace
     *
     * @Grid\Column(title="Academy")
     */
    private $place;

    /**
     * @var TeacherInterface
     *
     * @Grid\Column(title="Teacher")
     */
    private $teacher;

    /**
     * @var StudentInterface
     *
     * @Grid\Column(title="Student")
     */
    private $student;

    /**
     * @var Report
     */
    private $report;

    /**
     * Lesson constructor.
     * @param string $title
     * @param \DateTime $date
     * @param AcademyPlace $place
     * @param TeacherInterface $teacher
     * @param StudentInterface $student
     * @param string $resume
     */
    public function __construct($title, \DateTime $date, AcademyPlace $place, TeacherInterface $teacher, StudentInterface $student, $resume)
    {
        $this->title = $title;
        $this->date = $date;
        $this->place = $place;
        $this->teacher = $teacher;
        $this->student = $student;
        $this->makeReport($resume);
    }

    /**
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @return string
     */
    public function getTitle()
    {
        return $this->title;
    }

    /**
     * @return \DateTime
     */
    public function getDate()
    {
        return $this->date;
    }

    /**
     * @return AcademyPlace
     */
    public function getPlace()
    {
        return $this->place;
    }

    /**
     * @return TeacherInterface
     */
    public function getTeacher()
    {
        return $this->teacher;
    }

    /**
     * @param $resume
     * @return Report
     */
    private function makeReport($resume)
    {
        $this->report = new Report($this, new \DateTime(), $resume);

        return $this->report;
    }

    /**
     * @return Report
     */
    public function getReport()
    {
        return $this->report;
    }

    /**
     * @return StudentInterface
     */
    public function getStudent()
    {
        return $this->student;
    }

    /**
     * {@inheritdoc}
     */
    public function applyFeedback($value, $notes = null)
    {
        $feedback = new Feedback($value, $notes);
        $this->report->setFeedback($feedback);

        return $feedback;
    }

    /**
     * {@inheritdoc}
     */
    public function clearFeedback()
    {
        $this->report->setFeedback(null);
    }

    /**
     * @param $indication
     */
    public function linkReportToIndication($indication)
    {
        $this->report->addIndication($indication);
    }

    /**
     * @param string $title
     */
    public function setTitle($title)
    {
        $this->title = $title;
    }

    /**
     * @param \DateTime $date
     */
    public function setDate(\Datetime $date)
    {
        $this->date = $date;
    }

    /**
     * @param AcademyPlace $place
     */
    public function setPlace($place)
    {
        $this->place = $place;
    }
}
