<?php

namespace AndresGotta\Academy\Domain\Entity;

class AcademyPlace
{
    /**
     * @var int
     */
    private $id;

    /**
     * @var AcademyInterface
     */
    private $academy;

    /**
     * @var string
     */
    private $name;

    /**
     * @var string
     */
    private $address;

    /**
     * AcademyPlace constructor.
     * @param AcademyInterface $academy
     * @param string $name
     * @param string $address
     */
    public function __construct(AcademyInterface $academy, $name, $address = null)
    {
        $this->academy = $academy;
        $this->name = $name ? $name : $academy->getUsername();
        $this->address = $address;
    }

    /**
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @return AcademyInterface
     */
    public function getAcademy()
    {
        return $this->academy;
    }

    /**
     * @return string
     */
    public function getName()
    {
        return $this->name;
    }

    /**
     * @return string
     */
    public function getAddress()
    {
        return $this->address;
    }

    /**
     * @param string $name
     */
    public function setName($name)
    {
        $this->name = $name;
    }

    /**
     * @param string $address
     */
    public function setAddress($address)
    {
        $this->address = $address;
    }
}
