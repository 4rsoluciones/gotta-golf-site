<?php

namespace AndresGotta\Academy\Domain\Entity;

class Step
{
    /**
     * @var int
     */
    private $id;

    /**
     * @var Routine
     */
    private $routine;

    /**
     * @var int
     */
    private $number;

    /**
     * @var string
     */
    private $title;

    /**
     * @var string
     */
    private $description;

    /**
     * @var bool
     */
    private $completed;

    /**
     * @var \DateTime
     */
    private $completedAt;

    /**
     * @var string
     */
    private $notes;

    /**
     * Step constructor.
     *
     * @param Routine $routine
     * @param int $number
     * @param $title
     * @param string $description
     */
    public function __construct(Routine $routine, $number, $title, $description = null)
    {
        $this->routine = $routine;
        $this->number = $number;
        $this->title = $title;
        $this->description = $description;
        $this->completed = false;
        $this->completedAt = null;
        $this->notes = null;
    }

    /**
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @return Routine
     */
    public function getRoutine()
    {
        return $this->routine;
    }

    /**
     * @return int
     */
    public function getNumber()
    {
        return $this->number;
    }

    /**
     * @return string
     */
    public function getTitle()
    {
        return $this->title;
    }

    /**
     * @return string
     */
    public function getDescription()
    {
        return $this->description;
    }

    /**
     * @return bool
     */
    public function isCompleted()
    {
        return $this->completed;
    }

    /**
     * @return \DateTime
     */
    public function getCompletedAt()
    {
        return $this->completedAt;
    }

    /**
     * @return string
     */
    public function getNotes()
    {
        return $this->notes;
    }

    public function complete()
    {
        $this->completed = true;
        $this->completedAt = new \DateTime();
    }

    public function clear()
    {
        $this->completed = false;
        $this->completedAt = null;
    }

    /**
     * @param int $number
     */
    public function setNumber($number)
    {
        $this->number = $number;
    }

    /**
     * @param string $description
     */
    public function setDescription($description)
    {
        $this->description = $description;
    }

    /**
     * @param string $notes
     */
    public function setNotes($notes)
    {
        $this->notes = $notes;
    }
}
