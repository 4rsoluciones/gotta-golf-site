<?php

namespace AndresGotta\Academy\Domain\Entity;

use AndresGotta\Academy\Domain\ValueObject\Sport;
use WebFactory\Bundle\UserBundle\Entity\AcademyLogotype;

interface AcademyInterface
{
    /**
     * @return TeacherInterface[]
     */
    public function getTeachers();

    /**
     * @return StudentInterface[]
     */
    public function getStudents();

    /**
     * @return string
     */
    public function getName();

    /**
     * @return AcademyLogotype
     */
    public function getLogotype();

    /**
     * @return integer
     */
    public function getMaxTeachers();

    /**
     * @return integer
     */
    public function getMaxStudents();

    /**
     * @return AcademyPlace[]
     */
    public function getPlaces();

    /**
     * @return Sport
     */
    public function getSport();

}
