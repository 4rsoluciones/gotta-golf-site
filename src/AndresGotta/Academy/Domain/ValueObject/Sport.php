<?php

namespace AndresGotta\Academy\Domain\ValueObject;

/**
 * Class Sport.
 */
class Sport
{
    /**
     * @var string
     */
    private $name;

    /**
     * @var string
     */
    private $icon;

    /**
     * Sport constructor.
     *
     * @param $name
     */
    public function __construct($name)
    {
        if (!$name) {
            throw new \InvalidArgumentException('Invalid sport name');
        }
        $this->name = $name;
        $this->icon = str_replace(' áéíóúñ', '_', strtolower($name)).'.png';
    }

    /**
     * @return string
     */
    public function getName()
    {
        return $this->name;
    }

    /**
     * @return string
     */
    public function getIcon()
    {
        return $this->icon;
    }
}
