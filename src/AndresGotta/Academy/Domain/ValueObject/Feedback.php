<?php

namespace AndresGotta\Academy\Domain\ValueObject;

class Feedback
{
    /**
     * @var int
     */
    private $value;

    /**
     * @var \DateTime
     */
    private $createdAt;

    /**
     * @var string
     */
    private $notes;

    /**
     * Feedback constructor.
     *
     * @param int    $value
     * @param string $notes
     */
    public function __construct($value, $notes = null)
    {
        $this->value = $value;
        $this->notes = $notes;
        $this->createdAt = new \DateTime();
    }

    /**
     * @return int
     */
    public function getValue()
    {
        return $this->value;
    }

    /**
     * @return \DateTime
     */
    public function getCreatedAt()
    {
        return $this->createdAt;
    }

    /**
     * @return string
     */
    public function getNotes()
    {
        return $this->notes;
    }
}
