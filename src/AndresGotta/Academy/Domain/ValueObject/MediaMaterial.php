<?php

namespace AndresGotta\Academy\Domain\ValueObject;

class MediaMaterial
{
    const TYPE_VIDEO = 'type.video';
    const TYPE_IMAGE = 'type.image';
    const TYPE_AUDIO = 'type.audio';

    /**
     * @var string
     */
    private $uri;

    /**
     * @var string
     */
    private $type;


    /**
     * MediaMaterial constructor.
     *
     * @param string           $uri
     * @param string           $type
     */
    public function __construct($uri, $type)
    {
        $this->uri = $uri;
        $this->type = $type;
    }

    /**
     * @return string
     */
    public function getUri()
    {
        return $this->uri;
    }

    /**
     * @return string
     */
    public function getType()
    {
        return $this->type;
    }
}
