<?php

namespace AndresGotta\Academy\Domain\Factory;

use FOS\UserBundle\Model\UserManagerInterface;
use WebFactory\Bundle\LocationBundle\Repository\State as StateRepository;
use WebFactory\Bundle\UserBundle\Entity\Profile;
use WebFactory\Bundle\UserBundle\Entity\User;

class StudentFactory
{
    /**
     * @var UserManagerInterface
     */
    private $userManager;

    /**
     * @var StateRepository
     */
    private $stateRepository;

    /**
     * StudentFactory constructor.
     * @param UserManagerInterface $userManager
     * @param StateRepository $stateRepository
     */
    public function __construct(UserManagerInterface $userManager, StateRepository $stateRepository)
    {
        $this->userManager = $userManager;
        $this->stateRepository = $stateRepository;
    }

    /**
     * @param $email
     * @param string|null $firstName
     * @param string|null $lastName
     * @return User
     */
    public function fromEmail($email, $firstName = null, $lastName = null)
    {
        /** @var User $user */
        $user = $this->userManager->createUser();
        $user->addRole('ROLE_STUDENT');
        $user->setEmail($email);
        $user->setUsername($email);
        $user->setPlainPassword(User::generatePassword());
        $user->setEnabled(true);
        $profile = new Profile();
        $profile->setFirstName($firstName);
        $profile->setLastName($lastName);
        $profile->setBirthDate(new \DateTime('-18 year'));
        $profile->setGender('male');
        $profile->setHandicap(0);
        $profile->setCondition('player.amateur');
        $state = $this->stateRepository->findOneByName('Santa Fe');
        $profile->setState($state);
        $user->setProfile($profile);

        return $user;
    }
}