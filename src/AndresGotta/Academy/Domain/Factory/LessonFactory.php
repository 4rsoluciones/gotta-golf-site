<?php

namespace AndresGotta\Academy\Domain\Factory;

use AndresGotta\Academy\Domain\Entity\AcademyPlace;
use AndresGotta\Academy\Domain\Entity\Lesson;
use AndresGotta\Academy\Domain\Entity\StudentInterface;
use AndresGotta\Academy\Domain\Entity\TeacherInterface;

class LessonFactory
{
    /**
     * @param string $title
     * @param \DateTime $date
     * @param AcademyPlace $place
     * @param TeacherInterface $teacher
     * @param StudentInterface $student
     * @param string $resume
     * @return Lesson
     */
    public function create($title, \DateTime $date, AcademyPlace $place, TeacherInterface $teacher, StudentInterface $student, $resume)
    {
        $lesson = new Lesson($title, $date, $place, $teacher, $student, $resume);

        return $lesson;
    }
}
