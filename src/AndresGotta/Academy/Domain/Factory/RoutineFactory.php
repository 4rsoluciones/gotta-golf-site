<?php

namespace AndresGotta\Academy\Domain\Factory;

use AndresGotta\Academy\Domain\Entity\Routine;
use AndresGotta\Academy\Domain\Entity\TeacherInterface;

class RoutineFactory
{
    /**
     * @param $title
     * @param $resume
     * @param TeacherInterface $teacher
     * @return Routine
     */
    public function create($title, $resume, TeacherInterface $teacher)
    {
        return new Routine($title, $resume, $teacher);
    }
}
