<?php

namespace AndresGotta\Academy\Domain\Repository;

use AndresGotta\Academy\Domain\Entity\AcademyInterface;
use AndresGotta\Academy\Domain\Entity\Goal;
use AndresGotta\Academy\Domain\Entity\StudentInterface;
use AndresGotta\Academy\Domain\Entity\TeacherInterface;

interface GoalRepositoryInterface
{
    /**
     * @param string $query
     * @param TeacherInterface $teacher
     * @param AcademyInterface $academy
     * @param int $offset
     * @param int $limit
     * @return Goal[]
     */
    public function findAllOrderedByMostUsedBy($query, TeacherInterface $teacher, AcademyInterface $academy, $offset = 0, $limit = 20);

    /**
     * @param string $query
     * @param StudentInterface $student
     * @param AcademyInterface $academy
     * @param int $offset
     * @param int $limit
     * @return Goal[]
     */
    public function findAllOrderedByMostUsedByStudent($query, StudentInterface $student, AcademyInterface $academy, $offset = 0, $limit = 20);

    /**
     * @param Goal $goal
     */
    public function addGoal(Goal $goal);

    /**
     * @param $name
     * @return Goal
     */
    public function findOneByName($name);
}