<?php

namespace AndresGotta\Academy\Domain\Repository;

use AndresGotta\Academy\Domain\Entity\AcademyInterface;
use AndresGotta\Academy\Domain\Entity\Indication;
use AndresGotta\Academy\Domain\Entity\StudentInterface;
use AndresGotta\Academy\Domain\Entity\TeacherInterface;

interface IndicationRepositoryInterface
{
    /**
     * @param string $type
     * @param StudentInterface $student
     * @param AcademyInterface $academy
     * @param array $goals
     * @param string $query
     * @param int $offset
     * @param int $limit
     * @return Indication[]
     */
    public function findAllByStudent($type, StudentInterface $student, AcademyInterface $academy, $goals = [], $query = '', $offset = 0, $limit = 20);

    /**
     * @param string $type
     * @param TeacherInterface $teacher
     * @param AcademyInterface $academy
     * @param array $goals
     * @param string $query
     * @param int $offset
     * @param int $limit
     * @return Indication[]
     */
    public function findAllByTeacher($type, TeacherInterface $teacher, AcademyInterface $academy, $goals = [], $query = '', $offset = 0, $limit = 20);

    /**
     * @param Indication $indication
     * @return
     */
    public function addIndication(Indication $indication);

    /**
     * @param Indication $indication
     * @return
     */
    public function editIndication(Indication $indication);

    /**
     * @param integer $id
     */
    public function find($id);
}
