<?php

namespace AndresGotta\Academy\Domain\Repository;

use AndresGotta\Academy\Domain\Entity\AcademyInterface;
use AndresGotta\Academy\Domain\Entity\Routine;
use AndresGotta\Academy\Domain\Entity\StudentInterface;
use AndresGotta\Academy\Domain\Entity\TeacherInterface;

interface RoutineRepositoryInterface
{
    /**
     * @param StudentInterface $student
     * @param AcademyInterface $academy
     * @param int $offset
     * @param int $limit
     * @return Routine[]
     */
    public function findAllByStudent(StudentInterface $student, AcademyInterface $academy, $offset = 0, $limit = 20);

    /**
     * @param TeacherInterface $teacher
     * @param AcademyInterface $academy
     * @param int $offset
     * @param int $limit
     * @param string $search
     * @return Routine[]
     */
    public function findAllByTeacher(TeacherInterface $teacher, AcademyInterface $academy, $offset = 0, $limit = 20, $search = '');

    /**
     * @param Routine $routine
     */
    public function addRoutine(Routine $routine);
}
