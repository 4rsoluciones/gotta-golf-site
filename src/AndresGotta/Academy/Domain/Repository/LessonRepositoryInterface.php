<?php

namespace AndresGotta\Academy\Domain\Repository;

use AndresGotta\Academy\Domain\Entity\AcademyInterface;
use AndresGotta\Academy\Domain\Entity\Lesson;
use AndresGotta\Academy\Domain\Entity\StudentInterface;
use AndresGotta\Academy\Domain\Entity\TeacherInterface;

interface LessonRepositoryInterface
{
    /**
     * @param StudentInterface $student
     * @param AcademyInterface $academy
     * @param int $offset
     * @param int $limit
     * @return Lesson[]
     */
    public function findAllByStudent(StudentInterface $student, AcademyInterface $academy, $offset = 0, $limit = 20);

    /**
     * @param TeacherInterface $teacher
     * @param AcademyInterface $academy
     * @param int $offset
     * @param int $limit
     * @return Lesson[]
     */
    public function findAllByTeacher(TeacherInterface $teacher, AcademyInterface $academy, $offset = 0, $limit = 20);

    /**
     * @param Lesson $lesson
     */
    public function addLesson(Lesson $lesson);

    /**
     * @param Lesson $lesson
     */
    public function editLesson(Lesson $lesson);
}
