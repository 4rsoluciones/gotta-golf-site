<?php

namespace AndresGotta\Academy\Tests\Application\Command;

use AndresGotta\Academy\Application\Command\AddRoutineCommand;
use AndresGotta\Academy\Application\Command\DataTransferObject\RoutineStep;
use AndresGotta\Academy\Domain\Entity\Routine;
use Symfony\Bundle\FrameworkBundle\Test\WebTestCase;

class AddRoutineHandlerTest extends WebTestCase
{
    public function testHandle()
    {
        $container = $this->createClient()->getKernel()->getContainer();
        $teacher = $container->get('fos_user.user_manager')
            ->findUserBy(['username' => 'teacher']);
        $student = $container->get('fos_user.user_manager')
            ->findUserBy(['username' => 'student']);
        $academy = $container->get('fos_user.user_manager')
            ->findUserBy(['username' => 'academy']);
        $command = new AddRoutineCommand('Title', 'Resume', $teacher, [$student, $student], [
            new RoutineStep('title 1', 'description 1'),
            new RoutineStep('title 2'),
        ], $academy);

        $routines = $container->get('tactician.commandbus')->handle($command);
        $this->assertInternalType('array', $routines);
        $this->assertCount(2, $routines);

        $routine = current($routines);
        $this->assertInstanceOf(Routine::class, $routine);
        $this->assertEquals('title 1', $routine->getStepByNumber(1)->getTitle());
        $this->assertNull($routine->getStepByNumber(2)->getDescription());
    }
}
