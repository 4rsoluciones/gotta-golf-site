<?php

namespace AndresGotta\Academy\Application;

use AndresGotta\Academy\Domain\Entity\AcademyInterface;
use AndresGotta\Academy\Domain\ValueObject\Sport;

class AcademyContext
{
    /**
     * @var AcademyInterface
     */
    private $academy;

    /**
     * @var Sport
     */
    private $sport;

    /**
     * AcademyContext constructor.
     * @param AcademyInterface $academy
     * @param Sport $sport
     */
    public function __construct(AcademyInterface $academy, Sport $sport)
    {
        $this->academy = $academy;
        $this->sport = $sport;
    }

    /**
     * @return AcademyInterface
     */
    public function getAcademy()
    {
        return $this->academy;
    }

    /**
     * @return Sport
     */
    public function getSport()
    {
        return $this->sport;
    }

}