<?php

namespace AndresGotta\Academy\Application\Command;

use AndresGotta\Academy\Domain\Entity\Routine;

final class SetRoutineFeedbackCommand
{
    /**
     * @var Routine
     */
    private $routine;

    /**
     * @var int
     */
    private $value;

    /**
     * @var string
     */
    private $notes;

    /**
     * SetRoutineFeedbackCommand constructor.
     *
     * @param Routine $routine
     * @param int     $value
     * @param string  $notes
     */
    public function __construct(Routine $routine = null, $value = null, $notes = null)
    {
        $this->routine = $routine;
        $this->value = $value;
        $this->notes = $notes;
    }

    /**
     * @return Routine
     */
    public function getRoutine()
    {
        return $this->routine;
    }

    /**
     * @return int
     */
    public function getValue()
    {
        return $this->value;
    }

    /**
     * @return string
     */
    public function getNotes()
    {
        return $this->notes;
    }
}
