<?php

namespace AndresGotta\Academy\Application\Command;

use AndresGotta\Academy\Domain\Entity\Routine;

final class SetRoutineStepStatusCommand
{
    /**
     * @var Routine
     */
    private $routine;

    /**
     * @var integer
     */
    private $step;

    /**
     * @var boolean
     */
    private $completed;

    /**
     * SetRoutineStepStatusCommand constructor.
     * @param Routine $routine
     * @param int $step
     * @param bool $completed
     */
    public function __construct(Routine $routine = null, $step = null, $completed = null)
    {
        $this->routine = $routine;
        $this->step = $step;
        $this->completed = $completed;
    }

    /**
     * @return Routine
     */
    public function getRoutine()
    {
        return $this->routine;
    }

    /**
     * @return int
     */
    public function getStep()
    {
        return $this->step;
    }

    /**
     * @return bool
     */
    public function isCompleted()
    {
        return $this->completed;
    }

}
