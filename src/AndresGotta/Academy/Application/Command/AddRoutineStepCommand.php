<?php

namespace AndresGotta\Academy\Application\Command;

final class AddRoutineStepCommand
{
    private $routine;

    private $description;

    /**
     * AddRoutineStepCommand constructor.
     *
     * @param $routine
     * @param $description
     */
    public function __construct($routine, $description)
    {
        $this->routine = $routine;
        $this->description = $description;
    }

    /**
     * @return mixed
     */
    public function getRoutine()
    {
        return $this->routine;
    }

    /**
     * @return mixed
     */
    public function getDescription()
    {
        return $this->description;
    }
}
