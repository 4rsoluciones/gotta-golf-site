<?php

namespace AndresGotta\Academy\Application\Command;

use AndresGotta\Academy\Application\Command\DataTransferObject\RoutineStep;
use AndresGotta\Academy\Domain\Entity\AcademyInterface;
use AndresGotta\Academy\Domain\Entity\StudentInterface;
use AndresGotta\Academy\Domain\Entity\TeacherInterface;

final class AddRoutineCommand
{
    /**
     * @var string
     */
    private $title;

    /**
     * @var string
     */
    private $resume;

    /**
     * @var TeacherInterface
     */
    private $teacher;

    /**
     * @var StudentInterface[]
     */
    private $students;

    /**
     * @var RoutineStep[]
     */
    private $steps;

    /**
     * @var AcademyInterface
     */
    private $academy;

    /**
     * AddRoutineCommand constructor.
     * @param string $title
     * @param string $resume
     * @param TeacherInterface $teacher
     * @param StudentInterface[] $students
     * @param array $steps
     */
    public function __construct($title, $resume, TeacherInterface $teacher,
                                array $students, array $steps, AcademyInterface $academy)
    {
        $this->title = $title;
        $this->resume = $resume;
        $this->teacher = $teacher;
        $this->students = $students;
        $this->steps = $steps;
        $this->academy = $academy;
    }

    /**
     * @return string
     */
    public function getTitle()
    {
        return $this->title;
    }

    /**
     * @return string
     */
    public function getResume()
    {
        return $this->resume;
    }

    /**
     * @return TeacherInterface
     */
    public function getTeacher()
    {
        return $this->teacher;
    }

    /**
     * @return StudentInterface[]
     */
    public function getStudents()
    {
        return $this->students;
    }

    /**
     * @return RoutineStep[]
     */
    public function getSteps()
    {
        return $this->steps;
    }

    /**
     * @return AcademyInterface
     */
    public function getAcademy()
    {
        return $this->academy;
    }

}
