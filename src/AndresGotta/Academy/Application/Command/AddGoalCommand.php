<?php

namespace AndresGotta\Academy\Application\Command;

final class AddGoalCommand
{
    /**
     * @var string
     */
    private $name;

    /**
     * AddGoalCommand constructor.
     * @param string $name
     */
    public function __construct($name)
    {
        $this->name = $name;
    }

    /**
     * @return string
     */
    public function getName()
    {
        return $this->name;
    }

}
