<?php

namespace AndresGotta\Academy\Application\Command;

use AndresGotta\Academy\Domain\Entity\Lesson;

final class SetLessonFeedbackCommand
{
    /**
     * @var Lesson
     */
    private $lesson;

    /**
     * @var int
     */
    private $value;

    /**
     * @var string
     */
    private $notes;

    /**
     * SetLessonFeedbackCommand constructor.
     *
     * @param Lesson $lesson
     * @param int    $value
     * @param string $notes
     */
    public function __construct(Lesson $lesson = null, $value = null, $notes = null)
    {
        $this->lesson = $lesson;
        $this->value = $value;
        $this->notes = $notes;
    }

    /**
     * @return Lesson
     */
    public function getLesson()
    {
        return $this->lesson;
    }

    /**
     * @return int
     */
    public function getValue()
    {
        return $this->value;
    }

    /**
     * @return string
     */
    public function getNotes()
    {
        return $this->notes;
    }
}
