<?php

namespace AndresGotta\Academy\Application\Command;

use WebFactory\Bundle\UserBundle\Entity\User;

class RequestRecoverAccountCommand
{
    /**
     * @var User
     */
    private $user;

    /**
     * UserCommand constructor.
     * @param $user
     */
    public function __construct(User $user = null)
    {
        $this->user = $user;
    }

    /**
     * @return User
     */
    public function getUser()
    {
        return $this->user;
    }

}