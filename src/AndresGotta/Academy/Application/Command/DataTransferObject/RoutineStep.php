<?php

namespace AndresGotta\Academy\Application\Command\DataTransferObject;

final class RoutineStep
{
    /**
     * @var string
     */
    private $title;

    /**
     * @var string
     */
    private $description;

    /**
     * RoutineStep constructor.
     * @param $title
     * @param string $description
     */
    public function __construct($title, $description = null)
    {
        $this->title = $title;
        $this->description = $description;
    }

    /**
     * @return string
     */
    public function getTitle()
    {
        return $this->title;
    }

    /**
     * @return string
     */
    public function getDescription()
    {
        return $this->description;
    }
}
