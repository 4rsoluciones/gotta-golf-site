<?php

namespace AndresGotta\Academy\Application\Command;

use AndresGotta\Academy\Domain\Entity\AcademyPlace;
use AndresGotta\Academy\Domain\Entity\StudentInterface;
use AndresGotta\Academy\Domain\Entity\TeacherInterface;

final class AddLessonCommand
{
    /**
     * @var string
     */
    private $title;
    /**
     * @var \DateTime
     */
    private $date;
    /**
     * @var AcademyPlace
     */
    private $place;
    /**
     * @var TeacherInterface
     */
    private $teacher;
    /**
     * @var StudentInterface[]
     */
    private $students;
    /**
     * @var integer[]
     */
    private $indications;

    /**
     * @var string
     */
    private $resume;

    /**
     * AddLessonCommand constructor.
     *
     * @param $title
     * @param \DateTime $date
     * @param AcademyPlace $place
     * @param TeacherInterface $teacher
     * @param StudentInterface[] $students
     * @param null $resume
     * @param integer[] $indications
     */
    public function __construct($title = null, \DateTime $date = null, AcademyPlace $place = null, TeacherInterface $teacher = null, $students = null, $resume = null, $indications = [])
    {
        $this->title = $title;
        $this->date = $date;
        $this->place = $place;
        $this->teacher = $teacher;
        $this->students = $students;
        $this->resume = $resume;
        $this->indications = $indications;
    }

    /**
     * @return string
     */
    public function getTitle()
    {
        return $this->title;
    }

    /**
     * @return \DateTime
     */
    public function getDate()
    {
        return $this->date;
    }

    /**
     * @return AcademyPlace
     */
    public function getPlace()
    {
        return $this->place;
    }

    /**
     * @return TeacherInterface
     */
    public function getTeacher()
    {
        return $this->teacher;
    }

    /**
     * @return StudentInterface[]
     */
    public function getStudents()
    {
        return $this->students;
    }

    /**
     * @return string
     */
    public function getResume()
    {
        return $this->resume;
    }

    /**
     * @return integer[]
     */
    public function getIndications()
    {
        return $this->indications;
    }

}
