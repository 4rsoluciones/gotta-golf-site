<?php

namespace AndresGotta\Academy\Application\Command;

use AndresGotta\Academy\Domain\Entity\AcademyInterface;
use AndresGotta\Academy\Domain\Entity\Goal;
use AndresGotta\Academy\Domain\Entity\TeacherInterface;
use AndresGotta\Academy\Domain\ValueObject\MediaMaterial;

final class AddIndicationCommand
{
    /**
     * @var MediaMaterial
     */
    private $mediaMaterial;

    /**
     * @var string
     */
    private $description;

    /**
     * @var Goal[]
     */
    private $goals;
    /**
     * @var string
     */
    private $requirements;
    /**
     * @var string
     */
    private $security;
    /**
     * @var string
     */
    private $alternatives;

    /**
     * @var TeacherInterface
     */
    private $teacher;
    /**
     * @var string
     */
    private $name;
    /**
     * @var AcademyInterface
     */
    private $academy;

    /**
     * AddIndicationCommand constructor.
     *
     * @param TeacherInterface $teacher
     * @param MediaMaterial $mediaMaterial
     * @param string $name
     * @param string $description
     * @param Goal[] $goals
     * @param string $requirements
     * @param string $security
     * @param string $alternatives
     */
    public function __construct(TeacherInterface $teacher, MediaMaterial $mediaMaterial = null, $name = '', $description = '', array $goals = [], $requirements = null, $security = null, $alternatives = null)
    {
        $this->name = $name;
        $this->mediaMaterial = $mediaMaterial;
        $this->description = $description;
        $this->goals = $goals;
        $this->requirements = $requirements;
        $this->security = $security;
        $this->alternatives = $alternatives;
        $this->teacher = $teacher;
    }

    /**
     * @return MediaMaterial
     */
    public function getMediaMaterial()
    {
        return $this->mediaMaterial;
    }

    /**
     * @return string
     */
    public function getDescription()
    {
        return $this->description;
    }

    /**
     * @return Goal[]
     */
    public function getGoals()
    {
        return $this->goals;
    }

    /**
     * @return string
     */
    public function getRequirements()
    {
        return $this->requirements;
    }

    /**
     * @return string
     */
    public function getSecurity()
    {
        return $this->security;
    }

    /**
     * @return string
     */
    public function getAlternatives()
    {
        return $this->alternatives;
    }

    /**
     * @return TeacherInterface
     */
    public function getTeacher()
    {
        return $this->teacher;
    }

    public function getName()
    {
        return $this->name;
    }

    /**
     * @return AcademyInterface
     */
    public function getAcademy()
    {
        return $this->academy;
    }

    /**
     * @param AcademyInterface $academy
     */
    public function setAcademy($academy)
    {
        $this->academy = $academy;
    }

}
