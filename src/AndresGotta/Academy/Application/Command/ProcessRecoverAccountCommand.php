<?php

namespace AndresGotta\Academy\Application\Command;

final class ProcessRecoverAccountCommand
{
    /**
     * @var string
     */
    private $confirmationToken;

    /**
     * @var string
     */
    private $plainPassword;

    /**
     * ProcessRecoverAccountCommand constructor.
     * @param string $confirmationToken
     * @param string $plainPassword
     */
    public function __construct($confirmationToken = null, $plainPassword = null)
    {
        $this->confirmationToken = $confirmationToken;
        $this->plainPassword = $plainPassword;
    }

    /**
     * @return string
     */
    public function getConfirmationToken()
    {
        return $this->confirmationToken;
    }

    /**
     * @return string
     */
    public function getPlainPassword()
    {
        return $this->plainPassword;
    }

}
