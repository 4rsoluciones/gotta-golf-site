<?php

namespace AndresGotta\Academy\Application\Handler;

use AndresGotta\Academy\Application\Command\AddGoalCommand;
use AndresGotta\Academy\Domain\Entity\Goal;
use AndresGotta\Academy\Domain\Repository\GoalRepositoryInterface;

final class AddGoalHandler
{
    /**
     * @var GoalRepositoryInterface
     */
    private $goalRepository;

    /**
     * AddGoalHandler constructor.
     * @param GoalRepositoryInterface $goalRepository
     */
    public function __construct(GoalRepositoryInterface $goalRepository)
    {
        $this->goalRepository = $goalRepository;
    }

    /**
     * @param AddGoalCommand $command
     * @return Goal
     */
    public function handle(AddGoalCommand $command)
    {
        $goal = $this->goalRepository->findOneByName($command->getName());
        if (!$goal) {
            $goal = new Goal($command->getName());
            $this->goalRepository->addGoal($goal);
        }

        return $goal;
    }
}
