<?php

namespace AndresGotta\Academy\Application\Handler;

use AndresGotta\Academy\Application\Command\RequestRecoverAccountCommand;
use FOS\UserBundle\Util\TokenGeneratorInterface;
use WebFactory\Bundle\UserBundle\Mailer\Mailer;
use WebFactory\Bundle\UserBundle\Model\UserManager;

class RequestRecoverAccountHandler
{
    /**
     * @var TokenGeneratorInterface
     */
    private $tokenGenerator;

    /**
     * @var UserManager
     */
    private $userManager;
    /**
     * @var Mailer
     */
    private $userMailer;

    /**
     * RequestRecoverAccountHandler constructor.
     * @param TokenGeneratorInterface $tokenGenerator
     * @param UserManager $userManager
     * @param Mailer $userMailer
     */
    public function __construct(TokenGeneratorInterface $tokenGenerator, UserManager $userManager, Mailer $userMailer)
    {
        $this->tokenGenerator = $tokenGenerator;
        $this->userManager = $userManager;
        $this->userMailer = $userMailer;
    }


    /**
     * @param RequestRecoverAccountCommand $command
     */
    public function handle(RequestRecoverAccountCommand $command)
    {
        $user = $command->getUser();
        $user->setConfirmationToken($this->tokenGenerator->generateToken());
        $this->userManager->updateUser($user);
        $this->userMailer->sendResettingEmailMessage($user);
    }

}