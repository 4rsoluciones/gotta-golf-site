<?php

namespace AndresGotta\Academy\Application\Handler;

use AndresGotta\Academy\Application\Command\SetRoutineFeedbackCommand;
use AndresGotta\Academy\Domain\Entity\Routine;
use AndresGotta\Academy\Domain\Repository\RoutineRepositoryInterface;

final class SetRoutineFeedbackHandler
{
    /**
     * @var RoutineRepositoryInterface
     */
    private $repository;

    /**
     * AddRoutineStepHandler constructor.
     *
     * @param RoutineRepositoryInterface $repository
     */
    public function __construct(RoutineRepositoryInterface $repository)
    {
        $this->repository = $repository;
    }

    /**
     * @param SetRoutineFeedbackCommand $command
     */
    public function handle(SetRoutineFeedbackCommand $command)
    {
        /* @var Routine $routine */
        $routine = $command->getRoutine();
        $routine->applyFeedback($command->getValue(), $command->getNotes());

        $this->repository->addRoutine($routine);
    }
}
