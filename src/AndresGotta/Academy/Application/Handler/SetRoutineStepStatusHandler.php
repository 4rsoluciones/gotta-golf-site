<?php

namespace AndresGotta\Academy\Application\Handler;

use AndresGotta\Academy\Application\Command\SetRoutineFeedbackCommand;
use AndresGotta\Academy\Application\Command\SetRoutineStepStatusCommand;
use AndresGotta\Academy\Domain\Entity\Routine;
use AndresGotta\Academy\Domain\Repository\RoutineRepositoryInterface;

final class SetRoutineStepStatusHandler
{
    /**
     * @var RoutineRepositoryInterface
     */
    private $repository;

    /**
     * AddRoutineStepHandler constructor.
     *
     * @param RoutineRepositoryInterface $repository
     */
    public function __construct(RoutineRepositoryInterface $repository)
    {
        $this->repository = $repository;
    }

    /**
     * @param SetRoutineStepStatusCommand $command
     */
    public function handle(SetRoutineStepStatusCommand $command)
    {
        /* @var Routine $routine */
        $routine = $command->getRoutine();
        $number = $command->getStep();
        if ($command->isCompleted()) {
            $routine->completeStep($number);
        } else {
            $routine->clearStep($number);
        }

        $this->repository->addRoutine($routine);
    }
}
