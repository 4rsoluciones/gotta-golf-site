<?php

namespace AndresGotta\Academy\Application\Handler;

use AndresGotta\Academy\Application\Command\AddRoutineStepCommand;
use AndresGotta\Academy\Domain\Entity\Routine;
use AndresGotta\Academy\Domain\Repository\RoutineRepositoryInterface;

final class AddRoutineStepHandler
{
    /**
     * @var RoutineRepositoryInterface
     */
    private $repository;

    /**
     * AddRoutineStepHandler constructor.
     *
     * @param RoutineRepositoryInterface $repository
     */
    public function __construct(RoutineRepositoryInterface $repository)
    {
        $this->repository = $repository;
    }

    /**
     * @param AddRoutineStepCommand $command
     */
    public function handle(AddRoutineStepCommand $command)
    {
        /* @var Routine $routine */
        $routine = $command->getRoutine();
        $routine->createStep($command->getDescription());

        $this->repository->addRoutine($routine);
    }
}
