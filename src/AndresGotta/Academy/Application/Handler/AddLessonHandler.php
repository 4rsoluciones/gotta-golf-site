<?php

namespace AndresGotta\Academy\Application\Handler;

use AndresGotta\Academy\Application\Command\AddLessonCommand;
use AndresGotta\Academy\Domain\Entity\Lesson;
use AndresGotta\Academy\Domain\Factory\LessonFactory;
use AndresGotta\Academy\Domain\Repository\IndicationRepositoryInterface;
use AndresGotta\Academy\Domain\Repository\LessonRepositoryInterface;

final class AddLessonHandler
{
    /**
     * @var LessonRepositoryInterface
     */
    private $lessonRepository;

    /**
     * @var LessonFactory
     */
    private $factory;

    /**
     * @var IndicationRepositoryInterface
     */
    private $indicationRepository;

    /**
     * AddLessonHandler constructor.
     * @param LessonRepositoryInterface $lessonRepository
     * @param LessonFactory $factory
     * @param IndicationRepositoryInterface $indicationRepository
     */
    public function __construct(LessonRepositoryInterface $lessonRepository, LessonFactory $factory, IndicationRepositoryInterface $indicationRepository)
    {
        $this->lessonRepository = $lessonRepository;
        $this->factory = $factory;
        $this->indicationRepository = $indicationRepository;
    }

    /**
     * @param AddLessonCommand $command
     * @return Lesson[]
     */
    public function handle(AddLessonCommand $command)
    {
        $lessons = [];
        foreach ($command->getStudents() as $student) {
            $lesson = $this->factory->create(
                $command->getTitle(),
                $command->getDate(),
                $command->getPlace(),
                $command->getTeacher(),
                $student,
                $command->getResume()
            );

            /* @var integer $indicationId */
            foreach ($command->getIndications() as $indicationId) {
                $indication = $this->indicationRepository->find($indicationId);
                $lesson->linkReportToIndication($indication);
            }

            $this->lessonRepository->addLesson($lesson);
            $lessons[] = $lesson;
        }

        return $lessons;
    }
}
