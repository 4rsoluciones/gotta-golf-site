<?php

namespace AndresGotta\Academy\Application\Handler;

use AndresGotta\Academy\Application\Command\AddIndicationCommand;
use AndresGotta\Academy\Domain\Entity\Indication;
use AndresGotta\Academy\Domain\Entity\Lesson;
use AndresGotta\Academy\Domain\Repository\IndicationRepositoryInterface;

final class AddIndicationHandler
{
    /**
     * @var IndicationRepositoryInterface
     */
    private $indicationRepository;

    /**
     * AddIndicationHandler constructor.
     * @param IndicationRepositoryInterface $indicationRepository
     */
    public function __construct(IndicationRepositoryInterface $indicationRepository)
    {
        $this->indicationRepository = $indicationRepository;
    }

    /**
     * @param AddIndicationCommand $command
     * @return Indication
     */
    public function handle(AddIndicationCommand $command)
    {
        $indication = new Indication($command->getMediaMaterial(), $command->getName(), $command->getDescription(), $command->getGoals());
        $indication->setTeacher($command->getTeacher());
        $indication->setAlternatives($command->getAlternatives());
        $indication->setRequirements($command->getRequirements());
        $indication->setSecurity($command->getSecurity());
        $indication->setAcademy($command->getAcademy());

        $this->indicationRepository->addIndication($indication);

        return $indication;
    }
}
