<?php

namespace AndresGotta\Academy\Application\Handler;

use AndresGotta\Academy\Application\Command\SetLessonFeedbackCommand;
use AndresGotta\Academy\Domain\Entity\Lesson;
use AndresGotta\Academy\Domain\Repository\LessonRepositoryInterface;

final class SetLessonFeedbackHandler
{
    /**
     * @var LessonRepositoryInterface
     */
    private $repository;

    /**
     * SetLessonFeedbackHandler constructor.
     *
     * @param LessonRepositoryInterface $repository
     */
    public function __construct(LessonRepositoryInterface $repository)
    {
        $this->repository = $repository;
    }

    /**
     * @param SetLessonFeedbackCommand $command
     */
    public function handle(SetLessonFeedbackCommand $command)
    {
        /* @var Lesson $lesson */
        $lesson = $command->getLesson();
        $lesson->applyFeedback($command->getValue(), $command->getNotes());

        $this->repository->addLesson($lesson);
    }
}
