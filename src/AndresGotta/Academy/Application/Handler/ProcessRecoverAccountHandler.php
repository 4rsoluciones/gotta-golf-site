<?php

namespace AndresGotta\Academy\Application\Handler;

use AndresGotta\Academy\Application\Command\ProcessRecoverAccountCommand;
use WebFactory\Bundle\UserBundle\Model\UserManager;
use WebFactory\Bundle\UserBundle\Service\GottaCenterApiInterface;

final class ProcessRecoverAccountHandler
{
    /**
     * @var UserManager
     */
    private $userManager;
    /**
     * @var GottaCenterApiInterface
     */
    private $api;

    /**
     * ProcessRecoverAccountHandler constructor.
     * @param UserManager $userManager
     * @param GottaCenterApiInterface $api
     */
    public function __construct(UserManager $userManager, GottaCenterApiInterface $api)
    {
        $this->userManager = $userManager;
        $this->api = $api;
    }

    /**
     * @param ProcessRecoverAccountCommand $command
     */
    public function handle(ProcessRecoverAccountCommand $command)
    {
        $user = $this->userManager->findUserByConfirmationToken($command->getConfirmationToken());
        if (!$user) {
            throw new \InvalidArgumentException('User not found with this confirmation token');
        }

        $user->setPlainPassword($command->getPlainPassword());
        $user->setConfirmationToken(null);
        $user->setPasswordRequestedAt(null);

        $this->userManager->updateUser($user);
        $this->api->changePassword($user->getEmail(), $user->getPassword());
    }
}
