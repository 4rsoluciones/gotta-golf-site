<?php

namespace AndresGotta\Academy\Application\Handler;

use AndresGotta\Academy\Application\Command\AddRoutineCommand;
use AndresGotta\Academy\Domain\Entity\Routine;
use AndresGotta\Academy\Domain\Factory\RoutineFactory;
use AndresGotta\Academy\Domain\Repository\RoutineRepositoryInterface;

final class AddRoutineHandler
{
    /**
     * @var RoutineRepositoryInterface
     */
    private $repository;
    /**
     * @var RoutineFactory
     */
    private $factory;

    /**
     * AddRoutineHandler constructor.
     * @param RoutineRepositoryInterface $repository
     * @param RoutineFactory $factory
     */
    public function __construct(RoutineRepositoryInterface $repository, RoutineFactory $factory)
    {
        $this->repository = $repository;
        $this->factory = $factory;
    }

    /**
     * @param AddRoutineCommand $command
     * @return Routine[]
     */
    public function handle(AddRoutineCommand $command)
    {
        $result = [];
        foreach ($command->getStudents() as $student) {
            /* @var Routine $routine */
            $routine = $this->factory->create($command->getTitle(), $command->getResume(), $command->getTeacher());
            $routine->assignStudent($student);
            $routine->setAcademy($command->getAcademy());

            foreach ($command->getSteps() as $step) {
                $routine->createStep($step->getTitle(), $step->getDescription());
            }

            $this->repository->addRoutine($routine);
            $result[] = $routine;
        }

        return $result;
    }
}
